<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
    cargo_details_modal: '{{ $cargo_details_modal }}',
    change_address_modal: '{{ $change_address_modal }}',
    change_address_cons_modal: '{{ $change_address_cons_modal }}',
    create_addrs_modal: '{{ $create_addrs_modal }}',
    create_addrs_cons_modal: '{{ $create_addrs_cons_modal }}',
    confirmshipper_modal: '{{ $confirmshipper_modal }}',
    viewExisting_modal: '{{ $viewExisting_modal }}',
    consigneedetails_modal: '{{ $consigneedetails_modal }}',
    bookstatlog_modal: '{{ $bookstatlog_modal }}',
    confirmsub_modal: '{{ $confirmsub_modal }}',
    {{-- book_again_modal: '{{ $book_again_modal }}', --}}
    {{-- current_tab: @entangle('current_tab'), --}}
    current_tab: {{ $current_tab }},

}">
    <x-loading></x-loading>
    @if ($confirmation_modal)
        <x-modal id="confirmation_modal" size="w-auto">
            <x-slot name="body">
                <span class="relative block">
                    <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                        wire:click="$set('confirmation_modal', false)">
                    </span>
                </span>
                <h2 class="text-xl text-center">
                    Are you sure you want to submit new Booking?
                </h2>


                <div class="flex justify-center space-x-3">
                    <button type="button" wire:click="$set('confirmation_modal', false)"
                        class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                        No
                    </button>
                    <button type="button" wire:click="submit"
                        class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                        Yes
                    </button>
                </div>
            </x-slot>
        </x-modal>
    @endif
    @if ($cargo_details_modal)
        <x-modal id="cargo_details_modal" size="w-2/5">
            <x-slot name="body">
                <div class="col-span-12 p-6 text-left">
                    <div class="">
                        <table class="overflow-hidden">
                            {{-- <x-slot name="tbody"> --}}

                            <div class="col-span-12 mb-4 border-0 border-b-2 border-blue">
                                <h1 class="mb-1 text-xl font-semibold text-left text-blue">Cargo Details
                                </h1>
                            </div>
                            <tr class="font-normal bg-white border-none">
                                <td class="w-1/5 text-base text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    Quantity :
                                </td>
                                <td class="w-1/5 text-base font-semibold text-left text-black whitespace-nowrap">
                                    @php $qtycnt0 = 0; @endphp
                                    @foreach ($cardetsss as $d => $cardet)
                                        @php $qtycnt0 += $cardet['quantity_3']; @endphp
                                    @endforeach
                                    <table>
                                        <tr>
                                            <td>
                                                {{ $qtycnt0 }}
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="font-normal bg-white border-none">
                                <td class="w-1/5 text-base text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    Weight (Kg) :
                                </td>
                                <td class="w-1/5 text-base font-semibold text-left text-black whitespace-nowrap">
                                    @php $qtycntw = 0; @endphp
                                    @foreach ($cardetsss as $d => $cardet)
                                        @php $qtycntw += $cardet['weight_3']; @endphp
                                    @endforeach
                                    <table>
                                        <tr>
                                            <td>
                                                {{ $qtycntw }}
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="font-normal bg-white border-none">
                                <td class="w-1/5 text-base text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    Dimensions (LxWxH in cm) :
                                </td>
                                <td class="w-1/5 text-base font-semibold text-left text-black whitespace-nowrap">
                                    @foreach ($cardetsss as $d => $cardet)
                                        <table>
                                            <tr>
                                                <td>
                                                    {{ $cardet['length_3'] }} X {{ $cardet['width_3'] }} X
                                                    {{ $cardet['height_3'] }}
                                                </td>
                                            </tr>
                                        </table>
                                    @endforeach
                                </td>
                            </tr>
                            <tr class="font-normal bg-white border-0">
                                <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    Declared Value :
                                </td>
                                <td class="w-1/5 text-base font-semibold text-left text-black whitespace-nowrap">
                                    {{ $decval_summary }}
                                </td>
                            </tr>
                            <tr class="font-normal bg-white border-0">
                                <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    Transport Mode :
                                </td>
                                <td class="w-1/5 text-base font-semibold text-left text-black whitespace-nowrap">
                                    {{ $transposrt_mode_3_summary }}
                                </td>
                            </tr>
                            <tr class="font-normal bg-white border-0">
                                <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    Service Mode :
                                </td>
                                <td class="w-1/5 text-base font-semibold text-left text-black whitespace-nowrap">
                                    {{ $service_mode_3_summary == 1
                                        ? 'Airport - Airport'
                                        : ($service_mode_3_summary == 2
                                            ? 'Airport - Door'
                                            : ($service_mode_3_summary == 3
                                                ? 'Door - Airport'
                                                : ($service_mode_3_summary == 4
                                                    ? 'Door - Door'
                                                    : ($service_mode_3_summary == 5
                                                        ? 'Port - Port'
                                                        : ($service_mode_3_summary == 6
                                                            ? 'Port - Door'
                                                            : ($service_mode_3_summary == 7
                                                                ? 'Door - Port'
                                                                : 'Door - Door')))))) }}
                                </td>

                            </tr>
                            <tr class="font-normal bg-white border-0">
                                <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    Description of Goods :
                                </td>
                                <td class="w-1/5 text-base font-semibold text-left text-black whitespace-nowrap">
                                    {{ $description_goods_3_summary }}
                                </td>
                            </tr>
                            <tr class="font-normal bg-white border-0">
                                <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    Mode of Payment :
                                </td>
                                <td class="w-1/5 text-base font-semibold text-left text-black whitespace-nowrap">
                                    {{ $mode_of_payment_3_summary }}
                                </td>
                            </tr>
                            {{-- </x-slot> --}}
                        </table>
                    </div>
                </div>
            </x-slot>
        </x-modal>
    @endif
    @if ($change_address_modal)
        <x-modal id="change_address_modal" size="w-1/2">
            <x-slot name="body">
                <form>
                    <ul
                        class="w-full p-4 text-sm font-medium text-gray-900 bg-white border border-gray-200 rounded-lg shadow-md dark:bg-gray-700 dark:border-gray-600 dark:text-white">
                        @foreach ($shipperaddresses as $shipperaddress)
                            @if ($shipperaddress->address_label == 1)
                                <li class="w-full pb-4 border-b border-gray-200 rounded-t-lg dark:border-gray-600">
                                    <div class="flex items-center pl-3">

                                        @if ($shipperAddressList == $shipperaddress->id)
                                            <input type="radio" name="shipperAddressList"
                                                class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500"
                                                value="{{ $shipperaddress->id }}" wire:model="shipperAddressList"
                                                @if ($shipperaddress->is_primary == 1) checked @endif>
                                            {{-- @dd($shipperaddress->id); --}}
                                            <label for="list-radio-license"
                                                class="flex w-full py-3 ml-2 text-base font-medium text-gray-900 dark:text-gray-300">
                                                <p>Home</p>
                                                @if ($shipperaddress->is_primary == 1)
                                                    <p class="text-sm italic font-normal">(Primary)</p>
                                                @endif
                                            </label>
                                        @else
                                            <input type="radio" name="shipperAddressList"
                                                class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500"
                                                value="{{ $shipperaddress->id }}" wire:model="shipperAddressList"
                                                @if ($shipperaddress->is_primary == 1) checked @endif>
                                            <label for="list-radio-license"
                                                class="flex w-full py-3 ml-2 text-base font-medium text-gray-900 dark:text-gray-300">
                                                <p>Home</p>
                                                @if ($shipperaddress->is_primary == 1)
                                                    <p class="text-sm italic font-normal">(Primary)</p>
                                                @endif
                                            </label>
                                        @endif

                                    </div>
                                    <div class="ml-10 -mt-2 font-normal">
                                        {{ $shipperaddress->address_line_1 }}
                                        {{ $shipperaddress->address_line_2 }}
                                        {{ $shipperaddress->stateRef->name }}, {{ $shipperaddress->cityRef->name }},
                                        {{ $shipperaddress->barangayRef->name }}
                                    </div>
                                </li>
                            @endif
                            @if ($shipperaddress->address_label == 2)
                                <li class="w-full pb-4 border-b border-gray-200 rounded-t-lg dark:border-gray-600">
                                    <div class="flex items-center pl-3">

                                        <input type="radio" name="shipperAddressList"
                                            class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500"
                                            value="{{ $shipperaddress->id }}" wire:model="shipperAddressList"
                                            @if ($shipperaddress->is_primary != 1) checked @endif>

                                        <label for="list-radio-id"
                                            class="w-full py-3 ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Office</label>
                                    </div>
                                    <div class="ml-10 -mt-2 font-normal">
                                        {{ $shipperaddress->address_line_1 }}
                                        {{ $shipperaddress->address_line_2 }}
                                        {{ $shipperaddress->stateRef->name }}, {{ $shipperaddress->cityRef->name }},
                                        {{ $shipperaddress->barangayRef->name }}
                                    </div>
                                </li>
                            @endif
                            @if ($shipperaddress->address_label == 3)
                                <li class="w-full pb-4 border-b border-gray-200 rounded-t-lg dark:border-gray-600">
                                    <div class="flex items-center pl-3">

                                        <input type="radio" name="shipperAddressList"
                                            class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500"
                                            {{-- value="{{ $shipperaddress->id }} {{ $shipperaddress->address_line_1 }} {{ $shipperaddress->address_line_2 }} {{ $shipperaddress->stateRef->name }}, {{ $shipperaddress->cityRef->name }}, {{ $shipperaddress->barangayRef->name }}" --}} value="{{ $shipperaddress->id }}"
                                            wire:model="shipperAddressList"
                                            @if ($shipperaddress->is_primary != 1) checked @endif>

                                        <label for="list-radio-id"
                                            class="w-full py-3 ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Warehouse</label>
                                    </div>
                                    <div class="ml-10 -mt-2 font-normal">
                                        {{ $shipperaddress->address_line_1 }}
                                        {{ $shipperaddress->address_line_2 }}
                                        {{ $shipperaddress->stateRef->name }}, {{ $shipperaddress->cityRef->name }},
                                        {{ $shipperaddress->barangayRef->name }}
                                    </div>
                                </li>
                            @endif
                        @endforeach
                        <li class="w-full p-4 border-b border-gray-200 rounded-t-lg dark:border-gray-600">
                            <div class="flex" style="margin-left:40%;">
                                <svg class="w-6 h-6 cursor-pointer text-blue" aria-hidden="true" focusable="false"
                                    data-prefix="far" data-icon="edit" role="img"
                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                    <path fill="currentColor"
                                        d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z">
                                    </path>
                                </svg>
                                <span wire:click="actioncreateaddrs({}, 'create_address')"
                                    class="mt-1 ml-2 underline cursor-pointer text-blue">
                                    Add Address</span>
                            </div>
                        </li>
                    </ul>
                    <div class="mt-2" style="margin-left: 30%">
                        <x-input-error for="shipperAddressList" />
                    </div>
                    <x-button type="button" wire:click="actionchangeaddSubmit({},'')" title="Submit"
                        class=" w-30 h-8 mt-2 bg-blue text-white hover:bg-[#002161]" style="float:right;" />
                </form>
            </x-slot>
        </x-modal>
    @endif

    @if ($change_address_cons_modal)
        <x-modal id="change_address_cons_modal" size="w-1/2">
            <x-slot name="body">
                <form>
                    <ul
                        class="w-full p-4 text-sm font-medium text-gray-900 bg-white border border-gray-200 rounded-lg shadow-md dark:bg-gray-700 dark:border-gray-600 dark:text-white">
                        @foreach ($consigneeaddresses as $k => $consigneeaddress)
                            @if ($consigneeaddress->address_label == 1)
                                <li class="w-full pb-4 border-b border-gray-200 rounded-t-lg dark:border-gray-600">
                                    <div class="flex items-center pl-3">
                                        {{-- @dd($consigneeaddressList) --}}
                                        @if ($consigneeAddressList == $consigneeaddress->id)
                                            <input type="radio" name="consigneeAddressList"
                                                class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500"
                                                value="{{ $consigneeaddress->id }}" wire:model="consigneeAddressList"
                                                @if ($consigneeaddress->is_primary == 1) checked @endif>
                                            <label for="list-radio-license"
                                                class="flex w-full py-3 ml-2 text-base font-medium text-gray-900 dark:text-gray-300">
                                                <p>Home</p>
                                                @if ($consigneeaddress->is_primary == 1)
                                                    <p class="text-sm italic font-normal">(Primary)</p>
                                                @endif
                                            </label>
                                        @else
                                            <input type="radio" name="consigneeAddressList"
                                                class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500"
                                                value="{{ $consigneeaddress->id }}" wire:model="consigneeAddressList"
                                                @if ($consigneeaddress->is_primary == 1) checked="checked" @endif>
                                            <label for="list-radio-license"
                                                class="flex w-full py-3 ml-2 text-base font-medium text-gray-900 dark:text-gray-300">
                                                <p>Home</p>
                                                @if ($consigneeaddress->is_primary == 1)
                                                    <p class="text-sm italic font-normal">(Primary)</p>
                                                @endif
                                            </label>
                                        @endif
                                    </div>
                                    <div class="ml-10 -mt-2 font-normal">
                                        {{ $consigneeaddress->address_line_1 }}
                                        {{ $consigneeaddress->address_line_2 }}
                                        {{ $consigneeaddress->stateRef->name }},
                                        {{ $consigneeaddress->cityRef->name }},
                                        {{ $consigneeaddress->barangayRef->name }}
                                    </div>
                                </li>
                            @endif
                            @if ($consigneeaddress->address_label == 2)
                                <li class="w-full pb-4 border-b border-gray-200 rounded-t-lg dark:border-gray-600">
                                    <div class="flex items-center pl-3">

                                        <input type="radio" name="consigneeAddressList"
                                            class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500"
                                            value="{{ $consigneeaddress->id }}" wire:model="consigneeAddressList"
                                            @if ($consigneeaddress->is_primary != 1) checked @endif>

                                        <label for="list-radio-id"
                                            class="w-full py-3 ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Office</label>
                                    </div>
                                    <div class="ml-10 -mt-2 font-normal">
                                        {{ $consigneeaddress->address_line_1 }}
                                        {{ $consigneeaddress->address_line_2 }}
                                        {{ $consigneeaddress->stateRef->name }},
                                        {{ $consigneeaddress->cityRef->name }},
                                        {{ $consigneeaddress->barangayRef->name }}
                                    </div>
                                </li>
                            @endif
                            @if ($consigneeaddress->address_label == 3)
                                <li class="w-full pb-4 border-b border-gray-200 rounded-t-lg dark:border-gray-600">
                                    <div class="flex items-center pl-3">

                                        <input type="radio" name="consigneeAddressList"
                                            class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500"
                                            value="{{ $consigneeaddress->id }}" wire:model="consigneeAddressList"
                                            @if ($consigneeaddress->is_primary != 1) checked @endif>

                                        <label for="list-radio-id"
                                            class="w-full py-3 ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Warehouse</label>
                                    </div>
                                    <div class="ml-10 -mt-2 font-normal">
                                        {{ $consigneeaddress->address_line_1 }}
                                        {{ $consigneeaddress->address_line_2 }}
                                        {{ $consigneeaddress->stateRef->name }},
                                        {{ $consigneeaddress->cityRef->name }},
                                        {{ $consigneeaddress->barangayRef->name }}
                                    </div>
                                </li>
                            @endif
                        @endforeach
                        <li class="w-full p-4 border-b border-gray-200 rounded-t-lg dark:border-gray-600">
                            <div class="flex" style="margin-left:40%;">
                                <svg class="w-6 h-6 cursor-pointer text-blue" aria-hidden="true" focusable="false"
                                    data-prefix="far" data-icon="edit" role="img"
                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                    <path fill="currentColor"
                                        d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z">
                                    </path>
                                </svg>
                                <span wire:click="actioncreateaddrscons({}, 'create_address_cons')"
                                    class="mt-1 ml-2 underline cursor-pointer text-blue">
                                    Add Address</span>
                            </div>
                        </li>
                    </ul>
                    <div class="mt-2" style="margin-left: 30%">
                        <x-input-error for="consigneeAddressList" />
                    </div>
                    <x-button type="button" wire:click="actionchangeaddSubmitCons({},'')" title="Submit"
                        class=" w-30 h-8 mt-2 bg-blue text-white hover:bg-[#002161]" style="float:right;" />
                </form>
            </x-slot>
        </x-modal>
    @endif

    @if ($confirmshipper_modal)
        <x-modal id="confirmshipper_modal" size="w-1/2">
            <x-slot name="body">
                <form>
                    <div class="grid grid-cols-12 gap-2 p-2">
                        <div class="col-span-12 font-medium text-center">
                            <p> Our system shows that this customer has already an existing booking<br>
                                under Booking Reference Number <span class="text-blue">{{ $booking_ref_no }}</span>,
                                and
                                has not been completed<br>
                                for pick up. Would you like to submit new booking?
                            </p>
                        </div>
                        {{-- <div class="col-span-2">.</div> --}}
                        <div class="flex justify-center col-span-12 space-x-4">
                            <x-button type="button" wire:click="actionViewExisting({},'')"
                                title="View Existing Booking"
                                class="h-8 mt-4 bg-white w-25 text-blue hover:bg-gray-200" />
                            {{-- </div>
                    <div class="col-span-4"> --}}
                            <x-button type="button" wire:click="actionSubmitNewBooking({},'')"
                                title="Submit New Booking"
                                class=" w-25 h-8 mt-4 bg-blue text-white hover:bg-[#002161]" />
                        </div>
                        {{-- <div class="col-span-2">.</div> --}}
                    </div>
                </form>
            </x-slot>
        </x-modal>
    @endif
    @if ($create_addrs_modal)
        <x-modal id="create_addrs_modal" size="w-1/3">
            <x-slot name="body">
                <div class="col-span-12 p-4 text-left">
                    <div class="">

                        <x-table.table class="overflow-hidden">
                            <x-slot name="tbody">
                                <div class="col-span-12 mb-4">
                                    <h1 class="text-xl font-semibold text-left text-black">Add New Address
                                    </h1>
                                </div>
                                {{-- {{dd($customerinfos);}} --}}
                                <tr class="font-normal bg-white border-0">
                                    <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Customer No :
                                    </td>
                                    <td class="w-1/5 text-sm font-medium text-left text-black whitespace-nowrap">
                                        {{-- {{ $custno }} --}} {{ $customer_no_2 }}
                                    </td>
                                </tr>
                                <tr class="font-normal bg-white border-0">
                                    <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Shipper :
                                    </td>
                                    <td class="w-1/5 text-sm font-medium text-left text-black whitespace-nowrap">
                                        {{ $first_name_2 }} {{ $middle_name_2 }} {{ $last_name_2 }}
                                    </td>
                                </tr>
                                <tr class="font-normal bg-white border-0">
                                    <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Company Name :
                                    </td>
                                    <td class="w-1/5 text-sm font-medium text-left text-black whitespace-nowrap">
                                        @if (isset($company_name_2))
                                            {{ $company_name_2 }}
                                        @endif
                                    </td>
                                </tr>
                                <tr class="font-normal bg-white border-0">
                                    <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Contact No :

                                    </td>
                                    <td class="w-1/5 text-sm font-medium text-left text-black whitespace-nowrap">
                                        {{ $mobile_number_2 }}
                                    </td>
                                </tr>

                            </x-slot>
                        </x-table.table>

                        <form>
                            <div class="grid grid-cols-12 gap-4 mt-4 text-sm">
                                <div class="col-span-12 mt-2">
                                    <x-label for="add1" value="Address Label" :required="true" />
                                    <x-input-error for="addresslabel" />
                                </div>
                                <div class="col-span-3 -mt-4">
                                    <x-input-radio label="Home" name="addresslabel" value="1"
                                        wire:model.defer="addresslabel" />
                                </div>
                                <div class="col-span-3 -mt-4">
                                    <x-input-radio label="Office" name="addresslabel" value="2"
                                        wire:model.defer="addresslabel" />
                                </div>
                                <div class="col-span-3 -mt-4">
                                    <x-input-radio label="Warehouse" name="addresslabel" value="3"
                                        wire:model.defer="addresslabel" />
                                </div>
                                <div class="col-span-3">
                                </div>

                                <div class="col-span-12">
                                    <x-label for="add1" value="Address Line1" :required="true" />
                                    <x-input class="w-full rounded-md h-9" type="text" name="add1"
                                        wire:model.defer='add1'>
                                    </x-input>
                                    <x-input-error for="add1" />
                                </div>

                                <div class="col-span-12">
                                    <x-label for="add2" value="Address Line2" />
                                    <x-input class="w-full rounded-md h-9" type="text" name="add2"
                                        wire:model.defer='add2'>
                                    </x-input>
                                    <x-input-error for="add2" />
                                </div>

                                <div class="col-span-6">
                                    <div wire:init="provinceState">
                                        <x-label for="state" value="State/Province" :required="true" />
                                        <x-select name="state" wire:model='state'>
                                            <option value="">Select</option>
                                            @foreach ($state_province_references as $state_province_reference)
                                                <option value="{{ $state_province_reference->id }}">
                                                    {{ $state_province_reference->name }}
                                                </option>
                                            @endforeach
                                        </x-select>
                                        <x-input-error for="state" />
                                    </div>
                                </div>

                                <div class="col-span-6">
                                    {{-- <div wire:init="loadShipperCityMunicipality"> --}}
                                    <x-label for="city" value="City/Municipality" :required="true" />
                                    @if ($state != '')
                                        <x-select name="city" wire:model='city'>
                                            <option value="">Select</option>
                                            @foreach ($city_municipality_references as $city_municipality_reference)
                                                <option value="{{ $city_municipality_reference->id }}">
                                                    {{ $city_municipality_reference->name }}
                                                </option>
                                            @endforeach
                                        </x-select>
                                        <x-input-error for="city" />
                                    @else
                                        <x-select disabled name="city" wire:model='city'>
                                            <option value="">Select</option>
                                            @foreach ($city_municipality_references as $city_municipality_reference)
                                                <option value="{{ $city_municipality_reference->id }}">
                                                    {{ $city_municipality_reference->name }}
                                                </option>
                                            @endforeach
                                        </x-select>
                                        <x-input-error for="city" />
                                    @endif

                                    {{-- </div> --}}

                                </div>

                                <div class="col-span-6">
                                    {{-- <div wire:init="loadShipperBarangay"> --}}
                                    <x-label for="barangay" value="Barangay" :required="true" />
                                    @if ($city != '')
                                        <x-select name="barangay" wire:model='barangay'>
                                            <option value="">Select</option>
                                            @foreach ($barangay_references as $barangay_reference)
                                                <option value="{{ $barangay_reference->id }}">
                                                    {{ $barangay_reference->name }}
                                                </option>
                                            @endforeach
                                        </x-select>
                                        <x-input-error for="barangay" />
                                    @else
                                        <x-select disabled name="barangay" wire:model='barangay'>
                                            <option value="">Select</option>
                                            @foreach ($barangay_references as $barangay_reference)
                                                <option value="{{ $barangay_reference->id }}">
                                                    {{ $barangay_reference->name }}
                                                </option>
                                            @endforeach
                                        </x-select>
                                        <x-input-error for="barangay" />
                                    @endif

                                </div>

                                <div class="col-span-6">
                                    <x-label for="postal" value="Postal Code" />
                                    <x-input disabled class="w-full rounded-md h-9" type="text" value="2040"
                                        name="postal" wire:model='postal'>
                                    </x-input>
                                    <x-input-error for="postal" />
                                </div>
                            </div>
                        </form>

                        <div class="col-span-12">
                            <x-button type="button" wire:click="actionCreateaddSubmit({},'')" title="Save Address"
                                class=" w-full h-8 mt-6 bg-blue text-white hover:bg-[#002161]" />
                        </div>

                    </div>
                </div>
            </x-slot>
        </x-modal>
    @endif
    @if ($create_addrs_cons_modal)
        <x-modal id="create_addrs_cons_modal" size="w-1/3">
            <x-slot name="body">
                <div class="col-span-12 p-4 text-left">
                    <div class="">

                        <x-table.table class="overflow-hidden">
                            <x-slot name="tbody">
                                <div class="col-span-12 mb-4">
                                    <h1 class="text-xl font-semibold text-left text-black">Add New Address
                                    </h1>
                                </div>
                                <tr class="font-normal bg-white border-0">
                                    <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Customer No :
                                    </td>
                                    <td class="w-1/5 text-sm font-medium text-left text-black whitespace-nowrap">
                                        @if (isset($cons[$idcons][$idcons]['customer_no_3']))
                                            {{ $cons[$idcons][$idcons]['customer_no_3'] }}
                                        @endif
                                    </td>
                                </tr>
                                <tr class="font-normal bg-white border-0">
                                    <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Shipper :
                                    </td>
                                    <td class="w-1/5 text-sm font-medium text-left text-black whitespace-nowrap">
                                        @if (isset($cons[$idcons][$idcons]['first_name_3']))
                                            {{ $cons[$idcons][$idcons]['first_name_3'] }}
                                            {{ $cons[$idcons][$idcons]['middle_name_3'] }}
                                            {{ $cons[$idcons][$idcons]['last_name_3'] }}
                                        @endif
                                    </td>
                                </tr>
                                <tr class="font-normal bg-white border-0">
                                    <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Company Name :
                                    </td>
                                    <td class="w-1/5 text-sm font-medium text-left text-black whitespace-nowrap">
                                        @if (isset($cons[$idcons][$idcons]['company_name_3']))
                                            {{ $cons[$idcons][$idcons]['company_name_3'] }}
                                        @endif
                                    </td>
                                </tr>
                                <tr class="font-normal bg-white border-0">
                                    <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Contact No :
                                    </td>
                                    <td class="w-1/5 text-sm font-medium text-left text-black whitespace-nowrap">
                                        @if (isset($cons[$idcons][$idcons]['mobile_number_3']))
                                            {{ $cons[$idcons][$idcons]['mobile_number_3'] }}
                                        @endif
                                    </td>
                                </tr>

                            </x-slot>
                        </x-table.table>

                        <form>
                            <div class="grid grid-cols-12 gap-4 mt-4 text-sm">
                                <div class="col-span-12 mt-2">
                                    <x-label for="add1" value="Address Label" :required="true" />
                                    <x-input-error for="addresslabel" />
                                </div>
                                <div class="col-span-3 -mt-4">
                                    <x-input-radio label="Home" name="addresslabel" value="1"
                                        wire:model.defer="addresslabel" />
                                </div>
                                <div class="col-span-3 -mt-4">
                                    <x-input-radio label="Office" name="addresslabel" value="2"
                                        wire:model.defer="addresslabel" />
                                </div>
                                <div class="col-span-3 -mt-4">
                                    <x-input-radio label="Warehouse" name="addresslabel" value="3"
                                        wire:model.defer="addresslabel" />
                                </div>
                                <div class="col-span-3">
                                </div>


                                <div class="col-span-12">
                                    <x-label for="add1" value="Address Line1" :required="true" />
                                    <x-input class="w-full rounded-md h-9" type="text" name="add1"
                                        wire:model.defer='add1'>
                                    </x-input>
                                    <x-input-error for="add1" />
                                </div>

                                <div class="col-span-12">
                                    <x-label for="add2" value="Address Line2" />
                                    <x-input class="w-full rounded-md h-9" type="text" name="add2"
                                        wire:model.defer='add2'>
                                    </x-input>
                                    <x-input-error for="add2" />
                                </div>

                                <div class="col-span-6">
                                    <div wire:init="provinceState">
                                        <x-label for="state" value="State/Province" :required="true" />
                                        <x-select name="state" wire:model='state'>
                                            <option value="">Select</option>
                                            @foreach ($state_province_references as $state_province_reference)
                                                <option value="{{ $state_province_reference->id }}">
                                                    {{ $state_province_reference->name }}
                                                </option>
                                            @endforeach
                                        </x-select>
                                        <x-input-error for="state" />
                                    </div>
                                </div>

                                <div class="col-span-6">
                                    {{-- <div wire:init="loadShipperCityMunicipality"> --}}
                                    <x-label for="city" value="City/Municipality" :required="true" />
                                    @if ($state != '')
                                        <x-select name="city" wire:model='city'>
                                            <option value="">Select</option>
                                            @foreach ($city_municipality_references as $city_municipality_reference)
                                                <option value="{{ $city_municipality_reference->id }}">
                                                    {{ $city_municipality_reference->name }}
                                                </option>
                                            @endforeach
                                        </x-select>
                                    @else
                                        <x-select disabled name="city" wire:model='city'>
                                            <option value="">Select</option>
                                            @foreach ($city_municipality_references as $city_municipality_reference)
                                                <option value="{{ $city_municipality_reference->id }}">
                                                    {{ $city_municipality_reference->name }}
                                                </option>
                                            @endforeach
                                        </x-select>
                                    @endif
                                    <x-input-error for="city" />
                                    {{-- </div> --}}

                                </div>

                                <div class="col-span-6">
                                    {{-- <div wire:init="loadShipperBarangay"> --}}
                                    <x-label for="barangay" value="Barangay" :required="true" />
                                    @if ($city != '')
                                        <x-select name="barangay" wire:model='barangay'>
                                            <option value="">Select</option>
                                            @foreach ($barangay_references as $barangay_reference)
                                                <option value="{{ $barangay_reference->id }}">
                                                    {{ $barangay_reference->name }}
                                                </option>
                                            @endforeach
                                        </x-select>
                                        <x-input-error for="barangay" />
                                    @else
                                        <x-select disabled name="barangay" wire:model='barangay'>
                                            <option value="">Select</option>
                                            @foreach ($barangay_references as $barangay_reference)
                                                <option value="{{ $barangay_reference->id }}">
                                                    {{ $barangay_reference->name }}
                                                </option>
                                            @endforeach
                                        </x-select>
                                        <x-input-error for="barangay" />
                                    @endif
                                </div>

                                <div class="col-span-6">
                                    <x-label for="postal" value="Postal Code" />
                                    <x-input disabled class="w-full rounded-md h-9" type="text" value="2040"
                                        name="postal" wire:model='postal'>
                                    </x-input>
                                    <x-input-error for="postal" />
                                </div>
                            </div>
                        </form>

                        <div class="col-span-12">
                            <x-button type="button" wire:click="actionCreateaddconsSubmit({},'')"
                                title="Save Address" class=" w-full h-8 mt-6 bg-blue text-white hover:bg-[#002161]" />
                        </div>

                    </div>
                </div>
            </x-slot>
        </x-modal>
    @endif
    @if ($viewExisting_modal)
        <x-modal id="viewExisting_modal" size="w-3/4">

            <x-slot name="body">
                <form>
                    <div class="grid grid-cols-12 gap-2 px-6 gap-x-4 ">
                        <div class="col-span-6">
                            @if ($pickupwalk == 1)
                                <div class="flex items-center justify-start">
                                    <div wire:click=""
                                        class="flex items-center justify-between px-6 py-2 text-sm text-white border border-gray-400 cursor-pointer bg-blue">
                                        <span>PICK UP</span>
                                    </div>
                                    <div wire:click=""
                                        class="flex justify-between px-6 py-2 text-sm text-left bg-white">
                                        <span class="">WALK IN</span>
                                    </div>
                                </div>
                            @else
                                <div class="flex items-center justify-start">
                                    <div wire:click="" class="flex justify-between px-6 py-2 text-sm text-left">
                                        <span>PICK UP</span>
                                    </div>
                                    <div wire:click=""
                                        class="flex items-center justify-between px-6 py-2 text-sm text-white border border-gray-400 cursor-pointer bg-blue">
                                        <span class="">WALK IN</span>
                                    </div>
                                </div>
                            @endif
                        </div>
                        {{-- {{dd($SDcbId);}} --}}
                        <div class="col-span-6">
                            <x-button type="button" wire:click="actionBookingStatLogs({},'')" {{-- {'id':{{ $viewbookingdetailsconsignee[0][$i]['id'] }}},'c_det') } --}}
                                title="Booking Status Logs" class=" w-22 h-10 bg-blue text-white hover:bg-[#002161]"
                                style="float: right;" />
                        </div>
                        <div class="w-11/12 col-span-6">
                            <table class="overflow-hidden">
                                <div class="col-span-12 pb-2 mb-4 border-b-2 border-blue">
                                    <h1 class="text-xl font-semibold text-left text-blue">Shipper Details
                                    </h1>
                                </div>

                                <tr class="font-normal bg-white border-0">
                                    <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Booking Reference No. :
                                    </td>
                                    <td class="w-1/5 text-sm font-medium text-left text-black ">
                                        {{ $SDbookingrefno }}
                                    </td>
                                </tr>
                                <tr class="font-normal bg-white border-0">
                                    <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Booking Status :
                                    </td>
                                    <td class="w-1/5 text-sm font-medium text-left text-black ">
                                        {{ $SDbookingstat }}
                                    </td>
                                </tr>
                                <tr class="font-normal bg-white border-0">
                                    <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Shipper :
                                    </td>
                                    <td class="w-1/5 text-sm font-medium text-left text-black ">
                                        {{ $SDfullname }}
                                    </td>
                                </tr>
                                <tr class="font-normal bg-white border-0">
                                    <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Company :
                                    </td>
                                    <td class="w-1/5 text-sm font-medium text-left text-black">
                                        {{ $SDcompany ?? '-' }}
                                    </td>
                                </tr>
                                <tr class="font-normal bg-white border-0">
                                    <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Contact Person :
                                    </td>
                                    <td class="w-1/5 text-sm font-medium text-left text-black ">
                                        {{ $SDfname }}
                                    </td>
                                </tr>
                                <tr class="font-normal bg-white border-0">
                                    <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Contact Number :
                                    </td>
                                    <td class="w-1/5 text-sm font-medium text-left text-black ">
                                        {{ $SDmobile_nos }}
                                    </td>
                                </tr>
                                <tr class="font-normal bg-white border-0">
                                    <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Email Address :
                                    </td>
                                    <td class="w-1/5 text-sm font-medium text-left text-black ">
                                        {{ $SDemail_address }}
                                    </td>
                                </tr>
                                <tr class="font-normal bg-white border-0">
                                    <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Customer Type :

                                    </td>
                                    <td class="w-1/5 text-sm font-medium text-left text-black ">
                                        {{ $SDcustomertype }}
                                    </td>
                                </tr>
                                <tr class="font-normal bg-white border-0">
                                    <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Activity Type :
                                    </td>
                                    <td class="w-1/5 text-sm font-medium text-left text-black ">
                                        {{ $SDacttype }}
                                    </td>
                                </tr>

                            </table>
                        </div>
                        <div class="w-11/12 col-span-6 ml-12">
                            <table class="overflow-hidden">
                                {{-- <x-slot name="tbody" class=""> --}}
                                <div class="col-span-12 pb-2 mb-4 border-b-2 border-blue">
                                    <h1 class="text-xl font-semibold text-left text-blue">Pick Up Details
                                    </h1>
                                </div>

                                <tr class="font-normal bg-white border-0">
                                    <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Complete Address :
                                    </td>
                                    <td class="w-1/5 text-sm font-medium text-left text-black">
                                        {{ $SDaddress }}
                                    </td>
                                </tr>
                                <tr class="font-normal bg-white border-0">
                                    <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Landmark :
                                    </td>
                                    <td class="w-1/5 text-sm font-medium text-left text-black whitespace-nowrap">
                                        -
                                    </td>
                                </tr>
                                <tr class="font-normal bg-white border-0">
                                    <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Timeslot :
                                    </td>
                                    <td class="w-1/5 text-sm font-medium text-left text-black whitespace-nowrap">
                                        {{ $SDtimeslot }}
                                    </td>
                                </tr>
                                {{-- </x-slot> --}}
                            </table>
                        </div>
                    </div>
                    {{-- <div class="grid grid-cols-12 gap-0 px-0 m-0 border-b-2 border-gray-400">
                </div> --}}
                    <div class="col-span-12 px-0 mx-0 mt-2 gap-x-0">
                        <hr class="text-black border border-gray-400 ">
                    </div>
                    <div class="grid grid-cols-12 gap-2 px-6 mt-4 gap-x-4">
                        <div class="col-span-6">
                            @if ($singlemulti == 2)
                                <div class="flex items-center justify-start">
                                    <div wire:click=""
                                        class="flex items-center justify-between px-6 py-2 text-sm text-white border border-gray-400 cursor-pointer bg-blue">
                                        <span>SINGLE</span>
                                    </div>
                                    <div wire:click="" class="flex justify-between px-6 py-2 text-sm text-left">
                                        <span class="">MULTIPLE</span>
                                    </div>
                                </div>
                            @else
                                <div class="flex items-center justify-start">
                                    <div wire:click="" class="flex justify-between px-6 py-2 text-sm text-left">
                                        <span>SINGLE</span>
                                    </div>
                                    <div wire:click=""
                                        class="flex items-center justify-between px-6 py-2 text-sm text-white border border-gray-400 cursor-pointer bg-blue">
                                        <span class="">MULTIPLE</span>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="col-span-6">
                        </div>
                        <div class="col-span-12 pb-2 mb-4 border-b-2 border-blue">
                            <h1 class="text-xl font-semibold text-left text-blue">Consignee Details
                            </h1>
                        </div>
                        <div class="col-span-12">
                            <div class="overflow-auto bg-white rounded-lg shadow-md">
                                <x-table.table class="overflow-hidden">
                                    <x-slot name="thead">
                                        <x-table.th name="No." style="padding-left:;" />
                                        <x-table.th name="Consignee Name" style="padding-left:%;" />
                                        <x-table.th name="Company" style="padding-left;" />
                                        <x-table.th name="Contact Person" style="padding-left:;" />
                                        <x-table.th name="Contact Number" style="padding-left:;" />
                                        <x-table.th name="Email Address" style="padding-left:;" />
                                        <x-table.th name="Customer Type" style="padding-left:;" />
                                        <x-table.th name="Drop Off Location" style="padding-left:;" />
                                    </x-slot>
                                    <x-slot name="tbody" class="text-xs">
                                        @if (isset($viewbookingdetailsconsignee[0]))
                                            @foreach ($viewbookingdetailsconsignee[0] as $i => $viewcons)
                                                <tr
                                                    class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8] font-semibold">
                                                    <td class="p-3 whitespace-nowrap" style="padding-left:;">
                                                        {{ $i + 1 }}.
                                                    </td>
                                                    <td class="w-1/5 p-3 whitespace-nowrap" style="padding-left:%;">
                                                        <span
                                                            wire:click="actionviewcdet({'id':{{ $viewbookingdetailsconsignee[0][$i]['id'] }}},'c_details') }}"
                                                            class="underline cursor-pointer text-blue">{{ $viewbookingdetailsconsignee[0][$i]['name'] }}</span>
                                                    </td>
                                                    <td class="w-1/5 p-3 whitespace-nowrap" style="padding-left:;">
                                                        {{ $viewbookingdetailsconsignee[0][$i]['company_name'] ?? '-' }}
                                                    </td>
                                                    <td class="w-1/5 p-3 whitespace-nowrap" style="padding-left:;">
                                                        {{ $viewbookingdetailsconsignee[0][$i]['name'] }}
                                                    </td>
                                                    <td class="w-1/5 p-3 whitespace-nowrap" style="padding-left:;">
                                                        {{ $viewbookingdetailsconsignee[0][$i]['mobile_number'] }}
                                                    </td>
                                                    <td class="w-1/5 p-3 whitespace-nowrap" style="padding-left:;">
                                                        {{ $viewbookingdetailsconsignee[0][$i]['email_address'] }}
                                                    </td>
                                                    <td class="w-1/5 p-3 whitespace-nowrap" style="padding-left:;">
                                                        {{ $viewbookingdetailsconsignee[0][$i]['account_type_id'] == 1 ? 'Individual' : 'Corporate' }}
                                                    </td>
                                                    <td class="w-1/5 p-3 whitespace-nowrap" style="padding-left:;">
                                                        {{ $viewbookingdetailsconsignee[0][$i]['address'] }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </x-slot>
                                </x-table.table>
                            </div>
                        </div>
                    </div>
                </form>
            </x-slot>
        </x-modal>
    @endif
    @if ($bookstatlog_modal)
        <x-modal id="bookstatlog_modal" size="w-1/3">
            <x-slot name="body">
                <div class="grid grid-cols-12 p-4">
                    <div class="col-span-12 pb-2 mb-4 border-b-2 border-blue">
                        <h1 class="text-lg font-semibold text-left text-blue">Booking Status Logs
                        </h1>
                    </div>
                    <div class="col-span-12 mb-4">
                        <table class="overflow-hidden">
                            {{-- <x-slot name="tbody"> --}}
                            <tr class="font-normal bg-white border-0">
                                <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    Booking Reference No. : <span
                                        class="text-lg font-semibold text-blue">{{ $bstatrefno }}</span>
                                </td>
                            </tr>
                            {{-- </x-slot> --}}
                        </table>
                    </div>
                    <div class="col-span-12">
                        <div class="overflow-auto bg-white border rounded-lg shadow-md">
                            <x-table.table class="overflow-hidden">
                                <x-slot name="thead">
                                    <x-table.th name="Booking Status" style="padding-left:2%;" />
                                    <x-table.th name="Date and Time" style="padding-left:%;" />

                                </x-slot>
                                <x-slot name="tbody">
                                    <tr
                                        class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8] font-medium">
                                        <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:2%;">
                                            {{ $bstatstat }}
                                        </td>
                                        <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                            {{-- {{ $viewbookingdetailsconsignee[0][$i]['account_type_id'] == 1 ? 'Individual' : 'Corporate' }} --}}
                                            {{ date('m/d/Y h:i A', strtotime($bstatcreated)) }}
                                        </td>
                                    </tr>
                                </x-slot>
                            </x-table.table>
                        </div>
                    </div>
                </div>
            </x-slot>
        </x-modal>
    @endif
    @if ($consigneedetails_modal)
        <x-modal id="consigneedetails_modal" size="w-2/5">
            <x-slot name="body">
                <form>
                    <div class="col-span-12 p-6 text-left">
                        <div class="">
                            <table class="overflow-hidden">
                                {{-- <x-slot name="tbody"> --}}
                                <div class="col-span-12 mb-4 border-0 border-b-2 border-blue">
                                    <h1 class="mb-1 text-xl font-semibold text-left text-blue">Cargo Details
                                    </h1>
                                </div>

                                <tr class="font-normal bg-white border-none">
                                    <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Quantity :
                                    </td>
                                    <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                        @php $qtycnt = 0; @endphp
                                        @foreach ($viewbookingconsigneesdetail as $v => $vcd)
                                            @php $qtycnt += $viewbookingconsigneesdetail[$v]['quantity']; @endphp
                                        @endforeach
                                        <table>
                                            <tr>
                                                <td>
                                                    {{ $qtycnt }}
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="font-normal bg-white border-none">
                                    <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Weight (Kg) :
                                    </td>
                                    <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                        @php $qtycnt2 = 0; @endphp
                                        @foreach ($viewbookingconsigneesdetail as $v => $vcd)
                                            @php $qtycnt2 += $viewbookingconsigneesdetail[$v]['weight']; @endphp
                                        @endforeach
                                        <table>
                                            <tr>
                                                <td>
                                                    {{ $qtycnt2 }}
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="font-normal bg-white ">
                                    <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Dimensions (LxWxH in cm) :
                                    </td>
                                    <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                        @foreach ($viewbookingconsigneesdetail as $v => $vcd)
                                            <table>
                                                <tr>
                                                    <td>
                                                        {{ $viewbookingconsigneesdetail[$v]['length'] }} X
                                                        {{ $viewbookingconsigneesdetail[$v]['width'] }} X
                                                        {{ $viewbookingconsigneesdetail[$v]['height'] }}
                                                    </td>
                                                </tr>
                                            </table>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr class="font-normal bg-white border-0">
                                    <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Declared Value :
                                    </td>
                                    <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                        {{ $CDdecval }}
                                    </td>
                                </tr>
                                <tr class="font-normal bg-white border-0">
                                    <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Transport Mode :
                                    </td>
                                    <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                        {{ $CDtransportmode }}
                                    </td>
                                </tr>
                                <tr class="font-normal bg-white border-0">
                                    <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Service Mode :
                                    </td>
                                    <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                        {{ $CDservmode }}
                                    </td>

                                </tr>
                                <tr class="font-normal bg-white border-0">
                                    <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Description of Goods :
                                    </td>
                                    <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                        {{ $CDdescgoods }}
                                    </td>
                                </tr>
                                <tr class="text-sm font-normal bg-white border-0">
                                    <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Mode of Payment :
                                    </td>
                                    <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                        {{ $CDmodeofp }}
                                    </td>
                                </tr>
                                <tr class="text-sm font-normal bg-white border-0">
                                    <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Assigned Team :
                                    </td>
                                    <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                        -
                                    </td>
                                </tr>
                                <tr class="text-sm font-normal bg-white border-0">
                                    <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Waybill Count :
                                    </td>
                                    <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                        {{ $CDwcount }}
                                    </td>
                                </tr>
                                <tr class="text-sm font-normal bg-white border-0">
                                    <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Failed Activity Reason :
                                    </td>
                                    <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                        -
                                    </td>
                                </tr>
                                {{-- </x-slot> --}}
                            </table>
                        </div>
                    </div>
                </form>
            </x-slot>
        </x-modal>
    @endif
    @if ($confirmsub_modal)
        <x-modal id="confirmsub_modal" size="3/4">
            <x-slot name="body">
                <form>
                    <div class="grid grid-cols-12 p-2">
                        <div class="col-span-12">
                            <span class="flex justify-center "><img class="w-24 h-24"
                                    src="
                /images/logo/viber_image_2023-09-15_14-06-35-034.png"
                                    alt="VeMoBro_logo"></span>

                            <div class="flex justify-center mt-4">
                                <h1 class="text-xl font-normal ">Booking has been successfully submitted!</h1>
                            </div>

                            <div class="flex justify-center">
                                <h1 class="text-xl font-normal ">Your Booking Reference Number is</h1>
                            </div>

                            <div class="flex justify-center mt-4">
                                <h1 class="text-2xl font-normal text-blue-800 underline">
                                    {{ $booking_reference_no_2 ?? '' }}</h1>
                            </div>

                            <div class="flex justify-center mt-8 space-x-10">
                                <div class="flex justify-start" style="">
                                    <button wire:click="actionc({}, 'ba_close')"
                                        class="px-12 py-2 text-base font-medium text-blue-800 bg-white border-2 rounded-md border-blue whitespace-nowrap hover:bg-gray-200">
                                        CLOSE
                                    </button>
                                </div>
                                <div class="flex justify-end">
                                    <button wire:click="actionba({}, 'book_again')"
                                        class="px-6 py-2 text-base font-medium text-white bg-blue-800 rounded-md whitespace-nowrap hover:bg-blue-900 "
                                        type="button">
                                        BOOK AGAIN
                                    </button>

                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </x-slot>
        </x-modal>
    @endif

    {{-- <x-modal id="book_again_modal" size="w-3/4">
            <x-slot name="body">
                @livewire('crm.sales.booking-mgmt.create')
                asdasd
            </x-slot>
        </x-modal> --}}

    <form autocomplete="off">
        <div x-cloak x-show="current_tab == 1">
            <div x-cloak x-show="current_tab == 1" class="grid grid-cols-12 gap-0 py-4 border rounded-lg shadow-lg">
                <div class="col-span-1 pl-6">
                    <svg wire:click="closecreatemodal" class="w-10 h-10 cursor-pointer text-blue" aria-hidden="true"
                        focusable="false" data-prefix="far" data-icon="edit" role="img"
                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                        <path fill="currentColor"
                            d="M9.4 233.4c-12.5 12.5-12.5 32.8 0 45.3l160 160c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L109.2 288 416 288c17.7 0 32-14.3 32-32s-14.3-32-32-32l-306.7 0L214.6 118.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0l-160 160z">
                        </path>
                    </svg>
                </div>
                <div class="col-span-11" style="margin-left: -3%;">
                    <h1 class="mt-1 text-2xl font-semibold text-left text-black ">Book Now
                    </h1>
                </div>
            </div>

            <div x-cloak x-show="current_tab == 1" class="grid grid-cols-12 gap-0 pt-2 pb-8 mt-4 border rounded-t-lg">

                <div class="col-span-2"></div>
                <div class="col-span-1"></div>
                <div class="col-span-6" style="display: flex;">
                    <div
                        class="step"style="color: white; background-color: #003399; display: block; border-radius: 25px; width: 50px; height: 50px; text-align: center; line-height: 50px;">
                        1</div>
                    <div class="connector"
                        style="margin-top:22px; flex-grow: 1; width: 10px; content:none ; display: block; height: 3px; background-color: #003399;">
                    </div>
                    <div class="step"
                        style="color: #8D8D8D; background-color: #00000029; display: block; border-radius: 25px; width: 50px; height: 50px; text-align: center; line-height: 50px;">
                        2</div>
                    <div class="connector"
                        style="margin-top:22px; flex-grow: 1; width: 10px; content:none ; display: block; height: 3px; background-color: #00000029;">
                    </div>
                    <div class="step"
                        style="color: #8D8D8D; background-color: #00000029; display: block; border-radius: 25px; width: 50px; height: 50px; text-align: center; line-height: 50px;">
                        3</div>
                    <div class="connector"
                        style="margin-top:22px; flex-grow: 1; width: 10px; content:none ; display: block; height: 3px; background-color: #00000029;">
                    </div>
                    <div class="step"
                        style="color: #8D8D8D; background-color: #00000029; display: block; border-radius: 25px; width: 50px; height: 50px; text-align: center; line-height: 50px;">
                        4</div>
                </div>
                <div class="col-span-1"></div>
                <div class="col-span-2"></div>

                <div class="col-span-2"></div>
                <div class="col-span-8 mt-2" style="display: flex;">
                    <div
                        class="step"style="margin-left:12%; color: #003399;  display: block; width:; height: 20px; text-align: center; line-height: 22px;">
                        General<br> Details</div>
                    <div class="connector"
                        style="margin-top:; flex-grow: 1; width: ; content:none ; display: block; height: 1px; background-color: white;">
                    </div>
                    <div class="step"
                        style="margin-left:2%; color: #8D8D8D; display: block; width: ; height: 20px; text-align: center; line-height: 22px;">
                        Shipper<br> Details</div>
                    <div class="connector"
                        style="margin-top:; flex-grow: 1; width: ; content:none ; display: block; height: 1px; background-color:white;">
                    </div>
                    <div class="step"
                        style="margin-left:2% ; color: #8D8D8D;  display: block; width: ; height: 20px; text-align: center; line-height: 22px;">
                        Consignee<br> Details</div>
                    <div class="connector"
                        style="margin-left:; flex-grow: 1; width: ; content:none ; display: block; height: 1px; background-color:white;">
                    </div>
                    <div class="step"
                        style="margin-right:11% ; color: #8D8D8D;  display: block; width: ; height: 20px; text-align: center; line-height: 22px;">
                        Booking<br> Summary</div>
                </div>
                <div class="col-span-1"></div>
            </div>

            <div x-cloak x-show="current_tab == 1"
                class="grid grid-cols-12 gap-12 border rounded-b-lg shadow-lg gap-y-0 px-14">

                <div class="col-span-6 pt-4 ml-6 ">

                    <div wire:init="BookingTypeReferenceBK">
                        <x-label for="booking_type_1" value="Booking Type" :required="true" />
                        <x-select class="w-full rounded-md h-11" style="cursor: pointer;" name="booking_type_1"
                            wire:model.defer='booking_type_1'>
                            <option value="">Select</option>
                            @foreach ($booking_type_referencesbk as $booking_type_reference)
                                <option value="{{ $booking_type_reference->id }}">
                                    {{ $booking_type_reference->name }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="booking_type_1" />
                    </div>
                </div>

                <div class="col-span-6 pt-4 mr-6">

                    <div wire:init="VehicleTypeReferenceBK">
                        <x-label for="vehicle_type_1" value="Vehicle Type" :required="true" />
                        <x-select class="w-full rounded-md h-11" style="cursor: pointer;" name="vehicle_type_1"
                            wire:model.defer='vehicle_type_1'>
                            <option value="">Select</option>
                            @foreach ($vehicle_type_referencesbk as $vehicle_type_reference)
                                <option value="{{ $vehicle_type_reference->id }}">
                                    {{ $vehicle_type_reference->name }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="vehicle_type_1" />
                    </div>
                </div>

                <div class="col-span-6 pt-4 ml-6 ">

                    <x-label for="pickup_date_1" value="Pick Up Date" :required="true" />
                    <x-input class="w-full rounded-md h-11" style="cursor: pointer;" type="date"
                        name="pickup_date_1" wire:model.defer='pickup_date_1'>
                    </x-input>
                    <x-input-error for="pickup_date_1" />
                </div>

                <div class="col-span-3 pt-4 mr-3">

                    <div wire:init="TimeslotReferenceBK">
                        <x-label for="time_slot_1" value="Time Slot" :required="true" />
                        <x-select class="w-full rounded-md h-11" style="cursor: pointer;" name="time_slot_1"
                            wire:model='time_slot_1'>
                            <option selected value="">Select</option>
                            @foreach ($timeslot_referencesbk as $timeslot_reference)
                                <option value="{{ $timeslot_reference->id }}">
                                    {{ $timeslot_reference->name }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="time_slot_1" />
                    </div>
                </div>

                @if ($time_slot_1 == 1)
                    <div class="col-span-3 pt-4 mr-6">
                        <x-label for="time_1" value="Time" :required="true" />
                        <x-select class="w-full rounded-md h-11" style="cursor: pointer;" name="time_1"
                            wire:model='time_1'>
                            <option value="">Select</option>
                            <option value="1">1:00 PM - 3:00 PM</option>
                            <option value="2">3:00 PM - 5:00 PM</option>
                            <option value="3">5:00 PM - 7:00 PM</option>
                            <option value="4">7:00 PM - 9:00 PM</option>
                            <option value="5">9:00 PM - 11:00 PM</option>
                        </x-select>
                        <x-input-error for="time_from_1" />
                    </div>
                    {{-- <div class="col-span-6 pl-6"> --}}
                    <x-input disabled hidden class="w-full rounded-md h-11" style="cursor: pointer;" type="text"
                        name="time_from_1" wire:model.defer='time_from_1'>
                    </x-input>
                    {{-- </div> --}}

                    {{-- <div class="col-span-6 mr-6"> --}}
                    <x-input disabled hidden class="w-full rounded-md h-11" style="cursor: pointer;" type="text"
                        name="time_to_1" wire:model.defer='time_to_1'>
                    </x-input>
                    {{-- </div> --}}
                @else
                    <div class="col-span-3 pt-4 mr-6">
                    </div>
                @endif

                @if ($consignee_category_1 == 1)
                    <div class="col-span-6 pt-4 pl-6 mt-6">
                        <div class="flex items-center">
                            <div wire:click="single_consignee_1"
                                class="flex items-center justify-between w-full text-base text-blue-800 border border-blue-800 cursor-pointer h-11"
                                style="border-top-left-radius: 25px; border-bottom-left-radius: 25px;">

                                <svg class="w-8 h-5 ml-4" aria-hidden="true" focusable="false" data-prefix="far"
                                    data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 448 512">
                                    <path fill="currentColor"
                                        d="M224 256A128 128 0 1 0 224 0a128 128 0 1 0 0 256zm-45.7 48C79.8 304 0 383.8 0 482.3C0 498.7 13.3 512 29.7 512H418.3c16.4 0 29.7-13.3 29.7-29.7C448 383.8 368.2 304 269.7 304H178.3z" />
                                </svg>

                                <span class=" whitespace-nowrap">Single Consignee</span>
                                <span class="" wire:model.defer='consignee_category_1'></span>

                            </div>

                            <div wire:click="multiple_consignee_1"
                                class="flex items-center justify-between w-full text-base text-gray-400 bg-white border border-gray-400 cursor-pointer h-11"
                                style="border-top-right-radius: 25px; border-bottom-right-radius: 25px;">
                                <svg class="w-8 h-5" aria-hidden="true" focusable="false" data-prefix="far"
                                    data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 448 512">
                                    <path fill="currentColor"
                                        d="M144 0a80 80 0 1 1 0 160A80 80 0 1 1 144 0zM512 0a80 80 0 1 1 0 160A80 80 0 1 1 512 0zM0 298.7C0 239.8 47.8 192 106.7 192h42.7c15.9 0 31 3.5 44.6 9.7c-1.3 7.2-1.9 14.7-1.9 22.3c0 38.2 16.8 72.5 43.3 96c-.2 0-.4 0-.7 0H21.3C9.6 320 0 310.4 0 298.7zM405.3 320c-.2 0-.4 0-.7 0c26.6-23.5 43.3-57.8 43.3-96c0-7.6-.7-15-1.9-22.3c13.6-6.3 28.7-9.7 44.6-9.7h42.7C592.2 192 640 239.8 640 298.7c0 11.8-9.6 21.3-21.3 21.3H405.3zM224 224a96 96 0 1 1 192 0 96 96 0 1 1 -192 0zM128 485.3C128 411.7 187.7 352 261.3 352H378.7C452.3 352 512 411.7 512 485.3c0 14.7-11.9 26.7-26.7 26.7H154.7c-14.7 0-26.7-11.9-26.7-26.7z" />
                                </svg>
                                <span class="pl-2 whitespace-nowrap" style="">Multiple Consignee</span>
                                <span class="" wire:model.defer='consignee_category_1'></span>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-span-6 pt-4 pl-6 mt-6">
                        <div class="flex items-center">
                            <div wire:click="single_consignee_1"
                                class="flex items-center justify-between w-full text-base text-gray-400 bg-white border border-gray-400 cursor-pointer h-11"
                                style="border-top-left-radius: 25px; border-bottom-left-radius: 25px;">

                                <svg class="w-8 h-5 ml-4" aria-hidden="true" focusable="false" data-prefix="far"
                                    data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 448 512">
                                    <path fill="currentColor"
                                        d="M224 256A128 128 0 1 0 224 0a128 128 0 1 0 0 256zm-45.7 48C79.8 304 0 383.8 0 482.3C0 498.7 13.3 512 29.7 512H418.3c16.4 0 29.7-13.3 29.7-29.7C448 383.8 368.2 304 269.7 304H178.3z" />
                                </svg>

                                <span class="whitespace-nowrap">Single Consignee</span>
                                <span class="" wire:model.defer='consignee_category_1'></span>

                            </div>

                            <div wire:click="multiple_consignee_1"
                                class="flex items-center justify-between w-full text-base text-blue-800 border border-blue-800 cursor-pointer h-11"
                                style="border-top-right-radius: 25px; border-bottom-right-radius: 25px;">
                                <svg class="w-8 h-5" aria-hidden="true" focusable="false" data-prefix="far"
                                    data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 448 512">
                                    <path fill="currentColor"
                                        d="M144 0a80 80 0 1 1 0 160A80 80 0 1 1 144 0zM512 0a80 80 0 1 1 0 160A80 80 0 1 1 512 0zM0 298.7C0 239.8 47.8 192 106.7 192h42.7c15.9 0 31 3.5 44.6 9.7c-1.3 7.2-1.9 14.7-1.9 22.3c0 38.2 16.8 72.5 43.3 96c-.2 0-.4 0-.7 0H21.3C9.6 320 0 310.4 0 298.7zM405.3 320c-.2 0-.4 0-.7 0c26.6-23.5 43.3-57.8 43.3-96c0-7.6-.7-15-1.9-22.3c13.6-6.3 28.7-9.7 44.6-9.7h42.7C592.2 192 640 239.8 640 298.7c0 11.8-9.6 21.3-21.3 21.3H405.3zM224 224a96 96 0 1 1 192 0 96 96 0 1 1 -192 0zM128 485.3C128 411.7 187.7 352 261.3 352H378.7C452.3 352 512 411.7 512 485.3c0 14.7-11.9 26.7-26.7 26.7H154.7c-14.7 0-26.7-11.9-26.7-26.7z" />
                                </svg>
                                <span class="pl-2 whitespace-nowrap" style="">Multiple Consignee</span>
                                <span class="" wire:model.defer='consignee_category_1'></span>
                            </div>
                        </div>
                    </div>
                @endif

                @if ($booking_category_1 == 1)
                    <div class="col-span-6 pt-4 pr-6 mt-6">
                        <div class="flex items-center">
                            <div wire:click="pickup_category_1"
                                class="flex items-center justify-between w-full text-base text-blue-800 border border-blue-800 cursor-pointer h-11"
                                style="border-top-left-radius: 25px; border-bottom-left-radius: 25px;">

                                <svg class="w-8 h-5 ml-4" aria-hidden="true" focusable="false" data-prefix="far"
                                    data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 448 512">
                                    <path fill="currentColor"
                                        d="M575.8 255.5c0 18-15 32.1-32 32.1h-32l.7 160.2c0 2.7-.2 5.4-.5 8.1V472c0 22.1-17.9 40-40 40H456c-1.1 0-2.2 0-3.3-.1c-1.4 .1-2.8 .1-4.2 .1H416 392c-22.1 0-40-17.9-40-40V448 384c0-17.7-14.3-32-32-32H256c-17.7 0-32 14.3-32 32v64 24c0 22.1-17.9 40-40 40H160 128.1c-1.5 0-3-.1-4.5-.2c-1.2 .1-2.4 .2-3.6 .2H104c-22.1 0-40-17.9-40-40V360c0-.9 0-1.9 .1-2.8V287.6H32c-18 0-32-14-32-32.1c0-9 3-17 10-24L266.4 8c7-7 15-8 22-8s15 2 21 7L564.8 231.5c8 7 12 15 11 24z" />
                                </svg>

                                <span class="whitespace-nowrap">Pick Up</span>
                                <span class="" wire:model.defer='booking_category_1'></span>

                            </div>

                            <div wire:click="walkin_category_1"
                                class="flex items-center justify-between w-full text-base text-gray-400 bg-white border border-gray-400 cursor-pointer h-11"
                                style="border-top-right-radius: 25px; border-bottom-right-radius: 25px;">
                                <svg class="w-8 h-5" aria-hidden="true" focusable="false" data-prefix="far"
                                    data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 448 512">
                                    <path fill="currentColor"
                                        d="M0 488V171.3c0-26.2 15.9-49.7 40.2-59.4L308.1 4.8c7.6-3.1 16.1-3.1 23.8 0L599.8 111.9c24.3 9.7 40.2 33.3 40.2 59.4V488c0 13.3-10.7 24-24 24H568c-13.3 0-24-10.7-24-24V224c0-17.7-14.3-32-32-32H128c-17.7 0-32 14.3-32 32V488c0 13.3-10.7 24-24 24H24c-13.3 0-24-10.7-24-24zm488 24l-336 0c-13.3 0-24-10.7-24-24V432H512l0 56c0 13.3-10.7 24-24 24zM128 400V336H512v64H128zm0-96V224H512l0 80H128z" />
                                </svg>
                                <span class="pr-2 whitespace-nowrap">Walk In</span>
                                <span class="" wire:model.defer='booking_category_1'></span>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-span-6 pt-4 pr-6 mt-6">
                        <div class="flex items-center">
                            <div wire:click="pickup_category_1"
                                class="flex items-center justify-between w-full text-base text-gray-400 bg-white border border-gray-400 cursor-pointer h-11"
                                style="border-top-left-radius: 25px; border-bottom-left-radius: 25px;">

                                <svg class="w-8 h-5 ml-4" aria-hidden="true" focusable="false" data-prefix="far"
                                    data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 448 512">
                                    <path fill="currentColor"
                                        d="M575.8 255.5c0 18-15 32.1-32 32.1h-32l.7 160.2c0 2.7-.2 5.4-.5 8.1V472c0 22.1-17.9 40-40 40H456c-1.1 0-2.2 0-3.3-.1c-1.4 .1-2.8 .1-4.2 .1H416 392c-22.1 0-40-17.9-40-40V448 384c0-17.7-14.3-32-32-32H256c-17.7 0-32 14.3-32 32v64 24c0 22.1-17.9 40-40 40H160 128.1c-1.5 0-3-.1-4.5-.2c-1.2 .1-2.4 .2-3.6 .2H104c-22.1 0-40-17.9-40-40V360c0-.9 0-1.9 .1-2.8V287.6H32c-18 0-32-14-32-32.1c0-9 3-17 10-24L266.4 8c7-7 15-8 22-8s15 2 21 7L564.8 231.5c8 7 12 15 11 24z" />
                                </svg>

                                <span class="whitespace-nowrap">Pick Up</span>
                                <span class="" wire:model.defer='booking_category_1'></span>

                            </div>

                            <div wire:click="walkin_category_1"
                                class="flex items-center justify-between w-full text-base text-blue-800 border border-blue-800 cursor-pointer h-11"
                                style="border-top-right-radius: 25px; border-bottom-right-radius: 25px;">
                                <svg class="w-8 h-5" aria-hidden="true" focusable="false" data-prefix="far"
                                    data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 448 512">
                                    <path fill="currentColor"
                                        d="M0 488V171.3c0-26.2 15.9-49.7 40.2-59.4L308.1 4.8c7.6-3.1 16.1-3.1 23.8 0L599.8 111.9c24.3 9.7 40.2 33.3 40.2 59.4V488c0 13.3-10.7 24-24 24H568c-13.3 0-24-10.7-24-24V224c0-17.7-14.3-32-32-32H128c-17.7 0-32 14.3-32 32V488c0 13.3-10.7 24-24 24H24c-13.3 0-24-10.7-24-24zm488 24l-336 0c-13.3 0-24-10.7-24-24V432H512l0 56c0 13.3-10.7 24-24 24zM128 400V336H512v64H128zm0-96V224H512l0 80H128z" />
                                </svg>
                                <span class="pr-2 whitespace-nowrap">Walk In</span>
                                <span class="" wire:model.defer='booking_category_1'></span>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="col-span-6 pt-4 ml-6">
                    @if ($consignee_category_1 == 2)
                        <x-label for="consignee_count_1" value="Total No. Consignees" :required="true" />
                        <x-input class="w-full rounded-md h-11" type="number" name="consignee_count_1"
                            wire:model.defer='consignee_count_1'>
                        </x-input>
                        <x-input-error for="consignee_count_1" />
                    @endif
                </div>

                <div class="col-span-6 pt-4 mr-6">
                    @if ($booking_category_1 == 2)
                        <div wire:init="WalkinReferenceBK">
                            <x-label for="walk_in_branch_1" value="Branch" :required="true" />
                            <x-select class="w-full rounded-md h-11" style="cursor: pointer;" name="walk_in_branch_1"
                                wire:model.defer='walk_in_branch_1'>
                                <option value="">Select</option>
                                @foreach ($walk_referencesbk as $walk_reference)
                                    <option value="{{ $walk_reference->id }}">
                                        {{ $walk_reference->code }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="walk_in_branch_1" />
                        </div>
                    @endif
                </div>


                <div class="col-span-6 pt-4 ml-6">
                    <div wire:init="ActivityReferenceBK">
                        <x-label for="activity_type_1" value="Activity Type" :required="true" />
                        <x-select class="w-full rounded-md h-11" style="cursor: pointer;" name="activity_type_1"
                            wire:model.defer='activity_type_1'>
                            <option value="">Select</option>
                            @foreach ($activity_referencesbk as $activity_reference)
                                <option value="{{ $activity_reference->id }}">
                                    {{ $activity_reference->activity_type }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="activity_type_1" />
                    </div>
                </div>
                <div class="col-span-6 pt-4 mr-6">
                    {{-- <div wire:init="MarketingChannelReferenceBK">
                        <x-label for="channel_1" value="Marketing Channel" :required="true" />
                        <x-select class="w-full rounded-md h-11" style="cursor: pointer;" name="channel_1"
                            wire:model='channel_1'>
                            <option value="">Select</option>
                            @foreach ($marketingchannel_referencesbk as $marketingchannel_reference)
                                <option value="{{ $marketingchannel_reference->id }}">
                                    {{ $marketingchannel_reference->name }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="channel_1" />
                    </div> --}}
                </div>
                <div class="col-span-6"></div>
                <div class="col-span-3"></div>
                <div class="col-span-3 pb-4">
                    <x-button type="button" wire:click="action({},'create_next')" title="Next"
                        class=" w-11/12 mt-6 bg-blue text-white hover:bg-[#002161]" />
                </div>
            </div>
        </div>

        {{-- ?///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}

        <div x-cloak x-show="current_tab == 2">

            <div x-cloak x-show="current_tab == 2" class="grid grid-cols-12 gap-0 py-4 border rounded-lg shadow-lg">
                <div class="col-span-1 pl-6">
                    <svg wire:click="closecreatemodal" class="w-10 h-10 cursor-pointer text-blue" aria-hidden="true"
                        focusable="false" data-prefix="far" data-icon="edit" role="img"
                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                        <path fill="currentColor"
                            d="M9.4 233.4c-12.5 12.5-12.5 32.8 0 45.3l160 160c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L109.2 288 416 288c17.7 0 32-14.3 32-32s-14.3-32-32-32l-306.7 0L214.6 118.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0l-160 160z">
                        </path>
                    </svg>
                </div>
                <div class="col-span-11" style="margin-left: -3%;">
                    <h1 class="mt-1 text-2xl font-semibold text-left text-black ">Book Now
                    </h1>
                </div>
            </div>

            <div x-cloak x-show="current_tab == 2" class="grid grid-cols-12 gap-0 pt-2 pb-8 mt-4 border rounded-t-lg">

                <div class="col-span-2"></div>
                <div class="col-span-1"></div>
                <div class="col-span-6" style="display: flex;">
                    <div
                        class="step"style=" display: block; border-radius: 25px; width: 50px; height: 50px; text-align: center; line-height: 50px;">
                        <svg style="border-radius: 25px; width: 50px; height: 50px; text-align: center; line-height: 50px;"
                            class=" text-blue" aria-hidden="true" focusable="false" data-prefix="fas"
                            data-icon="trash-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 448 512">
                            <path fill="currentColor"
                                d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z">
                            </path>
                        </svg>
                    </div>
                    <div class="connector"
                        style="margin-top:22px; flex-grow: 1; width: 10px; content:none ; display: block; height: 3px; background-color: #003399;">
                    </div>
                    <div
                        class="step"style="color: white; background-color: #003399; display: block; border-radius: 25px; width: 50px; height: 50px; text-align: center; line-height: 50px;">
                        2</div>
                    <div class="connector"
                        style="margin-top:22px; flex-grow: 1; width: 10px; content:none ; display: block; height: 3px; background-color: #003399;">
                    </div>
                    <div class="step"
                        style="color: #8D8D8D; background-color: #00000029; display: block; border-radius: 25px; width: 50px; height: 50px; text-align: center; line-height: 50px;">
                        3</div>
                    <div class="connector"
                        style="margin-top:22px; flex-grow: 1; width: 10px; content:none ; display: block; height: 3px; background-color: #00000029;">
                    </div>
                    <div class="step"
                        style="color: #8D8D8D; background-color: #00000029; display: block; border-radius: 25px; width: 50px; height: 50px; text-align: center; line-height: 50px;">
                        4</div>
                </div>
                <div class="col-span-1"></div>
                <div class="col-span-2"></div>

                <div class="col-span-2"></div>
                <div class="col-span-8 mt-2" style="display: flex;">
                    <div
                        class="step"style="margin-left:12%; color: #8D8D8D;  display: block; width:; height: 20px; text-align: center; line-height: 22px;">
                        General<br> Details</div>
                    <div class="connector"
                        style="margin-top:; flex-grow: 1; width: ; content:none ; display: block; height: 1px; background-color: white;">
                    </div>
                    <div class="step"
                        style="margin-left:2%; color: #003399; display: block; width: ; height: 20px; text-align: center; line-height: 22px;">
                        Shipper<br> Details</div>
                    <div class="connector"
                        style="margin-top:; flex-grow: 1; width: ; content:none ; display: block; height: 1px; background-color:white;">
                    </div>
                    <div class="step"
                        style="margin-left:2% ; color: #8D8D8D;  display: block; width: ; height: 20px; text-align: center; line-height: 22px;">
                        Consignee<br> Details</div>
                    <div class="connector"
                        style="margin-left:; flex-grow: 1; width: ; content:none ; display: block; height: 1px; background-color:white;">
                    </div>
                    <div class="step"
                        style="margin-right:11% ; color: #8D8D8D;  display: block; width: ; height: 20px; text-align: center; line-height: 22px;">
                        Booking<br> Summary</div>
                </div>
                <div class="col-span-1"></div>
            </div>

            <div x-cloak x-show="current_tab == 2"class="grid grid-cols-12 gap-0 border-t border-l border-r gap-y-0">
                <div class="col-span-12 mt-2">
                    <button type="button" wire:click="$set('current_tab', 1)"
                        class="p-2 px-3 mr-3 text-base text-blue">
                        <div class="flex items-start justify-between">
                            <svg class="w-4 h-6 font-bold" aria-hidden="true" focusable="false"
                                data-prefix="far" data-icon="print-alt" role="img"
                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                <path fill="currentColor"
                                    d="M9.4 233.4c-12.5 12.5-12.5 32.8 0 45.3l160 160c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L77.3 256 214.6 118.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0l-160 160z" />
                            </svg>
                            Back To General Details
                        </div>
                    </button>
                </div>
                <div class="col-span-12 py-1 mt-2 border-b-2 border-blue-800 ">
                    <span class="px-12 py-2 text-white rounded-tr-lg bg-blue"
                        style="border-top-right-radius: 100px">
                        Shipper Details
                    </span>
                </div>
            </div>

            <div x-cloak x-show="current_tab == 2"
                class="grid grid-cols-12 gap-12 border rounded-b-lg shadow-lg gap-y-0 px-14">

                @if ($account_type_2 == 1)
                    <div class="col-span-6 pt-4 pl-6">
                        <div class="flex items-center">
                            <div wire:click="individual_type_2"
                                class="flex items-center justify-between w-full text-base text-blue-800 border border-blue-800 cursor-pointer h-11"
                                style="border-top-left-radius: 25px; border-bottom-left-radius: 25px;">

                                <svg class="w-8 h-5 ml-4" aria-hidden="true" focusable="false" data-prefix="far"
                                    data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 448 512">
                                    <path fill="currentColor"
                                        d="M224 256A128 128 0 1 0 224 0a128 128 0 1 0 0 256zm-45.7 48C79.8 304 0 383.8 0 482.3C0 498.7 13.3 512 29.7 512H418.3c16.4 0 29.7-13.3 29.7-29.7C448 383.8 368.2 304 269.7 304H178.3z" />
                                </svg>

                                <span class=" whitespace-nowrap">Individual</span>
                                <span class="" wire:model.defer='account_type_2'></span>

                            </div>

                            <div wire:click="Corporate_type_2"
                                class="flex items-center justify-between w-full text-base text-gray-400 bg-white border border-gray-400 cursor-pointer h-11"
                                style="border-top-right-radius: 25px; border-bottom-right-radius: 25px;">
                                <svg class="w-8 h-5" aria-hidden="true" focusable="false" data-prefix="far"
                                    data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 448 512">
                                    <path fill="currentColor"
                                        d="M144 0a80 80 0 1 1 0 160A80 80 0 1 1 144 0zM512 0a80 80 0 1 1 0 160A80 80 0 1 1 512 0zM0 298.7C0 239.8 47.8 192 106.7 192h42.7c15.9 0 31 3.5 44.6 9.7c-1.3 7.2-1.9 14.7-1.9 22.3c0 38.2 16.8 72.5 43.3 96c-.2 0-.4 0-.7 0H21.3C9.6 320 0 310.4 0 298.7zM405.3 320c-.2 0-.4 0-.7 0c26.6-23.5 43.3-57.8 43.3-96c0-7.6-.7-15-1.9-22.3c13.6-6.3 28.7-9.7 44.6-9.7h42.7C592.2 192 640 239.8 640 298.7c0 11.8-9.6 21.3-21.3 21.3H405.3zM224 224a96 96 0 1 1 192 0 96 96 0 1 1 -192 0zM128 485.3C128 411.7 187.7 352 261.3 352H378.7C452.3 352 512 411.7 512 485.3c0 14.7-11.9 26.7-26.7 26.7H154.7c-14.7 0-26.7-11.9-26.7-26.7z" />
                                </svg>
                                <span class="pl-2 whitespace-nowrap" style="">Corporate</span>
                                <span class="" wire:model.defer='account_type_2'></span>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-span-6 pt-4 pl-6">
                        <div class="flex items-center">
                            <div wire:click="individual_type_2"
                                class="flex items-center justify-between w-full text-base text-gray-400 bg-white border border-gray-400 cursor-pointer h-11"
                                style="border-top-left-radius: 25px; border-bottom-left-radius: 25px;">

                                <svg class="w-8 h-5 ml-4" aria-hidden="true" focusable="false" data-prefix="far"
                                    data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 448 512">
                                    <path fill="currentColor"
                                        d="M224 256A128 128 0 1 0 224 0a128 128 0 1 0 0 256zm-45.7 48C79.8 304 0 383.8 0 482.3C0 498.7 13.3 512 29.7 512H418.3c16.4 0 29.7-13.3 29.7-29.7C448 383.8 368.2 304 269.7 304H178.3z" />
                                </svg>

                                <span class="whitespace-nowrap">Individual</span>
                                <span class="" wire:model.defer='account_type_2'></span>

                            </div>

                            <div wire:click="Corporate_type_2"
                                class="flex items-center justify-between w-full text-base text-blue-800 border border-blue-800 cursor-pointer h-11"
                                style="border-top-right-radius: 25px; border-bottom-right-radius: 25px;">
                                <svg class="w-8 h-5" aria-hidden="true" focusable="false" data-prefix="far"
                                    data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 448 512">
                                    <path fill="currentColor"
                                        d="M144 0a80 80 0 1 1 0 160A80 80 0 1 1 144 0zM512 0a80 80 0 1 1 0 160A80 80 0 1 1 512 0zM0 298.7C0 239.8 47.8 192 106.7 192h42.7c15.9 0 31 3.5 44.6 9.7c-1.3 7.2-1.9 14.7-1.9 22.3c0 38.2 16.8 72.5 43.3 96c-.2 0-.4 0-.7 0H21.3C9.6 320 0 310.4 0 298.7zM405.3 320c-.2 0-.4 0-.7 0c26.6-23.5 43.3-57.8 43.3-96c0-7.6-.7-15-1.9-22.3c13.6-6.3 28.7-9.7 44.6-9.7h42.7C592.2 192 640 239.8 640 298.7c0 11.8-9.6 21.3-21.3 21.3H405.3zM224 224a96 96 0 1 1 192 0 96 96 0 1 1 -192 0zM128 485.3C128 411.7 187.7 352 261.3 352H378.7C452.3 352 512 411.7 512 485.3c0 14.7-11.9 26.7-26.7 26.7H154.7c-14.7 0-26.7-11.9-26.7-26.7z" />
                                </svg>
                                <span class="pl-2 whitespace-nowrap" style="">Corporate</span>
                                <span class="" wire:model.defer='account_type_2'></span>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="col-span-6 pt-4 mr-6">
                    <x-label for="booking_reference_no_2" value="Booking Reference Number" />
                    <x-input disabled class="w-full rounded-md h-11" style="cursor: pointer;" type="text"
                        name="booking_reference_no_2" wire:model.defer='booking_reference_no_2'>
                    </x-input>
                    <x-input-error for="booking_reference_no_2" />
                </div>

                <div x-data="{ open: false }" class="relative col-span-6 mb-2 ml-6 rounded-md"
                    @click.away="open = false">
                    <div>
                        <x-label for="customer_no_2" value="Customer Number" :required="true" />
                        <x-input class="w-full rounded-md h-11" style="cursor: pointer;" type="text"
                            name="customer_no_2" wire:model='customer_no_2' @click="open = !open">
                        </x-input>
                        <x-input-error for="customer_no_2" />

                    </div>
                    <div x-show="open" x-cloak
                        class="absolute z-10 w-full p-2 overflow-hidden overflow-y-auto bg-gray-100 rounded shadow max-h-96"
                        style="">
                        <ul class="list-reset">
                            @foreach ($customer_nos as $i => $customer_no)
                                <li @click="open = !open" wire:key="{{ 'customer_no_2' . $i }}"
                                    wire:click="getCustomerDetails({{ $customer_no->id }})"
                                    class="px-2 text-black cursor-pointer hover:bg-gray-200">
                                    <p>
                                        {{ $customer_no->account_no }}
                                    </p>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>

                <div class="col-span-6">
                </div>

                @if ($account_type_2 == 2)
                    <div x-data="{ open: false }" class="relative col-span-6 pt-4 ml-6 rounded-md"
                        @click.away="open = false">
                        <div>
                            <x-label for="company_name_2" value="Company Name" :required="true" />
                            <x-input class="w-full rounded-md h-11" style="cursor: pointer;" type="text"
                                name="company_name_2" wire:model='company_name_2' @click="open = !open">
                            </x-input>
                            <x-input-error for="company_name_2" />
                        </div>
                        <div x-show="open" x-cloak
                            class="absolute z-10 w-full p-2 overflow-hidden overflow-y-auto bg-gray-100 rounded shadow max-h-96"
                            style="">
                            <ul class="list-reset">
                                @foreach ($company_names as $i => $company_name)
                                    <li @click="open = !open" wire:key="{{ 'company_name_2' . $i }}"
                                        wire:click="getCustomerCompanyName({{ $company_name->id }})"
                                        class="px-2 text-black cursor-pointer hover:bg-gray-200">
                                        <p>
                                            {{ $company_name->company_name }}
                                        </p>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                    <div class="col-span-6">
                    </div>
                @endif

                @if ($account_type_2 == 2)
                    <div class="col-span-12">
                        <h1 class="text-2xl font-semibold text-left text-blue">Contact Person</h1>
                    </div>
                @endif

                <div class="col-span-4 pt-4 ml-6">
                    <x-label for="first_name_2" value="First Name" :required="true" />
                    <x-input class="w-full rounded-md h-11" style="cursor: pointer;" type="text"
                        name="first_name_2" wire:model.defer='first_name_2'>
                    </x-input>
                    <x-input-error for="first_name_2" />
                </div>

                <div class="col-span-4 pt-4">
                    <x-label for="middle_name_2" value="Middle Name" />
                    <x-input class="w-full rounded-md h-11" style="cursor: pointer;" type="text"
                        name="middle_name_2" wire:model.defer='middle_name_2'>
                    </x-input>
                    <x-input-error for="middle_name_2" />
                </div>

                <div class="col-span-4 pt-4 mr-6">
                    <x-label for="last_name_2" value="Last Name" :required="true" />
                    <x-input class="w-full rounded-md h-11" style="cursor: pointer;" type="text"
                        name="last_name_2" wire:model.defer='last_name_2'>
                    </x-input>
                    <x-input-error for="last_name_2" />
                </div>

                <div class="col-span-6 pt-4 ml-6">
                    <x-label for="mobile_number_2" value="Contact Number" :required="true" />
                    <div class="relative">
                        <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                            <svg class="w-6 h-6 text-gray-400 cursor-pointer" aria-hidden="true" focusable="false"
                                data-prefix="far" data-icon="edit" role="img"
                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                <path fill="currentColor"
                                    d="M16 64C16 28.7 44.7 0 80 0H304c35.3 0 64 28.7 64 64V448c0 35.3-28.7 64-64 64H80c-35.3 0-64-28.7-64-64V64zM144 448c0 8.8 7.2 16 16 16h64c8.8 0 16-7.2 16-16s-7.2-16-16-16H160c-8.8 0-16 7.2-16 16zM304 64H80V384H304V64z">
                                </path>
                            </svg>
                        </div>
                        <x-input class="block w-full pl-10 rounded-md h-11" style="cursor: pointer;"
                            type="text" name="mobile_number_2" wire:model.defer='mobile_number_2'>
                        </x-input>
                    </div>

                    <x-input-error for="mobile_number_2" />
                </div>

                <div class="col-span-6 pt-4 mr-6">
                    <x-label for="email_address_2" value="Email Address" :required="true" />

                    <div class="relative">
                        <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                            <svg class="w-6 h-6 text-gray-400 cursor-pointer" aria-hidden="true" focusable="false"
                                data-prefix="far" data-icon="edit" role="img"
                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                <path fill="currentColor"
                                    d="M48 64C21.5 64 0 85.5 0 112c0 15.1 7.1 29.3 19.2 38.4L236.8 313.6c11.4 8.5 27 8.5 38.4 0L492.8 150.4c12.1-9.1 19.2-23.3 19.2-38.4c0-26.5-21.5-48-48-48H48zM0 176V384c0 35.3 28.7 64 64 64H448c35.3 0 64-28.7 64-64V176L294.4 339.2c-22.8 17.1-54 17.1-76.8 0L0 176z">
                                </path>
                            </svg>
                        </div>
                        <x-input class="block w-full pl-10 rounded-md h-11" style="cursor: pointer;"
                            type="text" name="email_address_2" wire:model.defer='email_address_2'>
                        </x-input>
                    </div>
                    <x-input-error for="email_address_2" />
                </div>

                <div class="col-span-12 pt-4 ml-6">
                    @if ($address_2 != null)
                        <div style="display: flex; justify-content: flex-end">
                            <button type="button" wire:click="actionchangeadd({}, 'change_addrs')"
                                class="p-2 px-3 text-sm text-white rounded-md bg-blue">
                                Change Address
                            </button>
                        </div>
                    @endif

                    <x-label for="address_2" value="Address" :required="true" />
                    <x-input disabled class="w-full rounded-md h-11" style="cursor: pointer;" type="text"
                        name="address_2" wire:model='address_2'>
                    </x-input>
                    <x-input-error for="address_2" />
                </div>

                <div hidden="hidden" class="col-span-3 pt-4 mr-6">
                    <x-label for="state2" value="State2" :required="true" />
                    <x-input class="w-full rounded-md h-11" style="cursor: pointer;" type="text"
                        name="state2" wire:model.defer='state2'>
                    </x-input>
                    <x-input-error for="state2" />
                </div>

                <div hidden="hidden" class="col-span-3 pt-4 mr-6">
                    <x-label for="city2" value="City2" :required="true" />
                    <x-input class="w-full rounded-md h-11" style="cursor: pointer;" type="text"
                        name="city2" wire:model.defer='city2'>
                    </x-input>
                    <x-input-error for="city2" />
                </div>

                <div hidden="hidden" class="col-span-3 pt-4 mr-6">
                    <x-label for="barangay2" value="Barangay2" :required="true" />
                    <x-input class="w-full rounded-md h-11" style="cursor: pointer;" type="text"
                        name="barangay2" wire:model.defer='barangay2'>
                    </x-input>
                    <x-input-error for="barangay2" />
                </div>

                <div hidden="hidden" class="col-span-3 pt-4 mr-6">
                    <x-label for="postal2" value="Postal2" :required="true" />
                    <x-input class="w-full rounded-md h-11" style="cursor: pointer;" type="text"
                        name="postal2" wire:model.defer='postal2'>
                    </x-input>
                    <x-input-error for="postal2" />
                </div>

                <div class="col-span-6"></div>
                <div class="col-span-3"></div>
                <div class="col-span-3 pb-4">
                    <x-button type="button" wire:click="action({},'create_next_2')" title="Next"
                        class="w-11/12 mt-6 ml-6 bg-blue text-white hover:bg-[#002161]" />
                </div>
            </div>
        </div>

        {{-- ?///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}

        <div x-cloak x-show="current_tab == 3">
            <div x-cloak x-show="current_tab == 3" class="grid grid-cols-12 gap-0 py-4 border rounded-lg shadow-lg">
                <div class="col-span-1 pl-6">
                    <svg wire:click="closecreatemodal" class="w-10 h-10 cursor-pointer text-blue"
                        aria-hidden="true" focusable="false" data-prefix="far" data-icon="edit" role="img"
                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                        <path fill="currentColor"
                            d="M9.4 233.4c-12.5 12.5-12.5 32.8 0 45.3l160 160c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L109.2 288 416 288c17.7 0 32-14.3 32-32s-14.3-32-32-32l-306.7 0L214.6 118.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0l-160 160z">
                        </path>
                    </svg>
                </div>
                <div class="col-span-11" style="margin-left: -3%;">
                    <h1 class="mt-1 text-2xl font-semibold text-left text-black ">Book Now
                    </h1>
                </div>
            </div>

            <div x-cloak x-show="current_tab == 3"
                class="grid grid-cols-12 gap-0 pt-2 pb-8 mt-4 border rounded-t-lg">

                <div class="col-span-2"></div>
                <div class="col-span-1"></div>
                <div class="col-span-6" style="display: flex;">
                    <div
                        class="step"style=" display: block; border-radius: 25px; width: 50px; height: 50px; text-align: center; line-height: 50px;">
                        <svg style="border-radius: 25px; width: 50px; height: 50px; text-align: center; line-height: 50px;"
                            class=" text-blue" aria-hidden="true" focusable="false" data-prefix="fas"
                            data-icon="trash-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 448 512">
                            <path fill="currentColor"
                                d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z">
                            </path>
                        </svg>
                    </div>
                    <div class="connector"
                        style="margin-top:22px; flex-grow: 1; width: 8px; content:none ; display: block; height: 3px; background-color: #003399;">
                    </div>
                    <div
                        class="step"style="display: block; border-radius: 25px; width: 50px; height: 50px; text-align: center; line-height: 50px;">
                        <svg style=" border-radius: 25px; width: 50px; height: 50px; text-align: center; line-height: 50px;"
                            class=" text-blue" aria-hidden="true" focusable="false" data-prefix="fas"
                            data-icon="trash-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 448 512">
                            <path fill="currentColor"
                                d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z">
                            </path>
                        </svg>
                    </div>
                    <div class="connector"
                        style="margin-top:22px; flex-grow: 1; width: 10px; content:none ; display: block; height: 3px; background-color: #003399;">
                    </div>
                    <div class="step"
                        style="color: white; background-color: #003399; display: block; border-radius: 25px; width: 50px; height: 50px; text-align: center; line-height: 50px;">
                        3</div>
                    <div class="connector"
                        style="margin-top:22px; flex-grow: 1; width: 10px; content:none ; display: block; height: 3px; background-color: #003399;">
                    </div>
                    <div class="step"
                        style="color: #8D8D8D; background-color: #00000029; display: block; border-radius: 25px; width: 50px; height: 50px; text-align: center; line-height: 50px;">
                        4</div>
                </div>
                <div class="col-span-1"></div>
                <div class="col-span-2"></div>

                <div class="col-span-2"></div>
                <div class="col-span-8 mt-2" style="display: flex;">
                    <div
                        class="step"style="margin-left:12%; color: #8D8D8D;  display: block; width:; height: 20px; text-align: center; line-height: 22px;">
                        General<br> Details</div>
                    <div class="connector"
                        style="margin-top:; flex-grow: 1; width: ; content:none ; display: block; height: 1px; background-color: white;">
                    </div>
                    <div class="step"
                        style="margin-left:2%; color: #8D8D8D; display: block; width: ; height: 20px; text-align: center; line-height: 22px;">
                        Shipper<br> Details</div>
                    <div class="connector"
                        style="margin-top:; flex-grow: 1; width: ; content:none ; display: block; height: 1px; background-color:white;">
                    </div>
                    <div class="step"
                        style="margin-left:2% ; color: #003399;  display: block; width: ; height: 20px; text-align: center; line-height: 22px;">
                        Consignee<br> Details</div>
                    <div class="connector"
                        style="margin-left:; flex-grow: 1; width: ; content:none ; display: block; height: 1px; background-color:white;">
                    </div>
                    <div class="step"
                        style="margin-right:11% ; color: #8D8D8D;  display: block; width: ; height: 20px; text-align: center; line-height: 22px;">
                        Booking<br> Summary</div>
                </div>
                <div class="col-span-1"></div>
            </div>

            <div x-cloak x-show="current_tab == 3"
                class="grid grid-cols-12 gap-0 border-t border-l border-r gap-y-0">
                <div class="col-span-12 mt-2">
                    <button type="button" wire:click="$set('current_tab', 2)"
                        class="p-2 px-3 mr-3 text-base text-blue">
                        <div class="flex items-start justify-between">
                            <svg class="w-4 h-6 font-bold" aria-hidden="true" focusable="false"
                                data-prefix="far" data-icon="print-alt" role="img"
                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                <path fill="currentColor"
                                    d="M9.4 233.4c-12.5 12.5-12.5 32.8 0 45.3l160 160c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L77.3 256 214.6 118.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0l-160 160z" />
                            </svg>
                            Back To Shipper Details
                        </div>
                    </button>
                </div>
            </div>

            <?php $k = 0; ?>
            <?php $i = 0; ?>
            @foreach ($cons as $a => $con)
                <div class="col-span-12 py-1 mt-4 border-b-2 border-blue-800 ">
                    <span class="px-12 py-2 text-white rounded-tr-lg bg-blue"
                        style="border-top-right-radius: 100px">
                        Consignee {{ $k + 1 }}
                    </span>
                </div>

                <div x-cloak x-show="current_tab == 3"
                    class="grid grid-cols-12 gap-12 border rounded-b-lg shadow-lg gap-y-0 px-14">

                    @if (isset($cons[$a][$a]['account_type_3']))
                        @if ($cons[$a][$a]['account_type_3'] == 1)
                            <div class="col-span-6 pt-4 pl-6 mt-6">
                                <div class="flex items-center">
                                    <div wire:click="individual_type_3({'a': {{ $a }}})"
                                        class="flex items-center justify-between w-full text-base text-blue-800 border border-blue-800 cursor-pointer h-11 "
                                        style="border-top-left-radius: 25px; border-bottom-left-radius: 25px;">

                                        <svg class="w-8 h-5 ml-4" aria-hidden="true" focusable="false"
                                            data-prefix="far" data-icon="print-alt" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                            <path fill="currentColor"
                                                d="M224 256A128 128 0 1 0 224 0a128 128 0 1 0 0 256zm-45.7 48C79.8 304 0 383.8 0 482.3C0 498.7 13.3 512 29.7 512H418.3c16.4 0 29.7-13.3 29.7-29.7C448 383.8 368.2 304 269.7 304H178.3z" />
                                        </svg>

                                        <span class=" whitespace-nowrap">Individual</span>
                                        <span class=""
                                            wire:model.defer='cons.{{ $a }}.{{ $a }}.account_type_3'></span>

                                    </div>
                                    <div wire:click="Corporate_type_3({'a': {{ $a }}})"
                                        class="flex items-center justify-between w-full text-base text-gray-400 bg-white border border-gray-400 cursor-pointer h-11 "
                                        style="border-top-right-radius: 25px; border-bottom-right-radius: 25px;">

                                        <svg class="w-8 h-5" aria-hidden="true" focusable="false"
                                            data-prefix="far" data-icon="print-alt" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                            <path fill="currentColor"
                                                d="M184 48H328c4.4 0 8 3.6 8 8V96H176V56c0-4.4 3.6-8 8-8zm-56 8V96H64C28.7 96 0 124.7 0 160v96H192 320 512V160c0-35.3-28.7-64-64-64H384V56c0-30.9-25.1-56-56-56H184c-30.9 0-56 25.1-56 56zM512 288H320v32c0 17.7-14.3 32-32 32H224c-17.7 0-32-14.3-32-32V288H0V416c0 35.3 28.7 64 64 64H448c35.3 0 64-28.7 64-64V288z" />
                                        </svg>

                                        <span class="pl-2 whitespace-nowrap">Corporate</span>
                                        <span class=""
                                            wire:model.defer='cons.{{ $a }}.{{ $a }}.account_type_3'></span>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="col-span-6 pt-4 pl-6 mt-6">
                                <div class="flex items-center">
                                    <div wire:click="individual_type_3({'a': {{ $a }}})"
                                        class="flex items-center justify-between w-full text-base text-gray-400 border border-gray-400 cursor-pointer h-11"
                                        style="border-top-left-radius: 25px; border-bottom-left-radius: 25px;">

                                        <svg class="w-8 h-5 ml-4" aria-hidden="true" focusable="false"
                                            data-prefix="far" data-icon="print-alt" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                            <path fill="currentColor"
                                                d="M224 256A128 128 0 1 0 224 0a128 128 0 1 0 0 256zm-45.7 48C79.8 304 0 383.8 0 482.3C0 498.7 13.3 512 29.7 512H418.3c16.4 0 29.7-13.3 29.7-29.7C448 383.8 368.2 304 269.7 304H178.3z" />
                                        </svg>

                                        <span class="whitespace-nowrap">Individual</span>
                                        <span class=""
                                            wire:model.defer='cons.{{ $a }}.{{ $a }}.account_type_3'></span>

                                    </div>
                                    <div wire:click="Corporate_type_3({'a': {{ $a }}})"
                                        class="flex items-center justify-between w-full text-base text-blue-800 bg-white border border-blue-800 cursor-pointer h-11"
                                        style="border-top-right-radius: 25px; border-bottom-right-radius: 25px;">

                                        <svg class="w-8 h-5" aria-hidden="true" focusable="false"
                                            data-prefix="far" data-icon="print-alt" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                            <path fill="currentColor"
                                                d="M184 48H328c4.4 0 8 3.6 8 8V96H176V56c0-4.4 3.6-8 8-8zm-56 8V96H64C28.7 96 0 124.7 0 160v96H192 320 512V160c0-35.3-28.7-64-64-64H384V56c0-30.9-25.1-56-56-56H184c-30.9 0-56 25.1-56 56zM512 288H320v32c0 17.7-14.3 32-32 32H224c-17.7 0-32-14.3-32-32V288H0V416c0 35.3 28.7 64 64 64H448c35.3 0 64-28.7 64-64V288z" />
                                        </svg>

                                        <span class="pl-2 whitespace-nowrap">Corporate</span>
                                        <span class=""
                                            wire:model.defer='cons.{{ $a }}.{{ $a }}.account_type_3'></span>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endif

                    <div class="col-span-6">
                    </div>

                    <div x-data="{ open: false }" class="relative col-span-6 pt-4 mb-2 ml-6 rounded-md"
                        @click.away="open = false">
                        <div>
                            <x-label for="cons.{{ $a }}.{{ $a }}.customer_no_3"
                                value="Customer Number" :required="true" />
                            <x-input class="w-full rounded-md h-11" style="cursor: pointer;" type="text"
                                name="cons.{{ $a }}.{{ $a }}.customer_no_3"
                                wire:model='cons.{{ $a }}.{{ $a }}.customer_no_3'
                                @click="open = !open">
                            </x-input>
                            <x-input-error for="cons.{{ $a }}.{{ $a }}.customer_no_3" />

                        </div>
                        <div x-show="open" x-cloak
                            class="absolute w-full p-2 overflow-hidden overflow-y-auto bg-gray-100 rounded shadow max-h-96"
                            style="">
                            <ul class="list-reset">
                                @foreach ($customer_noc as $i => $customer_no)
                                    <li @click="open = !open" wire:key="{{ 'customer_no_3' . $i }}"
                                        wire:click="getCustomerDetailsConsignee({'id': {{ $customer_no->id }},'a' : {{ $a }}})"
                                        class="px-2 text-black cursor-pointer hover:bg-gray-200">
                                        <p>
                                            {{ $customer_no->account_no }}
                                        </p>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                    <div class="col-span-6">
                    </div>
                    {{-- {{dd($cons[$a][$a]['account_type_3']);}} --}}
                    @if (isset($cons[$a][$a]['account_type_3']))
                        @if ($cons[$a][$a]['account_type_3'] == 2)
                            <div x-data="{ open: false }" class="relative col-span-6 ml-6 rounded-md"
                                @click.away="open = false">
                                <div class="">
                                    <x-label for="cons.{{ $a }}.{{ $a }}.company_name_3"
                                        value="Company Name" :required="true" />
                                    <x-input class="w-full rounded-md h-11" style="cursor: pointer;"
                                        type="text"
                                        name="cons.{{ $a }}.{{ $a }}.company_name_3"
                                        wire:model='cons.{{ $a }}.{{ $a }}.company_name_3'
                                        @click="open = !open">
                                    </x-input>
                                    <x-input-error
                                        for="cons.{{ $a }}.{{ $a }}.company_name_3" />

                                </div>
                                <div x-show="open" x-cloak
                                    class="absolute w-full p-2 overflow-hidden overflow-y-auto bg-gray-100 rounded shadow max-h-96"
                                    style="">
                                    <ul class="list-reset">
                                        @foreach ($company_namescons as $i => $company_namescon)
                                            <li @click="open = !open" wire:key="{{ 'company_name' . $i }}"
                                                wire:click="getCustomerDetailsConsigneeCampName({'id': {{ $company_namescon->id }},'a' : {{ $a }}})"
                                                class="px-2 text-black cursor-pointer hover:bg-gray-200">
                                                <p>
                                                    {{ $company_namescon->company_name }}
                                                </p>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>


                                <div class="col-span-6">
                                </div>
                        @endif
                    @endif

                    <div class="col-span-4 pt-4 ml-6">
                        <x-label for="first_name_3" value="First Name" :required="true" />
                        <x-input class="w-full rounded-md h-11" style="cursor: pointer;" type="text"
                            name="cons.{{ $a }}.{{ $a }}.first_name_3"
                            wire:model.defer='cons.{{ $a }}.{{ $a }}.first_name_3'>
                        </x-input>
                        <x-input-error for="cons.{{ $a }}.{{ $a }}.first_name_3" />
                    </div>

                    <div class="col-span-4 pt-4">
                        <x-label for="cons.{{ $a }}.{{ $a }}.middle_name_3"
                            value="Middle Name" />
                        <x-input class="w-full rounded-md h-11" style="cursor: pointer;" type="text"
                            name="cons.{{ $a }}.{{ $a }}.middle_name_3"
                            wire:model.defer='cons.{{ $a }}.{{ $a }}.middle_name_3'>
                        </x-input>
                        <x-input-error for="cons.{{ $a }}.{{ $a }}.middle_name_3" />
                    </div>

                    <div class="col-span-4 pt-4 mr-6">
                        <x-label for="cons.{{ $a }}.{{ $a }}.last_name_3"
                            value="Last Name" :required="true" />
                        <x-input class="w-full rounded-md h-11" style="cursor: pointer;" type="text"
                            name="cons.{{ $a }}.{{ $a }}.last_name_3"
                            wire:model.defer='cons.{{ $a }}.{{ $a }}.last_name_3'>
                        </x-input>
                        <x-input-error for="cons.{{ $a }}.{{ $a }}.last_name_3" />
                    </div>

                    <div class="col-span-6 pt-4 ml-6">
                        <x-label for="cons.{{ $a }}.{{ $a }}.mobile_number_3"
                            value="Contact Number" :required="true" />
                        <div class="relative">
                            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                <svg class="w-6 h-6 text-gray-400 cursor-pointer" aria-hidden="true"
                                    focusable="false" data-prefix="far" data-icon="edit" role="img"
                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                    <path fill="currentColor"
                                        d="M16 64C16 28.7 44.7 0 80 0H304c35.3 0 64 28.7 64 64V448c0 35.3-28.7 64-64 64H80c-35.3 0-64-28.7-64-64V64zM144 448c0 8.8 7.2 16 16 16h64c8.8 0 16-7.2 16-16s-7.2-16-16-16H160c-8.8 0-16 7.2-16 16zM304 64H80V384H304V64z">
                                    </path>
                                </svg>
                            </div>
                            <x-input class="block w-full pl-10 rounded-md h-11" style="cursor: pointer;"
                                type="text"
                                name="cons.{{ $a }}.{{ $a }}.mobile_number_3"
                                wire:model.defer='cons.{{ $a }}.{{ $a }}.mobile_number_3'>
                            </x-input>
                        </div>

                        <x-input-error for="cons.{{ $a }}.{{ $a }}.mobile_number_3" />
                    </div>

                    <div class="col-span-6 pt-4 mr-6">
                        <x-label for="cons.{{ $a }}.{{ $a }}.email_address_3"
                            value="Email Address" :required="true" />

                        <div class="relative">
                            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                <svg class="w-6 h-6 text-gray-400 cursor-pointer" aria-hidden="true"
                                    focusable="false" data-prefix="far" data-icon="edit" role="img"
                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                    <path fill="currentColor"
                                        d="M48 64C21.5 64 0 85.5 0 112c0 15.1 7.1 29.3 19.2 38.4L236.8 313.6c11.4 8.5 27 8.5 38.4 0L492.8 150.4c12.1-9.1 19.2-23.3 19.2-38.4c0-26.5-21.5-48-48-48H48zM0 176V384c0 35.3 28.7 64 64 64H448c35.3 0 64-28.7 64-64V176L294.4 339.2c-22.8 17.1-54 17.1-76.8 0L0 176z">
                                    </path>
                                </svg>
                            </div>
                            <x-input class="block w-full pl-10 rounded-md h-11" style="cursor: pointer;"
                                type="text"
                                name="cons.{{ $a }}.{{ $a }}.email_address_3"
                                wire:model.defer='cons.{{ $a }}.{{ $a }}.email_address_3'>
                            </x-input>
                        </div>
                        <x-input-error for="cons.{{ $a }}.{{ $a }}.email_address_3" />
                    </div>

                    <div class="col-span-12 pt-4 ml-6 mr-6">
                        <div style="display: flex; justify-content: flex-end">
                            <button type="button"
                                wire:click="actionchangeaddcons({'a': {{ $a }}}, 'change_addrs_cons')"
                                class="p-2 px-3 text-sm text-white rounded-md bg-blue">
                                Change Address
                            </button>
                        </div>

                        <x-label for="cons.{{ $a }}.{{ $a }}.address_3" value="Address"
                            :required="true" />
                        <x-input class="w-full rounded-md h-11" disabled style="cursor: pointer;" type="text"
                            name="cons.{{ $a }}.{{ $a }}.address_3"
                            wire:model='cons.{{ $a }}.{{ $a }}.address_3'>
                        </x-input>
                        <x-input-error for="cons.{{ $a }}.{{ $a }}.address_3" />
                    </div>

                    <div class="col-span-12 pt-4 ml-6 mr-6">

                        <div hidden="hidden" class="col-span-3 mr-6">
                            <x-label for="cons.{{ $a }}.{{ $a }}.state_3"
                                value="cons.{{ $a }}.{{ $a }}.State_3" :required="true" />
                            <x-input class="w-full rounded-md h-11" style="cursor: pointer;" type="text"
                                name="cons.{{ $a }}.{{ $a }}.state_3"
                                wire:model.defer='cons.{{ $a }}.{{ $a }}.state_3'>
                            </x-input>
                            <x-input-error for="state_3" />
                        </div>

                        <div hidden="hidden" class="col-span-3 pt-4 mr-6">
                            <x-label for="cons.{{ $a }}.{{ $a }}.city_3"
                                value="cons.{{ $a }}.{{ $a }}.City_3" :required="true" />
                            <x-input class="w-full rounded-md h-11" style="cursor: pointer;" type="text"
                                name="cons.{{ $a }}.{{ $a }}.city_3"
                                wire:model.defer='cons.{{ $a }}.{{ $a }}.city_3'>
                            </x-input>
                            <x-input-error for="city_3" />
                        </div>

                        <div hidden="hidden" class="col-span-3 pt-4 mr-6">
                            <x-label for="cons.{{ $a }}.{{ $a }}.barangay_3"
                                value="cons.{{ $a }}.{{ $a }}.Barangay_3"
                                :required="true" />
                            <x-input class="w-full rounded-md h-11" style="cursor: pointer;" type="text"
                                name="cons.{{ $a }}.{{ $a }}.barangay_3"
                                wire:model.defer='cons.{{ $a }}.{{ $a }}.barangay_3'>
                            </x-input>
                            <x-input-error for="barangay_3" />
                        </div>

                        <div hidden="hidden" class="col-span-3 pt-4 mr-6">
                            <x-label for="cons.{{ $a }}.{{ $a }}.postal_3"
                                value="cons.{{ $a }}.{{ $a }}.Postal_3"
                                :required="true" />
                            <x-input class="w-full rounded-md h-11" style="cursor: pointer;" type="text"
                                name="cons.{{ $a }}.{{ $a }}.postal_3"
                                wire:model.defer='cons.{{ $a }}.{{ $a }}.postal_3'>
                            </x-input>
                            <x-input-error for="postal_3" />
                        </div>
                    </div>

                    <div class="col-span-12">
                        <h1 class="text-2xl font-semibold text-left text-blue ">Cargo Details</h1>
                    </div>

                    <div class="col-span-12 pt-6 ml-6" style="margin-top:-2%;">
                        <x-label class="text-lg font-semibold" value="Service Type :" />
                    </div>
                    @if ($a == $a)
                        <div class="z-10 flex col-span-6 pt-10 pb-2 ml-6 space-x-8 text-base font-medium"
                            style="margin-top:-6%;">
                            <x-input-radio label="Shipper's Owned Packaging" class="cursor-pointer"
                                name="cons.{{ $a }}.{{ $a }}.stype_3" value="1"
                                wire:model="cons.{{ $a }}.{{ $a }}.stype_3" />
                            {{-- </div>
                    <div class="col-span-2 text-lg font-medium" style="margin-top:-28%;"> --}}
                            <x-input-radio label="Pouch" class="cursor-pointer"
                                name="cons.{{ $a }}.{{ $a }}.stype_3" value="2"
                                wire:model="cons.{{ $a }}.{{ $a }}.stype_3" />
                            {{-- </div>
                    <div class="col-span-2 mr-6 text-lg font-medium" style="margin-top:-28%;"> --}}
                            <x-input-radio label="Box" class="cursor-pointer"
                                name="cons.{{ $a }}.{{ $a }}.stype_3" value="3"
                                wire:model="cons.{{ $a }}.{{ $a }}.stype_3" />
                        </div>
                    @endif

                    <div class="col-span-6">
                    </div>
                    {{-- @dd($cons[$a][$a]['stype_3']) --}}
                    <div class="col-span-12 pt-10 ml-6" style="margin-top: -3%">
                        @if ($cons[$a][$a]['stype_3'] == 1)
                            <div class="grid grid-cols-12">
                                <?php $b = 0; ?>
                                <div class="w-auto col-span-2" style="margin-bottom:-9%;">
                                    <x-label for="quantity_3" value="Quantity" />
                                </div>
                                <div class="w-auto col-span-2" style="margin-left: -14%; margin-bottom:-9%;">
                                    <x-label for="Weight(Kg.)" value="Weight(Kg.)" />
                                </div>
                                <div class="w-auto col-span-6" style="margin-left: -10%; margin-bottom:-9%;">
                                    <x-label for="Dimension(Lx in cm.)" value="Dimension (LxWxH in cm.)" />
                                </div>
                                {{-- <div class="w-auto col-span-2">
                                    <x-label class="text-white" for="Dimension(Wx in cm.)"
                                        value="Dimension(Wx in cm.)" />
                                </div>
                                <div class="w-auto col-span-2">
                                    <x-label class="text-white" for="Dimension(H in cm.)"
                                        value="Dimension(H in cm.)" />
                                </div> --}}
                                <div class="col-span-1 text-white">

                                </div>
                                @if (isset($cardets[$a]))
                                    @foreach ($cardets[$a] as $c => $cardet)
                                        <div class="w-full col-span-2 pt-4">
                                            <x-input class="w-3/5 rounded-md h-11" style="cursor: pointer;"
                                                type="number"
                                                name="cardets.{{ $a }}.{{ $c }}.quantity_3"
                                                wire:model.defer='cardets.{{ $a }}.{{ $c }}.quantity_3'>
                                            </x-input>
                                            <x-input-error
                                                for="cardets.{{ $a }}.{{ $c }}.quantity_3" />
                                        </div>
                                        <div class="w-full col-span-2 pt-4" style="margin-left: -15%">
                                            <x-input class="w-3/5 rounded-md h-11" style="cursor: pointer;"
                                                type="number"
                                                name="cardets.{{ $a }}.{{ $c }}.weight_3"
                                                wire:model.defer='cardets.{{ $a }}.{{ $c }}.weight_3'>
                                            </x-input>
                                            <x-input-error
                                                for="cardets.{{ $a }}.{{ $c }}.weight_3" />
                                        </div>
                                        <div class="w-full col-span-2 pt-4" style="margin-left: -30%">
                                            <x-input class="w-3/5 rounded-md h-11" style="cursor: pointer;"
                                                type="number"
                                                name="cardets.{{ $a }}.{{ $c }}.length_3"
                                                wire:model.defer='cardets.{{ $a }}.{{ $c }}.length_3'>
                                            </x-input>
                                            <x-input-error
                                                for="cardets.{{ $a }}.{{ $c }}.length_3" />
                                        </div>
                                        <div class="w-full col-span-2 pt-4" style="margin-left: -65%">
                                            <x-input class="w-3/5 rounded-md h-11" style="cursor: pointer;"
                                                type="number"
                                                name="cardets.{{ $a }}.{{ $c }}.width_3"
                                                wire:model.defer='cardets.{{ $a }}.{{ $c }}.width_3'>
                                            </x-input>
                                            <x-input-error
                                                for="cardets.{{ $a }}.{{ $c }}.width_3" />
                                        </div>
                                        <div class="w-full col-span-2 pt-4" style="margin-left: -100%">
                                            <x-input class="w-3/5 rounded-md h-11" style="cursor: pointer;"
                                                type="number"
                                                name="cardets.{{ $a }}.{{ $c }}.height_3"
                                                wire:model.defer='cardets.{{ $a }}.{{ $c }}.height_3'>
                                            </x-input>
                                            <x-input-error
                                                for="cardets.{{ $a }}.{{ $c }}.height_3" />
                                        </div>
                                        {{-- @if (count($cardets[$a]) > 1) --}}
                                        <div class="col-span-1" style="margin-left: -260%; margin-top: 7%;">
                                            <x-label for="" value="" />
                                            <svg wire:click="removeConsigneeDetails({'a': {{ $a }},'b' : {{ $c }}})"
                                                class="h-6 mt-4 cursor-pointer w-7 text-red" aria-hidden="true"
                                                focusable="false" data-prefix="fas" data-icon="circle-minus"
                                                role="img" xmlns="http://www.w3.org/2000/svg"
                                                viewBox="0 0 448 512">
                                                <path fill="currentColor"
                                                    d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM184 232H328c13.3 0 24 10.7 24 24s-10.7 24-24 24H184c-13.3 0-24-10.7-24-24s10.7-24 24-24z">
                                                </path>
                                            </svg>
                                        </div>
                                        {{-- @endif --}}
                                    @endforeach
                                @endif
                                <div class="col-span-1" style="margin-left: -175%; margin-top: 7%;">
                                    <x-label for="" value="" />
                                    <svg wire:click="addConsigneeDetails({{ $a }})"
                                        style="margin-left:-50%"
                                        class="h-6 mt-4 cursor-pointer text-blue border-blue w-7" aria-hidden="true"
                                        focusable="false" data-prefix="fas" data-icon="trash-alt" role="img"
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                        <path fill="currentColor"
                                            d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z">
                                        </path>
                                    </svg>
                                </div>
                            </div>

                            <?php $b++; ?>
                        @elseif ($cons[$a][$a]['stype_3'] == 2)
                            <div class="grid grid-cols-12">
                                <?php $b = 0; ?>
                                <div class="w-auto col-span-2" style="margin-bottom:-9%;">
                                    <x-label for="quantity_3" value="Quantity" />
                                </div>
                                <div class="w-auto col-span-2" style="margin-left: -14%; margin-bottom:-9%;">
                                    <x-label for="size_3" value="Size" />
                                </div>
                                <div class="w-auto col-span-6" style="margin-left: -10%; margin-bottom:-9%;">
                                    {{-- <x-label for="Dimension(Lx in cm.)" value="Dimension (LxWxH in cm.)" /> --}}
                                </div>
                                {{-- <div class="w-auto col-span-2">
                                <x-label class="text-white" for="Dimension(Wx in cm.)"
                                    value="Dimension(Wx in cm.)" />
                            </div>
                            <div class="w-auto col-span-2">
                                <x-label class="text-white" for="Dimension(H in cm.)"
                                    value="Dimension(H in cm.)" />
                            </div> --}}
                                <div class="col-span-1 text-white">

                                </div>
                                @if (isset($cardets[$a]))
                                    @foreach ($cardets[$a] as $c => $cardet)
                                        <div class="w-full col-span-2 pt-4">
                                            <x-input class="w-3/5 rounded-md h-11" style="cursor: pointer;"
                                                type="number"
                                                name="cardets.{{ $a }}.{{ $c }}.quantity_3"
                                                wire:model.defer='cardets.{{ $a }}.{{ $c }}.quantity_3'>
                                            </x-input>
                                            <x-input-error
                                                for="cardets.{{ $a }}.{{ $c }}.quantity_3" />
                                        </div>
                                        <div class="w-full col-span-2 pt-4" style="margin-left: -15%">
                                            {{-- <x-input class="w-3/5 rounded-md h-11" style="cursor: pointer;"
                                                type="number"
                                                name="cardets.{{ $a }}.{{ $c }}.size_3"
                                                wire:model.defer='cardets.{{ $a }}.{{ $c }}.size_3'>
                                            </x-input>
                                            <x-input-error
                                                for="cardets.{{ $a }}.{{ $c }}.size_3" /> --}}
                                            <x-select class="w-3/4 rounded-md h-11" style="cursor: pointer;"
                                                name="cardets.{{ $a }}.{{ $c }}.size_3"
                                                wire:model='cardets.{{ $a }}.{{ $c }}.size_3'>
                                                <option value="">Select</option>
                                                <option value="1">Small</option>
                                                <option value="2">Medium</option>
                                                <option value="3">large</option>
                                            </x-select>
                                            <x-input-error
                                                for="cardets.{{ $a }}.{{ $c }}.size_3" />
                                        </div>

                                        <div class="w-full col-span-2 pt-4" style="margin-left:">

                                        </div>
                                        <div class="w-full col-span-2 pt-4" style="margin-left:">

                                        </div>
                                        <div class="w-full col-span-2 pt-4" style="margin-left:">

                                        </div>
                                        {{-- @if (count($cardets[$a]) > 1) --}}
                                        <div class="col-span-1" style="margin-left: -660%; margin-top: 7%;">
                                            <x-label for="" value="" />
                                            <svg wire:click="removeConsigneeDetails({'a': {{ $a }},'b' : {{ $c }}})"
                                                class="h-6 mt-4 cursor-pointer w-7 text-red" aria-hidden="true"
                                                focusable="false" data-prefix="fas" data-icon="circle-minus"
                                                role="img" xmlns="http://www.w3.org/2000/svg"
                                                viewBox="0 0 448 512">
                                                <path fill="currentColor"
                                                    d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM184 232H328c13.3 0 24 10.7 24 24s-10.7 24-24 24H184c-13.3 0-24-10.7-24-24s10.7-24 24-24z">
                                                </path>
                                            </svg>
                                        </div>
                                        {{-- @endif --}}
                                    @endforeach
                                @endif
                                <div class="col-span-1" style="margin-left: -440%; margin-top: 7%;">
                                    <x-label for="" value="" />
                                    <svg wire:click="addConsigneeDetails({{ $a }})"
                                        style="margin-left:-50%"
                                        class="h-6 mt-4 cursor-pointer text-blue border-blue w-7" aria-hidden="true"
                                        focusable="false" data-prefix="fas" data-icon="trash-alt" role="img"
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                        <path fill="currentColor"
                                            d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z">
                                        </path>
                                    </svg>
                                </div>
                            </div>
                            <?php $b++; ?>
                        @elseif ($cons[$a][$a]['stype_3'] == 3)
                            <div class="grid grid-cols-12">
                                <?php $b = 0; ?>
                                <div class="w-auto col-span-2" style="margin-bottom:-9%;">
                                    <x-label for="quantity_3" value="Quantity" />
                                </div>
                                <div class="w-auto col-span-2" style="margin-left: -14%; margin-bottom:-9%;">
                                    <x-label for="size_3" value="Size" />
                                </div>
                                <div class="w-auto col-span-6" style="margin-left: -10%; margin-bottom:-9%;">
                                    {{-- <x-label for="Dimension(Lx in cm.)" value="Dimension (LxWxH in cm.)" /> --}}
                                </div>
                                {{-- <div class="w-auto col-span-2">
                                <x-label class="text-white" for="Dimension(Wx in cm.)"
                                    value="Dimension(Wx in cm.)" />
                            </div>
                            <div class="w-auto col-span-2">
                                <x-label class="text-white" for="Dimension(H in cm.)"
                                    value="Dimension(H in cm.)" />
                            </div> --}}
                                <div class="col-span-1 text-white">

                                </div>
                                @if (isset($cardets[$a]))
                                    @foreach ($cardets[$a] as $c => $cardet)
                                        <div class="w-full col-span-2 pt-4">
                                            <x-input class="w-3/5 rounded-md h-11" style="cursor: pointer;"
                                                type="number"
                                                name="cardets.{{ $a }}.{{ $c }}.quantity_3"
                                                wire:model.defer='cardets.{{ $a }}.{{ $c }}.quantity_3'>
                                            </x-input>
                                            <x-input-error
                                                for="cardets.{{ $a }}.{{ $c }}.quantity_3" />
                                        </div>
                                        <div class="w-full col-span-2 pt-4" style="margin-left: -15%">
                                            {{-- <x-input class="w-3/5 rounded-md h-11" style="cursor: pointer;"
                                                type="number"
                                                name="cardets.{{ $a }}.{{ $c }}.size_3"
                                                wire:model.defer='cardets.{{ $a }}.{{ $c }}.size_3'>
                                            </x-input>
                                            <x-input-error
                                                for="cardets.{{ $a }}.{{ $c }}.size_3" /> --}}
                                            <x-select class="w-3/4 rounded-md h-11" style="cursor: pointer;"
                                                name="cardets.{{ $a }}.{{ $c }}.size_3"
                                                wire:model='cardets.{{ $a }}.{{ $c }}.size_3'>
                                                <option class="text-sm" value="">Select</option>
                                                <option value="1">Small</option>
                                                <option value="2">Medium</option>
                                                <option value="3">large</option>
                                            </x-select>
                                            <x-input-error
                                                for="cardets.{{ $a }}.{{ $c }}.size_3" />
                                        </div>
                                        <div class="w-full col-span-2 pt-4" style="margin-left:">

                                        </div>
                                        <div class="w-full col-span-2 pt-4" style="margin-left:">

                                        </div>
                                        <div class="w-full col-span-2 pt-4" style="margin-left:">

                                        </div>
                                        {{-- @if (count($cardets[$a]) > 1) --}}
                                        <div class="col-span-1" style="margin-left: -660%; margin-top: 7%;">
                                            <x-label for="" value="" />
                                            <svg wire:click="removeConsigneeDetails({'a': {{ $a }},'b' : {{ $c }}})"
                                                class="h-6 mt-4 cursor-pointer w-7 text-red" aria-hidden="true"
                                                focusable="false" data-prefix="fas" data-icon="circle-minus"
                                                role="img" xmlns="http://www.w3.org/2000/svg"
                                                viewBox="0 0 448 512">
                                                <path fill="currentColor"
                                                    d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM184 232H328c13.3 0 24 10.7 24 24s-10.7 24-24 24H184c-13.3 0-24-10.7-24-24s10.7-24 24-24z">
                                                </path>
                                            </svg>
                                        </div>
                                        {{-- @endif --}}
                                    @endforeach
                                @endif
                                <div class="col-span-1" style="margin-left: -440%; margin-top: 7%;">
                                    <x-label for="" value="" />
                                    <svg wire:click="addConsigneeDetails({{ $a }})"
                                        style="margin-left:-50%"
                                        class="h-6 mt-4 cursor-pointer text-blue border-blue w-7" aria-hidden="true"
                                        focusable="false" data-prefix="fas" data-icon="trash-alt" role="img"
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                        <path fill="currentColor"
                                            d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z">
                                        </path>
                                    </svg>
                                </div>
                            </div>
                            <?php $b++; ?>
                        @endif


                    </div>

                    <div class="col-span-6 pt-4 ml-6">
                        <x-label for="cons.{{ $a }}.{{ $a }}.declared_value_3"
                            value="Declared Value" :required="true" />
                        <x-input class="w-full rounded-md h-11" style="cursor: pointer;" type="text"
                            name="cons.{{ $a }}.{{ $a }}.declared_value_3"
                            wire:model.defer='cons.{{ $a }}.{{ $a }}.declared_value_3'
                            x-data="{ imask: null }" x-init="imask = new IMask($refs.NumberInput, {
                                mask: Number,
                                scale: 2,
                                signed: false,
                                thousandsSeparator: ',',
                                radix: '.',
                                normalizeZeros: false
                            });" x-ref="NumberInput">
                            {{-- @input="$set('declared_value_3', number_format(event.target.value, 0, '', ''))"> --}}
                            {{-- @input="$set('cons.{{ $a }}.{{ $a }}.declared_value_3', number_format(event.target.value, 0, '', ''))"> --}}
                            {{-- id="number_element" required pattern="[0-9\.]+"> --}}
                        </x-input>
                        <x-input-error for="cons.{{ $a }}.{{ $a }}.declared_value_3" />
                    </div>

                    <div class="col-span-6 pt-4 mr-6">
                        <x-label for="cons.{{ $a }}.{{ $a }}.description_goods_3"
                            value="Description of Goods" :required="true" />
                        <x-input class="w-full rounded-md h-11" style="cursor: pointer;" type="text"
                            name="cons.{{ $a }}.{{ $a }}.description_goods_3"
                            wire:model.defer='cons.{{ $a }}.{{ $a }}.description_goods_3'>
                        </x-input>
                        <x-input-error for="cons.{{ $a }}.{{ $a }}.description_goods_3" />
                    </div>


                    <div class="col-span-6 pt-4 ml-6">
                        <div wire:init="TransportReferenceCD">
                            <x-label for="cons.{{ $a }}.{{ $a }}.transposrt_mode_3"
                                value="Transport Mode" :required="true" />
                            <x-select class="w-full rounded-md h-11" style="cursor: pointer;"
                                name="cons.{{ $a }}.{{ $a }}.transposrt_mode_3"
                                wire:model.defer='cons.{{ $a }}.{{ $a }}.transposrt_mode_3'
                                wire:change="listServiceMode({{ $a }})">
                                <option value="">Select</option>
                                @foreach ($transport_referencescd as $transport_reference)
                                    <option value="{{ $transport_reference->id }}">
                                        {{ $transport_reference->name }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="cons.{{ $a }}.{{ $a }}.transposrt_mode_3" />
                        </div>
                    </div>

                    <div class="col-span-6 pt-4 mr-6">
                        <div>
                            @if ($booking_category_1 == 1)
                                <x-label for="cons.{{ $a }}.{{ $a }}.service_mode_3"
                                    value="Service Mode" :required="true" />
                                <x-select class="w-full rounded-md h-11" style="cursor: pointer;"
                                    name="cons.{{ $a }}.{{ $a }}.service_mode_3"
                                    wire:model.defer='cons.{{ $a }}.{{ $a }}.service_mode_3'>
                                    <option value="">Select</option>
                                    @if ($cons[$a][$a]['servmode_identify'] == 1)
                                        <option value="3"> Door - Airport </option>
                                        <option value="4"> Door - Door </option>
                                    @else
                                        <option value="7"> Door - Port </option>
                                        <option value="8"> Door - Door </option>
                                    @endif
                                </x-select>
                            @else
                                <x-label for="cons.{{ $a }}.{{ $a }}.service_mode_3"
                                    value="Service Mode" :required="true" />
                                <x-select class="w-full rounded-md h-11" style="cursor: pointer;"
                                    name="cons.{{ $a }}.{{ $a }}.service_mode_3"
                                    wire:model.defer='cons.{{ $a }}.{{ $a }}.service_mode_3'>
                                    <option value="">Select</option>
                                    @if ($cons[$a][$a]['servmode_identify'] == 1)
                                        <option value="1"> Airport - Airport </option>
                                        <option value="2"> Airport - Door </option>
                                    @else
                                        <option value="5"> Port - Port </option>
                                        <option value="6"> Port - Door </option>
                                    @endif
                                </x-select>
                            @endif


                            <x-input-error for="cons.{{ $a }}.{{ $a }}.service_mode_3" />
                        </div>
                    </div>

                    @if ($cons[$a][$a]['account_type_3'] == 1)
                        <div class="col-span-6 pt-4 ml-6">
                            <div wire:init="ModeOfPaymenReferenceCD">
                                <x-label for="cons.{{ $a }}.{{ $a }}.mode_of_payment_3"
                                    value="Mode Of Payment" :required="true" />
                                <x-select class="w-full rounded-md h-11" style="cursor: pointer;"
                                    name="cons.{{ $a }}.{{ $a }}.mode_of_payment_3"
                                    wire:model='cons.{{ $a }}.{{ $a }}.mode_of_payment_3'>
                                    <option value="">Select</option>
                                    @foreach ($modepayment_referencescd as $modepayment_reference)
                                        <option value="{{ $modepayment_reference->id }}">
                                            {{ $modepayment_reference->name }}
                                        </option>
                                    @endforeach
                                </x-select>
                                <x-input-error
                                    for="cons.{{ $a }}.{{ $a }}.mode_of_payment_3" />
                            </div>
                        </div>

                        <div class="col-span-6 pt-4 mr-6">
                            <x-label for="cons.{{ $a }}.{{ $a }}.charge_to_name_3"
                                value="Charge to" :required="true" />
                            <x-input class="w-full rounded-md h-11" style="cursor: pointer;" type="text"
                                name="cons.{{ $a }}.{{ $a }}.charge_to_name_3"
                                wire:model.defer='cons.{{ $a }}.{{ $a }}.charge_to_name_3'>
                            </x-input>
                            <x-input-error for="cons.{{ $a }}.{{ $a }}.charge_to_name_3" />
                        </div>
                    @else
                        <div class="col-span-6 pt-4 ml-6">
                            <div wire:init="ModeOfPaymenReferenceCD">
                                <x-label for="cons.{{ $a }}.{{ $a }}.mode_of_payment_3"
                                    value="Mode Of Payment" :required="true" />
                                <x-select class="w-full rounded-md h-11" style="cursor: pointer;"
                                    name="cons.{{ $a }}.{{ $a }}.mode_of_payment_3"
                                    wire:model='cons.{{ $a }}.{{ $a }}.mode_of_payment_3'>
                                    <option value="">Select</option>
                                    @foreach ($modepayment_referencescdcorps as $modepayment_referencecorp)
                                        <option value="{{ $modepayment_referencecorp->id }}">
                                            {{ $modepayment_referencecorp->name }}
                                        </option>
                                    @endforeach
                                </x-select>
                                <x-input-error
                                    for="cons.{{ $a }}.{{ $a }}.mode_of_payment_3" />
                            </div>
                        </div>

                        <div class="col-span-6 pt-4 mr-6">
                            <div wire:init="BookingChargeTo">
                                <x-label for="cons.{{ $a }}.{{ $a }}.charge_to_3"
                                    value="Charge To" :required="true" />
                                <x-select class="w-full rounded-md h-11" style="cursor: pointer;"
                                    name="cons.{{ $a }}.{{ $a }}.charge_to_3"
                                    wire:model='cons.{{ $a }}.{{ $a }}.charge_to_3'>
                                    <option value="">Select</option>
                                    @foreach ($chargedto_references as $chargedto_reference)
                                        <option value="{{ $chargedto_reference->id }}">
                                            {{ $chargedto_reference->company_name }}
                                        </option>
                                    @endforeach
                                </x-select>
                                <x-input-error for="cons.{{ $a }}.{{ $a }}.charge_to_3" />
                            </div>
                        </div>
                    @endif

                    <div class="col-span-6">
                    </div>



                    {{-- <button type="button" title="Remove"
                    wire:click="removeOtherServicesMeasurement({{ $i }})"
                    class="w-6 h-6 px-1 pr-0.5 pt-0.5 text-md flex-none bg-red-600 text-white rounded-full">
                    -</button>
            @endif
            @if (count($other_services_measurements) == ($i += 1))
                <button type="button" title=""
                    wire:click="addOtherServicesMeasurement"
                    class="w-6 h-6 px-1 pr-0.5 text-md flex-none bg-[#003399] text-white rounded-full">
                    +
                </button> --}}

                    @if (count($cons) > 1)
                        <div class="col-span-3 pt-4 pb-4">
                            <button type="button" wire:click="removeConsigneeCreate({'a': {{ $a }}})"
                                class="flex-none w-full py-2 text-sm font-medium bg-white border-2 border-red-500 rounded-md text-red"><span
                                    class="flex-none w-2 h-2 px-1.5 py-0 text-xs font-extrabold border-2 border-red-500 rounded-full bg-white-600 text-red">-</span>
                                <span class="ml-2">Delete Consignee</span>
                            </button>

                        </div>
                    @endif

                    @if (count($cons) == 1)
                        <div class="col-span-3"></div>
                        <div class="col-span-3 pt-4 pb-4 mr-6">
                            <button type="button" wire:click="addConsignee({{ $a + 1 }})"
                                class=" w-full py-2 text-sm flex-none bg-[#003399] text-white rounded-md"><span
                                    class="flex-none w-2 h-2 px-1.5 py-0 text-sm font-semibold border-2 border-white-500 rounded-full bg-white-600 text-white">+</span>
                                <span class="ml-2">Add Consignee</span>
                            </button>
                        </div>
                    @endif

                    @if ($k != 0)
                        @if (count($cons) == $k + 1)
                            <div class="col-span-3 pt-4 pb-4 mr-6">
                                <button type="button" wire:click="addConsignee({{ $a + 1 }})"
                                    class=" w-full py-2 text-sm flex-none bg-[#003399] text-white rounded-md border-2 border-blue"><span
                                        class="flex-none w-2 h-2 px-1.5 py-0 text-sm font-semibold border-2 border-white-500 rounded-full bg-white-600 text-white">+</span>
                                    <span class="ml-2">Add Consignee</span>
                                </button>
                            </div>
                        @endif
                    @endif


                </div>
                <?php $i++; ?>
                <?php $k++; ?>
            @endforeach
            <div x-cloak x-show="current_tab == 3"
                class="grid grid-cols-12 gap-12 px-12 py-8 mt-6 border rounded-lg shadow-lg ">

                <div class="col-span-6 ml-6">
                    <x-label for="work_instruction_3" value="Work Instruction" />
                    <x-input class="w-full rounded-md h-11" style="cursor: pointer;" type="text"
                        name="work_instruction_3" wire:model.defer='work_instruction_3'>
                    </x-input>
                    <x-input-error for="work_instruction_3" />
                    <div class="pt-4">
                        <x-label for="remarks_3" value="Internal Remarks" />
                        <x-input class="w-full rounded-md h-11" style="cursor: pointer;" type="text"
                            name="remarks_3" wire:model.defer='remarks_3'>
                        </x-input>
                        <x-input-error for="remarks_3" />
                    </div>
                </div>

                <div class="col-span-6 mr-6">
                    <x-label for="attachment" value="Attachment" />
                    <div class="grid gap-2">
                        <div class="flex flex-col space-y-3">
                            <x-table.table class="overflow-hidden">
                                <x-slot name="thead">
                                </x-slot>
                                <x-slot name="tbody">
                                    @forelse ($attachments as $a => $attachment)
                                        <tr
                                            class="text-sm border-0 cursor-pointer even:bg-white hover:text-blue hover:bg-blue-200">
                                            <td class="flex p-2 items-left justify-left whitespace-nowrap">
                                                <div class="flex-shrink-0 mb-1 mr-1 whitespace-nowrap">
                                                    {{-- <div class="relative z-0 ">
                                                        <div class="absolute top-0 left-0" hidden>
                                                            @if (!$attachments[$a]['attachment'])
                                                                <img class="object-contain w-20 h-20 mx-auto border border-gray-500 rounded-lg "
                                                                    src="{{ $attachments[$a]['attachment'] ? $attachments[$a]['attachment']->temporaryUrl() : asset('images/form/add-image.png') }}">
                                                            @elseif (in_array($attachments[$a]['attachment']->extension(), config('filesystems.image_type')))
                                                                <img class="object-contain w-20 h-20 mx-auto border border-gray-500 rounded-lg "
                                                                    src="{{ $attachments[$a]['attachment'] ? $attachments[$a]['attachment']->temporaryUrl() : asset('images/form/add-image.png') }}">
                                                            @else
                                                                <svg class="object-contain w-20 h-20 mx-auto border border-gray-500 rounded-lg"
                                                                    aria-hidden="false" focusable="true"
                                                                    data-prefix="fas" data-icon="file-alt"
                                                                    role="img"
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    viewBox="0 0 384 512">
                                                                    <path fill="currentColor"
                                                                        d="M224 136V0H24C10.7 0 0 10.7 0 24v464c0 13.3 10.7 24 24 24h336c13.3 0 24-10.7 24-24V160H248c-13.2 0-24-10.8-24-24zm64 236c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-64c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-72v8c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12zm96-114.1v6.1H256V0h6.1c6.4 0 12.5 2.5 17 7l97.9 98c4.5 4.5 7 10.6 7 16.9z">
                                                                    </path>
                                                                </svg>
                                                            @endif
                                                        </div>
                                                        <input type="file"
                                                            name="attachments.{{ $a }}.attachment"
                                                            wire:model="attachments.{{ $a }}.attachment"
                                                            class="relative z-50 block text-sm cursor-pointer ">
                                                        <x-input-error
                                                            for="attachments.{{ $a }}.attachment" />
                                                    </div> --}}

                                                    <div class="relative z-0">
                                                        <input type="file"
                                                            name="attachments.{{ $a }}.attachment"
                                                            wire:model="attachments.{{ $a }}.attachment"
                                                            class="absolute top-0 left-0 z-50 opacity-0">

                                                        @if ($attachments[$a]['attachment'])
                                                            <label for="attachments.{{ $a }}.attachment"
                                                                :required="true"
                                                                class="relative z-30 block px-2 py-1 text-xs bg-gray-200 border border-gray-500 cursor-pointer">
                                                                <span class="flex gap-2"><svg class="w-3 h-3"
                                                                        aria-hidden="true" focusable="false"
                                                                        data-prefix="fas" data-icon="file-alt"
                                                                        role="img"
                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                        viewBox="0 0 384 512">
                                                                        <path fill="currentColor"
                                                                            d="M364.2 83.8c-24.4-24.4-64-24.4-88.4 0l-184 184c-42.1 42.1-42.1 110.3 0 152.4s110.3 42.1 152.4 0l152-152c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-152 152c-64 64-167.6 64-231.6 0s-64-167.6 0-231.6l184-184c46.3-46.3 121.3-46.3 167.6 0s46.3 121.3 0 167.6l-176 176c-28.6 28.6-75 28.6-103.6 0s-28.6-75 0-103.6l144-144c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-144 144c-6.7 6.7-6.7 17.7 0 24.4s17.7 6.7 24.4 0l176-176c24.4-24.4 24.4-64 0-88.4z">
                                                                        </path>
                                                                    </svg>

                                                                    {{ $attachments[$a]['attachment']->getClientOriginalName() }}
                                                                </span>
                                                            </label>
                                                        @else
                                                            <label for="attachments.{{ $a }}.attachment"
                                                                class="relative z-30 block px-2 py-1 text-xs bg-gray-200 border border-gray-500 cursor-pointer">
                                                                <span class="flex gap-2"><svg class="w-3 h-3"
                                                                        aria-hidden="true" focusable="false"
                                                                        data-prefix="fas" data-icon="file-alt"
                                                                        role="img"
                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                        viewBox="0 0 384 512">
                                                                        <path fill="currentColor"
                                                                            d="M364.2 83.8c-24.4-24.4-64-24.4-88.4 0l-184 184c-42.1 42.1-42.1 110.3 0 152.4s110.3 42.1 152.4 0l152-152c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-152 152c-64 64-167.6 64-231.6 0s-64-167.6 0-231.6l184-184c46.3-46.3 121.3-46.3 167.6 0s46.3 121.3 0 167.6l-176 176c-28.6 28.6-75 28.6-103.6 0s-28.6-75 0-103.6l144-144c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-144 144c-6.7 6.7-6.7 17.7 0 24.4s17.7 6.7 24.4 0l176-176c24.4-24.4 24.4-64 0-88.4z">
                                                                        </path>
                                                                    </svg>Add Attachment</span>
                                                            </label>
                                                        @endif
                                                        <x-input-error
                                                            for="attachments.{{ $a }}.attachment" />
                                                    </div>
                                            </td>
                                            <td class="p-2 whitespace-nowrap">
                                                <div class="flex space-x-2">
                                                    @if (count($attachments) > 1)
                                                        <svg wire:click="removeAttachments({{ $a }})"
                                                            class="w-5 h-5 text-red" data-toggle="tooltip"
                                                            title="Remove" aria-hidden="true" focusable="false"
                                                            data-prefix="fas" data-icon="times-circle"
                                                            role="img" xmlns="http://www.w3.org/2000/svg"
                                                            viewBox="0 0 512 512">
                                                            <path fill="currentColor"
                                                                d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z">
                                                            </path>
                                                        </svg>
                                                    @endif
                                                    <button type="button" title="Add Attachment"
                                                        wire:click="addAttachments"
                                                        class="flex-none w-5 h-5 font-semibold text-white rounded-full bg-blue">
                                                        +</button>
                                                </div>
                                            </td>
                                            {{-- <td>
                                                
                                            </td> --}}
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="3">
                                                <p class="text-center">Empty.</p>
                                            </td>
                                        </tr>
                                    @endforelse
                                </x-slot>
                            </x-table.table>
                            {{-- <div class="flex items-center justify-start">
                                <button type="button" title="Add Attachment" wire:click="addAttachments"
                                    class="flex-none px-2 py-1 -mt-2 text-xs text-white rounded-lg bg-blue">
                                    +</button>
                            </div> --}}
                        </div>
                    </div>
                </div>

                <div class="col-span-6 ml-6 ">

                </div>
                <div class="col-span-3"></div>
                <div class="col-span-3 mt-4 mr-6">
                    <x-button type="button" wire:click="action({},'create_next_3')" title="Next"
                        class="bg-blue w-full text-white hover:bg-[#002161]" />
                </div>

            </div>

        </div>

        {{-- ?///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}

        <div x-cloak x-show="current_tab == 4">
            <div x-cloak x-show="current_tab == 4" class="grid grid-cols-12 gap-0 py-12 mt-4 border rounded-t-lg">

                <div class="col-span-2"></div>
                <div class="col-span-1"></div>
                <div class="col-span-6" style="display: flex;">
                    <div
                        class="step"style=" display: block; border-radius: 25px; width: 50px; height: 50px; text-align: center; line-height: 50px;">
                        <svg style="border-radius: 25px; width: 50px; height: 50px; text-align: center; line-height: 50px;"
                            class=" text-blue" aria-hidden="true" focusable="false" data-prefix="fas"
                            data-icon="trash-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 448 512">
                            <path fill="currentColor"
                                d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z">
                            </path>
                        </svg>
                    </div>
                    <div class="connector"
                        style="margin-top:22px; flex-grow: 1; width: 10px; content:none ; display: block; height: 3px; background-color: #003399;">
                    </div>
                    <div
                        class="step"style=" display: block; border-radius: 25px; width: 50px; height: 50px; text-align: center; line-height: 50px;">
                        <svg style="border-radius: 25px; width: 50px; height: 50px; text-align: center; line-height: 50px;"
                            class=" text-blue" aria-hidden="true" focusable="false" data-prefix="fas"
                            data-icon="trash-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 448 512">
                            <path fill="currentColor"
                                d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z">
                            </path>
                        </svg>
                    </div>
                    <div class="connector"
                        style="margin-top:22px; flex-grow: 1; width: 10px; content:none ; display: block; height: 3px; background-color: #003399;">
                    </div>
                    <div
                        class="step"style=" display: block; border-radius: 25px; width: 50px; height: 50px; text-align: center; line-height: 50px;">
                        <svg style="border-radius: 25px; width: 50px; height: 50px; text-align: center; line-height: 50px;"
                            class=" text-blue" aria-hidden="true" focusable="false" data-prefix="fas"
                            data-icon="trash-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 448 512">
                            <path fill="currentColor"
                                d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z">
                            </path>
                        </svg>
                    </div>
                    <div class="connector"
                        style="margin-top:22px; flex-grow: 1; width: 10px; content:none ; display: block; height: 3px; background-color: #003399;">
                    </div>
                    <div class="step"
                        style="color: white; background-color: #003399; display: block; border-radius: 25px; width: 50px; height: 50px; text-align: center; line-height: 50px;">
                        4</div>
                </div>
                <div class="col-span-1"></div>
                <div class="col-span-2"></div>

                <div class="col-span-2"></div>
                <div class="col-span-8 mt-2" style="display: flex;">
                    <div
                        class="step"style="margin-left:12%; color: #8D8D8D;  display: block; width:; height: 20px; text-align: center; line-height: 22px;">
                        General<br> Details</div>
                    <div class="connector"
                        style="margin-top:; flex-grow: 1; width: ; content:none ; display: block; height: 1px; background-color: white;">
                    </div>
                    <div class="step"
                        style="margin-left:2%; color: #8D8D8D; display: block; width: ; height: 20px; text-align: center; line-height: 22px;">
                        Shipper<br> Details</div>
                    <div class="connector"
                        style="margin-top:; flex-grow: 1; width: ; content:none ; display: block; height: 1px; background-color:white;">
                    </div>
                    <div class="step"
                        style="margin-left:2%; color: #8D8D8D;  display: block; width: ; height: 20px; text-align: center; line-height: 22px;">
                        Consignee<br> Details</div>
                    <div class="connector"
                        style="margin-left:; flex-grow: 1; width: ; content:none ; display: block; height: 1px; background-color:white;">
                    </div>
                    <div class="step"
                        style="margin-right:11% ; color: #003399;  display: block; width: ; height: 20px; text-align: center; line-height: 22px;">
                        Booking<br> Summary</div>
                </div>
                <div class="col-span-1"></div>

            </div>

            <div x-cloak x-show="current_tab == 4" class="grid grid-cols-12 gap-0 pb-2 border gap-y-0">
                <div class="col-span-12 mt-2">
                    <button type="button" wire:click="$set('current_tab', 3)"
                        class="p-2 px-3 mr-3 text-base text-blue">
                        <div class="flex items-start justify-between">
                            <svg class="w-4 h-6 font-bold" aria-hidden="true" focusable="false"
                                data-prefix="far" data-icon="print-alt" role="img"
                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                <path fill="currentColor"
                                    d="M9.4 233.4c-12.5 12.5-12.5 32.8 0 45.3l160 160c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L77.3 256 214.6 118.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0l-160 160z" />
                            </svg>
                            Back To Consignee Details
                        </div>
                    </button>
                </div>
            </div>


            <div x-cloak x-show="current_tab == 4"
                class="grid grid-cols-12 gap-8 px-8 pt-4 pb-4 rounded-lg shadow-lg">

                <div class="col-span-12 border-0 border-b-2 border-blue">
                    <h1 class="mb-1 text-xl font-semibold text-left text-blue">Shipper Details
                    </h1>
                </div>

                <div class="col-span-12 ml-8 text-left">
                    <div class="">
                        <table class="overflow-hidden">

                            {{-- <x-slot name="tbody"> --}}

                            <tr class="font-normal bg-white border-none">
                                <td class="w-1/6 text-base text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    Booking Reference Number :
                                </td>
                                <td class="w-1/6 text-base font-normal text-left text-black whitespace-nowrap">
                                    {{ $booking_reference_no_2 }}
                                </td>
                            </tr>
                            <tr class="font-normal bg-white border-0">
                                <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    Shipper Name :
                                </td>
                                <td class="w-1/6 text-base font-normal text-left text-black whitespace-nowrap">
                                    {{ $first_name_2 }} {{ $middle_name_2 }} {{ $last_name_2 }}
                                </td>
                            </tr>
                            <tr class="font-normal bg-white border-0">
                                <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    Contact Number :
                                </td>
                                <td class="w-1/6 text-base font-normal text-left text-black whitespace-nowrap">
                                    {{ $mobile_number_2 }}
                                </td>
                            </tr>
                            <tr class="font-normal bg-white border-0">
                                <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    Pick Up Address :
                                </td>
                                <td class="w-1/6 text-base font-normal text-left text-black">
                                    {{ $address_2 }}
                                </td>
                            </tr>

                            {{-- </x-slot> --}}
                        </table>
                    </div>
                </div>


                <div class="col-span-12 border-0 border-b-2 border-blue">
                    <h1 class="mb-1 text-xl font-semibold text-left text-blue ">Consignee Details
                    </h1>
                </div>

                <div class="col-span-12 ml-6 mr-6 text-left">
                    <div class="mx-auto">
                        <ul class="divide-y-8 divide-transparent" x-data="{ selected: null }">
                            <?php $e = 0; ?>
                            @foreach ($cons as $c => $con)
                                <li class="relative">
                                    <button type="button"
                                        class="w-full px-8 py-3 text-left bg-white border rounded-md shadow-md hover:bg-gray-200"
                                        @click="selected !== {{ $e }} ? selected = {{ $e }} : selected = null">
                                        <div
                                            class="flex items-center justify-between text-base font-medium text-left text-gray-600 uppercase ">
                                            <span class="">{{ $cons[$c][$c]['first_name_3'] }}
                                                {{ $cons[$c][$c]['middle_name_3'] }}
                                                {{ $cons[$c][$c]['last_name_3'] }}
                                            </span>
                                            <span>
                                                <svg x-cloak x-show="selected != '{{ $e }}'"
                                                    class="h-6 w-7" xmlns="http://www.w3.org/2000/svg"
                                                    viewBox="0 0 256 512">
                                                    <path fill="currentColor"
                                                        d="M137.4 406.6l-128-127.1C3.125 272.4 0 264.2 0 255.1s3.125-16.38 9.375-22.63l128-127.1c9.156-9.156 22.91-11.9 34.88-6.943S192 115.1 192 128v255.1c0 12.94-7.781 24.62-19.75 29.58S146.5 415.8 137.4 406.6z" />
                                                </svg>
                                                <svg x-cloak x-show="selected == '{{ $e }}'"
                                                    class="h-6 w-7" xmlns="http://www.w3.org/2000/svg"
                                                    viewBox="0 0 320 512">
                                                    <path fill="currentColor"
                                                        d="M310.6 246.6l-127.1 128C176.4 380.9 168.2 384 160 384s-16.38-3.125-22.63-9.375l-127.1-128C.2244 237.5-2.516 223.7 2.438 211.8S19.07 192 32 192h255.1c12.94 0 24.62 7.781 29.58 19.75S319.8 237.5 310.6 246.6z" />
                                                </svg>
                                            </span>
                                        </div>
                                    </button>

                                    <div class="relative mt-2 ml-6 overflow-hidden transition-all duration-700 max-h-0"
                                        style="" x-ref="container{{ $e }}"
                                        x-bind:style="selected == {{ $e }} ? 'max-height: ' + $refs
                                            .container{{ $e }}
                                            .scrollHeight + 'px' : ''">

                                        <table class="overflow-hidden">

                                            {{-- <x-slot name="tbody"> --}}

                                            <tr class="font-normal bg-white border-none">
                                                <td class="w-1/6 text-base text-gray-400 whitespace-nowrap"
                                                    style="padding-left:;">
                                                    Booking Reference Number :
                                                </td>
                                                <td
                                                    class="w-1/6 text-base font-normal text-left text-black whitespace-nowrap">
                                                    {{ $booking_reference_no_2 }}
                                                </td>
                                            </tr>

                                            <tr class="font-normal bg-white border-0">
                                                <td class="w-1/6 text-gray-400 whitespace-nowrap"
                                                    style="padding-left:;">
                                                    Contact Number :
                                                </td>
                                                <td
                                                    class="w-1/6 text-base font-normal text-left text-black whitespace-nowrap">
                                                    {{ $cons[$c][$c]['mobile_number_3'] }}
                                                </td>
                                            </tr>
                                            <tr class="font-normal bg-white border-0">
                                                <td class="w-1/6 text-gray-400 whitespace-nowrap"
                                                    style="padding-left:;">
                                                    Pick Up Address :
                                                </td>
                                                <td class="w-1/6 text-base font-normal text-left text-black ">
                                                    {{ $cons[$c][$c]['address_3'] }}
                                                </td>
                                            </tr>
                                            <tr class="font-normal bg-white border-0">
                                                <td class="w-1/6 text-gray-400 whitespace-nowrap"
                                                    style="padding-left:;">

                                                    <span
                                                        wire:click="actionview({'a': {{ $c }}}, 'view_cargo_det')"
                                                        class="underline cursor-pointer text-blue">Cargo
                                                        Details</span>
                                                </td>
                                                <td
                                                    class="w-1/6 text-base font-normal text-left text-black whitespace-nowrap">
                                                </td>
                                            </tr>

                                            {{-- </x-slot> --}}
                                        </table>

                                    </div>
                                </li>

                                <?php $e++; ?>
                            @endforeach
                        </ul>
                    </div>
                </div>

                <div class="col-span-12 mt-6 ml-8 mr-8">
                    <x-button type="button" wire:click="action({},'create_next_4')" title="BOOK NOW"
                        class="bg-blue w-full text-white hover:bg-[#002161]" />
                </div>
            </div>
        </div>

    </form>
</div>
