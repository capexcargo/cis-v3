<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',

}">
    <x-loading></x-loading>

    @if ($confirmation_modal)
        <x-modal id="confirmation_modal" size="w-auto">
            <x-slot name="body">
                <span class="relative block">
                    <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                        wire:click="$set('confirmation_modal', false)">
                    </span>
                </span>
                <h2 class="text-xl text-left ">
                    Are you sure you want to submit this new Promo Voucher?
                </h2>

                <div class="flex justify-center space-x-3">
                    <button type="button" wire:click="$set('confirmation_modal', false)"
                        class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                        No
                    </button>
                    <button type="button" wire:click="submit"
                        class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                        Yes
                    </button>
                </div>
            </x-slot>
        </x-modal>
    @endif

    <form wire:submit.prevent="validateSubmit" autocomplete="off">
        <div class="grid grid-cols-12 gap-6 py-2">

            <div class="col-span-12">
                <x-label for="name" value="Campaign Name" :required="true" />
                <x-input type="text" name="name" wire:model.defer='name'></x-input>
                <x-input-error for="name" />
            </div>

            <div class="col-span-12">
                <x-label for="voucher_code" value="Voucher Code" :required="true" />
                <x-input type="text" name="voucher_code" wire:model.defer='voucher_code'></x-input>
                <x-input-error for="voucher_code" />
            </div>

            <div class="col-span-12">
                <x-label for="discount_type" value="Type of Discount" :required="true" />
                <x-select style="cursor: pointer;" name="discount_type" wire:model='discount_type'>
                    <option value="">Select</option>
                    <option value="1">
                        Flat
                    </option>
                    <option value="2">
                        Percentage
                    </option>
                </x-select>
                <x-input-error for="discount_type" />
            </div>

            <div class="col-span-6">
                @if ($discount_type != 1)
                    <x-label for="" value="Discount Amount" :required="true" />
                    <x-input disabled type="number" name="" wire:model.defer=''></x-input>
                    <x-input-error for="" />
                @else
                    <x-label for="discount_amount" value="Discount Amount" :required="true" />
                    <x-input type="number" name="discount_amount" wire:model.defer='discount_amount'></x-input>
                    <x-input-error for="discount_amount" />
                @endif
            </div>

            <div class="col-span-6">
                @if ($discount_type != 2)
                    <x-label for="" value="Discount Percentage" :required="true" />
                    <x-input disabled type="number" name="" wire:model.defer=''></x-input>
                    <x-input-error for="" />
                @else
                    <x-label for="discount_percentage" value="Discount Percentage" :required="true" />
                    <x-input type="number" name="discount_percentage" wire:model.defer='discount_percentage'></x-input>
                    <x-input-error for="discount_percentage" />
                @endif
            </div>

            <div class="col-span-6">
                <x-label for="start_datetime" value="Start Date and Time" :required="true" />
                <x-input type="datetime-local" name="start_datetime" wire:model.defer='start_datetime'></x-input>
                <x-input-error for="start_datetime" />
            </div>

            <div class="col-span-6">
                <x-label for="end_datetime" value="End Date and Time" :required="true" />
                <x-input type="datetime-local" name="end_datetime" wire:model.defer='end_datetime'></x-input>
                <x-input-error for="end_datetime" />
            </div>

            <div class="col-span-12">
                <x-label for="maximum_usage" value="Maximum Usage" :required="true" />
                <x-input type="number" name="maximum_usage" wire:model.defer='maximum_usage'></x-input>
                <x-input-error for="maximum_usage" />
            </div>

            <div class="col-span-12">
                <x-textarea style="overflow: hidden" rows="3" cols="25" type="text" id="autoresizing"
                                name="terms_conditon"
                                wire:model.defer='terms_conditon'>
                            </x-textarea>
                <x-input-error for="terms_conditon" />
            </div>

            <div class="col-span-12">
                <div class="grid grid-cols-1">
                    <div>
                        <div class="text-2xl font-bold text-blue">
                            <div class="flex items-center space-x-3">
                                <x-label for="attachment" value="Attachments*" />
                            </div>
                        </div>
                        <div class="grid gap-2">
                            <div>
                                <x-table.table class="overflow-hidden">
                                    <x-slot name="thead">
                                        
                                    </x-slot>
                                    <x-slot name="tbody">
                                        @forelse ($attachments as $i => $attachment)
                                            <tr
                                                class="text-sm border-0 cursor-pointer bg-white hover:text-blue">
                                                <td class="flex p-2 items-left justify-left whitespace-nowrap">
                                                    <div class="flex-shrink-0 mb-1 mr-1 whitespace-nowrap">
                                                       
                                                        <div class="relative z-0">
                                                            <input type="file"
                                                                name="attachments.{{ $i }}.attachment"
                                                                wire:model="attachments.{{ $i }}.attachment"
                                                                class="absolute top-0 left-0 z-50 opacity-0">
    
                                                            @if ($attachments[$i]['attachment'])
                                                            <div class="flex justify-between space-x-3">
                                                                <label for="attachments.{{ $i }}.attachment"
                                                                    class="relative z-30 block px-2 py-1 text-xs bg-gray-200 border border-gray-500 rounded-lg cursor-pointer">
                                                                    <span class="flex gap-2"><svg class="w-3 h-3 rotate-90" 
                                                                        aria-hidden="true" focusable="false"
                                                                        data-prefix="fas" data-icon="file-alt"
                                                                        role="img"
                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                        viewBox="0 0 384 512">
                                                                        <path fill="currentColor"
                                                                            d="M364.2 83.8c-24.4-24.4-64-24.4-88.4 0l-184 184c-42.1 42.1-42.1 110.3 0 152.4s110.3 42.1 152.4 0l152-152c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-152 152c-64 64-167.6 64-231.6 0s-64-167.6 0-231.6l184-184c46.3-46.3 121.3-46.3 167.6 0s46.3 121.3 0 167.6l-176 176c-28.6 28.6-75 28.6-103.6 0s-28.6-75 0-103.6l144-144c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-144 144c-6.7 6.7-6.7 17.7 0 24.4s17.7 6.7 24.4 0l176-176c24.4-24.4 24.4-64 0-88.4z">
                                                                        </path>
                                                                    </svg>Choose file
                                                                </span>
                                                                </label>
                                                                <span class="underline text-blue mt-1 text-xs cursor-pointer whitespace-pre-wrap">{{ $attachments[$i]['attachment']->getClientOriginalName() }}</span>
                                                            </div>

                                                            @else
                                                                <label for="attachments.{{ $i }}.attachment"
                                                                    class="relative z-30 block px-2 py-1 text-xs bg-gray-200 border border-gray-500 rounded-lg cursor-pointer">
                                                                    <span class="flex gap-2"><svg class="w-3 h-3 -rotate-90"
                                                                        aria-hidden="true" focusable="false"
                                                                        data-prefix="fas" data-icon="file-alt"
                                                                        role="img"
                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                        viewBox="0 0 384 512">
                                                                        <path fill="currentColor"
                                                                            d="M364.2 83.8c-24.4-24.4-64-24.4-88.4 0l-184 184c-42.1 42.1-42.1 110.3 0 152.4s110.3 42.1 152.4 0l152-152c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-152 152c-64 64-167.6 64-231.6 0s-64-167.6 0-231.6l184-184c46.3-46.3 121.3-46.3 167.6 0s46.3 121.3 0 167.6l-176 176c-28.6 28.6-75 28.6-103.6 0s-28.6-75 0-103.6l144-144c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-144 144c-6.7 6.7-6.7 17.7 0 24.4s17.7 6.7 24.4 0l176-176c24.4-24.4 24.4-64 0-88.4z">
                                                                        </path>
                                                                    </svg>Choose file</span>

                                                                </label>

                                                            @endif
    
                                                            <x-input-error
                                                                for="attachments.{{ $i }}.attachment" />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="3">
                                                    <p class="text-center">Empty.</p>
                                                </td>
                                            </tr>
                                        @endforelse
                                    </x-slot>
                                </x-table.table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-span-12">
                <div class="flex gap-4 mt-4">
                    <div class="w-full col-span-11">
                        <div class="flex justify-end gap-4">
                            <button type="button" wire:click="closecreatemodal"
                                class="px-10 py-2 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:text-blue hover:bg-gray-200">
                                Cancel
                            </button>
                            <button type="submit" {{-- wire:click="submit" --}}
                                class="px-10 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg">
                                Submit
                            </button>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </form>
</div>

{{-- @push('Scripts') --}}
    
<script type="text/javascript">
    textarea = document.querySelector("#autoresizing");
    textarea.addEventListener('input', autoResize, false);

    function autoResize() {
        this.style.height = 'auto';
        this.style.height = this.scrollHeight + 'px';
    }
</script>
{{-- @endpush --}}
