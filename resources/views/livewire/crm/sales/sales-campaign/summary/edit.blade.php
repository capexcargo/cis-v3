<div>
    <form wire:submit.prevent="validateSubmit" autocomplete="off">

        <div class="grid grid-cols-12 gap-6 py-2">
            <div class="col-span-12">
                <x-label for="name" value="Campaign Name" :required="true" />
                <x-input disabled type="text" name="name" wire:model.defer='name'></x-input>
                <x-input-error for="name" />
            </div>

            <div class="col-span-12">
                <x-label for="voucher_code" value="Voucher Code" :required="true" />
                <x-input disabled type="text" name="voucher_code" wire:model.defer='voucher_code'></x-input>
                <x-input-error for="voucher_code" />
            </div>

            <div class="col-span-12">
                <x-label for="discount_type" value="Type of Discount" :required="true" />
                <x-select disabled style="cursor: pointer;" name="discount_type" wire:model='discount_type'>
                    <option value="">Select</option>
                    <option value="1">
                        Flat
                    </option>
                    <option value="2">
                        Percentage
                    </option>
                </x-select>
                <x-input-error for="discount_type" />
            </div>

            <div class="col-span-6">
                {{-- @if ($discount_type != 1) --}}
                <x-label for="" value="Discount Amount" :required="true" />
                <x-input disabled type="number" name="" wire:model.defer='discount_amount'></x-input>
                <x-input-error for="" />
                {{-- @else
                    <x-label for="discount_amount" value="Discount Amount" :required="true" />
                    <x-input type="number" name="discount_amount" wire:model.defer='discount_amount'></x-input>
                    <x-input-error for="discount_amount" />
                @endif --}}
            </div>

            <div class="col-span-6">
                {{-- @if ($discount_type != 2) --}}
                <x-label for="" value="Discount Percentage" :required="true" />
                <x-input disabled type="number" name="" wire:model.defer='discount_percentage'></x-input>
                <x-input-error for="" />
                {{-- @else
                    <x-label for="discount_percentage" value="Discount Percentage" :required="true" />
                    <x-input type="number" name="discount_percentage" wire:model.defer='discount_percentage'></x-input>
                    <x-input-error for="discount_percentage" />
                @endif --}}
            </div>

            <div class="col-span-6">
                <x-label for="start_datetime" value="Start Date and Time" :required="true" />
                <x-input disabled type="datetime-local" name="start_datetime" wire:model.defer='start_datetime'>
                </x-input>
                <x-input-error for="start_datetime" />
            </div>

            <div class="col-span-6">
                <x-label for="end_datetime" value="End Date and Time" :required="true" />
                <x-input type="datetime-local" name="end_datetime" wire:model.defer='end_datetime'></x-input>
                <x-input-error for="end_datetime" />
            </div>

            <div class="col-span-12">
                <x-label for="maximum_usage" value="Maximum Usage" :required="true" />
                <x-input disabled type="number" name="maximum_usage" wire:model.defer='maximum_usage'></x-input>
                <x-input-error for="maximum_usage" />
            </div>

            <div class="col-span-12">
                <x-label for="terms_conditon" value="Terms and Conditions" :required="true" />
                <x-textarea disabled style="overflow: hidden" rows="3" cols="25" type="text"
                    id="autoresizing2" name="terms_conditon" wire:model.defer='terms_conditon'>
                </x-textarea>
                <x-input-error for="terms_conditon" />
            </div>

            <div class="col-span-12">
                <x-label for="attachment" value="Attachment" :required="true" />
                <div class="grid gap-2">
                    <div class="flex flex-col space-y-3">
                        <x-table.table class="overflow-hidden">
                            <x-slot name="thead">
                                {{-- <x-table.th name="File" />
                                        <x-table.th name="Action" /> --}}
                            </x-slot>
                            <x-slot name="tbody">
                                @foreach ($attachments as $a => $attachment)
                                    @if (!$attachment['is_deleted'])

                                        <tr class="text-sm bg-white border-0 cursor-pointer ">
                                            <td class="flex p-2 items-left justify-left whitespace-nowrap">
                                                <div class="flex-shrink-0 mb-1 mr-1 whitespace-nowrap">
                                                    <div class="relative z-0">
                                                        <input type="file"
                                                            name="attachments.{{ $a }}.attachment"
                                                            wire:model="attachments.{{ $a }}.attachment"
                                                            class="absolute top-0 left-0 z-50 opacity-0">

                                                        @if ($attachments[$a]['attachment'])
                                                            <div class="flex justify-between space-x-3">
                                                                <label for="attachments.{{ $a }}.attachment"
                                                                    class="relative z-30 block px-2 py-1 text-xs bg-gray-200 border border-gray-500 rounded-lg cursor-pointer">
                                                                    <span class="flex gap-2"><svg class="w-3 h-3"
                                                                            aria-hidden="true" focusable="false"
                                                                            data-prefix="fas" data-icon="file-alt"
                                                                            role="img"
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                            viewBox="0 0 384 512">
                                                                            <path fill="currentColor"
                                                                                d="M364.2 83.8c-24.4-24.4-64-24.4-88.4 0l-184 184c-42.1 42.1-42.1 110.3 0 152.4s110.3 42.1 152.4 0l152-152c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-152 152c-64 64-167.6 64-231.6 0s-64-167.6 0-231.6l184-184c46.3-46.3 121.3-46.3 167.6 0s46.3 121.3 0 167.6l-176 176c-28.6 28.6-75 28.6-103.6 0s-28.6-75 0-103.6l144-144c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-144 144c-6.7 6.7-6.7 17.7 0 24.4s17.7 6.7 24.4 0l176-176c24.4-24.4 24.4-64 0-88.4z">
                                                                            </path>
                                                                        </svg>Choose file
                                                                    </span>
                                                                </label>
                                                                <span
                                                                    class="mt-1 text-xs underline whitespace-pre-wrap cursor-pointer text-blue">{{ $attachments[$a]['name'] }}new</span>
                                                            </div>
                                                        @else
                                                            <div class="flex justify-between space-x-3">
                                                                <label for="attachments.{{ $a }}.attachment"
                                                                    class="relative z-30 block px-2 py-1 text-xs bg-gray-200 border border-gray-500 rounded-lg cursor-pointer">
                                                                    <span class="flex gap-2"><svg class="w-3 h-3"
                                                                            aria-hidden="true" focusable="false"
                                                                            data-prefix="fas" data-icon="file-alt"
                                                                            role="img"
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                            viewBox="0 0 384 512">
                                                                            <path fill="currentColor"
                                                                                d="M364.2 83.8c-24.4-24.4-64-24.4-88.4 0l-184 184c-42.1 42.1-42.1 110.3 0 152.4s110.3 42.1 152.4 0l152-152c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-152 152c-64 64-167.6 64-231.6 0s-64-167.6 0-231.6l184-184c46.3-46.3 121.3-46.3 167.6 0s46.3 121.3 0 167.6l-176 176c-28.6 28.6-75 28.6-103.6 0s-28.6-75 0-103.6l144-144c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-144 144c-6.7 6.7-6.7 17.7 0 24.4s17.7 6.7 24.4 0l176-176c24.4-24.4 24.4-64 0-88.4z">
                                                                            </path>
                                                                        </svg>Choose file</span>

                                                                </label>
                                                                <span
                                                                    class="mt-1 text-xs underline whitespace-pre-wrap cursor-pointer text-blue">{{ $attachments[$a]['name'] }}old</span>
                                                            </div>
                                                        @endif
                                                        <x-input-error
                                                            for="attachments.{{ $a }}.attachment" />
                                                    </div>
                                                </div>
                                            </td>
                                            {{-- <td class="p-2 whitespace-nowrap">
                                                <div class="flex items-center justify-center space-x-3">
                                                    @if (in_array($attachment['extension'], config('filesystems.image_type')))
                                                        <a href="{{ Storage::disk('crm_gcs')->url($attachment['path'] . $attachment['name']) }}"
                                                            target="_blank">
                                                            <svg class="w-5 h-5 text-blue"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                                viewBox="0 0 576 512" fill="currentColor">
                                                                <path
                                                                    d="M279.6 160.4C282.4 160.1 285.2 160 288 160C341 160 384 202.1 384 256C384 309 341 352 288 352C234.1 352 192 309 192 256C192 253.2 192.1 250.4 192.4 247.6C201.7 252.1 212.5 256 224 256C259.3 256 288 227.3 288 192C288 180.5 284.1 169.7 279.6 160.4zM480.6 112.6C527.4 156 558.7 207.1 573.5 243.7C576.8 251.6 576.8 260.4 573.5 268.3C558.7 304 527.4 355.1 480.6 399.4C433.5 443.2 368.8 480 288 480C207.2 480 142.5 443.2 95.42 399.4C48.62 355.1 17.34 304 2.461 268.3C-.8205 260.4-.8205 251.6 2.461 243.7C17.34 207.1 48.62 156 95.42 112.6C142.5 68.84 207.2 32 288 32C368.8 32 433.5 68.84 480.6 112.6V112.6zM288 112C208.5 112 144 176.5 144 256C144 335.5 208.5 400 288 400C367.5 400 432 335.5 432 256C432 176.5 367.5 112 288 112z" />
                                                            </svg>
                                                        </a>
                                                    @elseif(in_array($attachment['extension'], config('filesystems.file_type')))
                                                        <svg wire:click="download({{ $attachment['id'] }})"
                                                            class="w-5 h-5 text-blue"
                                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"
                                                            fill="currentColor">
                                                            <path
                                                                d="M480 352h-133.5l-45.25 45.25C289.2 409.3 273.1 416 256 416s-33.16-6.656-45.25-18.75L165.5 352H32c-17.67 0-32 14.33-32 32v96c0 17.67 14.33 32 32 32h448c17.67 0 32-14.33 32-32v-96C512 366.3 497.7 352 480 352zM432 456c-13.2 0-24-10.8-24-24c0-13.2 10.8-24 24-24s24 10.8 24 24C456 445.2 445.2 456 432 456zM233.4 374.6C239.6 380.9 247.8 384 256 384s16.38-3.125 22.62-9.375l128-128c12.49-12.5 12.49-32.75 0-45.25c-12.5-12.5-32.76-12.5-45.25 0L288 274.8V32c0-17.67-14.33-32-32-32C238.3 0 224 14.33 224 32v242.8L150.6 201.4c-12.49-12.5-32.75-12.5-45.25 0c-12.49 12.5-12.49 32.75 0 45.25L233.4 374.6z" />
                                                        </svg>
                                                    @endif
                                                    @if (count(collect($attachments)->where('is_deleted', false)) > 1)
                                                        <svg wire:click="removeAttachments({{ $a }})"
                                                            class="w-5 h-5 text-red" aria-hidden="true"
                                                            focusable="false" data-prefix="fas"
                                                            data-icon="times-circle" role="img"
                                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                            <path fill="currentColor"
                                                                d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z">
                                                            </path>
                                                        </svg>
                                                    @endif
                                                </div>
                                            </td> --}}
                                        </tr>
                                    @endif
                                @endforeach
                            </x-slot>
                        </x-table.table>
                        {{-- <div class="flex items-center justify-start">
                                <button type="button" title="Add Attachment" wire:click="addAttachments"
                                    class="flex-none px-2 py-1 -mt-2 text-xs text-white rounded-lg bg-blue">
                                    +</button>
                            </div> --}}
                    </div>
                </div>

            </div>

            <div class="col-span-12">
                <div class="flex gap-4 mt-4">
                    <div class="w-full col-span-11">
                        <div class="flex justify-end gap-4">
                            <button type="button" wire:click="$emit('close_modal', 'edit')"
                                class="px-10 py-2 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:text-blue hover:bg-gray-200">
                                Cancel
                            </button>
                            <button type="submit" {{-- wire:click="submit" --}}
                                class="px-10 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg">
                                Submit
                            </button>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </form>
</div>


<script type="text/javascript">
    textarea = document.querySelector("#autoresizing2");
    textarea.addEventListener('input', autoResize, false);

    function autoResize() {
        this.style.height = 'auto';
        this.style.height = this.scrollHeight + 'px';
    }
</script>
