{{-- <div>
    A good traveler has no fixed plans and is not intent upon arriving.
</div> --}}
<div class="grid grid-cols-1 gap-4 px-4">
    <h1 class="mt-1 text-xl font-bold text-left text-black ">Terms and Conditions
    </h1>
    <div class="flex items-center mb-4">

        <p class="font-semibold whitespace-pre-line">{{ $terms_conditon }}</p>
    </div>
</div>
