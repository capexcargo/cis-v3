<x-form x-data="{
    search_form: false,
    create_modal: '{{ $create_modal }}',
    edit_modal: '{{ $edit_modal }}',
    delete_modal: '{{ $delete_modal }}',
    terms_modal: '{{ $terms_modal }}',
    reactivate_modal: '{{ $reactivate_modal }}',
    deactivate_modal: '{{ $deactivate_modal }}',

}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>

    <x-slot name="modals">
        @can('crm_sales_sales_campaign_summary_add')
            <x-modal id="create_modal" size="w-1/3">
                <x-slot name="title">Create Promo Voucher</x-slot>
                <x-slot name="body">
                    @livewire('crm.sales.sales-campaign.summary.create')
                </x-slot>
            </x-modal>
        @endcan

        @can('crm_sales_sales_campaign_summary_edit')
            @if ($summary_id && $edit_modal)
                <x-modal id="edit_modal" size="w-1/3">
                    <x-slot name="title">Edit Promo Voucher</x-slot>
                    <x-slot name="body">
                        @livewire('crm.sales.sales-campaign.summary.edit', ['id' => $summary_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan

        @can('crm_sales_sales_campaign_summary_delete')
            @if ($summary_id && $delete_modal)
                <x-modal id="delete_modal" size="w-1/4">
                    <x-slot name="title">Delete Promo Voucher</x-slot>
                    <x-slot name="body">
                        <div>
                            <h2>Are you sure you want to delete this Promo Voucher?</h2>
                        </div>
                        <div class="flex justify-end mt-6 space-x-3">
                            <button
                                class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400"
                                type="button" wire:click="$emit('close_modal', 'delete')">No</button>
                            <button class="px-12 py-2 text-xs flex-none bg-[#003399] text-white rounded-lg" type="button"
                                wire:click="confirmDelete">Yes</button>
                        </div>
                    </x-slot>
                </x-modal>
            @endif
        @endcan

        @can('crm_sales_sales_campaign_summary_terms_view')
            @if ($summary_id && $terms_modal)
                <x-modal id="terms_modal" size="w-1/2">
                    {{-- <x-slot name="title">Terms and Conditions</x-slot> --}}
                    <x-slot name="body">
                        @livewire('crm.sales.sales-campaign.summary.terms-view', ['id' => $summary_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan

        @can('crm_sales_sales_campaign_summary_deactivate')
            <x-modal id="reactivate_modal" size="w-auto">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center">
                        <h2 class="text-xl text-center text-gray-900 ">
                            Are you sure you want to reactivate this Promo Voucher?</h2>
                        <div class="flex justify-center space-x-3">
                            <button wire:click="$set('reactivate_modal', false)"
                                class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">NO</button>
                            <button wire:click="updateStatus('{{ $summary_id }}', 1)"
                                class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                                YES</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
            <x-modal id="deactivate_modal" size="w-auto">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center">
                        <h2 class="text-xl text-center text-gray-900 ">
                            Are you sure you want to deactivate this Promo Voucher?</h2>
                        <div class="flex justify-center space-x-3">
                            <button wire:click="$set('deactivate_modal', false)"
                                class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">NO</button>
                            <button wire:click="updateStatus('{{ $summary_id }}', 2)"
                                class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                                YES</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endcan
    </x-slot>

    <x-slot name="header_title">Sales Campaign</x-slot>
    <x-slot name="header_button">
        @can('crm_sales_sales_campaign_summary_add')
            <button wire:click="action({}, 'add')"
                class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue hover:bg-blue-900">
                <div class="flex items-start justify-between">
                    Create Promo Voucher
                </div>
            </button>
        @endcan
        {{-- @can('crm_service_request_sr_related_mgmt_add') --}}
        <button wire:click="" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue hover:bg-blue-900">
            <div class="flex items-start justify-between">
                Blast Campaign
            </div>
        </button>
        {{-- @endcan --}}
        {{-- @can('crm_service_request_sr_related_mgmt_add') --}}
        {{-- <div class="text-blue whitespace-nowrap" x-data="{ open: false }">
            <button @click="open = !open"
                class="p-2 px-3 mr-3 text-sm bg-white border-2 border-blue-700 rounded-md text-blue hover:bg-gray-300">
                <div class="flex items-start justify-between">
                    Management
                    <svg class="w-3 h-3 mt-1 ml-2" aria-hidden="true" focusable="false" data-prefix="far"
                        data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M137.4 374.6c12.5 12.5 32.8 12.5 45.3 0l128-128c9.2-9.2 11.9-22.9 6.9-34.9s-16.6-19.8-29.6-19.8L32 192c-12.9 0-24.6 7.8-29.6 19.8s-2.2 25.7 6.9 34.9l128 128z" />
                    </svg>
                </div>
            </button>
            <div class="absolute" style="margin-left: -3.5rem">
                <ul class="text-gray-600 bg-white border-2 border-solid rounded shadow" x-show="open"
                    @click.away="open = false">
                    <li class="px-3 py-1" x-data="{ open: false }"><a
                            href="http://cis-v3.test/crm/sales/audiencesegmentation">Audience Segmentation</a>
                    </li>
                    </li>
                </ul>
            </div>
        </div> --}}
        <div class="text-blue whitespace-nowrap" x-data="{ open: false }">
            <button @click="open = !open"
                class="p-2 px-3 mr-3 text-sm bg-white border-2 border-blue-700 rounded-md text-blue hover:bg-gray-300">
                <div class="flex items-start justify-between">
                    Management
                    <svg class="w-3 h-3 mt-1 ml-2" aria-hidden="true" focusable="false" data-prefix="far"
                        data-icon="print-alt" role="img" x mlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M137.4 374.6c12.5 12.5 32.8 12.5 45.3 0l128-128c9.2-9.2 11.9-22.9 6.9-34.9s-16.6-19.8-29.6-19.8L32 192c-12.9 0-24.6 7.8-29.6 19.8s-2.2 25.7 6.9 34.9l128 128z" />
                    </svg>
                </div>
            </button>
            <div class="absolute mt-1 border border-gray-500 rounded-md" style="margin-left: -4.8rem">
                <ul class="text-gray-600 bg-white rounded shadow" x-show="open" @click.away="open = false">
                    <li class="px-3 py-1 cursor-pointer" x-data="{ open: false }"
                        wire:click="action({},'audiencesegmentation')">Audience Segmentation</a>
                    </li>
                    </li>
                </ul>
            </div>
        </div>
        {{-- @endcan --}}


    </x-slot>
    <x-slot name="body">

        <div class="py-12">
            <div class="flex items-center justify-start">
                <div wire:click="redirectTo({}, 'redirectToHome')"
                    class="flex items-center justify-between px-8 py-1 text-sm text-left bg-white border border-gray-400 cursor-pointer rounded-l-md">
                    <span>Home</span>
                    {{-- <span class="ml-6">{{ $sr_types->count() }}</span> --}}

                </div>
                <div wire:click="redirectTo({}, 'redirectToSummary')"
                    class="flex justify-between px-8 py-1 text-sm text-white border border-gray-400 cursor-pointer rounded-r-md bg-blue ">
                    <span class="">Summary</span>
                    {{-- <span class="ml-6">{{ $sr_subs->count() }}</span> --}}
                </div>

            </div>


            <div class="w-full text-center bg-white rounded-lg shadow-md">
                <x-table.table>
                    <x-slot name="thead">
                        <x-table.th name="No." />
                        <x-table.th name="Campaign Name" style="padding-left:;" />
                        <x-table.th name="Voucher Reference No." style="padding-left:;" />
                        <x-table.th name="Voucher Code" style="padding-left:;" />
                        <x-table.th name="Type of Discount" style="padding-left:;" />
                        <x-table.th name="Discount Amount" style="padding-left:;" />
                        <x-table.th name="Discount Percentage" style="padding-left:;" />
                        <x-table.th name="Start Date & Time" style="padding-left:;" />
                        <x-table.th name="End Date & Time" style="padding-left:;" />
                        <x-table.th name="Total Usage" style="padding-left:;" />
                        <x-table.th name="Maximum Usage" style="padding-left:;" />
                        <x-table.th name="Status" style="padding-left:2%;" />
                        <x-table.th name="Terms and Conditions" style="padding-left:;" />
                        <x-table.th name="Action" style="padding-left:2.5;" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($summary as $i => $summaries)
                            <tr class="font-normal border-0 cursor-pointer">
                                <td class="w-1/12 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ $i + 1 }}.
                                </td>
                                <td class="w-1/12 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ $summaries->name }}
                                </td>
                                <td class="w-1/12 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ $summaries->reference_no }}
                                </td>
                                <td class="w-1/12 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ $summaries->voucher_code }}
                                </td>
                                @if ($summaries->discount_type == 1)
                                    <td class="w-1/12 p-3 whitespace-nowrap" style="padding-left:;">
                                        Flat
                                    </td>
                                @elseif($summaries->discount_type == 2)
                                    <td class="w-1/12 p-3 whitespace-nowrap" style="padding-left:;">
                                        Percentage
                                    </td>
                                @endif
                                @if ($summaries->discount_amount != null)
                                    <td class="w-1/12 p-3 whitespace-nowrap" style="padding-left:;">
                                        {{ 'P ' . number_format((float) $summaries->discount_amount, 2, '.', '') }}
                                    </td>
                                @elseif ($summaries->discount_amount == null)
                                    <td class="w-1/12 p-3 whitespace-nowrap" style="padding-left:;">
                                        {{ $summaries->discount_amount }}
                                    </td>
                                @endif
                                @if ($summaries->discount_percentage != null)
                                    <td class="w-1/12 p-3 whitespace-nowrap" style="padding-left:;">
                                        {{ $summaries->discount_percentage . '%' }}
                                    </td>
                                @elseif($summaries->discount_percentage == null)
                                    <td class="w-1/12 p-3 whitespace-nowrap" style="padding-left:;">
                                        {{ $summaries->discount_percentage}}
                                    </td>
                                @endif
                                <td class="w-1/12 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ date('m/d/Y g:i:A', strtotime($summaries->start_datetime)) }}
                                </td>
                                <td class="w-1/12 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ date('m/d/Y g:i:A', strtotime($summaries->end_datetime)) }}
                                </td>
                                <td class="w-1/12 p-3 whitespace-nowrap" style="padding-left:;">
                                    -
                                </td>
                                <td class="w-1/12 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ $summaries->maximum_usage }}
                                </td>

                                @if ($summaries->status == 1)
                                    <td class="w-1/12 p-3 whitespace-nowrap" style="padding-left:;">
                                        <span
                                            class="py-1 text-xs text-green-600 bg-green-100 rounded-lg px-9">Active</span>
                                    </td>
                                @elseif($summaries->status == 2)
                                    <td class="w-1/12 p-3 whitespace-nowrap" style="padding-left:;">
                                        <span
                                            class="px-8 py-1 text-xs text-red-600 bg-red-100 rounded-lg">Inactive</span>
                                    </td>
                                @elseif($summaries->status == null)
                                    <td class="w-1/12 p-3 whitespace-nowrap" style="padding-left:;">
                                        <P class="text-center">-</P>
                                    </td>
                                @endif

                                @can('crm_sales_sales_campaign_summary_terms_view')
                                    <td class="w-1/12 p-3 whitespace-nowrap" style="padding-left:;">
                                        <a href="#" class="ml-4 text-blue-600 underline d-inline"
                                            wire:click="action({'id': {{ $summaries->id }}}, 'terms_view')">View</a>
                                    </td>
                                @endcan

                                <td class="p-3 whitespace-nowrap">
                                    <div class="flex ml-5 space-x-3">
                                        @can('crm_sales_sales_campaign_summary_edit')
                                            <svg wire:click="action({'id': {{ $summaries->id }}}, 'edit')"
                                                class="w-5 h-5 text-blue"" aria-hidden="true" focusable="false"
                                                data-prefix="far" data-icon="edit" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                <path fill="currentColor"
                                                    d="M471.6 21.7c-21.9-21.9-57.3-21.9-79.2 0L362.3 51.7l97.9 97.9 30.1-30.1c21.9-21.9 21.9-57.3 0-79.2L471.6 21.7zm-299.2 220c-6.1 6.1-10.8 13.6-13.5 21.9l-29.6 88.8c-2.9 8.6-.6 18.1 5.8 24.6s15.9 8.7 24.6 5.8l88.8-29.6c8.2-2.8 15.7-7.4 21.9-13.5L437.7 172.3 339.7 74.3 172.4 241.7zM96 64C43 64 0 107 0 160V416c0 53 43 96 96 96H352c53 0 96-43 96-96V320c0-17.7-14.3-32-32-32s-32 14.3-32 32v96c0 17.7-14.3 32-32 32H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h96c17.7 0 32-14.3 32-32s-14.3-32-32-32H96z">
                                                </path>
                                            </svg>
                                        @endcan

                                        @can('crm_sales_sales_campaign_summary_deactivate')
                                            @if ($summaries->status == 1)
                                                <span title="Deactivate">
                                                    <svg x-cloak x-show="'{{ $summaries->status == 1 }}'"
                                                        wire:click="action({'id': {{ $summaries->id }}, 'status': 2}, 'update_status')"
                                                        class="w-8 h-5 text-blue" aria-hidden="true" focusable="false"
                                                        data-prefix="fas" data-icon="user-slash" role="img"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512">
                                                        <path fill="currentColor"
                                                            d="M192 64C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192s-86-192-192-192H192zM384 352c-53 0-96-43-96-96s43-96 96-96s96 43 96 96s-43 96-96 96z">
                                                        </path>
                                                    </svg>
                                                </span>
                                            @else
                                                <span title="Reactivate">
                                                    <svg x-cloak x-show="'{{ $summaries->status == 2 }}'"
                                                        wire:click="action({'id': {{ $summaries->id }}, 'status': 1}, 'update_status')"
                                                        class="w-8 h-5 pl-2 text-gray-400 rotate-180" aria-hidden=" true"
                                                        focusable="false" data-prefix="fas" data-icon="user"
                                                        role="img" xmlns="http://www.w3.org/2000/svg"
                                                        viewBox="0 0 640 512">
                                                        <path fill="currentColor"
                                                            d="M192 64C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192s-86-192-192-192H192zm192 96a96 96 0 1 1 0 192 96 96 0 1 1 0-192z">
                                                        </path>
                                                    </svg>
                                                </span>
                                            @endif
                                        @endcan

                                        @can('crm_sales_sales_campaign_summary_delete')
                                            <svg wire:click="action({'id': {{ $summaries->id }}},'delete')"
                                                class="w-5 h-5 text-red" aria-hidden="true" focusable="false"
                                                data-prefix="fas" data-icon="trash-alt" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                <path fill="currentColor"
                                                    d="M160 400C160 408.8 152.8 416 144 416C135.2 416 128 408.8 128 400V192C128 183.2 135.2 176 144 176C152.8 176 160 183.2 160 192V400zM240 400C240 408.8 232.8 416 224 416C215.2 416 208 408.8 208 400V192C208 183.2 215.2 176 224 176C232.8 176 240 183.2 240 192V400zM320 400C320 408.8 312.8 416 304 416C295.2 416 288 408.8 288 400V192C288 183.2 295.2 176 304 176C312.8 176 320 183.2 320 192V400zM317.5 24.94L354.2 80H424C437.3 80 448 90.75 448 104C448 117.3 437.3 128 424 128H416V432C416 476.2 380.2 512 336 512H112C67.82 512 32 476.2 32 432V128H24C10.75 128 0 117.3 0 104C0 90.75 10.75 80 24 80H93.82L130.5 24.94C140.9 9.357 158.4 0 177.1 0H270.9C289.6 0 307.1 9.358 317.5 24.94H317.5zM151.5 80H296.5L277.5 51.56C276 49.34 273.5 48 270.9 48H177.1C174.5 48 171.1 49.34 170.5 51.56L151.5 80zM80 432C80 449.7 94.33 464 112 464H336C353.7 464 368 449.7 368 432V128H80V432z">
                                                </path>
                                            </svg>
                                        @endcan

                                    </div>
                                </td>
                            </tr>
                        @endforeach

                    </x-slot>

                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $summary->links() }}
                </div>
            </div>

        </div>

    </x-slot>
</x-form>
