<x-form x-data="{
    search_form: false,
    create_modal: '{{ $create_modal }}',
    terms_modal: '{{ $terms_modal }}',
    {{-- edit_modal: '{{ $edit_modal }}',
    delete_modal: '{{ $delete_modal }}', --}}

}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>

    <x-slot name="modals">
        @can('crm_sales_sales_campaign_add')
            <x-modal id="create_modal" size="w-1/3">
                <x-slot name="title">Create Promo Voucher</x-slot>
                <x-slot name="body">
                    @livewire('crm.sales.sales-campaign.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('crm_sales_sales_campaign_terms_view')
            @if ($summary_id && $terms_modal)
                <x-modal id="terms_modal" size="w-1/2">
                    <x-slot name="body">
                        @livewire('crm.sales.sales-campaign.terms-view', ['id' => $summary_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
    </x-slot>

    <x-slot name="header_title">Sales Campaign</x-slot>
    <x-slot name="header_button">

        @can('crm_sales_sales_campaign_add')
            <button wire:click="action({}, 'add')"
                class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue hover:bg-blue-900">
                <div class="flex items-start justify-between">
                    Create Promo Voucher
                </div>
            </button>
        @endcan
        {{-- @can('crm_service_request_sr_related_mgmt_add') --}}
        <button wire:click="" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue hover:bg-blue-900">
            <div class="flex items-start justify-between">
                Blast Campaign
            </div>
        </button>
        {{-- @endcan --}}
        {{-- @can('crm_service_request_sr_related_mgmt_add') --}}
        <div class="text-blue whitespace-nowrap" x-data="{ open: false }">
            <button @click="open = !open"
                class="p-2 px-3 mr-3 text-sm bg-white border-2 border-blue-700 rounded-md text-blue hover:bg-gray-300">
                <div class="flex items-start justify-between">
                    Management
                    <svg class="w-3 h-3 mt-1 ml-2" aria-hidden="true" focusable="false" data-prefix="far"
                        data-icon="print-alt" role="img" x mlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M137.4 374.6c12.5 12.5 32.8 12.5 45.3 0l128-128c9.2-9.2 11.9-22.9 6.9-34.9s-16.6-19.8-29.6-19.8L32 192c-12.9 0-24.6 7.8-29.6 19.8s-2.2 25.7 6.9 34.9l128 128z" />
                    </svg>
                </div>
            </button>
            <div class="absolute mt-1 border border-gray-500 rounded-md" style="margin-left: -4.8rem">
                <ul class="text-gray-600 bg-white rounded shadow" x-show="open" @click.away="open = false">
                    <li class="px-3 py-1 cursor-pointer" x-data="{ open: false }"
                        wire:click="action({},'audiencesegmentation')">Audience Segmentation</a>
                    </li>
                    </li>
                </ul>
            </div>
        </div>

        {{-- @endcan --}}


    </x-slot>
    <x-slot name="body">

        <div class="py-12">
            <div class="flex items-center justify-start">
                <div wire:click="redirectTo({}, 'redirectToHome')"
                    class="flex items-center justify-between px-8 py-1 text-sm text-white border border-gray-400 cursor-pointer rounded-l-md bg-blue">
                    <span>Home</span>
                    {{-- <span class="ml-6">{{ $sr_types->count() }}</span> --}}

                </div>
                <div wire:click="redirectTo({}, 'redirectToSummary')"
                    class="flex justify-between px-8 py-1 text-sm text-left bg-white border border-gray-400 cursor-pointer rounded-r-md">
                    <span class="">Summary</span>
                    {{-- <span class="ml-6">{{ $sr_subs->count() }}</span> --}}
                </div>
            </div>

            <div class="grid grid-cols-12 gap-12 mt-3">

                @foreach ($summary as $i => $summaries)
                    @can('crm_sales_sales_campaign_terms_view')
                        <div class="w-full h-full col-span-4 p-2 rounded-lg cursor-pointer hover:border-blue-700"
                            wire:click="action({'id': {{ $summaries->id }}}, 'terms_view')">

                            <div class="bg-white rounded-lg">
                                <div class="w-full h-[30rem]">
                                <img class="w-full h-[30rem]  rounded-lg border border-[#003399]" {{-- src="https://flowbite.s3.amazonaws.com/docs/gallery/square/image.jpg" alt=""> --}}
                                    src="{{ $summaries->Salesattachments[0]->extension != 'txt' ? Storage::disk('crm_gcs')->url($summaries->Salesattachments[0]->path . $summaries->Salesattachments[0]->name) : 'https://static.wikia.nocookie.net/logopedia/images/c/c4/Notepad_Vista_10.png' }}"
                                    alt="">
                                </div>
                                <div class="flex justify-between ml-4 mr-5 space-x-1">
                                    <div class="mt-3"><span class="text-xl font-semibold">{{ $summaries->name }}</span>
                                    </div>
                                    <div class="mt-3">
                                        @if ($summaries->discount_type == 1)
                                            <span
                                                class="text-2xl font-semibold text-blue">{{ '₱' . $summaries->discount_amount }}</span>
                                        @else
                                            <span
                                                class="text-2xl font-semibold text-blue">{{ $summaries->discount_percentage . '%' }}</span>
                                        @endif

                                    </div>
                                </div>
                                <div class="flex justify-between ml-4 mr-5">
                                    <div><span class="text-gray-400">{{ $summaries->voucher_code }}</span></div>
                                    <div><span class="text-xs text-gray-400">Discount</span></div>
                                </div>
                                <div class="flex justify-center mt-4 mb-4">
                                    <hr
                                        style="width:90%;text-align:left;margin-left:0;height:2px;border-width:0;color:lightgray;background-color:lightgray">
                                </div>
                                <div class="flex justify-between ml-4 mr-5">
                                    <div class="">
                                        <span>Usage</span>
                                    </div>
                                </div>
                                <div class="flex justify-center ml-4 mr-5">
                                    <div class="w-full border border-[#7F7F7F] rounded-lg h-6">
                                        <div class="h-6 rounded-lg" {{-- style="width:50%;background: linear-gradient(to right, #ff872c 0%, #90ee91 100%)"> --}} {{-- style="width: {{ $summaries->maximum_usage <= 0 ? 0 : round((30 / $summaries->maximum_usage) * 100, 2) }}% ;background: linear-gradient(to right, #ff872c 0%, #90ee91 100%)"> --}}
                                            style="width: {{ $summaries->maximum_usage <= 0 ? 0 : (30 / $summaries->maximum_usage) * 100 }}% ;background: linear-gradient(to right, #ff872c 0%, #90ee91 100%)">

                                        </div>
                                    </div>
                                </div>
                                <div class="flex justify-between mb-6 ml-4 mr-5">
                                    <div><span class="text-xs">30/{{ $summaries->maximum_usage }}</span></div>
                                    <div><span
                                            class="text-xs">{{ $summaries->maximum_usage <= 0 ? 0 : round((30 / $summaries->maximum_usage) * 100, 2) }}%</span>
                                    </div>
                                </div>
                                <div class="flex justify-between ml-4 mr-5">
                                    <div class="mb-3"><span
                                            class="text-xs italic text-gray-400 whitespace-nowrap">{{ date('M d, Y', strtotime($summaries->start_datetime)) }}
                                            - {{ date('M d, Y', strtotime($summaries->end_datetime)) }}</span>
                                    </div>
                                    @if ($summaries->status == 1)
                                        <div class="mb-3"><span
                                                class="py-1 mb-4 text-xs font-bold text-[#53F140] bg-[#E7FDE5] rounded-full px-7">Active</span>
                                        </div>
                                    @elseif($summaries->status == 2)
                                        <div class="mb-3"><span
                                                class="px-6 py-1 text-xs font-bold text-[#FF1414] bg-[#FFE5E5] rounded-full">Inactive</span>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endcan
                @endforeach
            </div>

    </x-slot>
    <div class="px-1 pb-2">
        {{ $summary->links() }}
    </div>
</x-form>
