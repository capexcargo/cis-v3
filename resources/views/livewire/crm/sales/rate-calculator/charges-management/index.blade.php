<x-form x-data="{
    search_form: false,
    create_modal: '{{ $create_modal }}',
    edit_modal: '{{ $edit_modal }}',
    update_status_modal: '{{ $update_status_modal }}',
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @can('crm_sales_charges_management_add')
            <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
                <x-slot name="title">Add Charges</x-slot>
                <x-slot name="body">
                    @livewire('crm.sales.rate-calculator.charges-management.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('crm_sales_charges_management_edit')
            @if ($charge_id && $edit_modal)
                <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
                    <x-slot name="title">Edit Charges</x-slot>
                    <x-slot name="body">
                        @livewire('crm.sales.rate-calculator.charges-management.edit', ['id' => $charge_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        @can('crm_sales_charges_management_updatestatus')
            <x-modal id="update_status_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-2/5">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center">
                        <h2 class="text-xl text-center">
                            {{ $confirmation_message }}
                        </h2>
                        <div class="flex justify-center space-x-1">
                            <button wire:click="$set('update_status_modal', false)"
                                class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">NO</button>
                            @if ($status == 2)
                                <button wire:click="confirm"
                                    class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-red-600">Yes,
                                    Deactivate</button>
                            @else
                                <button wire:click="confirm"
                                    class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">Yes,
                                    Activate</button>
                            @endif
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endcan
    </x-slot>
    <x-slot name="header_title">Charges Management</x-slot>
    <x-slot name="header_button">
        @can('crm_sales_charges_management_add')
            <x-button type="button" wire:click="$set('create_modal', true)" title="Add Charges"
                class="bg-blue text-white hover:bg-[#002161]">
                <x-slot name="icon">
                    <svg class="w-3 h-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                </x-slot>
            </x-button>
        @endcan
    </x-slot>
    <x-slot name="body">
        <div>
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table>
                    <x-slot name="thead">
                        <x-table.th name="No." />
                        <x-table.th name="Rate Calculator Charges" />
                        <x-table.th name="Charges Category" />
                        <x-table.th name="Rate Status" />
                        <x-table.th name="Action" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($charges_list as $charges)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                <td class="p-3 whitespace-nowrap">
                                    {{ ($charges_list->currentPage() - 1) * $charges_list->links()->paginator->perPage() + $loop->iteration }}.
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $charges->rate_calcu_charges }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $charges->charges_category == 1 ? 'Base Charge' : 'Other Fee' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <span
                                        class="{{ $charges->status == 1
                                            ? 'text-green-500 bg-green-100 px-8 py-1 text-xs rounded-full'
                                            : 'text-red-500 bg-red-100 px-8 py-1 text-xs rounded-full' }}">
                                        {{ $charges->status == 1 ? 'Active' : 'Inactive' }}
                                    </span>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <div class="flex space-x-3">
                                        @can('crm_sales_charges_management_edit')
                                            <svg wire:click="action({'id': {{ $charges->id }}}, 'edit')"
                                                class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                data-prefix="far" data-icon="edit" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                <path fill="currentColor"
                                                    d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                                </path>
                                            </svg>
                                        @endcan
                                        @can('crm_sales_charges_management_updatestatus')
                                            @if ($charges->status == 2)
                                                <svg wire:click="action({'id':{{ $charges->id }},'status':{{ $charges->status }}},'update_status') }}"
                                                    class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                    data-prefix="fas" data-icon="trash-alt" role="img"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                    <path fill="currentColor"
                                                        d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z">
                                                    </path>
                                                </svg>
                                            @else
                                                <svg wire:click="action({'id': {{ $charges->id }},'status':{{ $charges->status }}}, 'update_status')"
                                                    class="w-5 h-5 text-red" aria-hidden="true" focusable="false"
                                                    data-prefix="far" data-icon="delete" role="img"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                    <path fill="currentColor"
                                                        d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM175 175c9.4-9.4 24.6-9.4 33.9 0l47 47 47-47c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9l-47 47 47 47c9.4 9.4 9.4 24.6 0 33.9s-24.6 9.4-33.9 0l-47-47-47 47c-9.4 9.4-24.6 9.4-33.9 0s-9.4-24.6 0-33.9l47-47-47-47c-9.4-9.4-9.4-24.6 0-33.9z">
                                                    </path>
                                                </svg>
                                            @endif
                                        @endcan
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $charges_list->links() }}
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
