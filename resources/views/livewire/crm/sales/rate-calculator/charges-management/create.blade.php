<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
}">
    <x-loading></x-loading>

    <x-modal id="confirmation_modal" size="w-auto">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="text-xl text-left">
                Are you sure you want to submit this new rate calculator charges?
            </h2>
            <div class="flex justify-center space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                    NO
                </button>
                <button type="button" wire:click="submit"
                    class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                    Yes, Submit
                </button>
            </div>
        </x-slot>
    </x-modal>

    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">
        <div class="mt-5 space-y-3">
            <div class="grid grid-cols-5 space-x-6">
                <div class="col-span-3">
                    <x-label for="rate_calcu_charges" value="Rate Calculator Charges" :required="true" />
                    <x-input type="text" name="rate_calcu_charges" wire:model.defer='rate_calcu_charges'></x-input>
                    <x-input-error for="rate_calcu_charges" />
                </div>
                <div class="col-span-2">
                    <x-label for="charges_category" value="Charges Category" :required="true" />
                    <x-select type="text" name="charges_category" wire:model.defer='charges_category'>
                        <option value="">Select</option>
                        <option value="1">Base Charge</option>
                        <option value="2">Other Fee</option>
                    </x-select>
                    <x-input-error for="charges_category" />
                </div>
            </div>
        </div>
        <div class="flex justify-end gap-3 pt-6 space-x-3 mt-4">
            <button type="button" wire:click="closeCreateModal"
                class="px-12 py-2 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-md  hover:bg-gray-200">
                Cancel
            </button>
            <button type="submit"
                class="px-12 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg hover:bg-[#00246B]">
                Submit
            </button>
        </div>
    </form>
</div>
