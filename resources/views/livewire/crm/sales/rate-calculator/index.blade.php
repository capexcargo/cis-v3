<x-form wire:init="" x-data="{
    search_form: false,
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>

    <x-slot name="modals">
    </x-slot>

    <x-slot name="header_title">Rate Calculator</x-slot>
    <x-slot name="header_button">
        <button wire:click="action({}, '')" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
            Charges Management
        </button>
    </x-slot>
    <x-slot name="body">
        <div>
            <div class="grid grid-cols-1 gap-4 mt-4">
                <div class="col-span-2">
                    <div class="grid grid-cols-10 gap-6">
                        <div class="col-span-9">
                            <ul class="flex mt-2">
                                @foreach ($type_header_cards as $i => $type_card)
                                    <li
                                        class="px-3 py-2 text-xs {{ $type_header_cards[$i]['class'] }} {{ $type == $type_header_cards[$i]['id'] ? 'bg-blue text-white' : '' }}">
                                        <button wire:click="ActiveType('{{ $type_header_cards[$i]['id'] }}')">
                                            {{ $type_header_cards[$i]['title'] }}
                                        </button>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            @if ($type == 1)
                <div class="grid grid-cols-12 gap-5">
                    <div class="col-span-8">
                        <div class="space-y-4">
                            <div class="grid grid-cols-1 px-12 pb-6 pr-24 bg-white rounded-lg shadow-lg"
                                style="height:295px;">
                                <div class="mt-6">
                                    <x-label for="transport_mode" value="Transport Mode" :required="true" />
                                    <x-select class="w-full rounded-md cursor-pointer h-11" name="transport_mode"
                                        wire:model='transport_mode'>
                                        <option value="">Select</option>
                                        @foreach ($transport_mode_references as $transport_mode_ref)
                                            <option value="{{ $transport_mode_ref->id }}">
                                                {{ $transport_mode_ref->name }}
                                            </option>
                                        @endforeach
                                    </x-select>
                                    <x-input-error for="transport_mode" />
                                </div>
                                <div class="grid grid-cols-2 gap-8">
                                    <div>
                                        <x-label for="origin" value="Origin" :required="true" />
                                        <select class="w-full rounded-md cursor-pointer h-11" name="origin"
                                            wire:model='origin' @if ($transport_mode == null) disabled @endif>
                                            <option value="">Select</option>
                                            @foreach ($origin_references as $origin_ref)
                                                <option value="{{ $origin_ref->id }}">
                                                    {{ $origin_ref->display }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <x-input-error for="origin" />
                                    </div>
                                    <div>
                                        <x-label for="destination" value="Destination" :required="true" />
                                        <select class="w-full rounded-md cursor-pointer h-11" name="destination"
                                            wire:model='destination' @if ($transport_mode == null) disabled @endif>
                                            <option value="">Select</option>
                                            @foreach ($destination_references as $destination_ref)
                                                <option value="{{ $destination_ref->id }}">
                                                    {{ $destination_ref->display }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <x-input-error for="destination" />
                                    </div>
                                </div>
                                <div class="grid grid-cols-2 gap-8">
                                    <div>
                                        <x-label for="transhipment" value="Transhipment" />
                                        <select class="w-full rounded-md cursor-pointer h-11" name="transhipment"
                                            wire:model='transhipment' @if ($transport_mode == null) disabled @endif>
                                            <option value="">Select</option>
                                            {{-- @foreach ($transhipment_references as $transhipment_ref)
                                                <option value="{{ $transhipment_ref->id }}">
                                                    {{ $transhipment_ref->name }}
                                                </option>
                                            @endforeach --}}
                                            @foreach ($destination_references as $destination_ref)
                                                <option value="{{ $destination_ref->id }}">
                                                    {{ $destination_ref->display }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <x-input-error for="transhipment" />
                                    </div>
                                </div>
                            </div>
                            <div class="grid grid-cols-1 px-12 pb-6 pr-24 space-y-4 bg-white rounded-lg shadow-lg">
                                <div class="mt-6">
                                    <x-label for="declared_value" value="Declared Value" />
                                    <input class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                        name="declared_value" wire:model='declared_value'
                                        @if ($transport_mode == null) disabled @endif>
                                    <x-input-error for="declared_value" />
                                </div>
                                <div class="grid grid-cols-2 gap-8">
                                    <div>
                                        <x-label for="commodity_type" value="Commodity Type" />
                                        <select class="w-full rounded-md cursor-pointer h-11" name="commodity_type"
                                            wire:model='commodity_type'
                                            @if ($transport_mode == null) disabled @endif>
                                            <option value="">Select</option>
                                            @foreach ($commodity_type_references as $commodity_type_ref)
                                                <option value="{{ $commodity_type_ref->id }}">
                                                    {{ $commodity_type_ref->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <x-input-error for="commodity_type" />
                                    </div>
                                    <div>
                                        <x-label for="commodity_applicable_rate" value="Commodity Applicable Rate" />
                                        <select class="w-full rounded-md cursor-pointer h-11"
                                            name="commodity_applicable_rate" wire:model='commodity_applicable_rate'
                                            @if ($transport_mode == null) disabled @endif>
                                            <option value="">Select</option>
                                            @foreach ($commodity_applicable_rate_references as $commodity_applicable_rate_ref)
                                                <option value="{{ $commodity_applicable_rate_ref->id }}">
                                                    {{ $commodity_applicable_rate_ref->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <x-input-error for="commodity_applicable_rate" />
                                    </div>
                                </div>
                                <div class="grid grid-cols-2 gap-8">
                                    <div>
                                        <x-label for="paymode" value="Paymode" :required="true" />
                                        <select class="w-full rounded-md cursor-pointer h-11" name="paymode"
                                            wire:model='paymode' @if ($transport_mode == null) disabled @endif>
                                            <option value="">Select</option>
                                            @foreach ($paymode_references as $paymode_ref)
                                                <option value="{{ $paymode_ref->id }}">
                                                    {{ $paymode_ref->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <x-input-error for="paymode" />
                                    </div>
                                    <div>
                                        <x-label for="service_mode" value="Service Mode" :required="true" />
                                        <select class="w-full rounded-md cursor-pointer h-11" name="service_mode"
                                            wire:model='service_mode' @if ($transport_mode == null) disabled @endif>
                                            <option value="">Select</option>
                                            @foreach ($service_mode_references as $service_mode_ref)
                                                <option value="{{ $service_mode_ref->id }}">
                                                    {{ $service_mode_ref->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <x-input-error for="service_mode" />
                                    </div>
                                </div>
                                {{-- for Transport Mode AIR --}}
                                @if ($transport_mode == 1)
                                    <div class="my-6">
                                        <div>
                                            <div class="flex gap-2 mt-4 font-medium text-gray-500">
                                                <div class="mt-[-8px]">
                                                    <input type="checkbox"
                                                        class="w-6 h-6 rounded-md border-[#003399] border cursor-pointer"
                                                        name="air_cargo" wire:model='air_cargo'
                                                        @if ($air_cargo) checked @endif>
                                                </div>
                                                <div class="-mt-2 text-lg">Air Cargo</div>
                                            </div>
                                            @if ($air_cargo)
                                                <div class="flex px-2.5 mt-2">
                                                    <div class="overflow-auto border-l-4 border-[#003399]">
                                                        <table class="text-md text-[#003399] ml-4">
                                                            <thead>
                                                                <tr class="text-left">
                                                                    <th class="w-10 px-1">
                                                                        <x-label value="Qty" class="text-xs" />
                                                                    </th>
                                                                    <th class="w-10 px-1">
                                                                        <x-label value="Wt" class="text-xs" />
                                                                    </th>
                                                                    <th class="w-20 px-1">
                                                                        <x-label value="Dimension (LxWxH)"
                                                                            class="text-xs" />
                                                                    </th>
                                                                    <th class="w-10 px-1">
                                                                        <x-label value="Unit of Measurement"
                                                                            class="text-xs" />
                                                                    </th>
                                                                    <th class="w-[20px] px-1">
                                                                        <x-label value="Measurement Type"
                                                                            class="text-xs" />
                                                                    </th>
                                                                    <th class="w-[20px] px-1">
                                                                        <x-label value="Type of Packaging"
                                                                            class="text-xs" />
                                                                    </th>
                                                                    <th class="w-10 px-1">
                                                                        <x-label value="For Crating"
                                                                            class="text-xs" />
                                                                    </th>
                                                                    <th class="w-10 px-1">
                                                                        <x-label value="Crating Type"
                                                                            class="text-xs" />
                                                                    </th>
                                                                    <th class="w-10 px-1">
                                                                        <x-label value="CWT" class="text-xs" />
                                                                    </th>
                                                                    <th class="px-2"></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach ($air_cargos as $i => $ac)
                                                                    <tr class="">
                                                                        <td class="px-1">
                                                                            <div class="-mt-2">
                                                                                <input type="text"
                                                                                    class="w-6 h-6 p-1 text-xs border rounded-md cursor-pointer"
                                                                                    name="air_cargos.{{ $i }}.qty"
                                                                                    wire:model.defer='air_cargos.{{ $i }}.qty'>
                                                                            </div>
                                                                        </td>
                                                                        <td class="px-1">
                                                                            <div class="-mt-2">
                                                                                <input type="text"
                                                                                    class="w-6 h-6 p-1 text-xs border rounded-md cursor-pointer"
                                                                                    name="air_cargos.{{ $i }}.wt"
                                                                                    wire:model.defer='air_cargos.{{ $i }}.wt'
                                                                                    wire:change="forCWT_CBM({{ $transport_mode }})">
                                                                            </div>
                                                                        </td>
                                                                        <td class="px-1">
                                                                            <div class="-mt-2">
                                                                                <div class="flex gap-2">
                                                                                    <input type="text"
                                                                                        class="w-6 h-6 p-1 text-xs border rounded-md cursor-pointer"
                                                                                        name="air_cargos.{{ $i }}.dimension_l"
                                                                                        wire:model.defer='air_cargos.{{ $i }}.dimension_l'
                                                                                        wire:change="forCWT_CBM({{ $transport_mode }})">
                                                                                    <input type="text"
                                                                                        class="w-6 h-6 p-1 text-xs border rounded-md cursor-pointer"
                                                                                        name="air_cargos.{{ $i }}.dimension_w"
                                                                                        wire:model.defer='air_cargos.{{ $i }}.dimension_w'
                                                                                        wire:change="forCWT_CBM({{ $transport_mode }})">
                                                                                    <input type="text"
                                                                                        class="w-6 h-6 p-1 text-xs border rounded-md cursor-pointer"
                                                                                        name="air_cargos.{{ $i }}.dimension_h"
                                                                                        wire:model.defer='air_cargos.{{ $i }}.dimension_h'
                                                                                        wire:change="forCWT_CBM({{ $transport_mode }})">
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="px-1">
                                                                            <div
                                                                                class="w-full -mt-2 whitespace-normal">
                                                                                <select
                                                                                    class="h-6 p-0 pl-2 text-xs border rounded-md cursor-pointer"
                                                                                    name="air_cargos.{{ $i }}.unit_of_measurement"
                                                                                    wire:model='air_cargos.{{ $i }}.unit_of_measurement'>
                                                                                    <option value="">Select
                                                                                    </option>
                                                                                    <option value="1">mm</option>
                                                                                    <option value="2">cm</option>
                                                                                    <option value="3">inches
                                                                                    </option>
                                                                                    <option value="4">m</option>
                                                                                    <option value="5">ft</option>
                                                                                    <option value="6">yard
                                                                                    </option>
                                                                                </select>
                                                                            </div>
                                                                        </td>
                                                                        <td class="px-1">
                                                                            <div
                                                                                class="w-full -mt-2 whitespace-normal">
                                                                                <select
                                                                                    class="h-6 p-0 pl-2 text-xs border rounded-md cursor-pointer"
                                                                                    name="air_cargos.{{ $i }}.measurement_type"
                                                                                    wire:model='air_cargos.{{ $i }}.measurement_type'>
                                                                                    <option value="">Select
                                                                                    </option>
                                                                                    <option value="1">Lot</option>
                                                                                    <option value="2">Per piece
                                                                                    </option>
                                                                                </select>
                                                                            </div>
                                                                        </td>
                                                                        <td class="px-1">
                                                                            <div
                                                                                class="w-full -mt-2 whitespace-normal">
                                                                                <select
                                                                                    class="h-6 p-0 pl-2 text-xs border rounded-md cursor-pointer"
                                                                                    name="air_cargos.{{ $i }}.type_of_packaging"
                                                                                    wire:model='air_cargos.{{ $i }}.type_of_packaging'>
                                                                                    <option value="">Select
                                                                                    </option>
                                                                                    <option value="1">Box</option>
                                                                                    <option value="2">Plastic
                                                                                    </option>
                                                                                    <option value="3">Crate
                                                                                    </option>
                                                                                </select>
                                                                            </div>
                                                                        </td>
                                                                        <td class="px-1">
                                                                            <div class="-mt-2 whitespace-normal">
                                                                                <div class="-mt-2">
                                                                                    @if ($air_cargos[$i]['for_crating'] == true)
                                                                                        <svg wire:click="isForCreating({{ $i }}, 1, false)"
                                                                                            class="w-10 h-10 py-1 rounded-full text-green"
                                                                                            aria-hidden="true"
                                                                                            focusable="false"
                                                                                            data-prefix="fas"
                                                                                            data-icon="user-slash"
                                                                                            role="img"
                                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                                            viewBox="0 0 680 510">
                                                                                            <path fill="currentColor"
                                                                                                d="M192 64C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192s-86-192-192-192H192zM384 352c-53 0-96-43-96-96s43-96 96-96s96 43 96 96s-43 96-96 96z">
                                                                                            </path>
                                                                                        </svg>
                                                                                    @else
                                                                                        <svg wire:click="isForCreating({{ $i }}, 1, true)"
                                                                                            class="w-10 h-10 py-1 text-gray-400 rounded-full"
                                                                                            aria-hidden="true"
                                                                                            focusable="false"
                                                                                            data-prefix="fas"
                                                                                            data-icon="user-slash"
                                                                                            role="img"
                                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                                            viewBox="0 0 680 510">
                                                                                            <path fill="currentColor"
                                                                                                d="M384 128c70.7 0 128 57.3 128 128s-57.3 128-128 128H192c-70.7 0-128-57.3-128-128s57.3-128 128-128H384zM576 256c0-106-86-192-192-192H192C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192zM192 352c53 0 96-43 96-96s-43-96-96-96s-96 43-96 96s43 96 96 96z">
                                                                                            </path>
                                                                                        </svg>
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="px-1">
                                                                            <div class="-mt-2">
                                                                                <select
                                                                                    class="w-[20px] h-6 p-0 px-2 text-xs border rounded-md cursor-pointer"
                                                                                    name="air_cargos.{{ $i }}.crating_type"
                                                                                    wire:model='air_cargos.{{ $i }}.crating_type'
                                                                                    @if ($air_cargos[$i]['for_crating'] == false) disabled @endif>
                                                                                    <option value="">Select
                                                                                    </option>
                                                                                    @foreach ($crating_type_references as $crating_type_ref)
                                                                                        <option
                                                                                            value="{{ $crating_type_ref->id }}">
                                                                                            {{ $crating_type_ref->name }}
                                                                                        </option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </td>
                                                                        <td class="px-1">
                                                                            <div class="-mt-2">
                                                                                <input type="text"
                                                                                    class="w-10 h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                    name="air_cargos.{{ $i }}.cwt"
                                                                                    wire:model.defer='air_cargos.{{ $i }}.cwt'
                                                                                    disabled>
                                                                            </div>
                                                                        </td>
                                                                        <td class="px-2">
                                                                            <div class="flex gap-2 -mt-2">
                                                                                @if (count($air_cargos) > 1)
                                                                                    <button type="button"
                                                                                        title="Remove"
                                                                                        wire:click="removeAirCargos({{ $i }})"
                                                                                        class="w-5 h-5 px-1 pr-1.5 -pt-0.5 text-xs flex-none bg-red-600 text-white rounded-full">
                                                                                        -</button>
                                                                                @endif
                                                                                @if (count($air_cargos) == ($i += 1))
                                                                                    <button type="button"
                                                                                        title=""
                                                                                        wire:click="addAirCargos"
                                                                                        class="w-5 h-5 px-1 pr-1.5 -pt-0.5 text-xs flex-none bg-[#003399] text-white rounded-full">
                                                                                        +
                                                                                    </button>
                                                                                @endif
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                        <div>
                                            <div class="flex gap-2 mt-4 font-medium text-gray-500">
                                                <div class="mt-[-8px]">
                                                    <input type="checkbox"
                                                        class="w-6 h-6 rounded-md border-[#003399] border cursor-pointer"
                                                        name="air_pouch" wire:model='air_pouch'>
                                                </div>
                                                <div class="-mt-2 text-lg">Air Pouch</div>
                                            </div>
                                            @if ($air_pouch)
                                                <div class="flex px-2.5 mt-2">
                                                    <div class="overflow-auto border-l-4 border-[#003399]">
                                                        <table class="text-md text-[#003399] ml-4">
                                                            <thead>
                                                                <tr class="text-left">
                                                                    <th class="w-12 px-3">
                                                                        <x-label value="Qty" class="text-xs" />
                                                                    </th>
                                                                    <th class="w-24 px-3">
                                                                        <x-label value="Pouch Size" class="text-xs" />
                                                                    </th>
                                                                    <th class="w-24 px-3">
                                                                        <x-label value="Amount" class="text-xs" />
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach ($air_pouches as $i => $ap)
                                                                    <tr class="mt-4">
                                                                        <td class="px-2">
                                                                            <div class="mb-2">
                                                                                <input type="text"
                                                                                    class="w-10 h-6 p-1 text-xs border rounded-md cursor-pointer"
                                                                                    name="air_pouches.{{ $i }}.qty"
                                                                                    wire:model.defer='air_pouches.{{ $i }}.qty'>
                                                                            </div>
                                                                        </td>
                                                                        <td class="px-2">
                                                                            <div class="w-24 mb-2 whitespace-normal">
                                                                                <select
                                                                                    class="w-24 h-6 p-0 px-2 text-xs border rounded-md cursor-pointer"
                                                                                    name="air_pouches.{{ $i }}.pouch_size"
                                                                                    wire:model='air_pouches.{{ $i }}.pouch_size'
                                                                                    wire:change="forAirPouchSize">
                                                                                    <option value="">Select
                                                                                    </option>
                                                                                    <option value="1">Small
                                                                                    </option>
                                                                                    <option value="2">Medium
                                                                                    </option>
                                                                                    <option value="3">Large
                                                                                    </option>
                                                                                </select>
                                                                            </div>
                                                                        </td>
                                                                        <td class="px-2">
                                                                            <div class="w-24 mb-2 whitespace-normal">
                                                                                <input type="text"
                                                                                    class="w-24 h-6 p-0 px-2 text-xs border rounded-md cursor-pointer"
                                                                                    name="air_pouches.{{ $i }}.amount"
                                                                                    wire:model.defer='air_pouches.{{ $i }}.amount'
                                                                                    disabled>
                                                                            </div>
                                                                        </td>
                                                                        <td class="px-2">
                                                                            <div class="flex gap-2 -mt-2">
                                                                                @if (count($air_pouches) > 1)
                                                                                    <button type="button"
                                                                                        title="Remove"
                                                                                        wire:click="removeAirPouches({{ $i }})"
                                                                                        class="w-5 h-5 px-1 pr-1.5 -pt-0.5 text-xs flex-none bg-red-600 text-white rounded-full">
                                                                                        -</button>
                                                                                @endif
                                                                                @if (count($air_pouches) == ($i += 1))
                                                                                    <button type="button"
                                                                                        title=""
                                                                                        wire:click="addAirPouches"
                                                                                        class="w-5 h-5 px-1 pr-1.5 -pt-0.5 text-xs flex-none bg-[#003399] text-white rounded-full">
                                                                                        +
                                                                                    </button>
                                                                                @endif
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                        <div>
                                            <div class="flex gap-2 mt-4 font-medium text-gray-500">
                                                <div class="mt-[-8px]">
                                                    <input type="checkbox"
                                                        class="w-6 h-6 rounded-md border-[#003399] border cursor-pointer"
                                                        name="air_box" wire:model='air_box'>
                                                </div>
                                                <div class="-mt-2 text-lg">Air Box</div>
                                            </div>
                                            @if ($air_box)
                                                <div class="flex px-2.5 mt-2">
                                                    <div class="overflow-auto border-l-4 border-[#003399]">
                                                        <table class="text-md text-[#003399] ml-4">
                                                            <thead>
                                                                <tr class="text-left">
                                                                    <th class="w-12 px-3">
                                                                        <x-label value="Qty" class="text-xs" />
                                                                    </th>
                                                                    <th class="w-24 px-3">
                                                                        <x-label value="Box Size" class="text-xs" />
                                                                    </th>
                                                                    <th class="w-24 px-3">
                                                                        <x-label value="Weight (KG)"
                                                                            class="text-xs" />
                                                                    </th>
                                                                    <th class="w-24 px-3">
                                                                        <x-label value="Amount" class="text-xs" />
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach ($air_boxes as $i => $ab)
                                                                    <tr class="mt-4">
                                                                        <td class="px-2">
                                                                            <div class="mb-2">
                                                                                <input type="text"
                                                                                    class="w-10 h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                    name="air_boxes.{{ $i }}.qty"
                                                                                    wire:model.defer='air_boxes.{{ $i }}.qty'>
                                                                            </div>
                                                                        </td>
                                                                        <td class="px-2">
                                                                            <div class="w-24 mb-2 whitespace-normal">
                                                                                <select
                                                                                    class="w-24 h-6 p-0 px-2 text-xs border rounded-md cursor-pointer"
                                                                                    name="air_boxes.{{ $i }}.box_size"
                                                                                    wire:model='air_boxes.{{ $i }}.box_size'
                                                                                    wire:change="forBoxWeight({{ $transport_mode }})">
                                                                                    <option value="">Select
                                                                                    </option>
                                                                                    <option value="1">Small
                                                                                    </option>
                                                                                    <option value="2">Medium
                                                                                    </option>
                                                                                    <option value="2">Large
                                                                                    </option>
                                                                                </select>
                                                                            </div>
                                                                        </td>
                                                                        <td class="px-2">
                                                                            <div class="w-24 mb-2 whitespace-normal">
                                                                                <input type="text"
                                                                                    class="w-24 h-6 p-0 px-2 text-xs border rounded-md cursor-pointer"
                                                                                    name="air_boxes.{{ $i }}.weight"
                                                                                    wire:model.defer='air_boxes.{{ $i }}.weight'
                                                                                    wire:change="forBoxWeight({{ $transport_mode }})">
                                                                            </div>
                                                                        </td>
                                                                        <td class="px-2">
                                                                            <div class="w-24 mb-2 whitespace-normal">
                                                                                <input type="text"
                                                                                    class="w-24 h-6 p-0 px-2 text-xs border rounded-md cursor-pointer"
                                                                                    name="air_boxes.{{ $i }}.amount"
                                                                                    wire:model.defer='air_boxes.{{ $i }}.amount'
                                                                                    disabled>
                                                                            </div>
                                                                        </td>
                                                                        <td class="px-2">
                                                                            <div class="flex gap-2 -mt-2">
                                                                                @if (count($air_boxes) > 1)
                                                                                    <button type="button"
                                                                                        title="Remove Air Box"
                                                                                        wire:click="removeAirBoxes({{ $i }})"
                                                                                        class="w-5 h-5 px-1 pr-1.5 -pt-0.5 text-xs flex-none bg-red-600 text-white rounded-full">
                                                                                        -</button>
                                                                                @endif
                                                                                @if (count($air_boxes) == ($i += 1))
                                                                                    <button type="button"
                                                                                        title=""
                                                                                        wire:click="addAirBoxes"
                                                                                        class="w-5 h-5 px-1 pr-1.5 -pt-0.5 text-xs flex-none bg-[#003399] text-white rounded-full">
                                                                                        +
                                                                                    </button>
                                                                                @endif
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endif
                                {{-- for Transport Mode SEA --}}
                                @if ($transport_mode == 2)
                                    <div class="my-6">
                                        <div>
                                            <div class="flex gap-2 mt-4 font-medium text-gray-500">
                                                <div class="mt-[-8px]">
                                                    <input type="checkbox"
                                                        class="w-6 h-6 rounded-md border-[#003399] border cursor-pointer"
                                                        name="sea_cargo" wire:model='sea_cargo'
                                                        @if ($sea_cargo) checked @endif>
                                                </div>
                                                <div class="-mt-2 text-lg">Sea Cargo</div>
                                            </div>
                                            @if ($sea_cargo)
                                                <div class="flex px-2.5 mt-2">
                                                    <div class="overflow-auto border-l-4 border-[#003399]">
                                                        <div
                                                            class="flex gap-6 mt-1 mb-4 ml-6 text-xs font-medium text-gray-500">
                                                            @foreach ($movement_sf_type_references as $i => $sf_type)
                                                                <div class="flex gap-1">
                                                                    <input type="radio"
                                                                        class="border-[#003399] border"
                                                                        name="movement_sf_type"
                                                                        wire:model='movement_sf_type'
                                                                        value="{{ $movement_sf_type_references[$i]['id'] }}">
                                                                    {{ $movement_sf_type_references[$i]['name'] }}
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                        @if ($movement_sf_type == 1)
                                                            <table class="text-md text-[#003399] ml-6">
                                                                <thead>
                                                                    <tr class="text-left">
                                                                        <th class="w-12 px-2">
                                                                            <x-label value="Qty" class="text-xs" />
                                                                        </th>
                                                                        <th class="w-12 px-2">
                                                                            <x-label value="Wt" class="text-xs" />
                                                                        </th>
                                                                        <th class="w-24 px-1 whitespace-nowrap">
                                                                            <x-label value="Dimension (LxWxH)"
                                                                                class="text-xs" />
                                                                        </th>
                                                                        <th class="w-24 px-2">
                                                                            <x-label value="Unit of Measurement"
                                                                                class="text-xs" />
                                                                        </th>
                                                                        <th class="w-24 px-2">
                                                                            <x-label value="Measurement Type"
                                                                                class="text-xs" />
                                                                        </th>
                                                                        <th class="w-24 px-2">
                                                                            <x-label value="Type of Packaging"
                                                                                class="text-xs" />
                                                                        </th>
                                                                        <th class="w-12 px-2">
                                                                            <x-label value="For Crating"
                                                                                class="text-xs" />
                                                                        </th>
                                                                        <th class="w-12 px-2">
                                                                            <x-label value="Crating Type"
                                                                                class="text-xs" />
                                                                        </th>
                                                                        <th class="w-12 px-2">
                                                                            <x-label value="CBM" class="text-xs" />
                                                                        </th>
                                                                        <th class="px-2"></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @foreach ($sea_cargos_is_lcl as $i => $sc)
                                                                        <tr class="">
                                                                            <td class="px-1">
                                                                                <div class="-mt-2">
                                                                                    <input type="text"
                                                                                        class="w-16 h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="sea_cargos_is_lcl.{{ $i }}.qty"
                                                                                        wire:model.defer='sea_cargos_is_lcl.{{ $i }}.qty'
                                                                                        wire:change="forCWT_CBM({{ $transport_mode }})">
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-1">
                                                                                <div class="-mt-2">
                                                                                    <input type="text"
                                                                                        class="w-16 h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="sea_cargos_is_lcl.{{ $i }}.wt"
                                                                                        wire:model.defer='sea_cargos_is_lcl.{{ $i }}.wt'
                                                                                        wire:change="forCWT_CBM({{ $transport_mode }})">
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-1">
                                                                                <div class="-mt-2">
                                                                                    <div class="flex gap-2">
                                                                                        <input type="text"
                                                                                            class="w-16 h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                            name="sea_cargos_is_lcl.{{ $i }}.dimension_l"
                                                                                            wire:model.defer='sea_cargos_is_lcl.{{ $i }}.dimension_l'
                                                                                            wire:change="forCWT_CBM({{ $transport_mode }})">
                                                                                        <input type="text"
                                                                                            class="w-16 h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                            name="sea_cargos_is_lcl.{{ $i }}.dimension_w"
                                                                                            wire:model.defer='sea_cargos_is_lcl.{{ $i }}.dimension_w'
                                                                                            wire:change="forCWT_CBM({{ $transport_mode }})">
                                                                                        <input type="text"
                                                                                            class="w-16 h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                            name="sea_cargos_is_lcl.{{ $i }}.dimension_h"
                                                                                            wire:model.defer='sea_cargos_is_lcl.{{ $i }}.dimension_h'
                                                                                            wire:change="forCWT_CBM({{ $transport_mode }})">
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-1">
                                                                                <div
                                                                                    class="w-24 -mt-2 whitespace-normal">
                                                                                    <select
                                                                                        class="w-24 h-6 p-0 px-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="sea_cargos_is_lcl.{{ $i }}.unit_of_measurement"
                                                                                        wire:model='sea_cargos_is_lcl.{{ $i }}.unit_of_measurement'>
                                                                                        <option value="">Select
                                                                                        </option>
                                                                                        <option value="1">mm
                                                                                        </option>
                                                                                        <option value="2">cm
                                                                                        </option>
                                                                                        <option value="3">inches
                                                                                        </option>
                                                                                        <option value="4">m
                                                                                        </option>
                                                                                        <option value="5">ft
                                                                                        </option>
                                                                                        <option value="6">yard
                                                                                        </option>
                                                                                    </select>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-1">
                                                                                <div
                                                                                    class="w-24 -mt-2 whitespace-normal">
                                                                                    <select
                                                                                        class="w-24 h-6 p-0 px-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="sea_cargos_is_lcl.{{ $i }}.measurement_type"
                                                                                        wire:model='sea_cargos_is_lcl.{{ $i }}.measurement_type'>
                                                                                        <option value="">Select
                                                                                        </option>
                                                                                        <option value="1">Lot
                                                                                        </option>
                                                                                        <option value="2">Per
                                                                                            piece
                                                                                        </option>
                                                                                    </select>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-1">
                                                                                <div
                                                                                    class="w-24 -mt-2 whitespace-normal">
                                                                                    <select
                                                                                        class="w-24 h-6 p-0 px-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="sea_cargos_is_lcl.{{ $i }}.type_of_packaging"
                                                                                        wire:model='sea_cargos_is_lcl.{{ $i }}.type_of_packaging'>
                                                                                        <option value="">Select
                                                                                        </option>
                                                                                        <option value="1">Box
                                                                                        </option>
                                                                                        <option value="2">Plastic
                                                                                        </option>
                                                                                        <option value="3">Crate
                                                                                        </option>
                                                                                    </select>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-1">
                                                                                <div class="-mt-2 whitespace-normal">
                                                                                    <div class="-mt-2">
                                                                                        @if ($sea_cargos_is_lcl[$i]['for_crating'] == true)
                                                                                            <svg wire:click="isForCreating({{ $i }}, 2, false)"
                                                                                                class="py-1 rounded-full text-green"
                                                                                                aria-hidden="true"
                                                                                                focusable="false"
                                                                                                data-prefix="fas"
                                                                                                data-icon="user-slash"
                                                                                                role="img"
                                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                                viewBox="0 0 720 510">
                                                                                                <path
                                                                                                    fill="currentColor"
                                                                                                    d="M192 64C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192s-86-192-192-192H192zM384 352c-53 0-96-43-96-96s43-96 96-96s96 43 96 96s-43 96-96 96z">
                                                                                                </path>
                                                                                            </svg>
                                                                                        @else
                                                                                            <svg wire:click="isForCreating({{ $i }}, 2, true)"
                                                                                                class="py-1 text-gray-400 rounded-full"
                                                                                                aria-hidden="true"
                                                                                                focusable="false"
                                                                                                data-prefix="fas"
                                                                                                data-icon="user-slash"
                                                                                                role="img"
                                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                                viewBox="0 0 720 510">
                                                                                                <path
                                                                                                    fill="currentColor"
                                                                                                    d="M384 128c70.7 0 128 57.3 128 128s-57.3 128-128 128H192c-70.7 0-128-57.3-128-128s57.3-128 128-128H384zM576 256c0-106-86-192-192-192H192C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192zM192 352c53 0 96-43 96-96s-43-96-96-96s-96 43-96 96s43 96 96 96z">
                                                                                                </path>
                                                                                            </svg>
                                                                                        @endif
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-1">
                                                                                <div class="-mt-2">
                                                                                    <select
                                                                                        class="h-6 p-0 px-2 text-xs border rounded-md cursor-pointer w-28"
                                                                                        name="sea_cargos_is_lcl.{{ $i }}.crating_type"
                                                                                        wire:model='sea_cargos_is_lcl.{{ $i }}.crating_type'
                                                                                        @if ($sea_cargos_is_lcl[$i]['for_crating'] == false) disabled @endif>
                                                                                        <option value="">Select
                                                                                        </option>
                                                                                        @foreach ($crating_type_references as $crating_type_ref)
                                                                                            <option
                                                                                                value="{{ $crating_type_ref->id }}">
                                                                                                {{ $crating_type_ref->name }}
                                                                                            </option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-1">
                                                                                <div class="-mt-2">
                                                                                    <input type="text"
                                                                                        class="w-16 h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="sea_cargos_is_lcl.{{ $i }}.cbm"
                                                                                        wire:model.defer='sea_cargos_is_lcl.{{ $i }}.cbm'
                                                                                        disabled>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-2">
                                                                                <div class="flex gap-2 -mt-2">
                                                                                    @if (count($sea_cargos_is_lcl) > 1)
                                                                                        <button type="button"
                                                                                            title="Remove"
                                                                                            wire:click="removeSeaCargosLcl({{ $i }})"
                                                                                            class="w-5 h-5 px-1 pr-1.5 -pt-0.5 text-xs flex-none bg-red-600 text-white rounded-full">
                                                                                            -</button>
                                                                                    @endif
                                                                                    @if (count($sea_cargos_is_lcl) == ($i += 1))
                                                                                        <button type="button"
                                                                                            title=""
                                                                                            wire:click="addSeaCargosLcl"
                                                                                            class="w-5 h-5 px-1 pr-1.5 -pt-0.5 text-xs flex-none bg-[#003399] text-white rounded-full">
                                                                                            +
                                                                                        </button>
                                                                                    @endif
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        @elseif($movement_sf_type == 2)
                                                            <table class="text-md text-[#003399] ml-6 mb-4">
                                                                <thead>
                                                                    <tr class="text-left">
                                                                        <th class="w-12 px-2">
                                                                            <x-label value="Qty" />
                                                                        </th>
                                                                        <th class="w-12 px-2">
                                                                            <x-label value="Container" />
                                                                        </th>
                                                                        <th class="px-2"></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @foreach ($sea_cargos_is_fcl as $i => $sc_fcl)
                                                                        <tr class="">
                                                                            <td class="px-1">
                                                                                <div class="mb-2">
                                                                                    <input type="text"
                                                                                        class="w-10 h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="sea_cargos_is_fcl.{{ $i }}.qty"
                                                                                        wire:model.defer='sea_cargos_is_fcl.{{ $i }}.qty'>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-1">
                                                                                <div
                                                                                    class="w-24 mb-2 whitespace-normal">
                                                                                    <select
                                                                                        class="w-24 h-6 p-0 px-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="sea_cargos_is_fcl.{{ $i }}.container"
                                                                                        wire:model='sea_cargos_is_fcl.{{ $i }}.container'>
                                                                                        <option value="">Select
                                                                                        </option>
                                                                                        <option value="10">10
                                                                                        </option>
                                                                                        <option value="20">20
                                                                                        </option>
                                                                                        <option value="40">40
                                                                                        </option>
                                                                                    </select>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-2">
                                                                                <div class="flex gap-2 -mt-2">
                                                                                    @if (count($sea_cargos_is_fcl) > 1)
                                                                                        <button type="button"
                                                                                            title="Remove"
                                                                                            wire:click="removeSeaCargosFcl({{ $i }})"
                                                                                            class="w-5 h-5 px-1 pr-1.5 -pt-0.5 text-xs flex-none bg-red-600 text-white rounded-full">
                                                                                            -</button>
                                                                                    @endif
                                                                                    @if (count($sea_cargos_is_fcl) == ($i += 1))
                                                                                        <button type="button"
                                                                                            title=""
                                                                                            wire:click="addSeaCargosFcl"
                                                                                            class="w-5 h-5 px-1 pr-1.5 -pt-0.5 text-xs flex-none bg-[#003399] text-white rounded-full">
                                                                                            +
                                                                                        </button>
                                                                                    @endif
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        @elseif ($movement_sf_type == 3)
                                                            <table class="text-md text-[#003399] ml-6">
                                                                <thead>
                                                                    <tr class="text-left">
                                                                        <th class="w-12 px-2">
                                                                            <x-label value="Qty" class="text-xs" />
                                                                        </th>
                                                                        <th class="w-12 px-2">
                                                                            <x-label value="Wt" class="text-xs" />
                                                                        </th>
                                                                        <th class="w-24 px-1 whitespace-nowrap">
                                                                            <x-label value="Dimension (LxWxH)"
                                                                                class="text-xs" />
                                                                        </th>
                                                                        <th class="w-24 px-2">
                                                                            <x-label value="Unit of Measurement"
                                                                                class="text-xs" />
                                                                        </th>
                                                                        <th class="w-24 px-2">
                                                                            <x-label value="Measurement Type"
                                                                                class="text-xs" />
                                                                        </th>
                                                                        <th class="w-24 px-2">
                                                                            <x-label value="Amount" class="text-xs" />
                                                                        </th>
                                                                        <th class="w-12 px-2">
                                                                            <x-label value="For Crating"
                                                                                class="text-xs" />
                                                                        </th>
                                                                        <th class="w-12 px-2">
                                                                            <x-label value="Crating Type"
                                                                                class="text-xs" />
                                                                        </th>
                                                                        <th class="w-12 px-2">
                                                                            <x-label value="CBM" class="text-xs" />
                                                                        </th>
                                                                        <th class="px-2"></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @foreach ($sea_cargos_is_rcl as $i => $sc)
                                                                        <tr class="">
                                                                            <td class="px-1">
                                                                                <div class="-mt-2">
                                                                                    <input type="text"
                                                                                        class="w-16 h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="sea_cargos_is_rcl.{{ $i }}.qty"
                                                                                        wire:model.defer='sea_cargos_is_rcl.{{ $i }}.qty'
                                                                                        wire:change="forCWT_CBM({{ $transport_mode }})">
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-1">
                                                                                <div class="-mt-2">
                                                                                    <input type="text"
                                                                                        class="w-16 h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="sea_cargos_is_rcl.{{ $i }}.wt"
                                                                                        wire:model.defer='sea_cargos_is_rcl.{{ $i }}.wt'
                                                                                        wire:change="forCWT_CBM({{ $transport_mode }})">
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-1">
                                                                                <div class="-mt-2">
                                                                                    <div class="flex gap-2">
                                                                                        <input type="text"
                                                                                            class="w-16 h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                            name="sea_cargos_is_rcl.{{ $i }}.dimension_l"
                                                                                            wire:model.defer='sea_cargos_is_rcl.{{ $i }}.dimension_l'
                                                                                            wire:change="forCWT_CBM({{ $transport_mode }})">
                                                                                        <input type="text"
                                                                                            class="w-16 h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                            name="sea_cargos_is_rcl.{{ $i }}.dimension_w"
                                                                                            wire:model.defer='sea_cargos_is_rcl.{{ $i }}.dimension_w'
                                                                                            wire:change="forCWT_CBM({{ $transport_mode }})">
                                                                                        <input type="text"
                                                                                            class="w-16 h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                            name="sea_cargos_is_rcl.{{ $i }}.dimension_h"
                                                                                            wire:model.defer='sea_cargos_is_rcl.{{ $i }}.dimension_h'
                                                                                            wire:change="forCWT_CBM({{ $transport_mode }})">
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-1">
                                                                                <div
                                                                                    class="w-24 -mt-2 whitespace-normal">
                                                                                    <select
                                                                                        class="w-24 h-6 p-0 px-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="sea_cargos_is_rcl.{{ $i }}.unit_of_measurement"
                                                                                        wire:model='sea_cargos_is_rcl.{{ $i }}.unit_of_measurement'>
                                                                                        <option value="">
                                                                                            Select
                                                                                        </option>
                                                                                        <option value="1">mm
                                                                                        </option>
                                                                                        <option value="2">cm
                                                                                        </option>
                                                                                        <option value="3">
                                                                                            inches
                                                                                        </option>
                                                                                        <option value="4">m
                                                                                        </option>
                                                                                        <option value="5">ft
                                                                                        </option>
                                                                                        <option value="6">yard
                                                                                        </option>
                                                                                    </select>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-1">
                                                                                <div
                                                                                    class="w-24 -mt-2 whitespace-normal">
                                                                                    <select
                                                                                        class="w-24 h-6 p-0 px-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="sea_cargos_is_rcl.{{ $i }}.measurement_type"
                                                                                        wire:model='sea_cargos_is_rcl.{{ $i }}.measurement_type'>
                                                                                        <option value="">
                                                                                            Select
                                                                                        </option>
                                                                                        <option value="1">Lot
                                                                                        </option>
                                                                                        <option value="2">Per
                                                                                            piece
                                                                                        </option>
                                                                                    </select>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-1">
                                                                                <div
                                                                                    class="w-24 -mt-2 whitespace-normal">
                                                                                    <input type="text"
                                                                                        class="w-24 h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="sea_cargos_is_rcl.{{ $i }}.amount"
                                                                                        wire:model.defer='sea_cargos_is_rcl.{{ $i }}.amount'
                                                                                        wire:change="forCWT_CBM({{ $transport_mode }})">
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-1">
                                                                                <div class="-mt-2 whitespace-normal">
                                                                                    <div class="-mt-2">
                                                                                        @if ($sea_cargos_is_rcl[$i]['for_crating'] == true)
                                                                                            <svg wire:click="isForCreating({{ $i }}, 2, false)"
                                                                                                class="py-1 rounded-full text-green"
                                                                                                aria-hidden="true"
                                                                                                focusable="false"
                                                                                                data-prefix="fas"
                                                                                                data-icon="user-slash"
                                                                                                role="img"
                                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                                viewBox="0 0 720 510">
                                                                                                <path
                                                                                                    fill="currentColor"
                                                                                                    d="M192 64C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192s-86-192-192-192H192zM384 352c-53 0-96-43-96-96s43-96 96-96s96 43 96 96s-43 96-96 96z">
                                                                                                </path>
                                                                                            </svg>
                                                                                        @else
                                                                                            <svg wire:click="isForCreating({{ $i }}, 2, true)"
                                                                                                class="py-1 text-gray-400 rounded-full"
                                                                                                aria-hidden="true"
                                                                                                focusable="false"
                                                                                                data-prefix="fas"
                                                                                                data-icon="user-slash"
                                                                                                role="img"
                                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                                viewBox="0 0 720 510">
                                                                                                <path
                                                                                                    fill="currentColor"
                                                                                                    d="M384 128c70.7 0 128 57.3 128 128s-57.3 128-128 128H192c-70.7 0-128-57.3-128-128s57.3-128 128-128H384zM576 256c0-106-86-192-192-192H192C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192zM192 352c53 0 96-43 96-96s-43-96-96-96s-96 43-96 96s43 96 96 96z">
                                                                                                </path>
                                                                                            </svg>
                                                                                        @endif
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-1">
                                                                                <div class="-mt-2">
                                                                                    <select
                                                                                        class="h-6 p-0 px-2 text-xs border rounded-md cursor-pointer w-28"
                                                                                        name="sea_cargos_is_rcl.{{ $i }}.crating_type"
                                                                                        wire:model='sea_cargos_is_rcl.{{ $i }}.crating_type'
                                                                                        @if ($sea_cargos_is_rcl[$i]['for_crating'] == false) disabled @endif>
                                                                                        <option value="">
                                                                                            Select
                                                                                        </option>
                                                                                        @foreach ($crating_type_references as $crating_type_ref)
                                                                                            <option
                                                                                                value="{{ $crating_type_ref->id }}">
                                                                                                {{ $crating_type_ref->name }}
                                                                                            </option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-1">
                                                                                <div class="-mt-2">
                                                                                    <input type="text"
                                                                                        class="w-16 h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="sea_cargos_is_rcl.{{ $i }}.cbm"
                                                                                        wire:model.defer='sea_cargos_is_rcl.{{ $i }}.cbm'
                                                                                        disabled>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-2">
                                                                                <div class="flex gap-2 -mt-2">
                                                                                    @if (count($sea_cargos_is_rcl) > 1)
                                                                                        <button type="button"
                                                                                            title="Remove"
                                                                                            wire:click="removeSeaCargosRcl({{ $i }})"
                                                                                            class="w-5 h-5 px-1 pr-1.5 -pt-0.5 text-xs flex-none bg-red-600 text-white rounded-full">
                                                                                            -</button>
                                                                                    @endif
                                                                                    @if (count($sea_cargos_is_rcl) == ($i += 1))
                                                                                        <button type="button"
                                                                                            title=""
                                                                                            wire:click="addSeaCargosRcl"
                                                                                            class="w-5 h-5 px-1 pr-1.5 -pt-0.5 text-xs flex-none bg-[#003399] text-white rounded-full">
                                                                                            +
                                                                                        </button>
                                                                                    @endif
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        @endif
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                        <div>
                                            <div class="flex gap-2 mt-4 font-medium text-gray-500">
                                                <div class="mt-[-8px]">
                                                    <input type="checkbox"
                                                        class="w-6 h-6 rounded-md border-[#003399] border cursor-pointer"
                                                        name="sea_box" wire:model='sea_box'
                                                        @if ($sea_box) checked @endif>
                                                </div>
                                                <div class="-mt-2 text-lg">Sea Box</div>
                                            </div>
                                            @if ($sea_box)
                                                <div class="flex px-2.5 mt-2">
                                                    <div class="overflow-auto border-l-4 border-[#003399]">
                                                        <table class="text-md text-[#003399] ml-4">
                                                            <thead>
                                                                <tr class="text-left">
                                                                    <th class="w-12 px-3">
                                                                        <x-label value="Qty" />
                                                                    </th>
                                                                    <th class="w-24 px-3">
                                                                        <x-label value="Box Size" />
                                                                    </th>
                                                                    <th class="w-24 px-3">
                                                                        <x-label value="Weight (KG)" />
                                                                    </th>
                                                                    <th class="w-24 px-3">
                                                                        <x-label value="Amount" />
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach ($sea_boxes as $i => $ab)
                                                                    <tr class="mt-4">
                                                                        <td class="px-2">
                                                                            <div class="mb-2">
                                                                                <input type="text"
                                                                                    class="w-10 h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                    name="sea_boxes.{{ $i }}.qty"
                                                                                    wire:model.defer='sea_boxes.{{ $i }}.qty'>
                                                                            </div>
                                                                        </td>
                                                                        <td class="px-2">
                                                                            <div class="w-24 mb-2 whitespace-normal">
                                                                                <select
                                                                                    class="w-24 h-6 p-0 px-2 text-xs border rounded-md cursor-pointer"
                                                                                    name="sea_boxes.{{ $i }}.box_size"
                                                                                    wire:model='sea_boxes.{{ $i }}.box_size'>
                                                                                    <option value="">Select
                                                                                    </option>
                                                                                    <option value="1">Small
                                                                                    </option>
                                                                                    <option value="2">Medium
                                                                                    </option>
                                                                                    <option value="2">Large
                                                                                    </option>
                                                                                    </option>
                                                                                </select>
                                                                            </div>
                                                                        </td>
                                                                        <td class="px-2">
                                                                            <div class="w-24 mb-2 whitespace-normal">
                                                                                <input type="text"
                                                                                    class="w-24 h-6 p-0 px-2 text-xs border rounded-md cursor-pointer"
                                                                                    name="sea_boxes.{{ $i }}.weight"
                                                                                    wire:model.defer='sea_boxes.{{ $i }}.weight'
                                                                                    wire:change="forBoxWeight({{ $transport_mode }})">
                                                                            </div>
                                                                        </td>
                                                                        <td class="px-2">
                                                                            <div class="w-24 mb-2 whitespace-normal">
                                                                                <input type="text"
                                                                                    class="w-24 h-6 p-0 px-2 text-xs border rounded-md cursor-pointer"
                                                                                    name="sea_boxes.{{ $i }}.amount"
                                                                                    wire:model.defer='sea_boxes.{{ $i }}.amount'
                                                                                    disabled>
                                                                            </div>
                                                                        </td>
                                                                        <td class="px-2">
                                                                            <div class="flex gap-2 -mt-2">
                                                                                @if (count($sea_boxes) > 1)
                                                                                    <button type="button"
                                                                                        title="Remove"
                                                                                        wire:click="removeSeaBoxes({{ $i }})"
                                                                                        class="w-5 h-5 px-1 pr-1.5 -pt-0.5 text-xs flex-none bg-red-600 text-white rounded-full">
                                                                                        -</button>
                                                                                @endif
                                                                                @if (count($sea_boxes) == ($i += 1))
                                                                                    <button type="button"
                                                                                        title=""
                                                                                        wire:click="addSeaBoxes"
                                                                                        class="w-5 h-5 px-1 pr-1.5 -pt-0.5 text-xs flex-none bg-[#003399] text-white rounded-full">
                                                                                        +
                                                                                    </button>
                                                                                @endif
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endif
                                {{-- for Transport Mode LAND --}}
                                @if ($transport_mode == 3)
                                    <div class="my-6">
                                        <div>
                                            <div class="flex gap-2 mt-4 font-medium text-gray-500">
                                                <div class="mt-[-8px]">
                                                    <input type="checkbox"
                                                        class="w-6 h-6 rounded-md border-[#003399] border cursor-pointer"
                                                        name="land" wire:model='land'
                                                        @if ($land) checked @endif>
                                                </div>
                                                <div class="-mt-2 text-lg">Land</div>
                                            </div>
                                            @if ($land)
                                                <div class="flex px-2.5 mt-2">
                                                    <div class="overflow-auto border-l-4 border-[#003399]">
                                                        <div>
                                                            <div
                                                                class="flex gap-6 mt-1 mb-4 ml-6 text-xs font-medium text-gray-500">
                                                                <div class="flex gap-1">
                                                                    <input type="radio"
                                                                        class="border-[#003399] border"
                                                                        name="land_packaging_type"
                                                                        wire:model='land_packaging_type'
                                                                        value="1">
                                                                    <span class="">Shipper's Box</span>
                                                                </div>
                                                                <div class="flex gap-1">
                                                                    <input type="radio"
                                                                        class="border-[#003399] border"
                                                                        name="land_packaging_type"
                                                                        wire:model='land_packaging_type'
                                                                        value="2">
                                                                    <span class="">Land Pouch</span>
                                                                </div>
                                                                <div class="flex gap-1">
                                                                    <input type="radio"
                                                                        class="border-[#003399] border"
                                                                        name="land_packaging_type"
                                                                        wire:model='land_packaging_type'
                                                                        value="3">
                                                                    <span class="">Land Box</span>
                                                                </div>
                                                                <div class="flex gap-1">
                                                                    <input type="radio"
                                                                        class="border-[#003399] border"
                                                                        name="land_packaging_type"
                                                                        wire:model='land_packaging_type'
                                                                        value="4">
                                                                    <span class="">FTL</span>
                                                                </div>
                                                            </div>
                                                            @if ($land_packaging_type == 2 || $land_packaging_type == 3)
                                                                <div
                                                                    class="flex gap-6 mt-1 mb-4 ml-6 text-xs font-medium text-gray-500">
                                                                    @foreach ($booking_type_references as $i => $sf_type)
                                                                        <div class="flex gap-1">
                                                                            <input type="radio"
                                                                                class="border-[#003399] border"
                                                                                name="booking_type"
                                                                                wire:model='booking_type'
                                                                                value="{{ $booking_type_references[$i]['id'] }}">
                                                                            <span
                                                                                class="uppercase">{{ $booking_type_references[$i]['name'] }}</span>
                                                                        </div>
                                                                    @endforeach
                                                                </div>
                                                            @endif
                                                        </div>
                                                        @if ($land_packaging_type == 1)
                                                            <table class="text-md text-[#003399] ml-4">
                                                                <thead>
                                                                    <tr class="text-left">
                                                                        <th class="w-24 px-2 whitespace-nowrap">
                                                                            <x-label value="Total Quantity"
                                                                                class="text-xs" />
                                                                        </th>
                                                                        <th class="w-24 px-2">
                                                                            <x-label value="Total" class="text-xs" />
                                                                            <x-label value="Weight (KG)"
                                                                                class="-mt-1 text-xs whitespace-nowrap" />
                                                                        </th>
                                                                        <th class="w-40 px-1 whitespace-nowrap">
                                                                            <x-label value="Dimension (LxWxH)"
                                                                                class="text-xs" />
                                                                        </th>
                                                                        <th class="w-20 px-1">
                                                                            <x-label value="CBM" class="text-xs" />
                                                                        </th>
                                                                        <th class="w-20 px-1">
                                                                            <x-label value="CWT" class="text-xs" />
                                                                        </th>
                                                                        <th class="w-24 px-1">
                                                                            <x-label value="Land Rate"
                                                                                class="text-xs" />
                                                                        </th>
                                                                        <th class="w-24 px-1">
                                                                            <x-label value="Rate Amount"
                                                                                class="text-xs" />
                                                                        </th>
                                                                        <th class="px-1"></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @foreach ($lands_shippers_box as $i => $ac)
                                                                        <tr class="">
                                                                            <td class="px-1">
                                                                                <div class="mb-2">
                                                                                    <input type="text"
                                                                                        class="w-full h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="lands_shippers_box.{{ $i }}.total_qty"
                                                                                        wire:model.defer='lands_shippers_box.{{ $i }}.total_qty'>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-1">
                                                                                <div class="mb-2">
                                                                                    <input type="text"
                                                                                        class="w-full h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="lands_shippers_box.{{ $i }}.total_weight"
                                                                                        wire:model.defer='lands_shippers_box.{{ $i }}.total_weight'>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-1">
                                                                                <div class="mb-2">
                                                                                    <div class="flex gap-2">
                                                                                        <input type="text"
                                                                                            class="w-8 h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                            name="lands_shippers_box.{{ $i }}.dimension_l"
                                                                                            wire:model.defer='lands_shippers_box.{{ $i }}.dimension_l'>
                                                                                        <input type="text"
                                                                                            class="w-8 h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                            name="lands_shippers_box.{{ $i }}.dimension_w"
                                                                                            wire:model.defer='lands_shippers_box.{{ $i }}.dimension_w'>
                                                                                        <input type="text"
                                                                                            class="w-8 h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                            name="lands_shippers_box.{{ $i }}.dimension_h"
                                                                                            wire:model.defer='lands_shippers_box.{{ $i }}.dimension_h'>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-1">
                                                                                <div class="mb-2">
                                                                                    <input type="text"
                                                                                        class="w-12 h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="lands_shippers_box.{{ $i }}.cbm"
                                                                                        wire:model.defer='lands_shippers_box.{{ $i }}.cbm'
                                                                                        disabled>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-1">
                                                                                <div class="mb-2">
                                                                                    <input type="text"
                                                                                        class="w-12 h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="lands_shippers_box.{{ $i }}.cwt"
                                                                                        wire:model.defer='lands_shippers_box.{{ $i }}.cwt'
                                                                                        disabled>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-1">
                                                                                <div
                                                                                    class="w-24 mb-2 whitespace-normal">
                                                                                    <select
                                                                                        class="w-24 h-6 p-0 px-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="lands_shippers_box.{{ $i }}.land_rate"
                                                                                        wire:model='lands_shippers_box.{{ $i }}.land_rate'>
                                                                                        <option value="">Select
                                                                                        </option>
                                                                                        <option value="1">Lot
                                                                                        </option>
                                                                                        <option value="2">Per
                                                                                            piece
                                                                                        </option>
                                                                                    </select>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-1">
                                                                                <div class="mb-2">
                                                                                    <input type="text"
                                                                                        class="w-24 h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="lands_shippers_box.{{ $i }}.rate_amount"
                                                                                        wire:model.defer='lands_shippers_box.{{ $i }}.rate_amount'
                                                                                        disabled>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-2">
                                                                                <div class="flex gap-2 -mt-2">
                                                                                    @if (count($lands_shippers_box) > 1)
                                                                                        <button type="button"
                                                                                            title="Remove"
                                                                                            wire:click="removeLandsShippersBox({{ $i }})"
                                                                                            class="w-5 h-5 px-1 pr-1.5 -pt-0.5 text-xs flex-none bg-red-600 text-white rounded-full">
                                                                                            -</button>
                                                                                    @endif
                                                                                    @if (count($lands_shippers_box) == ($i += 1))
                                                                                        <button type="button"
                                                                                            title=""
                                                                                            wire:click="addLandsShippersBox"
                                                                                            class="w-5 h-5 px-1 pr-1.5 -pt-0.5 text-xs flex-none bg-[#003399] text-white rounded-full">
                                                                                            +
                                                                                        </button>
                                                                                    @endif
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        @elseif($land_packaging_type == 2)
                                                            <table class="text-md text-[#003399] ml-4">
                                                                <thead>
                                                                    <tr class="text-left">
                                                                        <th class="w-24 px-1">
                                                                            <x-label value="Size" />
                                                                        </th>
                                                                        <th class="w-24 px-2 whitespace-nowrap">
                                                                            <x-label value="Weight Per"
                                                                                class="text-xs" />
                                                                            <x-label value="Pouch (KG)"
                                                                                class="-mt-1 text-xs whitespace-nowrap" />
                                                                        </th>
                                                                        <th class="w-20 px-1">
                                                                            <x-label value="CBM" />
                                                                        </th>
                                                                        <th class="w-24 px-1">
                                                                            <x-label value="Total Amount" />
                                                                        </th>
                                                                        <th class="px-1"></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @foreach ($lands_pouches as $i => $lc)
                                                                        <tr class="">
                                                                            <td class="px-1">
                                                                                <div
                                                                                    class="w-24 mb-2 whitespace-normal">
                                                                                    <select
                                                                                        class="w-24 h-6 p-0 px-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="lands_pouches.{{ $i }}.size"
                                                                                        wire:model='lands_pouches.{{ $i }}.size'>
                                                                                        <option value="">Select
                                                                                        </option>
                                                                                        <option value="1">Small
                                                                                        </option>
                                                                                        <option value="2">Medium
                                                                                        </option>
                                                                                        <option value="3">Large
                                                                                        </option>
                                                                                    </select>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-1">
                                                                                <div class="mb-2">
                                                                                    <input type="text"
                                                                                        class="w-full h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="lands_pouches.{{ $i }}.weight_per_pouch"
                                                                                        wire:model.defer='lands_pouches.{{ $i }}.weight_per_pouch'>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-1">
                                                                                <div class="mb-2">
                                                                                    <input type="text"
                                                                                        class="w-full h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="lands_pouches.{{ $i }}.cbm"
                                                                                        wire:model.defer='lands_pouches.{{ $i }}.cbm'
                                                                                        disabled>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-1">
                                                                                <div class="mb-2">
                                                                                    <input type="text"
                                                                                        class="w-24 h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="lands_pouches.{{ $i }}.total_amount"
                                                                                        wire:model.defer='lands_pouches.{{ $i }}.total_amount'
                                                                                        disabled>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-2">
                                                                                <div class="flex gap-2 -mt-2">
                                                                                    @if (count($lands_pouches) > 1)
                                                                                        <button type="button"
                                                                                            title="Remove"
                                                                                            wire:click="removeLandsPouches({{ $i }})"
                                                                                            class="w-5 h-5 px-1 pr-1.5 -pt-0.5 text-xs flex-none bg-red-600 text-white rounded-full">
                                                                                            -</button>
                                                                                    @endif
                                                                                    @if (count($lands_pouches) == ($i += 1))
                                                                                        <button type="button"
                                                                                            title=""
                                                                                            wire:click="addLandsPouches"
                                                                                            class="w-5 h-5 px-1 pr-1.5 -pt-0.5 text-xs flex-none bg-[#003399] text-white rounded-full">
                                                                                            +
                                                                                        </button>
                                                                                    @endif
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        @elseif($land_packaging_type == 3)
                                                            <table class="text-md text-[#003399] ml-4">
                                                                <thead>
                                                                    <tr class="text-left">
                                                                        <th class="w-24 px-1">
                                                                            <x-label value="Size" />
                                                                        </th>
                                                                        <th class="w-24 px-2 whitespace-nowrap">
                                                                            <x-label value="Weight Per"
                                                                                class="text-xs" />
                                                                            <x-label value="Box (KG)"
                                                                                class="-mt-1 text-xs whitespace-nowrap" />
                                                                        </th>
                                                                        <th class="w-20 px-1">
                                                                            <x-label value="CBM" />
                                                                        </th>
                                                                        <th class="w-24 px-1">
                                                                            <x-label value="Total Amount" />
                                                                        </th>
                                                                        <th class="px-1"></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @foreach ($lands_boxes as $i => $lb)
                                                                        <tr class="">
                                                                            <td class="px-1">
                                                                                <div
                                                                                    class="w-24 mb-2 whitespace-normal">
                                                                                    <select
                                                                                        class="w-24 h-6 p-0 px-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="lands_boxes.{{ $i }}.size"
                                                                                        wire:model='lands_boxes.{{ $i }}.size'>
                                                                                        <option value="">Select
                                                                                        </option>
                                                                                        <option value="1">Small
                                                                                        </option>
                                                                                        <option value="2">Medium
                                                                                        </option>
                                                                                        <option value="3">Large
                                                                                        </option>
                                                                                    </select>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-1">
                                                                                <div class="mb-2">
                                                                                    <input type="text"
                                                                                        class="w-full h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="lands_boxes.{{ $i }}.weight_per_box"
                                                                                        wire:model.defer='lands_boxes.{{ $i }}.weight_per_box'>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-1">
                                                                                <div class="mb-2">
                                                                                    <input type="text"
                                                                                        class="w-full h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="lands_boxes.{{ $i }}.cbm"
                                                                                        wire:model.defer='lands_boxes.{{ $i }}.cbm'
                                                                                        disabled>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-1">
                                                                                <div class="mb-2">
                                                                                    <input type="text"
                                                                                        class="w-24 h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="lands_boxes.{{ $i }}.total_amount"
                                                                                        wire:model.defer='lands_boxes.{{ $i }}.total_amount'
                                                                                        disabled>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-2">
                                                                                <div class="flex gap-2 -mt-2">
                                                                                    @if (count($lands_boxes) > 1)
                                                                                        <button type="button"
                                                                                            title="Remove"
                                                                                            wire:click="removeLandsBoxes({{ $i }})"
                                                                                            class="w-5 h-5 px-1 pr-1.5 -pt-0.5 text-xs flex-none bg-red-600 text-white rounded-full">
                                                                                            -</button>
                                                                                    @endif
                                                                                    @if (count($lands_boxes) == ($i += 1))
                                                                                        <button type="button"
                                                                                            title=""
                                                                                            wire:click="addLandsBoxes"
                                                                                            class="w-5 h-5 px-1 pr-1.5 -pt-0.5 text-xs flex-none bg-[#003399] text-white rounded-full">
                                                                                            +
                                                                                        </button>
                                                                                    @endif
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        @elseif($land_packaging_type == 4)
                                                            <table class="text-md text-[#003399] ml-4">
                                                                <thead>
                                                                    <tr class="text-left">
                                                                        <th class="w-20 px-1">
                                                                            <x-label value="Quantity" />
                                                                        </th>
                                                                        <th class="w-48 px-2 whitespace-nowrap">
                                                                            <x-label value="Track Type" />
                                                                        </th>
                                                                        <th class="w-24 px-1">
                                                                            <x-label value="Unit Price" />
                                                                        </th>
                                                                        <th class="w-24 px-1">
                                                                            <x-label value="Amount" />
                                                                        </th>
                                                                        <th class="px-1"></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @foreach ($lands_ftl as $i => $ftl)
                                                                        <tr class="">
                                                                            <td class="px-2">
                                                                                <div class="mb-2">
                                                                                    <input type="text"
                                                                                        class="w-20 h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="lands_ftl.{{ $i }}.quantity"
                                                                                        wire:model.defer='lands_ftl.{{ $i }}.quantity'>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-2">
                                                                                <div class="mb-2">
                                                                                    <input type="text"
                                                                                        class="w-48 h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="lands_ftl.{{ $i }}.truck_type"
                                                                                        wire:model.defer='lands_ftl.{{ $i }}.truck_type'>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-2">
                                                                                <div class="mb-2">
                                                                                    <input type="text"
                                                                                        class="w-24 h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="lands_ftl.{{ $i }}.unit_price"
                                                                                        wire:model.defer='lands_ftl.{{ $i }}.unit_price'>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-2">
                                                                                <div class="mb-2">
                                                                                    <input type="text"
                                                                                        class="w-24 h-6 p-2 text-xs border rounded-md cursor-pointer"
                                                                                        name="lands_ftl.{{ $i }}.amount"
                                                                                        wire:model.defer='lands_ftl.{{ $i }}.amount'
                                                                                        disabled>
                                                                                </div>
                                                                            </td>
                                                                            <td class="px-2">
                                                                                <div class="flex gap-2 -mt-2">
                                                                                    @if (count($lands_boxes) > 1)
                                                                                        <button type="button"
                                                                                            title="Remove"
                                                                                            wire:click="removeLandsFtl({{ $i }})"
                                                                                            class="w-5 h-5 px-1 pr-1.5 -pt-0.5 text-xs flex-none bg-red-600 text-white rounded-full">
                                                                                            -</button>
                                                                                    @endif
                                                                                    @if (count($lands_boxes) == ($i += 1))
                                                                                        <button type="button"
                                                                                            title=""
                                                                                            wire:click="addLandsFtl"
                                                                                            class="w-5 h-5 px-1 pr-1.5 -pt-0.5 text-xs flex-none bg-[#003399] text-white rounded-full">
                                                                                            +
                                                                                        </button>
                                                                                    @endif
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        @endif
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="grid grid-cols-1 bg-white rounded-lg shadow-lg">
                                <div
                                    class="flex gap-12 text-center justify-start border-2 border-[#003399] rounded-b-lg px-12 py-6 whitespace-nowrap">
                                    <div class="text-xl text-[#003399] font-semibold">TOTAL :</div>
                                    <div class="flex gap-3">
                                        <div class="mt-1">Quantity :</div>
                                        <div class="text-[#003399] font-semibold text-xl">0</div>
                                    </div>
                                    <div class="flex gap-3">
                                        <div class="mt-1">Weight :</div>
                                        <div class="text-[#003399] font-semibold text-xl">0</div>
                                    </div>
                                    <div class="flex gap-3">
                                        <div class="mt-1">Volume :</div>
                                        <div class="text-[#003399] font-semibold text-xl">0</div>
                                    </div>
                                </div>
                            </div>
                            @if ($is_compute)
                                <div class="grid grid-cols-1 px-10 py-4 mt-2 bg-white rounded-lg shadow-lg">
                                    <span class="italic font-normal text-gray-400 text-md">Please note that this is
                                        just an estimated computation based on the given information. Above amount may
                                        change depending on the actual cargo upon pick-up/acceptance.</span>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-span-4">
                        <div class="pb-4 bg-white rounded-lg shadow-lg">
                            <div
                                class="grid grid-cols-1 py-2 text-2xl text-center text-gray-500 bg-gray-300 rounded-t-lg">
                                BREAKDOWN OF FREIGHT CHARGES
                            </div>
                            <div class="grid grid-cols-1 px-6 mt-4 space-y-2">
                                <div class="grid grid-cols-2">
                                    <div>CBM Charge :</div>
                                    <div>
                                        <input type="text"
                                            class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                            name="weight_charge" wire:model.defer='weight_charge' disabled>
                                    </div>
                                </div>
                                <div class="grid grid-cols-2">
                                    <div>AWB Fee :</div>
                                    <div>
                                        <input type="text"
                                            class="w-full h-8 p-2 border rounded-md cursor-pointer" name="awb_fee"
                                            wire:model.defer='awb_fee' disabled>
                                    </div>
                                </div>
                                <div class="grid grid-cols-2">
                                    <div>Valuation :</div>
                                    <div>
                                        <input type="text"
                                            class="w-full h-8 p-2 border rounded-md cursor-pointer" name="valuation"
                                            wire:model.defer='valuation' disabled>
                                    </div>
                                </div>
                                <div class="grid grid-cols-2">
                                    <div>COD Charges :</div>
                                    <div>
                                        <input type="text"
                                            class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                            name="cod_charge" wire:model.defer='cod_charge' disabled>
                                    </div>
                                </div>
                                <div class="grid grid-cols-2">
                                    <div>Insurance :</div>
                                    <div>
                                        <input type="text"
                                            class="w-full h-8 p-2 border rounded-md cursor-pointer" name="insurance"
                                            wire:model.defer='insurance' disabled>
                                    </div>
                                </div>
                                <div class="grid grid-cols-2">
                                    <div>Handling Fee :</div>
                                    <div>
                                        <input type="text"
                                            class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                            name="handling_fee" wire:model.defer='handling_fee' disabled>
                                    </div>
                                </div>
                                <div class="grid grid-cols-2">
                                    <div>Doc Fee :</div>
                                    <div>
                                        <input type="text"
                                            class="w-full h-8 p-2 border rounded-md cursor-pointer" name=""
                                            wire:model.defer='' disabled>
                                    </div>
                                </div>
                                <div class="grid grid-cols-2">
                                    <div class="flex gap-2">Other Fees :
                                        <div>
                                            <input type="checkbox"
                                                class="w-6 h-6 rounded-md border-[#003399] border cursor-pointer"
                                                name="other_fees" wire:model='other_fees'
                                                @if ($other_fees) checked @endif>
                                        </div>
                                    </div>
                                    <div></div>
                                </div>
                                @if ($other_fees)
                                    <div class="grid grid-cols-2">
                                        <div>OPA Fee :</div>
                                        <div>
                                            <input type="text"
                                                class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                name="" wire:model.defer='' disabled>
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-2">
                                        <div>ODA Fee :</div>
                                        <div>
                                            <input type="text"
                                                class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                name="" wire:model.defer='' disabled>
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-2">
                                        <div>Crating Fee :</div>
                                        <div>
                                            <input type="text"
                                                class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                name="" wire:model.defer='' disabled>
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-2">
                                        <div>Equipment Rental :</div>
                                        <div>
                                            <input type="text"
                                                class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                name="" wire:model.defer='' disabled>
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-2">
                                        <div>Lashing :</div>
                                        <div>
                                            <input type="text"
                                                class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                name="" wire:model.defer='' disabled>
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-2">
                                        <div>Manpower/Special Handling :</div>
                                        <div>
                                            <input type="text"
                                                class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                name="" wire:model.defer='' disabled>
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-2">
                                        <div>Dangerous Goods Fee :</div>
                                        <div>
                                            <input type="text"
                                                class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                name="" wire:model.defer='' disabled>
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-2">
                                        <div>Trucking :</div>
                                        <div>
                                            <input type="text"
                                                class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                name="" wire:model.defer='' disabled>
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-2">
                                        <div>Perishable Fee :</div>
                                        <div>
                                            <input type="text"
                                                class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                name="" wire:model.defer='' disabled>
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-2">
                                        <div>Packaging Fee :</div>
                                        <div>
                                            <input type="text"
                                                class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                name="" wire:model.defer='' disabled>
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-2">
                                        <div>Other Charges :</div>
                                        <div>
                                            <input type="text"
                                                class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                name="" wire:model.defer='' disabled>
                                        </div>
                                    </div>
                                @endif
                                <div class="grid grid-cols-2">
                                    <div>Subtotal :</div>
                                    <div>
                                        <input type="text"
                                            class="w-full h-8 p-2 border rounded-md cursor-pointer" name="subtotal"
                                            wire:model.defer='subtotal' disabled>
                                    </div>
                                </div>
                                <div class="grid grid-cols-2">
                                    <div class="whitespace-nowrap">Discount Percentage :</div>
                                    <div>
                                        <input type="text"
                                            class="w-full h-8 p-2 border rounded-md cursor-pointer" name=""
                                            wire:model.defer='' disabled>
                                    </div>
                                </div>
                                <div class="grid grid-cols-2">
                                    <div>Discount Amount :</div>
                                    <div>
                                        <input type="text"
                                            class="w-full h-8 p-2 border rounded-md cursor-pointer" name=""
                                            wire:model.defer='' disabled>
                                    </div>
                                </div>
                                <div class="grid grid-cols-2">
                                    <div class="flex gap-2">EVAT :
                                        <div>
                                            <input type="checkbox"
                                                class="w-6 h-6 rounded-md border-[#003399] border cursor-pointer"
                                                name="evat" wire:model='evat'
                                                @if ($other_fees) checked @endif>
                                        </div>
                                    </div>
                                    <div></div>
                                </div>
                            </div>
                            <div class="flex mt-4 border-2 border-b-none border-x-none border-[#003399]">
                            </div>
                            <div
                                class="font-semibold flex justify-between px-6 pt-2 text-lg text-[#003399] rounded-b-lg">
                                <div>Grand Total :</div>
                                <div class="text-2xl">
                                    {{ $grand_total }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="space-y-4">
                    <div class="grid grid-cols-1 px-12 bg-white rounded-lg shadow-lg">
                        <div class="py-4 text-lg font-semibold">Service Type</div>
                        <div class="flex gap-20 px-6 py-6">
                            @foreach ($service_type_references as $i => $st)
                                <div class="flex gap-2 font-medium text-gray-500">
                                    <div class="mt-[-8px]">
                                        <input type="checkbox"
                                            class="w-6 h-6 rounded-md border-[#003399] border cursor-pointer"
                                            name="service_type"
                                            wire:model="{{ $service_type_references[$i]['wire_model'] }}"
                                            value="{{ $service_type_references[$i]['id'] }}"
                                            @if ($service_type_references[$i]['wire_model']) checked @endif>
                                    </div>
                                    <div class="-mt-2 text-lg">{{ $service_type_references[$i]['name'] }}</div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="grid grid-cols-1 bg-white rounded-lg shadow-lg">
                        <div class="flex px-6 py-6 whitespace-nowrap">
                            <div class="flex gap-20 px-6 py-6 overflow-auto">
                                <table class="text-md text-[#003399]">
                                    <thead>
                                        <tr class="text-left">
                                            <th class="w-12 px-2">
                                                <x-label value="Quantity" />
                                            </th>
                                            <th class="w-12 px-2">
                                                <x-label value="Weight" />
                                            </th>
                                            <th class="w-24 px-1 whitespace-nowrap">
                                                <x-label value="Dimension (LxWxH)" />
                                            </th>
                                            <th class="w-24 px-2">
                                                <x-label value="Unit of Measurement" />
                                            </th>
                                            <th class="w-24 px-2">
                                                <x-label value="Measurement Type" />
                                            </th>
                                            <th class="w-24 px-2">
                                                <x-label value="Type of Packaging" />
                                            </th>
                                            <th class="w-12 px-2">
                                                <x-label value="For Crating" />
                                            </th>
                                            <th class="w-24 px-2">
                                                <x-label value="Crating Type" />
                                            </th>
                                            <th class="w-24 px-2">
                                                <x-label value="CBM" />
                                            </th>
                                            <th class="w-16 px-2"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($other_services_measurements as $i => $osm)
                                            <tr class="">
                                                <td class="px-1">
                                                    <div class="mt-1">
                                                        <input type="text"
                                                            class="w-16 h-8 p-2 border rounded-md cursor-pointer"
                                                            name="other_services_measurements.{{ $i }}.qty"
                                                            wire:model.defer='other_services_measurements.{{ $i }}.qty'>
                                                    </div>
                                                </td>
                                                <td class="px-1">
                                                    <div class="mt-1">
                                                        <input type="text"
                                                            class="w-16 h-8 p-2 border rounded-md cursor-pointer"
                                                            name="other_services_measurements.{{ $i }}.wt"
                                                            wire:model.defer='other_services_measurements.{{ $i }}.wt'
                                                            wire:change="forCWT_CBM({{ $os_transport_mode }})">
                                                    </div>
                                                </td>
                                                <td class="px-1">
                                                    <div class="mt-1">
                                                        <div class="flex gap-2">
                                                            <input type="text"
                                                                class="w-16 h-8 p-2 border rounded-md cursor-pointer"
                                                                name="other_services_measurements.{{ $i }}.dimension_l"
                                                                wire:model.defer='other_services_measurements.{{ $i }}.dimension_l'
                                                                wire:change="forCWT_CBM({{ $os_transport_mode }})">
                                                            <input type="text"
                                                                class="w-16 h-8 p-2 border rounded-md cursor-pointer"
                                                                name="other_services_measurements.{{ $i }}.dimension_w"
                                                                wire:model.defer='other_services_measurements.{{ $i }}.dimension_w'
                                                                wire:change="forCWT_CBM({{ $os_transport_mode }})">
                                                            <input type="text"
                                                                class="w-16 h-8 p-2 border rounded-md cursor-pointer"
                                                                name="other_services_measurements.{{ $i }}.dimension_h"
                                                                wire:model.defer='other_services_measurements.{{ $i }}.dimension_h'
                                                                wire:change="forCWT_CBM({{ $os_transport_mode }})">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="px-1">
                                                    <div class="w-40 mt-1 whitespace-normal">
                                                        <select
                                                            class="w-40 h-8 p-0 px-2 border rounded-md cursor-pointer"
                                                            name="other_services_measurements.{{ $i }}.unit_of_measurement"
                                                            wire:model='other_services_measurements.{{ $i }}.unit_of_measurement'>
                                                            <option value="">Select
                                                            </option>
                                                            <option value="1">mm</option>
                                                            <option value="2">cm</option>
                                                            <option value="3">inches
                                                            </option>
                                                            <option value="4">m</option>
                                                            <option value="5">ft</option>
                                                            <option value="6">yard
                                                            </option>
                                                        </select>
                                                    </div>
                                                </td>
                                                <td class="px-1">
                                                    <div class="w-40 mt-1 whitespace-normal">
                                                        <select
                                                            class="w-40 h-8 p-0 px-2 border rounded-md cursor-pointer"
                                                            name="other_services_measurements.{{ $i }}.measurement_type"
                                                            wire:model='other_services_measurements.{{ $i }}.measurement_type'>
                                                            <option value="">Select
                                                            </option>
                                                            <option value="1">Lot</option>
                                                            <option value="2">Per piece
                                                            </option>
                                                        </select>
                                                    </div>
                                                </td>
                                                <td class="px-1">
                                                    <div class="w-40 mt-1 whitespace-normal">
                                                        <select
                                                            class="w-40 h-8 p-0 px-2 border rounded-md cursor-pointer"
                                                            name="other_services_measurements.{{ $i }}.amount"
                                                            wire:model='other_services_measurements.{{ $i }}.amount'>
                                                            <option value="">Select
                                                            </option>
                                                            <option value="1">Box</option>
                                                            <option value="2">Plastic
                                                            </option>
                                                            <option value="3">Crate
                                                            </option>
                                                        </select>
                                                    </div>
                                                </td>
                                                <td class="px-1">
                                                    <div class="mt-1 whitespace-normal">
                                                        <div class="mt-1">
                                                            @if ($other_services_measurements[$i]['for_crating'] == true)
                                                                <svg wire:click="osIsForCreating({{ $i }}, 1, false)"
                                                                    class="w-16 h-10 py-1 rounded-full text-green"
                                                                    aria-hidden="true" focusable="false"
                                                                    data-prefix="fas" data-icon="user-slash"
                                                                    role="img"
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    viewBox="0 0 580 450">
                                                                    <path fill="currentColor"
                                                                        d="M192 64C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192s-86-192-192-192H192zM384 352c-53 0-96-43-96-96s43-96 96-96s96 43 96 96s-43 96-96 96z">
                                                                    </path>
                                                                </svg>
                                                            @else
                                                                <svg wire:click="osIsForCreating({{ $i }}, 1, true)"
                                                                    class="w-16 h-10 py-1 text-gray-400 rounded-full"
                                                                    aria-hidden="true" focusable="false"
                                                                    data-prefix="fas" data-icon="user-slash"
                                                                    role="img"
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    viewBox="0 0 580 450">
                                                                    <path fill="currentColor"
                                                                        d="M384 128c70.7 0 128 57.3 128 128s-57.3 128-128 128H192c-70.7 0-128-57.3-128-128s57.3-128 128-128H384zM576 256c0-106-86-192-192-192H192C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192zM192 352c53 0 96-43 96-96s-43-96-96-96s-96 43-96 96s43 96 96 96z">
                                                                    </path>
                                                                </svg>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="px-1">
                                                    <div class="mt-1">
                                                        <select
                                                            class="w-24 h-8 p-0 px-2 border rounded-md cursor-pointer"
                                                            name="other_services_measurements.{{ $i }}.crating_type"
                                                            wire:model='other_services_measurements.{{ $i }}.crating_type'
                                                            @if ($other_services_measurements[$i]['for_crating'] == false) disabled @endif>
                                                            <option value="">Select
                                                            </option>
                                                            @foreach ($os_crating_type_references as $os_crating_type_ref)
                                                                <option value="{{ $os_crating_type_ref->id }}">
                                                                    {{ $os_crating_type_ref->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </td>
                                                <td class="px-1">
                                                    <div class="mt-1">
                                                        <input type="text"
                                                            class="w-40 h-8 p-2 border rounded-md cursor-pointer"
                                                            name="other_services_measurements.{{ $i }}.cbm"
                                                            wire:model.defer='other_services_measurements.{{ $i }}.cbm'
                                                            disabled>
                                                    </div>
                                                </td>
                                                <td class="px-2">
                                                    <div class="flex gap-2 mt-1">
                                                        @if (count($other_services_measurements) > 1)
                                                            <button type="button" title="Remove"
                                                                wire:click="removeOtherServicesMeasurement({{ $i }})"
                                                                class="w-6 h-6 px-1 pr-0.5 pt-0.5 text-md flex-none bg-red-600 text-white rounded-full">
                                                                -</button>
                                                        @endif
                                                        @if (count($other_services_measurements) == ($i += 1))
                                                            <button type="button" title=""
                                                                wire:click="addOtherServicesMeasurement"
                                                                class="w-6 h-6 px-1 pr-0.5 text-md flex-none bg-[#003399] text-white rounded-full">
                                                                +
                                                            </button>
                                                        @endif
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div
                            class="flex gap-12 text-center justify-end border-2 border-[#003399] rounded-b-lg px-12 py-6 whitespace-nowrap">
                            <div class="text-xl text-[#003399] font-semibold">TOTAL :</div>
                            <div class="flex gap-3">
                                <div class="mt-1">Quantity :</div>
                                <div class="text-[#003399] font-semibold text-xl">0</div>
                            </div>
                            <div class="flex gap-3">
                                <div class="mt-1">Weight :</div>
                                <div class="text-[#003399] font-semibold text-xl">0</div>
                            </div>
                            <div class="flex gap-3">
                                <div class="mt-1">Volume :</div>
                                <div class="text-[#003399] font-semibold text-xl">0</div>
                            </div>
                        </div>
                    </div>

                    @if (!$domestic_freight)
                        <div class="grid grid-cols-2 gap-4">
                            <div class="pb-4 bg-white rounded-lg shadow-lg">
                                <div class="grid grid-cols-1 py-2 rounded-t-lg bg-[#003399]">
                                    <div class="px-12 text-xl text-left text-white">Domestic Freight</div>
                                </div>
                                <div class="px-12 mt-6 space-y-4">
                                    <div class="grid grid-cols-1">
                                        <x-label for="" value="Transport Mode" :required="true" />
                                        <x-select class="w-full rounded-md cursor-pointer h-11"
                                            name="os_transport_mode" wire:model='os_transport_mode'>
                                            <option value="">Select</option>
                                            @foreach ($os_transport_mode_references as $os_transport_mode_ref)
                                                <option value="{{ $os_transport_mode_ref->id }}">
                                                    {{ $os_transport_mode_ref->name }}
                                                </option>
                                            @endforeach
                                        </x-select>
                                        <x-input-error for="" />
                                    </div>
                                    <div class="grid grid-cols-2 gap-8">
                                        <div>
                                            <x-label for="" value="Origin" :required="true" />
                                            <select class="w-full rounded-md cursor-pointer h-11" name="os_origin"
                                                wire:model='os_origin'>
                                                <option value="">Select</option>
                                                @foreach ($os_origin_references as $os_origin_ref)
                                                    <option value="{{ $os_origin_ref->id }}">
                                                        {{ $os_origin_ref->display }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <x-input-error for="" />
                                        </div>
                                        <div>
                                            <x-label for="" value="Destination" :required="true" />
                                            <select class="w-full rounded-md cursor-pointer h-11"
                                                name="os_destination" wire:model='os_destination'>
                                                <option value="">Select</option>
                                                @foreach ($os_destination_references as $os_destination_ref)
                                                    <option value="{{ $os_destination_ref->id }}">
                                                        {{ $os_destination_ref->display }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <x-input-error for="" />
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-1">
                                        <div>
                                            <x-label for="" value="Transhipment" />
                                            <select class="w-full rounded-md cursor-pointer h-11"
                                                name="os_transhipment" wire:model='os_transhipment'>
                                                <option value="">Select</option>
                                                @foreach ($os_transhipment_references as $os_transhipment_ref)
                                                    <option value="{{ $os_transhipment_ref->id }}">
                                                        {{ $os_transhipment_ref->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <x-input-error for="" />
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-1">
                                        <div>
                                            <x-label for="" value="Declared Value" />
                                            <input
                                                class="w-full px-2 border border-gray-500 rounded-md cursor-pointer h-11"
                                                name="os_declared_value" wire:model='os_declared_value' />
                                            <x-input-error for="" />
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-2 gap-8">
                                        <div>
                                            <x-label for="" value="Commodity Type" />
                                            <select class="w-full rounded-md cursor-pointer h-11"
                                                name="os_commodity_type" wire:model='os_commodity_type'>
                                                <option value="">Select</option>
                                                @foreach ($os_commodity_type_references as $os_commodity_type_ref)
                                                    <option value="{{ $os_commodity_type_ref->id }}">
                                                        {{ $os_commodity_type_ref->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <x-input-error for="" />
                                        </div>
                                        <div>
                                            <x-label for="" value="Commodity Applicable Rate" />
                                            <select class="w-full rounded-md cursor-pointer h-11"
                                                name="os_commodity_applicable_rate"
                                                wire:model='os_commodity_applicable_rate'>
                                                <option value="">Select</option>
                                                @foreach ($os_commodity_applicable_rate_references as $os_commodity_applicable_rate_ref)
                                                    <option value="{{ $os_commodity_applicable_rate_ref->id }}">
                                                        {{ $os_commodity_applicable_rate_ref->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <x-input-error for="" />
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-2 gap-8">
                                        <div>
                                            <x-label for="" value="Paymode" :required="true" />
                                            <select class="w-full rounded-md cursor-pointer h-11" name="os_paymode"
                                                wire:model.defer='os_paymode'>
                                                <option value="">Select</option>
                                                @foreach ($os_paymode_references as $os_paymode_ref)
                                                    <option value="{{ $os_paymode_ref->id }}">
                                                        {{ $os_paymode_ref->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <x-input-error for="" />
                                        </div>
                                        <div>
                                            <x-label for="" value="Service Mode" :required="true" />
                                            <select class="w-full rounded-md cursor-pointer h-11"
                                                name="os_service_mode" wire:model.defer='os_service_mode'>
                                                <option value="">Select</option>
                                                @foreach ($os_service_mode_references as $os_service_mode_ref)
                                                    <option value="{{ $os_service_mode_ref->id }}">
                                                        {{ $os_service_mode_ref->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <x-input-error for="" />
                                        </div>
                                    </div>

                                    @if ($os_transport_mode == 2)
                                        <div class="space-y-4">
                                            <div class="grid grid-cols-4 gap-6 mt-2">
                                                <div class="flex gap-1">
                                                    <input type="radio" class="border-[#003399] border mt-1"
                                                        name="os_transhipment_type"
                                                        wire:model='os_transhipment_type' value="1">LCL
                                                </div>
                                                <div class="flex gap-1">
                                                    <input type="radio" class="border-[#003399] border mt-1"
                                                        name="os_transhipment_type"
                                                        wire:model='os_transhipment_type' value="2">FCL
                                                </div>
                                            </div>
                                            @if ($os_transhipment_type == 1)
                                                <div class="grid grid-cols-4 gap-6">
                                                    <div>
                                                        <x-label for="" value="Quantity" />
                                                        <input
                                                            class="w-full border border-gray-500 rounded-md cursor-pointer h-11"
                                                            name="" wire:model=''>
                                                        <x-input-error for="" />
                                                    </div>
                                                    <div>
                                                        <x-label for="" value="Weight" />
                                                        <input
                                                            class="w-full border border-gray-500 rounded-md cursor-pointer h-11"
                                                            name="" wire:model=''>
                                                        <x-input-error for="" />
                                                    </div>
                                                </div>
                                                <div class="grid grid-cols-12 gap-8">
                                                    <div class="col-span-6">
                                                        <div class="">
                                                            <x-label for="" value="Dimension (LxWxH)" />
                                                            <div class="flex gap-4">
                                                                <input
                                                                    class="w-full border border-gray-500 rounded-md cursor-pointer h-11"
                                                                    name="" wire:model=''>
                                                                <x-input-error for="" />
                                                                <input
                                                                    class="w-full border border-gray-500 rounded-md cursor-pointer h-11"
                                                                    name="" wire:model=''>
                                                                <x-input-error for="" />
                                                                <input
                                                                    class="w-full border border-gray-500 rounded-md cursor-pointer h-11"
                                                                    name="" wire:model=''>
                                                                <x-input-error for="" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-span-4">
                                                        <x-label for="" value="Crate" />
                                                        <select class="w-full rounded-md cursor-pointer h-11"
                                                            name="" wire:model.defer=''>
                                                            <option value="">Select</option>
                                                        </select>
                                                        <x-input-error for="" />
                                                    </div>
                                                </div>
                                            @else
                                                <div class="grid grid-cols-4 gap-6">
                                                    <div>
                                                        <x-label for="" value="Quantity" />
                                                        <input
                                                            class="w-full border border-gray-500 rounded-md cursor-pointer h-11"
                                                            name="" wire:model=''>
                                                        <x-input-error for="" />
                                                    </div>
                                                    <div>
                                                        <x-label for="" value="Container" />
                                                        <select class="w-full rounded-md cursor-pointer h-11"
                                                            name="" wire:model.defer=''>
                                                            <option value="">Select</option>
                                                        </select>
                                                        <x-input-error for="" />
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="pb-4 bg-white rounded-lg shadow-lg">
                                <div class="grid grid-cols-1 py-2 bg-gray-300 rounded-t-lg">
                                    <div class="px-12 text-xl text-left text-gray-500">Breakdown Of Freight Charges
                                    </div>
                                </div>
                                <div class="grid grid-cols-1 px-6 mt-4 space-y-2">
                                    <div class="grid grid-cols-1 px-6 mt-4 space-y-2">
                                        <div class="grid grid-cols-12 whitespace-nowrap">
                                            <div class="col-span-5">Weight/ CBM Charge :</div>
                                            <div class="col-span-6">
                                                <input type="text"
                                                    class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                    name="os_weight_charge" wire:model.defer='os_weight_charge'
                                                    disabled>
                                            </div>
                                        </div>
                                        <div class="grid grid-cols-12 whitespace-nowrap">
                                            <div class="col-span-5">AWB Fee :</div>
                                            <div class="col-span-6">
                                                <input type="text"
                                                    class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                    name="os_awb_fee" wire:model.defer='os_awb_fee' disabled>
                                            </div>
                                        </div>
                                        <div class="grid grid-cols-12 whitespace-nowrap">
                                            <div class="col-span-5">Valuation :</div>
                                            <div class="col-span-6">
                                                <input type="text"
                                                    class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                    name="os_valuation" wire:model.defer='os_valuation' disabled>
                                            </div>
                                        </div>
                                        <div class="grid grid-cols-12 whitespace-nowrap">
                                            <div class="col-span-5">COD Charges :</div>
                                            <div class="col-span-6">
                                                <input type="text"
                                                    class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                    name="os_cod_charge" wire:model.defer='os_cod_charge' disabled>
                                            </div>
                                        </div>
                                        <div class="grid grid-cols-12 whitespace-nowrap">
                                            <div class="col-span-5">Insurance :</div>
                                            <div class="col-span-6">
                                                <input type="text"
                                                    class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                    name="os_insurance" wire:model.defer='os_insurance' disabled>
                                            </div>
                                        </div>
                                        <div class="grid grid-cols-12 whitespace-nowrap">
                                            <div class="col-span-5">Handling Fee :</div>
                                            <div class="col-span-6">
                                                <input type="text"
                                                    class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                    name="os_handling_fee" wire:model.defer='os_handling_fee'
                                                    disabled>
                                            </div>
                                        </div>
                                        <div class="grid grid-cols-12 whitespace-nowrap">
                                            <div class="col-span-5">Doc Fee :</div>
                                            <div class="col-span-6">
                                                <input type="text"
                                                    class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                    name="os_doc_fee" wire:model.defer='os_doc_fee' disabled>
                                            </div>
                                        </div>
                                        <div class="grid grid-cols-12 whitespace-nowrap">
                                            <div class="flex col-span-4 gap-4">Other Fees :
                                                <div>
                                                    <input type="checkbox"
                                                        class="w-6 h-6 rounded-md border-[#003399] border cursor-pointer"
                                                        name="os_other_fees" wire:model='os_other_fees'
                                                        @if ($os_other_fees) checked @endif>
                                                </div>
                                            </div>
                                            <div></div>
                                        </div>
                                        @if ($os_other_fees)
                                            <div class="grid grid-cols-12 whitespace-nowrap">
                                                <div class="col-span-5">OPA Fee :</div>
                                                <div class="col-span-6">
                                                    <input type="text"
                                                        class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                        name="" wire:model.defer='' disabled>
                                                </div>
                                            </div>
                                            <div class="grid grid-cols-12 whitespace-nowrap">
                                                <div class="col-span-5">ODA Fee :</div>
                                                <div class="col-span-6">
                                                    <input type="text"
                                                        class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                        name="" wire:model.defer='' disabled>
                                                </div>
                                            </div>
                                            <div class="grid grid-cols-12 whitespace-nowrap">
                                                <div class="col-span-5">Crating Fee :</div>
                                                <div class="col-span-6">
                                                    <input type="text"
                                                        class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                        name="" wire:model.defer='' disabled>
                                                </div>
                                            </div>
                                            <div class="grid grid-cols-12 whitespace-nowrap">
                                                <div class="col-span-5">Equipment Rental :</div>
                                                <div class="col-span-6">
                                                    <input type="text"
                                                        class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                        name="" wire:model.defer='' disabled>
                                                </div>
                                            </div>
                                            <div class="grid grid-cols-12 whitespace-nowrap">
                                                <div class="col-span-5">Lashing :</div>
                                                <div class="col-span-6">
                                                    <input type="text"
                                                        class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                        name="" wire:model.defer='' disabled>
                                                </div>
                                            </div>
                                            <div class="grid grid-cols-12 whitespace-nowrap">
                                                <div class="col-span-5">Manpower/Special Handling :</div>
                                                <div class="col-span-6">
                                                    <input type="text"
                                                        class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                        name="" wire:model.defer='' disabled>
                                                </div>
                                            </div>
                                            <div class="grid grid-cols-12 whitespace-nowrap">
                                                <div class="col-span-5">Dangerous Goods Fee :</div>
                                                <div class="col-span-6">
                                                    <input type="text"
                                                        class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                        name="" wire:model.defer='' disabled>
                                                </div>
                                            </div>
                                            <div class="grid grid-cols-12 whitespace-nowrap">
                                                <div class="col-span-5">Trucking :</div>
                                                <div class="col-span-6">
                                                    <input type="text"
                                                        class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                        name="" wire:model.defer='' disabled>
                                                </div>
                                            </div>
                                            <div class="grid grid-cols-12 whitespace-nowrap">
                                                <div class="col-span-5">Perishable Fee :</div>
                                                <div class="col-span-6">
                                                    <input type="text"
                                                        class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                        name="" wire:model.defer='' disabled>
                                                </div>
                                            </div>
                                            <div class="grid grid-cols-12 whitespace-nowrap">
                                                <div class="col-span-5">Packaging Fee :</div>
                                                <div class="col-span-6">
                                                    <input type="text"
                                                        class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                        name="" wire:model.defer='' disabled>
                                                </div>
                                            </div>
                                            <div class="grid grid-cols-12 whitespace-nowrap">
                                                <div class="col-span-5">Other Charges :</div>
                                                <div class="col-span-6">
                                                    <input type="text"
                                                        class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                        name="" wire:model.defer='' disabled>
                                                </div>
                                            </div>
                                        @endif
                                        <div class="grid grid-cols-12 whitespace-nowrap">
                                            <div class="col-span-5">Subtotal :</div>
                                            <div class="col-span-6">
                                                <input type="text"
                                                    class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                    name="os_subtotal" wire:model.defer='os_subtotal' disabled>
                                            </div>
                                        </div>
                                        <div class="grid grid-cols-12 whitespace-nowrap">
                                            <div class="col-span-5">Discount Percentage :</div>
                                            <div class="col-span-6">
                                                <input type="text"
                                                    class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                    name="" wire:model.defer='' disabled>
                                            </div>
                                        </div>
                                        <div class="grid grid-cols-12 whitespace-nowrap">
                                            <div class="col-span-5">Discount Amount :</div>
                                            <div class="col-span-6">
                                                <input type="text"
                                                    class="w-full h-8 p-2 border rounded-md cursor-pointer"
                                                    name="" wire:model.defer='' disabled>
                                            </div>
                                        </div>
                                        <div class="grid grid-cols-12 whitespace-nowrap">
                                            <div class="flex col-span-4 gap-4">EVAT :
                                                <div>
                                                    <input type="checkbox"
                                                        class="w-6 h-6 rounded-md border-[#003399] border cursor-pointer"
                                                        name="os_evat" wire:model='os_evat'
                                                        @if ($os_other_fees) checked @endif>
                                                </div>
                                            </div>
                                            <div></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if ($iff)
                        <div class="grid grid-cols-1 bg-white rounded-lg shadow-lg" x-data="{ selected: null }">
                            <div class="flex justify-between py-2 bg-[#003399] rounded-t-lg pl-10 pr-6">
                                <div class="text-xl text-left text-white">International Freight Forwarding (IFF)</div>
                                <button type="button" class="mr-4 text-white"
                                    @click="selected !== {{ $i }} ? selected = {{ $i }} : selected = null">
                                    <span>
                                        <svg x-cloak x-show="selected != '{{ $i }}'" class="w-6 h-6"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
                                            <path fill="currentColor"
                                                d="M137.4 406.6l-128-127.1C3.125 272.4 0 264.2 0 255.1s3.125-16.38 9.375-22.63l128-127.1c9.156-9.156 22.91-11.9 34.88-6.943S192 115.1 192 128v255.1c0 12.94-7.781 24.62-19.75 29.58S146.5 415.8 137.4 406.6z" />
                                        </svg>
                                        <svg x-cloak x-show="selected == '{{ $i }}'" class="w-6 h-6"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                                            <path fill="currentColor"
                                                d="M310.6 246.6l-127.1 128C176.4 380.9 168.2 384 160 384s-16.38-3.125-22.63-9.375l-127.1-128C.2244 237.5-2.516 223.7 2.438 211.8S19.07 192 32 192h255.1c12.94 0 24.62 7.781 29.58 19.75S319.8 237.5 310.6 246.6z" />
                                        </svg>
                                    </span>
                                </button>
                            </div>
                            <div class="grid grid-cols-5 gap-12 px-12 my-6">
                                <div>
                                    <x-label for="" value="Type" :required="true" />
                                    <x-select class="w-full rounded-md cursor-pointer h-11" name=""
                                        wire:model=''>
                                        <option value="">Select</option>
                                        <option value="1">Import</option>
                                        <option value="2">Export</option>
                                    </x-select>
                                    <x-input-error for="" />
                                </div>
                                <div>
                                    <x-label for="" value="Transport Mode" :required="true" />
                                    <x-select class="w-full rounded-md cursor-pointer h-11" name=""
                                        wire:model=''>
                                        <option value="">Select</option>
                                        <option value="1">Air</option>
                                        <option value="2">Sea</option>
                                    </x-select>
                                    <x-input-error for="" />
                                </div>
                                <div>
                                    <x-label for="" value="Loader" :required="true" />
                                    <input class="w-full border border-gray-500 rounded-md cursor-pointer h-11"
                                        name="" wire:model=''>
                                    <x-input-error for="" />
                                </div>
                                <div>
                                    <x-label for="" value="Origin" :required="true" />
                                    <input class="w-full border border-gray-500 rounded-md cursor-pointer h-11"
                                        name="" wire:model=''>
                                    <x-input-error for="" />
                                </div>
                                <div>
                                    <x-label for="" value="Destination" :required="true" />
                                    <input class="w-full border border-gray-500 rounded-md cursor-pointer h-11"
                                        name="" wire:model=''>
                                    <x-input-error for="" />
                                </div>
                            </div>
                            <div class="my-6 border-2 border-gray-400"></div>
                            <div class="py-4">
                                <div class="grid grid-cols-4 gap-12 px-12">
                                    <div>
                                        <x-label for="" value="Particular" :required="true" />
                                        <input class="w-full border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="" wire:model=''>
                                        <x-input-error for="" />
                                    </div>
                                    <div>
                                        <x-label for="" value="Unit Cost" :required="true" />
                                        <input class="w-full border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="" wire:model=''>
                                        <x-input-error for="" />
                                    </div>
                                    <div>
                                        <x-label for="" value="Quantity" :required="true" />
                                        <input class="w-full border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="" wire:model=''>
                                        <x-input-error for="" />
                                    </div>
                                    <div class="flex gap-4">
                                        <div class="w-5/6">
                                            <x-label for="" value="Amount" :required="true" />
                                            <input
                                                class="w-full px-2 border border-gray-500 rounded-md cursor-pointer h-11"
                                                name="" wire:model='' disabled>
                                            <x-input-error for="" />
                                        </div>
                                        <div class="flex pt-8 whitespace-nowrap">
                                            {{-- @if (count($other_services_measurements) > 1)
                                                <button type="button" title="Remove"
                                                    wire:click="removeOtherServicesMeasurement({{ $i }})"
                                                    class="w-6 h-6 px-1 pr-0.5 pt-0.5 text-md flex-none bg-red-600 text-white rounded-full">
                                                    -</button>
                                            @endif
                                            @if (count($other_services_measurements) == ($i += 1)) --}}
                                            <button type="button" title="" wire:click=""
                                                class="w-6 h-6 px-1 pr-0.5 text-md flex-none bg-[#003399] text-white rounded-full">
                                                +
                                            </button>
                                            {{-- @endif --}}
                                        </div>
                                    </div>
                                </div>
                                <div class="grid grid-cols-4 gap-12 px-12 mt-6">
                                    <div></div>
                                    <div></div>
                                    <div class="flex justify-end mt-2 text-gray-600">Sub-Total :</div>
                                    <div class="flex gap-4">
                                        <div class="w-5/6">
                                            <input
                                                class="w-full px-2 border border-gray-500 rounded-md cursor-pointer h-11"
                                                name="" wire:model='' disabled>
                                            <x-input-error for="" />
                                        </div>
                                        <div class="flex gap-2 mt-1">
                                        </div>
                                    </div>
                                </div>
                                <div class="grid grid-cols-4 gap-12 px-12 mt-6">
                                    <div></div>
                                    <div></div>
                                    <div class="flex justify-end mt-2 text-gray-600">VAT :</div>
                                    <div class="flex gap-4">
                                        {{-- @if ($other_services_measurements[$i]['for_crating'] == true) --}}
                                        <svg wire:click="" class="w-12 h-10 py-1 rounded-full text-green"
                                            aria-hidden="true" focusable="false" data-prefix="fas"
                                            data-icon="user-slash" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 580 450">
                                            <path fill="currentColor"
                                                d="M192 64C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192s-86-192-192-192H192zM384 352c-53 0-96-43-96-96s43-96 96-96s96 43 96 96s-43 96-96 96z">
                                            </path>
                                        </svg>
                                        {{-- @else
                                                                        <svg wire:click="isForCreating({{ $i }}, 1)"
                                                                            class="w-16 h-10 py-1 text-gray-400 rounded-full"
                                                                            aria-hidden="true" focusable="false"
                                                                            data-prefix="fas" data-icon="user-slash"
                                                                            role="img"
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                            viewBox="0 0 580 450">
                                                                            <path fill="currentColor"
                                                                                d="M384 128c70.7 0 128 57.3 128 128s-57.3 128-128 128H192c-70.7 0-128-57.3-128-128s57.3-128 128-128H384zM576 256c0-106-86-192-192-192H192C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192zM192 352c53 0 96-43 96-96s-43-96-96-96s-96 43-96 96s43 96 96 96z">
                                                                            </path>
                                                                        </svg>
                                                                    @endif --}}
                                    </div>
                                </div>
                                <div class="grid grid-cols-4 gap-12 px-12 mt-6">
                                    <div></div>
                                    <div></div>
                                    <div class="flex justify-end mt-2 text-gray-600">VAT Amount :</div>
                                    <div class="flex gap-4">
                                        <div class="w-5/6">
                                            <input
                                                class="w-full px-2 border border-gray-500 rounded-md cursor-pointer h-11"
                                                name="" wire:model='' disabled>
                                            <x-input-error for="" />
                                        </div>
                                        <div class="flex gap-2 mt-1">
                                        </div>
                                    </div>
                                </div>
                                <div class="grid grid-cols-4 gap-12 px-12 mt-6">
                                    <div></div>
                                    <div></div>
                                    <div class="flex justify-end mt-2 text-gray-600">Total IFF Amount :</div>
                                    <div class="flex gap-4">
                                        <div class="w-5/6">
                                            <input
                                                class="w-full px-2 border border-gray-500 rounded-md cursor-pointer h-11"
                                                name="" wire:model='' disabled>
                                            <x-input-error for="" />
                                        </div>
                                        <div class="flex gap-2 mt-1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if ($project_shipment)
                        <div class="grid grid-cols-1 bg-white rounded-lg shadow-lg" x-data="{ selected: null }">
                            <div class="flex justify-between py-2 bg-[#003399] rounded-t-lg pl-10 pr-6">
                                <div class="text-xl text-left text-white">Project Shipment</div>
                                <button type="button" class="mr-4 text-white"
                                    @click="selected !== {{ $i }} ? selected = {{ $i }} : selected = null">
                                    <span>
                                        <svg x-cloak x-show="selected != '{{ $i }}'" class="w-6 h-6"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
                                            <path fill="currentColor"
                                                d="M137.4 406.6l-128-127.1C3.125 272.4 0 264.2 0 255.1s3.125-16.38 9.375-22.63l128-127.1c9.156-9.156 22.91-11.9 34.88-6.943S192 115.1 192 128v255.1c0 12.94-7.781 24.62-19.75 29.58S146.5 415.8 137.4 406.6z" />
                                        </svg>
                                        <svg x-cloak x-show="selected == '{{ $i }}'" class="w-6 h-6"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                                            <path fill="currentColor"
                                                d="M310.6 246.6l-127.1 128C176.4 380.9 168.2 384 160 384s-16.38-3.125-22.63-9.375l-127.1-128C.2244 237.5-2.516 223.7 2.438 211.8S19.07 192 32 192h255.1c12.94 0 24.62 7.781 29.58 19.75S319.8 237.5 310.6 246.6z" />
                                        </svg>
                                    </span>
                                </button>
                            </div>
                            <div class="py-4">
                                <div class="grid grid-cols-4 gap-12 px-12">
                                    <div>
                                        <x-label for="" value="Particular" :required="true" />
                                        <input class="w-full border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="" wire:model=''>
                                        <x-input-error for="" />
                                    </div>
                                    <div>
                                        <x-label for="" value="Unit Cost" :required="true" />
                                        <input class="w-full border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="" wire:model=''>
                                        <x-input-error for="" />
                                    </div>
                                    <div>
                                        <x-label for="" value="Quantity" :required="true" />
                                        <input class="w-full border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="" wire:model=''>
                                        <x-input-error for="" />
                                    </div>
                                    <div class="flex gap-4">
                                        <div class="w-5/6">
                                            <x-label for="" value="Amount" :required="true" />
                                            <input
                                                class="w-full px-2 border border-gray-500 rounded-md cursor-pointer h-11"
                                                name="" wire:model='' disabled>
                                            <x-input-error for="" />
                                        </div>
                                        <div class="flex pt-8 whitespace-nowrap">
                                            {{-- @if (count($other_services_measurements) > 1)
                                                <button type="button" title="Remove"
                                                    wire:click="removeOtherServicesMeasurement({{ $i }})"
                                                    class="w-6 h-6 px-1 pr-0.5 pt-0.5 text-md flex-none bg-red-600 text-white rounded-full">
                                                    -</button>
                                            @endif
                                            @if (count($other_services_measurements) == ($i += 1)) --}}
                                            <button type="button" title="" wire:click=""
                                                class="w-6 h-6 px-1 pr-0.5 text-md flex-none bg-[#003399] text-white rounded-full">
                                                +
                                            </button>
                                            {{-- @endif --}}
                                        </div>
                                    </div>
                                </div>
                                <div class="grid grid-cols-4 gap-12 px-12 mt-6">
                                    <div></div>
                                    <div></div>
                                    <div class="flex justify-end mt-2 text-gray-600">Sub-Total :</div>
                                    <div class="flex gap-4">
                                        <div class="w-5/6">
                                            <input
                                                class="w-full px-2 border border-gray-500 rounded-md cursor-pointer h-11"
                                                name="" wire:model='' disabled>
                                            <x-input-error for="" />
                                        </div>
                                        <div class="flex gap-2 mt-1">
                                        </div>
                                    </div>
                                </div>
                                <div class="grid grid-cols-4 gap-12 px-12 mt-6">
                                    <div></div>
                                    <div></div>
                                    <div class="flex justify-end mt-2 text-gray-600">VAT :</div>
                                    <div class="flex gap-4">
                                        {{-- @if ($other_services_measurements[$i]['for_crating'] == true) --}}
                                        <svg wire:click="" class="w-12 h-10 py-1 rounded-full text-green"
                                            aria-hidden="true" focusable="false" data-prefix="fas"
                                            data-icon="user-slash" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 580 450">
                                            <path fill="currentColor"
                                                d="M192 64C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192s-86-192-192-192H192zM384 352c-53 0-96-43-96-96s43-96 96-96s96 43 96 96s-43 96-96 96z">
                                            </path>
                                        </svg>
                                        {{-- @else
                                                                        <svg wire:click="isForCreating({{ $i }}, 1)"
                                                                            class="w-16 h-10 py-1 text-gray-400 rounded-full"
                                                                            aria-hidden="true" focusable="false"
                                                                            data-prefix="fas" data-icon="user-slash"
                                                                            role="img"
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                            viewBox="0 0 580 450">
                                                                            <path fill="currentColor"
                                                                                d="M384 128c70.7 0 128 57.3 128 128s-57.3 128-128 128H192c-70.7 0-128-57.3-128-128s57.3-128 128-128H384zM576 256c0-106-86-192-192-192H192C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192zM192 352c53 0 96-43 96-96s-43-96-96-96s-96 43-96 96s43 96 96 96z">
                                                                            </path>
                                                                        </svg>
                                                                    @endif --}}
                                    </div>
                                </div>
                                <div class="grid grid-cols-4 gap-12 px-12 mt-6">
                                    <div></div>
                                    <div></div>
                                    <div class="flex justify-end mt-2 text-gray-600">VAT Amount :</div>
                                    <div class="flex gap-4">
                                        <div class="w-5/6">
                                            <input
                                                class="w-full px-2 border border-gray-500 rounded-md cursor-pointer h-11"
                                                name="" wire:model='' disabled>
                                            <x-input-error for="" />
                                        </div>
                                        <div class="flex gap-2 mt-1">
                                        </div>
                                    </div>
                                </div>
                                <div class="grid grid-cols-4 gap-12 px-12 mt-6">
                                    <div></div>
                                    <div></div>
                                    <div class="flex justify-end mt-2 text-gray-600">Total Project<br>Shipment Amount
                                        :
                                    </div>
                                    <div class="flex gap-4">
                                        <div class="w-5/6">
                                            <input
                                                class="w-full px-2 border border-gray-500 rounded-md cursor-pointer h-11"
                                                name="" wire:model='' disabled>
                                            <x-input-error for="" />
                                        </div>
                                        <div class="flex gap-2 mt-1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if ($brokerage)
                        <div class="grid grid-cols-1 bg-white rounded-lg shadow-lg" x-data="{ selected: null }">
                            <div class="flex justify-between py-2 bg-[#003399] rounded-t-lg pl-10 pr-6">
                                <div class="text-xl text-left text-white">Brokerage</div>
                                <button type="button" class="mr-4 text-white"
                                    @click="selected !== {{ $i }} ? selected = {{ $i }} : selected = null">
                                    <span>
                                        <svg x-cloak x-show="selected != '{{ $i }}'" class="w-6 h-6"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
                                            <path fill="currentColor"
                                                d="M137.4 406.6l-128-127.1C3.125 272.4 0 264.2 0 255.1s3.125-16.38 9.375-22.63l128-127.1c9.156-9.156 22.91-11.9 34.88-6.943S192 115.1 192 128v255.1c0 12.94-7.781 24.62-19.75 29.58S146.5 415.8 137.4 406.6z" />
                                        </svg>
                                        <svg x-cloak x-show="selected == '{{ $i }}'" class="w-6 h-6"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                                            <path fill="currentColor"
                                                d="M310.6 246.6l-127.1 128C176.4 380.9 168.2 384 160 384s-16.38-3.125-22.63-9.375l-127.1-128C.2244 237.5-2.516 223.7 2.438 211.8S19.07 192 32 192h255.1c12.94 0 24.62 7.781 29.58 19.75S319.8 237.5 310.6 246.6z" />
                                        </svg>
                                    </span>
                                </button>
                            </div>
                            <div class="py-4">
                                <div class="grid grid-cols-4 gap-12 px-12">
                                    <div>
                                        <x-label for="" value="Particular" :required="true" />
                                        <input class="w-full border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="" wire:model=''>
                                        <x-input-error for="" />
                                    </div>
                                    <div>
                                        <x-label for="" value="Unit Cost" :required="true" />
                                        <input class="w-full border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="" wire:model=''>
                                        <x-input-error for="" />
                                    </div>
                                    <div>
                                        <x-label for="" value="Quantity" :required="true" />
                                        <input class="w-full border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="" wire:model=''>
                                        <x-input-error for="" />
                                    </div>
                                    <div class="flex gap-4">
                                        <div class="w-5/6">
                                            <x-label for="" value="Amount" :required="true" />
                                            <input
                                                class="w-full px-2 border border-gray-500 rounded-md cursor-pointer h-11"
                                                name="" wire:model='' disabled>
                                            <x-input-error for="" />
                                        </div>
                                        <div class="flex pt-8 whitespace-nowrap">
                                            {{-- @if (count($other_services_measurements) > 1)
                                                <button type="button" title="Remove"
                                                    wire:click="removeOtherServicesMeasurement({{ $i }})"
                                                    class="w-6 h-6 px-1 pr-0.5 pt-0.5 text-md flex-none bg-red-600 text-white rounded-full">
                                                    -</button>
                                            @endif
                                            @if (count($other_services_measurements) == ($i += 1)) --}}
                                            <button type="button" title="" wire:click=""
                                                class="w-6 h-6 px-1 pr-0.5 text-md flex-none bg-[#003399] text-white rounded-full">
                                                +
                                            </button>
                                            {{-- @endif --}}
                                        </div>
                                    </div>
                                </div>
                                <div class="grid grid-cols-4 gap-12 px-12 mt-6">
                                    <div></div>
                                    <div></div>
                                    <div class="flex justify-end mt-2 text-gray-600">Sub-Total :</div>
                                    <div class="flex gap-4">
                                        <div class="w-5/6">
                                            <input
                                                class="w-full px-2 border border-gray-500 rounded-md cursor-pointer h-11"
                                                name="" wire:model='' disabled>
                                            <x-input-error for="" />
                                        </div>
                                        <div class="flex gap-2 mt-1">
                                        </div>
                                    </div>
                                </div>
                                <div class="grid grid-cols-4 gap-12 px-12 mt-6">
                                    <div></div>
                                    <div></div>
                                    <div class="flex justify-end mt-2 text-gray-600">VAT :</div>
                                    <div class="flex gap-4">
                                        {{-- @if ($other_services_measurements[$i]['for_crating'] == true) --}}
                                        <svg wire:click="" class="w-12 h-10 py-1 rounded-full text-green"
                                            aria-hidden="true" focusable="false" data-prefix="fas"
                                            data-icon="user-slash" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 580 450">
                                            <path fill="currentColor"
                                                d="M192 64C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192s-86-192-192-192H192zM384 352c-53 0-96-43-96-96s43-96 96-96s96 43 96 96s-43 96-96 96z">
                                            </path>
                                        </svg>
                                        {{-- @else
                                                                        <svg wire:click="isForCreating({{ $i }}, 1)"
                                                                            class="w-16 h-10 py-1 text-gray-400 rounded-full"
                                                                            aria-hidden="true" focusable="false"
                                                                            data-prefix="fas" data-icon="user-slash"
                                                                            role="img"
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                            viewBox="0 0 580 450">
                                                                            <path fill="currentColor"
                                                                                d="M384 128c70.7 0 128 57.3 128 128s-57.3 128-128 128H192c-70.7 0-128-57.3-128-128s57.3-128 128-128H384zM576 256c0-106-86-192-192-192H192C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192zM192 352c53 0 96-43 96-96s-43-96-96-96s-96 43-96 96s43 96 96 96z">
                                                                            </path>
                                                                        </svg>
                                                                    @endif --}}
                                    </div>
                                </div>
                                <div class="grid grid-cols-4 gap-12 px-12 mt-6">
                                    <div></div>
                                    <div></div>
                                    <div class="flex justify-end mt-2 text-gray-600">VAT Amount :</div>
                                    <div class="flex gap-4">
                                        <div class="w-5/6">
                                            <input
                                                class="w-full px-2 border border-gray-500 rounded-md cursor-pointer h-11"
                                                name="" wire:model='' disabled>
                                            <x-input-error for="" />
                                        </div>
                                        <div class="flex gap-2 mt-1">
                                        </div>
                                    </div>
                                </div>
                                <div class="grid grid-cols-4 gap-12 px-12 mt-6">
                                    <div></div>
                                    <div></div>
                                    <div class="flex justify-end mt-2 text-gray-600">Total Brokerage<br> Amount :
                                    </div>
                                    <div class="flex gap-4">
                                        <div class="w-5/6">
                                            <input
                                                class="w-full px-2 border border-gray-500 rounded-md cursor-pointer h-11"
                                                name="" wire:model='' disabled>
                                            <x-input-error for="" />
                                        </div>
                                        <div class="flex gap-2 mt-1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if ($ftl)
                        <div class="grid grid-cols-1 bg-white rounded-lg shadow-lg" x-data="{ selected: null }">
                            <div class="flex justify-between py-2 bg-[#003399] rounded-t-lg pl-10 pr-6">
                                <div class="text-xl text-left text-white">Full Truck Load (FTL)</div>
                                <button type="button" class="mr-4 text-white"
                                    @click="selected !== {{ $i }} ? selected = {{ $i }} : selected = null">
                                    <span>
                                        <svg x-cloak x-show="selected != '{{ $i }}'" class="w-6 h-6"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
                                            <path fill="currentColor"
                                                d="M137.4 406.6l-128-127.1C3.125 272.4 0 264.2 0 255.1s3.125-16.38 9.375-22.63l128-127.1c9.156-9.156 22.91-11.9 34.88-6.943S192 115.1 192 128v255.1c0 12.94-7.781 24.62-19.75 29.58S146.5 415.8 137.4 406.6z" />
                                        </svg>
                                        <svg x-cloak x-show="selected == '{{ $i }}'" class="w-6 h-6"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                                            <path fill="currentColor"
                                                d="M310.6 246.6l-127.1 128C176.4 380.9 168.2 384 160 384s-16.38-3.125-22.63-9.375l-127.1-128C.2244 237.5-2.516 223.7 2.438 211.8S19.07 192 32 192h255.1c12.94 0 24.62 7.781 29.58 19.75S319.8 237.5 310.6 246.6z" />
                                        </svg>
                                    </span>
                                </button>
                            </div>
                            <div class="grid grid-cols-5 gap-12 px-12 my-6">
                                <div>
                                    <x-label for="" value="Truck Size" :required="true" />
                                    <x-select class="w-full rounded-md cursor-pointer h-11" name=""
                                        wire:model=''>
                                        <option value="">Select</option>
                                        <option value="1">Import</option>
                                        <option value="2">Export</option>
                                    </x-select>
                                    <x-input-error for="" />
                                </div>
                                <div>
                                    <x-label for="" value="Category" :required="true" />
                                    <x-select class="w-full rounded-md cursor-pointer h-11" name=""
                                        wire:model=''>
                                        <option value="">Select</option>
                                        <option value="1">Air</option>
                                        <option value="2">Sea</option>
                                    </x-select>
                                    <x-input-error for="" />
                                </div>
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                            <div class="py-4">
                                <div class="grid grid-cols-4 gap-12 px-12">
                                    <div>
                                        <x-label for="" value="Particular" :required="true" />
                                        <input class="w-full border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="" wire:model=''>
                                        <x-input-error for="" />
                                    </div>
                                    <div>
                                        <x-label for="" value="Unit Cost" :required="true" />
                                        <input class="w-full border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="" wire:model=''>
                                        <x-input-error for="" />
                                    </div>
                                    <div>
                                        <x-label for="" value="Quantity" :required="true" />
                                        <input class="w-full border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="" wire:model=''>
                                        <x-input-error for="" />
                                    </div>
                                    <div class="flex gap-4">
                                        <div class="w-5/6">
                                            <x-label for="" value="Amount" :required="true" />
                                            <input
                                                class="w-full px-2 border border-gray-500 rounded-md cursor-pointer h-11"
                                                name="" wire:model='' disabled>
                                            <x-input-error for="" />
                                        </div>
                                        <div class="flex pt-8 whitespace-nowrap">
                                            {{-- @if (count($other_services_measurements) > 1)
                                                <button type="button" title="Remove"
                                                    wire:click="removeOtherServicesMeasurement({{ $i }})"
                                                    class="w-6 h-6 px-1 pr-0.5 pt-0.5 text-md flex-none bg-red-600 text-white rounded-full">
                                                    -</button>
                                            @endif
                                            @if (count($other_services_measurements) == ($i += 1)) --}}
                                            <button type="button" title="" wire:click=""
                                                class="w-6 h-6 px-1 pr-0.5 text-md flex-none bg-[#003399] text-white rounded-full">
                                                +
                                            </button>
                                            {{-- @endif --}}
                                        </div>
                                    </div>
                                </div>
                                <div class="grid grid-cols-4 gap-12 px-12 mt-6">
                                    <div></div>
                                    <div></div>
                                    <div class="flex justify-end mt-2 text-gray-600">Sub-Total :</div>
                                    <div class="flex gap-4">
                                        <div class="w-5/6">
                                            <input
                                                class="w-full px-2 border border-gray-500 rounded-md cursor-pointer h-11"
                                                name="" wire:model='' disabled>
                                            <x-input-error for="" />
                                        </div>
                                        <div class="flex gap-2 mt-1">
                                        </div>
                                    </div>
                                </div>
                                <div class="grid grid-cols-4 gap-12 px-12 mt-6">
                                    <div></div>
                                    <div></div>
                                    <div class="flex justify-end mt-2 text-gray-600">VAT :</div>
                                    <div class="flex gap-4">
                                        {{-- @if ($other_services_measurements[$i]['for_crating'] == true) --}}
                                        <svg wire:click="" class="w-12 h-10 py-1 rounded-full text-green"
                                            aria-hidden="true" focusable="false" data-prefix="fas"
                                            data-icon="user-slash" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 580 450">
                                            <path fill="currentColor"
                                                d="M192 64C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192s-86-192-192-192H192zM384 352c-53 0-96-43-96-96s43-96 96-96s96 43 96 96s-43 96-96 96z">
                                            </path>
                                        </svg>
                                        {{-- @else
                                                                        <svg wire:click="isForCreating({{ $i }}, 1)"
                                                                            class="w-16 h-10 py-1 text-gray-400 rounded-full"
                                                                            aria-hidden="true" focusable="false"
                                                                            data-prefix="fas" data-icon="user-slash"
                                                                            role="img"
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                            viewBox="0 0 580 450">
                                                                            <path fill="currentColor"
                                                                                d="M384 128c70.7 0 128 57.3 128 128s-57.3 128-128 128H192c-70.7 0-128-57.3-128-128s57.3-128 128-128H384zM576 256c0-106-86-192-192-192H192C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192zM192 352c53 0 96-43 96-96s-43-96-96-96s-96 43-96 96s43 96 96 96z">
                                                                            </path>
                                                                        </svg>
                                                                    @endif --}}
                                    </div>
                                </div>
                                <div class="grid grid-cols-4 gap-12 px-12 mt-6">
                                    <div></div>
                                    <div></div>
                                    <div class="flex justify-end mt-2 text-gray-600">VAT Amount :</div>
                                    <div class="flex gap-4">
                                        <div class="w-5/6">
                                            <input
                                                class="w-full px-2 border border-gray-500 rounded-md cursor-pointer h-11"
                                                name="" wire:model='' disabled>
                                            <x-input-error for="" />
                                        </div>
                                        <div class="flex gap-2 mt-1">
                                        </div>
                                    </div>
                                </div>
                                <div class="grid grid-cols-4 gap-12 px-12 mt-6">
                                    <div></div>
                                    <div></div>
                                    <div class="flex justify-end mt-2 text-gray-600">Total FTL Amount :
                                    </div>
                                    <div class="flex gap-4">
                                        <div class="w-5/6">
                                            <input
                                                class="w-full px-2 border border-gray-500 rounded-md cursor-pointer h-11"
                                                name="" wire:model='' disabled>
                                            <x-input-error for="" />
                                        </div>
                                        <div class="flex gap-2 mt-1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            @endif
        </div>
        <div class="grid grid-cols-2 gap-5 pt-4 mt-12">
            @if ($type == 1)
                <button type="button" wire:click="compute"
                    class="w-full px-10 py-4 text-xl font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:text-white hover:bg-[#003399]">
                    COMPUTE TRANSACTION
                </button>
                @if ($is_compute)
                    <button type="button" wire:click="action({}, 'redirectToBooking')"
                        class="flex-none w-full px-10 py-4 text-xl font-medium text-white bg-[#003399] rounded-lg">
                        BOOK NOW
                    </button>
                @else
                    <button type="button"
                        class="flex-none w-full px-10 py-4 text-xl font-medium text-gray-700 bg-gray-200 rounded-lg">
                        BOOK NOW
                    </button>
                @endif
            @else
                <button type="button" wire:click="osCompute"
                    class="w-full px-10 py-4 text-xl font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:text-white hover:bg-[#003399]">
                    COMPUTE TRANSACTION
                </button>
                <div class="flex w-full gap-6 px-10 py-4 font-medium bg-white rounded-lg shadow-lg">
                    <div class="text-xl text-gray-700">GRAND TOTAL :</div>
                    <div class="text-2xl text-[#003399]">{{ $os_grand_total }}</div>
                </div>
            @endif
        </div>
        </div>
    </x-slot>
</x-form>
