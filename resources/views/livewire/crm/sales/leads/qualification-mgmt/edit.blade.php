<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
}">
    <x-loading></x-loading>

    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="mb-3 text-xl font-bold text-left text-blue">
                Are you sure you want to submit this Questionnaire?
            </h2>

            <div class="flex justify-end mt-6 space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'edit')"
                    class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                    Cancel
                </button>
                <button type="button" wire:click="submit"
                    class="px-12 py-2 text-xs flex-none bg-[#003399] text-white rounded-lg">
                    Submit
                </button>
            </div>
        </x-slot>
    </x-modal>

    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="mt-5 space-y-3">
            <div class="grid grid-cols-12 gap-4">
                <div class="col-span-12">
                    <x-label for="question" value="Question" :required="true" />
                    <x-textarea rows="3" cols="50" type="text" name="question"
                        wire:model.defer='question'>
                    </x-textarea>
                    <x-input-error for="question" />
                </div>
                <div class="col-span-12">
                    <x-label for="qualification_type" value="Qualification Type" :required="true" />
                    <x-select name="qualification_type" wire:model.defer='qualification_type'>
                        <option value="">Select</option>
                        <option value="1">Domestic</option>
                        <option value="2">International</option>
                    </x-select>
                    <x-input-error for="qualification_type" />
                </div>
            </div>
        </div>
        <div class="flex justify-end gap-3 pt-5 space-x-3">
            <button type="button" wire:click="$emit('close_modal', 'edit')"
                class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">Cancel</button>
            <button type="submit" class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-lg">
                Submit</button>
        </div>
    </form>
</div>
