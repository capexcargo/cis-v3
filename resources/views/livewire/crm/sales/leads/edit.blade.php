<x-form wire:init="load" x-data="{
    search_form: false,
    retire_modal: '{{ $retire_modal }}',
    message_modal: '{{ $message_modal }}',
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>

    <x-slot name="modals">
        @can('crm_sales_leads_retire')
            @if ($lead_id && $retire_modal)
                <x-modal id="retire_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                    <x-slot name="title">Retire a Lead</x-slot>
                    <x-slot name="body">
                        @livewire('crm.sales.leads.retire', ['id' => $lead_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan

        <div wire:key="message_modal" x-show="message_modal" x-cloak
            class="fixed inset-0 z-40 w-full bg-gray-900 bg-opacity-60"
            @keydown.escape.window="$wire.set('message_modal', false)">
            <div class="flex items-center justify-center h-screen py-10 overflow-auto">
                <div class="relative w-11/12 p-4 m-auto bg-white rounded-md shadow-md xs:w-11/12 sm:w-11/12 md:w-1/4">
                    <div class="mt-3">
                        <h2 class="mb-3 text-2xl font-normal text-center">
                            Lead has been successfully saved!
                        </h2>

                        <div class="flex justify-center mt-6 space-x-6">
                            <button type="button" wire:click="okAction"
                                class="px-10 py-2 text-xs flex-none bg-[#003399] text-white rounded-lg">
                                OK
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </x-slot>

    <x-slot name="header_title">
        <span>
            <div class="flex items-start justify-between" wire:click="action({}, 'back')">
                <svg class="w-8 h-8 mt-1 mr-4 text-blue-800 cursor-pointer" aria-hidden="true" focusable="false"
                    data-prefix="far" data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 448 512">
                    <path fill="currentColor"
                        d="M257.5 445.1l-22.2 22.2c-9.4 9.4-24.6 9.4-33.9 0L7 273c-9.4-9.4-9.4-24.6 0-33.9L201.4 44.7c9.4-9.4 24.6-9.4 33.9 0l22.2 22.2c9.5 9.5 9.3 25-.4 34.3L136.6 216H424c13.3 0 24 10.7 24 24v32c0 13.3-10.7 24-24 24H136.6l120.5 114.8c9.8 9.3 10 24.8.4 34.3z" />
                </svg>
                <span class="mt-1 ml-4 text-gray-400">Edit Lead : <span
                        class="ml-2 text-black">{{ $lead_name }}</span></span>
            </div>
        </span>
    </x-slot>
    <x-slot name="header_button">
        @if ($header_type == 1)
            <div x-data="{ open: false }">
                <button class="flex gap-3 p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue" @click="open = !open">
                    <span>Convert Lead</span>
                    <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
                        <path fill="currentColor"
                            d="M310.6 246.6l-127.1 128C176.4 380.9 168.2 384 160 384s-16.38-3.125-22.63-9.375l-127.1-128C.2244 237.5-2.516 223.7 2.438 211.8S19.07 192 32 192h255.1c12.94 0 24.62 7.781 29.58 19.75S319.8 237.5 310.6 246.6z" />
                    </svg>
                </button>
                <div class="absolute border-2 border-gray-300 rounded-sm " style="margin-top: .4%; margin-left: -4.6rem"
                    x-transition:leave="transition ease-in duration-150" x-transition:leave-start="opacity-100"
                    x-transition:leave-end="opacity-0" x-show="open" @click.away="open = false"
                    @keydown.escape="open = false">
                    <ul class="text-gray-600 bg-white rounded shadow">
                        @can('crm_sales_leads_convert')
                            <li class="px-4 py-2 cursor-pointer" x-data="{ open: false }" wire:click="action({},'convert')">
                                Convert to Opportunity
                                <div class="grid grid-cols-1 px-3 py-1 border-b-2 border-gray-400"></div>
                            </li>
                        @endcan
                        @can('crm_sales_leads_retire')
                            <li class="px-4 py-1 cursor-pointer" x-data="{ open: false }"
                                wire:click="action({'id': {{ $lead_id }}},'retire')">
                                Retire
                            </li>
                        @endcan
                    </ul>
                </div>
            </div>
        @else
            <div x-data="{ open: false }">
                <button class="flex gap-3 p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue" @click="open = !open">
                    <span>Management</span>
                    <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
                        <path fill="currentColor"
                            d="M310.6 246.6l-127.1 128C176.4 380.9 168.2 384 160 384s-16.38-3.125-22.63-9.375l-127.1-128C.2244 237.5-2.516 223.7 2.438 211.8S19.07 192 32 192h255.1c12.94 0 24.62 7.781 29.58 19.75S319.8 237.5 310.6 246.6z" />
                    </svg>
                </button>
                <div class="absolute border-2 border-gray-300 rounded-sm "
                    style="margin-top: .4%; margin-left: -13.6rem" x-transition:leave="transition ease-in duration-150"
                    x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0" x-show="open"
                    @click.away="open = false" @keydown.escape="open = false">

                    <ul class="text-gray-600 bg-white rounded shadow">
                        <li class="px-4 py-1 cursor-pointer whitespace-nowrap" x-data="{ open: false }"
                            wire:click="action({},'management')">
                            Qualification Questionnaire Management
                        </li>
                    </ul>
                </div>
            </div>
        @endif
    </x-slot>
    <x-slot name="body">
        <div class="grid grid-cols-1 gap-4">
            <div class="overflow-auto rounded-lg">
                <div class="grid grid-cols-1 gap-4">
                    <div class="grid grid-cols-10 gap-6">
                        <div class="col-span-9">
                            <ul class="flex mt-2">
                                @foreach ($type_header_cards as $i => $type_card)
                                    <li
                                        class="px-3 py-2 text-xs {{ $type_header_cards[$i]['class'] }} {{ $header_type == $type_header_cards[$i]['id'] ? 'bg-blue text-white' : '' }}">
                                        <button wire:click="ActiveType('{{ $type_header_cards[$i]['id'] }}')">
                                            {{ $type_header_cards[$i]['title'] }}
                                        </button>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div @if ($header_type != 1) hidden @endif>
                    <div class="space-y-6 text-sm">
                        <div class="grid grid-cols-12 gap-12">
                            <div class="col-span-5 px-8 py-6 space-y-4 bg-white rounded-lg shadow-md">
                                <div class="grid grid-cols-2 gap-12">
                                    <div>
                                        <x-label for="lead_name" value="Lead Name" :required="true" />
                                        <input class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="lead_name" wire:model='lead_name'>
                                        <x-input-error for="lead_name" />
                                    </div>
                                    <div>
                                        <x-label for="sr_number" value="SR Number" />
                                        <input class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="sr_number" wire:model='sr_number'>
                                        <x-input-error for="sr_number" />
                                    </div>
                                </div>
                                <div class="grid grid-cols-2 gap-12">
                                    <div>
                                        <x-label for="lead_name" value="Shipment Type" :required="true" />
                                        <x-select class="w-full rounded-md cursor-pointer h-11" name="shipment_type"
                                            wire:model='shipment_type'>
                                            <option value="">Select</option>
                                            @foreach ($shipment_type_references as $shipment_type_ref)
                                                <option value="{{ $shipment_type_ref->id }}">
                                                    {{ $shipment_type_ref->name }}
                                                </option>
                                            @endforeach
                                        </x-select>
                                        <x-input-error for="shipment_type" />
                                    </div>
                                    <div>
                                        <x-label for="sr_number" value="Service Requirement" :required="true" />
                                        <x-select class="w-full rounded-md cursor-pointer h-11"
                                            name="service_requirement" wire:model='service_requirement'>
                                            <option value="">Select</option>
                                            @foreach ($service_requirement_references as $service_requirement_ref)
                                                <option value="{{ $service_requirement_ref->id }}">
                                                    {{ $service_requirement_ref->name }}
                                                </option>
                                            @endforeach
                                        </x-select>
                                        <x-input-error for="service_requirement" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-span-7 px-8 py-6 space-y-4 bg-white rounded-lg shadow-md">
                                <div class="grid grid-cols-3 gap-12">
                                    <div>
                                        <x-label for="lead_status" value="Lead Status" />
                                        <input
                                            class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="lead_status" wire:model='lead_status' disabled>
                                        <x-input-error for="lead_status" />
                                    </div>
                                    <div class="flex gap-12">
                                        <div class="whitespace-nowrap">
                                            <x-label for="qualification_score" value="Qualification Score" />
                                            <input
                                                class="w-16 px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                                name="qualification_score" wire:model='qualification_score' disabled>
                                            <x-input-error for="qualification_score" />
                                        </div>
                                        <div class="whitespace-nowrap">
                                            <x-label for="lead_classification" value="Lead Classification"
                                                :required="true" />
                                            <x-select class="rounded-md cursor-pointer w-36 h-11"
                                                name="lead_classification" wire:model='lead_classification'>
                                                <option value="">Select</option>
                                                @foreach ($lead_classification_references as $lead_classification_ref)
                                                    <option value="{{ $lead_classification_ref->id }}">
                                                        {{ $lead_classification_ref->name }}
                                                    </option>
                                                @endforeach
                                            </x-select>
                                            <x-input-error for="lead_classification" />
                                        </div>
                                    </div>
                                </div>
                                <div class="grid grid-cols-3 gap-12">
                                    <div>
                                        <x-label for="deal_size" value="Deal Size" :required="true" />
                                        <input
                                            class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="deal_size" wire:model='deal_size'>
                                        <x-input-error for="deal_size" />
                                    </div>
                                    <div>
                                        <x-label for="actual_amount_converted" class="whitespace-nowrap"
                                            value="Actual Amount Converted" />
                                        <input
                                            class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="actual_amount_converted" wire:model='actual_amount_converted'
                                            disabled>
                                        <x-input-error for="actual_amount_converted" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid grid-cols-12 gap-12 px-8 py-4 bg-white rounded-lg shadow-md">
                            <div class="col-span-5 py-4" {{-- style="border-right: dashed 3px; border-color:rgb(160, 160, 160);" --}}
                                style="border-right: 3px dashed transparent;
                                        border-image: repeating-linear-gradient(to bottom, rgb(173, 173, 173), rgb(173, 173, 173) 12px, transparent 12px, transparent 24px) 1;">
                                <div class="text-2xl font-medium">Contact Person</div>
                                <div class="grid grid-cols-1 pr-8 mt-4 space-y-4">
                                    <div class="pr-8">
                                        <x-label for="contact_person" value="Contact Person" />
                                        <input
                                            class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="contact_person" wire:model='contact_person'>
                                        <x-input-error for="contact_person" />
                                    </div>
                                    <div class="pr-8">
                                        <x-label for="contact_mobile_no" value="Contact Mobile Number"
                                            :required="true" />
                                        <input
                                            class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="contact_mobile_no" wire:model='contact_mobile_no'>
                                        <x-input-error for="contact_mobile_no" />
                                    </div>
                                    <div class="pr-8">
                                        <x-label for="contact_email_add" value="Contact Email Address" />
                                        <input
                                            class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="contact_email_add" wire:model='contact_email_add'>
                                        <x-input-error for="contact_email_add" />
                                    </div>
                                    <div class="pr-8">
                                        <x-label for="description" value="Description" />
                                        <x-textarea
                                            class="w-full h-24 px-4 border border-gray-500 rounded-md cursor-pointer"
                                            name="description" wire:model='description'>
                                        </x-textarea>
                                        <x-input-error for="description" />
                                    </div>
                                    <div class="grid grid-cols-2 gap-6 pr-8">
                                        <div>
                                            <x-label for="contact_owner" value="Contact Owner" />
                                            <input
                                                class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                                name="contact_owner" wire:model='contact_owner' disabled>
                                            <x-input-error for="contact_owner" />
                                        </div>
                                        <div>
                                            <x-label for="customer_type" value="Customer Type" />
                                            <input
                                                class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                                name="customer_type" wire:model='customer_type' disabled>
                                            <x-input-error for="customer_type" />
                                        </div>
                                    </div>
                                    <div class="pr-8">
                                        <x-label for="channel_source" value="Channel Source" :required="true" />
                                        <x-select class="w-full rounded-md cursor-pointer h-11" name="channel_source"
                                            wire:model='channel_source'>
                                            <option value="">Select</option>
                                            @foreach ($channel_source_references as $channel_source_ref)
                                                <option value="{{ $channel_source_ref->id }}">
                                                    {{ $channel_source_ref->name }}
                                                </option>
                                            @endforeach
                                        </x-select>
                                        <x-input-error for="channel_source" />
                                    </div>
                                    <div class="pr-8">
                                        <x-label for="currency" value="Currency" :required="true" />
                                        <x-select class="w-full rounded-md cursor-pointer h-11" name="currency"
                                            wire:model='currency'>
                                            <option value="">Select</option>
                                            <option value="1">PHP</option>
                                            <option value="2">USD</option>
                                            <option value="3">EUR</option>
                                        </x-select>
                                        <x-input-error for="currency" />
                                    </div>
                                    <div class="pr-8">
                                        <x-label for="" value="Attachment" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-span-6 px-6 py-4">
                                <div class="text-2xl font-medium">Contact Address</div>
                                <div class="grid grid-cols-1 pr-8 mt-4 space-y-4">
                                    <div class="flex gap-12 pr-8 mt-1 text-sm font-medium text-gray-500 ">
                                        <div class="flex gap-1">
                                            <input type="radio" class="border-[#003399] border" name="address_type"
                                                wire:model='address_type' value="1">
                                            Domestic
                                        </div>
                                        <div class="flex gap-1">
                                            <input type="radio" class="border-[#003399] border" name="address_type"
                                                wire:model='address_type' value="2">
                                            Internationl
                                        </div>
                                    </div>
                                    <div class="pr-8">
                                        <x-label for="address_line1" value="Address Line 1" :required="true" />
                                        <input
                                            class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="address_line1" wire:model='address_line1'>
                                        <x-input-error for="address_line1" />
                                    </div>
                                    <div class="pr-8">
                                        <x-label for="address_line2" value="Address Line 2" />
                                        <input
                                            class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="address_line2" wire:model='address_line2'>
                                        <x-input-error for="address_line2" />
                                    </div>
                                    <div class="grid grid-cols-2 gap-6 pr-8">
                                        <div>
                                            <x-label for="state_province" value="State/ Province" :required="true" />
                                            <x-select class="w-full rounded-md cursor-pointer h-11"
                                                name="state_province" wire:model='state_province'>
                                                <option value="">Select</option>
                                                <option value="1">State Province 1</option>
                                            </x-select>
                                            <x-input-error for="state_province" />
                                        </div>
                                        <div>
                                            <x-label for="city_municipality" value="City/ Municipality"
                                                :required="true" />
                                            <x-select class="w-full rounded-md cursor-pointer h-11"
                                                name="city_municipality" wire:model='city_municipality'>
                                                <option value="">Select</option>
                                                <option value="1">City Province 1</option>
                                            </x-select>
                                            <x-input-error for="city_municipality" />
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-2 gap-6 pr-8">
                                        <div>
                                            <x-label for="barangay" value="Barangay" :required="true" />
                                            <x-select class="w-full rounded-md cursor-pointer h-11" name="barangay"
                                                wire:model='barangay'>
                                                <option value="">Select</option>
                                                <option value="1">Barangay 1</option>
                                            </x-select>
                                            <x-input-error for="barangay" />
                                        </div>
                                        <div>
                                            <x-label for="postal_code" value="Postal Code" />
                                            <input
                                                class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                                name="postal_code" wire:model='postal_code' disabled>
                                            <x-input-error for="postal_code" />
                                        </div>
                                    </div>
                                </div>
                                <div class="grid grid-cols-1 pr-8" style="margin-top:16rem">
                                    <div class="flex justify-end gap-6 pr-8">
                                        <button type="button" wire:click=""
                                            class="px-10 py-2 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:text-white hover:bg-red-400">
                                            Cancel
                                        </button>
                                        <button type="submit"
                                            class="px-10 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg">
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div @if ($header_type != 2) hidden @endif>
                    <div class="space-y-6">
                        <div class="grid grid-cols-12 gap-12">
                            <div class="col-span-12">
                                <div class="flex gap-6">
                                    <div
                                        class="px-8 py-6 space-y-6 bg-white rounded-lg shadow-md sm:col-span-8 md:col-span-8 lg:col-span-7 xl:col-span-7">
                                        <div class="grid grid-cols-4 gap-4">
                                            <div class="col-span-2 mt-[1px] text-gray-400 whitespace-nowrap">
                                                Qualification Type :</div>
                                            <div class="col-span-2 text-lg font-medium whitespace-nowrap">Domestic
                                                Services</div>
                                        </div>
                                        <div class="grid grid-cols-4 gap-4">
                                            <div class="col-span-2 mt-[1px] text-gray-400 whitespace-nowrap">Status :
                                            </div>
                                            <div class="col-span-2 text-lg font-medium whitespace-nowrap">Completed
                                            </div>
                                        </div>
                                        <div class="grid grid-cols-4 gap-4">
                                            <div class="col-span-2 mt-[1px] text-gray-400 whitespace-nowrap">Questions
                                                Completed :</div>
                                            <div class="col-span-2 text-lg font-medium whitespace-nowrap">
                                                {{ count($count_response) }} out of
                                                {{ count($qualifications_questionnaires) }}</div>
                                        </div>
                                        <div class="grid grid-cols-4 gap-4">
                                            <div class="col-span-2 mt-[1px] text-gray-400 whitespace-nowrap">Last
                                                Updated :</div>
                                            <div class="col-span-2 text-lg font-medium whitespace-nowrap">
                                                {{ $last_updated }}</div>
                                        </div>
                                    </div>
                                    <div
                                        class="col-span-4 px-8 pb-2 text-center bg-white rounded-lg shadow-md sm:w-1/4 sm:h-1/4 md:w-1/5 md:h-1/5 lg:w-1/4 lg:h-1/4">
                                        <div class="grid grid-cols-1">
                                            <canvas class="w-full h-full p-1" id="chartDonut"></canvas>
                                            <div class="mt-2 text-lg font-medium">
                                                <span class="">Qualification Score
                                                    <span
                                                        class="text-xl text-[#3CFC2E] font-semibold">{{ $res_qualification_score }}%</span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid grid-cols-1 py-4 space-y-4">
                        <div class="text-lg font-medium">Domestic Services</div>
                        <div class="bg-white rounded-lg shadow-md ">
                            <x-table.table>
                                <x-slot name="thead">
                                    <x-table.th name="No." class="w-16" />
                                    <x-table.th name="Questions" class="w-2/5" />
                                    <x-table.th name="Response" class="w-1/6" />
                                    <x-table.th name="Remarks" />
                                </x-slot>
                                <x-slot name="tbody">
                                    @foreach ($qualifications_questionnaires as $i => $questionnaire)
                                        <tr class="border-0 cursor-pointer hover:bg-[#4068b8]">
                                            <td class="p-3 whitespace-nowrap">
                                                {{ $qualifications_questionnaires[$i]['question_id'] }}.
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                {{ $qualifications_questionnaires[$i]['question'] }}.
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                <x-select class="w-3/5 h-10 rounded-md cursor-pointer hover:text-black"
                                                    name="qualifications_questionnaires.{{ $i }}.response"
                                                    wire:model.defer='qualifications_questionnaires.{{ $i }}.response'>
                                                    <option value="">Select</option>
                                                    <option value="1">Yes</option>
                                                    <option value="2">No</option>
                                                </x-select>
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                <input
                                                    class="w-3/4 h-10 px-4 border border-gray-500 rounded-md cursor-pointer"
                                                    name="qualifications_questionnaires.{{ $i }}.remarks"
                                                    wire:model.defer='qualifications_questionnaires.{{ $i }}.remarks'>
                                            </td>
                                        </tr>
                                    @endforeach
                                </x-slot>
                            </x-table.table>
                        </div>
                        <div class="grid grid-cols-2 gap-3 pt-5 mt-4 space-x-3">
                            <button type="button" wire:click=""
                                class="px-10 py-4 font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg text-md hover:bg-blue-100">
                                CANCEL</button>
                            <button type="button" wire:click="action({},'save_qualifications')" id="save_edit"
                                class="px-10 py-4 text-md flex-none bg-[#003399] text-white rounded-lg">
                                SAVE</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script type="text/javascript">
        function renderPieChart(res_qualification_score, diff_percent) {
            const dataDonut = {
                datasets: [{
                    label: "",
                    data: [res_qualification_score, diff_percent],
                    backgroundColor: [
                        "#3CFC2E",
                        "#EFEFEF",
                    ],
                    hoverOffset: 4,
                }],
            };

            const configDonut = {
                type: "doughnut",
                data: dataDonut,
                options: {
                    pieHole: 2,
                    pieSliceTextStyle: {
                        color: 'black',
                    },
                    legend: 'none'
                }
            };

            // Destroy the existing Chart instance if it exists
            if (typeof chartBar !== 'undefined') {
                chartBar.destroy();
            }

            // Create the new Chart instance
            chartBar = new Chart(document.getElementById("chartDonut"), configDonut);
        }

        document.addEventListener('livewire:load', function() {
            Livewire.on('dataLoaded', function(res_qualification_score, diff_percent) {
                renderPieChart(res_qualification_score, diff_percent);
            });

            // Call the renderPieChart function on Livewire load
            renderPieChart({{ $res_qualification_score }}, {{ $diff_percent }});
        });
    </script>
@endpush
