<x-form wire:init="" x-data="{
    search_form: false,
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>

    <x-slot name="modals">
    </x-slot>

    <x-slot name="header_title">Leads</x-slot>
    <x-slot name="header_button">
        {{-- <button wire:click="action({}, '')" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
            Charge Management
        </button> --}}
    </x-slot>

    <x-slot name="body">
        <div class="flex gap-6">
            <button
                class="flex justify-between gap-24 px-4 py-4 bg-white border-2 border-[#003399] rounded-lg hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
                <h5 class="font-medium text-left text-gray-700 uppercase dark:text-gray-40">ALL LEADS</h5>
                <h5 class="text-3xl font-medium text-gray-700 dark:text-gray-400">{{ $all_leads }}</h5>
            </button>
            <button
                class="flex justify-between gap-24 px-4 py-4 bg-white border border-[#003399] rounded-lg hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
                <h5 class="font-medium text-left text-gray-700 uppercase dark:text-gray-400">MY OPEN<br>LEADS</h5>
                <h5 class="text-3xl font-medium text-gray-700 dark:text-gray-400">{{ $my_open_leads }}</h5>
            </button>
            <button
                class="flex justify-between gap-24 px-4 py-4 bg-white border border-[#003399] rounded-lg hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
                <h5 class="font-medium text-left text-gray-700 uppercase dark:text-gray-400">MY TEAM'S<br>LEADS</h5>
                <h5 class="text-3xl font-medium text-gray-700 dark:text-gray-400">{{ $my_teams_leads }}</h5>
            </button>
        </div>
        <div class="flex gap-12" style="margin-top: 3rem">
            <div class="w-48">
                <x-transparent.input label="Lead Name" wire:model.defer='' name="">
                </x-transparent.input>
            </div>
            <div class="w-48">
                <x-transparent.input label="Sr Number" wire:model.defer='' name="">
                </x-transparent.input>
            </div>
            <div class="w-48">
                <x-transparent.input type="date" value="" label="Date Range" name=""
                    wire:model.defer="">
                </x-transparent.input>
            </div>
            <div class="">
                <x-button type="button" wire:click="search" title="Search"
                    class="text-white bg-blue hover:bg-blue-800" />
            </div>
        </div>
        <div class="grid grid-cols-1 gap-4 mt-4">
            <div class="overflow-auto rounded-lg">
                <div class="grid grid-cols-1 gap-4 mt-4">
                    <div class="grid grid-cols-10 gap-6">
                        <div class="col-span-9">
                            <ul class="flex mt-2">
                                @foreach ($status_header_cards as $i => $status_card)
                                    <li wire:click="ActiveType('{{ $status_header_cards[$i]['id'] }}')"
                                        class="px-3 py-2 text-xs cursor-pointer {{ $status_header_cards[$i]['class'] }} {{ $status == $status_header_cards[$i]['id'] ? 'bg-blue text-white' : '' }}">
                                        <button>
                                            {{ $status_header_cards[$i]['title'] }}
                                        </button>
                                        <div class="text-md">{{ $status_header_cards[$i]['value'] }}</div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="-mt-4 bg-white rounded-lg shadow-md">
                        <x-table.table>
                            <x-slot name="thead">
                                <x-table.th name="No." />
                                <x-table.th name="Lead Classification" />
                                <x-table.th name="Lead Name" />
                                <x-table.th name="SR Number" />
                                <x-table.th name="Shipment Type" />
                                <x-table.th name="Industry" />
                                <x-table.th name="Customer Category" />
                                <x-table.th name="Channel Source" />
                                <th class='p-3 tracking-wider text-left border-2 border-t-0 border-b-0 border-gray-500 whitespace-nowrap'
                                    scope="col">
                                    <div class="font-medium w-28">
                                        Deal Size
                                    </div>
                                </th>
                                <th class='p-3 tracking-wider text-left border-2 border-t-0 border-b-0 border-gray-500'
                                    scope="col">
                                    <div class="w-32 font-medium">
                                        Actual Amount Converted
                                    </div>
                                </th>
                                <x-table.th name="Lead Status" />
                                <x-table.th name="Lead Conversion Date" />
                                <x-table.th name="Created By" />
                                <x-table.th name="Date Converted" />
                                <x-table.th name="Date Retired" />
                                <x-table.th name="Ageing Days" />
                            </x-slot>
                            <x-slot name="tbody">
                                @foreach ($leads as $i => $lead)
                                    <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                        <td class="p-3 whitespace-nowrap">
                                            {{ ($leads->currentPage() - 1) * $leads->links()->paginator->perPage() + $loop->iteration }}.
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <div
                                                class="flex gap-2 justify-center rounded-full w-24 px-4 py-1 
                                                {{ $lead->lead_classification_id == 1
                                                    ? 'bg-blue-100 text-[#00A8FF]'
                                                    : ($lead->lead_classification_id == 2
                                                        ? 'bg-[#feeada] text-[#FF8800]'
                                                        : 'bg-red-100 text-[#E50000]') }}">

                                                @if ($lead->lead_classification_id == 1)
                                                    <svg class="w-5 h-5" aria-hidden="true" focusable="false"
                                                        data-prefix="fas" ata-icon="users-cog" role="img"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                        <path fill="currentColor"
                                                            d="M224 0c17.7 0 32 14.3 32 32V62.1l15-15c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9l-49 49v70.3l61.4-35.8 17.7-66.1c3.4-12.8 16.6-20.4 29.4-17s20.4 16.6 17 29.4l-5.2 19.3 23.6-13.8c15.3-8.9 34.9-3.7 43.8 11.5s3.8 34.9-11.5 43.8l-25.3 14.8 21.7 5.8c12.8 3.4 20.4 16.6 17 29.4s-16.6 20.4-29.4 17l-67.7-18.1L287.5 256l60.9 35.5 67.7-18.1c12.8-3.4 26 4.2 29.4 17s-4.2 26-17 29.4l-21.7 5.8 25.3 14.8c15.3 8.9 20.4 28.5 11.5 43.8s-28.5 20.4-43.8 11.5l-23.6-13.8 5.2 19.3c3.4 12.8-4.2 26-17 29.4s-26-4.2-29.4-17l-17.7-66.1L256 311.7v70.3l49 49c9.4 9.4 9.4 24.6 0 33.9s-24.6 9.4-33.9 0l-15-15V480c0 17.7-14.3 32-32 32s-32-14.3-32-32V449.9l-15 15c-9.4 9.4-24.6 9.4-33.9 0s-9.4-24.6 0-33.9l49-49V311.7l-61.4 35.8-17.7 66.1c-3.4 12.8-16.6 20.4-29.4 17s-20.4-16.6-17-29.4l5.2-19.3L48.1 395.6c-15.3 8.9-34.9 3.7-43.8-11.5s-3.7-34.9 11.5-43.8l25.3-14.8-21.7-5.8c-12.8-3.4-20.4-16.6-17-29.4s16.6-20.4 29.4-17l67.7 18.1L160.5 256 99.6 220.5 31.9 238.6c-12.8 3.4-26-4.2-29.4-17s4.2-26 17-29.4l21.7-5.8L15.9 171.6C.6 162.7-4.5 143.1 4.4 127.9s28.5-20.4 43.8-11.5l23.6 13.8-5.2-19.3c-3.4-12.8 4.2-26 17-29.4s26 4.2 29.4 17l17.7 66.1L192 200.3V129.9L143 81c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l15 15V32c0-17.7 14.3-32 32-32z" />
                                                    </svg>
                                                @elseif ($lead->lead_classification_id == 2)
                                                    <svg class="w-5 h-5" aria-hidden="true" focusable="false"
                                                        data-prefix="fas" ata-icon="users-cog" role="img"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                        <path fill="currentColor"
                                                            d="M269.5 69.9c11.1-7.9 25.9-7.9 37 0C329 85.4 356.5 96 384 96c26.9 0 55.4-10.8 77.4-26.1l0 0c11.9-8.5 28.1-7.8 39.2 1.7c14.4 11.9 32.5 21 50.6 25.2c17.2 4 27.9 21.2 23.9 38.4s-21.2 27.9-38.4 23.9c-24.5-5.7-44.9-16.5-58.2-25C449.5 149.7 417 160 384 160c-31.9 0-60.6-9.9-80.4-18.9c-5.8-2.7-11.1-5.3-15.6-7.7c-4.5 2.4-9.7 5.1-15.6 7.7c-19.8 9-48.5 18.9-80.4 18.9c-33 0-65.5-10.3-94.5-25.8c-13.4 8.4-33.7 19.3-58.2 25c-17.2 4-34.4-6.7-38.4-23.9s6.7-34.4 23.9-38.4C42.8 92.6 61 83.5 75.3 71.6c11.1-9.5 27.3-10.1 39.2-1.7l0 0C136.7 85.2 165.1 96 192 96c27.5 0 55-10.6 77.5-26.1zm37 288C329 373.4 356.5 384 384 384c26.9 0 55.4-10.8 77.4-26.1l0 0c11.9-8.5 28.1-7.8 39.2 1.7c14.4 11.9 32.5 21 50.6 25.2c17.2 4 27.9 21.2 23.9 38.4s-21.2 27.9-38.4 23.9c-24.5-5.7-44.9-16.5-58.2-25C449.5 437.7 417 448 384 448c-31.9 0-60.6-9.9-80.4-18.9c-5.8-2.7-11.1-5.3-15.6-7.7c-4.5 2.4-9.7 5.1-15.6 7.7c-19.8 9-48.5 18.9-80.4 18.9c-33 0-65.5-10.3-94.5-25.8c-13.4 8.4-33.7 19.3-58.2 25c-17.2 4-34.4-6.7-38.4-23.9s6.7-34.4 23.9-38.4c18.1-4.2 36.2-13.3 50.6-25.2c11.1-9.4 27.3-10.1 39.2-1.7l0 0C136.7 373.2 165.1 384 192 384c27.5 0 55-10.6 77.5-26.1c11.1-7.9 25.9-7.9 37 0zm0-144C329 229.4 356.5 240 384 240c26.9 0 55.4-10.8 77.4-26.1l0 0c11.9-8.5 28.1-7.8 39.2 1.7c14.4 11.9 32.5 21 50.6 25.2c17.2 4 27.9 21.2 23.9 38.4s-21.2 27.9-38.4 23.9c-24.5-5.7-44.9-16.5-58.2-25C449.5 293.7 417 304 384 304c-31.9 0-60.6-9.9-80.4-18.9c-5.8-2.7-11.1-5.3-15.6-7.7c-4.5 2.4-9.7 5.1-15.6 7.7c-19.8 9-48.5 18.9-80.4 18.9c-33 0-65.5-10.3-94.5-25.8c-13.4 8.4-33.7 19.3-58.2 25c-17.2 4-34.4-6.7-38.4-23.9s6.7-34.4 23.9-38.4c18.1-4.2 36.2-13.3 50.6-25.2c11.1-9.5 27.3-10.1 39.2-1.7l0 0C136.7 229.2 165.1 240 192 240c27.5 0 55-10.6 77.5-26.1c11.1-7.9 25.9-7.9 37 0z" />
                                                    </svg>
                                                @elseif ($lead->lead_classification_id == 3)
                                                    <svg class="w-5 h-5" aria-hidden="true" focusable="false"
                                                        data-prefix="fas" ata-icon="users-cog" role="img"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                        <path fill="currentColor"
                                                            d="M159.3 5.4c7.8-7.3 19.9-7.2 27.7 .1c27.6 25.9 53.5 53.8 77.7 84c11-14.4 23.5-30.1 37-42.9c7.9-7.4 20.1-7.4 28 .1c34.6 33 63.9 76.6 84.5 118c20.3 40.8 33.8 82.5 33.8 111.9C448 404.2 348.2 512 224 512C98.4 512 0 404.1 0 276.5c0-38.4 17.8-85.3 45.4-131.7C73.3 97.7 112.7 48.6 159.3 5.4zM225.7 416c25.3 0 47.7-7 68.8-21c42.1-29.4 53.4-88.2 28.1-134.4c-4.5-9-16-9.6-22.5-2l-25.2 29.3c-6.6 7.6-18.5 7.4-24.7-.5c-16.5-21-46-58.5-62.8-79.8c-6.3-8-18.3-8.1-24.7-.1c-33.8 42.5-50.8 69.3-50.8 99.4C112 375.4 162.6 416 225.7 416z" />
                                                    </svg>
                                                @endif
                                                {{ $lead->classification->name }}
                                            </div>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <p class="text-[#003399] underline"
                                                wire:click="action({'id': {{ $lead['id'] }}}, 'edit')">
                                                {{ $lead->lead_name }}</p>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <p class="text-[#003399] underline">{{ $lead->sr_no }}</p>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $lead->shipmentType->name ?? null }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $lead->account->industry->name ?? null }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            New
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $lead->channelSource->name ?? null }}
                                        </td>
                                        <td
                                            class="p-3 border-2 border-t-0 border-b-0 border-gray-500 whitespace-nowrap">
                                            Php {{ number_format($lead->deal_size ?? null) }}

                                        </td>
                                        <td
                                            class="p-3 border-2 border-t-0 border-b-0 border-gray-500 whitespace-nowrap">
                                            @if ($lead->lead_status_id == 3)
                                                <p wire:click="action({'id': {{ $lead['id'] }}}, 'redirectToOpportunity')"
                                                    class="text-[#003399] underline">
                                                    Php 46,000
                                                </p>
                                            @endif
                                        </td>
                                        <td
                                            class="p-3 whitespace-nowrap {{ $lead->lead_status_id == 2 || $lead->lead_status_id == 4 ? 'text-red' : '' }}">
                                            {{ $lead->leadStatus->name ?? null }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $lead->createdBy->name ?? null }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ date('M. d, Y', strtotime($lead->created_at)) }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                        </td>
                                    </tr>
                                @endforeach
                            </x-slot>
                        </x-table.table>
                        <div class="px-1 pb-2">
                            {{ $leads->links() }}
                        </div>
                    </div>
                </div>
            </div>
    </x-slot>
</x-form>
