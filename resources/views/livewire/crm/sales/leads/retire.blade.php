<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
}">
    <x-loading></x-loading>

    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="mb-3 text-xl font-bold text-left text-blue">
                Are you sure you want to retire this lead?
            </h2>

            <div class="flex justify-end mt-6 space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                    No
                </button>
                <button type="button" wire:click="submit"
                    class="px-12 py-2 text-xs flex-none bg-[#003399] text-white rounded-lg">
                    yes
                </button>
            </div>
        </x-slot>
    </x-modal>

    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">
        <div class="mt-5 space-y-3">
            <div class="grid grid-cols-12 gap-4">
                <div class="col-span-12" wire:init="retirementReasonReferences">
                    <x-label for="retirement_reason" value="Retirement Reason" :required="true" />
                    <x-select name="retirement_reason" wire:model.defer='retirement_reason'>
                        <option value="">Select</option>
                        @foreach ($retirement_reason_references as $retirement_reason_ref)
                            <option value="{{ $retirement_reason_ref->id }}">
                                {{ $retirement_reason_ref->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="retirement_reason" />
                </div>
                <div class="col-span-12">
                    <x-label for="remarks" value="Remarks/Comments" />
                    <x-textarea rows="3" cols="50" type="text" name="remarks" wire:model.defer='remarks'>
                    </x-textarea>
                    <x-input-error for="remarks" />
                </div>
            </div>
        </div>
        <div class="flex justify-end gap-3 pt-5 mt-4 space-x-3">
            <button type="button" wire:click="$emit('close_modal', 'retire')"
                class="px-10 py-2 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">Cancel</button>

            <button type="submit" class="px-10 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg">
                Retire</button>
        </div>
    </form>
</div>
