<div class="px-6 pb-6 text-gray-800" x-data="{
    generate_quotation_modal: '{{ $generate_quotation_modal }}',
}">
    <x-loading></x-loading>
    @if ($quot_id && $from_sr && $generate_quotation_modal)
        <x-modal id="generate_quotation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-3/5">
            <x-slot name="body">
                <span class="relative block">
                    <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                        wire:click="$set('generate_quotation_modal', false)">
                    </span>
                </span>
                <div class="px-10">
                    <div class="grid grid-cols-1 py-10">
                        <div class="flex justify-end gap-6">
                            <button
                                class="px-6 py-3 mt-2 text-black bg-gray-200 border border-gray-400 rounded-md shadow-md text-md hover:bg-gray-400">
                                <div class="flex items-start justify-between md:flex">
                                    <svg wire:click="action({'id': ''}, '')" class="w-4 h-4 mt-1 mr-4"
                                        aria-hidden="true" focusable="false" data-prefix="far" data-icon="print-alt"
                                        role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                        <path fill="currentColor"
                                            d="M288 32c0-17.7-14.3-32-32-32s-32 14.3-32 32V274.7l-73.4-73.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3l128 128c12.5 12.5 32.8 12.5 45.3 0l128-128c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L288 274.7V32zM64 352c-35.3 0-64 28.7-64 64v32c0 35.3 28.7 64 64 64H448c35.3 0 64-28.7 64-64V416c0-35.3-28.7-64-64-64H346.5l-45.3 45.3c-25 25-65.5 25-90.5 0L165.5 352H64zm368 56a24 24 0 1 1 0 48 24 24 0 1 1 0-48z" />
                                    </svg>Download
                                </div>
                            </button>
                            <a target="_blank"
                                href="{{ route('crm.sales.print-generated-quotation', ['id' => $quot_id, 'from_sr' => $from_sr]) }}"
                                class="px-6 py-3 mt-2 bg-gray-200 border border-gray-400 rounded-md shadow-md text-md hover:bg-gray-400">
                                <div class="flex items-start justify-between md:flex">
                                    <svg class="w-4 h-4 mt-1 mr-4" aria-hidden="true" focusable="false"
                                        data-prefix="far" data-icon="print-alt" role="img"
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                        <path fill="currentColor"
                                            d="M448 192H64C28.65 192 0 220.7 0 256v96c0 17.67 14.33 32 32 32h32v96c0 17.67 14.33 32 32 32h320c17.67 0 32-14.33 32-32v-96h32c17.67 0 32-14.33 32-32V256C512 220.7 483.3 192 448 192zM384 448H128v-96h256V448zM432 296c-13.25 0-24-10.75-24-24c0-13.27 10.75-24 24-24s24 10.73 24 24C456 285.3 445.3 296 432 296zM128 64h229.5L384 90.51V160h64V77.25c0-8.484-3.375-16.62-9.375-22.62l-45.25-45.25C387.4 3.375 379.2 0 370.8 0H96C78.34 0 64 14.33 64 32v128h64V64z" />
                                    </svg>Print
                                </div>
                            </a>
                        </div>
                    </div>
                    @livewire('crm.sales.quotation.quotation-modal', ['id' => $quot_id, 'from_sr' => $from_sr])
                </div>
            </x-slot>
        </x-modal>
    @endif
    <div class="space-y-4 bg-white rounded-lg shadow-md">
        <div class="p-6 mt-6 space-y-6">
            <div class="flex gap-4 mt-4">
                <div class="flex items-start justify-between" wire:click="action({}, 'back')">
                    <svg class="w-8 h-8 mt-1 mr-4 text-blue-800 cursor-pointer" aria-hidden="true" focusable="false"
                        data-prefix="far" data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M257.5 445.1l-22.2 22.2c-9.4 9.4-24.6 9.4-33.9 0L7 273c-9.4-9.4-9.4-24.6 0-33.9L201.4 44.7c9.4-9.4 24.6-9.4 33.9 0l22.2 22.2c9.5 9.5 9.3 25-.4 34.3L136.6 216H424c13.3 0 24 10.7 24 24v32c0 13.3-10.7 24-24 24H136.6l120.5 114.8c9.8 9.3 10 24.8.4 34.3z" />
                    </svg>
                    <span class="mt-1 ml-4 text-3xl font-semibold">Quotation</span>
                </div>
            </div>
            <div class="grid grid-cols-1 px-12 space-y-6" style="padding-right: 20rem">
                <div class="grid grid-cols-2 gap-24 px-6">
                    <div>
                        <x-label for="date" value="Date" />
                        <input type="date" class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                            name="date" wire:model='date' disabled>
                        <x-input-error for="date" />
                    </div>
                    <div>
                        <x-label for="quotation_ref_no" value="Quotation Ref. No." />
                        <input type="text" class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                            name="quotation_ref_no" wire:model='quotation_ref_no' disabled>
                        <x-input-error for="quotation_ref_no" />
                    </div>
                </div>
                <div class="grid grid-cols-2 gap-24 px-6">
                    <div wire:init="shipmentTypeReferences">
                        <x-label for="date" value="Shipment Type" :required="true" />
                        <select class="w-full rounded-md cursor-pointer h-11" name="shipment_type"
                            wire:model='shipment_type'>
                            <option value="">Select</option>
                            @foreach ($shipment_type_references as $shipment_type_ref)
                                <option value="{{ $shipment_type_ref->id }}">
                                    {{ $shipment_type_ref->name }}
                                </option>
                            @endforeach
                        </select>
                        <x-input-error for="shipment_type" />
                    </div>
                    <div>
                        <x-label for="sr_number" value="SR Number" />
                        <input type="text" class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                            name="sr_number" wire:model='sr_number' disabled>
                        <x-input-error for="sr_number" />
                    </div>
                </div>
                <div class="grid grid-cols-1 gap-24 px-6">
                    <div>
                        <x-label for="subject" value="Subject" :require="true" />
                        <input type="texts" class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                            name="subject" wire:model='subject'>
                        <x-input-error for="subject" />
                    </div>
                </div>
            </div>
        </div>

        <div class="grid grid-cols-1">
            <div class="bg-[#003399] text-white px-12 py-2 w-60 whitespace-nowrap"
                style="border-radius: 3px 60px 3px 3px;">Customer
                Details</div>
            <div class="border-b-2 border-[#003399]"></div>
            <div class="px-12 py-6 space-y-2">
                <div class="grid grid-cols-1 px-12" style="padding-right: 20rem">
                    <div class="grid w-auto grid-cols-12">
                        <div class="col-span-3 text-[#003399]">Customer Number :</div>
                        <div class="col-span-8 font-semibold text-md">0001</div>
                    </div>
                </div>
                <div class="grid grid-cols-1 px-12" style="padding-right: 20rem">
                    <div class="grid w-auto grid-cols-12">
                        <div class="col-span-3 text-[#003399]">Customer Name :</div>
                        <div class="col-span-8 font-semibold text-md">
                            {{ $company_business_name == null ? $primary_contact_person : $company_business_name }}
                        </div>
                    </div>
                </div>
                <div class="grid grid-cols-1 px-12" style="padding-right: 20rem">
                    <div class="grid w-auto grid-cols-12">
                        <div class="col-span-3 text-[#003399]">Company Name :</div>
                        <div class="col-span-8 font-semibold text-md">{{ $company_business_name }}</div>
                    </div>
                </div>
                <div class="grid grid-cols-1 px-12" style="padding-right: 20rem">
                    <div class="grid w-auto grid-cols-12">
                        <div class="col-span-3 text-[#003399]">Contact Person :</div>
                        <div class="col-span-8 font-semibold text-md">{{ $primary_contact_person }}</div>
                    </div>
                </div>
                <div class="grid grid-cols-1 px-12" style="padding-right: 20rem">
                    <div class="grid w-auto grid-cols-12">
                        <div class="col-span-3 text-[#003399]">Position :</div>
                        <div class="col-span-8 font-semibold text-md">{{ $position }}</div>
                    </div>
                </div>
                <div class="grid grid-cols-1 px-12" style="padding-right: 20rem">
                    <div class="grid w-auto grid-cols-12">
                        <div class="col-span-3 text-[#003399]">Address :</div>
                        <div class="col-span-8 font-semibold text-md">{{ $address_line_1 }}@if ($address_line_1 != null)
                                ,
                            @endif {{ $barangay }}
                            {{ $city_municipal }} @if ($city_municipal != null)
                                ,
                            @endif
                            {{ $state_province }} {{ $postal }}</div>
                    </div>
                </div>
                <div class="grid grid-cols-1 px-12" style="padding-right: 20rem">
                    <div class="grid w-auto grid-cols-12">
                        <div class="col-span-3 text-[#003399]">Mobile Number :</div>
                        <div class="col-span-8 font-semibold text-md">{{ $mobile_number }}</div>
                    </div>
                </div>
                <div class="grid grid-cols-1 px-12" style="padding-right: 20rem">
                    <div class="grid w-auto grid-cols-12">
                        <div class="col-span-3 text-[#003399]">Telephone Number :</div>
                        <div class="col-span-8 font-semibold text-md">{{ $telephone_number }}</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid grid-cols-1">
            <div class="bg-[#003399] text-white px-12 py-2 w-60 whitespace-nowrap"
                style="border-radius: 3px 60px 3px 3px;">Shipment
                Details</div>
            <div class="border-b-2 border-[#003399]"></div>

            <div class="p-6 my-4 mt-2 space-y-6">
                <div class="grid grid-cols-1 px-12 space-y-6" style="padding-right: 20rem">
                    <div class="grid grid-cols-1 gap-24 px-6">
                        <div>
                            <x-label for="transport_mode" value="Transport Mode" :required="true" />
                            <select class="w-full rounded-md cursor-pointer h-11" name="transport_mode"
                                wire:model='transport_mode'>
                                <option value="">Select</option>
                                @foreach ($transport_mode_references as $transport_mode_ref)
                                    <option value="{{ $transport_mode_ref->id }}">
                                        {{ $transport_mode_ref->name }}
                                    </option>
                                @endforeach
                            </select>
                            <x-input-error for="transport_mode" />
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-24 px-6">
                        <div>
                            <x-label for="origin" value="Origin" :required="true" />
                            <select class="w-full rounded-md cursor-pointer h-11" name="origin" wire:model='origin'
                                @if ($transport_mode == null) disabled @endif>
                                <option value="">Select</option>
                                @foreach ($origin_references as $origin_ref)
                                    <option value="{{ $origin_ref->id }}">
                                        {{ $origin_ref->display }}
                                    </option>
                                @endforeach
                            </select>
                            <x-input-error for="origin" />
                        </div>
                        <div>
                            <x-label for="destination" value="Destination" :required="true" />
                            <select class="w-full rounded-md cursor-pointer h-11" name="destination"
                                wire:model='destination' @if ($transport_mode == null) disabled @endif>
                                <option value="">Select</option>
                                @foreach ($destination_references as $destination_ref)
                                    <option value="{{ $destination_ref->id }}">
                                        {{ $destination_ref->display }}
                                    </option>
                                @endforeach
                            </select>
                            <x-input-error for="destination" />
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-24 px-6">
                        <div>
                            <x-label for="exact_pickup_location" value="Exact Pick Up Location" :required="true" />
                            <input type="text"
                                class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                name="exact_pickup_location" wire:model.defer='exact_pickup_location'
                                @if ($transport_mode == null) disabled @endif>
                            <x-input-error for="exact_pickup_location" />
                        </div>
                        <div>
                            <x-label for="exact_dropoff_location" value="Exact Drop Off Location" :required="true" />
                            <input type="text"
                                class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                name="exact_dropoff_location" wire:model.defer='exact_dropoff_location'
                                @if ($transport_mode == null) disabled @endif>
                            <x-input-error for="exact_dropoff_location" />
                        </div>
                    </div>
                </div>
                <div class="grid grid-cols-1 gap-24 px-6" style="padding-right: 18rem">
                    <div class="px-6 mt-4 border-b border-gray-400"></div>
                </div>
                <div class="grid grid-cols-1 px-12 space-y-6" style="padding-right: 20rem">
                    <div class="grid grid-cols-2 gap-24 px-6">
                        <div>
                            <x-label for="declared_value" value="Declared Value" />
                            <input type="text"
                                class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                name="declared_value" wire:model.defer='declared_value'
                                @if ($transport_mode == null) disabled @endif>
                            <x-input-error for="declared_value" />
                        </div>
                        <div>
                            <x-label for="service_mode" value="Service Mode" :required="true" />
                            <select class="w-full rounded-md cursor-pointer h-11" name="service_mode"
                                wire:model='service_mode' @if ($transport_mode == null) disabled @endif>
                                <option value="">Select</option>
                                @foreach ($service_mode_references as $service_mode_ref)
                                    <option value="{{ $service_mode_ref->id }}">
                                        {{ $service_mode_ref->name }}
                                    </option>
                                @endforeach
                            </select>
                            <x-input-error for="service_mode" />
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-24 px-6">
                        <div>
                            <x-label for="commodity_type" value="Commodity Type" />
                            <select class="w-full rounded-md cursor-pointer h-11" name="commodity_type"
                                wire:model='commodity_type' @if ($transport_mode == null) disabled @endif>
                                <option value="">Select</option>
                                @foreach ($commodity_type_references as $commodity_type_ref)
                                    <option value="{{ $commodity_type_ref->id }}">
                                        {{ $commodity_type_ref->name }}
                                    </option>
                                @endforeach
                            </select>
                            <x-input-error for="commodity_type" />
                        </div>
                        <div>
                            <x-label for="commodity_applicable_rate" value="Commodity Applicable Rate" />
                            <select class="w-full rounded-md cursor-pointer h-11" name="commodity_applicable_rate"
                                wire:model='commodity_applicable_rate'
                                @if ($transport_mode == null) disabled @endif>
                                <option value="">Select</option>
                                @foreach ($commodity_applicable_rate_references as $commodity_applicable_rate_ref)
                                    <option value="{{ $commodity_applicable_rate_ref->id }}">
                                        {{ $commodity_applicable_rate_ref->name }}
                                    </option>
                                @endforeach
                            </select>
                            <x-input-error for="commodity_applicable_rate" />
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-24 px-6">
                        <div>
                            <x-label for="description" value="Description of Goods" />
                            <input type="text"
                                class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                name="description" wire:model.defer='description'
                                @if ($transport_mode == null) disabled @endif>
                            <x-input-error for="description" />
                        </div>
                        <div>
                            <x-label for="paymode" value="Paymode" :required="true" />
                            <select class="w-full rounded-md cursor-pointer h-11" name="paymode" wire:model='paymode'
                                @if ($transport_mode == null) disabled @endif>
                                <option value="">Select</option>
                                @foreach ($paymode_references as $paymode_ref)
                                    <option value="{{ $paymode_ref->id }}">
                                        {{ $paymode_ref->name }}
                                    </option>
                                @endforeach
                            </select>
                            <x-input-error for="paymode" />
                        </div>
                    </div>
                    <div class="px-6">
                        {{-- for Transport Mode AIR --}}
                        @if ($transport_mode == 1)
                            <div class="my-6">
                                <div>
                                    <div class="flex gap-2 mt-4 font-medium text-gray-500">
                                        <div class="mt-[-8px]">
                                            <input type="checkbox"
                                                class="w-6 h-6 rounded-md border-[#003399] border cursor-pointer"
                                                name="air_cargo" wire:model='air_cargo'
                                                @if ($air_cargo) checked @endif>
                                        </div>
                                        <div class="-mt-2 text-lg">Air Cargo</div>
                                    </div>
                                    @if ($air_cargo)
                                        <div class="flex px-2.5 mt-2">
                                            <div class="overflow-auto border-l-4 border-[#003399]">
                                                <table class="text-md text-[#003399] ml-4">
                                                    <thead>
                                                        <tr class="text-left">
                                                            <th class="w-12 px-2">
                                                                <x-label value="Qty" />
                                                            </th>
                                                            <th class="w-12 px-2">
                                                                <x-label value="Wt" />
                                                            </th>
                                                            <th class="w-32 px-1 whitespace-nowrap">
                                                                <x-label value="Dimension (LxWxH)" />
                                                            </th>
                                                            <th class="w-32 px-2">
                                                                <x-label value="Unit of Measurement" />
                                                            </th>
                                                            <th class="w-32 px-2">
                                                                <x-label value="Measurement Type" />
                                                            </th>
                                                            <th class="w-32 px-2">
                                                                <x-label value="Type of Packaging" />
                                                            </th>
                                                            <th class="w-12 px-2">
                                                                <x-label value="For Crating" />
                                                            </th>
                                                            <th class="w-12 px-2">
                                                                <x-label value="Crating Type" />
                                                            </th>
                                                            <th class="w-12 px-2">
                                                                <x-label value="CWT" />
                                                            </th>
                                                            <th class="px-2"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($air_cargos as $i => $ac)
                                                            @if (!$ac['is_deleted'])
                                                                <tr class="">
                                                                    <td class="px-1">
                                                                        <div class="-mt-2">
                                                                            <input type="text"
                                                                                class="w-12 h-8 p-2 border rounded-md cursor-pointer"
                                                                                name="air_cargos.{{ $i }}.qty"
                                                                                wire:model.defer='air_cargos.{{ $i }}.qty'>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="-mt-2">
                                                                            <input type="text"
                                                                                class="w-12 h-8 p-2 border rounded-md cursor-pointer"
                                                                                name="air_cargos.{{ $i }}.wt"
                                                                                wire:model.defer='air_cargos.{{ $i }}.wt'
                                                                                wire:change="forCWT_CBM({{ $transport_mode }})">
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="-mt-2">
                                                                            <div class="flex gap-2">
                                                                                <input type="text"
                                                                                    class="w-12 h-8 p-2 border rounded-md cursor-pointer"
                                                                                    name="air_cargos.{{ $i }}.dimension_l"
                                                                                    wire:model.defer='air_cargos.{{ $i }}.dimension_l'
                                                                                    wire:change="forCWT_CBM({{ $transport_mode }})">
                                                                                <input type="text"
                                                                                    class="w-12 h-8 p-2 border rounded-md cursor-pointer"
                                                                                    name="air_cargos.{{ $i }}.dimension_w"
                                                                                    wire:model.defer='air_cargos.{{ $i }}.dimension_w'
                                                                                    wire:change="forCWT_CBM({{ $transport_mode }})">
                                                                                <input type="text"
                                                                                    class="w-12 h-8 p-2 border rounded-md cursor-pointer"
                                                                                    name="air_cargos.{{ $i }}.dimension_h"
                                                                                    wire:model.defer='air_cargos.{{ $i }}.dimension_h'
                                                                                    wire:change="forCWT_CBM({{ $transport_mode }})">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="w-32 -mt-2 whitespace-normal">
                                                                            <select
                                                                                class="w-32 h-8 p-0 px-2 text-sm border rounded-md cursor-pointer"
                                                                                name="air_cargos.{{ $i }}.unit_of_measurement"
                                                                                wire:model='air_cargos.{{ $i }}.unit_of_measurement'>
                                                                                <option value="">Select
                                                                                </option>
                                                                                <option value="1">mm</option>
                                                                                <option value="2">cm</option>
                                                                                <option value="3">inches
                                                                                </option>
                                                                                <option value="4">m</option>
                                                                                <option value="5">ft</option>
                                                                                <option value="6">yard
                                                                                </option>
                                                                            </select>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="w-32 -mt-2 whitespace-normal">
                                                                            <select
                                                                                class="w-32 h-8 p-0 px-2 text-sm border rounded-md cursor-pointer"
                                                                                name="air_cargos.{{ $i }}.measurement_type"
                                                                                wire:model='air_cargos.{{ $i }}.measurement_type'>
                                                                                <option value="">Select
                                                                                </option>
                                                                                <option value="1">Lot</option>
                                                                                <option value="2">Per piece
                                                                                </option>
                                                                            </select>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="w-32 -mt-2 whitespace-normal">
                                                                            <select
                                                                                class="w-32 h-8 p-0 px-2 text-sm border rounded-md cursor-pointer"
                                                                                name="air_cargos.{{ $i }}.type_of_packaging"
                                                                                wire:model='air_cargos.{{ $i }}.type_of_packaging'>
                                                                                <option value="">Select
                                                                                </option>
                                                                                <option value="1">Box</option>
                                                                                <option value="2">Plastic
                                                                                </option>
                                                                                <option value="3">Crate
                                                                                </option>
                                                                            </select>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="-mt-2 whitespace-normal">
                                                                            <div class="-mt-2">
                                                                                @if ($air_cargos[$i]['for_crating'] == true)
                                                                                    <svg wire:click="isForCreating({{ $i }}, 1, false)"
                                                                                        class="py-1 rounded-full text-green"
                                                                                        aria-hidden="true"
                                                                                        focusable="false"
                                                                                        data-prefix="fas"
                                                                                        data-icon="user-slash"
                                                                                        role="img"
                                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                                        viewBox="0 0 720 510">
                                                                                        <path fill="currentColor"
                                                                                            d="M192 64C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192s-86-192-192-192H192zM384 352c-53 0-96-43-96-96s43-96 96-96s96 43 96 96s-43 96-96 96z">
                                                                                        </path>
                                                                                    </svg>
                                                                                @else
                                                                                    <svg wire:click="isForCreating({{ $i }}, 1, true)"
                                                                                        class="py-1 text-gray-400 rounded-full"
                                                                                        aria-hidden="true"
                                                                                        focusable="false"
                                                                                        data-prefix="fas"
                                                                                        data-icon="user-slash"
                                                                                        role="img"
                                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                                        viewBox="0 0 720 510">
                                                                                        <path fill="currentColor"
                                                                                            d="M384 128c70.7 0 128 57.3 128 128s-57.3 128-128 128H192c-70.7 0-128-57.3-128-128s57.3-128 128-128H384zM576 256c0-106-86-192-192-192H192C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192zM192 352c53 0 96-43 96-96s-43-96-96-96s-96 43-96 96s43 96 96 96z">
                                                                                        </path>
                                                                                    </svg>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="-mt-2">
                                                                            <select
                                                                                class="h-8 p-0 px-2 text-sm border rounded-md cursor-pointer w-28"
                                                                                name="air_cargos.{{ $i }}.crating_type"
                                                                                wire:model='air_cargos.{{ $i }}.crating_type'
                                                                                @if ($air_cargos[$i]['for_crating'] == false) disabled @endif>
                                                                                <option value="">Select
                                                                                </option>
                                                                                @foreach ($crating_type_references as $crating_type_ref)
                                                                                    <option
                                                                                        value="{{ $crating_type_ref->id }}">
                                                                                        {{ $crating_type_ref->name }}
                                                                                    </option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="-mt-2">
                                                                            <input type="text"
                                                                                class="w-20 h-8 p-2 border rounded-md cursor-pointer"
                                                                                name="air_cargos.{{ $i }}.cwt"
                                                                                wire:model.defer='air_cargos.{{ $i }}.cwt'
                                                                                disabled>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-2">
                                                                        <div class="flex gap-2 -mt-2">
                                                                            @if (count($air_cargos) > 1)
                                                                                <button type="button" title="Remove"
                                                                                    wire:click="removeAirCargos({{ $i }})"
                                                                                    class="w-6 h-6 px-1 pr-0.5 pt-0.5 text-md flex-none bg-red-600 text-white rounded-full">
                                                                                    -</button>
                                                                            @endif
                                                                            @if (count($air_cargos) == ($i += 1))
                                                                                <button type="button" title=""
                                                                                    wire:click="addAirCargos"
                                                                                    class="w-6 h-6 px-1 pr-0.5 text-md flex-none bg-[#003399] text-white rounded-full">
                                                                                    +
                                                                                </button>
                                                                            @endif
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div>
                                    <div class="flex gap-2 mt-4 font-medium text-gray-500">
                                        <div class="mt-[-8px]">
                                            <input type="checkbox"
                                                class="w-6 h-6 rounded-md border-[#003399] border cursor-pointer"
                                                name="air_pouch" wire:model='air_pouch'>
                                        </div>
                                        <div class="-mt-2 text-lg">Air Pouch</div>
                                    </div>
                                    @if ($air_pouch)
                                        <div class="flex px-2.5 mt-2">
                                            <div class="overflow-auto border-l-4 border-[#003399]">
                                                <table class="text-md text-[#003399] ml-4">
                                                    <thead>
                                                        <tr class="text-left">
                                                            <th class="w-12 px-3">
                                                                <x-label value="Qty" />
                                                            </th>
                                                            <th class="w-32 px-3">
                                                                <x-label value="Pouch Size" />
                                                            </th>
                                                            <th class="w-32 px-3">
                                                                <x-label value="Amount" />
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($air_pouches as $i => $ap)
                                                            <tr class="mt-4">
                                                                <td class="px-2">
                                                                    <div class="mb-2">
                                                                        <input type="text"
                                                                            class="w-10 h-8 p-2 border rounded-md cursor-pointer"
                                                                            name="air_pouches.{{ $i }}.qty"
                                                                            wire:model.defer='air_pouches.{{ $i }}.qty'>
                                                                    </div>
                                                                </td>
                                                                <td class="px-2">
                                                                    <div class="w-32 mb-2 whitespace-normal">
                                                                        <select
                                                                            class="w-32 h-8 p-0 px-2 text-sm border rounded-md cursor-pointer"
                                                                            name="air_pouches.{{ $i }}.pouch_size"
                                                                            wire:model='air_pouches.{{ $i }}.pouch_size'
                                                                            wire:change="forAirPouchSize">
                                                                            <option value="">Select
                                                                            </option>
                                                                            <option value="1">Small
                                                                            </option>
                                                                            <option value="2">Medium
                                                                            </option>
                                                                            <option value="3">Large
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                </td>
                                                                <td class="px-2">
                                                                    <div class="w-32 mb-2 whitespace-normal">
                                                                        <input type="text"
                                                                            class="w-32 h-8 p-0 px-2 text-sm border rounded-md cursor-pointer"
                                                                            name="air_pouches.{{ $i }}.amount"
                                                                            wire:model.defer='air_pouches.{{ $i }}.amount'
                                                                            disabled>
                                                                    </div>
                                                                </td>
                                                                <td class="px-2">
                                                                    <div class="flex gap-2 -mt-2">
                                                                        @if (count($air_pouches) > 1)
                                                                            <button type="button" title="Remove"
                                                                                wire:click="removeAirPouches({{ $i }})"
                                                                                class="w-6 h-6 px-1 pr-0.5 pt-0.5 text-md flex-none bg-red-600 text-white rounded-full">
                                                                                -</button>
                                                                        @endif
                                                                        @if (count($air_pouches) == ($i += 1))
                                                                            <button type="button" title=""
                                                                                wire:click="addAirPouches"
                                                                                class="w-6 h-6 px-1 pr-0.5 text-md flex-none bg-[#003399] text-white rounded-full">
                                                                                +
                                                                            </button>
                                                                        @endif
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div>
                                    <div class="flex gap-2 mt-4 font-medium text-gray-500">
                                        <div class="mt-[-8px]">
                                            <input type="checkbox"
                                                class="w-6 h-6 rounded-md border-[#003399] border cursor-pointer"
                                                name="air_box" wire:model='air_box'>
                                        </div>
                                        <div class="-mt-2 text-lg">Air Box</div>
                                    </div>
                                    @if ($air_box)
                                        <div class="flex px-2.5 mt-2">
                                            <div class="overflow-auto border-l-4 border-[#003399]">
                                                <table class="text-md text-[#003399] ml-4">
                                                    <thead>
                                                        <tr class="text-left">
                                                            <th class="w-12 px-3">
                                                                <x-label value="Qty" />
                                                            </th>
                                                            <th class="w-32 px-3">
                                                                <x-label value="Box Size" />
                                                            </th>
                                                            <th class="w-32 px-3">
                                                                <x-label value="Weight (KG)" />
                                                            </th>
                                                            <th class="w-32 px-3">
                                                                <x-label value="Amount" />
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($air_boxes as $i => $ab)
                                                            <tr class="mt-4">
                                                                <td class="px-2">
                                                                    <div class="mb-2">
                                                                        <input type="text"
                                                                            class="w-10 h-8 p-2 border rounded-md cursor-pointer"
                                                                            name="air_boxes.{{ $i }}.qty"
                                                                            wire:model.defer='air_boxes.{{ $i }}.qty'>
                                                                    </div>
                                                                </td>
                                                                <td class="px-2">
                                                                    <div class="w-32 mb-2 whitespace-normal">
                                                                        <select
                                                                            class="w-32 h-8 p-0 px-2 text-sm border rounded-md cursor-pointer"
                                                                            name="air_boxes.{{ $i }}.box_size"
                                                                            wire:model='air_boxes.{{ $i }}.box_size'
                                                                            wire:change="forBoxWeight({{ $transport_mode }})">
                                                                            <option value="">Select
                                                                            </option>
                                                                            <option value="1">Small
                                                                            </option>
                                                                            <option value="2">Medium
                                                                            </option>
                                                                            <option value="2">Large
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                </td>
                                                                <td class="px-2">
                                                                    <div class="w-32 mb-2 whitespace-normal">
                                                                        <input type="text"
                                                                            class="w-32 h-8 p-0 px-2 text-sm border rounded-md cursor-pointer"
                                                                            name="air_boxes.{{ $i }}.weight"
                                                                            wire:model.defer='air_boxes.{{ $i }}.weight'
                                                                            wire:change="forBoxWeight({{ $transport_mode }})">
                                                                    </div>
                                                                </td>
                                                                <td class="px-2">
                                                                    <div class="w-32 mb-2 whitespace-normal">
                                                                        <input type="text"
                                                                            class="w-32 h-8 p-0 px-2 text-sm border rounded-md cursor-pointer"
                                                                            name="air_boxes.{{ $i }}.amount"
                                                                            wire:model.defer='air_boxes.{{ $i }}.amount'
                                                                            disabled>
                                                                    </div>
                                                                </td>
                                                                <td class="px-2">
                                                                    <div class="flex gap-2 -mt-2">
                                                                        @if (count($air_boxes) > 1)
                                                                            <button type="button"
                                                                                title="Remove Air Box"
                                                                                wire:click="removeAirBoxes({{ $i }})"
                                                                                class="w-6 h-6 px-1 pr-0.5 pt-0.5 text-md flex-none bg-red-600 text-white rounded-full">
                                                                                -</button>
                                                                        @endif
                                                                        @if (count($air_boxes) == ($i += 1))
                                                                            <button type="button" title=""
                                                                                wire:click="addAirBoxes"
                                                                                class="w-6 h-6 px-1 pr-0.5 text-md flex-none bg-[#003399] text-white rounded-full">
                                                                                +
                                                                            </button>
                                                                        @endif
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        @endif
                        {{-- for Transport Mode SEA --}}
                        @if ($transport_mode == 2)
                            <div class="my-6">
                                <div>
                                    <div class="flex gap-2 mt-4 font-medium text-gray-500">
                                        <div class="mt-[-8px]">
                                            <input type="checkbox"
                                                class="w-6 h-6 rounded-md border-[#003399] border cursor-pointer"
                                                name="sea_cargo" wire:model='sea_cargo'
                                                @if ($sea_cargo) checked @endif>
                                        </div>
                                        <div class="-mt-2 text-lg">Sea Cargo</div>
                                    </div>
                                    @if ($sea_cargo)
                                        <div class="flex px-2.5 mt-2">
                                            <div class="overflow-auto border-l-4 border-[#003399]">
                                                <div
                                                    class="flex gap-6 mt-1 mb-4 ml-6 text-sm font-medium text-gray-500">
                                                    @foreach ($movement_sf_type_references as $i => $sf_type)
                                                        <div class="flex gap-1">
                                                            <input type="radio" class="border-[#003399] border"
                                                                name="movement_sf_type" wire:model='movement_sf_type'
                                                                value="{{ $movement_sf_type_references[$i]['id'] }}">
                                                            {{ $movement_sf_type_references[$i]['name'] }}
                                                        </div>
                                                    @endforeach
                                                </div>
                                                @if ($movement_sf_type == 1)
                                                    <table class="text-md text-[#003399] ml-6">
                                                        <thead>
                                                            <tr class="text-left">
                                                                <th class="w-12 px-2">
                                                                    <x-label value="Qty" />
                                                                </th>
                                                                <th class="w-12 px-2">
                                                                    <x-label value="Wt" />
                                                                </th>
                                                                <th class="w-32 px-1 whitespace-nowrap">
                                                                    <x-label value="Dimension (LxWxH)" />
                                                                </th>
                                                                <th class="w-32 px-2">
                                                                    <x-label value="Unit of Measurement" />
                                                                </th>
                                                                <th class="w-32 px-2">
                                                                    <x-label value="Measurement Type" />
                                                                </th>
                                                                <th class="w-32 px-2">
                                                                    <x-label value="Type of Packaging" />
                                                                </th>
                                                                <th class="w-12 px-2">
                                                                    <x-label value="For Crating" />
                                                                </th>
                                                                <th class="w-12 px-2">
                                                                    <x-label value="Crating Type" />
                                                                </th>
                                                                <th class="w-12 px-2">
                                                                    <x-label value="CBM" />
                                                                </th>
                                                                <th class="px-2"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($sea_cargos_is_lcl as $i => $sc)
                                                                <tr class="">
                                                                    <td class="px-1">
                                                                        <div class="-mt-2">
                                                                            <input type="text"
                                                                                class="w-16 h-8 p-2 border rounded-md cursor-pointer"
                                                                                name="sea_cargos_is_lcl.{{ $i }}.qty"
                                                                                wire:model.defer='sea_cargos_is_lcl.{{ $i }}.qty'
                                                                                wire:change="forCWT_CBM({{ $transport_mode }})">
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="-mt-2">
                                                                            <input type="text"
                                                                                class="w-16 h-8 p-2 border rounded-md cursor-pointer"
                                                                                name="sea_cargos_is_lcl.{{ $i }}.wt"
                                                                                wire:model.defer='sea_cargos_is_lcl.{{ $i }}.wt'
                                                                                wire:change="forCWT_CBM({{ $transport_mode }})">
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="-mt-2">
                                                                            <div class="flex gap-2">
                                                                                <input type="text"
                                                                                    class="w-16 h-8 p-2 border rounded-md cursor-pointer"
                                                                                    name="sea_cargos_is_lcl.{{ $i }}.dimension_l"
                                                                                    wire:model.defer='sea_cargos_is_lcl.{{ $i }}.dimension_l'
                                                                                    wire:change="forCWT_CBM({{ $transport_mode }})">
                                                                                <input type="text"
                                                                                    class="w-16 h-8 p-2 border rounded-md cursor-pointer"
                                                                                    name="sea_cargos_is_lcl.{{ $i }}.dimension_w"
                                                                                    wire:model.defer='sea_cargos_is_lcl.{{ $i }}.dimension_w'
                                                                                    wire:change="forCWT_CBM({{ $transport_mode }})">
                                                                                <input type="text"
                                                                                    class="w-16 h-8 p-2 border rounded-md cursor-pointer"
                                                                                    name="sea_cargos_is_lcl.{{ $i }}.dimension_h"
                                                                                    wire:model.defer='sea_cargos_is_lcl.{{ $i }}.dimension_h'
                                                                                    wire:change="forCWT_CBM({{ $transport_mode }})">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="w-32 -mt-2 whitespace-normal">
                                                                            <select
                                                                                class="w-32 h-8 p-0 px-2 text-sm border rounded-md cursor-pointer"
                                                                                name="sea_cargos_is_lcl.{{ $i }}.unit_of_measurement"
                                                                                wire:model='sea_cargos_is_lcl.{{ $i }}.unit_of_measurement'>
                                                                                <option value="">Select
                                                                                </option>
                                                                                <option value="1">mm
                                                                                </option>
                                                                                <option value="2">cm
                                                                                </option>
                                                                                <option value="3">inches
                                                                                </option>
                                                                                <option value="4">m
                                                                                </option>
                                                                                <option value="5">ft
                                                                                </option>
                                                                                <option value="6">yard
                                                                                </option>
                                                                            </select>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="w-32 -mt-2 whitespace-normal">
                                                                            <select
                                                                                class="w-32 h-8 p-0 px-2 text-sm border rounded-md cursor-pointer"
                                                                                name="sea_cargos_is_lcl.{{ $i }}.measurement_type"
                                                                                wire:model='sea_cargos_is_lcl.{{ $i }}.measurement_type'>
                                                                                <option value="">Select
                                                                                </option>
                                                                                <option value="1">Lot
                                                                                </option>
                                                                                <option value="2">Per
                                                                                    piece
                                                                                </option>
                                                                            </select>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="w-32 -mt-2 whitespace-normal">
                                                                            <select
                                                                                class="w-32 h-8 p-0 px-2 text-sm border rounded-md cursor-pointer"
                                                                                name="sea_cargos_is_lcl.{{ $i }}.type_of_packaging"
                                                                                wire:model='sea_cargos_is_lcl.{{ $i }}.type_of_packaging'>
                                                                                <option value="">Select
                                                                                </option>
                                                                                <option value="1">Box
                                                                                </option>
                                                                                <option value="2">Plastic
                                                                                </option>
                                                                                <option value="3">Crate
                                                                                </option>
                                                                            </select>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="-mt-2 whitespace-normal">
                                                                            <div class="-mt-2">
                                                                                @if ($sea_cargos_is_lcl[$i]['for_crating'] == true)
                                                                                    <svg wire:click="isForCreating({{ $i }}, 2, false)"
                                                                                        class="py-1 rounded-full text-green"
                                                                                        aria-hidden="true"
                                                                                        focusable="false"
                                                                                        data-prefix="fas"
                                                                                        data-icon="user-slash"
                                                                                        role="img"
                                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                                        viewBox="0 0 720 510">
                                                                                        <path fill="currentColor"
                                                                                            d="M192 64C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192s-86-192-192-192H192zM384 352c-53 0-96-43-96-96s43-96 96-96s96 43 96 96s-43 96-96 96z">
                                                                                        </path>
                                                                                    </svg>
                                                                                @else
                                                                                    <svg wire:click="isForCreating({{ $i }}, 2, true)"
                                                                                        class="py-1 text-gray-400 rounded-full"
                                                                                        aria-hidden="true"
                                                                                        focusable="false"
                                                                                        data-prefix="fas"
                                                                                        data-icon="user-slash"
                                                                                        role="img"
                                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                                        viewBox="0 0 720 510">
                                                                                        <path fill="currentColor"
                                                                                            d="M384 128c70.7 0 128 57.3 128 128s-57.3 128-128 128H192c-70.7 0-128-57.3-128-128s57.3-128 128-128H384zM576 256c0-106-86-192-192-192H192C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192zM192 352c53 0 96-43 96-96s-43-96-96-96s-96 43-96 96s43 96 96 96z">
                                                                                        </path>
                                                                                    </svg>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="-mt-2">
                                                                            <select
                                                                                class="h-8 p-0 px-2 text-sm border rounded-md cursor-pointer w-28"
                                                                                name="sea_cargos_is_lcl.{{ $i }}.crating_type"
                                                                                wire:model='sea_cargos_is_lcl.{{ $i }}.crating_type'
                                                                                @if ($sea_cargos_is_lcl[$i]['for_crating'] == false) disabled @endif>
                                                                                <option value="">Select
                                                                                </option>
                                                                                @foreach ($crating_type_references as $crating_type_ref)
                                                                                    <option
                                                                                        value="{{ $crating_type_ref->id }}">
                                                                                        {{ $crating_type_ref->name }}
                                                                                    </option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="-mt-2">
                                                                            <input type="text"
                                                                                class="w-16 h-8 p-2 border rounded-md cursor-pointer"
                                                                                name="sea_cargos_is_lcl.{{ $i }}.cbm"
                                                                                wire:model.defer='sea_cargos_is_lcl.{{ $i }}.cbm'
                                                                                disabled>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-2">
                                                                        <div class="flex gap-2 -mt-2">
                                                                            @if (count($sea_cargos_is_lcl) > 1)
                                                                                <button type="button" title="Remove"
                                                                                    wire:click="removeSeaCargosLcl({{ $i }})"
                                                                                    class="w-6 h-6 px-1 pr-0.5 pt-0.5 text-md flex-none bg-red-600 text-white rounded-full">
                                                                                    -</button>
                                                                            @endif
                                                                            @if (count($sea_cargos_is_lcl) == ($i += 1))
                                                                                <button type="button" title=""
                                                                                    wire:click="addSeaCargosLcl"
                                                                                    class="w-6 h-6 px-1 pr-0.5 text-md flex-none bg-[#003399] text-white rounded-full">
                                                                                    +
                                                                                </button>
                                                                            @endif
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                @elseif($movement_sf_type == 2)
                                                    <table class="text-md text-[#003399] ml-6 mb-4">
                                                        <thead>
                                                            <tr class="text-left">
                                                                <th class="w-12 px-2">
                                                                    <x-label value="Qty" />
                                                                </th>
                                                                <th class="w-12 px-2">
                                                                    <x-label value="Container" />
                                                                </th>
                                                                <th class="px-2"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($sea_cargos_is_fcl as $i => $sc_fcl)
                                                                <tr class="">
                                                                    <td class="px-1">
                                                                        <div class="mb-2">
                                                                            <input type="text"
                                                                                class="w-10 h-8 p-2 border rounded-md cursor-pointer"
                                                                                name="sea_cargos_is_fcl.{{ $i }}.qty"
                                                                                wire:model.defer='sea_cargos_is_fcl.{{ $i }}.qty'>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="w-32 mb-2 whitespace-normal">
                                                                            <select
                                                                                class="w-32 h-8 p-0 px-2 text-sm border rounded-md cursor-pointer"
                                                                                name="sea_cargos_is_fcl.{{ $i }}.container"
                                                                                wire:model='sea_cargos_is_fcl.{{ $i }}.container'>
                                                                                <option value="">Select
                                                                                </option>
                                                                                <option value="10">10
                                                                                </option>
                                                                                <option value="20">20
                                                                                </option>
                                                                                <option value="40">40
                                                                                </option>
                                                                            </select>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-2">
                                                                        <div class="flex gap-2 -mt-2">
                                                                            @if (count($sea_cargos_is_fcl) > 1)
                                                                                <button type="button" title="Remove"
                                                                                    wire:click="removeSeaCargosFcl({{ $i }})"
                                                                                    class="w-6 h-6 px-1 pr-0.5 pt-0.5 text-md flex-none bg-red-600 text-white rounded-full">
                                                                                    -</button>
                                                                            @endif
                                                                            @if (count($sea_cargos_is_fcl) == ($i += 1))
                                                                                <button type="button" title=""
                                                                                    wire:click="addSeaCargosFcl"
                                                                                    class="w-6 h-6 px-1 pr-0.5 text-md flex-none bg-[#003399] text-white rounded-full">
                                                                                    +
                                                                                </button>
                                                                            @endif
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                @elseif ($movement_sf_type == 3)
                                                    <table class="text-md text-[#003399] ml-6">
                                                        <thead>
                                                            <tr class="text-left">
                                                                <th class="w-12 px-2">
                                                                    <x-label value="Qty" />
                                                                </th>
                                                                <th class="w-12 px-2">
                                                                    <x-label value="Wt" />
                                                                </th>
                                                                <th class="w-32 px-1 whitespace-nowrap">
                                                                    <x-label value="Dimension (LxWxH)" />
                                                                </th>
                                                                <th class="w-32 px-2">
                                                                    <x-label value="Unit of Measurement" />
                                                                </th>
                                                                <th class="w-32 px-2">
                                                                    <x-label value="Measurement Type" />
                                                                </th>
                                                                <th class="w-32 px-2">
                                                                    <x-label value="Amount" />
                                                                </th>
                                                                <th class="w-12 px-2">
                                                                    <x-label value="For Crating" />
                                                                </th>
                                                                <th class="w-12 px-2">
                                                                    <x-label value="Crating Type" />
                                                                </th>
                                                                <th class="w-12 px-2">
                                                                    <x-label value="CBM" />
                                                                </th>
                                                                <th class="px-2"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($sea_cargos_is_rcl as $i => $sc)
                                                                <tr class="">
                                                                    <td class="px-1">
                                                                        <div class="-mt-2">
                                                                            <input type="text"
                                                                                class="w-16 h-8 p-2 border rounded-md cursor-pointer"
                                                                                name="sea_cargos_is_rcl.{{ $i }}.qty"
                                                                                wire:model.defer='sea_cargos_is_rcl.{{ $i }}.qty'
                                                                                wire:change="forCWT_CBM({{ $transport_mode }})">
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="-mt-2">
                                                                            <input type="text"
                                                                                class="w-16 h-8 p-2 border rounded-md cursor-pointer"
                                                                                name="sea_cargos_is_rcl.{{ $i }}.wt"
                                                                                wire:model.defer='sea_cargos_is_rcl.{{ $i }}.wt'
                                                                                wire:change="forCWT_CBM({{ $transport_mode }})">
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="-mt-2">
                                                                            <div class="flex gap-2">
                                                                                <input type="text"
                                                                                    class="w-16 h-8 p-2 border rounded-md cursor-pointer"
                                                                                    name="sea_cargos_is_rcl.{{ $i }}.dimension_l"
                                                                                    wire:model.defer='sea_cargos_is_rcl.{{ $i }}.dimension_l'
                                                                                    wire:change="forCWT_CBM({{ $transport_mode }})">
                                                                                <input type="text"
                                                                                    class="w-16 h-8 p-2 border rounded-md cursor-pointer"
                                                                                    name="sea_cargos_is_rcl.{{ $i }}.dimension_w"
                                                                                    wire:model.defer='sea_cargos_is_rcl.{{ $i }}.dimension_w'
                                                                                    wire:change="forCWT_CBM({{ $transport_mode }})">
                                                                                <input type="text"
                                                                                    class="w-16 h-8 p-2 border rounded-md cursor-pointer"
                                                                                    name="sea_cargos_is_rcl.{{ $i }}.dimension_h"
                                                                                    wire:model.defer='sea_cargos_is_rcl.{{ $i }}.dimension_h'
                                                                                    wire:change="forCWT_CBM({{ $transport_mode }})">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="w-32 -mt-2 whitespace-normal">
                                                                            <select
                                                                                class="w-32 h-8 p-0 px-2 text-sm border rounded-md cursor-pointer"
                                                                                name="sea_cargos_is_rcl.{{ $i }}.unit_of_measurement"
                                                                                wire:model='sea_cargos_is_rcl.{{ $i }}.unit_of_measurement'>
                                                                                <option value="">
                                                                                    Select
                                                                                </option>
                                                                                <option value="1">mm
                                                                                </option>
                                                                                <option value="2">cm
                                                                                </option>
                                                                                <option value="3">
                                                                                    inches
                                                                                </option>
                                                                                <option value="4">m
                                                                                </option>
                                                                                <option value="5">ft
                                                                                </option>
                                                                                <option value="6">yard
                                                                                </option>
                                                                            </select>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="w-32 -mt-2 whitespace-normal">
                                                                            <select
                                                                                class="w-32 h-8 p-0 px-2 text-sm border rounded-md cursor-pointer"
                                                                                name="sea_cargos_is_rcl.{{ $i }}.measurement_type"
                                                                                wire:model='sea_cargos_is_rcl.{{ $i }}.measurement_type'>
                                                                                <option value="">
                                                                                    Select
                                                                                </option>
                                                                                <option value="1">Lot
                                                                                </option>
                                                                                <option value="2">Per
                                                                                    piece
                                                                                </option>
                                                                            </select>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="w-32 -mt-2 whitespace-normal">
                                                                            <input type="text"
                                                                                class="w-32 h-8 p-2 border rounded-md cursor-pointer"
                                                                                name="sea_cargos_is_rcl.{{ $i }}.amount"
                                                                                wire:model.defer='sea_cargos_is_rcl.{{ $i }}.amount'
                                                                                wire:change="forCWT_CBM({{ $transport_mode }})">
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="-mt-2 whitespace-normal">
                                                                            <div class="-mt-2">
                                                                                @if ($sea_cargos_is_rcl[$i]['for_crating'] == true)
                                                                                    <svg wire:click="isForCreating({{ $i }}, 3, false)"
                                                                                        class="py-1 rounded-full text-green"
                                                                                        aria-hidden="true"
                                                                                        focusable="false"
                                                                                        data-prefix="fas"
                                                                                        data-icon="user-slash"
                                                                                        role="img"
                                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                                        viewBox="0 0 720 510">
                                                                                        <path fill="currentColor"
                                                                                            d="M192 64C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192s-86-192-192-192H192zM384 352c-53 0-96-43-96-96s43-96 96-96s96 43 96 96s-43 96-96 96z">
                                                                                        </path>
                                                                                    </svg>
                                                                                @else
                                                                                    <svg wire:click="isForCreating({{ $i }}, 3, true)"
                                                                                        class="py-1 text-gray-400 rounded-full"
                                                                                        aria-hidden="true"
                                                                                        focusable="false"
                                                                                        data-prefix="fas"
                                                                                        data-icon="user-slash"
                                                                                        role="img"
                                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                                        viewBox="0 0 720 510">
                                                                                        <path fill="currentColor"
                                                                                            d="M384 128c70.7 0 128 57.3 128 128s-57.3 128-128 128H192c-70.7 0-128-57.3-128-128s57.3-128 128-128H384zM576 256c0-106-86-192-192-192H192C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192zM192 352c53 0 96-43 96-96s-43-96-96-96s-96 43-96 96s43 96 96 96z">
                                                                                        </path>
                                                                                    </svg>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="-mt-2">
                                                                            <select
                                                                                class="h-8 p-0 px-2 text-sm border rounded-md cursor-pointer w-28"
                                                                                name="sea_cargos_is_rcl.{{ $i }}.crating_type"
                                                                                wire:model='sea_cargos_is_rcl.{{ $i }}.crating_type'
                                                                                @if ($sea_cargos_is_rcl[$i]['for_crating'] == false) disabled @endif>
                                                                                <option value="">
                                                                                    Select
                                                                                </option>
                                                                                @foreach ($crating_type_references as $crating_type_ref)
                                                                                    <option
                                                                                        value="{{ $crating_type_ref->id }}">
                                                                                        {{ $crating_type_ref->name }}
                                                                                    </option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="-mt-2">
                                                                            <input type="text"
                                                                                class="w-16 h-8 p-2 border rounded-md cursor-pointer"
                                                                                name="sea_cargos_is_rcl.{{ $i }}.cbm"
                                                                                wire:model.defer='sea_cargos_is_rcl.{{ $i }}.cbm'
                                                                                disabled>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-2">
                                                                        <div class="flex gap-2 -mt-2">
                                                                            @if (count($sea_cargos_is_rcl) > 1)
                                                                                <button type="button" title="Remove"
                                                                                    wire:click="removeSeaCargosRcl({{ $i }})"
                                                                                    class="w-6 h-6 px-1 pr-0.5 pt-0.5 text-md flex-none bg-red-600 text-white rounded-full">
                                                                                    -</button>
                                                                            @endif
                                                                            @if (count($sea_cargos_is_rcl) == ($i += 1))
                                                                                <button type="button" title=""
                                                                                    wire:click="addSeaCargosRcl"
                                                                                    class="w-6 h-6 px-1 pr-0.5 text-md flex-none bg-[#003399] text-white rounded-full">
                                                                                    +
                                                                                </button>
                                                                            @endif
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                @endif
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div>
                                    <div class="flex gap-2 mt-4 font-medium text-gray-500">
                                        <div class="mt-[-8px]">
                                            <input type="checkbox"
                                                class="w-6 h-6 rounded-md border-[#003399] border cursor-pointer"
                                                name="sea_box" wire:model='sea_box'
                                                @if ($sea_box) checked @endif>
                                        </div>
                                        <div class="-mt-2 text-lg">Sea Box</div>
                                    </div>
                                    @if ($sea_box)
                                        <div class="flex px-2.5 mt-2">
                                            <div class="overflow-auto border-l-4 border-[#003399]">
                                                <table class="text-md text-[#003399] ml-4">
                                                    <thead>
                                                        <tr class="text-left">
                                                            <th class="w-12 px-3">
                                                                <x-label value="Qty" />
                                                            </th>
                                                            <th class="w-32 px-3">
                                                                <x-label value="Box Size" />
                                                            </th>
                                                            <th class="w-32 px-3">
                                                                <x-label value="Weight (KG)" />
                                                            </th>
                                                            <th class="w-32 px-3">
                                                                <x-label value="Amount" />
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($sea_boxes as $i => $ab)
                                                            <tr class="mt-4">
                                                                <td class="px-2">
                                                                    <div class="mb-2">
                                                                        <input type="text"
                                                                            class="w-10 h-8 p-2 border rounded-md cursor-pointer"
                                                                            name="sea_boxes.{{ $i }}.qty"
                                                                            wire:model.defer='sea_boxes.{{ $i }}.qty'>
                                                                    </div>
                                                                </td>
                                                                <td class="px-2">
                                                                    <div class="w-32 mb-2 whitespace-normal">
                                                                        <select
                                                                            class="w-32 h-8 p-0 px-2 text-sm border rounded-md cursor-pointer"
                                                                            name="sea_boxes.{{ $i }}.box_size"
                                                                            wire:model='sea_boxes.{{ $i }}.box_size'>
                                                                            <option value="">Select
                                                                            </option>
                                                                            <option value="1">Small
                                                                            </option>
                                                                            <option value="2">Medium
                                                                            </option>
                                                                            <option value="2">Large
                                                                            </option>
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                </td>
                                                                <td class="px-2">
                                                                    <div class="w-32 mb-2 whitespace-normal">
                                                                        <input type="text"
                                                                            class="w-32 h-8 p-0 px-2 text-sm border rounded-md cursor-pointer"
                                                                            name="sea_boxes.{{ $i }}.weight"
                                                                            wire:model.defer='sea_boxes.{{ $i }}.weight'
                                                                            wire:change="forBoxWeight({{ $transport_mode }})">
                                                                    </div>
                                                                </td>
                                                                <td class="px-2">
                                                                    <div class="w-32 mb-2 whitespace-normal">
                                                                        <input type="text"
                                                                            class="w-32 h-8 p-0 px-2 text-sm border rounded-md cursor-pointer"
                                                                            name="sea_boxes.{{ $i }}.amount"
                                                                            wire:model.defer='sea_boxes.{{ $i }}.amount'
                                                                            disabled>
                                                                    </div>
                                                                </td>
                                                                <td class="px-2">
                                                                    <div class="flex gap-2 -mt-2">
                                                                        @if (count($sea_boxes) > 1)
                                                                            <button type="button" title="Remove"
                                                                                wire:click="removeSeaBoxes({{ $i }})"
                                                                                class="w-6 h-6 px-1 pr-0.5 pt-0.5 text-md flex-none bg-red-600 text-white rounded-full">
                                                                                -</button>
                                                                        @endif
                                                                        @if (count($sea_boxes) == ($i += 1))
                                                                            <button type="button" title=""
                                                                                wire:click="addSeaBoxes"
                                                                                class="w-6 h-6 px-1 pr-0.5 text-md flex-none bg-[#003399] text-white rounded-full">
                                                                                +
                                                                            </button>
                                                                        @endif
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        @endif
                        {{-- for Transport Mode LAND --}}
                        @if ($transport_mode == 3)
                            <div class="my-6">
                                <div>
                                    <div class="flex gap-2 mt-4 font-medium text-gray-500">
                                        <div class="mt-[-8px]">
                                            <input type="checkbox"
                                                class="w-6 h-6 rounded-md border-[#003399] border cursor-pointer"
                                                name="land" wire:model='land'
                                                @if ($land) checked @endif>
                                        </div>
                                        <div class="-mt-2 text-lg">Land</div>
                                    </div>
                                    @if ($land)
                                        <div class="flex px-2.5 mt-2">
                                            <div class="overflow-auto border-l-4 border-[#003399]">
                                                <div>
                                                    <div
                                                        class="flex gap-6 mt-1 mb-4 ml-6 text-sm font-medium text-gray-500">
                                                        <div class="flex gap-1">
                                                            <input type="radio" class="border-[#003399] border"
                                                                name="land_packaging_type"
                                                                wire:model='land_packaging_type' value="1">
                                                            <span class="">Shipper's Box</span>
                                                        </div>
                                                        <div class="flex gap-1">
                                                            <input type="radio" class="border-[#003399] border"
                                                                name="land_packaging_type"
                                                                wire:model='land_packaging_type' value="2">
                                                            <span class="">Land Pouch</span>
                                                        </div>
                                                        <div class="flex gap-1">
                                                            <input type="radio" class="border-[#003399] border"
                                                                name="land_packaging_type"
                                                                wire:model='land_packaging_type' value="3">
                                                            <span class="">Land Box</span>
                                                        </div>
                                                        <div class="flex gap-1">
                                                            <input type="radio" class="border-[#003399] border"
                                                                name="land_packaging_type"
                                                                wire:model='land_packaging_type' value="4">
                                                            <span class="">FTL</span>
                                                        </div>
                                                    </div>
                                                    @if ($land_packaging_type == 2 || $land_packaging_type == 3)
                                                        <div
                                                            class="flex gap-6 mt-1 mb-4 ml-6 text-sm font-medium text-gray-500">
                                                            @foreach ($booking_type_references as $i => $sf_type)
                                                                <div class="flex gap-1">
                                                                    <input type="radio"
                                                                        class="border-[#003399] border"
                                                                        name="booking_type" wire:model='booking_type'
                                                                        value="{{ $booking_type_references[$i]['id'] }}">
                                                                    <span
                                                                        class="uppercase">{{ $booking_type_references[$i]['name'] }}</span>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    @endif
                                                </div>
                                                @if ($land_packaging_type == 1)
                                                    <table class="text-md text-[#003399] ml-4">
                                                        <thead>
                                                            <tr class="text-left">
                                                                <th class="w-24 px-2 whitespace-nowrap">
                                                                    <x-label value="Total Quantity" />
                                                                </th>
                                                                <th class="w-32 px-2 whitespace-nowrap">
                                                                    <x-label value="Total Weight (KG)" />
                                                                </th>
                                                                <th class="w-40 px-1 whitespace-nowrap">
                                                                    <x-label value="Dimension (LxWxH)" />
                                                                </th>
                                                                <th class="w-20 px-1">
                                                                    <x-label value="CBM" />
                                                                </th>
                                                                <th class="w-20 px-1">
                                                                    <x-label value="CWT" />
                                                                </th>
                                                                <th class="w-32 px-1">
                                                                    <x-label value="Land Rate" />
                                                                </th>
                                                                <th class="w-32 px-1">
                                                                    <x-label value="Rate Amount" />
                                                                </th>
                                                                <th class="px-1"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($lands_shippers_box as $i => $ac)
                                                                <tr class="">
                                                                    <td class="px-1">
                                                                        <div class="mb-2">
                                                                            <input type="text"
                                                                                class="w-20 h-8 p-2 border rounded-md cursor-pointer"
                                                                                name="lands_shippers_box.{{ $i }}.total_qty"
                                                                                wire:model.defer='lands_shippers_box.{{ $i }}.total_qty'>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="mb-2">
                                                                            <input type="text"
                                                                                class="w-24 h-8 p-2 border rounded-md cursor-pointer"
                                                                                name="lands_shippers_box.{{ $i }}.total_weight"
                                                                                wire:model.defer='lands_shippers_box.{{ $i }}.total_weight'>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="mb-2">
                                                                            <div class="flex gap-2">
                                                                                <input type="text"
                                                                                    class="w-12 h-8 p-2 border rounded-md cursor-pointer"
                                                                                    name="lands_shippers_box.{{ $i }}.dimension_l"
                                                                                    wire:model.defer='lands_shippers_box.{{ $i }}.dimension_l'>
                                                                                <input type="text"
                                                                                    class="w-12 h-8 p-2 border rounded-md cursor-pointer"
                                                                                    name="lands_shippers_box.{{ $i }}.dimension_w"
                                                                                    wire:model.defer='lands_shippers_box.{{ $i }}.dimension_w'>
                                                                                <input type="text"
                                                                                    class="w-12 h-8 p-2 border rounded-md cursor-pointer"
                                                                                    name="lands_shippers_box.{{ $i }}.dimension_h"
                                                                                    wire:model.defer='lands_shippers_box.{{ $i }}.dimension_h'>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="mb-2">
                                                                            <input type="text"
                                                                                class="w-20 h-8 p-2 border rounded-md cursor-pointer"
                                                                                name="lands_shippers_box.{{ $i }}.cbm"
                                                                                wire:model.defer='lands_shippers_box.{{ $i }}.cbm'
                                                                                disabled>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="mb-2">
                                                                            <input type="text"
                                                                                class="w-20 h-8 p-2 border rounded-md cursor-pointer"
                                                                                name="lands_shippers_box.{{ $i }}.cwt"
                                                                                wire:model.defer='lands_shippers_box.{{ $i }}.cwt'
                                                                                disabled>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="w-32 mb-2 whitespace-normal">
                                                                            <select
                                                                                class="w-32 h-8 p-0 px-2 text-sm border rounded-md cursor-pointer"
                                                                                name="lands_shippers_box.{{ $i }}.land_rate"
                                                                                wire:model='lands_shippers_box.{{ $i }}.land_rate'>
                                                                                <option value="">Select
                                                                                </option>
                                                                                <option value="1">Lot
                                                                                </option>
                                                                                <option value="2">Per
                                                                                    piece
                                                                                </option>
                                                                            </select>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="mb-2">
                                                                            <input type="text"
                                                                                class="w-32 h-8 p-2 border rounded-md cursor-pointer"
                                                                                name="lands_shippers_box.{{ $i }}.rate_amount"
                                                                                wire:model.defer='lands_shippers_box.{{ $i }}.rate_amount'
                                                                                disabled>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-2">
                                                                        <div class="flex gap-2 -mt-2">
                                                                            @if (count($lands_shippers_box) > 1)
                                                                                <button type="button" title="Remove"
                                                                                    wire:click="removeLandsShippersBox({{ $i }})"
                                                                                    class="w-6 h-6 px-1 pr-0.5 pt-0.5 text-md flex-none bg-red-600 text-white rounded-full">
                                                                                    -</button>
                                                                            @endif
                                                                            @if (count($lands_shippers_box) == ($i += 1))
                                                                                <button type="button" title=""
                                                                                    wire:click="addLandsShippersBox"
                                                                                    class="w-6 h-6 px-1 pr-0.5 text-md flex-none bg-[#003399] text-white rounded-full">
                                                                                    +
                                                                                </button>
                                                                            @endif
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                @elseif($land_packaging_type == 2)
                                                    <table class="text-md text-[#003399] ml-4">
                                                        <thead>
                                                            <tr class="text-left">
                                                                <th class="w-32 px-1">
                                                                    <x-label value="Size" />
                                                                </th>
                                                                <th class="w-32 px-2 whitespace-nowrap">
                                                                    <x-label value="Weight Per Pouch (KG)" />
                                                                </th>
                                                                <th class="w-20 px-1">
                                                                    <x-label value="CBM" />
                                                                </th>
                                                                <th class="w-32 px-1">
                                                                    <x-label value="Total Amount" />
                                                                </th>
                                                                <th class="px-1"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($lands_pouches as $i => $lc)
                                                                <tr class="">
                                                                    <td class="px-1">
                                                                        <div class="w-32 mb-2 whitespace-normal">
                                                                            <select
                                                                                class="w-32 h-8 p-0 px-2 text-sm border rounded-md cursor-pointer"
                                                                                name="lands_pouches.{{ $i }}.size"
                                                                                wire:model='lands_pouches.{{ $i }}.size'>
                                                                                <option value="">Select
                                                                                </option>
                                                                                <option value="1">Small
                                                                                </option>
                                                                                <option value="2">Medium
                                                                                </option>
                                                                                <option value="3">Large
                                                                                </option>
                                                                            </select>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="mb-2">
                                                                            <input type="text"
                                                                                class="w-20 h-8 p-2 border rounded-md cursor-pointer"
                                                                                name="lands_pouches.{{ $i }}.weight_per_pouch"
                                                                                wire:model.defer='lands_pouches.{{ $i }}.weight_per_pouch'>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="mb-2">
                                                                            <input type="text"
                                                                                class="w-20 h-8 p-2 border rounded-md cursor-pointer"
                                                                                name="lands_pouches.{{ $i }}.cbm"
                                                                                wire:model.defer='lands_pouches.{{ $i }}.cbm'
                                                                                disabled>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="mb-2">
                                                                            <input type="text"
                                                                                class="w-32 h-8 p-2 border rounded-md cursor-pointer"
                                                                                name="lands_pouches.{{ $i }}.total_amount"
                                                                                wire:model.defer='lands_pouches.{{ $i }}.total_amount'
                                                                                disabled>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-2">
                                                                        <div class="flex gap-2 -mt-2">
                                                                            @if (count($lands_pouches) > 1)
                                                                                <button type="button"
                                                                                    title="Remove"
                                                                                    wire:click="removeLandsPouches({{ $i }})"
                                                                                    class="w-6 h-6 px-1 pr-0.5 pt-0.5 text-md flex-none bg-red-600 text-white rounded-full">
                                                                                    -</button>
                                                                            @endif
                                                                            @if (count($lands_pouches) == ($i += 1))
                                                                                <button type="button"
                                                                                    title=""
                                                                                    wire:click="addLandsPouches"
                                                                                    class="w-6 h-6 px-1 pr-0.5 text-md flex-none bg-[#003399] text-white rounded-full">
                                                                                    +
                                                                                </button>
                                                                            @endif
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                @elseif($land_packaging_type == 3)
                                                    <table class="text-md text-[#003399] ml-4">
                                                        <thead>
                                                            <tr class="text-left">
                                                                <th class="w-32 px-1">
                                                                    <x-label value="Size" />
                                                                </th>
                                                                <th class="w-32 px-2 whitespace-nowrap">
                                                                    <x-label value="Weight Per Box (KG)" />
                                                                </th>
                                                                <th class="w-20 px-1">
                                                                    <x-label value="CBM" />
                                                                </th>
                                                                <th class="w-32 px-1">
                                                                    <x-label value="Total Amount" />
                                                                </th>
                                                                <th class="px-1"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($lands_boxes as $i => $lb)
                                                                <tr class="">
                                                                    <td class="px-1">
                                                                        <div class="w-32 mb-2 whitespace-normal">
                                                                            <select
                                                                                class="w-32 h-8 p-0 px-2 text-sm border rounded-md cursor-pointer"
                                                                                name="lands_boxes.{{ $i }}.size"
                                                                                wire:model='lands_boxes.{{ $i }}.size'>
                                                                                <option value="">Select
                                                                                </option>
                                                                                <option value="1">Small
                                                                                </option>
                                                                                <option value="2">Medium
                                                                                </option>
                                                                                <option value="3">Large
                                                                                </option>
                                                                            </select>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="mb-2">
                                                                            <input type="text"
                                                                                class="w-20 h-8 p-2 border rounded-md cursor-pointer"
                                                                                name="lands_boxes.{{ $i }}.weight_per_box"
                                                                                wire:model.defer='lands_boxes.{{ $i }}.weight_per_box'>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="mb-2">
                                                                            <input type="text"
                                                                                class="w-20 h-8 p-2 border rounded-md cursor-pointer"
                                                                                name="lands_boxes.{{ $i }}.cbm"
                                                                                wire:model.defer='lands_boxes.{{ $i }}.cbm'
                                                                                disabled>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-1">
                                                                        <div class="mb-2">
                                                                            <input type="text"
                                                                                class="w-32 h-8 p-2 border rounded-md cursor-pointer"
                                                                                name="lands_boxes.{{ $i }}.total_amount"
                                                                                wire:model.defer='lands_boxes.{{ $i }}.total_amount'
                                                                                disabled>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-2">
                                                                        <div class="flex gap-2 -mt-2">
                                                                            @if (count($lands_boxes) > 1)
                                                                                <button type="button"
                                                                                    title="Remove"
                                                                                    wire:click="removeLandsBoxes({{ $i }})"
                                                                                    class="w-6 h-6 px-1 pr-0.5 pt-0.5 text-md flex-none bg-red-600 text-white rounded-full">
                                                                                    -</button>
                                                                            @endif
                                                                            @if (count($lands_boxes) == ($i += 1))
                                                                                <button type="button"
                                                                                    title=""
                                                                                    wire:click="addLandsBoxes"
                                                                                    class="w-6 h-6 px-1 pr-0.5 text-md flex-none bg-[#003399] text-white rounded-full">
                                                                                    +
                                                                                </button>
                                                                            @endif
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                @elseif($land_packaging_type == 4)
                                                    <table class="text-md text-[#003399] ml-4">
                                                        <thead>
                                                            <tr class="text-left">
                                                                <th class="w-20 px-1">
                                                                    <x-label value="Quantity" />
                                                                </th>
                                                                <th class="w-48 px-2 whitespace-nowrap">
                                                                    <x-label value="Track Type" />
                                                                </th>
                                                                <th class="w-24 px-1">
                                                                    <x-label value="Unit Price" />
                                                                </th>
                                                                <th class="w-24 px-1">
                                                                    <x-label value="Amount" />
                                                                </th>
                                                                <th class="px-1"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($lands_ftl as $i => $ftl)
                                                                <tr class="">
                                                                    <td class="px-2">
                                                                        <div class="mb-2">
                                                                            <input type="text"
                                                                                class="w-20 h-8 p-2 border rounded-md cursor-pointer"
                                                                                name="lands_ftl.{{ $i }}.quantity"
                                                                                wire:model.defer='lands_ftl.{{ $i }}.quantity'>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-2">
                                                                        <div class="mb-2">
                                                                            <input type="text"
                                                                                class="w-48 h-8 p-2 border rounded-md cursor-pointer"
                                                                                name="lands_ftl.{{ $i }}.truck_type"
                                                                                wire:model.defer='lands_ftl.{{ $i }}.truck_type'>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-2">
                                                                        <div class="mb-2">
                                                                            <input type="text"
                                                                                class="w-24 h-8 p-2 border rounded-md cursor-pointer"
                                                                                name="lands_ftl.{{ $i }}.unit_price"
                                                                                wire:model.defer='lands_ftl.{{ $i }}.unit_price'>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-2">
                                                                        <div class="mb-2">
                                                                            <input type="text"
                                                                                class="w-24 h-8 p-2 border rounded-md cursor-pointer"
                                                                                name="lands_ftl.{{ $i }}.amount"
                                                                                wire:model.defer='lands_ftl.{{ $i }}.amount'
                                                                                disabled>
                                                                        </div>
                                                                    </td>
                                                                    <td class="px-2">
                                                                        <div class="flex gap-2 -mt-2">
                                                                            @if (count($lands_boxes) > 1)
                                                                                <button type="button"
                                                                                    title="Remove"
                                                                                    wire:click="removeLandsFtl({{ $i }})"
                                                                                    class="w-6 h-6 px-1 pr-0.5 pt-0.5 text-md flex-none bg-red-600 text-white rounded-full">
                                                                                    -</button>
                                                                            @endif
                                                                            @if (count($lands_boxes) == ($i += 1))
                                                                                <button type="button"
                                                                                    title=""
                                                                                    wire:click="addLandsFtl"
                                                                                    class="w-6 h-6 px-1 pr-0.5 text-md flex-none bg-[#003399] text-white rounded-full">
                                                                                    +
                                                                                </button>
                                                                            @endif
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                @endif
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="grid grid-cols-1">
            <div class="bg-[#003399] text-white px-12 py-2 w-72 whitespace-nowrap"
                style="border-radius: 3px 60px 3px 3px;">Breakdown of
                Charges</div>
            <div class="border-b-2 border-[#003399]"></div>

            <div class="p-6 my-4 mt-2 space-y-6">
                <div class="grid grid-cols-1 px-12 space-y-6" style="padding-right: 20rem">
                    <div class="grid grid-cols-12 gap-24 px-6">
                        <div class="col-span-6 space-y-4">
                            <div class="grid grid-cols-12 gap-6">
                                <div class="col-span-4 text-[#003399] mt-2 whitespace-nowrap">
                                    Weight Charge :
                                </div>
                                <div class="col-span-8">
                                    <input type="text"
                                        class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                        name="weight_charge" wire:model='weight_charge' disabled>
                                    <x-input-error for="weight_charge" />
                                </div>
                            </div>
                            <div class="grid grid-cols-12 gap-6">
                                <div class="col-span-4 text-[#003399] mt-2 whitespace-nowrap">
                                    Waybill Fee :
                                </div>
                                <div class="col-span-8">
                                    <input type="text"
                                        class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                        name="awb_fee" wire:model='awb_fee' disabled>
                                    <x-input-error for="awb_fee" />
                                </div>
                            </div>
                            <div class="grid grid-cols-12 gap-6">
                                <div class="col-span-4 text-[#003399] mt-2 whitespace-nowrap">
                                    Valuation Fee :
                                </div>
                                <div class="col-span-8">
                                    <input type="text"
                                        class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                        name="valuation" wire:model='valuation' disabled>
                                    <x-input-error for="valuation" />
                                </div>
                            </div>
                            <div class="grid grid-cols-12 gap-6">
                                <div class="col-span-4 text-[#003399] mt-2 whitespace-nowrap">
                                    COD Charges :
                                </div>
                                <div class="col-span-8">
                                    <input type="text"
                                        class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                        name="cod_charge" wire:model='cod_charge' disabled>
                                    <x-input-error for="cod_charge" />
                                </div>
                            </div>
                            <div class="grid grid-cols-12 gap-6">
                                <div class="col-span-4 text-[#003399] mt-2 whitespace-nowrap">
                                    Insurance Fee :
                                </div>
                                <div class="col-span-8">
                                    <input type="text"
                                        class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                        name="insurance" wire:model='insurance' disabled>
                                    <x-input-error for="insurance" />
                                </div>
                            </div>
                            <div class="grid grid-cols-12 gap-6">
                                <div class="col-span-4 text-[#003399] mt-2 whitespace-nowrap">
                                    Handling Fee :
                                </div>
                                <div class="col-span-8">
                                    <input type="text"
                                        class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                        name="handling_fee" wire:model='handling_fee' disabled>
                                    <x-input-error for="handling_fee" />
                                </div>
                            </div>
                            <div class="grid grid-cols-12 gap-6">
                                <div class="col-span-4 text-[#003399] mt-2 whitespace-nowrap">
                                    <div class="flex gap-2">Other Fees :
                                        <div>
                                            <input type="checkbox"
                                                class="w-6 h-6 rounded-md border-[#003399] border cursor-pointer"
                                                name="other_fees" wire:model='other_fees'
                                                @if ($other_fees) checked @endif>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-span-8">
                                </div>
                            </div>
                            @if ($other_fees)
                                <div class="grid grid-cols-12 gap-6">
                                    <div class="col-span-4 text-[#003399] mt-2 whitespace-nowrap">
                                        OPA Fee :
                                    </div>
                                    <div class="col-span-8">
                                        <input type="text"
                                            class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="opa_fee" wire:model.defer='opa_fee'>
                                        <x-input-error for="opa_fee" />
                                    </div>
                                </div>
                                <div class="grid grid-cols-12 gap-6">
                                    <div class="col-span-4 text-[#003399] mt-2 whitespace-nowrap">
                                        ODA Fee :
                                    </div>
                                    <div class="col-span-8">
                                        <input type="text"
                                            class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="oda_fee" wire:model.defer='oda_fee'>
                                        <x-input-error for="oda_fee" />
                                    </div>
                                </div>
                                <div class="grid grid-cols-12 gap-6">
                                    <div class="col-span-4 text-[#003399] mt-2 whitespace-nowrap">
                                        Equipment Rental :
                                    </div>
                                    <div class="col-span-8">
                                        <input type="text"
                                            class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="equipment_rental" wire:model.defer='equipment_rental'>
                                        <x-input-error for="equipment_rental" />
                                    </div>
                                </div>
                                <div class="grid grid-cols-12 gap-6">
                                    <div class="col-span-4 text-[#003399] mt-2 whitespace-nowrap">
                                        Crating Fee :
                                    </div>
                                    <div class="col-span-8">
                                        <input type="text"
                                            class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="crating_fee" wire:model.defer='crating_fee'>
                                        <x-input-error for="crating_fee" />
                                    </div>
                                </div>
                                <div class="grid grid-cols-12 gap-6">
                                    <div class="col-span-4 text-[#003399] mt-2 whitespace-nowrap">
                                        Lashing :
                                    </div>
                                    <div class="col-span-8">
                                        <input type="text"
                                            class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="lashing_fee" wire:model.defer='lashing_fee'>
                                        <x-input-error for="lashing_fee" />
                                    </div>
                                </div>
                                <div class="grid grid-cols-12 gap-6">
                                    <div class="col-span-4 text-[#003399] mt-2 whitespace-nowrap">
                                        Manpower/Special Handling :
                                    </div>
                                    <div class="col-span-8">
                                        <input type="text"
                                            class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="manpower_fee" wire:model.defer='manpower_fee'>
                                        <x-input-error for="manpower_fee" />
                                    </div>
                                </div>
                                <div class="grid grid-cols-12 gap-6">
                                    <div class="col-span-4 text-[#003399] mt-2 whitespace-nowrap">
                                        Dangerous Goods Fee :
                                    </div>
                                    <div class="col-span-8">
                                        <input type="text"
                                            class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="dangerous_goods_fee" wire:model.defer='dangerous_goods_fee'>
                                        <x-input-error for="dangerous_goods_fee" />
                                    </div>
                                </div>
                                <div class="grid grid-cols-12 gap-6">
                                    <div class="col-span-4 text-[#003399] mt-2 whitespace-nowrap">
                                        Trucking :
                                    </div>
                                    <div class="col-span-8">
                                        <input type="text"
                                            class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="trucking_fee" wire:model.defer='trucking_fee'>
                                        <x-input-error for="trucking_fee" />
                                    </div>
                                </div>
                                <div class="grid grid-cols-12 gap-6">
                                    <div class="col-span-4 text-[#003399] mt-2 whitespace-nowrap">
                                        Perishable Fee :
                                    </div>
                                    <div class="col-span-8">
                                        <input type="text"
                                            class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="perishable_fee" wire:model.defer='perishable_fee'>
                                        <x-input-error for="perishable_fee" />
                                    </div>
                                </div>
                                <div class="grid grid-cols-12 gap-6">
                                    <div class="col-span-4 text-[#003399] mt-2 whitespace-nowrap">
                                        Packaging Fee :
                                    </div>
                                    <div class="col-span-8">
                                        <input type="text"
                                            class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="packaging_fee" wire:model.defer='packaging_fee'>
                                        <x-input-error for="packaging_fee" />
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="col-span-6 space-y-4">
                            <div class="grid grid-cols-12 gap-6">
                                <div class="col-span-4 text-[#003399] mt-2 whitespace-nowrap">
                                    Subtotal :
                                </div>
                                <div class="col-span-8">
                                    <input type="text"
                                        class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                        name="subtotal" wire:model='subtotal' disabled>
                                    <x-input-error for="subtotal" />
                                </div>
                            </div>
                            <div class="grid grid-cols-12 gap-6">
                                <div class="col-span-4 text-[#003399] mt-2 whitespace-nowrap">
                                    Discount Amount :
                                </div>
                                <div class="col-span-8">
                                    <input type="text"
                                        class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                        name="discount_amount" wire:model.defer='discount_amount'>
                                    <x-input-error for="discount_amount" />
                                </div>
                            </div>
                            <div class="grid grid-cols-12 gap-6">
                                <div class="col-span-4 text-[#003399] mt-2 whitespace-nowrap">
                                    Discount Percentage :
                                </div>
                                <div class="col-span-8">
                                    <input type="text"
                                        class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                        name="discount_percentage" wire:model.defer='discount_percentage' disabled>
                                    <x-input-error for="discount_percentage" />
                                </div>
                            </div>
                            <div class="grid grid-cols-12 gap-6">
                                <div class="col-span-4 text-[#003399] mt-2 whitespace-nowrap">
                                    <div class="flex gap-2">EVAT :
                                        <div>
                                            <input type="checkbox"
                                                class="w-6 h-6 rounded-md border-[#003399] border cursor-pointer"
                                                name="evat_chkbox" wire:model='evat_chkbox'
                                                @if ($evat_chkbox) checked @endif>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-span-8">
                                    <input type="text"
                                        class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                        name="evat" wire:model='evat' disabled>
                                    <x-input-error for="evat" />
                                </div>
                            </div>
                            <div class="grid grid-cols-12 gap-6">
                                <div class="col-span-4 text-[#003399] mt-2 whitespace-nowrap">
                                    Grand Total :
                                </div>
                                <div class="col-span-8">
                                    <input type="text"
                                        class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                        name="grand_total" wire:model='grand_total' disabled>
                                    <x-input-error for="grand_total" />
                                </div>
                            </div>
                            <div class="grid grid-cols-12 gap-6">
                                <div class="col-span-4 text-[#003399] mt-2 whitespace-nowrap">
                                </div>
                                <div class="col-span-8">
                                    <button type="button" wire:click="compute"
                                        class="px-6 py-2 text-md font-medium text-white bg-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:text-white hover:bg-blue-700">
                                        Compute
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid grid-cols-1">
            <div class="bg-[#003399] text-white px-12 py-2 w-72 whitespace-nowrap"
                style="border-radius: 3px 60px 3px 3px;">Terms and
                Conditions</div>
            <div class="border-b-2 border-[#003399]"></div>
            <div class="p-6 my-4 mt-2 space-y-6">
                <div class="grid grid-cols-1 px-12 space-y-2" style="padding-right: 20rem">
                    <div class="grid grid-cols-1 gap-24 px-6">
                        1. Charges subject to 12% VAT.
                    </div>
                    <div class="grid grid-cols-1 gap-24 px-6">
                        2. Subject to additional cranage fee; 10ftr and 20ftr
                        ctnr-Php4,500.00, 40ftr ctnr-Php6,500.00
                    </div>
                    <div class="grid grid-cols-1 gap-24 px-6">
                        3. Excluding other Port Charges if applicable i.e
                        revoyage fee, lift on/lift off fee, lashing, hustling fee, etc.
                    </div>
                    <div class="grid grid-cols-1 gap-24 px-6">

                        4. Rates apply to the shipment of General Cargo and SOC (Shipper’s Own Container).
                    </div>
                    <div class="grid grid-cols-1 gap-24 px-6">

                        5. Valuation Charges of Php5.00 per Php1,000 + vat in excess of allowable maximum declared
                        value: 10ftr – php 250,000; 20ftr – php500,000; 40ftr – 1 Million.
                    </div>
                    <div class="grid grid-cols-1 gap-24 px-6">

                        6. Maximum allowable Weight: 10footer – 9 tons; 20footer – 18tons; 40footer – 21tons
                    </div>
                    <div class="grid grid-cols-1 gap-24 px-6">
                        7. There will be two (2) calendar days of storage free time, otherwise standard Storage Fees
                        per container shall be imposed exceeding the free time mentioned: 10ftr – php1,000.00; 20ftr
                        – php1,000.00; 40ftr – php2,000.00
                    </div>
                    <div class="grid grid-cols-1 gap-24 px-6">
                        8. There will be two (2) calendar days of demurrage free time, otherwise standard Storage
                        Fees per container shall be imposed exceeding the free time mentioned: 10ftr – php1,000.00;
                        20ftr – php1,000.00; 40ftr – php2,000.00
                    </div>
                    <div class="grid grid-cols-1 gap-24 px-6">
                        9. Detention (containers pulled out as empty or released as laden but are overstaying at the
                        customers or its representative facility): 10ftr – php1,500.00,; 20ftr – php1,500.00; 40ftr
                        – php2,500.00
                    </div>
                    <div class="grid grid-cols-1 gap-24 px-6">
                        10. Payment should be in cash, managers, or cashier’s check unless with approved credit
                        terms as assessed from Finance.
                    </div>
                    <div class="grid grid-cols-1 gap-24 px-6">
                        11. No 10-footer and 40-footer acceptance unless with prior arrangement due to container
                        limitation of availability.
                    </div>
                    <div class="grid grid-cols-1 gap-24 px-6">
                        12. Rates are subject to government and 3rd party increases
                    </div>
                    <div class="grid grid-cols-1 gap-24 px-6">
                        13. Rates do not apply to Rolling Cargo and or Dangerous Goods
                    </div>
                    <div class="grid grid-cols-1 gap-24 px-6">
                        14. Rates are not applicable for outside city limits pick-up and delivery of origin and
                        destination port.
                    </div>
                    <div class="grid grid-cols-1 gap-24 px-6">
                        15. Loading and unloading of containers, equipment rental, lashing, hauling, manpower, and
                        other special handling are not included in the above-given rates.
                    </div>
                    <div class="grid grid-cols-1 gap-24 px-6">
                        16. Incidental cost is not included in the above rates.
                    </div>
                    <div class="grid grid-cols-1 gap-24 px-6">
                        17. Above rates are subject to change until further notice.
                    </div>
                </div>
            </div>
        </div>

        <div class="grid grid-cols-1 py-10 pr-4">
            <div class="flex justify-end gap-6 pr-8">
                @if ($from_sr == 'sr-create-quotation')
                    <button type="button" wire:click="action({},'save_sr_create_quotation')"
                        class="px-10 py-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:text-white hover:bg-[#003399]">
                        Save Quotation
                    </button>
                @else
                    <button type="button" wire:click="action({},'save_edit_quotation')"
                        class="px-10 py-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:text-white hover:bg-[#003399]">
                        Save Quotation
                    </button>
                @endif
                <button type="button"
                    wire:click="action({'id': {{ $quotation_id }}, 'from_sr': '{{ $from_sr }}'},'generate')"
                    class="px-10 py-4 text-sm flex-none bg-[#003399] text-white rounded-lg">
                    Generate Quotation
                </button>
            </div>
        </div>
    </div>
</div>
