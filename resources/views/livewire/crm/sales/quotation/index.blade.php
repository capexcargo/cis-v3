<x-form x-data="{
    {{-- view_modal: '{{ $view_modal }}' --}}
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>

    <x-slot name="modals">
    </x-slot>

    <x-slot name="header_title">Quotation</x-slot>
    <x-slot name="body">
        <div class="flex gap-12" style="margin-top: 3rem">
            <div class="w-48">
                <x-transparent.input type="text" label="Customer Name" name="customer_name_search"
                    wire:model.defer="customer_name_search">
                </x-transparent.input>
            </div>
            <div class="w-48">
                <x-transparent.input type="text" label="SR Name" name="sr_name_search"
                    wire:model.defer="sr_name_search">
                </x-transparent.input>
            </div>
            <div class="w-48">
                <x-transparent.input type="text" label="Quotation Ref No." name="quotation_ref_no_search"
                    wire:model.defer="quotation_ref_no_search">
                </x-transparent.input>
            </div><div class="w-48">
                <div class="relative z-0 w-full mb-5">
                    <input datepicker type="text" placeholder=" "
                        class="pt-2 pl-2 pb-1 block w-full mt-0 bg-transparent border-0 border-b-2 appearance-none focus:outline-none focus:ring-0 focus:border-[#003399] border-gray-600"
                        name="date_created_search" wire:model.defer="date_created_search">
                    <label class="absolute text-gray-500 duration-300 focus:text-blue top-3 -z-1 origin-0">
                        <span>Date Created</span>
                    </label>
                    <div class="absolute inset-y-0 right-0 flex items-center pr-3 pointer-events-none">
                        <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor"
                            viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd"
                                d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z"
                                clip-rule="evenodd"></path>
                        </svg>
                    </div>
                </div>
            </div>
            <div class="">
                <x-button type="button" wire:click="search" title="Search"
                    class="text-white bg-blue hover:bg-blue-800" />
            </div>
        </div>

        <div class="grid grid-cols-1 gap-4 mt-4">
            <div class="overflow-auto rounded-lg">
                <div class="grid grid-cols-1 gap-4 mt-4">
                    <div class="-mt-4 bg-white rounded-lg shadow-md">
                        <x-table.table>
                            <x-slot name="thead">
                                <x-table.th name="No." />
                                <x-table.th name="Quotation Ref. No." />
                                <x-table.th name="Shipment Type" />
                                <x-table.th name="Quotation Amount" />
                                <x-table.th name="Customer Number" />
                                <x-table.th name="Customer Name" />
                                <x-table.th name="SR Number" />
                                <x-table.th name="Date Created" />
                                <x-table.th name="Assigned to" />
                            </x-slot>
                            <x-slot name="tbody">
                                @foreach ($quotations as $i => $quotation)
                                    <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                        <td class="p-3 whitespace-nowrap">
                                            {{ ($quotations->currentPage() - 1) * $quotations->links()->paginator->perPage() + $loop->iteration }}.
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <p
                                                class="text-[#003399] underline"wire:click="action({'id': {{ $quotation->id }}, 'from_sr': 'show-quotation'},'quotation')">
                                                {{ $quotation->reference_no }}</p>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $quotation->shipmentType->name }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ number_format($quotation->shipmentCharges->grandtotal ?? null, 2) }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <p class="text-[#003399] underline">CN-0001</p>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $quotation->customerDetails->customerIndDetails->fullname }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <p class="text-[#003399] underline">{{ $quotation->sr_no }}</p>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ date('M. d, Y', strtotime($quotation->created_at)) }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $quotation->customerDetails->assignedTo->name }}
                                        </td>
                                    </tr>
                                @endforeach
                            </x-slot>
                        </x-table.table>
                        <div class="px-1 pb-2">
                            {{ $quotations->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
@push('heads')
    <style>
        .datepicker-grid>*+* {
            margin-top: .5rem
        }

        .datepicker-cell {
            margin-top: .5rem
        }
    </style>
@endpush
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.5/datepicker.min.js"></script>
@endpush
