<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
<div class="swiper mySwiper">
    <div class="swiper-wrapper">
        <div class="swiper-slide border-2 border-gray-400  w-full">
            <div class="" style="transform: translate3d(0,0,0);overflow:hidden">
                <header class="fixed top-0 w-full h-28 bg-white bottom-4 z-40">
                    <div class="div-header">
                        {{-- <div class="flex justify-start">
                            <div class="fixed"
                                style="
                                border-top: solid 105px #003399;
                                border-right: solid 95px transparent">
                            </div>
                            <div class="fixed"
                                style="
                                border: solid 6px #003399;
                                margin-left:2.9rem; transform: skewX(-42deg);
                                height:8rem">
                            </div>
                        </div> --}}
                        <div class="flex justify-between gap-2 px-12">
                            <div class="px-10">
                                <div class="px-4 py-[2px] mt-2">
                                    <img class="w-full h-20 header-icon-capex" src="/images/logo/capex-logo.png"
                                        alt="CaPEx_logo">
                                </div>
                            </div>
                            <div class="px-10" style="margin-top:-1.4rem; margin-bottom:-.6rem">
                                <div class="py-[2px] mt-2" style="margin-right: -2.4rem">
                                    <img class="w-full h-28 header-icon-iso" src="/images/logo/ISO.png" alt="ISO_logo">
                                </div>
                            </div>
                        </div>
                        <div class="px-12 border-lines">
                            <div class="px-10 space-y-1">
                                <div class="px-4">
                                    <div class="px-12 border-2 border-[#003399]">
                                    </div>
                                </div>
                                <div class="px-12" style="border:3px solid #003399">
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <main class="flex-1 px-12 overflow-y-scroll" style="z-index: 1; overflow: hidden;">
                    <div class="px-12 pt-8">
                        <div class="min-h-screen" style="padding-top: 4rem; padding-bottom:4rem">
                            <div class="px-4 my-6 space-y-4 text-sm text-left text-gray-600">
                                <p>{{ $date }}</p>
                                <div>
                                    <p>{{ $name }}</p>
                                    <p>{{ $position }}</p>
                                    <p>{{ $address }}</p>
                                    <p>{{ $mobile_number }}</p>
                                </div>
                                <p>Subject : <span class="font-semibold text-black text-md uppercase">QOUTATION FOR
                                        {{ $transport_mode }}FREIGHT - {{ $origin_code }}
                                        TO
                                        {{ $destination_code }}</span></p>
                            </div>
                            <div class="px-2 space-y-1">
                                <div class="px-12 border-2 border-gray-800">
                                </div>
                            </div>
                            <div class="px-4 my-6 space-y-4 text-sm text-left text-gray-700">
                                <p>Dear Valued Customer,</p>
                                <div>
                                    <p>Thank you for opportunity to offer our service rate for your forwarding
                                        requirement.
                                        With
                                        this, we are pleased to provide the following quote per your request.
                                        Below
                                        is the
                                        breakdown
                                        of charges for your perusal.</p>
                                </div>
                            </div>
                            <div class="px-2 my-6 space-y-4 text-sm text-left text-gray-700">
                                <div class="bg-[#003399] text-white px-2 text-left text-sm border-2 border-gray-800">
                                    SHIPMENT
                                    DETAILS</div>
                                <div class="grid grid-cols-4 text-sm text-black text_p_xs">
                                    <div class="px-2 font-semibold border border-gray-600">Shipment Type:</div>
                                    <div class="px-2 border border-gray-600">{{ $shipment_type }}</div>
                                    <div class="px-2 font-semibold border border-gray-600">Transport Mode:</div>
                                    <div class="px-2 border border-gray-600">AIR</div>

                                    <div class="px-2 font-semibold border border-gray-600">Origin:</div>
                                    <div class="px-2 border border-gray-600">{{ $origin }}</div>
                                    <div class="px-2 font-semibold border border-gray-600">Service Mode:</div>
                                    <div class="px-2 border border-gray-600">Door to Door</div>

                                    <div class="px-2 font-semibold border border-gray-600">Exact Pick-Up Address:</div>
                                    <div class="px-2 border border-gray-600">{{ $exact_pickup_address }}</div>
                                    <div class="px-2 font-semibold border border-gray-600">Quantity:</div>
                                    <div class="px-2 border border-gray-600">{{ $quantity }}</div>

                                    <div class="px-2 font-semibold border border-gray-600">Destination:</div>
                                    <div class="px-2 border border-gray-600">{{ $destination }}</div>
                                    <div class="px-2 font-semibold border border-gray-600">Weight(KG):</div>
                                    <div class="px-2 border border-gray-600">{{ $weight }}</div>

                                    <div class="px-2 font-semibold border border-gray-600">Exact Drop-Off Address:</div>
                                    <div class="px-2 border border-gray-600">{{ $exact_dropoff_address }}</div>
                                    <div class="px-2 font-semibold border border-gray-600">Dimension(LxWxH):</div>
                                    <div class="px-2 border border-gray-600">{{ $dimension }}</div>

                                    <div class="px-2 font-semibold border border-gray-600">Description of Goods:</div>
                                    <div class="px-2 border border-gray-600">{{ $description }}</div>
                                    <div class="px-2 font-semibold border border-gray-600">Paymode:</div>
                                    <div class="px-2 border border-gray-600">{{ $paymode }}</div>

                                    <div class="px-2 font-semibold border border-gray-600">Declared Value:</div>
                                    <div class="px-2 border border-gray-600">P{{ $declared_value }}</div>
                                    <div class="px-2 border border-gray-600"></div>
                                    <div class="px-2 border border-gray-600"></div>
                                </div>
                                <div class="bg-[#003399] text-white px-2 text-left text-sm border-2 border-gray-800">
                                    BREAKDOWN
                                    OF
                                    CHARGES</div>
                                <div class="grid grid-cols-4 text-sm text-black text_p_xs">
                                    <div class="px-2 font-semibold border border-gray-600">Weight Charge:</div>
                                    <div class="px-2 border border-gray-600">{{ $weight_charge }}</div>
                                    <div class="px-2 font-semibold border border-gray-600">Subtotal:</div>
                                    <div class="px-2 border border-gray-600">{{ $subtotal }}</div>

                                    <div class="px-2 font-semibold border border-gray-600">Waybill Charge:</div>
                                    <div class="px-2 border border-gray-600">{{ $awb_fee }}</div>
                                    <div class="px-2 font-semibold border border-gray-600">Discount Amount:</div>
                                    <div class="px-2 border border-gray-600">{{ $discount_amount }}</div>

                                    <div class="px-2 font-semibold border border-gray-600">Valuation Fee:</div>
                                    <div class="px-2 border border-gray-600">{{ $valuation }}</div>
                                    <div class="px-2 font-semibold border border-gray-600">Discount Percentage:</div>
                                    <div class="px-2 border border-gray-600">{{ $discount_percentage }}</div>

                                    <div class="px-2 font-semibold border border-gray-600">COD Charges:</div>
                                    <div class="px-2 border border-gray-600">{{ $cod_charge }}</div>
                                    <div class="px-2 font-semibold border border-gray-600">EVAT:</div>
                                    <div class="px-2 border border-gray-600">{{ $evat }}</div>

                                    <div class="px-2 font-semibold border border-gray-600">Insurance Fee:</div>
                                    <div class="px-2 border border-gray-600">{{ $insurance }}</div>
                                    <div class="px-2 font-semibold border border-gray-600">GRAND TOTAL:</div>
                                    <div class="px-2 font-bold underline border border-gray-600">{{ $grand_total }}
                                    </div>

                                    <div class="px-2 font-semibold border-t border-b border-l border-gray-600">Other
                                        Fee:
                                    </div>
                                    <div class="px-2 border-t border-b border-gray-600"></div>
                                    <div class="px-2 border-t border-b border-gray-600"></div>
                                    <div class="px-2 border-t border-b border-r border-gray-600"></div>

                                    <div class="px-4 font-semibold border border-gray-600">OPA Fee: </div>
                                    <div class="px-2 border border-gray-600">{{ $opa_fee }}</div>
                                    <div class="px-4 border border-gray-600"></div>
                                    <div class="px-4 border border-gray-600"></div>

                                    <div class="px-4 font-semibold border border-gray-600">ODA Fee: </div>
                                    <div class="px-2 border border-gray-600">{{ $oda_fee }}</div>
                                    <div class="px-4 border border-gray-600"></div>
                                    <div class="px-4 border border-gray-600"></div>

                                    <div class="px-4 font-semibold border border-gray-600">Equipment Rental:</div>
                                    <div class="px-2 border border-gray-600">{{ $equipment_rental }}</div>
                                    <div class="px-4 border border-gray-600"></div>
                                    <div class="px-4 border border-gray-600"></div>

                                    <div class="px-4 font-semibold border border-gray-600">Crating Fee:</div>
                                    <div class="px-2 border border-gray-600">{{ $crating_fee }}</div>
                                    <div class="px-4 border border-gray-600"></div>
                                    <div class="px-4 border border-gray-600"></div>

                                    <div class="px-4 font-semibold border border-gray-600">Manpower Fee:</div>
                                    <div class="px-2 border border-gray-600">{{ $manpower_fee }}</div>
                                    <div class="px-4 border border-gray-600"></div>
                                    <div class="px-4 border border-gray-600"></div>

                                    <div class="px-4 font-semibold border border-gray-600">Dangerous Goods Fee:</div>
                                    <div class="px-2 border border-gray-600">{{ $dangerous_goods_fee }}</div>
                                    <div class="px-4 border border-gray-600"></div>
                                    <div class="px-4 border border-gray-600"></div>

                                    <div class="px-4 font-semibold border border-gray-600">Trucking Fee:</div>
                                    <div class="px-2 border border-gray-600">{{ $trucking_fee }}</div>
                                    <div class="px-4 border border-gray-600"></div>
                                    <div class="px-4 border border-gray-600"></div>

                                    <div class="px-4 font-semibold border border-gray-600">Perishable Fee:</div>
                                    <div class="px-2 border border-gray-600">{{ $perishable_fee }}</div>
                                    <div class="px-4 border border-gray-600"></div>
                                    <div class="px-4 border border-gray-600"></div>

                                    <div class="px-4 font-semibold border border-gray-600">Packaging Fee:</div>
                                    <div class="px-2 border border-gray-600">{{ $packaging_fee }}</div>
                                    <div class="px-4 border border-gray-600"></div>
                                    <div class="px-4 border border-gray-600"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="fixed bottom-0 w-full bg-white z-30">
                    <div class="div-footer">
                        <div class="px-12">
                            <div class="px-10">
                                <div class="space-y-1">
                                    <div class="px-12 border-2 border-[#003399]">
                                    </div>
                                </div>
                                <div class="px-4 my-2 text-xs text-left text-gray-700 contact-sec">
                                    <div class="flex justify-between text-[#003399] gap-12">
                                        <div><span class="font-semibold">Main Office: </span>Bldg. 9a, Salem
                                            International
                                            Commercial Complex, Domestic Road, Pasay City <br>1301, Philippines
                                        </div>
                                        <div class="grid grid-cols-1">
                                            <div class="whitespace-nowrap"><span class="font-semibold">Telephone:
                                                </span>(+632) 8396-8888</div>
                                            <div class="whitespace-nowrap"><span class="font-semibold">Website:
                                                </span>www.capex.com.ph</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="flex justify-end" style="margin-top:-6rem">
                            <div class="shape-bottom-side"
                                style="
                                border: solid 6px #003399;
                                margin-right:-3.4rem; margin-top: -1.5rem; transform: skewX(-42deg);
                                height:9rem">
                            </div>
                            <div class=""
                                style="
                                border-bottom: solid 105px #003399;
                                border-left: solid 95px transparent">
                            </div>
                        </div> --}}
                    </div>
                </footer>
            </div>
        </div>
        {{-- SECOND SLIDE --}}
        <div class="swiper-slide border-2 border-gray-400  w-full">
            <div class="" style="transform: translate3d(0,0,0);overflow:hidden">
                <header class="fixed top-0 w-full h-28 bg-white bottom-4 z-40">
                    <div class="div-header">
                        {{-- <div class="flex justify-start">
                            <div class="fixed"
                                style="
                                border-top: solid 105px #003399;
                                border-right: solid 95px transparent">
                            </div>
                            <div class="fixed"
                                style="
                                border: solid 6px #003399;
                                margin-left:2.9rem; transform: skewX(-42deg);
                                height:8rem;">
                            </div>
                        </div> --}}
                        <div class="flex justify-between gap-2 px-12">
                            <div class="px-10">
                                <div class="px-4 py-[2px] mt-2">
                                    <img class="w-full h-20 header-icon-capex" src="/images/logo/capex-logo.png"
                                        alt="CaPEx_logo">
                                </div>
                            </div>
                            <div class="px-10" style="margin-top:-1.4rem; margin-bottom:-.6rem">
                                <div class="py-[2px] mt-2" style="margin-right: -2.4rem">
                                    <img class="w-full h-28 header-icon-iso" src="/images/logo/ISO.png"
                                        alt="ISO_logo">
                                </div>
                            </div>
                        </div>
                        <div class="px-12 border-lines">
                            <div class="px-10 space-y-1">
                                <div class="px-4">
                                    <div class="px-12 border-2 border-[#003399]">
                                    </div>
                                </div>
                                <div class="px-12" style="border:3px solid #003399">
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <main class="flex-1 px-12 overflow-y-scroll" style="z-index: 1; overflow: hidden;">
                    <div class="px-12 pt-8">
                        <div class="min-h-screen" style="padding-top: 4rem; padding-bottom:4rem">
                            <div class="px-4 my-4 space-y-3 text-sm text-left text-gray-700">
                                <div style="page-break-after: always;"></div>
                                <div class="next-page">
                                    <div>
                                        <p>We do hope that the above details will suffice your requirement, and
                                            we look forward
                                            to
                                            be
                                            the best service to your company soon.</p>
                                        <p>For further information, please feel free to get in touch with us
                                            through (02)
                                            8396-8888,
                                            or
                                            email us at <span
                                                class="text-[#003399] cursor-pointer underline">sales@capex.com.ph.</span>
                                        </p>
                                    </div>
                                    <div>
                                        <p>Again, Thank you for considering Cargo Padala Express as your
                                            logistic needs!</p>
                                    </div>
                                </div>
                                <div>
                                    <p class="underline">Terms & Conditions:</p>
                                </div>
                                <div class="px-6 mt-2 text-justify">
                                    <p>1. <span class="ml-2">Charges subject to 12% VAT</span></p>
                                    <p>2. <span class="ml-2">Subject to additional charge fee; 10ftr and
                                            20ftr
                                            ctnr-Php4,500,
                                            40ftr ctnr-Php6,500</span></p>
                                    <p>3. <span class="ml-2">Excluding other Port Charges if applicable
                                            i.e
                                            revoyage fee, lift on/lift off fee, lashing, hustling fee,
                                            etc.</span></p>
                                    <p>4. <span class="ml-2">Rates apply to the shipment of General Cargo and
                                            SOC
                                            (Shipper’s
                                            Own
                                            Container).</span></p>
                                    <p>5. <span class="ml-2">Valuation Charges of Php5.00 per Php1,000 + vat
                                            in excess of
                                            allowable maximum declared
                                            value: 10ftr – php 250,000; 20ftr – php500,000; 40ftr – 1
                                            Million</span></p>
                                    <p>6. <span class="ml-2">Maximum allowable Weight: 10footer – 9 tons;
                                            20footer –
                                            18tons;
                                            40footer – 21tons</span></p>
                                    <p>7. <span class="ml-2">There will be two (2) calendar days of storage
                                            free time,
                                            otherwise
                                            standard Storage Fees
                                            per container shall be imposed exceeding the free time mentioned:
                                            10ftr –
                                            php1,000.00;
                                            20ftr
                                            – php1,000.00; 40ftr – php2,000.00</span></p>
                                    <p>8. <span class="ml-2">There will be two (2) calendar days of demurrage
                                            free time,
                                            otherwise standard Storage
                                            Fees per container shall be imposed exceeding the free time
                                            mentioned: 10ftr –
                                            php1,000.00;
                                            20ftr – php1,000.00; 40ftr – php2,000.00</span></p>
                                    <p>9. <span class="ml-2">Detention (containers pulled out as empty or
                                            released as
                                            laden
                                            but
                                            are overstaying at the
                                            customers or its representative facility): 10ftr – php1,500.00,;
                                            20ftr –
                                            php1,500.00;
                                            40ftr
                                            – php2,500.00</span></p>
                                    <p>10. <span class="ml-2">Payment should be in cash, managers, or
                                            cashier’s check
                                            unless
                                            with
                                            approved credit
                                            terms as assessed from Finance.</span></p>
                                    <p>11. <span class="ml-2">No 10-footer and 40-footer acceptance unless
                                            with prior
                                            arrangement
                                            due to container
                                            limitation of availability.</span></p>
                                    <p>12. <span class="ml-2">Rates are subject to government and 3rd party
                                            increase.</span>
                                    </p>
                                    <p>13. <span class="ml-2">Rates do not apply to Rolling Cargo and or
                                            Dangerous
                                            Goods.</span>
                                    </p>
                                    <p>14. <span class="ml-2">Rates are not applicable for outside city
                                            limits pick-up
                                            and
                                            delivery of origin and
                                            destination port.</span></p>
                                    <p>15. <span class="ml-2">Loading and unloading of containers, equipment
                                            rental,
                                            lashing,
                                            hauling, manpower, and
                                            other special handling are not included in the above-given
                                            rates.</span></p>
                                    <p>16. <span class="ml-2">Incidental cost is not included in the above
                                            rates.</span>
                                    </p>
                                    <p>17. <span class="ml-2">Above rates are subject to change until further
                                            notice.</span>
                                    </p>
                                </div>

                                <div class="pt-4">
                                    <p class="">Prepared By:</p>
                                </div>
                                <div class="pt-4 pb-6">
                                    <p class="">(Marvin De Leon)</p>
                                    <p class="font-semibold">(Sales Assistant)</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="fixed bottom-0 w-full bg-white z-30">
                    <div class="div-footer">
                        <div class="px-12">
                            <div class="px-10">
                                <div class="space-y-1">
                                    <div class="px-12 border-2 border-[#003399]">
                                    </div>
                                </div>
                                <div class="px-4 my-2 text-xs text-left text-gray-700 contact-sec">
                                    <div class="flex justify-between text-[#003399] gap-12">
                                        <div><span class="font-semibold">Main Office: </span>Bldg. 9a, Salem
                                            International
                                            Commercial Complex, Domestic Road, Pasay City <br>1301, Philippines
                                        </div>
                                        <div class="grid grid-cols-1">
                                            <div class="whitespace-nowrap"><span class="font-semibold">Telephone:
                                                </span>(+632) 8396-8888</div>
                                            <div class="whitespace-nowrap"><span class="font-semibold">Website:
                                                </span>www.capex.com.ph</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="flex justify-end" style="margin-top:-6rem">
                            <div class="shape-bottom-side"
                                style="
                                border: solid 6px #003399;
                                margin-right:-3.4rem; margin-top: -1.5rem; transform: skewX(-42deg);
                                height:9rem">
                            </div>
                            <div class=""
                                style="
                                border-bottom: solid 105px #003399;
                                border-left: solid 95px transparent">
                            </div>
                        </div> --}}
                    </div>
                </footer>
            </div>
        </div>
    </div>
    <div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
    <div class="swiper-pagination"></div>
</div>
<!-- Swiper JS -->
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<script>
    var swiper = new Swiper(".mySwiper", {
        cssMode: true,
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        pagination: {
            el: ".swiper-pagination",
        },
        mousewheel: true,
        keyboard: true,
    });
</script>
