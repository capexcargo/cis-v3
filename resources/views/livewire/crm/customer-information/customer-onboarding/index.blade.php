<x-form x-data="{
    search_form: false,
    create_modal: '{{ $create_modal }}',
}">
    {{-- @if (!$name_search)
        <x-slot name="loading">
            <div wire:loading class="fixed top-0 left-0 z-50 block w-full h-full">
                <span class="relative block w-0 h-0 mx-auto my-0 text-blue top-1/2"
                    style="
                    top: 50%;">
                    <svg class="w-10 h-10 animate-spin" xmlns="http://www.w3.org/2000/svg" fill="none"
                        viewBox="0 0 24 24">
                        <circle class="opacity-25" cx="12" cy="12" r="10" stroke="currentColor"
                            stroke-width="4"></circle>
                        <path class="opacity-75" fill="currentColor"
                            d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z">
                        </path>
                    </svg>
                </span>
            </div>
        </x-slot>
    @endif --}}

    <x-slot name="modals">
        @if ($create_modal)
        @can('crm_customer_information_customer_data_mgmt_add')
            <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-2/5">
                {{-- <x-slot name="title">Add Contact</x-slot> --}}
                <x-slot name="body">
                    @livewire('crm.customer-information.customer-onboarding.create')
                </x-slot>
            </x-modal>
        @endcan
        @endif
    </x-slot>

    <x-slot name="header_title">Customer Onboarding</x-slot>
    <x-slot name="body">
        <div class="px-6 py-8 mb-8 bg-white rounded-lg shadow-md">
            <div class="flex gap-10 text-sm">
                <div class="w-80">
                    <div x-data="{ open: false }" class="relative mb-2 rounded-md" @click.away="open = false">
                        <div>
                            <span class="relative block">
                                <div class="relative z-0 w-full mb-5">
                                    <input type="text" @click="open = !open" wire:model='name_search'
                                        name="full_comp_name" placeholder=" "
                                        class="pt-2 pl-2 pb-1 block w-full mt-0 bg-transparent border-0 border-b-2 appearance-none focus:outline-none focus:ring-0 focus:border-[#003399] border-gray-600" />
                                    <label :class="{ 'whitespace-nowrap': open }"
                                        class="absolute text-gray-500 duration-300 form-control focus:text-blue focus:whitespace-nowrap top-3 -z-1 origin-0">
                                        <span
                                            class="@if ($name_search != '') whitespace-nowrap @endif leading-6">Customer
                                            Name/ Company Name</span>
                                    </label>
                                </div>
                            </span>
                        </div>
                        <div x-show="open" x-cloak
                            class="absolute w-full p-2 overflow-hidden overflow-y-auto bg-gray-100 rounded shadow max-h-96"
                            style="margin:-16px 0 0 0">
                            <ul class="list-reset">
                                @if ($name_search != '')
                                    @forelse ($name_references as $i => $customer_data)
                                        <li @click="open = !open"
                                            wire:click="getCustomerDetails({{ $customer_data->id }})"
                                            wire:key="{{ 'full_comp_name' . $i }}"
                                            class="px-2 text-black cursor-pointer hover:bg-gray-200">
                                            <p>
                                                {{ $customer_data->fullname }}
                                            </p>
                                        </li>
                                    @empty
                                        {{-- <li>
                                        <p class="px-2 text-black cursor-pointer hover:bg-gray-200">
                                            No Customer / Company Found.
                                        </p>
                                    </li> --}}
                                    @endforelse
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="w-72">
                    <x-transparent.input type="text" value="" label="Email Address" name="email_search"
                        wire:model.defer="email_search">
                    </x-transparent.input>
                </div>
                <div class="w-72">
                    <x-transparent.input type="text" value="" label="Mobile Number" name="mobile_number_search"
                        wire:model.defer="mobile_number_search" data-format="***********" data-mask="">
                    </x-transparent.input>
                </div>
                <div class="w-54">
                    <x-transparent.input type="text" value="" label="TIN" name="tin_search"
                        wire:model.defer="tin_search" data-format="************" data-mask="">
                    </x-transparent.input>
                </div>
            </div>
            <div class="mt-4">
                <x-button type="button" wire:click="search" title="Search"
                    class="text-white bg-blue hover:bg-blue-800" />
            </div>
        </div>
        @if (count($search_result) < 1 && $search_result != null)
            <div style="margin:9rem 0 0 -1rem">
                <div class="flex justify-center">
                    <div>
                        <h1 class="text-2xl font-medium" style="margin-left: -26px">No results found.</h1>
                        @can('crm_customer_information_customer_data_mgmt_add')
                            <button wire:click="action({}, 'add')"
                                class="p-2 px-4 mt-3 ml-1 text-sm text-white rounded-md bg-blue">
                                <div class="flex items-start justify-between">
                                    <svg class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                                        data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 448 512">
                                        <path fill="currentColor"
                                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                                    </svg>
                                    Add Contact
                                </div>
                            </button>
                        @endcan
                    </div>
                </div>
            </div>
        @endif
    </x-slot>
</x-form>

@push('scripts')
    <script>
        function doFormat(x, pattern, mask) {
            var strippedValue = x.replace(/[^0-9]/g, "");
            var chars = strippedValue.split('');
            var count = 0;

            var formatted = '';
            for (var i = 0; i < pattern.length; i++) {
                const c = pattern[i];
                if (chars[count]) {
                    if (/\*/.test(c)) {
                        formatted += chars[count];
                        count++;
                    } else {
                        formatted += c;
                    }
                } else if (mask) {
                    if (mask.split('')[i])
                        formatted += mask.split('')[i];
                }
            }
            return formatted;
        }

        document.querySelectorAll('[data-mask]').forEach(function(e) {
            function format(elem) {
                const val = doFormat(elem.value, elem.getAttribute('data-format'));
                elem.value = doFormat(elem.value, elem.getAttribute('data-format'), elem.getAttribute('data-mask'));

                if (elem.createTextRange) {
                    var range = elem.createTextRange();
                    range.move('character', val.length);
                    range.select();
                } else if (elem.selectionStart) {
                    elem.focus();
                    elem.setSelectionRange(val.length, val.length);
                }
            }
            e.addEventListener('keyup', function() {
                format(e);
            });
            e.addEventListener('keydown', function() {
                format(e);
            });
            format(e)
        });
    </script>
@endpush
{{-- @push('scripts')
    <script type="text/javascript">
        document.getElementsByClassName("add_contact_section")[0].style.visibility = 'hidden';
    </script>
@endpush --}}


{{-- @push('scripts')
    <script type="text/javascript" src="path_to/phil.min.js"></script>
    <script type="text/javascript">
        Philippines.getProvincesByRegion('01');
        console.log(Philippines.provinces);
    </script>
@endpush --}}
