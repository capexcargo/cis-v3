<div class="space-y-6">
    <x-loading></x-loading>
    <div class="space-y-4 cursor-pointer">
        <div class="grid grid-cols-1 -mt-4">
            <div class="flex space-x-2">
                <p class="text-3xl font-medium uppercase text-[#003399]">
                    @if ($data['account_type'] == 1)
                        {{ $data['first_name'] . ' ' . $data['middle_name'] . ' ' . $data['last_name'] }}
                    @else
                        {{ $data['company_name'] }}
                    @endif
                </p>
                <p class="mt-2 italic text-gray-500 text-md">{{ $data['customer_number'] }}</p>
                {{-- @if ($data['account_type'] == 2 && $data['associate_with'] != null)
                    <p class="mt-2 italic text-gray-500 text-md">
                        {{ $data['is_mother_account'] == 1 ? '(Mother Account)' : '(Sub-Account)' }}</p>
                @endif --}}
            </div>
            {{-- <div class="-mt-3">
                <p class="mt-2 text-lg font-medium">{{ $data['customer_number'] }}</p>
            </div> --}}
        </div>
        <div class="grid grid-cols-2 gap-6">
            <div class="text-sm font-medium border border-gray-500 rounded-md">
                <div class="px-3 py-2 text-lg bg-gray-200 border border-gray-500 rounded-t-md">
                    {{ $data['account_type'] == 1 ? 'Customer' : 'Company' }} Primary Details
                </div>
                <div class="flex flex-col p-3 space-y-2">
                    @if ($data['account_type'] == 2)
                        <div class="grid items-center grid-cols-3 gap-3">
                            <p class="text-xs text-gray-500">Industry:</p>
                            <p class="col-span-2">
                                {{ $industry }}
                            </p>
                        </div>
                        <div class="grid items-center grid-cols-3 gap-3">
                            <p class="text-xs text-gray-500">Website / Social Media Account:</p>
                            <p class="col-span-2 text-[#003399] underline">
                                {{ $data['company_website_soc_med'] }}
                            </p>
                        </div>
                        @if ($data['associate_with'] != null)
                            <div class="grid items-center grid-cols-3 gap-3">
                                <p class="text-xs text-gray-500">Associate With:</p>
                                <p class="col-span-2">
                                    <span class="text-[#003399]">{{ $associate_with }}
                                        <span class="text-xs italic text-gray-500">
                                            ({{ $associate_with_accnt_no }})
                                        </span>
                                    </span>
                                </p>
                            </div>
                        @endif
                    @endif
                    @if (!empty($data['emails'][0]['email']))
                        <div class="grid items-center grid-cols-3 gap-3">
                            <p class="text-xs text-gray-500">Email Addresses</p>
                            <p class="col-span-2">
                                @foreach ($data['emails'] as $i => $val)
                                    <span class="flex gap-1">
                                        {{ $val['email'] }}
                                        @if ($val['default'])
                                            <span title="primary">
                                                <svg class="w-4 h-4 mt-[2px] text-blue" aria-hidden="true"
                                                    focusable="false" data-prefix="far" data-icon="approve"
                                                    role="img" xmlns="http://www.w3.org/2000/svg"
                                                    viewBox="0 0 576 512">
                                                    <path fill="currentColor"
                                                        d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" />
                                                </svg>
                                            </span>
                                        @endif
                                    </span>
                                @endforeach
                            </p>
                        </div>
                    @endif
                    @if (!empty($data['telephone_numbers'][0]['telephone']))
                        <div class="grid items-center grid-cols-3 gap-3">
                            <p class="text-xs text-gray-500">Telephone Numbers:</p>
                            <p class="col-span-2">
                                @foreach ($data['telephone_numbers'] as $i => $val)
                                    <span class="flex gap-1">
                                        {{ $val['telephone'] }}
                                        @if ($val['default'])
                                            <span title="primary">
                                                <svg class="w-4 h-4 mt-[2px] text-blue" aria-hidden="true"
                                                    focusable="false" data-prefix="far" data-icon="approve"
                                                    role="img" xmlns="http://www.w3.org/2000/svg"
                                                    viewBox="0 0 576 512">
                                                    <path fill="currentColor"
                                                        d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" />
                                                </svg>
                                            </span>
                                        @endif
                                    </span>
                                @endforeach
                            </p>
                        </div>
                    @endif
                    @if (!empty($data['mobile_numbers'][0]['mobile']))
                        <div class="grid items-center grid-cols-3 gap-3">
                            <p class="text-xs text-gray-500">Mobile Numbers:</p>
                            <p class="col-span-2">
                                @foreach ($data['mobile_numbers'] as $i => $val)
                                    <span class="flex gap-1">
                                        {{ $val['mobile'] }}
                                        @if ($val['default'])
                                            <span title="primary">
                                                <svg class="w-4 h-4 mt-[2px] text-blue" aria-hidden="true"
                                                    focusable="false" data-prefix="far" data-icon="approve"
                                                    role="img" xmlns="http://www.w3.org/2000/svg"
                                                    viewBox="0 0 576 512">
                                                    <path fill="currentColor"
                                                        d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" />
                                                </svg>
                                            </span>
                                        @endif
                                    </span>
                                @endforeach
                            </p>
                        </div>
                    @endif
                    @if ($data['account_type'] == 2)
                        <div class="grid items-center grid-cols-3 gap-3">
                            <p class="text-xs text-gray-500">TIN:</p>
                            <p class="col-span-2">
                                {{ $data['tin'] }}
                            </p>
                        </div>
                        <div class="grid items-center grid-cols-3 gap-3">
                            <p class="text-xs text-gray-500">Company Anniversary:</p>
                            <p class="col-span-2">
                                {{ date('M. d, Y', strtotime($data['company_anniversary'])) }}
                            </p>
                        </div>
                    @endif
                </div>
            </div>
            @if (!empty($data['addresses'][0]['address_line1']))
                <div class="text-sm font-medium border border-gray-500 rounded-md">
                    <div class="px-3 py-2 text-lg bg-gray-200 border border-gray-500 rounded-t-md">Addresses
                    </div>

                    <div class="flex flex-col p-3 space-y-2">
                        <div class="flex flex-col p-3 space-y-2">
                            {{-- <div class="grid items-center grid-cols-3 gap-3">
                            <p class="text-xs text-gray-500">Address 1 (Office):</p>
                            <p class="col-span-2">Saint Monique Valais, Pantok Binangonan, Rizal</p>
                        </div> --}}
                            @foreach ($data['addresses'] as $i => $val)
                                <div class="grid items-center grid-cols-3 gap-3">
                                    <p class="text-xs text-gray-500">
                                        Address {{ $i + 1 }} (<span
                                            class="italic">{{ $val['address_label'] == 1 ? 'Home' : ($val['address_label'] == 2 ? 'Office' : 'Warehouse') }}</span>):
                                    </p>
                                    <div class="col-span-2">
                                        <div class="flex gap-1">
                                            <div class="grid gap-2">
                                                {{ $val['address_line1'] }}
                                                {{ $val['address_line2'] }}
                                                {{ $val['state_province'] ?? null != null ? $state[$i] . ',' : '' }}
                                                {{ $val['city_municipality'] ?? null != null ? $city[$i] . ',' : '' }}
                                                {{ $val['barangay'] ?? null != null ? $barangay[$i] . ',' : '' }}
                                                <div class="flex gap-2 -mt-2">
                                                    {{ $val['postal_code'] ?? null }} - PH
                                                    @if ($val['default'])
                                                        <span title="primary">
                                                            <svg class="w-4 h-4 mt-[2px] text-blue" aria-hidden="true"
                                                                focusable="false" data-prefix="far" data-icon="approve"
                                                                role="img" xmlns="http://www.w3.org/2000/svg"
                                                                viewBox="0 0 576 512">
                                                                <path fill="currentColor"
                                                                    d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" />
                                                            </svg>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
            <div class="text-sm font-medium border border-gray-500 rounded-md">
                <div class="px-3 py-2 text-lg bg-gray-200 border border-gray-500 rounded-t-md">Additional Details
                </div>
                <div class="flex flex-col p-3 space-y-2">
                    <div class="flex flex-col p-3 space-y-2">
                        <div class="grid items-center grid-cols-3 gap-3">
                            <p class="text-xs text-gray-500">Life Stage:</p>
                            <p class="col-span-2">Lead</p>
                        </div>
                        <div class="grid items-center grid-cols-3 gap-3">
                            <p class="text-xs text-gray-500">Account Type:</p>
                            <p class="col-span-2">Non-Account</p>
                        </div>
                        <div class="grid items-center grid-cols-3 gap-3">
                            <p class="text-xs text-gray-500">Contact Owner:</p>
                            <p class="col-span-2">{{ $contact_owner }}</p>
                        </div>
                        <div class="grid items-center grid-cols-3 gap-3">
                            <p class="text-xs text-gray-500">Marketing Channel:</p>
                            <p class="col-span-2">{{ $marketing_channel }}</p>
                        </div>
                        <div class="grid items-center grid-cols-3 gap-3">
                            <p class="text-xs text-gray-500">Notes:</p>
                            <p class="col-span-2">{{ $data['notes'] ?? $data['con_per_notes'] }}</p>
                        </div>
                    </div>
                </div>
            </div>
            @if ($data['account_type'] == 2)
                <div class="text-sm font-medium border border-gray-500 rounded-md">
                    <div class="px-3 py-2 text-lg bg-gray-200 border border-gray-500 rounded-t-md">Contact Person
                        Details
                    </div>
                    <div class="flex flex-col p-3 space-y-2">
                        <div class="grid items-center grid-cols-3 gap-3">
                            <p class="text-xs text-gray-500">Name:</p>
                            <p class="col-span-2">
                                {{ $data['contact_persons'][0]['first_name'] . ' ' . $data['contact_persons'][0]['middle_name'] . ' ' . $data['contact_persons'][0]['last_name'] }}
                            </p>
                        </div>
                        <div class="grid items-center grid-cols-3 gap-3">
                            <p class="text-xs text-gray-500">Position:</p>
                            <p class="col-span-2">
                                {{ $data['contact_persons'][0]['position'] }}
                            </p>
                        </div>
                        @if (!empty($data['contact_persons'][0]['emails'][0]['email']))
                            <div class="grid items-center grid-cols-3 gap-3">
                                <p class="text-xs text-gray-500">Email Addresses</p>
                                <p class="col-span-2">
                                    @foreach ($data['contact_persons'][0]['emails'] as $i => $val)
                                        <span class="flex gap-1">
                                            {{ $val['email'] }}
                                            @if ($val['default'])
                                                <span>
                                                    <svg class="w-4 h-4 mt-[2px] text-blue" aria-hidden="true"
                                                        focusable="false" data-prefix="far" data-icon="approve"
                                                        role="img" xmlns="http://www.w3.org/2000/svg"
                                                        viewBox="0 0 576 512">
                                                        <path fill="currentColor"
                                                            d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" />
                                                    </svg>
                                                </span>
                                            @endif
                                        </span>
                                    @endforeach
                                </p>
                            </div>
                        @endif
                        @if (!empty($data['contact_persons'][0]['telephone_numbers'][0]['telephone']))
                            <div class="grid items-center grid-cols-3 gap-3">
                                <p class="text-xs text-gray-500">Telephone Numbers:</p>
                                <p class="col-span-2">
                                    @foreach ($data['contact_persons'][0]['telephone_numbers'] as $i => $val)
                                        <span class="flex gap-1">
                                            {{ $val['telephone'] }}
                                            @if ($val['default'])
                                                <span>
                                                    <svg class="w-4 h-4 mt-[2px] text-blue" aria-hidden="true"
                                                        focusable="false" data-prefix="far" data-icon="approve"
                                                        role="img" xmlns="http://www.w3.org/2000/svg"
                                                        viewBox="0 0 576 512">
                                                        <path fill="currentColor"
                                                            d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" />
                                                    </svg>
                                                </span>
                                            @endif
                                        </span>
                                    @endforeach
                                </p>
                            </div>
                        @endif
                        @if (!empty($data['contact_persons'][0]['mobile_numbers'][0]['mobile']))
                            <div class="grid items-center grid-cols-3 gap-3">
                                <p class="text-xs text-gray-500">Mobile Numbers:</p>
                                <p class="col-span-2">
                                    @foreach ($data['contact_persons'][0]['mobile_numbers'] as $i => $val)
                                        <span class="flex gap-1">
                                            {{ $val['mobile'] }}
                                            @if ($val['default'])
                                                <span>
                                                    <svg class="w-4 h-4 mt-[2px] text-blue" aria-hidden="true"
                                                        focusable="false" data-prefix="far" data-icon="approve"
                                                        role="img" xmlns="http://www.w3.org/2000/svg"
                                                        viewBox="0 0 576 512">
                                                        <path fill="currentColor"
                                                            d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" />
                                                    </svg>
                                                </span>
                                            @endif
                                        </span>
                                    @endforeach
                                </p>
                            </div>
                        @endif
                    </div>
                </div>
            @endif
        </div>
    </div>
    <div class="flex justify-end space-x-4">
        {{-- <button type="button" wire:click="$set('perview_summary_modal', false)"
            class="px-10 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
            Cancel
        </button> --}}
        <div class="mt-4">
            <button type="button" wire:click="submit"
                class="px-12 py-3 text-xs flex-none bg-[#003399] text-white rounded-lg">
                Confirm
            </button>
        </div>
    </div>
</div>
