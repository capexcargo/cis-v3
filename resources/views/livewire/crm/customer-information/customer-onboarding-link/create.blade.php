    @section('title', $pageTitle)

    <div wire:init="load" x-data="{
        perview_summary_modal: '{{ $perview_summary_modal }}',
        current_tab: {{ $current_tab }},
    }">
        <x-loading></x-loading>
        <div class="flex justify-center py-4">
            @if ($account_type)
                <div class="overflow-hidden border border-gray-400 rounded-md bg-gray-100 px-8 py-4 w-2/5">
                    @if ($perview_summary_modal)
                        <x-modal id="perview_summary_modal" size="w-1/2 xs:w-1/2 sm:w-1/2 md:w-1/2">
                            <x-slot name="body">
                                <span class="relative block">
                                    <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                                        wire:click="$set('perview_summary_modal', false)">
                                    </span>
                                </span>
                                <div>
                                    @livewire('crm.customer-information.customer-onboarding-link.preview-summary', ['data' => $this->getRequest()])
                                </div>
                            </x-slot>
                        </x-modal>
                    @endif

                    <div class="pl-3 space-y-4" x-cloak x-show="current_tab == 1">
                        <div class="flex gap-4 mb-4">
                            <div class="col-span-11 hidden">
                                <div class="grid grid-cols-2 gap-6">
                                    <ul class="flex mt-2">
                                        @foreach ($account_type_cards as $i => $status_card)
                                            <li wire:click="accountType({{ $account_type_cards[$i]['id'] }})"
                                                class="px-8 py-1 text-sm font-semibold {{ $account_type_cards[$i]['class'] }}
                                                {{ $account_type == $account_type_cards[$i]['id'] ? 'bg-blue-100 text-[#003399] border border-[#003399]' : ' border border-gray-600' }}">
                                                <button
                                                    wire:click="$set('{{ $account_type_cards[$i]['action'] }}', {{ $account_type_cards[$i]['id'] }})">
                                                    {{ $account_type_cards[$i]['title'] }}
                                                </button>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">
                        <div class="pl-3 mt-5 space-y-4" x-cloak x-show="current_tab == 1">
                            <div class="grid grid-cols-12 gap-6" style="width:92%">
                                <div class="col-span-4">
                                    <x-label for="customer_number" value="Customer Number" />
                                    <x-input type="text" name="customer_number" wire:model.defer='customer_number'
                                        disabled>
                                    </x-input>
                                    <x-input-error for="customer_number" />
                                </div>
                                @if ($account_type == 2)
                                    <div class="col-span-8">
                                        <x-label for="company_name" value="Company Name" :required="true" />
                                        <x-input type="text" name="company_name"
                                            wire:model.defer='company_name'></x-input>
                                        <x-input-error for="company_name" />
                                    </div>
                                @endif
                            </div>
                            @if ($account_type != 2)
                                <div class="flex gap-4" style="padding-right: 2.8rem; width:98%">
                                    <div class="col-span-11">
                                        <div class="grid grid-cols-3 gap-6">
                                            <div class="">
                                                <x-label for="first_name" value="First Name" :required="true" />
                                                <x-input type="text" name="first_name"
                                                    wire:model.defer='first_name'></x-input>
                                                <x-input-error for="first_name" />
                                            </div>
                                            <div class="">
                                                <x-label for="middle_name" value="Middle Name" />
                                                <x-input type="text" name="middle_name"
                                                    wire:model.defer='middle_name'></x-input>
                                                <x-input-error for="middle_name" />
                                            </div>
                                            <div class="">
                                                <x-label for="last_name" value="Last Name" :required="true" />
                                                <x-input type="text" name="last_name"
                                                    wire:model.defer='last_name'></x-input>
                                                <x-input-error for="last_name" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            @if ($account_type == 2)
                                <div class="grid grid-cols-12 gap-6" style="width:92%">
                                    <div class="col-span-4">
                                        <x-label for="company_industry" value="Company Industry" :required="true" />
                                        <x-select name="company_industry" wire:model.defer='company_industry'>
                                            <option value="">Select</option>
                                            @foreach ($industry_references as $i => $industry_ref)
                                                <option value="{{ $industry_ref->id }}">{{ $industry_ref->name }}
                                                </option>
                                            @endforeach
                                        </x-select>
                                        <x-input-error for="company_industry" />
                                    </div>
                                    <div class="col-span-8">
                                        <x-label for="company_website_soc_med"
                                            value="Company Website / Social Media Account" />
                                        <x-input type="text" name="company_website_soc_med"
                                            wire:model.defer='company_website_soc_med'>
                                        </x-input>
                                        <x-input-error for="company_website_soc_med" />
                                    </div>
                                </div>
                                <div class="flex gap-4" style="width:98%">
                                    <div class="grid grid-cols-1">
                                        <div class="flex gap-6 mt-1 text-sm font-medium text-gray-500">
                                            <div class=""><input type="radio" name="is_mother_account"
                                                    wire:model='is_mother_account' value="1"
                                                    class="border-[#003399] border">
                                                Mother Account
                                            </div>
                                            <div class=""><input type="radio" name="is_mother_account"
                                                    wire:model='is_mother_account' value="0"
                                                    class="border-[#003399] border">
                                                Sub-Account
                                            </div>
                                        </div>
                                        <x-input-error for="is_mother_account" />
                                    </div>
                                </div>
                                <div class="flex gap-4" style="width:98%">
                                    <div class="w-52">
                                        <x-label for="associate_with" value="Associate With" />
                                        <select
                                            class="text-gray-600 block w-full mt-1 border border-gray-500 p-[5px] rounded-md shadow-sm focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50"
                                            name="associate_with" wire:model.defer='associate_with'
                                            @if ($is_mother_account != 0 || $is_mother_account == '') disabled @endif>
                                            <option value="">Select</option>
                                            @foreach ($associate_with_references as $i => $associate_with_ref)
                                                <option value="{{ $associate_with_ref->id }}">
                                                    {{ $associate_with_ref->fullname }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <x-input-error for="associate_with" />
                                    </div>
                                </div>
                            @endif

                            <div class="flex gap-4 mb-2" style="width:98%">
                                <div class="grid w-full grid-cols-1 gap-4">
                                    @foreach ($emails as $i => $email)
                                        <div style="{{ count($emails) > 1 ? 'margin: 0 0 -6px 0' : '' }}">
                                            <div class="relative z-0"
                                                style="{{ count($emails) > 1 ? 'width:95%' : 'width:100%' }}">
                                                <div class="">
                                                    @if ($account_type == 2)
                                                        <x-label for="email_address" value="Company Email Address"
                                                            :required="true" />
                                                    @else
                                                        <x-label for="email_address" value="Email Address" />
                                                    @endif
                                                    <x-input type="text" name="emails.{{ $i }}.email"
                                                        wire:model.defer='emails.{{ $i }}.email'></x-input>
                                                </div>
                                                <div class="flex gap-2 mt-2 font-medium text-gray-500">
                                                    <div class="mt-[-8px]">
                                                        <input type="checkbox"
                                                            class="rounded-sm border-[#003399] border default_email"
                                                            name="emails.{{ $i }}.default"
                                                            wire:model.defer='emails.{{ $i }}.default'
                                                            wire:change="checkEmailDefault({{ $i }})">
                                                    </div>
                                                    <div class="text-sm" style="margin-top:-4px">Set as Primary Email
                                                        Address
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="relative w-full">
                                                <div class="absolute right-0 w-18"
                                                    style="margin-top: -3.6rem;margin-right: -4px;">
                                                    @if (count($emails) > 1)
                                                        <button type="button" title="Remove Email Address"
                                                            wire:click="removeEmails({{ $i }})"
                                                            class="w-7 h-7 text-xl flex-none bg-white text-blue border border-[#003399] rounded-full">
                                                            -</button>
                                                    @endif
                                                </div>
                                                <x-input-error for="emails.{{ $i }}.email" />
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="mt-[2px]">
                                    <button type="button" title="Add Email Address" wire:click="addEmails"
                                        class="mt-6 w-7 h-7 text-xl flex-none bg-[#003399] text-white rounded-full">
                                        +</button>
                                </div>
                            </div>

                            <div class="flex gap-4" style="width:98%">
                                <div class="grid w-full grid-cols-1 gap-4">
                                    @foreach ($telephone_numbers as $i => $telephone)
                                        <div style="{{ count($telephone_numbers) > 1 ? 'margin: 0 0 -6px 0' : '' }}">
                                            <div class="relative z-0"
                                                style="{{ count($telephone_numbers) > 1 ? 'width:95%' : 'width:100%' }}">
                                                <div class="">
                                                    <x-label for="telephone_number"
                                                        value="{{ $account_type == 2 ? 'Company' : '' }} Telephone Number" />
                                                    <input type="text"
                                                        class="block w-full border border-gray-500 p-[5px] rounded-md shadow-sm focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50"
                                                        x-data x-init="new IMask($refs.mobileNumberInput, { mask: '00000000000' })" x-ref="mobileNumberInput"
                                                        name="telephone_numbers.{{ $i }}.telephone"
                                                        wire:model.defer="telephone_numbers.{{ $i }}.telephone">
                                                </div>
                                                <div class="flex gap-2 mt-2 font-medium text-gray-500">
                                                    <div class="mt-[-8px]"><input type="checkbox"
                                                            class="rounded-sm border-[#003399] border default_telephone_number"
                                                            name="telephone_numbers.{{ $i }}.default"
                                                            wire:model.defer='telephone_numbers.{{ $i }}.default'
                                                            wire:change="checkTelephoneNumberDefault({{ $i }})">
                                                    </div>
                                                    <div class="mt-[2px] text-sm" style="margin-top:-4px">Set as
                                                        Primary
                                                        Telephone
                                                        Number</div>
                                                </div>
                                            </div>
                                            <div class="relative w-full">
                                                <div class="absolute right-0 w-18"
                                                    style="margin-top: -3.6rem;margin-right: -4px;">
                                                    @if (count($telephone_numbers) > 1)
                                                        <button type="button" title="Remove Telephone Number"
                                                            wire:click="removeTelephoneNumbers({{ $i }})"
                                                            class="w-7 h-7 text-xl flex-none bg-white text-blue border border-[#003399] rounded-full">
                                                            -</button>
                                                    @endif
                                                </div>
                                                <x-input-error
                                                    for="telephone_numbers.{{ $i }}.telephone" />
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="mt-[2px]">
                                    <button type="button" title="Add Telephone Number"
                                        wire:click="addTelephoneNumbers"
                                        class="mt-6 w-7 h-7 text-xl flex-none bg-[#003399] text-white rounded-full">
                                        +</button>
                                </div>
                            </div>

                            <div class="flex gap-4 mb-2" style="width:98%">
                                <div class="grid w-full grid-cols-1 gap-4">
                                    @foreach ($mobile_numbers as $i => $mobile)
                                        <div style="{{ count($mobile_numbers) > 1 ? 'margin: 0 0 -6px 0' : '' }}">
                                            <div class="relative z-0"
                                                style="{{ count($mobile_numbers) > 1 ? 'width:95%' : 'width:100%' }}">
                                                <div class="">
                                                    <x-label for="mobile_number"
                                                        value="{{ $account_type == 2 ? 'Company' : '' }} Mobile Number"
                                                        :required="true" />
                                                    <input type="text"
                                                        class="block w-full border border-gray-500 p-[5px] rounded-md shadow-sm focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50"
                                                        x-data x-init="new IMask($refs.mobileNumberInput, { mask: '0000-000-0000' })" x-ref="mobileNumberInput"
                                                        name="mobile_numbers.{{ $i }}.mobile"
                                                        wire:model.defer="mobile_numbers.{{ $i }}.mobile">
                                                </div>
                                                <div class="flex gap-2 mt-2 font-medium text-gray-500">
                                                    <div class="mt-[-8px]"><input type="checkbox"
                                                            class="rounded-sm border-[#003399] border default_mobile_number"
                                                            name="mobile_numbers.{{ $i }}.default"
                                                            wire:model.defer='mobile_numbers.{{ $i }}.default'
                                                            wire:change="checkMobileNumberDefault({{ $i }})">
                                                    </div>
                                                    <div class="mt-[2px] text-sm" style="margin-top:-4px">Set as
                                                        Primary
                                                        Mobile
                                                        Number</div>
                                                </div>
                                            </div>
                                            <div class="relative w-full">
                                                <div class="absolute right-0 w-18"
                                                    style="margin-top: -3.6rem;margin-right: -4px;">
                                                    @if (count($mobile_numbers) > 1)
                                                        <button type="button" title="Remove Mobile Number"
                                                            wire:click="removeMobileNumbers({{ $i }})"
                                                            class="w-7 h-7 text-xl flex-none bg-white text-blue border border-[#003399] rounded-full">
                                                            -</button>
                                                    @endif
                                                </div>
                                                <x-input-error for="mobile_numbers.{{ $i }}.mobile" />
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="mt-[2px]">
                                    <button type="button" title="Add Mobile Number" wire:click="addMobileNumbers"
                                        class="mt-6 w-7 h-7 text-xl flex-none bg-[#003399] text-white rounded-full">
                                        +</button>
                                </div>
                            </div>

                            @if ($account_type == 2)
                                <div class="flex gap-4" style="width:92%">
                                    <div class="col-span-11">
                                        <div class="grid grid-cols-12 gap-6">
                                            <div class="col-span-4">
                                                <x-label for="tin" value="TIN" :required="true" />
                                                <input type="text"
                                                    class="block w-full border border-gray-500 p-[5px] rounded-md shadow-sm focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50"
                                                    x-data x-init="new IMask($refs.mobileNumberInput, { mask: '000000000000' })" x-ref="mobileNumberInput"
                                                    name="tin" wire:model.defer="tin">
                                                <x-input-error for="tin" />
                                            </div>
                                            <div class="col-span-8">
                                                <x-label for="company_anniversary" value="Company Anniversary"
                                                    :required="true" />
                                                <x-input type="date" name="company_anniversary"
                                                    wire:model.defer='company_anniversary'>
                                                </x-input>
                                                <x-input-error for="company_anniversary" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            <div class="flex gap-4">
                                <div class="grid grid-cols-1">
                                    <div>
                                        <x-label for="address_type" value="Address" :require="true" />
                                    </div>
                                </div>
                            </div>

                            @foreach ($addresses as $i => $address)
                                <div class="py-4 border border-gray-500 rounded-lg shadow-md"
                                    style="margin-top:-2px; width:92%; margin-bottom:18px">
                                    <div class="px-4 mb-2">
                                        <div class="flex justify-between">
                                            <div class="flex gap-6 mt-1 text-sm font-medium text-gray-500">
                                                <div class="">
                                                    <input type="radio" class="border-[#003399] border"
                                                        name="addresses.{{ $i }}.address_type"
                                                        wire:model='addresses.{{ $i }}.address_type'
                                                        value="1"
                                                        wire:change="getAddressType({{ $i }})">
                                                    Domestic
                                                </div>
                                                <div class="">
                                                    <input type="radio" class="border-[#003399] border"
                                                        name="addresses.{{ $i }}.address_type"
                                                        wire:model='addresses.{{ $i }}.address_type'
                                                        value="2"
                                                        wire:change="getAddressType({{ $i }})">
                                                    International
                                                </div>
                                            </div>
                                            <div class="flex">
                                                @if (count($addresses) > 1)
                                                    <div>
                                                        <button type="button" title="Remove Address"
                                                            wire:click="removeAddresses({{ $i }})"
                                                            class="w-7 h-7 text-xl flex-none bg-white text-blue border border-[#003399] rounded-full">
                                                            -</button>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="flex gap-4 px-4 mb-4">
                                        <div class="grid w-full grid-cols-1 space-y-4">
                                            @if ($addresses[$i]['address_type'] == 2)
                                                <div>
                                                    <x-label for="country" value="Country" :required="true" />
                                                    <x-select name="addresses.{{ $i }}.country"
                                                        wire:model.defer='addresses.{{ $i }}.country'>
                                                        <option value="">Select</option>
                                                        <option value="1">Country1</option>
                                                    </x-select>
                                                    <x-input-error for="addresses.{{ $i }}.country" />
                                                </div>
                                            @endif
                                            <div>
                                                <x-label for="address_line1" value="Address Line 1"
                                                    :required="true" />
                                                <x-input type="text"
                                                    name="addresses.{{ $i }}.address_line1"
                                                    wire:model.defer='addresses.{{ $i }}.address_line1'>
                                                </x-input>
                                                <x-input-error for="addresses.{{ $i }}.address_line1" />
                                            </div>
                                            <div>
                                                <x-label for="address_line2" value="Address Line 2" />
                                                <x-input type="text"
                                                    name="addresses.{{ $i }}.address_line2"
                                                    wire:model.defer='addresses.{{ $i }}.address_line2'>
                                                </x-input>
                                                <x-input-error for="addresses.{{ $i }}.address_line2" />
                                            </div>
                                            <div class="flex gap-4">
                                                <div class="">
                                                    <div class="grid grid-cols-3 gap-8">
                                                        <div>
                                                            <x-label class="whitespace-nowrap" for="state_province"
                                                                value="State {{ $addresses[$i]['address_type'] != 2 ? '/ Province' : '' }}"
                                                                :required="true" />
                                                            <x-select
                                                                name="addresses.{{ $i }}.state_province"
                                                                wire:model='addresses.{{ $i }}.state_province'
                                                                wire:change="getStateProvinceID({{ $i }})">
                                                                <option value="">Select</option>
                                                                @foreach ($state_province_references as $state_province)
                                                                    <option value="{{ $state_province->id }}">
                                                                        {{ $state_province->name }}</option>
                                                                @endforeach
                                                            </x-select>
                                                            <x-input-error
                                                                for="addresses.{{ $i }}.state_province" />
                                                        </div>
                                                        <div>
                                                            <x-label class="whitespace-nowrap" for="city_municipality"
                                                                value="City {{ $addresses[$i]['address_type'] != 2 ? '/ Municipality' : '' }}"
                                                                :required="true" />
                                                            <x-select
                                                                name="addresses.{{ $i }}.city_municipality"
                                                                wire:model='addresses.{{ $i }}.city_municipality'
                                                                wire:change="getCityMunicipalID({{ $i }})">
                                                                <option value="">Select</option>
                                                                @foreach ($addresses[$i]['city_municipal_references'] as $a => $city_municipal)
                                                                    <option value="{{ $city_municipal['id'] }}">
                                                                        {{ $city_municipal['name'] }}</option>
                                                                @endforeach
                                                            </x-select>
                                                            <x-input-error
                                                                for="addresses.{{ $i }}.city_municipality" />
                                                        </div>
                                                        <div>
                                                            @if ($addresses[$i]['address_type'] != 2)
                                                                <x-label class="whitespace-nowrap" for="barangay"
                                                                    value="Barangay" :required="true" />
                                                                <x-select
                                                                    name="addresses.{{ $i }}.barangay"
                                                                    wire:model.defer='addresses.{{ $i }}.barangay'>
                                                                    <option value="">Select</option>
                                                                    @foreach ($addresses[$i]['barangay_references'] as $barangay)
                                                                        <option value="{{ $barangay['id'] }}">
                                                                            {{ $barangay['name'] }}</option>
                                                                    @endforeach
                                                                </x-select>
                                                                <x-input-error
                                                                    for="addresses.{{ $i }}.barangay" />
                                                            @endif
                                                        </div>
                                                        <div>
                                                            <x-label for="postal_code" value="Postal Code"
                                                                :required="true" />
                                                            <x-input type="text"
                                                                name="addresses.{{ $i }}.postal_code"
                                                                wire:model.defer='addresses.{{ $i }}.postal_code'
                                                                disabled>
                                                            </x-input>
                                                            <x-input-error
                                                                for="addresses.{{ $i }}.postal_code" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="flex gap-4 px-4 mb-4">
                                        <div class="grid grid-cols-1">
                                            <div>
                                                <x-label for="address_label" value="Address Label"
                                                    :required="true" />
                                            </div>
                                            <div class="flex gap-6 mt-1 text-sm font-medium text-gray-500">
                                                @if ($account_type != 2)
                                                    <div class="">
                                                        <input type="radio" class="border-[#003399] border"
                                                            name="addresses.{{ $i }}.address_label"
                                                            wire:model.defer='addresses.{{ $i }}.address_label'
                                                            value="1"> Home
                                                    </div>
                                                @endif
                                                <div class="">
                                                    <input type="radio" class="border-[#003399] border"
                                                        name="addresses.{{ $i }}.address_label"
                                                        wire:model.defer='addresses.{{ $i }}.address_label'
                                                        value="2"> Office
                                                </div>
                                                <div class="">
                                                    <input type="radio" class="border-[#003399] border"
                                                        name="addresses.{{ $i }}.address_label"
                                                        wire:model.defer='addresses.{{ $i }}.address_label'
                                                        value="3"> Warehouse
                                                </div>
                                            </div>
                                            <x-input-error for="addresses.{{ $i }}.address_label" />
                                        </div>
                                    </div>
                                    <div class="w-full mb-4 border-t border-gray-500"></div>
                                    <div class="flex gap-4 px-4 font-medium text-gray-500">
                                        <div class="mt-[-8px]"><input type="checkbox"
                                                class="rounded-sm border-[#003399] border"
                                                name="addresses.{{ $i }}.default"
                                                wire:model.defer='addresses.{{ $i }}.default'
                                                wire:change="checkAddressDefault({{ $i }})">
                                        </div>
                                        <div class="mt-[2px] text-sm" style="margin-top:-4px">Set as Primary Address
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="flex gap-4" style="margin-top: -8px">
                                <button type="button" title="Add Address" wire:click="addAddresses"
                                    class="w-7 h-7 text-xl flex-none bg-[#003399] text-white rounded-full">
                                    +</button>
                            </div>
                            @if ($account_type != 2)
                                <div class="flex gap-4">
                                    <div class="col-span-11">
                                        <div class="grid grid-cols-3 gap-6">
                                            <div class="">
                                                <x-label for="life_stage" value="Life Stage" />
                                                <x-input type="text" name="life_stage"
                                                    wire:model.defer='life_stage' disabled>
                                                </x-input>
                                                <x-input-error for="life_stage" />
                                            </div>
                                            <div class="">
                                                <x-label for="account_type" value="Account Type" />
                                                <input type="text"
                                                    value="{{ $account_type == 1 ? 'Non-Account' : 'Account' }}"
                                                    class="text-gray-600 block w-full border border-gray-500 p-[5px] rounded-md
                                    shadow-sm focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50"
                                                    disabled>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flex gap-4">
                                    <div class="col-span-11">
                                        <div class="grid grid-cols-3 gap-6">
                                            <div class="">
                                                <x-label for="contact_owner" value="Contact Owner"
                                                    :required="true" />
                                                <x-select name="contact_owner" wire:model.defer='contact_owner'>
                                                    <option value="">Select</option>
                                                    @foreach ($contact_owner_references as $i => $contact_owner_ref)
                                                        <option value="{{ $contact_owner_ref->id }}">
                                                            {{ $contact_owner_ref->name }}
                                                        </option>
                                                    @endforeach
                                                </x-select>
                                                <x-input-error for="contact_owner" />
                                            </div>
                                            <div class="">
                                                <x-label for="marketing_channel" value="Marketing Channel"
                                                    :required="true" />
                                                <x-select name="marketing_channel"
                                                    wire:model.defer='marketing_channel'>
                                                    <option value="">Select</option>
                                                    @foreach ($marketing_channel_references as $i => $marketing_channel_ref)
                                                        <option value="{{ $marketing_channel_ref->id }}">
                                                            {{ $marketing_channel_ref->name }}
                                                        </option>
                                                    @endforeach
                                                </x-select>
                                                <x-input-error for="marketing_channel" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flex gap-4">
                                    <div class="w-full col-span-11">
                                        <div class="">
                                            <x-label for="notes" value="Notes" />
                                            <x-textarea type="text" name="notes"
                                                wire:model.defer='notes'></x-textarea>
                                            <x-input-error for="notes" />
                                        </div>
                                    </div>
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                </div>
                            @endif

                            <div class="flex justify-end gap-4">
                                <div class="col-span-11">
                                    <div class="flex gap-6 mt-8">
                                        {{-- <button type="button" wire:click="$emit('close_modal', 'create')" --}}
                                        <button type="button" wire:click="clear"
                                            class="px-10 py-2 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:text-white hover:bg-red-400">
                                            Clear
                                        </button>
                                        @if ($account_type != 2)
                                            <button type="submit"
                                                class="px-10 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg">
                                                Submit
                                            </button>
                                        @else
                                            <button type="submit"
                                                class="px-10 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg">
                                                Next
                                            </button>
                                        @endif
                                    </div>
                                </div>
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                        </div>
                    </form>
                    {{-- CONTACT PERSON ENTRY --}}
                    <form wire:submit.prevent="submitCorpContactPerson" autocomplete="off">
                        <div class="-mt-2 text-2xl font-semibold text-gray-800" x-cloak x-show="current_tab == 2">
                            Contact Person
                        </div>
                        <div class="pl-3 mt-5 space-y-4" x-cloak x-show="current_tab == 2">
                            @foreach ($contact_persons as $i => $contact_person)
                                <div class="pb-4 border border-gray-500 rounded-lg shadow-md"
                                    style="margin-top:-2px; width:98%; margin-bottom:18px; padding-top: {{ count($contact_persons) == 1 ? '1.5rem' : '1rem' }}">
                                    <div class="px-4 mb-2 -mt-2">
                                        <div class="flex justify-between">
                                            <div class="flex gap-2 mt-3 font-medium text-gray-500">
                                                <div class="mt-[-8px]">
                                                    <input type="checkbox"
                                                        class="rounded-sm border-[#003399] border default_position"
                                                        name="contact_persons.{{ $i }}.default"
                                                        wire:model.defer='contact_persons.{{ $i }}.default'
                                                        wire:change="checkContactPersonDefault({{ $i }})">
                                                </div>
                                                <div class="text-sm" style="margin-top:-4px">Set as Primary Contact
                                                    Person
                                                </div>
                                                {{-- <x-input-error for="contact_persons.{{ $i }}.default" /> --}}
                                            </div>
                                            <div class="flex -mr-2">
                                                @if (count($contact_persons) > 1)
                                                    <button type="button" title="Remove Contact Person"
                                                        wire:click="removeContactPersons({{ $i }})"
                                                        class="w-7 h-7 text-xl flex-none bg-white text-blue border border-[#003399] rounded-full">
                                                        -</button>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="w-full pb-2 border-b-2 border-gray-300"></div>
                                    <div class="flex gap-4 px-4 mt-3">
                                        <div class="col-span-11">
                                            <div class="grid grid-cols-3 gap-6">
                                                <div class="">
                                                    <x-label for="con_per_first_name" value="First Name"
                                                        :required="true" />
                                                    <x-input type="text"
                                                        name="contact_persons.{{ $i }}.first_name"
                                                        wire:model.defer='contact_persons.{{ $i }}.first_name'>
                                                    </x-input>
                                                    <x-input-error
                                                        for="contact_persons.{{ $i }}.first_name" />
                                                </div>
                                                <div class="">
                                                    <x-label for="con_per_middle_name" value="Middle Name" />
                                                    <x-input type="text"
                                                        name="contact_persons.{{ $i }}.middle_name"
                                                        wire:model.defer='contact_persons.{{ $i }}.middle_name'></x-input>
                                                    <x-input-error
                                                        for="contact_persons.{{ $i }}.middle_name" />
                                                </div>
                                                <div class="">
                                                    <x-label for="con_per_last_name" value="Last Name"
                                                        :required="true" />
                                                    <x-input type="text"
                                                        name="contact_persons.{{ $i }}.last_name"
                                                        wire:model.defer='contact_persons.{{ $i }}.last_name'>
                                                    </x-input>
                                                    <x-input-error
                                                        for="contact_persons.{{ $i }}.last_name" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="flex gap-4 px-4 mt-2 mb-2 w-80">
                                        <div class="grid w-full grid-cols-1 gap-4">
                                            <div
                                                style="{{ count($contact_persons) > 1 ? 'margin: 0 0 -6px 0' : '' }}">
                                                <div class=""
                                                    style="{{ count($contact_persons) > 1 ? 'width:95%' : 'width:100%' }}">
                                                    <div class="w-52">
                                                        <x-label for="position" value="Position" :required="true" />
                                                        <x-input type="text"
                                                            name="contact_persons.{{ $i }}.position"
                                                            wire:model.defer='contact_persons.{{ $i }}.position'>
                                                        </x-input>
                                                    </div>
                                                    <x-input-error
                                                        for="contact_persons.{{ $i }}.position" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="w-full pt-4 border-b-2 border-gray-300"></div>
                                    <div class="flex gap-4 px-4 mb-2" style="width:98%">
                                        <div class="grid w-full grid-cols-1 gap-4 mt-6">
                                            @foreach ($contact_persons[$i]['emails'] as $x => $email)
                                                <div
                                                    style="{{ count($contact_persons[$i]['emails']) > 1 ? 'margin: 0 0 -6px 0' : '' }}">
                                                    <div class="relative z-0"
                                                        style="{{ count($contact_persons[$i]['emails']) > 1 ? 'width:95%' : 'width:100%' }}">
                                                        <div class="">
                                                            <x-label for="email_address" value="Email Address"
                                                                :required="true" />
                                                            <x-input type="text"
                                                                name="contact_persons.{{ $i }}.emails.{{ $x }}.email"
                                                                wire:model.defer='contact_persons.{{ $i }}.emails.{{ $x }}.email'>
                                                            </x-input>
                                                        </div>
                                                        <div class="flex gap-2 mt-2 font-medium text-gray-500">
                                                            <div class="mt-[-8px]">
                                                                <input type="checkbox"
                                                                    class="rounded-sm border-[#003399] border default_email"
                                                                    name="contact_persons.{{ $i }}.emails.{{ $x }}.default"
                                                                    wire:model.defer='contact_persons.{{ $i }}.emails.{{ $x }}.default'
                                                                    wire:change="checkConPerEmailDefault({{ $x }})">
                                                            </div>
                                                            <div class="text-sm" style="margin-top:-4px">Set as
                                                                Primary
                                                                Email
                                                                Address
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="relative w-full">
                                                        <div class="absolute right-0 w-18"
                                                            style="margin-top: -3.6rem;margin-right: -4px;">
                                                            @if (count($contact_persons[$i]['emails']) > 1)
                                                                <button type="button" title="Remove Email Address"
                                                                    wire:click="removeConPerEmails({{ $i }}, {{ $x }})"
                                                                    class="w-7 h-7 text-xl flex-none bg-white text-blue border border-[#003399] rounded-full">
                                                                    -</button>
                                                            @endif
                                                        </div>
                                                        <x-input-error
                                                            for="contact_persons.{{ $i }}.emails.{{ $x }}.email" />
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="mt-[1px]">
                                            <button type="button" title="Add Email Address"
                                                wire:click="addConPerEmails('{{ $i }}')"
                                                class="mt-12 w-7 h-7 text-xl flex-none bg-[#003399] text-white rounded-full">
                                                +</button>
                                        </div>
                                    </div>
                                    <div class="flex gap-4 px-4 mb-2" style="width:98%">
                                        <div class="grid w-full grid-cols-1 gap-4 mt-6">
                                            @foreach ($contact_persons[$i]['telephone_numbers'] as $x => $telephone_number)
                                                <div
                                                    style="{{ count($contact_persons[$i]['telephone_numbers']) > 1 ? 'margin: 0 0 -6px 0' : '' }}">
                                                    <div class="relative z-0"
                                                        style="{{ count($contact_persons[$i]['telephone_numbers']) > 1 ? 'width:95%' : 'width:100%' }}">
                                                        <div class="">
                                                            <x-label for="telephone_number"
                                                                value="Telephone Number" />
                                                            <input type="text"
                                                                class="block w-full border border-gray-500 p-[5px] rounded-md shadow-sm focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50"
                                                                x-data x-init="new IMask($refs.mobileNumberInput, { mask: '00000000000' })"
                                                                x-ref="mobileNumberInput"
                                                                name="contact_persons.{{ $i }}.telephone_numbers.{{ $x }}.telephone"
                                                                wire:model.defer='contact_persons.{{ $i }}.telephone_numbers.{{ $x }}.telephone'>
                                                        </div>
                                                        <div class="flex gap-2 mt-2 font-medium text-gray-500">
                                                            <div class="mt-[-8px]">
                                                                <input type="checkbox"
                                                                    class="rounded-sm border-[#003399] border default_telephone"
                                                                    name="contact_persons.{{ $i }}.telephone_numbers.{{ $x }}.default"
                                                                    wire:model.defer='contact_persons.{{ $i }}.telephone_numbers.{{ $x }}.default'
                                                                    wire:change="checkConPerTelephoneNumberDefault({{ $x }})">
                                                            </div>
                                                            <div class="text-sm" style="margin-top:-4px">Set as
                                                                Primary
                                                                Email
                                                                Address
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="relative w-full">
                                                        <div class="absolute right-0 w-18"
                                                            style="margin-top: -3.6rem;margin-right: -4px;">
                                                            @if (count($contact_persons[$i]['telephone_numbers']) > 1)
                                                                <button type="button" title="Remove Telephone Number"
                                                                    wire:click="removeConPerTelephoneNumbers({{ $i }}, {{ $x }})"
                                                                    class="w-7 h-7 text-xl flex-none bg-white text-blue border border-[#003399] rounded-full">
                                                                    -</button>
                                                            @endif
                                                        </div>
                                                        <x-input-error
                                                            for="contact_persons.{{ $i }}.telephone_numbers.{{ $x }}.telephone" />
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="mt-[1px]">
                                            <button type="button" title="Add Telephone Number"
                                                wire:click="addConPerTelephoneNumbers('{{ $i }}')"
                                                class="mt-12 w-7 h-7 text-xl flex-none bg-[#003399] text-white rounded-full">
                                                +</button>
                                        </div>
                                    </div>
                                    <div class="flex gap-4 px-4 mb-2" style="width:98%">
                                        <div class="grid w-full grid-cols-1 gap-4 mt-6">
                                            @foreach ($contact_persons[$i]['mobile_numbers'] as $x => $mobile_number)
                                                <div
                                                    style="{{ count($contact_persons[$i]['mobile_numbers']) > 1 ? 'margin: 0 0 -6px 0' : '' }}">
                                                    <div class="relative z-0"
                                                        style="{{ count($contact_persons[$i]['mobile_numbers']) > 1 ? 'width:95%' : 'width:100%' }}">
                                                        <div class="">
                                                            <x-label for="mobile_number" value="Mobile Number"
                                                                :required="true" />
                                                            <input type="text"
                                                                class="block w-full border border-gray-500 p-[5px] rounded-md shadow-sm focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50"
                                                                x-data x-init="new IMask($refs.mobileNumberInput, { mask: '0000-000-0000' })"
                                                                x-ref="mobileNumberInput"
                                                                name="contact_persons.{{ $i }}.mobile_numbers.{{ $x }}.mobile"
                                                                wire:model.defer='contact_persons.{{ $i }}.mobile_numbers.{{ $x }}.mobile'>
                                                        </div>
                                                        <div class="flex gap-2 mt-2 font-medium text-gray-500">
                                                            <div class="mt-[-8px]">
                                                                <input type="checkbox"
                                                                    class="rounded-sm border-[#003399] border default_mobile"
                                                                    name="contact_persons.{{ $i }}.mobile_numbers.{{ $x }}.default"
                                                                    wire:model.defer='contact_persons.{{ $i }}.mobile_numbers.{{ $x }}.default'
                                                                    wire:change="checkConPerMobileNumberDefault({{ $x }})">
                                                            </div>
                                                            <div class="text-sm" style="margin-top:-4px">Set as
                                                                Primary
                                                                Email
                                                                Address
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="relative w-full">
                                                        <div class="absolute right-0 w-18"
                                                            style="margin-top: -3.6rem;margin-right: -4px;">
                                                            @if (count($contact_persons[$i]['mobile_numbers']) > 1)
                                                                <button type="button" title="Remove Mobile Number"
                                                                    wire:click="removeConPerMobileNumbers({{ $i }}, {{ $x }})"
                                                                    class="w-7 h-7 text-xl flex-none bg-white text-blue border border-[#003399] rounded-full">
                                                                    -</button>
                                                            @endif
                                                        </div>
                                                        <x-input-error
                                                            for="contact_persons.{{ $i }}.mobile_numbers.{{ $x }}.mobile" />
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="mt-[1px]">
                                            <button type="button" title="Add Mobile Number"
                                                wire:click="addConPerMobileNumbers('{{ $i }}')"
                                                class="mt-12 w-7 h-7 text-xl flex-none bg-[#003399] text-white rounded-full">
                                                +</button>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="flex gap-4" style="margin-top: -8px;">
                                <button type="button" title="Add Contact Person" wire:click="addContactPersons"
                                    class="w-7 h-7 text-xl flex-none bg-[#003399] text-white rounded-full">
                                    +</button>
                            </div>
                            <div class="flex gap-4">
                                <div class="col-span-11">
                                    <div class="grid grid-cols-3 gap-6">
                                        <div class="">
                                            <x-label for="con_per_life_stage" value="Life Stage" />
                                            <x-input type="text" name="con_per_life_stage"
                                                wire:model.defer='con_per_life_stage' disabled>
                                            </x-input>
                                            <x-input-error for="con_per_life_stage" />
                                        </div>
                                        <div class="">
                                            <x-label for="con_per_account_type" value="Account Type" />
                                            <input type="text"
                                                value="{{ $account_type == 1 ? 'Non-Account' : 'Account' }}"
                                                class="text-gray-600 block w-full border border-gray-500 p-[5px] rounded-md
                                shadow-sm focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50"
                                                disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="flex gap-4">
                                <div class="col-span-11">
                                    <div class="grid grid-cols-3 gap-6">
                                        <div class="">
                                            <x-label for="con_per_contact_owner" value="Contact Owner"
                                                :required="true" />
                                            <x-select name="con_per_contact_owner"
                                                wire:model.defer='con_per_contact_owner'>
                                                <option value="">Select</option>
                                                @foreach ($contact_owner_references as $i => $contact_owner_ref)
                                                    <option value="{{ $contact_owner_ref->id }}">
                                                        {{ $contact_owner_ref->name }}
                                                    </option>
                                                @endforeach
                                            </x-select>
                                            <x-input-error for="con_per_contact_owner" />
                                        </div>
                                        <div class="">
                                            <x-label for="con_per_marketing_channel" value="Marketing Channel"
                                                :required="true" />
                                            <x-select name="con_per_marketing_channel"
                                                wire:model.defer='con_per_marketing_channel'>
                                                <option value="">Select</option>
                                                @foreach ($marketing_channel_references as $i => $marketing_channel_ref)
                                                    <option value="{{ $marketing_channel_ref->id }}">
                                                        {{ $marketing_channel_ref->name }}
                                                    </option>
                                                @endforeach
                                            </x-select>
                                            <x-input-error for="con_per_marketing_channel" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="flex gap-4">
                                <div class="w-full col-span-11">
                                    <div class="">
                                        <x-label for="con_per_notes" value="Notes" />
                                        <x-textarea type="text" name="con_per_notes"
                                            wire:model.defer='con_per_notes'>
                                        </x-textarea>
                                        <x-input-error for="con_per_notes" />
                                    </div>
                                </div>
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                            <div class="flex justify-end gap-4 mr-2">
                                <div class="col-span-11">
                                    <div class="flex gap-6 mt-4">
                                        <button type="button" wire:click="$set('current_tab', 1)"
                                            class="px-10 py-2 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:text-white hover:bg-red-400">
                                            Back
                                        </button>
                                        <button type="submit"
                                            class="px-10 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg">
                                            Submit
                                        </button>
                                    </div>
                                </div>
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                        </div>
                    </form>
                </div>
            @else
                <div
                    class="relative flex items-top justify-center min-h-screen dark:bg-gray-900 sm:items-center sm:pt-0">
                    <div class="bg-white p-8 rounded shadow-lg">
                        <h1 class="text-3xl font-bold mb-4">404 Not Found</h1>
                        <p class="text-gray-600 text-xl">The page you are looking for does not exist.</p>
                    </div>
                </div>
            @endif
        </div>
    </div>
