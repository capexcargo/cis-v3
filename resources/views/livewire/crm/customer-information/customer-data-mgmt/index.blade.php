<x-form wire:init="load" x-data="{
    search_form: false,
    view_modal: '{{ $view_modal }}',
    edit_modal: '{{ $edit_modal }}',
    confirmation_modal: '{{ $confirmation_modal }}'
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>

    <x-slot name="modals">
        @can('crm_customer_information_customer_data_mgmt_view')
            @if ($customer_id && $view_modal)
                <x-modal id="view_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-4/5">
                    {{-- <x-slot name="title">View Customer</x-slot> --}}
                    <x-slot name="body">
                        @livewire('crm.customer-information.customer-data-mgmt.view', ['id' => $customer_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        @can('crm_customer_information_customer_data_mgmt_edit')
            @if ($customer_id && $edit_modal)
                <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-2/5">
                    {{-- <x-slot name="title">Edit Contact</x-slot> --}}
                    <x-slot name="body">
                        @livewire('crm.customer-information.customer-data-mgmt.edit', ['id' => $customer_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        @can('crm_customer_information_customer_data_mgmt_delete')
            @if ($customer_id && $cat_status && $confirmation_modal)
                <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                    <x-slot name="body">
                        <span class="relative block">
                            <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                                wire:click="$set('confirmation_modal', false)">
                            </span>
                        </span>
                        @if ($cat_status == 'Deactivate')
                            <div class="mb-3 text-lg font-semibold text-center">
                                Are you sure you want to deactivate this contact?
                            </div>
                        @else
                            <div class="mb-3 text-lg font-semibold text-center">
                                Are you sure you want to activate this contact?
                            </div>
                        @endif
                        <div class="flex justify-center gap-6 mt-6 space-x-3">
                            <button type="button" wire:click="$set('confirmation_modal', false)"
                                class="px-10 py-2 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:text-white hover:bg-[#003399]">
                                NO
                            </button>
                            <button type="button" wire:click="update"
                                class="px-10 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg">
                                YES
                            </button>
                        </div>
                    </x-slot>
                </x-modal>
            @endif
        @endcan

    </x-slot>

    <x-slot name="header_title">Customer Data Management</x-slot>
    <x-slot name="header_button">
        @can('crm_customer_information_customer_data_mgmt_view')
            @if ($account_type == 2)
                <button wire:click="action({}, '')" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
                    Apply for Accounts
                </button>
            @endif
        @endcan
    </x-slot>
    <x-slot name="body">
        <div class="flex gap-6">
            <button wire:click="accountType(1)"
                class="{{ $account_type == 1 ? 'border-2' : '' }} flex-none px-12 py-8 bg-white border border-[#003399] rounded-lg hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
                <h5 class="font-medium text-gray-700 uppercase dark:text-gray-400">Individual</h5>
            </button>
            <button wire:click="accountType(2)"
                class="{{ $account_type == 2 ? 'border-2' : '' }} flex-none px-12 py-8 bg-white border border-[#003399] rounded-lg hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
                <h5 class="font-medium text-gray-700 uppercase dark:text-gray-400">Corporate</h5>
            </button>
        </div>
        <div class="">
            <div class="flex gap-10">
                <div class="w-54 whitespace-nowrap">
                    <x-transparent.input type="text" value="" label="Customer Name"
                        wire:model.defer='name_search' name="name_search">
                    </x-transparent.input>
                </div>
                <div class="w-54">
                    <x-transparent.input type="text" value="" label="Email Address" name="email_search"
                        wire:model.defer="email_search">
                    </x-transparent.input>
                </div>
                <div class="w-54">
                    <x-transparent.input type="number" value="" label="Mobile Number" name="mobile_number_search"
                        wire:model.defer="mobile_number_search">
                    </x-transparent.input>
                </div>
                <div class="">
                    <x-button type="button" wire:click="search" title="Search"
                        class="text-white bg-blue hover:bg-blue-800" />
                </div>
            </div>
        </div>
        <div class="grid grid-cols-1 gap-4 mt-4">
            <div class="col-span-2">
                <div class="grid grid-cols-10 gap-6">
                    <div class="col-span-9">
                        <ul class="flex mt-2">
                            @foreach ($status_header_cards as $i => $status_card)
                                <li
                                    class="px-3 py-1 text-xs {{ $status_header_cards[$i]['class'] }} {{ $status == $status_header_cards[$i]['id'] ? 'bg-blue text-white' : '' }}">
                                    <button wire:click="checkStatus('{{ $status_header_cards[$i]['id'] }}')">
                                        {{ $status_header_cards[$i]['title'] }}
                                        <span class="ml-6 text-sm">{{ $status_header_cards[$i]['value'] }}</span>
                                    </button>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="overflow-auto rounded-lg">
                    <div class="bg-white rounded-lg shadow-md">
                        <x-table.table>
                            <x-slot name="thead">
                                <x-table.th name="No." />
                                <x-table.th name="Customer Number" />
                                <x-table.th name="{{ $account_type == 1 ? 'Customer Name' : 'Company Name' }}" />
                                @if ($account_type == 1)
                                    <x-table.th name="Primary Email Address" />
                                @else
                                    <x-table.th name="Company Email Address" />
                                    <x-table.th name="Industry" />
                                    <x-table.th name="Primary Contact Person" />
                                @endif
                                <x-table.th name="Primary Mobile No." />
                                {{-- <x-table.th name="Primary Telephone No." /> --}}
                                @if ($account_type == 2)
                                    <x-table.th name="TIN" />
                                @else
                                    <x-table.th name="Lifestage" />
                                    <x-table.th name="Category" />
                                @endif
                                <x-table.th name="Account Type" />
                                <x-table.th name="Contact Owner" />
                                <x-table.th name="Date Onboarded" />
                                <x-table.th name="Onboarding Channel" />
                                <x-table.th name="Action" />
                            </x-slot>
                            <x-slot name="tbody">
                                @foreach ($customer_informations as $i => $customer)
                                    <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                        <td class="p-3 whitespace-nowrap">
                                            {{ ($customer_informations->currentPage() - 1) * $customer_informations->links()->paginator->perPage() + $loop->iteration }}.
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <p class="underline text-blue"
                                                wire:click="action({'id': {{ $customer->id }}}, 'view')">
                                                {{ $customer->account_no }}
                                            </p>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ ucwords(strtolower($customer->fullname)) }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            @foreach ($customer->emails as $email)
                                                @if ($email->is_primary == 1 && $email->account_type != 3)
                                                    {{ $email->email }}
                                                @endif
                                            @endforeach
                                        </td>
                                        @if ($account_type == 2)
                                            <td class="p-3 whitespace-nowrap">
                                                {{ $customer->industry->name }}
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                @foreach ($customer->contactPersons as $contact_person)
                                                    @if ($contact_person->is_primary == 1 && $contact_person->account_type != 3)
                                                        {{ ucwords(strtolower($contact_person->first_name)) }}
                                                        {{ ucwords(strtolower($contact_person->middle_name)) }}
                                                        {{ ucwords(strtolower($contact_person->last_name)) }}
                                                    @endif
                                                @endforeach
                                            </td>
                                        @endif
                                        <td class="p-3 whitespace-nowrap">
                                            @foreach ($customer->mobileNumbers as $mobile_number)
                                                @if ($mobile_number->is_primary == 1 && $mobile_number->account_type != 3)
                                                    {{ $mobile_number->mobile }}
                                                @endif
                                            @endforeach
                                        </td>
                                        {{-- <td class="p-3 whitespace-nowrap">
                                            @foreach ($customer->telephoneNumbers as $telephone_number)
                                                @if ($telephone_number->is_primary == 1 && $telephone_number->account_type != 3)
                                                    {{ $telephone_number->telephone }}
                                                @endif
                                            @endforeach
                                        </td> --}}
                                        @if ($account_type == 2)
                                            <td class="p-3 whitespace-nowrap">
                                                {{ $customer->tin }}
                                            </td>
                                        @else
                                            <td class="p-3 whitespace-nowrap">
                                                {{ $customer->lifeStage->name ?? null }}
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                <span class="{{ $customer->status == 2 ? 'text-red' : 'text-blue' }}">
                                                    {{ $customer->statusRef->name ?? null }}
                                                </span>
                                            </td>
                                        @endif

                                        @if ($account_type == 2)
                                            <td class="p-3 whitespace-nowrap">
                                                {{ $customer->accountType->name ?? null }}
                                            </td>
                                        @else
                                            <td class="p-3 whitespace-nowrap">
                                                {{ $customer->account_type == 1 ? 'Non-Account' : '' }}
                                            </td>
                                        @endif

                                        <td class="p-3 whitespace-nowrap">
                                            {{ $customer->contactOwner->name ?? null }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ date('M. d, Y', strtotime($customer->created_at)) }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $customer->onboardingChannel->name ?? null }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <div class="flex gap-3">
                                                <svg wire:click="action({'id': {{ $customer->id }}}, 'view')"
                                                    class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                    data-prefix="far" data-icon="approve" role="img"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                    <path fill="currentColor"
                                                        d="M279.6 160.4C282.4 160.1 285.2 160 288 160C341 160 384 202.1 384 256C384 309 341 352 288 352C234.1 352 192 309 192 256C192 253.2 192.1 250.4 192.4 247.6C201.7 252.1 212.5 256 224 256C259.3 256 288 227.3 288 192C288 180.5 284.1 169.7 279.6 160.4zM480.6 112.6C527.4 156 558.7 207.1 573.5 243.7C576.8 251.6 576.8 260.4 573.5 268.3C558.7 304 527.4 355.1 480.6 399.4C433.5 443.2 368.8 480 288 480C207.2 480 142.5 443.2 95.42 399.4C48.62 355.1 17.34 304 2.461 268.3C-.8205 260.4-.8205 251.6 2.461 243.7C17.34 207.1 48.62 156 95.42 112.6C142.5 68.84 207.2 32 288 32C368.8 32 433.5 68.84 480.6 112.6V112.6zM288 112C208.5 112 144 176.5 144 256C144 335.5 208.5 400 288 400C367.5 400 432 335.5 432 256C432 176.5 367.5 112 288 112z" />
                                                </svg>
                                                @can('crm_customer_information_customer_data_mgmt_edit')
                                                    <svg wire:click="action({'id': {{ $customer->id }}}, 'edit')"
                                                        class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                        data-prefix="far" data-icon="edit" role="img"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                        <path fill="currentColor"
                                                            d="M471.6 21.7c-21.9-21.9-57.3-21.9-79.2 0L362.3 51.7l97.9 97.9 30.1-30.1c21.9-21.9 21.9-57.3 0-79.2L471.6 21.7zm-299.2 220c-6.1 6.1-10.8 13.6-13.5 21.9l-29.6 88.8c-2.9 8.6-.6 18.1 5.8 24.6s15.9 8.7 24.6 5.8l88.8-29.6c8.2-2.8 15.7-7.4 21.9-13.5L437.7 172.3 339.7 74.3 172.4 241.7zM96 64C43 64 0 107 0 160V416c0 53 43 96 96 96H352c53 0 96-43 96-96V320c0-17.7-14.3-32-32-32s-32 14.3-32 32v96c0 17.7-14.3 32-32 32H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h96c17.7 0 32-14.3 32-32s-14.3-32-32-32H96z">
                                                        </path>
                                                    </svg>
                                                @endcan
                                                @can('crm_customer_information_customer_data_mgmt_delete')
                                                    @if ($customer->status == 1)
                                                        <svg wire:click="action({'id': {{ $customer->id }}, 'status': 'Deactivate'},'update_status') }}"
                                                            class="w-5 h-5 text-red-500 cursor-pointer hover:text-red-800"
                                                            aria-hidden="true" focusable="false" data-prefix="far"
                                                            title="Deactivate" data-icon="print-alt"
                                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                            <path fill="currentColor"
                                                                d="M95.1 477.3c0 19.14 15.52 34.67 34.66 34.67h378.7c5.625 0 10.73-1.65 15.42-4.029L264.9 304.3C171.3 306.7 95.1 383.1 95.1 477.3zM630.8 469.1l-277.1-217.9c54.69-14.56 95.18-63.95 95.18-123.2C447.1 57.31 390.7 0 319.1 0C250.2 0 193.7 55.93 192.3 125.4l-153.4-120.3C34.41 1.672 29.19 0 24.03 0C16.91 0 9.845 3.156 5.127 9.187c-8.187 10.44-6.375 25.53 4.062 33.7L601.2 506.9c10.5 8.203 25.56 6.328 33.69-4.078C643.1 492.4 641.2 477.3 630.8 469.1z" />
                                                        </svg>
                                                    @else
                                                        <svg wire:click="action({'id': {{ $customer->id }}, 'status': 'Activate'},'update_status') }}"
                                                            class="w-5 h-5 cursor-pointer text-blue hover:text-green-800"
                                                            aria-hidden="true" focusable="false" data-prefix="far"
                                                            title="Activate" data-icon="print-alt"
                                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                            <path fill="currentColor"
                                                                d="M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0S96 57.3 96 128s57.3 128 128 128zm-45.7 48C79.8 304 0 383.8 0 482.3C0 498.7 13.3 512 29.7 512H418.3c16.4 0 29.7-13.3 29.7-29.7C448 383.8 368.2 304 269.7 304H178.3z" />
                                                        </svg>
                                                    @endif
                                                @endcan
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </x-slot>
                        </x-table.table>
                        <div class="px-1 pb-2">
                            {{ $customer_informations->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
