<div class="flex gap-4">
    <div class="w-1/5 pl-4 mt-3 text-lg font-medium border-r-2 border-gray-500 whitespace-nowrap">
        <div class="grid grid-cols-1 mt-6 text-blue">Contact</div>
        <div class="grid grid-cols-1 mt-6">My Tasks</div>
        <div class="grid grid-cols-1 mt-6">My Notes</div>
        <div class="grid grid-cols-1 mt-6">My Emails</div>
        <div class="grid grid-cols-1 mt-6">Quotation</div>
        <div class="grid grid-cols-1 mt-6">My Logged Meetings</div>
        <div class="grid grid-cols-1 mt-6 mb-6">My Scheduled Meetings</div>
    </div>
    <div class="w-4/5 px-6 font-medium">
        <div class="flex justify-between">
            <div>
                <div class="text-3xl text-blue whitespace-nowrap">{{ $fullname }}
                    @if ($account_type == 2)
                        <span
                            class="text-sm italic text-gray-500">{{ $is_mother_account == 1 ? '(Mother Account)' : '(Sub-Account)' }}</span>
                    @endif
                </div>
            </div>

            <div class="text-blue whitespace-nowrap" x-data="{ open: false }">
                <button @click="open = !open" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
                    <div class="flex items-start justify-between">
                        Add Activity
                        <svg class="w-3 h-3 mt-1 ml-2" aria-hidden="true" focusable="false" data-prefix="far"
                            data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 448 512">
                            <path fill="currentColor"
                                d="M137.4 374.6c12.5 12.5 32.8 12.5 45.3 0l128-128c9.2-9.2 11.9-22.9 6.9-34.9s-16.6-19.8-29.6-19.8L32 192c-12.9 0-24.6 7.8-29.6 19.8s-2.2 25.7 6.9 34.9l128 128z" />
                        </svg>
                    </div>
                </button>

                <div class="absolute" style="margin-left: -3.5rem">
                    <ul class="text-gray-600 bg-white rounded shadow" x-show="open" @click.away="open = false">
                        <li class="px-3 py-1 hover:bg-indigo-100 " x-data="{ open: false }"><a href="">Create New
                                Task</a>
                        </li>
                        <li class="px-3 py-1 hover:bg-indigo-100 " x-data="{ open: false }"><a
                                wire:click="action({},''}">Create a Note</a>
                        </li>
                        <li class="px-3 py-1 hover:bg-indigo-100 " x-data="{ open: false }"><a
                                wire:click="action({},''}">Create an Email</a>
                        </li>
                        <li class="px-3 py-1 hover:bg-indigo-100 " x-data="{ open: false }"><a
                                wire:click="action({},''}">Quotation</a>
                        </li>
                        <li class="px-3 py-1 hover:bg-indigo-100 " x-data="{ open: false }"><a
                                wire:click="action({},''}">Log a Meeting</a>
                        </li>
                        <li class="px-3 py-1 hover:bg-indigo-100 " x-data="{ open: false }"><a
                                wire:click="action({},''}">Schedule a Meeting</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="grid grid-cols-1 mb-2 text-lg whitespace-nowrap">{{ $customer_number }}</div>

        @if ($account_type == 2)
            @if ($sub_account != null)
                <div class="text-xl text-blue whitespace-nowrap">
                    <span class="underline">{{ $sub_account_name }}</span>
                    <span class="text-sm italic text-gray-500">(Sub-Account)</span>
                </div>
                <div class="grid grid-cols-1 text-lg whitespace-nowrap">{{ $sub_customer_number }}</div>
            @endif
        @endif

        <div class="grid grid-cols-2 gap-8 mt-6">
            <div class="grid grid-cols-1 space-y-2">
                <div class="grid grid-cols-2 whitespace-nowrap">
                    <div class="text-sm font-normal text-gray-500">
                        {{ $account_type == 1 ? 'Email Address :' : 'Company Email Address' }}
                    </div>
                    <div class="whitespace-wrap" style="margin-left: -20px">
                        @foreach ($emails as $i => $email)
                            <div class="flex gap-2">
                                {{ $email->email }}
                                @if ($email->is_primary == 1)
                                    <span>
                                        <svg class="w-4 h-4 mt-1 text-blue" aria-hidden="true" focusable="false"
                                            data-prefix="far" data-icon="approve" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                            <path fill="currentColor"
                                                d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" />
                                        </svg>
                                    </span>
                                @endif
                            </div>
                        @endforeach
                    </div>
                </div>
                @if ($account_type == 1)
                    <div class="grid grid-cols-2 whitespace-nowrap">
                        <div class="text-sm font-normal text-gray-500">
                            Telephone No. :
                        </div>
                        <div class="whitespace-wrap" style="margin-left: -20px">
                            @if (count($telephone_numbers) > 0)
                                @foreach ($telephone_numbers as $i => $telephone_number)
                                    <div class="flex gap-2">
                                        @if ($telephone_number->telephone != null || $telephone_number->telephone != '')
                                            {{ $telephone_number->telephone }}
                                            @if ($telephone_number->is_primary == 1)
                                                <span>
                                                    <svg class="w-4 h-4 mt-1 text-blue" aria-hidden="true"
                                                        focusable="false" data-prefix="far" data-icon="approve"
                                                        role="img" xmlns="http://www.w3.org/2000/svg"
                                                        viewBox="0 0 576 512">
                                                        <path fill="currentColor"
                                                            d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" />
                                                    </svg>
                                                </span>
                                            @endif
                                        @else
                                            <span class="">None</span>
                                        @endif
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                @endif
                <div class="grid grid-cols-2 whitespace-nowrap">
                    <div class="text-sm font-normal text-gray-500">
                        {{ $account_type == 1 ? 'Mobile No. :' : 'Company Mobile Number :' }}
                    </div>
                    <div class="whitespace-wrap" style="margin-left: -20px">
                        @foreach ($mobile_numbers as $i => $mobile_number)
                            <div class="flex gap-2">
                                {{ $mobile_number->mobile }}
                                @if ($mobile_number->is_primary == 1)
                                    <span>
                                        <svg class="w-4 h-4 mt-1 text-blue" aria-hidden="true" focusable="false"
                                            data-prefix="far" data-icon="approve" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                            <path fill="currentColor"
                                                d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" />
                                        </svg>
                                    </span>
                                @endif
                            </div>
                        @endforeach
                    </div>
                </div>
                @if ($account_type == 2)
                    <div class="grid grid-cols-2 whitespace-nowrap">
                        <div class="text-sm font-normal text-gray-500">
                            Industry
                        </div>
                        <div class="whitespace-wrap" style="margin-left: -20px">
                            {{ $industry }}
                        </div>
                    </div>
                    @php $contact_person_mobiles = []; @endphp
                    <div class="grid grid-cols-2 whitespace-nowrap">
                        <div class="text-sm font-normal text-gray-500">
                            Contact Person :
                        </div>
                        <div class="whitespace-wrap" style="margin-left: -20px">
                            @foreach ($contact_persons as $i => $contact_person)
                                <div class="flex gap-2">
                                    {{ $contact_person->first_name . ' ' . $contact_person->middle_name . ' ' . $contact_person->last_name }}
                                    @if ($contact_person->is_primary == 1)
                                        <span>
                                            <svg class="w-4 h-4 mt-1 text-blue" aria-hidden="true" focusable="false"
                                                data-prefix="far" data-icon="approve" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                <path fill="currentColor"
                                                    d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" />
                                            </svg>
                                        </span>
                                    @endif
                                </div>
                                @php $contact_person_mobiles = $contact_person->conPerMobileNumbers @endphp
                            @endforeach
                        </div>
                    </div>
                    <div class="grid grid-cols-2 whitespace-nowrap">
                        <div class="text-sm font-normal text-gray-500">
                            Contact Number :
                        </div>
                        <div class="whitespace-wrap" style="margin-left: -20px">
                            @if (count($contact_person_mobiles) > 0)
                                @foreach ($contact_person_mobiles as $i => $contact_person_mobile)
                                    <div class="flex gap-2">
                                        {{ $contact_person_mobile->mobile }}
                                        @if ($contact_person_mobile->is_primary == 1)
                                            <span>
                                                <svg class="w-4 h-4 mt-1 text-blue" aria-hidden="true"
                                                    focusable="false" data-prefix="far" data-icon="approve"
                                                    role="img" xmlns="http://www.w3.org/2000/svg"
                                                    viewBox="0 0 576 512">
                                                    <path fill="currentColor"
                                                        d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" />
                                                </svg>
                                            </span>
                                        @endif
                                    </div>
                                @endforeach
                            @else
                                <span class="text-sm italic text-gray-400">None</span>
                            @endif
                        </div>
                    </div>
                @endif

                @foreach ($addresses as $i => $address)
                    <div class="grid grid-cols-2">
                        <div class="text-sm font-normal text-gray-500">
                            Address {{ $i + 1 }} (<span
                                class="italic">{{ $address->address_label == 1 ? 'Home' : ($address->address_label == 2 ? 'Office' : 'Warehouse') }}</span>)
                            :
                        </div>
                        <div class="grid gap-2" style="margin-left: -20px">
                            <div>
                                {{ $address->address_line_1 }}
                                {{ $address->address_line_2 }}
                                {{ $address->barangayRef->name ?? null != null ? $address->barangayRef->name . ',' : '' }}
                                <div class="flex gap-2">
                                    {{ $address->cityRef->name ?? null != null ? $address->cityRef->name . ',' : '' }}
                                    {{ $address->stateRef->name ?? null != null ? $address->stateRef->name . ',' : '' }}
                                </div>
                                <div class="flex gap-2">
                                    {{ $address->cityRef->city_postal ?? null }} - PH
                                    @if ($address->is_primary == 1)
                                        <span>
                                            <svg class="w-4 h-4 mt-1 text-blue" aria-hidden="true" focusable="false"
                                                data-prefix="far" data-icon="approve" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                <path fill="currentColor"
                                                    d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" />
                                            </svg>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="grid grid-cols-2">
                <div class="grid grid-cols-1 space-y-2">
                    @if ($account_type == 2)
                        <div class="grid grid-cols-2 gap-24 whitespace-nowrap">
                            <div class="text-sm font-normal text-gray-500">TIN :</div>
                            <div class="">
                                {{ $tin }}
                            </div>
                        </div>
                    @endif
                    <div class="grid grid-cols-2 gap-24 whitespace-nowrap">
                        <div class="text-sm font-normal text-gray-500">Category :</div>
                        <div class="">
                            {{ $category }}
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-24 whitespace-nowrap">
                        <div class="text-sm font-normal text-gray-500">Life Stage :</div>
                        <div class="">
                            {{ $life_stage }}
                        </div>
                    </div>
                    
                    <div class="grid grid-cols-2 gap-24 whitespace-nowrap">
                        <div class="text-sm font-normal text-gray-500">Account Type :</div>
                        <div class="">
                            {{-- @if ($account_type_id == 1)
                                Non-Account
                            @else
                                {{ $account_type ?? 'Account' }}
                            @endif --}}
                            @if ($final_status == 1)
                                Account
                            @else
                                Non-Account
                            @endif
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-24 whitespace-nowrap">
                        <div class="text-sm font-normal text-gray-500">Account Applicable Rate :</div>
                        <div class="">
                            {{ $rate_name ?? "" }}
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-24 whitespace-nowrap">
                        <div class="text-sm font-normal text-gray-500">Contact Owner :</div>
                        <div class="">{{ $contact_owner }}</div>
                    </div>
                    <div class="grid grid-cols-2 gap-24 whitespace-nowrap">
                        <div class="text-sm font-normal text-gray-500">Onboarding Channel :</div>
                        <div class="">
                            CRM
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-24 whitespace-nowrap">
                        <div class="text-sm font-normal text-gray-500">Marketing Channel :</div>
                        <div class="">
                            {{ $marketing_channel }}
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-24 whitespace-nowrap">
                        <div class="text-sm font-normal text-gray-500">Date Onboarded :</div>
                        <div class="">
                            {{ $date_onboarded }}
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-24 whitespace-nowrap">
                        <div class="text-sm font-normal text-gray-500">Note :</div>
                        <div class="">
                            {{ $note }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
