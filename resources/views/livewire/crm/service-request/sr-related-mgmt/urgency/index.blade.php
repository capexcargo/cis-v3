<x-form wire:init="load" x-data="{
    search_form: false,
    {{-- create_modal: '{{ $create_modal }}',
    edit_modal: '{{ $edit_modal }}',
    delete_modal: '{{ $delete_modal }}', --}}

}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>

    {{-- <x-slot name="modals">

        @can('crm_service_request_sr_related_mgmt_resolution_add')
            <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                <x-slot name="title">Add Resolution</x-slot>
                <x-slot name="body">
                    @livewire('crm.service-request.sr-related-mgmt.resolution.create')
                </x-slot>
            </x-modal>
        @endcan

        @can('crm_service_request_sr_related_mgmt_resolution_edit')
            @if ($reso_id && $edit_modal)
                <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                    <x-slot name="title">Edit Resolution</x-slot>
                    <x-slot name="body">
                        @livewire('crm.service-request.sr-related-mgmt.resolution.edit', ['id' => $reso_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan

        @can('crm_service_request_sr_related_mgmt_resolution_delete')
            <x-modal id="delete_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
                <x-slot name="body">
                    <h2 class="mb-3 text-lg text-center text-gray-900">
                        {{ $confirmation_message }}
                    </h2>
                    <div class="flex justify-center space-x-3">
                        <button type="button" wire:click="$set('delete_modal', false)"
                            class="px-5 py-1 mt-4 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">No</button>
                        <button type="button" wire:click="confirm"
                            class="flex-none px-5 py-1 mt-4 text-sm text-white rounded-lg bg-blue">
                            Yes</button>
                    </div>
                </x-slot>
            </x-modal>
        @endcan
    </x-slot> --}}

    <x-slot name="header_title">SR Related Management</x-slot>
    <x-slot name="header_button">
    </x-slot>
    <x-slot name="body">

        <div class="grid grid-cols-6 gap-4">
            <button wire:click="redirectTo({}, 'redirectToSRTypeMgmt')"
                class="w-auto p-4 text-gray-700 bg-white border border-solid rounded-md border-blue hover:bg-blue-100">
                <div class="items-center">
                    <span class="inline-block text-xl font-medium text-left">SR TYPE</span>
                </div>
            </button>
            <button wire:click="redirectTo({}, 'redirectToResponse')"
                class="w-auto py-4 text-gray-700 bg-white rounded-md" style="border: 4px solid darkblue;">
                <div class="items-center">
                    <span class="inline-block text-xl font-medium text-left">MILESTONE</span>
                </div>
            </button>
            <button wire:click="redirectTo({}, 'redirectToHierarchy')"
                class="w-auto p-4 text-gray-700 bg-white border border-solid rounded-md border-blue hover:bg-blue-100">
                <div class="items-center">
                    <span class="inline-block text-xl font-medium text-left">HIERARCHY</span>
                </div>
            </button>
            <button wire:click="redirectTo({}, 'redirectToChannel')"
                class="w-auto p-4 text-gray-700 bg-white border border-solid rounded-md border-blue hover:bg-blue-100">
                <div class="items-center">
                    <span class="inline-block text-xl font-medium text-left">CHANNEL/<br>SR SOURCE</span>
                </div>
            </button>
            <button wire:click="redirectTo({}, 'redirectToService')"
                class="w-auto p-4 text-gray-700 bg-white border border-solid rounded-md border-blue hover:bg-blue-100">
                <div class="items-center">
                    <span class="inline-block text-xl font-medium text-left">SERVICE<br>REQUIREMENTS</span>
                </div>
            </button>
        </div>


        <div class="py-12">
            <div class="flex items-center justify-start">
                <div wire:click="redirectTo({}, 'redirectToResponse')"
                    class="flex justify-between px-2 py-1 text-sm text-left bg-white border border-gray-400 cursor-pointer ">
                    <span>Response</span>
                    <span class="ml-6">{{ $m_responses->count() }}</span>
                </div>
                <div wire:click="redirectTo({}, 'redirectToResolution')"
                    class="flex justify-between px-2 py-1 text-sm text-left bg-white border border-gray-400 cursor-pointer ">
                    <span class="">Resolution</span>
                    <span class="ml-6">{{ $m_resolutions->count() }}</span>

                </div>
                <div wire:click="redirectTo({}, 'redirectToUrgency')"
                    class="flex items-center justify-between px-2 py-1 text-sm text-white border border-gray-400 cursor-pointer bg-blue">
                    <span class="">Urgency and Impact</span>
                    <span class="ml-4">{{ $counturgency }}</span>
                    {{-- @foreach ($urgency_impacts as $i => $urgency_impact)

                    <span class="ml-4">{{ $urgency_impacts->count() }}</span>
                    @endforeach --}}
                    {{-- <span class="ml-10"></span> --}}
                </div>

            </div>
            <form wire:submit.prevent="" autocomplete="off">

                <div class="bg-white rounded-lg shadow-md">
                    <x-table.table class="overflow-hidden">
                        <x-slot name="thead">
                            <tr>
                                <x-table.th rowspan="0" colspan="1" name="" class="pl-4 border "
                                    style="padding-left:10%; background-color: #e6e6e6;" />

                                <x-table.th colspan="1" name="" class="pl-4 "
                                    style="padding-left:10%;height:1px" />


                                <x-table.th colspan="4" name="IMPACT" class="pl-6 bg-gray-200 "
                                    style="padding-left:10%;height:1px; padding-left:34%; font-size: 20px; background-color:#e6e6e6; color: grey; " />


                            </tr>
                            <x-table.th name="Severity" style="padding-left:5.5%;" />
                            <x-table.th name="High" style="padding-left:13%" />
                            <x-table.th name="Medium" style="padding-left:7%" />
                            <x-table.th name="Low" style="padding-left:3%" />
                            <x-table.th name="" />
                        </x-slot>
                        <x-slot name="tbody">
                            <tr class="bg-gray-700 " style="padding-bottom: ">
                                <x-table.th rowspan="0" name="URGENCY" class="w-20 bg-gray-700 border-none"
                                    style="writing-mode: vertical-rl; text-orientation:mixed; transform: rotate(-180deg); width:4%; font-size: 20px; text-combine-upright: ;
                                background-color:#e6e6e6; padding-top:11%; padding-bottom: ; color: grey;" />
                            </tr>
                            @foreach ($urgency_impacts as $i => $urgency_impact)
                                @if (!$urgency_impact['is_deleted'])
                                    {{-- @if ($i >= 0) --}}


                                    <tr class="border-0 cursor-pointer">


                                        <td class=" whitespace-nowrap md:w-1/5">
                                            <div class="w-32" style="margin-left: 20%">
                                                <x-select class="rounded-sm"
                                                    name="urgency_impacts.{{ $i }}.severity"
                                                    wire:model.defer='urgency_impacts.{{ $i }}.severity'>
                                                    <option value="">Select</option>
                                                    @foreach ($severity_references as $severity_ref)
                                                        <option value="{{ $severity_ref->id }}">
                                                            {{ $severity_ref->severity }}
                                                        </option>
                                                    @endforeach
                                                </x-select>
                                                <x-input-error for="urgency_impacts.{{ $i }}.severity" />
                                            </div>
                                        </td>

                                        <td class="p-3 whitespace-nowrap" style="padding-left:12%;">
                                            <div class="w-12">
                                                <x-select class="rounded-sm"
                                                    name="urgency_impacts.{{ $i }}.high_priority_level"
                                                    wire:model.defer='urgency_impacts.{{ $i }}.high_priority_level'>
                                                    <option value="">-</option>
                                                    @foreach ($high_references as $high_ref)
                                                        <option value="{{ $high_ref->id }}">
                                                            {{ $high_ref->priority_level }}
                                                        </option>
                                                    @endforeach
                                                </x-select>
                                                <x-input-error
                                                    for="urgency_impacts.{{ $i }}.high_priority_level" />
                                            </div>
                                        </td>
                                        <td class="p-3 whitespace-nowrap" style="padding-left:7%;">
                                            <div class="w-12">
                                                <x-select class="rounded-sm"
                                                    name="urgency_impacts.{{ $i }}.medium_priority_level"
                                                    wire:model.defer='urgency_impacts.{{ $i }}.medium_priority_level'>
                                                    <option value="">-</option>
                                                    @foreach ($medium_references as $medium_ref)
                                                        <option value="{{ $medium_ref->id }}">
                                                            {{ $medium_ref->priority_level }}
                                                        </option>
                                                    @endforeach
                                                </x-select>
                                            </div>
                                            <x-input-error
                                                for="urgency_impacts.{{ $i }}.medium_priority_level" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap md:w-1/5" style="padding-left:2%;">
                                            <div class="w-12">
                                                <x-select class="rounded-sm"
                                                    name="urgency_impacts.{{ $i }}.low_priority_level"
                                                    wire:model.defer='urgency_impacts.{{ $i }}.low_priority_level'>
                                                    <option value="">-</option>
                                                    @foreach ($low_references as $low_ref)
                                                        <option value="{{ $low_ref->id }}">
                                                            {{ $low_ref->priority_level }}
                                                        </option>
                                                    @endforeach
                                                </x-select>
                                            </div>
                                            <x-input-error
                                                for="urgency_impacts.{{ $i }}.low_priority_level" />
                                        </td>

                                        <td class="p-3 whitespace-nowrap" style="padding-left;5%">
                                            {{-- @if (count($urgency_impacts) > 1) --}}
                                            @if (count(collect($urgency_impacts)->where('is_deleted', false)) > 1)
                                                <button wire:click="removeUrgency({{ $i }})"
                                                    class="p-2 px-6 mr-3 text-sm text-red-600 bg-white border border-red-600 rounded-sm">
                                                    <div class="flex items-start justify-between">
                                                        Deactivate
                                                    </div>
                                                </button>
                                            @endif
                                        </td>
                                    </tr>
                                @endif
                            @endforeach


                            {{-- @foreach ($m_resolutions as $i => $m_resolution) --}}
                            <tr class="border-0 cursor-pointer">
                                <td class="p-3 whitespace-nowrap md:w-24">
                                    <p class="w-24">
                                        {{-- {{ $i + 1 }} --}}
                                    </p>
                                </td>
                                <td class="p-3 whitespace-nowrap sm:w-1/3">
                                    {{-- {{ $m_resolution->priority_level }} --}}
                                </td>
                                <td class="p-3 whitespace-nowrap sm:w-1/3">
                                    {{-- {{ $m_resolution->severity }} --}}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{-- {{ $m_resolution->target_response_time}} {{ ($m_resolution->target_response_time > 1 ?  "Hours" : "Hour") }}  --}}

                                </td>

                                <td class="p-3 whitespace-nowrap">

                                    <button wire:click="addUrgency()"
                                        class="p-2 px-12 mr-3 text-sm text-white rounded-md bg-blue">
                                        <div class="flex items-start justify-between">
                                            Add
                                        </div>
                                    </button>
                                </td>

                            </tr>

                            <tr class="h-12 border-0 cursor-pointer">
                                <td class="p-3 whitespace-nowrap md:w-24">
                                    <p class="w-24">
                                        {{-- {{ $i + 1 }} --}}
                                    </p>
                                </td>
                                <td class="p-3 whitespace-nowrap sm:w-1/3">
                                    {{-- {{ $m_resolution->priority_level }} --}}
                                </td>
                                <td class="p-3 whitespace-nowrap sm:w-1/3">
                                    {{-- {{ $m_resolution->severity }} --}}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{-- {{ $m_resolution->target_response_time}} {{ ($m_resolution->target_response_time > 1 ?  "Hours" : "Hour") }}  --}}

                                </td>

                                <td class="p-3 mr-3 text-sm px-14 whitespace-nowrap">


                                </td>

                            </tr>

                            <tr class="relative h-12 border-0 cursor-pointer">
                                <td class=" whitespace-nowrap">
                                        <button wire:click="submit"
                                            class="absolute w-full p-3 px-12 -mt-5 -ml-6 text-sm text-white bg-blue">
                                            <div class="flex items-start justify-center">
                                                Save
                                            </div>
                                        </button>
                                </td>
                            </tr>


                        </x-slot>
                    </x-table.table>
                    {{-- <div class="grid grid-cols-1 -mt-4">
                        <button wire:click="submit" class="w-full p-3 px-12 mr-3 text-sm text-white rounded-md bg-blue">
                            <div class="flex items-start justify-center">
                                Save
                            </div>
                        </button>
                    </div> --}}
                    {{-- <div class="px-1 pb-2">
                        {{ $m_resolutions->links() }}
                    </div> --}}

                </div>


                {{-- <div class="grid grid-cols-1 -mt-4">
                    <button wire:click="submit" class="w-full p-3 px-12 mr-3 text-sm text-white rounded-md bg-blue">
                        <div class="flex items-start justify-center">
                            Save
                        </div>
                    </button>
                </div> --}}
            </form>

        </div>


    </x-slot>
</x-form>
