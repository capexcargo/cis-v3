<div 
x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
}">
    <x-loading></x-loading>

    <x-modal id="confirmation_modal" size="w-auto">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="text-xl text-center">
                Are you sure you want to submit this new SR Subcategory?
            </h2>

            <div class="flex justify-center space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                    No
                </button>
                <button type="button" wire:click="submit"
                    class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                    Yes
                </button>
            </div>
        </x-slot>
    </x-modal>
    
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="mt-4 space-y-3">
            <div class="grid grid-cols-12 gap-8">
                <div class="col-span-7">
                    <x-label for="srsub" value="SR Subcategory" :required="true" />
                    <x-input type="text" name="srsub" wire:model.defer='srsub'></x-input>
                    <x-input-error for="srsub" />
                </div>
                <div class="col-span-5">
                    <div wire:init="categoryReferences">
                        <x-label for="category" value="Tag to" :required="true" />
                        <x-select name="category" wire:model='category'>
                            <option value="">Select</option>
                            @foreach ($category_references as $category_ref)
                                <option value="{{ $category_ref->id }}">
                                    {{ $category_ref->name }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="category" />
                    </div>
                </div>
            </div> 
            
        </div>
        <div class="flex justify-end gap-3 pt-8 space-x-3">
            <button type="button" wire:click="closecreatemodal"
                class="px-12 py-2 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-md  hover:bg-gray-200">
                Cancel
            </button>
            <button type="submit"
                class="px-12 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg hover:bg-[#00246B]">
                Submit
            </button>
        </div>
    </form>
</div>
