<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
}">
    <x-loading></x-loading>

    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="mb-3 text-xl font-bold text-left text-blue">
                Are you sure you want to submit this Resolution?
            </h2>

            <div class="flex justify-end mt-6 space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'edit')"
                    class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                    Cancel
                </button>
                <button type="button" wire:click="submit"
                    class="px-12 py-2 text-xs flex-none bg-[#003399] text-white rounded-lg">
                    Submit
                </button>
            </div>
        </x-slot>
    </x-modal>

    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="mt-4 space-y-3">
            <div class="grid grid-cols-12 gap-8">
                <div class="col-span-7">
                    <x-label for="priority_level" value="Priority Level" :required="true" />
                    <x-input type="number" name="priority_level" wire:model.defer='priority_level'></x-input>
                    <x-input-error for="priority_level" />
                </div>
                <div class="col-span-5">
                    <x-label for="severity" value="Severity" :required="true" />
                    <x-input type="text" name="severity" wire:model.defer='severity'></x-input>
                    <x-input-error for="severity" />
                </div>

                <div class="col-span-7">
                    <x-label for="target_response_time" value="Target Response Time" :required="true" />
                    <x-input type="number" name="target_response_time" wire:model.defer='target_response_time'>
                    </x-input>
                    <x-input-error for="target_response_time" />
                </div>
                <div class="col-span-5">
                    {{-- <x-label for="target_response_time" value="Target Response Time" :required="true" />
                    <x-input type="number" name="target_response_time" wire:model.defer='target_response_time'>
                    </x-input>
                    <x-input-error for="target_response_time" /> --}}
                </div>
            </div>
        </div>
        <div class="flex justify-end gap-3 pt-8 space-x-3">
            <button type="button" wire:click="$emit('close_modal', 'edit')"
                class="px-12 py-2 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-md  hover:bg-gray-200">Cancel</button>
            <button type="submit"
                class="px-12 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg hover:bg-blue">
                Save</button>
        </div>
    </form>
</div>
