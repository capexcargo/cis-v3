<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
}">
    <x-loading></x-loading>

    <x-modal id="confirmation_modal" size="w-auto">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="text-xl text-center">
                Are you sure you want to submit new Hierarchy?
            </h2>

            <div class="flex justify-center space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                    No
                </button>
                <button type="button" wire:click="submit"
                    class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                    Yes
                </button>
            </div>
        </x-slot>
    </x-modal>

    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="grid grid-cols-10 mt-2" style="margin-right: -7%">
            <div class="col-span-9">
                <div class="grid grid-cols-10 gap-3">
                    <div class="col-span-5">
                        <x-label for="email" value="Email Address" :required="true" />
                        <x-input type="text" name="email" wire:model.defer='email'></x-input>
                        <x-input-error for="email" />
                    </div>
                    <?php $i = 0; ?>

                    @foreach ($positions as $a => $position)
                        @if ($i != 0)
                            <div class="col-span-5">
                            </div>
                        @endif
                        <div class="col-span-4 ml-6">
                            {{-- @if ($i == 0) --}}
                            @if ($i == 0)
                                <x-label for="tagged_position_id" value="Tag To" :required="true" />
                            @else
                                <br>
                            @endif

                            {{-- @endif --}}
                            <x-select name="positions.{{ $a }}.tagged_position_id"
                                wire:model.defer='positions.{{ $a }}.tagged_position_id'>
                                <option value="">Select</option>
                                <option value="1">Customer Relations Executive</option>
                                <option value="2">Customer Care Expert</option>
                                <option value="3">Customer Care Associate</option>
                                <option value="4">Second</option>
                            </x-select>
                            <x-input-error for="positions.{{ $a }}.tagged_position_id" />
                        </div>
                        <div class="col-span-1">
                            <div class="grid grid-cols-10 mt-6">
                                <div class="col-span-5">
                                    @if (count($positions) > 1)
                                        <x-label for="tagged_position_id" value="" />
                                        <svg wire:click="removePosition({'a': {{ $a }}})"
                                            class="w-8 h-7 text-red" aria-hidden="true" focusable="false"
                                            data-prefix="fas" data-icon="circle-minus" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                            <path fill="currentColor"
                                                d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM184 232H328c13.3 0 24 10.7 24 24s-10.7 24-24 24H184c-13.3 0-24-10.7-24-24s10.7-24 24-24z">
                                            </path>
                                        </svg>
                                    @endif
                                    @if (count($positions) == 1)
                                        <x-label for="tagged_position_id" value="" />
                                        <svg wire:click="addPosition()" class="w-8 h-7 text-blue" aria-hidden="true"
                                            focusable="false" data-prefix="fas" data-icon="trash-alt" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                            <path fill="currentColor"
                                                d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z">
                                            </path>
                                        </svg>
                                    @endif

                                </div>
                                <div class="col-span-5 ml-4">
                                    @if ($i != 0)
                                        @if (count($positions) == $i + 1)
                                            <x-label for="tagged_position_id" value="" />
                                            <svg wire:click="addPosition()" class="w-8 h-7 text-blue" aria-hidden="true"
                                                focusable="false" data-prefix="fas" data-icon="trash-alt" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                <path fill="currentColor"
                                                    d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z">
                                                </path>
                                            </svg>
                                        @endif
                                    @endif

                                </div>

                            </div>
                        </div>
                        <?php $i++; ?>
                    @endforeach
                </div>
            </div>


        </div>

        <div class="flex justify-end mt-12 space-x-3" style="margin-right:13%;">
            <button type="button" wire:click="closecreatemodal"
            class="px-12 py-2 mr-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-md  hover:bg-gray-200">
                Cancel
            </button>
            <button type="submit" class=" ml-4 px-12 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg hover:bg-[#00246B]">
                Submit
            </button>
        </div>
    </form>
</div>
