<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
}">
    <x-loading></x-loading>

    <x-modal id="confirmation_modal" size="w-auto">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="text-xl text-center">
                Are you sure you want to submit this new Sales Coach?
            </h2>

            <div class="flex justify-center space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                    No
                </button>
                <button type="button" wire:click="submit"
                    class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                    Yes
                </button>
            </div>
        </x-slot>
    </x-modal>

    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">
        <div class="w-auto mt-5 space-y-3">
            <div class="grid h-auto grid-cols-12 gap-3 px-6">
                <div class="col-span-11">
                    <x-label for="title" value="Title" :required="true" />
                    <x-input type="text" name="title" wire:model.defer='title'></x-input>
                    <x-input-error for="title" />
                </div>
                <div class="col-span-11">
                    <x-label for="description" value="Description" :required="true" />
                    <x-input type="text" name="description" wire:model.defer='description'></x-input>
                    <x-input-error for="description" />
                </div>

                <?php $i = 0; ?>
                @foreach ($pros as $a => $pro)
                    <div class="col-span-11">
                        <x-label for="process" value="Process" :required="true" />
                        <x-input type="text" name="pros.{{ $a }}.{{ $a }}.process"
                            wire:model.defer='pros.{{ $a }}.{{ $a }}.process'></x-input>
                        <x-input-error for="pros.{{ $a }}.{{ $a }}.process" />
                    </div>
                    <div class="col-span-1">
                        <div class="grid grid-cols-2">
                            @if (count($pros) > 1)
                                <div class="col-span-1 mt-6">
                                    <x-label for="process" value="" />
                                    <svg wire:click="removeProcessCreate({'a': {{ $a }}})"
                                        class="h-6 cursor-pointer w-7 text-red" aria-hidden="true" focusable="false" data-prefix="fas"
                                        data-icon="circle-minus" role="img" xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 448 512">
                                        <path fill="currentColor"
                                            d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM184 232H328c13.3 0 24 10.7 24 24s-10.7 24-24 24H184c-13.3 0-24-10.7-24-24s10.7-24 24-24z">
                                        </path>
                                    </svg>
                                </div>
                            @endif


                            @if (count($pros) == 1)
                                <div class="col-span-1 pr-2 mt-6">
                                    <x-label for="process" value="" />
                                    <svg wire:click="addProcess({{ $a + 1 }})" class="h-6 cursor-pointer w-7 text-blue"
                                        aria-hidden="true" focusable="false" data-prefix="fas" data-icon="trash-alt"
                                        role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                        <path fill="currentColor"
                                            d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z">
                                        </path>
                                    </svg>
                                </div>
                            @endif

                            @if ($i != 0)
                                @if (count($pros) == $i + 1)
                                    <div class="col-span-1 pl-2 pr-2 mt-6">
                                        <x-label for="process" value="" />
                                        <svg wire:click="addProcess({{ $a + 1 }})" class="h-6 ml-3 cursor-pointer w-7 text-blue"
                                            aria-hidden="true" focusable="false" data-prefix="fas" data-icon="trash-alt"
                                            role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                            <path fill="currentColor"
                                                d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z">
                                            </path>
                                        </svg>
                                    </div>
                                @endif
                            @endif

                        </div>
                    </div>

                    <?php $b = 0; ?>
                    @if (isset($subpros[$a]))
                        @foreach ($subpros[$a] as $c => $subpro)
                            <div class="col-span-11">
                                <x-label for="sub_process" value="Sub Process" :required="true" />
                                <x-input type="text"
                                    name="subpros.{{ $a }}.{{ $c }}.sub_process"
                                    wire:model.defer='subpros.{{ $a }}.{{ $c }}.sub_process'>
                                </x-input>
                                <x-input-error for="subpros.{{ $a }}.{{ $c }}.sub_process" />
                            </div>

                            <div class="col-span-1 mt-6">
                                <x-label for="process" value="" />
                                <svg wire:click="removeSubProcess({'a': {{ $a }},'b' : {{ $c }}})"
                                    class="h-6 cursor-pointer w-7 text-red" aria-hidden="true" focusable="false" data-prefix="fas"
                                    data-icon="circle-minus" role="img" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 448 512">
                                    <path fill="currentColor"
                                        d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM184 232H328c13.3 0 24 10.7 24 24s-10.7 24-24 24H184c-13.3 0-24-10.7-24-24s10.7-24 24-24z">
                                    </path>
                                </svg>
                            </div>
                        @endforeach
                    @endif
                    <div class="col-span-12">
                        <button type="button" wire:click="addSubProcess({{ $a }})"
                            class="px-4 py-1 text-sm flex-none cursor-pointer bg-[#003399] text-white rounded-sm">+
                            Sub-Process
                        </button>
                    </div>
                    <?php $b++; ?>


                    <?php $i++; ?>
                @endforeach

            </div>
            <div class="flex justify-end gap-3 pt-6 space-x-3" style="padding-right:11%;">
                <button type="button" wire:click="closecreatemodal"
                    class="px-12 py-2 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-md  hover:bg-gray-200">
                    Cancel
                </button>
                <button type="submit"
                    class="px-12 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg hover:bg-[#00246B]">
                    Submit
                </button>
                {{-- <div class="text-white">.____</div> --}}
            </div>
        </div>
    </form>
</div>
