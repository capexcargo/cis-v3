<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
}">
    <x-loading></x-loading>

    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="mb-3 text-xl font-bold text-left text-blue">
                Are you sure you want to submit Knowledge?
            </h2>

            <div class="flex justify-end mt-6 space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                    Cancel
                </button>
                <button type="button" wire:click="submit"
                    class="px-12 py-2 text-xs flex-none bg-[#003399] text-white rounded-lg">
                    Submit
                </button>
            </div>
        </x-slot>
    </x-modal>

    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="mt-5 space-y-3">

            <div class="grid grid-cols-1 gap-3">
                <div>
                    <x-label for="faq_concern" value="FAQ/Concerns" :required="true" />
                    <x-input type="text" name="faq_concern" wire:model.defer='faq_concern'></x-input>
                    <x-input-error for="faq_concern" />
                </div>

                <div class="grid grid-cols-2 gap-3">
                    <x-label class="-mb-3" for="response_area" value="Response" :required="true" />

                    <?php $i = 0; ?>
                    @foreach ($positions as $a => $position)
                        @if (!$position['is_deleted'])
                            <div class="col-span-2">
                                {{-- <x-label for="response_area"
                                    value="Response[{{ $min }}][{{ $a }}][{{ $i }}][{{ $nearest }}]"
                                    :required="true" /> --}}
                                <x-textarea style="overflow: hidden" rows="3" cols="25" type="text"
                                    id="autoresizing2" name="positions.{{ $a }}.response_area"
                                    wire:model.defer='positions.{{ $a }}.response_area'>
                                </x-textarea>
                                <x-input-error for="positions.{{ $a }}.response_area" />
                            </div>
                            <div class="col-span-2">
                                <div class="grid w-20 grid-cols-2">
                                    @if ($nearest > $min)
                                        <div class="col-span-1">
                                            <svg wire:click="removePosition({'a': {{ $a }}})"
                                                class="h-6 w-7 text-red" aria-hidden="true" focusable="false"
                                                data-prefix="fas" data-icon="circle-minus" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                <path fill="currentColor"
                                                    d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM184 232H328c13.3 0 24 10.7 24 24s-10.7 24-24 24H184c-13.3 0-24-10.7-24-24s10.7-24 24-24z">
                                                </path>
                                            </svg>
                                        </div>
                                    @endif

                                    @if ($nearest == $a)
                                        <div class="col-span-1">
                                            <svg wire:click="addPosition()" class="h-6 w-7 text-blue" aria-hidden="true"
                                                focusable="false" data-prefix="fas" data-icon="trash-alt" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                <path fill="currentColor"
                                                    d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z">
                                                </path>
                                            </svg>
                                        </div>
                                    @endif

                                </div>
                            </div>
                        @endif
                        <?php $i++; ?>
                    @endforeach



                </div>

            </div>


        </div>
        <div class="flex justify-end gap-3 pt-4 space-x-3">
            <button type="button" wire:click="$emit('close_modal', 'edit')"
                class="px-12 py-2 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-md  hover:bg-gray-200">Cancel</button>
            <button type="submit"
                class="px-12 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg hover:bg-blue">
                Save</button>
        </div>
    </form>
</div>

<script type="text/javascript">
    textarea = document.querySelector("#autoresizing2");
    textarea.addEventListener('input', autoResize, false);

    function autoResize() {
        this.style.height = 'auto';
        this.style.height = this.scrollHeight + 'px';
    }
</script>
