<x-form wire:init="load" x-data="{
    search_form: false,
    create_modal: '{{ $create_modal }}',
    edit_modal: '{{ $edit_modal }}',
    reactivate_modal: '{{ $reactivate_modal }}',
    deactivate_modal: '{{ $deactivate_modal }}',
    {{-- delete_modal: '{{ $delete_modal }}', --}}

}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>

    <x-slot name="modals">

        @can('crm_service_request_knowledge_add')
            <x-modal id="create_modal" size="w-1/3">
                <x-slot name="title">Add Script</x-slot>
                <x-slot name="body">
                    @livewire('crm.service-request.knowledge.create')
                </x-slot>
            </x-modal>
        @endcan

        @can('crm_service_request_knowledge_edit')
            @if ($res_id && $edit_modal)
                <x-modal id="edit_modal" size="w-1/3">
                    <x-slot name="title">Edit Script</x-slot>
                    <x-slot name="body">
                        @livewire('crm.service-request.knowledge.edit', ['id' => $res_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan


        @can('crm_service_request_knowledge_deactivate')
            <x-modal id="reactivate_modal" size="w-auto">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center">
                        <h2 class="text-xl text-center text-gray-900 ">
                            Are you sure you want to reactivate this script?</h2>
                        <div class="flex justify-center space-x-3">
                            <button wire:click="$set('reactivate_modal', false)"
                                class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">NO</button>
                            <button wire:click="updateStatus('{{ $res_id }}', 1)"
                                class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                                YES</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
            <x-modal id="deactivate_modal" size="w-auto">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center">
                        <h2 class="text-xl text-center text-gray-900 ">
                            Are you sure you want to deactivate this script?</h2>
                        <div class="flex justify-center space-x-3">
                            <button wire:click="$set('deactivate_modal', false)"
                                class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">NO</button>
                            <button wire:click="updateStatus('{{ $res_id }}', 2)"
                                class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                                YES</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endcan
    </x-slot>

    <x-slot name="header_title">Knowledge</x-slot>
    <x-slot name="header_button">
        @can('crm_service_request_knowledge_add')
            <button wire:click="action({}, 'add')" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
                <div class="flex items-start justify-between">
                    <svg class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                        data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                    Add Knowledge
                </div>
            </button>
        @endcan
    </x-slot>
    <x-slot name="body">

        <div class="grid grid-cols-5 gap-4">
            <button wire:click="redirectTo({}, 'redirectToKnowledge')"
                class="w-auto py-6 text-gray-700 bg-white rounded-md" style="border: 4px solid darkblue;">
                <div class="items-center">
                    <span class="inline-block text-xl font-medium text-left">KNOWLEDGE</span>
                </div>
            </button>
            <button wire:click="redirectTo({}, 'redirectToSalesCoach')"
                class="w-auto p-6 text-gray-700 bg-white border border-solid rounded-md border-blue hover:bg-blue-100">
                <div class="items-center ">
                    <span class="inline-block text-xl font-medium text-left">SALES COACH</span>
                </div>
            </button>
        </div>

        <div class="py-4">
            <div class="flex items-center justify-start">
                <ul class="flex mt-6">
                    @foreach ($status_header_cards as $i => $status_card)
                        <li class="px-3 py-1 text-sm {{ $status_header_cards[$i]['class'] }}"
                            :class="$wire.{{ $status_header_cards[$i]['action'] }} ==
                                '{{ $status_header_cards[$i]['id'] }}' ?
                                'bg-blue text-white' : ''">
                            <button
                                wire:click="$set('{{ $status_header_cards[$i]['action'] }}', {{ $status_header_cards[$i]['id'] }})">
                                {{ $status_header_cards[$i]['title'] }}
                                <span class="ml-6 text-sm">{{ $status_header_cards[$i]['value'] }}</span>
                            </button>
                        </li>
                    @endforeach
                </ul>
            </div>



            <div class="bg-white rounded-lg shadow-md">
                <x-table.table class="overflow-hidden">
                    <x-slot name="thead">
                        <x-table.th name="No." style="padding-left:2%;" />
                        <x-table.th name="FAQ/Concerns" style="padding-left:;" />
                        <x-table.th name="Response" style="padding-left:;" />
                        <x-table.th name="Action" style="padding-left:2%;" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($knowledge_s as $i => $knowledge)
                            <tr class="font-semibold border-0 cursor-pointer">
                                <div id="tables">
                                    <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:2%;">
                                        <p class="w-1/6">
                                            {{ ($knowledge_s->currentPage() - 1) * $knowledge_s->links()->paginator->perPage() + $loop->iteration }}.
                                        </p>
                                    </td>
                                    <td class="w-1/5 p-3 text-base font-normal" style="padding-left:;">
                                        {{ $knowledge->faq_concern }}
                                    </td>
                                    <td id="concerns{{ $i }}" class="w-3/5 p-3 whitespace-pre-line"
                                        style="padding-left:;">
                                        <table class="capitalize text-base font-normal">
                                            @foreach ($knowledge->respHasMany as $c => $knowledge_res)
                                                <tr>
                                                    <td class="whitespace-pre-line">
                                                        {{ $knowledge_res->response_area }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                            <tr>
                                                <td> <br></td>
                                            </tr>

                                        </table>
                                    </td>
                                </div>

                                <td class="w-1/5 p-3 whitespace-nowrap" style="padding-left;">
                                    <div class="flex space-x-3">
                                        @if ($knowledge->status == 1)
                                            <button title="Copy to Clipboard"
                                                onclick="copyToClipboard( {{ $i }} )"><svg
                                                    wire:click="copied"
                                                    class="w-5 h-5 text-gray-500 hover:text-blue-800" aria-hidden="true"
                                                    focusable="false" data-prefix="far" data-icon="edit" role="img"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                    <path fill="currentColor"
                                                        d="M502.6 70.63l-61.25-61.25C435.4 3.371 427.2 0 418.7 0H255.1c-35.35 0-64 28.66-64 64l.0195 256C192 355.4 220.7 384 256 384h192c35.2 0 64-28.8 64-64V93.25C512 84.77 508.6 76.63 502.6 70.63zM464 320c0 8.836-7.164 16-16 16H255.1c-8.838 0-16-7.164-16-16L239.1 64.13c0-8.836 7.164-16 16-16h128L384 96c0 17.67 14.33 32 32 32h47.1V320zM272 448c0 8.836-7.164 16-16 16H63.1c-8.838 0-16-7.164-16-16L47.98 192.1c0-8.836 7.164-16 16-16H160V128H63.99c-35.35 0-64 28.65-64 64l.0098 256C.002 483.3 28.66 512 64 512h192c35.2 0 64-28.8 64-64v-32h-47.1L272 448z">
                                                </svg></button>
                                        @else
                                            <button title="Copy to Clipboard"><svg wire:click="notcopied"
                                                    class="w-5 h-5 text-gray-500 hover:text-blue-800" aria-hidden="true"
                                                    focusable="false" data-prefix="far" data-icon="edit" role="img"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                    <path fill="currentColor"
                                                        d="M502.6 70.63l-61.25-61.25C435.4 3.371 427.2 0 418.7 0H255.1c-35.35 0-64 28.66-64 64l.0195 256C192 355.4 220.7 384 256 384h192c35.2 0 64-28.8 64-64V93.25C512 84.77 508.6 76.63 502.6 70.63zM464 320c0 8.836-7.164 16-16 16H255.1c-8.838 0-16-7.164-16-16L239.1 64.13c0-8.836 7.164-16 16-16h128L384 96c0 17.67 14.33 32 32 32h47.1V320zM272 448c0 8.836-7.164 16-16 16H63.1c-8.838 0-16-7.164-16-16L47.98 192.1c0-8.836 7.164-16 16-16H160V128H63.99c-35.35 0-64 28.65-64 64l.0098 256C.002 483.3 28.66 512 64 512h192c35.2 0 64-28.8 64-64v-32h-47.1L272 448z">
                                                </svg></button>
                                        @endif


                                        @can('crm_service_request_knowledge_edit')
                                            <span title="Edit">
                                                <svg wire:click="action({'id': {{ $knowledge->id }}}, 'edit')"
                                                    class="w-5 h-5 text-blue hover:text-blue-800" aria-hidden="true"
                                                    focusable="false" data-prefix="far" data-icon="edit" role="img"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                    <path fill="currentColor"
                                                        d="M471.6 21.7c-21.9-21.9-57.3-21.9-79.2 0L362.3 51.7l97.9 97.9 30.1-30.1c21.9-21.9 21.9-57.3 0-79.2L471.6 21.7zm-299.2 220c-6.1 6.1-10.8 13.6-13.5 21.9l-29.6 88.8c-2.9 8.6-.6 18.1 5.8 24.6s15.9 8.7 24.6 5.8l88.8-29.6c8.2-2.8 15.7-7.4 21.9-13.5L437.7 172.3 339.7 74.3 172.4 241.7zM96 64C43 64 0 107 0 160V416c0 53 43 96 96 96H352c53 0 96-43 96-96V320c0-17.7-14.3-32-32-32s-32 14.3-32 32v96c0 17.7-14.3 32-32 32H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h96c17.7 0 32-14.3 32-32s-14.3-32-32-32H96z">
                                                    </path>
                                                </svg>
                                            </span>
                                        @endcan

                                        @can('crm_service_request_knowledge_deactivate')
                                            @if ($knowledge->status == 1)
                                                <span title="Deactivate">
                                                    <svg x-cloak x-show="'{{ $knowledge->status == 1 }}'"
                                                        wire:click="action({'id': {{ $knowledge->id }}, 'status': 2}, 'update_status')"
                                                        class="w-8 h-5 text-green" aria-hidden="true" focusable="false"
                                                        data-prefix="fas" data-icon="user-slash" role="img"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512">
                                                        <path fill="currentColor"
                                                            d="M192 64C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192s-86-192-192-192H192zM384 352c-53 0-96-43-96-96s43-96 96-96s96 43 96 96s-43 96-96 96z">
                                                        </path>
                                                    </svg>
                                                </span>
                                            @else
                                                <span title="Reactivate">
                                                    <svg x-cloak x-show="'{{ $knowledge->status == 2 }}'"
                                                        wire:click="action({'id': {{ $knowledge->id }}, 'status': 1}, 'update_status')"
                                                        class="w-8 h-5 text-red" aria-hidden=" true" focusable="false"
                                                        data-prefix="fas" data-icon="user" role="img"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512">
                                                        <path fill="currentColor"
                                                            d="M384 128c70.7 0 128 57.3 128 128s-57.3 128-128 128H192c-70.7 0-128-57.3-128-128s57.3-128 128-128H384zM576 256c0-106-86-192-192-192H192C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192zM192 352c53 0 96-43 96-96s-43-96-96-96s-96 43-96 96s43 96 96 96z">
                                                        </path>
                                                    </svg>
                                                </span>
                                            @endif
                                        @endcan
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $knowledge_s->links() }}
                </div>
            </div>
        </div>

    </x-slot>
</x-form>






@push('scripts')
    <!-- Required chart.js -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script type="text/javascript"></script>

    <script>
        // function copyToClipboard(i) {
        //     // alert(i);
        //     // Get the text field
        //     var copyText = document.getElementById("concerns" + i);
        //     var text = copyText.innerText;
        //     //  alert(copyText.innerText);
        //     // Select the text field
        //     //   copyText.select();
        //     //   copyText.setSelectionRange(0, 99999); // For mobile devices

        //     // Copy the text inside the text field
        //     // navigator.clipboard.writeText(copyText.innerText);
        //     text.select();
        //     document.execCommand('copy')
        //     // Alert the copied text
        // }
        function copyToClipboard(i) {
            const str = document.getElementById('concerns' + i).innerText
            const el = document.createElement('textarea')
            el.value = str
            el.setAttribute('readonly', '')
            el.style.position = 'absolute'
            el.style.left = '-9999px'
            document.body.appendChild(el)
            el.select()
            document.execCommand('copy')
            document.body.removeChild(el);
            // alert('Copied To Clipboard');


        }
    </script>
@endpush
