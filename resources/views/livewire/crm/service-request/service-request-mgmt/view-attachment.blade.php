<div>
    @foreach ($requests as $i => $attachment)
        @if (in_array($attachment->extension, config('filesystems.image_type')))
            <div class="swiper-slide">
                <img class="min-h-screen object-fit min-w-screen"
                    src="{{ Storage::disk('crm_gcs')->url($attachment->path . $attachment->name) }}">
            </div>
        @elseif(in_array($attachment->extension, config('filesystems.file_type')))
            <iframe src="{{ Storage::disk('crm_gcs')->url($attachment->path . $attachment->name) }}"
                type="application/pdf" class="min-h-screen object-fit min-w-screen" width="100%" height="600px">
            </iframe>
        @endif
    @endforeach
</div>
