<x-form x-data="{
    search_form: false,
    create_modal: '{{ $create_modal }}',
    edit_modal: '{{ $edit_modal }}',
    confirmation_modal: '{{ $confirmation_modal }}',
}">
    <x-slot name="loading">
        {{-- <x-loading /> --}}
    </x-slot>

    <x-slot name="modals">
        @can('crm_service_request_service_request_mgmt_view')
            <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
                <x-slot name="body">
                    @livewire('crm.service-request.service-request-mgmt.create')
                </x-slot>
            </x-modal>
        @endcan
        <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/5">
            <x-slot name="body">
                <span class="relative block">
                    <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                        wire:click="$set('confirmation_modal', false)">
                    </span>
                </span>
                <h2 class="mb-3 text-xl font-bold text-center text-blue">
                    SR is already resolved?
                </h2>

                <div class="flex justify-between px-12 mt-6 space-x-3">
                    <button type="button" wire:click="$set('confirmation_modal', false)"
                        class="px-8 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                        No
                    </button>
                    <button type="button" wire:click="resolveSr"
                        class="px-8 py-2 text-xs flex-none bg-[#003399] text-white rounded-lg">
                        Yes
                    </button>
                </div>
            </x-slot>
        </x-modal>
    </x-slot>

    <x-slot name="header_title">Service Request Management</x-slot>
    <x-slot name="header_button">
        @can('crm_service_request_service_request_mgmt_view')
            <button wire:click="action({}, 'add')" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
                <div class="flex items-start justify-between">
                    <svg class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                        data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                    Add Service Request
                </div>
            </button>
        @endcan
    </x-slot>
    <x-slot name="body">
        <div class="flex gap-6">
            <div wire:click="cardHeader(1)"
                class="{{ $card_header == 1 ? 'border-4' : '' }} flex justify-between items-center gap-24 px-4 py-4 bg-white border border-[#003399] rounded-lg hover:bg-blue-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
                <h5 class="font-medium text-gray-700 uppercase dark:text-gray-40">ALL SR</h5>
                <h5 class="text-3xl font-medium text-gray-700 dark:text-gray-400">{{ $all_sr }}</h5>
            </div>
            <div wire:click="cardHeader(2)"
                class="{{ $card_header == 2 ? 'border-4' : '' }} flex justify-between items-center gap-24 px-4 py-4 bg-white border border-[#003399] rounded-lg hover:bg-blue-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
                <h5 class="font-medium text-gray-700 uppercase dark:text-gray-400">CLOSED SR</h5>
                <h5 class="text-3xl font-medium text-gray-700 dark:text-gray-400">{{ $closed_sr }}</h5>
            </div>
            <div wire:click="cardHeader(3)"
                class="{{ $card_header == 3 ? 'border-4' : '' }} flex justify-between items-center gap-24 px-4 py-4 bg-white border border-[#003399] rounded-lg hover:bg-blue-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
                <h5 class="font-medium text-left text-gray-700 uppercase dark:text-gray-400">TEAM'S<br> OPEN SR</h5>
                <h5 class="text-3xl font-medium text-gray-700 dark:text-gray-400">{{ $teams_sr }}</h5>
            </div>
            <div wire:click="cardHeader(4)"
                class="{{ $card_header == 4 ? 'border-4' : '' }} flex justify-between items-center gap-24 px-4 py-4 bg-white border border-[#003399] rounded-lg hover:bg-blue-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
                <h5 class="font-medium text-left text-gray-700 uppercase dark:text-gray-400">MY<br> OPEN SR</h5>
                <h5 class="text-3xl font-medium text-gray-700 dark:text-gray-400">{{ $my_open_sr }}</h5>
            </div>
        </div>
        <div class="flex gap-12" style="margin-top: 3rem">
            <div class="w-48">
                <x-transparent.select value="{{ $severity_search }}" label="Severity" name="severity_search"
                    wire:model.defer='severity_search'>
                    <option value=""></option>
                    <option value="1">High</option>
                    <option value="2">Medium</option>
                    <option value="3">Low</option>
                </x-transparent.select>
            </div>
            <div class="w-48">
                <x-transparent.input label="SR Number" wire:model.defer='sr_no_search' name="sr_no_search">
                </x-transparent.input>
            </div>
            <div class="w-48">
                {{-- <x-transparent.input type="date" value="" label="Date Created" name="date_created_search"
                    wire:model.defer="date_created_search">
                </x-transparent.input> --}}
                <div class="relative z-0 w-full mb-5">
                    <input datepicker type="text" placeholder=" "
                        class="pt-2 pl-2 pb-1 block w-full mt-0 bg-transparent border-0 border-b-2 appearance-none focus:outline-none focus:ring-0 focus:border-[#003399] border-gray-600"
                        name="date_created_search" wire:model.defer="date_created_search">
                    <label class="absolute text-gray-500 duration-300 focus:text-blue top-3 -z-1 origin-0">
                        <span>Date Created</span>
                    </label>
                    <div class="absolute inset-y-0 right-0 flex items-center pr-3 pointer-events-none">
                        <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor"
                            viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd"
                                d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z"
                                clip-rule="evenodd"></path>
                        </svg>
                    </div>
                </div>
            </div>
            <div class="w-48">
                <x-transparent.select value="{{ $status_search }}" label="Status" name="status_search"
                    wire:model.defer='status_search'>
                    <option value=""></option>
                    <option value="1">New</option>
                    <option value="2">In Progress</option>
                    <option value="3">Closed</option>
                </x-transparent.select>
            </div>
            <div class="">
                <x-button type="button" wire:click="search()" title="Search"
                    class="text-white bg-blue hover:bg-blue-800" />
            </div>
        </div>
        <div class="grid grid-cols-1 gap-4 mt-4">
            <div class="overflow-auto rounded-lg">
                <div class="bg-white rounded-lg shadow-md">
                    <x-table.table>
                        <x-slot name="thead">
                            <x-table.th name="No." />
                            <x-table.th name="Severity" />
                            <x-table.th name="Service Request Number" />
                            <x-table.th name="Subject" />
                            <x-table.th name="Service Request Type" />
                            <x-table.th name="Subcategory" />
                            <x-table.th name="Channel / SR Source" />
                            <x-table.th name="Date Created" />
                            <th class="p-3 tracking-wider whitespace-nowrap" scope="col">
                                <span class="flex gap-2 font-medium">
                                    <div class="mt-[5px]">
                                        Status
                                    </div>
                                    <div class="grid grid-cols-1">
                                        <span title="Descending">
                                            <svg wire:click="sortBy(false)" class="w-4 text-gray-500"
                                                aria-hidden="true" focusable="false" data-prefix="fas"
                                                data-icon="users-cog" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                <path fill="currentColor"
                                                    d="M182.6 137.4c-12.5-12.5-32.8-12.5-45.3 0l-128 128c-9.2 9.2-11.9 22.9-6.9 34.9s16.6 19.8 29.6 19.8H288c12.9 0 24.6-7.8 29.6-19.8s2.2-25.7-6.9-34.9l-128-128z" />
                                            </svg>
                                        </span>
                                        <span title="Ascending">
                                            <svg wire:click="sortBy(true)" class="w-4 text-gray-500"
                                                aria-hidden="true" focusable="false" data-prefix="fas"
                                                data-icon="users-cog" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                <path fill="currentColor"
                                                    d="M137.4 374.6c12.5 12.5 32.8 12.5 45.3 0l128-128c9.2-9.2 11.9-22.9 6.9-34.9s-16.6-19.8-29.6-19.8L32 192c-12.9 0-24.6 7.8-29.6 19.8s-2.2 25.7 6.9 34.9l128 128z" />
                                            </svg>
                                        </span>
                                    </div>
                                </span>
                            </th>
                            <x-table.th name="Company / Business Name" />
                            <x-table.th name="Primary Contact Person" />
                            <x-table.th name="Last Date Updated" />
                            <x-table.th name="Assigned To" />
                        </x-slot>
                        <x-slot name="tbody">
                            @foreach ($service_requests as $i => $sr)
                                <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8] hover-row">
                                    <td class="p-3 whitespace-nowrap">
                                        {{ ($service_requests->currentPage() - 1) * $service_requests->links()->paginator->perPage() + $loop->iteration }}.
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <span
                                            class="flex gap-1 {{ $sr->severity_id == 1 ? 'text-[#FF0000]' : ($sr->severity_id == 2 ? 'text-[#FF8800]' : 'text-[#18EB00]') }}">
                                            @if ($sr->severity_id == 1)
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"
                                                    class="h-3 w-3 text-[#FF0000] mt-1">
                                                    <path fill="currentColor"
                                                        d="M182.6 137.4c-12.5-12.5-32.8-12.5-45.3 0l-128 128c-9.2 9.2-11.9 22.9-6.9 34.9s16.6 19.8 29.6 19.8H288c12.9 0 24.6-7.8 29.6-19.8s2.2-25.7-6.9-34.9l-128-128z" />
                                                </svg>
                                            @elseif ($sr->severity_id == 2)
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"
                                                    class="h-3 w-3 text-[#FF0000] mt-1">
                                                    <path fill="currentColor"
                                                        d="M137.4 374.6c12.5 12.5 32.8 12.5 45.3 0l128-128c9.2-9.2 11.9-22.9 6.9-34.9s-16.6-19.8-29.6-19.8L32 192c-12.9 0-24.6 7.8-29.6 19.8s-2.2 25.7 6.9 34.9l128 128z" />
                                                </svg>
                                            @else
                                                <p class="mr-1">~</p>
                                            @endif
                                            {{ $sr->severity_id == 1 ? 'High' : ($sr->severity_id == 2 ? 'Medium' : 'Low') }}
                                        </span>
                                    </td>
                                    <td class="p-3 whitespace-nowrap ">
                                        <div class="inline-flex">
                                            <p class="underline text-blue"
                                                wire:click="action({'id': {{ $sr->id }}}, 'edit')">
                                                {{ $sr->sr_no }}
                                            </p>
                                            @if ($sr->count_thread > 0)
                                                <span
                                                    class="inline-flex items-center justify-center text-xs w-4 h-4 ml-2 mt-[2px] 
                                                    {{ $sr->count_thread != $sr->read ? 'text-white bg-red-600 rounded-full font-semibold' : 'text-black font-bold' }} 
                                                    ">
                                                    {{ $sr->count_thread }}
                                                </span>
                                            @endif
                                        </div>
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ $sr->subject }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ $sr->srType->name ?? null }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ $sr->subCategory->name ?? null }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ $sr->channelSource->name ?? null }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ date('M. d, Y', strtotime($sr->created_at)) }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <span
                                            class="{{ $sr->status_id == 1 && $sr->created_at == $sr->updated_at
                                                ? 'text-green-500 bg-green-100 px-8 py-1 text-xs rounded-full'
                                                : ($sr->status_id == 3 && $sr->created_at != $sr->updated_at
                                                    ? 'text-blue-500 bg-blue-100 px-8 py-1 text-xs rounded-full'
                                                    : '') }}">
                                            @if ($sr->status_id == 2 || ($sr->status_id == 1 && $sr->created_at != $sr->updated_at))
                                                <select
                                                    class="px-6 text-xs rounded-md hover:text-gray-700 hover-status"
                                                    name="sr_status" wire:model="sr_status"
                                                    wire:change="action({'id': {{ $sr->id }}}, 'resolve')">
                                                    <option value="">In Progress</option>
                                                    <option value="3">Resolved</option>
                                                </select>
                                            @else
                                                {{-- {{ $sr->statusRef->name ?? null }} --}}
                                                @if ($sr->status_id == 3 && $sr->created_at != $sr->updated_at)
                                                    Resolved
                                                @else
                                                    New
                                                @endif
                                            @endif
                                        </span>
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ $sr->customerCorpDetails->account_type ?? null == 2 ? $sr->customerCorpDetails->company_name : '' }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ $sr->customerIndDetails->account_type ?? null == 1 ? $sr->customerIndDetails->fullname : '' }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ date('M. d, Y', strtotime($sr->updated_at)) }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ $sr->assignedTo->name ?? null }}
                                    </td>
                                </tr>
                            @endforeach
                        </x-slot>
                    </x-table.table>
                    <div class="px-1 pb-2">
                        {{ $service_requests->links() }}
                    </div>
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
@push('heads')
    <style>
        .datepicker-grid>*+* {
            margin-top: .5rem
        }

        .datepicker-cell {
            margin-top: .5rem
        }

        tr.hover-row:hover .hover-status {
            color: rgba(55, 65, 81, var(--tw-text-opacity));
        }
    </style>
@endpush
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.5/datepicker.min.js"></script>
@endpush
