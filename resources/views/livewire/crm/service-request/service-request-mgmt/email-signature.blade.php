<div class="mt-6">
    <table width="100%">
        <tbody>
            <tr>
                <td width="200"><a href="https://capex.com.ph"> <img
                            src="https://capex.com.ph/app/uploads/2020/07/CaPEx-Logo.png" width="180" /> </a></td>
                <td width="350">
                    <p style="font-family: Poppins;"><strong><span style="font-family: Poppins; font-size: 16px;">GERLY R.
                                GIANAN</span></strong><br>
                        <span style="font-family: Poppins; font-size: 12px;">Digital Marketing Technology Head</span><br>
                        Cargo Padala Express Forwarding Services Corp. (CaPEx)
                        <br><a style="text-decoration: none; color: #333;" href="callto:0917 622 6625">0917 622
                            6625</a> | <a style="text-decoration: none; color: #333;" href="callto:02 7964 2078">(02)
                            7964 2078</a>
                    </p>
                    <p class="flex gap-1">
                        <a style="text-decoration: none; color: #fff;" href="https://www.capex.com.ph"> <img
                                src="https://capex.com.ph/app/uploads/2021/01/Web.png" width="20px" /> </a> <a
                            style="color: #fff; text-decoration: none !important;"
                            href="https://www.facebook.com/CaPExCargoPadalaExpress"> <img
                                src="https://capex.com.ph/app/uploads/2021/01/Facebook.png" width="20px" /> </a>
                        <a style="color: #fff; text-decoration: none !important;"
                            href="https://www.linkedin.com/company/capex-cargo-padala-express/"> <img
                                src="https://capex.com.ph/app/uploads/2021/01/LinkedIn.png" width="20px" /> </a>
                        <a style="color: #fff; text-decoration: none !important;"
                            href="https://www.instagram.com/capex.forwarder/"> <img
                                src="https://capex.com.ph/app/uploads/2021/01/Instagram.png" width="20px" /> </a>
                        <a style="color: #fff; text-decoration: none !important;"
                            href="http://bit.ly/youtube-cargopadalaexpress"> <img
                                src="https://capex.com.ph/app/uploads/2021/01/Youtube.png" width="20px" /> </a> <a
                            style="tcolor: #fff; text-decoration: none !important;"
                            href="https://forms.gle/neQQoGFeCvGRp6mZ8"> <img
                                src="https://capex.com.ph/app/uploads/2021/01/Survey.png" width="20px" /> </a>
                    </p>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                    {{-- <hr /> --}}
                    <div class="w-1/3 border-t border-gray-200">
                        <p style="font-family: Poppins; font-size: 9px;">Disclaimer: "The information in this
                            electronic message, including any file(s) transmitted with it, is legally privileged and
                            confidential, intended only for the sole use of the individual or entity named as
                            addressee and recipient hereof. If you are not the addressee indicated in this message
                            (or responsible for the delivery of the message to such person) or you have received
                            this message by mistake; you are prohibited to copy, use, disseminate or deliver this
                            message. In such case, you should immediately delete this e-mail from your system and
                            notify the sender by e-mail reply."
                        </p>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>
