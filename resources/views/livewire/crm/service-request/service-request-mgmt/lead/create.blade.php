<div wire:init="load" x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
}">
    <x-loading></x-loading>

    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="mb-3 text-xl font-bold text-left text-blue">
                Are you sure you want to create new lead?
            </h2>

            <div class="flex justify-end mt-6 space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                    No
                </button>
                <button type="button" wire:click="submit"
                    class="px-12 py-2 text-xs flex-none bg-[#003399] text-white rounded-lg">
                    Yes
                </button>
            </div>
        </x-slot>
    </x-modal>

    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">
        <div class="space-y-4 px-2">
            <div class="text-xl font-semibold text-gray-800 -mt-4 mb-4">
                Create Lead
            </div>
            <div class="grid grid-cols-1 space-y-4">
                <div class="grid grid-cols-2 gap-4">
                    <div>
                        <x-label for="lead_name" value="Lead Name" :required="true" />
                        <x-input type="text" name="lead_name" wire:model.defer='lead_name'>
                        </x-input>
                        <x-input-error for="lead_name" />
                    </div>
                    <div>
                        <x-label for="sr_no" value="SR Number" />
                        <x-input type="text" name="sr_no" wire:model.defer='sr_no' disabled>
                        </x-input>
                        <x-input-error for="sr_no" />
                    </div>
                </div>
                <div class="grid grid-cols-2 gap-4">
                    <div>
                        <x-label for="shipment_type" value="Shipment Type" :required="true" />
                        <x-select name="shipment_type" wire:model.defer='shipment_type'>
                            <option value=""></option>
                            @foreach ($shipment_type_references as $shipment_type_ref)
                                <option value="{{ $shipment_type_ref->id }}">
                                    {{ $shipment_type_ref->name }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="shipment_type" />
                    </div>
                    <div>
                        <x-label for="service_requirement" value="Service Requirement" :required="true" />
                        <x-select name="service_requirement" wire:model.defer='service_requirement'>
                            <option value=""></option>
                            @foreach ($service_requirement_references as $service_requirement_ref)
                                <option value="{{ $service_requirement_ref->id }}">
                                    {{ $service_requirement_ref->name }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="service_requirement" />
                    </div>
                </div>
            </div>
            <div class="text-xl font-semibold text-gray-800 -mt-4 mb-4">
                Contact Person
            </div>
            <div class="grid grid-cols-1 space-y-4">
                <div class="whitespace-nowrap">
                    <x-label for="contact_person" value="Contact Person" />
                    <x-input type="text" name="contact_person" wire:model.defer='contact_person' disabled>
                    </x-input>
                    <x-input-error for="contact_person" />
                </div>
                <div>
                    <x-label for="contact_mobile_no" value="Contact Mobile Number"/>
                    <x-input type="number" name="contact_mobile_no" wire:model.defer='contact_mobile_no' disabled>
                    </x-input>
                    <x-input-error for="contact_mobile_no" />
                </div>
                <div>
                    <x-label for="contact_email_address" value="Contact Email Address" />
                    <x-input type="contact_email_address" name="contact_email_address"
                        wire:model.defer='contact_email_address' disabled>
                    </x-input>
                    <x-input-error for="contact_email_address" />
                </div>
                <div>
                    <x-label for="description" value="Description" />
                    <x-textarea type="description" name="description" wire:model.defer='description'>
                    </x-textarea>
                    <x-input-error for="description" />
                </div>
                <div class="grid grid-cols-2 gap-4">
                    <div>
                        <x-label for="contact_owner" value="Contact Owner" />
                        <x-input type="text" name="contact_owner" wire:model.defer='contact_owner' disabled>
                        </x-input>
                        <x-input-error for="contact_owner" />
                    </div>
                    <div>
                        <x-label for="customer_type" value="Customer Type" />
                        <x-input type="text" name="customer_type" wire:model.defer='customer_type' disabled>
                        </x-input>
                        <x-input-error for="customer_type" />
                    </div>
                </div>
                <div>
                    <x-label for="channel_source" value="Channel Source" :required="true" />
                    <x-select name="channel_source" wire:model.defer='channel_source'>
                        <option value=""></option>
                        @foreach ($channel_source_references as $channel_source_ref)
                            <option value="{{ $channel_source_ref->id }}">
                                {{ $channel_source_ref->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="channel_source" />
                </div>
                <div>
                    <x-label for="currency" value="Currency" :required="true" />
                    <x-select name="currency" wire:model.defer='currency'>
                        <option value=""></option>
                        <option value="1">PHP</option>
                        <option value="2">USD</option>
                        <option value="3">EUR</option>
                    </x-select>
                    <x-input-error for="currency" />
                </div>
                <div class="grid grid-cols-1">
                    <div>
                        <div class="text-2xl font-bold text-blue">
                            <div class="flex items-center space-x-3">
                                <x-label for="attachment" value="Attachment" />
                            </div>
                        </div>
                        {{-- <div class="grid gap-2">
                            <div class="flex flex-col overflow-hidden">
                                <x-table.table>
                                    <x-slot name="thead">
                                    </x-slot>
                                    <x-slot name="tbody">
                                        @forelse ($attachments as $i => $attachment)
                                            <tr
                                                class="text-sm  border-0 cursor-pointer even:bg-white hover:text-white hover:bg-blue-200">
                                                <td class="flex items-left justify-left p-2 whitespace-nowrap">
                                                    <div class="flex-shrink-0 mb-1 mr-1 whitespace-nowrap">
                                                        <div class="relative z-0 ">
                                                            <div class="absolute top-0 left-0" hidden>
                                                                @if (!$attachments[$i]['attachment'])
                                                                    <img class="object-contain w-20 h-20 mx-auto border border-gray-500 rounded-lg "
                                                                        src="{{ $attachments[$i]['attachment'] ? $attachments[$i]['attachment']->temporaryUrl() : asset('images/form/add-image.png') }}">
                                                                @elseif (in_array($attachments[$i]['attachment']->extension(), config('filesystems.image_type')))
                                                                    <img class="object-contain w-20 h-20 mx-auto border border-gray-500 rounded-lg "
                                                                        src="{{ $attachments[$i]['attachment'] ? $attachments[$i]['attachment']->temporaryUrl() : asset('images/form/add-image.png') }}">
                                                                @else
                                                                    <svg class="object-contain w-20 h-20 mx-auto border border-gray-500 rounded-lg"
                                                                        aria-hidden="true" focusable="false"
                                                                        data-prefix="fas" data-icon="file-alt"
                                                                        role="img" xmlns="http://www.w3.org/2000/svg"
                                                                        viewBox="0 0 384 512">
                                                                        <path fill="currentColor"
                                                                            d="M224 136V0H24C10.7 0 0 10.7 0 24v464c0 13.3 10.7 24 24 24h336c13.3 0 24-10.7 24-24V160H248c-13.2 0-24-10.8-24-24zm64 236c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-64c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-72v8c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12zm96-114.1v6.1H256V0h6.1c6.4 0 12.5 2.5 17 7l97.9 98c4.5 4.5 7 10.6 7 16.9z">
                                                                        </path>
                                                                    </svg>
                                                                @endif
                                                            </div>
                                                            <input type="file"
                                                                name="attachments.{{ $i }}.attachment"
                                                                wire:model="attachments.{{ $i }}.attachment"
                                                                class="relative z-50 block cursor-pointer text-sm ">
                                                            <x-input-error
                                                                for="attachments.{{ $i }}.attachment" />
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="p-2 whitespace-nowrap">
                                                    @if (count($attachments) > 1)
                                                        <a wire:click="removeAttachments({{ $i }})">
                                                            <p class="text-red-600 underline underline-offset-2">Remove</p>
                                                        </a>
                                                        <svg hidden wire:click="removeAttachments({{ $i }})"
                                                            class="w-5 h-5 text-red" aria-hidden="true" focusable="false"
                                                            data-prefix="fas" data-icon="times-circle" role="img"
                                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                            <path fill="currentColor"
                                                                d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z">
                                                            </path>
                                                        </svg>
                                                    @endif
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="3">
                                                    <p class="text-center">Empty.</p>
                                                </td>
                                            </tr>
                                        @endforelse
                                    </x-slot>
                                </x-table.table>
                                <div class="flex items-center justify-start">
                                    <button type="button" title="Add Attachment" wire:click="addAttachments"
                                        class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-lg">
                                        +</button>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
            <div class="text-xl font-semibold text-gray-800 -mt-4 mb-4">
                Contact Address
            </div>
            <div>
                <div class="flex mb-2">
                    <div class="flex gap-6 mt-1 text-sm font-medium text-gray-500">
                        <div class="">
                            <input type="radio" class="border-[#003399] border" name="address_type"
                                wire:model='address_type' value="1" disabled>
                            Domestic
                        </div>
                        <div class="">
                            <input type="radio" class="border-[#003399] border" name="address_type"
                                wire:model='address_type' value="2" disabled>
                            International
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid grid-cols-1">
                <div class="flex gap-4 mb-6">
                    <div class="grid w-full grid-cols-1 space-y-4">
                        @if ($address_type == 2)
                            <div>
                                <x-label for="country" value="Country" :required="true" />
                                <x-select name="country" wire:model.defer='country' disabled>
                                    <option value="">Select</option>
                                    <option value="1">Country1</option>
                                </x-select>
                                <x-input-error for="country" />
                            </div>
                        @endif
                        <div>
                            <x-label for="address_line1" value="Address Line 1" :required="true" />
                            <x-input type="text" name="address_line1" wire:model.defer='address_line1' disabled>
                            </x-input>
                            <x-input-error for="address_line1" />
                        </div>
                        <div>
                            <x-label for="address_line2" value="Address Line 2" />
                            <x-input type="text" name="address_line2" wire:model.defer='address_line2' disabled>
                            </x-input>
                            <x-input-error for="address_line2" />
                        </div>
                        <div class="grid grid-cols-2 gap-4">
                            <div class="">
                                <x-label for="state_province"
                                    value="State {{ $address_type != 2 ? '/ Province' : '' }}" :required="true" />
                                <x-select name="state_province" wire:model.defer='state_province' disabled>
                                    <option value="">Select</option>
                                    <option value="1">State Province 1</option>
                                </x-select>
                                <x-input-error for="state_province" />
                            </div>
                            <div class="">
                                <x-label for="city_municipality"
                                    value="City {{ $address_type != 2 ? '/ Municipality' : '' }}" :required="true" />
                                <x-select name="city_municipality" wire:model.defer='city_municipality' disabled>
                                    <option value="">Select</option>
                                    <option value="1">City Province 1</option>
                                </x-select>
                                <x-input-error for="city_municipality" />
                            </div>
                            @if ($address_type != 2)
                                <div class="">
                                    <x-label for="barangay" value="Barangay" :required="true" />
                                    <x-select name="barangay" wire:model.defer='barangay' disabled>
                                        <option value="">Select</option>
                                        <option value="1">Barangay 1</option>
                                    </x-select>
                                    <x-input-error for="barangay" />
                                </div>
                            @endif
                            <div class="">
                                <x-label for="postal_code" value="Postal Code" :required="true" />
                                <x-input type="text" name="postal_code" wire:model.defer='postal_code' disabled>
                                </x-input>
                                <x-input-error for="postal_code" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid grid-cols-1">
                <div class="flex justify-end gap-6 mt-12">
                    <button type="button" wire:click="$emit('close_modal', 'add')"
                        class="px-10 py-2 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:text-white hover:bg-red-400">
                        Cancel
                    </button>
                    <button type="submit" class="px-10 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg">
                        Create
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
