<div wire:init="load" x-data="{
    {{-- confirmation_modal: '{{ $confirmation_modal }}', --}}
}" class="px-2">
    <x-loading></x-loading>

    {{-- <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="mb-3 text-xl font-bold text-left text-blue">
                Are you sure you want to submit this sr?
            </h2>

            <div class="flex justify-end mt-6 space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                    No
                </button>
                <button type="button" wire:click="submit"
                    class="px-12 py-2 text-xs flex-none bg-[#003399] text-white rounded-lg">
                    Yes
                </button>
            </div>
        </x-slot>
    </x-modal> --}}

    {{-- <form wire:submit.prevent="confirmationSubmit" autocomplete="off"> --}}
    <div class="mb-6 -mt-2 text-xl font-semibold text-gray-800">
        Add Service Request
    </div>
    <div class="mt-5 space-y-4">
        <div class="flex gap-4 mb-4">
            <div class="grid grid-cols-2 gap-6">
                <ul class="flex mt-2">
                    @foreach ($account_type_cards as $i => $status_card)
                        <li wire:click="accountType({{ $account_type_cards[$i]['id'] }})"
                            class="px-8 py-1 text-sm font-semibold {{ $account_type_cards[$i]['class'] }}
                            {{ $account_type == $account_type_cards[$i]['id'] ? 'bg-blue-100 text-[#003399] border border-[#003399]' : ' border border-gray-600' }}">
                            <button
                                wire:click="$set('{{ $account_type_cards[$i]['action'] }}', {{ $account_type_cards[$i]['id'] }})">
                                {{ $account_type_cards[$i]['title'] }}
                            </button>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <form wire:submit.prevent="" autocomplete="off">
        <div class="space-y-4">
            <div class="grid grid-cols-1">
                <div class="">
                    <x-label for="subject" value="Subject" :required="true" />
                    <x-input type="text" name="subject" wire:model.defer='subject'>
                    </x-input>
                    <x-input-error for="subject" />
                </div>
            </div>
            @if ($account_type == 2)
                <div class="grid grid-cols-1">
                    <div class="whitespace-nowrap">
                        <x-label for="company_business_name" value="Company / Business Name" :required="true" />
                        <div x-data="{ open: false }" class="relative rounded-md" @click.away="open = false">
                            <div>
                                <span class="relative block">
                                    <x-input type="text" value="" @click="open = !open"
                                        wire:model='company_business_search' name="company_business_name">
                                    </x-input>
                                </span>
                            </div>
                            <div x-show="open" x-cloak
                                class="absolute z-10 w-full p-2 overflow-hidden overflow-y-auto bg-gray-100 rounded shadow max-h-96"
                                style="margin:0 0 0 0">
                                <ul class="list-reset">
                                    @forelse ($company_business_references as $i => $company_business)
                                        <li @click="open = !open"
                                            wire:click="getCompanyBusinessDetails({{ $company_business->id }})"
                                            wire:key="{{ 'company_business_name' . $i }}"
                                            class="px-2 text-black cursor-pointer hover:bg-gray-200">
                                            <p>
                                                {{ $company_business->fullname }}
                                            </p>
                                        </li>
                                    @empty
                                        <li>
                                            <p class="px-2 text-black cursor-pointer hover:bg-gray-200">
                                                No Contact Found.
                                            </p>
                                        </li>
                                    @endforelse
                                </ul>
                            </div>
                        </div>
                        <x-input-error for="company_business_name" />
                    </div>
                </div>
            @endif
            <div class="grid grid-cols-1">
                <div class="whitespace-nowrap">
                    <x-label for="contact_person" value="Primary Contact Person" :required="true" />
                    <div x-data="{ open: false }" class="relative rounded-md" @click.away="open = false">
                        <div>
                            <span class="relative block">
                                <input type="text"
                                    class="text-gray-600 block w-full border border-gray-500 p-[5px] 
                                    rounded-md shadow-sm focus:border-blue-300 focus:ring focus:ring-blue-200 
                                    focus:ring-opacity-50"
                                    @click="open = !open" wire:model='contact_person_search' name="contact_person"
                                    @if ($account_type == 2) disabled @endif />
                            </span>
                        </div>
                        <div x-show="open" x-cloak
                            class="absolute w-full p-2 overflow-hidden overflow-y-auto bg-gray-100 rounded shadow max-h-96"
                            style="margin:0 0 0 0">
                            <ul class="list-reset">
                                @if ($contact_person_search != '')
                                    @forelse ($contact_person_references as $i => $contact_person)
                                        <li @click="open = !open"
                                            wire:click="getContactDetails({{ $contact_person->id }})"
                                            wire:key="{{ 'contact_person' . $i }}"
                                            class="px-2 text-black cursor-pointer hover:bg-gray-200">
                                            <p>
                                                {{ $contact_person->fullname }}
                                            </p>
                                        </li>
                                    @empty
                                        <li>
                                            <p class="px-2 text-black cursor-pointer hover:bg-gray-200">
                                                No Contact Found.
                                            </p>
                                        </li>
                                    @endforelse

                                @endif
                            </ul>
                        </div>
                    </div>
                    <x-input-error for="primary_contact_person" />
                </div>
            </div>
            <div class="grid grid-cols-1">
                <div class="grid grid-cols-2 gap-4">
                    <div>
                        <x-label for="mobile_number" value="Mobile Number" />
                        <x-input type="text" name="mobile_number" wire:model.defer='mobile_number' disabled>
                        </x-input>
                        <x-input-error for="mobile_number" />
                    </div>
                    <div>
                        <x-label for="telephone_number" value="Telephone Number" />
                        <x-input type="text" name="telephone_number" wire:model.defer='telephone_number' disabled>
                        </x-input>
                        <x-input-error for="telephone_number" />
                    </div>
                </div>
            </div>
            <div class="grid grid-cols-1">
                <div class="">
                    <x-label for="email_address" value="Email Address" />
                    <x-input type="text" name="email_address" wire:model.defer='email_address' disabled>
                    </x-input>
                    <x-input-error for="email_address" />
                </div>
            </div>
            <div class="grid grid-cols-1">
                <div class="grid grid-cols-2 gap-4">
                    <div class="whitespace-nowrap">
                        <x-label for="sr_type" value="Service Request Type" :required="true" />
                        <x-select type="text" name="sr_type" wire:model='sr_type'>
                            <option value=""></option>
                            @foreach ($sr_type_references as $sr_type_ref)
                                <option value="{{ $sr_type_ref->id }}">
                                    {{ $sr_type_ref->name }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="sr_type" />
                    </div>
                    <div>
                        <x-label for="severity" value="Severity" :required="true" />
                        <x-select type="text" name="severity" wire:model.defer='severity' disabled>
                            <option value=""></option>
                            <option value="1">High</option>
                            <option value="2">Medium</option>
                            <option value="3">Low</option>
                        </x-select>
                        <x-input-error for="severity" />
                    </div>
                </div>
            </div>
            @if ($sr_type > 1)
                <div class="grid grid-cols-1">
                    <div class="grid grid-cols-2 gap-4">
                        <div>
                            <x-label for="subcategory" value="Subcategory" :required="true" />
                            <x-select type="text" name="subcategory" wire:model='subcategory'>
                                <option value=""></option>
                                @foreach ($sub_category_references as $sub_category_ref)
                                    <option value="{{ $sub_category_ref->id }}">
                                        {{ $sub_category_ref->name }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="subcategory" />
                        </div>
                        @if ($sr_type == 2 && $subcategory == 1)
                            <div>
                                <x-label for="booking_ref_no" value="Booking Reference No." :required="true" />
                                <x-input type="text" name="booking_ref_no" wire:model.defer='booking_ref_no'>
                                </x-input>
                                <x-input-error for="booking_ref_no" />
                            </div>
                        @elseif ($sr_type == 2 && $subcategory == 2)
                            <div>
                                <x-label for="waybill_no" value="Concerned Waybill No." :required="true" />
                                <x-input type="text" name="waybill_no" wire:model.defer='waybill_no'>
                                </x-input>
                                <x-input-error for="waybill_no" />
                            </div>
                        @elseif ($sr_type > 1)
                            <div class="mt-6">
                                <x-input type="text" name="" wire:model.defer='' disabled>
                                </x-input>
                            </div>
                        @endif
                    </div>
                </div>
            @endif
            <div class="grid grid-cols-1">
                @if ($sr_type == 3)
                    <div class="">
                        <x-label for="service_requirement" value="Service Requirement"/>
                        <x-input type="text" name="" wire:model.defer='' disabled>
                        </x-input>
                    </div>
                @else
                    <div class="">
                        <x-label for="service_requirement" value="Service Requirement" :required="true" />
                        <x-select type="text" name="service_requirement" wire:model='service_requirement'>
                            <option value=""></option>
                            @foreach ($service_requirement_references as $service_requirement_ref)
                                <option value="{{ $service_requirement_ref->id }}">
                                    {{ $service_requirement_ref->name }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="service_requirement" />
                    </div>
                @endif
            </div>
            <div class="grid grid-cols-1">
                <div class="">
                    <x-label for="assigned_to" value="Assigned To" :required="true" />
                    <x-select type="text" name="assigned_to" wire:model='assigned_to'>
                        <option value=""></option>
                        @foreach ($assigned_to_references as $assigned_to_ref)
                            <option value="{{ $assigned_to_ref->id }}">
                                {{ $assigned_to_ref->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="assigned_to" />
                </div>
            </div>
            <div class="grid grid-cols-1">
                <div class="">
                    <x-label for="channel_sr_source" value="Channel / SR Source" :required="true" />
                    <x-select type="text" name="channel_sr_source" wire:model='channel_sr_source'>
                        <option value=""></option>
                        @foreach ($channel_references as $channel_ref)
                            <option value="{{ $channel_ref->id }}">
                                {{ $channel_ref->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="channel_sr_source" />
                </div>
            </div>
            <div class="grid grid-cols-1">
                <div class="">
                    <x-label for="status" value="Status" />
                    <span class="px-12 py-1 text-xs text-green-600 bg-green-100 rounded-full">New</span>
                </div>
            </div>
            <div class="grid grid-cols-1">
                <div>
                    <div class="text-2xl font-bold text-blue">
                        <div class="flex items-center space-x-3">
                            <x-label for="attachment" value="Attachments" />
                        </div>
                    </div>
                    <div class="grid gap-2">
                        <div class="flex flex-col overflow-hidden">
                            <table class="w-full">
                                <thead>
                                </thead>
                                <tbody>
                                    @forelse ($attachments as $i => $attachment)
                                        <tr class="text-xs border-0 cursor-pointer even:bg-white">
                                            <td class="flex p-1 items-left justify-left whitespace-nowrap">
                                                <div class="flex-shrink-0 mb-1 mr-1 whitespace-nowrap ">
                                                    <div class="relative z-0">
                                                        <input type="file"
                                                            name="attachments.{{ $i }}.attachment"
                                                            wire:model="attachments.{{ $i }}.attachment"
                                                            class="absolute top-0 left-0 z-50 opacity-0">

                                                        @if ($attachments[$i]['attachment'])
                                                            <label for="attachments.{{ $i }}.attachment"
                                                                class="relative z-30 block px-2 py-1 text-xs bg-gray-200 border border-gray-500 rounded-lg cursor-pointer">
                                                                <span class="flex gap-1">
                                                                    <svg class="w-3 h-3" aria-hidden="true"
                                                                        focusable="false" data-prefix="fas"
                                                                        data-icon="file-alt" role="img"
                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                        viewBox="0 0 384 512">
                                                                        <path fill="currentColor"
                                                                            d="M364.2 83.8c-24.4-24.4-64-24.4-88.4 0l-184 184c-42.1 42.1-42.1 110.3 0 152.4s110.3 42.1 152.4 0l152-152c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-152 152c-64 64-167.6 64-231.6 0s-64-167.6 0-231.6l184-184c46.3-46.3 121.3-46.3 167.6 0s46.3 121.3 0 167.6l-176 176c-28.6 28.6-75 28.6-103.6 0s-28.6-75 0-103.6l144-144c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-144 144c-6.7 6.7-6.7 17.7 0 24.4s17.7 6.7 24.4 0l176-176c24.4-24.4 24.4-64 0-88.4z">
                                                                        </path>
                                                                    </svg>{{ $attachments[$i]['attachment']->getClientOriginalName() }}
                                                                </span>
                                                            </label>
                                                        @else
                                                            <label for="attachments.{{ $i }}.attachment"
                                                                class="relative z-30 block px-2 py-1 text-xs bg-gray-200 border border-gray-500 rounded-lg cursor-pointer">
                                                                <span class="flex gap-1">
                                                                    <svg class="w-3 h-3" aria-hidden="true"
                                                                        focusable="false" data-prefix="fas"
                                                                        data-icon="file-alt" role="img"
                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                        viewBox="0 0 384 512">
                                                                        <path fill="currentColor"
                                                                            d="M364.2 83.8c-24.4-24.4-64-24.4-88.4 0l-184 184c-42.1 42.1-42.1 110.3 0 152.4s110.3 42.1 152.4 0l152-152c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-152 152c-64 64-167.6 64-231.6 0s-64-167.6 0-231.6l184-184c46.3-46.3 121.3-46.3 167.6 0s46.3 121.3 0 167.6l-176 176c-28.6 28.6-75 28.6-103.6 0s-28.6-75 0-103.6l144-144c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-144 144c-6.7 6.7-6.7 17.7 0 24.4s17.7 6.7 24.4 0l176-176c24.4-24.4 24.4-64 0-88.4z">
                                                                        </path>
                                                                    </svg>Choose Attachment
                                                                </span>
                                                            </label>
                                                        @endif

                                                        <x-input-error
                                                            for="attachments.{{ $i }}.attachment" />
                                                    </div>

                                                </div>
                                            </td>
                                            <td class="p-2 whitespace-nowrap">
                                                @if (count($attachments) > 1)
                                                    <a wire:click="removeAttachments({{ $i }})">
                                                        <p class="text-red-600 underline underline-offset-2">Remove</p>
                                                    </a>
                                                    <svg hidden wire:click="removeAttachments({{ $i }})"
                                                        class="w-5 text-red" aria-hidden="true" focusable="false"
                                                        data-prefix="fas" data-icon="times-circle" role="img"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                        <path fill="currentColor"
                                                            d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z">
                                                        </path>
                                                    </svg>
                                                @endif
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="3">
                                                <p class="text-center">Empty.</p>
                                            </td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                            <div class="flex items-center justify-start">
                                <button type="button" title="Add Attachment" wire:click="addAttachments"
                                    class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-lg">
                                    +</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid grid-cols-1">
                <div class="">
                    <x-label for="sr_description" value="Service Request Description" :required="true" />
                    <textarea rows="4"
                        class="text-gray-600 block w-full mt-1
                        border border-gray-500 p-[5px] rounded-md shadow-sm focus:border-blue-300 focus:ring focus:ring-blue-200
                        focus:ring-opacity-50"
                        name="sr_description" wire:model.defer='sr_description'></textarea>
                    <x-input-error for="sr_description" />
                </div>
            </div>
            <div class="grid grid-cols-1">
                <div class="flex gap-4 mt-4">
                    <div class="w-full col-span-11">
                        <div class="flex justify-end gap-4">
                            <button type="button" wire:click="$emit('close_modal', 'add')"
                                class="px-10 py-2 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:text-white hover:bg-red-400">
                                Cancel
                            </button>
                            <button {{-- type="submit"  --}} wire:click="submit"
                                class="px-10 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg">
                                Add SR
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
