<div>
    <x-loading></x-loading>
    <form wire:submit.prevent="send" autocomplete="off">
        <div class="px-2 space-y-4">
            <div class="mb-4 -mt-4 text-xl font-semibold text-gray-800">
                New Message
            </div>
            <div class="grid grid-cols-1 space-y-4">
                <div class="grid gap-4 grid-cols-1s">
                    <div>
                        <x-label for="to" value="To" />
                        <x-input type="text" name="to" wire:model.defer='to' disabled>
                        </x-input>
                        <x-input-error for="to" />
                    </div>
                </div>
                <div class="grid grid-cols-1 gap-4">
                    <div>
                        <x-label for="subject" value="Subject" />
                        <textarea name="subject" wire:model.defer='subject' rows="2"
                            class="text-gray-600 block w-full mt-1 border border-gray-500 p-[5px] rounded-md shadow-sm focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50"></textarea>
                        <x-input-error for="subject" />
                    </div>
                </div>
                <div class="grid grid-cols-1 gap-4">
                    <div>
                        <x-label for="body" value="Body" />
                        <textarea name="body" wire:model.defer='body' rows="18"
                            class="text-gray-600 block w-full mt-1 border border-gray-500 p-[5px] rounded-md shadow-sm focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50"></textarea>
                        <x-input-error for="body" />
                    </div>
                </div>
                <div class="flex justify-end gap-6 mt-12">
                    <button type="button" wire:click="$emit('close_modal', 'reply')"
                        class="px-10 py-2 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:text-white hover:bg-red-400">
                        Cancel
                    </button>
                    <button type="submit" class="px-10 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg">
                        Send
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
