<x-form wire:init="load" x-data="{
    search_form: false,
    create_modal: '{{ $create_modal }}',
    compose_modal: '{{ $compose_modal }}',
    reply_modal: '{{ $reply_modal }}',
    attachment_modal: '{{ $attachment_modal }}',
    open: '{{ $open }}',
    isOpen: '{{ $isOpen }}',
}">
    <x-slot name="loading">
        {{-- <x-loading /> --}}
    </x-slot>

    <x-slot name="modals">
        @if ($sr_id && $create_modal)
            <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                <x-slot name="body">
                    {{-- <x-slot name="title">Create Lead</x-slot> --}}
                    @livewire('crm.service-request.service-request-mgmt.lead.create', ['id' => $sr_id])
                </x-slot>
            </x-modal>
        @endif
        @if ($sr_number && $reply_modal)
            <x-modal id="reply_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-2/5">
                <x-slot name="body">
                    @livewire('crm.service-request.service-request-mgmt.reply', ['sr_number' => $sr_number])
                </x-slot>
            </x-modal>
        @endif
        @if ($sr_number && $compose_modal)
            <x-modal id="compose_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                <x-slot name="body">
                    @livewire('crm.service-request.service-request-mgmt.compose', ['sr_number' => $sr_number])
                </x-slot>
            </x-modal>
        @endif
        @if ($attachment_id && $attachment_modal)
            <x-modal id="attachment_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/2">
                <x-slot name="body">
                    @livewire('crm.service-request.service-request-mgmt.view-attachment', ['id' => $attachment_id])
                </x-slot>
            </x-modal>
        @endif
    </x-slot>

    <x-slot name="header_title">
        <span>
            <div class="flex items-start justify-between gap-2" wire:click="action({}, 'back')">
                <svg class="w-8 h-8 mt-1 mr-4 text-blue-800 cursor-pointer" aria-hidden="true" focusable="false"
                    data-prefix="far" data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 448 512">
                    <path fill="currentColor"
                        d="M257.5 445.1l-22.2 22.2c-9.4 9.4-24.6 9.4-33.9 0L7 273c-9.4-9.4-9.4-24.6 0-33.9L201.4 44.7c9.4-9.4 24.6-9.4 33.9 0l22.2 22.2c9.5 9.5 9.3 25-.4 34.3L136.6 216H424c13.3 0 24 10.7 24 24v32c0 13.3-10.7 24-24 24H136.6l120.5 114.8c9.8 9.3 10 24.8.4 34.3z" />
                </svg>
                <div class="flex gap-3">
                    <span class="text-gray-400">Edit Service Request : </span>
                    {{ $sr_no }}
                </div>

            </div>
        </span>
    </x-slot>
    <x-slot name="header_button">
        @can('crm_service_request_service_request_mgmt_view')
            <div class="flex gap-4 mr-4">
                <button wire:click="action({id:{{ $sr_id }}}, 'add')"
                    @if ($with_lead) disabled readonly @endif
                    class="p-2 px-3 mr-3 text-sm text-white rounded-md 
                    {{ $with_lead ? 'bg-gray-400 text-gray-700' : 'bg-[#003399]' }}">
                    <div class="flex items-start justify-between">
                        Create as Lead
                    </div>
                </button>
                <div x-data="{ open: '{{ $open }}' }">
                    <button wire:click="action({}, '')" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue"
                        @click="open = !open">
                        <div class="flex items-start justify-between">
                            Add Activity
                            <svg class="w-3 h-3 mt-1 ml-2" aria-hidden="true" focusable="false" data-prefix="far"
                                data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 448 512">
                                <path fill="currentColor"
                                    d="M137.4 374.6c12.5 12.5 32.8 12.5 45.3 0l128-128c9.2-9.2 11.9-22.9 6.9-34.9s-16.6-19.8-29.6-19.8L32 192c-12.9 0-24.6 7.8-29.6 19.8s-2.2 25.7 6.9 34.9l128 128z" />
                            </svg>
                        </div>
                    </button>
                    <div class="absolute border-2 border-gray-300 rounded-sm " style="margin-top: .4%; margin-left: -4.2rem"
                        x-transition:leave="transition ease-in duration-150" x-transition:leave-start="opacity-100"
                        x-transition:leave-end="opacity-0" x-show="open" @click.away="open = false"
                        @keydown.escape="open = false">
                        <ul class="text-gray-600 bg-white rounded shadow">
                            <li class="px-4 py-1 cursor-pointer" x-data="{ open: false }" wire:click="action({'id': },'')">
                                Create a Task
                            </li>
                            <li class="px-4 py-1 cursor-pointer" x-data="{ open: false }" wire:click="action({'id': },'')">
                                Create a Note
                            </li>
                            <li class="px-4 py-1 cursor-pointer" x-data="{ open: false }" wire:click="action({'id': },'')">
                                Create an Email
                            </li>
                            <li class="px-4 py-1 cursor-pointer" x-data="{ open: false }"
                                wire:click="action({'id': {{ $sr_id }}, 'from_sr': 'sr-create-quotation'},'create_quotation')">
                                Create Quotation
                            </li>
                            <li class="px-4 py-1 cursor-pointer" x-data="{ open: false }" wire:click="action({'id': },'')">
                                Log a Meeting
                            </li>
                            <li class="px-4 py-1 cursor-pointer" x-data="{ open: false }" wire:click="action({'id': },'')">
                                Schedule a Meeting
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        @endcan
    </x-slot>
    <x-slot name="body">
        <div class="grid grid-cols-1 gap-4 p-2 mt-4 bg-white rounded-lg shadow-lg" wire:init="load">
            <div class="px-12 mt-4 text-3xl font-medium text-blue">Summary</div>
            <form wire:submit.prevent="submit" autocomplete="off">
                <div class="grid grid-cols-2 px-12 mt-4 mb-4">
                    <div class="space-y-6">
                        <div class="grid items-center grid-cols-3 px-8 space-x-8">
                            <p class="text-gray-400">Subject :</p>
                            <p class="col-span-2 font-medium text-gray-800">{{ $subject }}</p>
                        </div>
                        <div class="grid items-center grid-cols-3 px-8 space-x-8">
                            <p class="text-gray-400">Date Reported :</p>
                            <p class="col-span-2 font-medium text-gray-800">{{ date('m/d/Y', strtotime($created_at)) }}
                            </p>
                        </div>
                        <div class="grid items-center grid-cols-3 px-8 space-x-8">
                            <p class="text-gray-400">Created by :</p>
                            <p class="col-span-2 font-medium text-gray-800">{{ $created_by }}
                            </p>
                        </div>
                        <div class="grid items-center grid-cols-3 px-8 space-x-8">
                            <p class="text-gray-400 whitespace-nowrap">Company / Business Name :</p>
                            <p class="col-span-2 font-medium text-gray-800">
                                @if ($account_type == 1)
                                    -
                                @else
                                    <input name="company_business_name" wire:model.defer="company_business_name"
                                        class="pt-2 pb-1 block font-medium w-full mt-0 bg-transparent border-0 border-b-2 border-gray-400 appearance-none focus:outline-none focus:ring-0 focus:border-[#003399]">
                                @endif
                            </p>
                        </div>
                        <div class="grid items-center grid-cols-3 px-8 space-x-8">
                            <p class="text-gray-400 whitespace-nowrap">Primary Contact Person :</p>
                            @if ($customer_data)
                                <div class="col-span-2 font-medium text-gray-800">
                                    <div
                                        class="flex justify-between {{ $clicked_edit == 0 ? 'border-b-2 border-gray-400' : 'mb-3' }} ">
                                        @if ($clicked_edit == 0)
                                            <div class="">{{ $primary_contact_person }}</div>
                                            <div>
                                                <svg wire:click="action({}, 'click_edit_name')"
                                                    class="w-4 h-4 cursor-pointer text-blue hover:text-blue-800"
                                                    aria-hidden="true" focusable="false" data-prefix="far"
                                                    data-icon="edit" role="img" xmlns="http://www.w3.org/2000/svg"
                                                    viewBox="0 0 576 512">
                                                    <path fill="currentColor"
                                                        d="M471.6 21.7c-21.9-21.9-57.3-21.9-79.2 0L362.3 51.7l97.9 97.9 30.1-30.1c21.9-21.9 21.9-57.3 0-79.2L471.6 21.7zm-299.2 220c-6.1 6.1-10.8 13.6-13.5 21.9l-29.6 88.8c-2.9 8.6-.6 18.1 5.8 24.6s15.9 8.7 24.6 5.8l88.8-29.6c8.2-2.8 15.7-7.4 21.9-13.5L437.7 172.3 339.7 74.3 172.4 241.7zM96 64C43 64 0 107 0 160V416c0 53 43 96 96 96H352c53 0 96-43 96-96V320c0-17.7-14.3-32-32-32s-32 14.3-32 32v96c0 17.7-14.3 32-32 32H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h96c17.7 0 32-14.3 32-32s-14.3-32-32-32H96z">
                                                    </path>
                                                </svg>
                                            </div>
                                        @else
                                            <div>
                                                <div class="grid-cols-1 -mt-2 -mb-4 font-medium text-gray-800">
                                                    <div class="relative">
                                                        <div
                                                            class="absolute mt-2 text-gray-400 font-normal text-[13px]">
                                                            FN
                                                            :
                                                        </div>
                                                        <input name="fname" wire:model.defer="fname"
                                                            class="pt-2 pb-1 pl-10 block font-medium w-full mt-0 bg-transparent border-0 border-b-2 border-gray-400 appearance-none focus:outline-none focus:ring-0 focus:border-[#003399]">
                                                    </div>
                                                    <div class="relative">
                                                        <div
                                                            class="absolute mt-2 text-gray-400 font-normal text-[13px]">
                                                            MN
                                                            :
                                                        </div>
                                                        <input name="mname" wire:model.defer="mname"
                                                            class="pt-2 pb-1 pl-10 block font-medium w-full mt-0 bg-transparent border-0 border-b-2 border-gray-400 appearance-none focus:outline-none focus:ring-0 focus:border-[#003399]">
                                                    </div>
                                                    <div class="relative">
                                                        <div
                                                            class="absolute mt-2 text-gray-400 font-normal text-[13px]">
                                                            LN
                                                            :
                                                        </div>
                                                        <input name="lname" wire:model.defer="lname"
                                                            class="pt-2 pb-1 pl-10 block font-medium w-full mt-0 bg-transparent border-0 border-b-2 border-gray-400 appearance-none focus:outline-none focus:ring-0 focus:border-[#003399]">
                                                    </div>
                                                    <div class="flex justify-end mt-1">
                                                        <button type="button"
                                                            wire:click="action({id:{{ $sr_id }}}, 'cancel_edit_name')"
                                                            class="text-xs text-gray-600">
                                                            Cancel
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="grid items-center grid-cols-3 px-8 space-x-8">
                            <p class="text-gray-400 whitespace-nowrap">Mobile Number :</p>
                            <div class="col-span-2 font-medium text-gray-800">
                                @if ($channel_sr_source == 5)
                                    <div>-</div>
                                @else
                                    <div class="-mt-4 -mb-4 ">
                                        <input name="mobile_number" wire:model.defer="mobile_number"
                                            class="pt-2 pb-1 block font-medium w-full mt-0 bg-transparent border-0 border-b-2 border-gray-400 appearance-none focus:outline-none focus:ring-0 focus:border-[#003399]">

                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="grid items-center grid-cols-3 px-8 space-x-8">
                            <p class="text-gray-400 whitespace-nowrap">Telephone Number :</p>
                            <div class="col-span-2 font-medium text-gray-800">
                                @if ($channel_sr_source == 5)
                                    <div>-</div>
                                @else
                                    <div class="-mt-4 -mb-4 ">
                                        <input name="telephone_number" wire:model.defer="telephone_number"
                                            class="pt-2 pb-1 block font-medium w-full mt-0 bg-transparent border-0 border-b-2 border-gray-400 appearance-none focus:outline-none focus:ring-0 focus:border-[#003399]">
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="grid items-center grid-cols-3 px-8 space-x-8">
                            <p class="text-gray-400 whitespace-nowrap">Email Address :</p>
                            <div class="col-span-2 font-medium text-gray-800">
                                @if ($channel_sr_source == 5)
                                    <div>-</div>
                                @else
                                    <div class="-mt-4 -mb-4 ">
                                        <input name="email_address" wire:model.defer="email_address"
                                            class="pt-2 pb-1 block font-medium w-full mt-0 bg-transparent border-0 border-b-2 border-gray-400 appearance-none focus:outline-none focus:ring-0 focus:border-[#003399]">
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="grid items-center grid-cols-3 px-8 space-x-8">
                            <p class="text-gray-400 whitespace-nowrap">Service Request Type :</p>
                            <div class="col-span-2 font-medium text-gray-800">
                                {{-- @if ($channel_sr_source == 5)
                                    <div>-</div>
                                @else --}}
                                <div class="-mt-4 -mb-4 ">
                                    <select name="sr_type" wire:model="sr_type"
                                        class="block w-full px-0 pt-2 pb-1 mt-0 font-medium bg-transparent border-0 border-b-2 border-gray-400 focus:border-[#003399] appearance-none z-1 focus:outline-none focus:ring-0 ">
                                        <option value=""></option>
                                        @foreach ($sr_type_references as $sr_type_ref)
                                            <option value="{{ $sr_type_ref->id }}">
                                                {{ $sr_type_ref->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                {{-- @endif --}}
                            </div>
                        </div>
                        @if ($sr_type > 1)
                            <div class="grid items-center grid-cols-3 px-8 space-x-8">
                                <p class="text-gray-400 whitespace-nowrap">Sub category :</p>
                                <div class="col-span-2 font-medium text-gray-800">
                                    @if ($channel_sr_source == 5)
                                        <div>-</div>
                                    @else
                                        <div class="-mt-4 -mb-4 ">
                                            <select name="subcategory" wire:model="subcategory"
                                                class="block w-full px-0 pt-2 pb-1 mt-0 font-medium bg-transparent border-0 border-b-2 border-gray-400 focus:border-[#003399] appearance-none z-1 focus:outline-none focus:ring-0 ">
                                                <option value=""></option>
                                                @foreach ($sub_category_references as $sub_category_ref)
                                                    <option value="{{ $sub_category_ref->id }}">
                                                        {{ $sub_category_ref->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="space-y-6">
                        @if ($sr_type > 1)
                            @if ($sr_type == 2 && $subcategory == 1)
                                <div class="grid items-center grid-cols-3 px-8 space-x-8">
                                    <p class="text-gray-400 whitespace-nowrap">Booking Reference Number :</p>
                                    <div class="col-span-2 font-medium text-gray-800">
                                        <input name="booking_ref_no" wire:model.defer="booking_ref_no"
                                            class="pt-2 pb-1 block font-medium w-full mt-0 bg-transparent border-0 border-b-2 border-gray-400 appearance-none focus:outline-none focus:ring-0 focus:border-[#003399]">
                                    </div>
                                </div>
                            @elseif ($sr_type == 2 && $subcategory == 2)
                                <div class="grid items-center grid-cols-3 px-8 space-x-8">
                                    <p class="text-gray-400 whitespace-nowrap">Concerned Waybill Number :</p>
                                    <div class="col-span-2 font-medium text-gray-800">
                                        <input name="waybill_no" wire:model.defer="waybill_no"
                                            class="pt-2 pb-1 block font-medium w-full mt-0 bg-transparent border-0 border-b-2 border-gray-400 appearance-none focus:outline-none focus:ring-0 focus:border-[#003399]">
                                    </div>
                                </div>
                            @endif
                        @endif
                        <div class="grid items-center grid-cols-3 px-8 space-x-8">
                            <p class="text-gray-400 whitespace-nowrap">Severity :</p>
                            <div class="col-span-2 font-medium text-gray-800">
                                <select name="severity" wire:model.defer="severity"
                                    class="block w-full px-0 pt-2 pb-1 mt-0 font-medium bg-transparent border-0 border-b-2 border-gray-400 focus:border-[#003399] appearance-none z-1 focus:outline-none focus:ring-0 ">
                                    <option value=""></option>
                                    <option value="1">High</option>
                                    <option value="2">Medium</option>
                                    <option value="3">Low</option>
                                </select>
                            </div>
                        </div>
                        <div class="grid items-center grid-cols-3 px-8 space-x-8">
                            <p class="text-gray-400 whitespace-nowrap">Service Requirement :</p>
                            <div class="col-span-2 font-medium text-gray-800">
                                <select name="service_requirement" wire:model.defer="service_requirement"
                                    class="block w-full px-0 pt-2 pb-1 mt-0 font-medium bg-transparent border-0 border-b-2 border-gray-400 focus:border-[#003399] appearance-none z-1 focus:outline-none focus:ring-0 ">
                                    <option value=""></option>
                                    @foreach ($service_requirement_references as $service_requirement_ref)
                                        <option value="{{ $service_requirement_ref->id }}">
                                            {{ $service_requirement_ref->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="grid items-center grid-cols-3 px-8 space-x-8">
                            <p class="text-gray-400 whitespace-nowrap">Assigned to :</p>
                            <div class="col-span-2 font-medium text-gray-800">
                                <select name="assigned_to" wire:model.defer="assigned_to"
                                    class="block w-full px-0 pt-2 pb-1 mt-0 font-medium bg-transparent border-0 border-b-2 border-gray-400 focus:border-[#003399] appearance-none z-1 focus:outline-none focus:ring-0 ">
                                    <option value=""></option>
                                    @foreach ($assigned_to_references as $assigned_to_ref)
                                        <option value="{{ $assigned_to_ref->id }}">
                                            {{ $assigned_to_ref->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="grid items-center grid-cols-3 px-8 space-x-8">
                            <p class="text-gray-400 whitespace-nowrap">Channel / SR Source :</p>
                            <div class="col-span-2 font-medium text-gray-800">
                                <select name="channel_sr_source" wire:model.defer="channel_sr_source"
                                    class="block w-full px-0 pt-2 pb-1 mt-0 font-medium bg-transparent border-0 border-b-2 border-gray-400 focus:border-[#003399] appearance-none z-1 focus:outline-none focus:ring-0 ">
                                    <option value=""></option>
                                    @foreach ($channel_references as $channel_ref)
                                        <option value="{{ $channel_ref->id }}">
                                            {{ $channel_ref->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="grid items-center grid-cols-3 px-8 space-x-8">
                            <p class="text-gray-400 whitespace-nowrap">Attachment :</p>
                            <div class="col-span-2 font-medium text-gray-800">
                                <div class="grid grid-cols-1">
                                    @foreach ($service_request->attachments as $attachment)
                                        <div class="font-medium text-blue-800 underline cursor-pointer whitespace-nowrap"
                                            wire:click="action({id:'{{ $attachment->id }}'},'view-attachment')">
                                            {{ $attachment->original_file_name ?? null }}</div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="grid items-center grid-cols-3 px-8 space-x-8">
                            <p class="text-gray-400 whitespace-nowrap">Status :</p>
                            <div class="col-span-2 font-medium text-gray-800">
                                <span
                                    class="{{ $status == 1 && $created_at == $updated_at
                                        ? 'text-green-500 bg-green-100 px-8 py-1 text-xs rounded-full'
                                        : ($status == 2 || ($status == 1 && $created_at != $updated_at)
                                            ? 'text-gray-500 bg-gray-100 px-8 py-1 text-xs rounded-full'
                                            : ($status == 3 || $created_at != $updated_at
                                                ? 'text-blue-500 bg-blue-100 px-8 py-1 text-xs rounded-full'
                                                : '')) }}">

                                    {{ $status == 1 && $created_at == $updated_at ? 'New' : ($status == 2 || ($status == 1 && $created_at != $updated_at) ? 'In Progress' : 'Resolved') }}
                                </span>
                            </div>
                        </div>
                        <div class="grid items-center grid-cols-3 px-8 space-x-8">
                            <p class="text-gray-400 whitespace-nowrap">Last Updated by :</p>
                            <div class="col-span-2 font-medium text-gray-800">
                                {{ $updated_by }}
                            </div>
                        </div>
                        <div class="grid items-center grid-cols-3 px-8 space-x-8">
                            <p class="text-gray-400 whitespace-nowrap">Milestone :</p>
                            <div class="col-span-2 font-medium text-gray-800">
                                {{-- @if (count($emails) > 0) --}}
                                <div class="flex justify-between">
                                    <div class="flex font-medium text-[#003399]" @mouseenter="isOpen = true"
                                        @mouseleave="isOpen = false">
                                        {{ $status == 1 || $status == 2 ? 'First Response Metric' : 'Resolution Metric' }}
                                    </div>
                                    <div></div>
                                </div>
                                {{-- @else
                                    <div>-</div>
                                @endif --}}
                            </div>
                        </div>
                        <div class="grid items-center grid-cols-3 px-8 space-x-8">
                            <p class="text-gray-400 whitespace-nowrap">Time Remaining :</p>
                            <div class="flex col-span-2 font-medium text-gray-800">
                                {{-- @if (count($emails) > 0) --}}
                                <div class="font-medium text-gray-800">{{ $time_remaining }}</div>
                                {{-- @else
                                    <div>-</div>
                                @endif --}}
                            </div>
                        </div>
                        <div class="grid items-center grid-cols-3 px-8 space-x-8">
                            <p class="text-gray-400 whitespace-nowrap">Due Date :</p>
                            <div class="col-span-2 font-medium text-gray-800">
                                {{-- @if (count($emails) > 0) --}}
                                <div class="font-medium text-gray-800">{{ $due_date }}</div>
                                {{-- @else
                                    <div>-</div>
                                @endif --}}
                            </div>
                        </div>
                    </div>
                    {{-- @if (count($emails) > 0) --}}
                    <div class="absolute flex items-center justify-center w-1/3 text-sm border-2 border-gray-300 rounded-md sm:w-1/2 md:w-1/3 lg:w-1/3 xl:w-1/3"
                        style="margin-top: 6%; margin-left: 22%" x-transition:leave="transition ease-in duration-150"
                        x-show="isOpen" @mouseenter.away="isOpen = false" @mouseleave="isOpen = true">
                        <!-- Modal content goes here -->
                        <div class="grid w-full grid-cols-1 p-3 space-y-4 bg-white">
                            <div class="text-lg font-bold">First Response Metric</div>
                            <div class="grid grid-cols-2">
                                <div class="grid grid-cols-2 gap-4">
                                    <div class="text-gray-300">Time Remaining :</div>
                                    <div class="whitespace-nowrap">{{ $time_remaining }}</div>
                                </div>
                                <div class="grid grid-cols-2 gap-4">
                                    <div class="text-gray-300">Next Milestone :</div>
                                    <div class="whitespace-nowrap">Resolution Metric</div>
                                </div>
                            </div>
                            <div class="grid grid-cols-2">
                                <div class="grid grid-cols-2 gap-4">
                                    <div class="text-gray-300">Due Date</div>
                                    <div class="whitespace-nowrap">{{ $due_date }}</div>
                                </div>
                                <div class="grid grid-cols-2">
                                </div>
                            </div>
                            <div class="border-b border-gray-500"></div>
                            <div class="grid grid-cols-2">
                                <div class="grid grid-cols-2 gap-4">
                                    <div class="text-gray-300 whitespace-nowrap">Starts When :</div>
                                    <div class="whitespace-nowrap">
                                        {{ $starts_when }}</div>
                                </div>
                                <div class="grid grid-cols-2">
                                </div>
                            </div>
                            <div class="grid grid-cols-2">
                                <div class="grid grid-cols-2 gap-4">
                                    <div class="text-gray-300 whitespace-nowrap">Completes When :</div>
                                    <div class="whitespace-nowrap">{{ $resolved_date }}</div>
                                </div>
                                <div class="grid grid-cols-2">
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- @endif --}}
                </div>
                <div class="flex justify-end px-6 mb-4">
                    <button type="submit" class="p-2 px-10 mr-3 text-sm text-white rounded-md bg-blue">
                        Save
                    </button>
                </div>
            </form>
        </div>
        <div class="grid grid-cols-1 gap-4 p-2 mt-4 bg-white rounded-lg shadow-lg">
            <div class="px-8 py-6">
                <div class="grid grid-cols-3">
                    <div class="col-span-2 px-12 mr-0">
                        <div class="text-gray-400">Service Request Description :</div>
                    </div>
                    @if (!$clicked_edit_sr_description)
                        <div class="col-span-1 px-12">
                            <svg wire:click="action({}, 'edit_sr_description')"
                                class="w-5 h-5 ml-auto cursor-pointer text-blue hover:text-blue-800"
                                aria-hidden="true" focusable="false" data-prefix="far" data-icon="edit"
                                role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                <path fill="currentColor"
                                    d="M471.6 21.7c-21.9-21.9-57.3-21.9-79.2 0L362.3 51.7l97.9 97.9 30.1-30.1c-21.9-21.9-57.3-21.9-79.2 0L471.6 21.7zm-299.2 220c-6.1 6.1-10.8 13.6-13.5 21.9l-29.6 88.8c-2.9 8.6-.6 18.1 5.8 24.6s15.9 8.7 24.6 5.8l88.8-29.6c8.2-2.8 15.7-7.4 21.9-13.5L437.7 172.3 339.7 74.3 172.4 241.7zM96 64C43 64 0 107 0 160V416c0 53 43 96 96 96H352c53 0 96-43 96-96V320c0-17.7-14.3-32-32-32s-32 14.3-32 32v96c0 17.7-14.3 32-32 32H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h96c17.7 0 32-14.3 32-32s-14.3-32-32-32H96z">
                                </path>
                            </svg>
                        </div>
                    @endif
                </div>
                <div class="px-12 text-gray-500">
                    @if (!$clicked_edit_sr_description)
                        {{ $sr_description }}
                    @else
                        <textarea name="sr_description" wire:model.defer='sr_description' rows="6"
                            class="w-full text-gray-600 block mt-1 border border-gray-500 p-[5px] rounded-md shadow-sm focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50"></textarea>
                    @endif
                </div>
                @if ($clicked_edit_sr_description)
                    <div class="flex justify-end px-12 mt-2">
                        <button wire:click="action({}, 'save_sr_description')" type="button"
                            class="px-6 py-1 ml-auto text-sm text-white rounded-md bg-blue">
                            Save
                        </button>
                    </div>
                @endif
            </div>
        </div>

        @if (count($emails) > 0)
            <div class="grid grid-cols-1 gap-4 p-2 mt-4 bg-white rounded-lg shadow-lg">
                <div class="flex justify-between">
                    <div class="px-12 mt-4 text-3xl font-medium text-blue">Messages</div>
                    <div class="flex gap-6 px-12">
                        <div class="flex gap-6 mt-2 mb-4">
                            <button type="button"wire:click=""
                                class="text-gray-400 bg-gray-100 hover:bg-gray-200  border border-gray-400 flex items-center py-[2px] px-3  gap-3 rounded-md text-md">
                                <svg class="w-4 h-4 text-gray-400" aria-hidden="true" focusable="false"
                                    data-prefix="far" data-icon="edit" role="img"
                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                    <path fill="currentColor"
                                        d="M307 34.8c-11.5 5.1-19 16.6-19 29.2v64H176C78.8 128 0 206.8 0 304C0 417.3 81.5 467.9 100.2 478.1c2.5 1.4 5.3 1.9 8.1 1.9c10.9 0 19.7-8.9 19.7-19.7c0-7.5-4.3-14.4-9.8-19.5C108.8 431.9 96 414.4 96 384c0-53 43-96 96-96h96v64c0 12.6 7.4 24.1 19 29.2s25 3 34.4-5.4l160-144c6.7-6.1 10.6-14.7 10.6-23.8s-3.8-17.7-10.6-23.8l-160-144c-9.4-8.5-22.9-10.6-34.4-5.4z" />
                                </svg>
                                Forward
                            </button>
                            <button type="button" wire:click="action({sr_number:'{{ $sr_no }}'}, 'reply')"
                                class="text-gray-400 bg-gray-100 hover:bg-gray-200 border border-gray-400 flex items-center py-[2px] px-3  gap-3 rounded-md text-md">
                                <svg class="w-4 h-4 text-gray-400" aria-hidden="true" focusable="false"
                                    data-prefix="far" data-icon="edit" role="img"
                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                    <path fill="currentColor"
                                        d="M205 34.8c11.5 5.1 19 16.6 19 29.2v64H336c97.2 0 176 78.8 176 176c0 113.3-81.5 163.9-100.2 174.1c-2.5 1.4-5.3 1.9-8.1 1.9c-10.9 0-19.7-8.9-19.7-19.7c0-7.5 4.3-14.4 9.8-19.5c9.4-8.8 22.2-26.4 22.2-56.7c0-53-43-96-96-96H224v64c0 12.6-7.4 24.1-19 29.2s-25 3-34.4-5.4l-160-144C3.9 225.7 0 217.1 0 208s3.9-17.7 10.6-23.8l160-144c9.4-8.5 22.9-10.6 34.4-5.4z" />
                                </svg>
                                Reply
                            </button>
                        </div>
                        <div class="mt-4 ml-4 font-normal">
                            <h4 class="text-[13px] text-[#003399] underline cursor-pointer underline-offset-4"
                                wire:click="action({},'')">
                                See All
                            </h4>
                        </div>
                    </div>
                </div>
                <div class="grid grid-cols-1">
                    {{-- <div wire:poll> --}}
                    <div>
                        @php
                            $is_composed = [];
                            $message_id = [];
                            foreach ($emails as $i => $email_replies) {
                                $is_composed[$i] = $email_replies['type'];
                                $message_id[$i] = $email_replies['message_id'];
                            }
                        @endphp

                        @foreach ($emails as $i => $email_replies)
                            <div class="px-12 mt-4 mb-6 text-gray-500">
                                <div class="flex justify-between">
                                    <div class="grid grid-cols-1">
                                        <div class="text-lg font-medium">
                                            {{ $email_replies->type == 1 ? $email_replies->email_external : $email_replies->email_internal }}
                                        </div>
                                        <br>
                                        <div>{!! html_entity_decode($email_replies->body) !!}
                                        </div>
                                    </div>
                                    <div class="text-xs whitespace-nowrap">
                                        {{ $email_replies->created_at->diffForHumans() }}
                                        ({{ date('m/d/y : h:i a', strtotime($email_replies->created_at)) }})
                                    </div>
                                </div>
                                <div class="w-full mt-6 mb-2 border-t border-gray-300"></div>
                                @if (!in_array(3, $is_composed))
                                    @if (count($emails) - 2 == $i--)
                                        <div class="flex justify-between">
                                            <div>
                                                @livewire('crm.service-request.service-request-mgmt.generated-reply')
                                            </div>
                                            <div class="text-xs whitespace-nowrap">
                                                {{ $email_replies->created_at->diffForHumans() }}
                                                ({{ date('m/d/y : h:i a', strtotime($email_replies->created_at)) }})
                                            </div>
                                        </div>
                                        <div class="w-full mt-6 mb-2 border-t border-gray-300"></div>
                                    @endif
                                @endif
                            </div>
                        @endforeach
                    </div>
                </div>
                {{-- <div class="flex gap-6 px-12 mt-2 mb-4">
                    <button type="button" wire:click="action({sr_number:'{{ $sr_no }}'}, 'reply')"
                        class="text-gray-400 bg-gray-100 hover:bg-gray-200 border border-gray-400 flex items-center py-[2px] px-3  gap-3 rounded-md text-md">
                        <svg class="w-4 h-4 text-gray-400" aria-hidden="true" focusable="false" data-prefix="far"
                            data-icon="edit" role="img" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 576 512">
                            <path fill="currentColor"
                                d="M205 34.8c11.5 5.1 19 16.6 19 29.2v64H336c97.2 0 176 78.8 176 176c0 113.3-81.5 163.9-100.2 174.1c-2.5 1.4-5.3 1.9-8.1 1.9c-10.9 0-19.7-8.9-19.7-19.7c0-7.5 4.3-14.4 9.8-19.5c9.4-8.8 22.2-26.4 22.2-56.7c0-53-43-96-96-96H224v64c0 12.6-7.4 24.1-19 29.2s-25 3-34.4-5.4l-160-144C3.9 225.7 0 217.1 0 208s3.9-17.7 10.6-23.8l160-144c9.4-8.5 22.9-10.6 34.4-5.4z" />
                        </svg>
                        Reply
                    </button>
                    <button type="button"wire:click=""
                        class="text-gray-400 bg-gray-100 hover:bg-gray-200  border border-gray-400 flex items-center py-[2px] px-3  gap-3 rounded-md text-md">
                        <svg class="w-4 h-4 text-gray-400" aria-hidden="true" focusable="false" data-prefix="far"
                            data-icon="edit" role="img" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 576 512">
                            <path fill="currentColor"
                                d="M307 34.8c-11.5 5.1-19 16.6-19 29.2v64H176C78.8 128 0 206.8 0 304C0 417.3 81.5 467.9 100.2 478.1c2.5 1.4 5.3 1.9 8.1 1.9c10.9 0 19.7-8.9 19.7-19.7c0-7.5-4.3-14.4-9.8-19.5C108.8 431.9 96 414.4 96 384c0-53 43-96 96-96h96v64c0 12.6 7.4 24.1 19 29.2s25 3 34.4-5.4l160-144c6.7-6.1 10.6-14.7 10.6-23.8s-3.8-17.7-10.6-23.8l-160-144c-9.4-8.5-22.9-10.6-34.4-5.4z" />
                        </svg>
                        Forward
                    </button>
                </div> --}}
            </div>
        @else
            <div class="grid grid-cols-1 gap-4 p-2 mt-4 bg-white rounded-lg shadow-lg">
                <div class="flex justify-between">
                    <div class="px-12 mt-4 text-3xl font-medium text-blue">Messages</div>
                </div>

                <div class="flex gap-6 px-12 mt-2 mb-4">
                    <button type="button" wire:click="action({sr_number:'{{ $sr_no }}'}, 'compose-email')"
                        class="flex items-center gap-3 px-4 py-4 text-gray-400 bg-gray-100 border border-gray-400 rounded-md hover:bg-gray-200 text-md">
                        <svg class="w-5 h-5 text-gray-400" aria-hidden="true" focusable="false" data-prefix="far"
                            data-icon="edit" role="img" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 576 512">
                            <path fill="currentColor"
                                d="M410.3 231l11.3-11.3-33.9-33.9-62.1-62.1L291.7 89.8l-11.3 11.3-22.6 22.6L58.6 322.9c-10.4 10.4-18 23.3-22.2 37.4L1 480.7c-2.5 8.4-.2 17.5 6.1 23.7s15.3 8.5 23.7 6.1l120.3-35.4c14.1-4.2 27-11.8 37.4-22.2L387.7 253.7 410.3 231zM160 399.4l-9.1 22.7c-4 3.1-8.5 5.4-13.3 6.9L59.4 452l23-78.1c1.4-4.9 3.8-9.4 6.9-13.3l22.7-9.1v32c0 8.8 7.2 16 16 16h32zM362.7 18.7L348.3 33.2 325.7 55.8 314.3 67.1l33.9 33.9 62.1 62.1 33.9 33.9 11.3-11.3 22.6-22.6 14.5-14.5c25-25 25-65.5 0-90.5L453.3 18.7c-25-25-65.5-25-90.5 0zm-47.4 168l-144 144c-6.2 6.2-16.4 6.2-22.6 0s-6.2-16.4 0-22.6l144-144c6.2-6.2 16.4-6.2 22.6 0s6.2 16.4 0 22.6z" />
                        </svg>
                        Compose
                    </button>
                </div>
            </div>
        @endif
    </x-slot>
</x-form>
