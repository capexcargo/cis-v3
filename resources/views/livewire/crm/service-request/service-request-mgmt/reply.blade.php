<div x-data="{
    {{-- confirmation_modal: '{{ $confirmation_modal }}', --}}
}">
    <x-loading></x-loading>
    <form wire:submit.prevent="SendReply" autocomplete="off">
        <div class="space-y-4 px-2">
            <div class="text-xl font-semibold text-gray-800 -mt-4 mb-4">
                Reply
            </div>
            <div class="grid grid-cols-1 space-y-4">
                <div class="grid grid-cols-1s gap-4">
                    <div>
                        <x-label for="to" value="To" />
                        <x-input type="text" name="to" wire:model.defer='to' disabled>
                        </x-input>
                        <x-input-error for="to" />
                    </div>
                </div>
                <div class="grid grid-cols-1 gap-4">
                    <div>
                        <x-label for="body" value="Body" />
                        <textarea name="body" wire:model.defer='body' rows="20"
                            class="text-gray-600 block w-full mt-1 border border-gray-500 p-[5px] rounded-md shadow-sm focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50"></textarea>
                        <x-input-error for="body" />
                    </div>
                </div>
                <div class="flex justify-end gap-6 mt-12">
                    <button type="button" wire:click="$emit('close_modal', 'reply')"
                        class="px-10 py-2 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:text-white hover:bg-red-400">
                        Cancel
                    </button>
                    <button type="submit" class="px-10 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg">
                        Send
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
