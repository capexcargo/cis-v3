<!DOCTYPE html>
<html :class="{ 'theme-dark': dark }" x-data="data()" lang="en">
{{-- <html lang="{{ str_replace('_', '-', app()->getLocale()) }}"> --}}

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'CIS V3') }}</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    @livewireStyles

    <style>
        body {
            font-family: 'Poppins', sans-serif;
        }

        [x-cloak] {
            display: none !important;
        }

    </style>
</head>

<body>
    <div class="flex h-screen bg-white dark:bg-gray-900" :class="{ 'overflow-hidden': isSideMenuOpen }">
        <div class="flex flex-col flex-1">
            <main class="h-full overflow-y-auto bg-center bg-no-repeat bg-cover bg-login">
                <div class="fixed px-4 py-2">
                    <a href="/dashboard" class="">
                        <img class="h-16 " src="
                        /images/logo/capex-logo.png" alt="CaPEx_logo">
                    </a>
                </div>
                @yield('content')
            </main>
        </div>
    </div>
    <script src="{{ asset('js/init-alpine.js') }}"></script>
    @livewireScripts
    <script>
        window.addEventListener('swal', function(e) {
            Swal.fire(e.detail);
        });
    </script>
    @if (session('swal_default'))
        <script>
            Swal.fire({
                'icon': '{{ session('swal_default')['icon'] }}',
                'title': '{{ session('swal_default')['title'] }}',
                'text': '{{ session('swal_default')['text'] }}'
            })
        </script>
        {{ session()->flash('swal_default', null) }}
    @endif
    @stack('scripts')
</body>

</html>
