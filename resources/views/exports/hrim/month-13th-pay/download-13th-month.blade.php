<table>
    <thead>
        <tr>
            <th>Name</th>
            <th>Employee ID</th>
            <th>Employee Code</th>
            <th>Payroll Bank Account No.</th>
            <th>Date Hired</th>
            <th>Employment Status</th>
            <th>Job Rank</th>
            <th>Position</th>
            <th>Branch</th>
            <th>Basic Pay</th>
            <th>Leave w/o Pay</th>
            <th>Tardiness</th>
            <th>Undertime</th>
            <th>Absences</th>
            <th>13th Month Pay</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($month_13th_details as $month_13th_detail)
            @foreach ($month_13th_detail as $month_13th)
                <tr>
                    <td>
                        {{ $month_13th->first_name }}
                        {{ $month_13th->last_name }}
                    </td>
                    <td>
                        {{ $month_13th->employee_number ?? 'Not Set' }}
                    </td>
                    <td></td>
                    <td>{{ $month_13th->bank_account_number ?? 'Not Set' }}</td>
                    <td>
                        {{ $month_13th->hired_date ?? 'Not Set' }}
                    </td>
                    <td>
                        {{ $month_13th->emp_status ?? 'Not Set' }}
                    </td>
                    <td>
                        {{ $month_13th->job_rank ?? 'Not Set' }}
                    </td>
                    <td>
                        {{ $month_13th->position ?? 'Not Set' }}
                    </td>
                    <td>
                        {{ $month_13th->branch ?? 'Not Set' }}
                    </td>
                    <td>
                        {{ $month_13th->basic_pay / 2 }}
                    </td>
                    <td>
                        {{ round($month_13th->leaveswtpay * ($month_13th->basic_pay / 26), 2) }}
                    </td>
                    <td>
                        {{ $month_13th->tardiness }}
                    </td>
                    <td>
                        {{ $month_13th->undertime }}
                    </td>
                    <td>
                        {{ round($month_13th->absentcount * ($month_13th->basic_pay / 26), 2) }}
                    </td>
                    <td>
                        {{ number_format((($month_13th->basic_pay / 2) * 6 - ($month_13th->tardiness + $month_13th->undertime + round($month_13th->absentcount * ($month_13th->basic_pay / 26), 2))) / 6, 2) }}
                    </td>
                </tr>
            @endforeach
        @endforeach
    </tbody>
</table>
