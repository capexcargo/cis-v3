<table>
    <thead>
        <tr>
            <th>Name</th>
            <th>Employee ID</th>
            <th>Payroll Bank Account No.</th>
            <th>Date</th>
            <th>13th Month Pay</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($month_13th_details as $month_13th_detail)
            @foreach ($month_13th_detail as $month_13th)
                <tr>
                    <td>
                        {{ $month_13th->first_name }}
                        {{ $month_13th->last_name }}
                    </td>
                    <td>
                        {{ $month_13th->employee_number ?? 'Not Set' }}
                    </td>
                    <td>{{ $month_13th->bank_account_number ?? 'Not Set' }}</td>
                    <td>
                        {{ date('M. d, Y') }}
                    </td>
                    <td>
                        {{ number_format((($month_13th->basic_pay / 2) * 6 - ($month_13th->tardiness + $month_13th->undertime + round($month_13th->absentcount * ($month_13th->basic_pay / 26), 2))) / 6, 2) }}
                    </td>
                </tr>
            @endforeach
        @endforeach
    </tbody>
</table>
