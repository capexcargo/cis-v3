<table>
    <thead>
        <tr>
            <th>Name</th>
            <th>Employee ID</th>
            <th>Employee Code</th>
            <th>Payroll Bank Account No.</th>
            <th>Date Hired</th>
            <th>Employment Status</th>
            <th>Job Rank</th>
            <th>Position</th>
            <th>Branch</th>
            <th>Basic Pay</th>
            <th>COLA</th>
            <th>Gross Pay</th>
            <th>Holiday Pay</th>
            <th>ND Pay</th>
            <th>OT Pay</th>
            <th>Rest Day Pay</th>
            <th>Leave w/ Pay</th>
            <th>Leave w/o Pay</th>
            <th>Tardiness</th>
            <th>Undertime</th>
            <th>Absences</th>
            <th>Government Contributions</th>
            <th>Government Loans</th>
            <th>Withholding Tax</th>
            <th>Other Loans and Deductions</th>
            <th>Admin
                Adjustments</th>
            <th>DLB</th>
            <th>Net Pay</th>
        </tr>
    </thead>
    <tbody>

        @foreach ($payrolls as $payroll)
            <tr>
                <td>{{ $payroll->first_name }}
                    {{ $payroll->last_name }}</td>
                <td>

                </td>
                <td>

                </td>
                <td>
                    {{ $payroll->employee_number ?? 'Not Set' }}
                </td>
                <td>
                    {{ $payroll->hired_date ?? 'Not Set' }}
                </td>
                <td>
                    {{ $payroll->emp_status ?? 'Not Set' }}
                </td>
                <td>
                    {{ $payroll->job_rank ?? 'Not Set' }}
                </td>
                <td>
                    {{ $payroll->position ?? 'Not Set' }}
                </td>
                <td>
                    {{ $payroll->branch ?? 'Not Set' }}
                </td>
                <td>
                    {{ $payroll->basic_pay / 2 }}
                </td>
                <td>
                    {{ $payroll->cola / 2 }}
                </td>
                <td>
                    {{ $payroll->gross / 2 }}
                </td>
                <td>
                    {{ $payroll->holidayp }}
                </td>
                <td>
                    {{ $payroll->ndval }}
                </td>
                <td>
                    {{ $payroll->ot_pay }}
                </td>
                <td>
                    {{ round($payroll->restdaypays, 2) }}
                </td>
                <td>
                    {{ round($payroll->leaveswtpay * ($payroll->basic_pay / 26), 2) }}
                </td>
                <td>
                    {{ round($payroll->leaves * ($payroll->basic_pay / 26), 2) }}
                </td>
                <td>
                    {{ $payroll->tardiness }}
                </td>
                <td>
                    {{ $payroll->undertime }}
                </td>
                <td>
                    {{ round($payroll->absentcount * ($payroll->basic_pay / 26), 2) }}
                </td>
                <td>
                    {{ $payroll->cut_off == 1 ? $payroll->philhealth + $payroll->pagibig : 0 }}
                </td>
                <td>
                    {{ $payroll->loansgovt ?? 'Not Set' }}
                </td>
                <td>
                    0.00
                </td>
                <td>
                    {{ $payroll->loans ?? 'Not Set' }}
                </td>
                <td>
                    {{ $payroll->aadpay ?? 'Not Set' }}
                </td>
                <td>
                    {{ $payroll->dlbpay ?? 'Not Set' }}
                </td>
                <td>
                    {{ $payroll->gross / 2 +
                        $payroll->ot_pay +
                        $payroll->holidayp +
                        $payroll->ndval +
                        round($payroll->restdaypays, 2) +
                        $payroll->dlbpay +
                        $payroll->aadpay +
                        round($payroll->leaveswtpay * ($payroll->basic_pay / 26), 2) -
                        (round($payroll->leaves * ($payroll->basic_pay / 26), 2) +
                            $payroll->tardiness +
                            $payroll->undertime +
                            (round($payroll->absentcount * ($payroll->basic_pay / 26), 2) +
                                ($payroll->cut_off == 1 ? $payroll->philhealth + $payroll->pagibig : $payroll->loansgovt)) +
                            $payroll->loans) }}
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
