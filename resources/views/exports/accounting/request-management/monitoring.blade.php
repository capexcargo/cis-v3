<table>
    <thead>
        <tr>
            <th>Reference No.</th>
            <th>Request Type</th>
            <th>Payee</th>
            <th>Description</th>
            <th>Branch</th>
            <th>Date Needed</th>
            <th>Opex Type</th>
            <th>Amount</th>
            <th>Requested By</th>
            <th>Status</th>
            <th>Created At</th>
        </tr>
    </thead>
    <tbody>

        @foreach ($request_for_payments as $request_for_payment)
            <tr>
                <td>{{ $request_for_payment->reference_id }}</td>
                <td>
                    {{ $request_for_payment->type->display ?? 'Not Set' }}
                </td>
                <td>
                    {{ $request_for_payment->payee->company ?? 'Not Set' }}
                </td>
                <td>
                    {{ $request_for_payment->description ?? 'Not Set' }}
                </td>
                <td>
                    {{ $request_for_payment->branch->display ?? 'Not Set' }}
                </td>
                <td>
                    {{ $request_for_payment->date_needed ?? 'Not Set' }}
                </td>
                <td>
                    {{ $request_for_payment->opex->name ?? 'Not Set' }}
                </td>
                <td>
                    {{ $request_for_payment->amount }}
                </td>
                <td>
                    {{ $request_for_payment->user->name ?? 'Not Set' }}
                </td>
                <td>
                    <span>{{ $request_for_payment->status->display ?? 'Not Set' }}</span>
                </td>
                <td>
                    {{ date('M. d, Y', strtotime($request_for_payment->created_at)) }}
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
