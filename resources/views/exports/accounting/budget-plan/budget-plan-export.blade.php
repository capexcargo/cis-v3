
<table class="relative min-w-full font-medium divide-y-2">
                            <thead class="text-xs text-gray-700 capitalize bg-white border-0 cursor-pointer">
                                <tr>
                                    <th
                                        class="text-left sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[10rem] whitespace-nowrap">
                                        Division</th>
                                    <th
                                        class="text-left sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[10rem] whitespace-nowrap">
                                        Account Code
                                    </th>
                                    <th
                                        class="text-left sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[10rem] whitespace-nowrap">
                                        Total Approved
                                    </th>
                                    <th
                                        class="text-left sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[10rem] whitespace-nowrap">
                                        Total Availed
                                    </th>
                                    <th
                                        class="text-left sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[10rem] whitespace-nowrap">
                                        Total Balance
                                    </th>
                                    <th
                                        class="text-left sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[10rem] whitespace-nowrap">
                                        Opex</th>
                                    <th
                                        class="text-left sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[10rem] whitespace-nowrap">
                                        Year</th>

                                    <th
                                        class="sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">
                                        January</th>
                                    <th
                                        class="sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">
                                        February</th>
                                    <th
                                        class="sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">
                                        March</th>
                                    <th
                                        class="sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">
                                        April</th>
                                    <th
                                        class="sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">
                                        May
                                    </th>
                                    <th
                                        class="sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">
                                        June
                                    </th>
                                    <th
                                        class="sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">
                                        July
                                    </th>
                                    <th
                                        class="sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">
                                        August</th>
                                    <th
                                        class="sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">
                                        September</th>
                                    <th
                                        class="sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">
                                        October</th>
                                    <th
                                        class="sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">
                                        November</th>
                                    <th
                                        class="sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">
                                        December</th>
                                    <th class="sticky top-0 p-3 font-medium bg-white whitespace-nowrap">
                                        Created At</th>
                                    <th class="sticky top-0 p-3 font-medium bg-white whitespace-nowrap">
                                        Action</th>
                                </tr>
                            </thead>
                            <tbody class="text-sm text-gray-800 bg-gray-100 border-0 divide-y-4 divide-white">
                                <tr class="text-xs bg-white">
                                    <td class="p-3 whitespace-nowrap"></td>
                                    <td class="p-3 whitespace-nowrap"></td>
                                    <td class="p-3 whitespace-nowrap"></td>
                                    <td class="p-3 whitespace-nowrap"></td>
                                    <td class="p-3 whitespace-nowrap"></td>
                                    <td class="p-3 whitespace-nowrap"></td>
                                    <td class="p-3 whitespace-nowrap"></td>
                                    <td class="p-3 whitespace-nowrap">
                                        <div class="flex items-center justify-between space-x-3">
                                            <div>Approved</div>
                                            <div>Availed</div>
                                            <div>Balanced</div>
                                        </div>
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <div class="flex items-center justify-between space-x-3">
                                            <div>Approved</div>
                                            <div>Availed</div>
                                            <div>Balanced</div>
                                        </div>
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <div class="flex items-center justify-between space-x-3">
                                            <div>Approved</div>
                                            <div>Availed</div>
                                            <div>Balanced</div>
                                        </div>
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <div class="flex items-center justify-between space-x-3">
                                            <div>Approved</div>
                                            <div>Availed</div>
                                            <div>Balanced</div>
                                        </div>
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <div class="flex items-center justify-between space-x-3">
                                            <div>Approved</div>
                                            <div>Availed</div>
                                            <div>Balanced</div>
                                        </div>
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <div class="flex items-center justify-between space-x-3">
                                            <div>Approved</div>
                                            <div>Availed</div>
                                            <div>Balanced</div>
                                        </div>
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <div class="flex items-center justify-between space-x-3">
                                            <div>Approved</div>
                                            <div>Availed</div>
                                            <div>Balanced</div>
                                        </div>
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <div class="flex items-center justify-between space-x-3">
                                            <div>Approved</div>
                                            <div>Availed</div>
                                            <div>Balanced</div>
                                        </div>
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <div class="flex items-center justify-between space-x-3">
                                            <div>Approved</div>
                                            <div>Availed</div>
                                            <div>Balanced</div>
                                        </div>
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <div class="flex items-center justify-between space-x-3">
                                            <div>Approved</div>
                                            <div>Availed</div>
                                            <div>Balanced</div>
                                        </div>
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <div class="flex items-center justify-between space-x-3">
                                            <div>Approved</div>
                                            <div>Availed</div>
                                            <div>Balanced</div>
                                        </div>
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <div class="flex items-center justify-between space-x-3">
                                            <div>Approved</div>
                                            <div>Availed</div>
                                            <div>Balanced </div>
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                                @foreach ($divisions as $division)
                                    @php
                                        $div_approved = $division->budget_plan_sum_total_amount + $division->transfer_budget_to_sum_amount - $division->transfer_budget_from_sum_amount;
                                        $div_availed = $division->availment_sum_amount;
                                        $div_persent_availed = $div_approved && $div_availed ? ($div_availed / $div_approved) * 100 : 0;
                                    @endphp
                                    <tr
                                        class="border-0 cursor-pointer bg-[#003399] text-white hover:text-white hover:bg-[#4068b8]">
                                        <td class="p-3 whitespace-nowrap">
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            Total
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ number_format($div_approved, 2) }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ number_format($div_availed, 2) . '(' . round($div_persent_availed, 2) . ' %)' }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ number_format($div_approved - $div_availed, 2) . '(' . round(100 - $div_persent_availed, 2) . ' %)' }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            @php
                                                $div_january_approved =
                                                    $division->budget_plan_sum_january +
                                                    ($division->transferBudgetTo
                                                        ? $division
                                                            ->transferBudgetTo()
                                                            ->where('month_to', 'january')
                                                            ->sum('amount')
                                                        : 0) -
                                                    ($division->transferBudgetFrom
                                                        ? $division
                                                            ->transferBudgetFrom()
                                                            ->where('month_from', 'january')
                                                            ->sum('amount')
                                                        : 0);
                                                $div_january_availed = $division->availment
                                                    ? $division
                                                        ->availment()
                                                        ->where('month', 'january')
                                                        ->sum('amount')
                                                    : 0;
                                                
                                                $div_persent_january_availed = $div_january_availed && $div_january_approved ? ($div_january_availed / $div_january_approved) * 100 : 0;
                                            @endphp
                                            <div class="flex items-center justify-between space-x-3">
                                                <div>
                                                    {{ number_format($div_january_approved, 2) }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_january_availed, 2) . '(' . round($div_persent_january_availed, 2) . ' %)' }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_january_approved - $div_january_availed, 2) . '(' . round(100 - $div_persent_january_availed, 2) . ' %)' }}
                                                </div>
                                            </div>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            @php
                                                $div_february_approved =
                                                    $division->budget_plan_sum_february +
                                                    ($division->transferBudgetTo
                                                        ? $division
                                                            ->transferBudgetTo()
                                                            ->where('month_to', 'february')
                                                            ->sum('amount')
                                                        : 0) -
                                                    ($division->transferBudgetFrom
                                                        ? $division
                                                            ->transferBudgetFrom()
                                                            ->where('month_from', 'february')
                                                            ->sum('amount')
                                                        : 0);
                                                $div_february_availed = $division->availment
                                                    ? $division
                                                        ->availment()
                                                        ->where('month', 'february')
                                                        ->sum('amount')
                                                    : 0;
                                                
                                                $div_persent_february_availed = $div_february_availed && $div_february_approved ? ($div_february_availed / $div_february_approved) * 100 : 0;
                                            @endphp
                                            <div class="flex items-center justify-between space-x-3">
                                                <div>
                                                    {{ number_format($div_february_approved, 2) }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_february_availed, 2) . '(' . round($div_persent_february_availed, 2) . ' %)' }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_february_approved - $div_february_availed, 2) . '(' . round(100 - $div_persent_february_availed, 2) . ' %)' }}
                                                </div>
                                            </div>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            @php
                                                $div_march_approved =
                                                    $division->budget_plan_sum_march +
                                                    ($division->transferBudgetTo
                                                        ? $division
                                                            ->transferBudgetTo()
                                                            ->where('month_to', 'march')
                                                            ->sum('amount')
                                                        : 0) -
                                                    ($division->transferBudgetFrom
                                                        ? $division
                                                            ->transferBudgetFrom()
                                                            ->where('month_from', 'march')
                                                            ->sum('amount')
                                                        : 0);
                                                $div_march_availed = $division->availment
                                                    ? $division
                                                        ->availment()
                                                        ->where('month', 'march')
                                                        ->sum('amount')
                                                    : 0;
                                                
                                                $div_persent_march_availed = $div_march_availed && $div_march_approved ? ($div_march_availed / $div_march_approved) * 100 : 0;
                                            @endphp
                                            <div class="flex items-center justify-between space-x-3">
                                                <div>
                                                    {{ number_format($div_march_approved, 2) }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_march_availed, 2) . '(' . round($div_persent_march_availed, 2) . ' %)' }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_march_approved - $div_march_availed, 2) . '(' . round(100 - $div_persent_march_availed, 2) . ' %)' }}
                                                </div>
                                            </div>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            @php
                                                $div_april_approved =
                                                    $division->budget_plan_sum_april +
                                                    ($division->transferBudgetTo
                                                        ? $division
                                                            ->transferBudgetTo()
                                                            ->where('month_to', 'april')
                                                            ->sum('amount')
                                                        : 0) -
                                                    ($division->transferBudgetFrom
                                                        ? $division
                                                            ->transferBudgetFrom()
                                                            ->where('month_from', 'april')
                                                            ->sum('amount')
                                                        : 0);
                                                $div_april_availed = $division->availment
                                                    ? $division
                                                        ->availment()
                                                        ->where('month', 'april')
                                                        ->sum('amount')
                                                    : 0;
                                                
                                                $div_persent_april_availed = $div_april_availed && $div_april_approved ? ($div_april_availed / $div_april_approved) * 100 : 0;
                                            @endphp
                                            <div class="flex items-center justify-between space-x-3">
                                                <div>
                                                    {{ number_format($div_april_approved, 2) }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_april_availed, 2) . '(' . round($div_persent_april_availed, 2) . ' %)' }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_april_approved - $div_april_availed, 2) . '(' . round(100 - $div_persent_april_availed, 2) . ' %)' }}
                                                </div>
                                            </div>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            @php
                                                $div_may_approved =
                                                    $division->budget_plan_sum_may +
                                                    ($division->transferBudgetTo
                                                        ? $division
                                                            ->transferBudgetTo()
                                                            ->where('month_to', 'may')
                                                            ->sum('amount')
                                                        : 0) -
                                                    ($division->transferBudgetFrom
                                                        ? $division
                                                            ->transferBudgetFrom()
                                                            ->where('month_from', 'may')
                                                            ->sum('amount')
                                                        : 0);
                                                $div_may_availed = $division->availment
                                                    ? $division
                                                        ->availment()
                                                        ->where('month', 'may')
                                                        ->sum('amount')
                                                    : 0;
                                                
                                                $div_persent_may_availed = $div_may_availed && $div_may_approved ? ($div_may_availed / $div_may_approved) * 100 : 0;
                                            @endphp
                                            <div class="flex items-center justify-between space-x-3">
                                                <div>
                                                    {{ number_format($div_may_approved, 2) }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_may_availed, 2) . '(' . round($div_persent_may_availed, 2) . ' %)' }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_may_approved - $div_may_availed, 2) . '(' . round(100 - $div_persent_may_availed, 2) . ' %)' }}
                                                </div>
                                            </div>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            @php
                                                $div_june_approved =
                                                    $division->budget_plan_sum_june +
                                                    ($division->transferBudgetTo
                                                        ? $division
                                                            ->transferBudgetTo()
                                                            ->where('month_to', 'june')
                                                            ->sum('amount')
                                                        : 0) -
                                                    ($division->transferBudgetFrom
                                                        ? $division
                                                            ->transferBudgetFrom()
                                                            ->where('month_from', 'june')
                                                            ->sum('amount')
                                                        : 0);
                                                $div_june_availed = $division->availment
                                                    ? $division
                                                        ->availment()
                                                        ->where('month', 'june')
                                                        ->sum('amount')
                                                    : 0;
                                                
                                                $div_persent_june_availed = $div_june_availed && $div_june_approved ? ($div_june_availed / $div_june_approved) * 100 : 0;
                                            @endphp
                                            <div class="flex items-center justify-between space-x-3">
                                                <div>
                                                    {{ number_format($div_june_approved, 2) }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_june_availed, 2) . '(' . round($div_persent_june_availed, 2) . ' %)' }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_june_approved - $div_june_availed, 2) . '(' . round(100 - $div_persent_june_availed, 2) . ' %)' }}
                                                </div>
                                            </div>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            @php
                                                $div_july_approved =
                                                    $division->budget_plan_sum_july +
                                                    ($division->transferBudgetTo
                                                        ? $division
                                                            ->transferBudgetTo()
                                                            ->where('month_to', 'july')
                                                            ->sum('amount')
                                                        : 0) -
                                                    ($division->transferBudgetFrom
                                                        ? $division
                                                            ->transferBudgetFrom()
                                                            ->where('month_from', 'july')
                                                            ->sum('amount')
                                                        : 0);
                                                $div_july_availed = $division->availment
                                                    ? $division
                                                        ->availment()
                                                        ->where('month', 'july')
                                                        ->sum('amount')
                                                    : 0;
                                                
                                                $div_persent_july_availed = $div_july_availed && $div_july_approved ? ($div_july_availed / $div_july_approved) * 100 : 0;
                                            @endphp
                                            <div class="flex items-center justify-between space-x-3">
                                                <div>
                                                    {{ number_format($div_july_approved, 2) }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_july_availed, 2) . '(' . round($div_persent_july_availed, 2) . ' %)' }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_july_approved - $div_july_availed, 2) . '(' . round(100 - $div_persent_july_availed, 2) . ' %)' }}
                                                </div>
                                            </div>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            @php
                                                $div_august_approved =
                                                    $division->budget_plan_sum_august +
                                                    ($division->transferBudgetTo
                                                        ? $division
                                                            ->transferBudgetTo()
                                                            ->where('month_to', 'august')
                                                            ->sum('amount')
                                                        : 0) -
                                                    ($division->transferBudgetFrom
                                                        ? $division
                                                            ->transferBudgetFrom()
                                                            ->where('month_from', 'august')
                                                            ->sum('amount')
                                                        : 0);
                                                $div_august_availed = $division->availment
                                                    ? $division
                                                        ->availment()
                                                        ->where('month', 'august')
                                                        ->sum('amount')
                                                    : 0;
                                                
                                                $div_persent_august_availed = $div_august_availed && $div_august_approved ? ($div_august_availed / $div_august_approved) * 100 : 0;
                                            @endphp
                                            <div class="flex items-center justify-between space-x-3">
                                                <div>
                                                    {{ number_format($div_august_approved, 2) }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_august_availed, 2) . '(' . round($div_persent_august_availed, 2) . ' %)' }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_august_approved - $div_august_availed, 2) . '(' . round(100 - $div_persent_august_availed, 2) . ' %)' }}
                                                </div>
                                            </div>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            @php
                                                $div_september_approved =
                                                    $division->budget_plan_sum_september +
                                                    ($division->transferBudgetTo
                                                        ? $division
                                                            ->transferBudgetTo()
                                                            ->where('month_to', 'september')
                                                            ->sum('amount')
                                                        : 0) -
                                                    ($division->transferBudgetFrom
                                                        ? $division
                                                            ->transferBudgetFrom()
                                                            ->where('month_from', 'september')
                                                            ->sum('amount')
                                                        : 0);
                                                $div_september_availed = $division->availment
                                                    ? $division
                                                        ->availment()
                                                        ->where('month', 'september')
                                                        ->sum('amount')
                                                    : 0;
                                                
                                                $div_persent_september_availed = $div_september_availed && $div_september_approved ? ($div_september_availed / $div_september_approved) * 100 : 0;
                                            @endphp
                                            <div class="flex items-center justify-between space-x-3">
                                                <div>
                                                    {{ number_format($div_september_approved, 2) }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_september_availed, 2) . '(' . round($div_persent_september_availed, 2) . ' %)' }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_september_approved - $div_september_availed, 2) . '(' . round(100 - $div_persent_september_availed, 2) . ' %)' }}
                                                </div>
                                            </div>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            @php
                                                $div_october_approved =
                                                    $division->budget_plan_sum_october +
                                                    ($division->transferBudgetTo
                                                        ? $division
                                                            ->transferBudgetTo()
                                                            ->where('month_to', 'october')
                                                            ->sum('amount')
                                                        : 0) -
                                                    ($division->transferBudgetFrom
                                                        ? $division
                                                            ->transferBudgetFrom()
                                                            ->where('month_from', 'october')
                                                            ->sum('amount')
                                                        : 0);
                                                $div_october_availed = $division->availment
                                                    ? $division
                                                        ->availment()
                                                        ->where('month', 'october')
                                                        ->sum('amount')
                                                    : 0;
                                                
                                                $div_persent_october_availed = $div_october_availed && $div_october_approved ? ($div_october_availed / $div_october_approved) * 100 : 0;
                                            @endphp
                                            <div class="flex items-center justify-between space-x-3">
                                                <div>
                                                    {{ number_format($div_october_approved, 2) }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_october_availed, 2) . '(' . round($div_persent_october_availed, 2) . ' %)' }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_october_approved - $div_october_availed, 2) . '(' . round(100 - $div_persent_october_availed, 2) . ' %)' }}
                                                </div>
                                            </div>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            @php
                                                $div_november_approved =
                                                    $division->budget_plan_sum_november +
                                                    ($division->transferBudgetTo
                                                        ? $division
                                                            ->transferBudgetTo()
                                                            ->where('month_to', 'november')
                                                            ->sum('amount')
                                                        : 0) -
                                                    ($division->transferBudgetFrom
                                                        ? $division
                                                            ->transferBudgetFrom()
                                                            ->where('month_from', 'november')
                                                            ->sum('amount')
                                                        : 0);
                                                $div_november_availed = $division->availment
                                                    ? $division
                                                        ->availment()
                                                        ->where('month', 'november')
                                                        ->sum('amount')
                                                    : 0;
                                                
                                                $div_persent_november_availed = $div_november_availed && $div_november_approved ? ($div_november_availed / $div_november_approved) * 100 : 0;
                                            @endphp
                                            <div class="flex items-center justify-between space-x-3">
                                                <div>
                                                    {{ number_format($div_november_approved, 2) }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_november_availed, 2) . '(' . round($div_persent_november_availed, 2) . ' %)' }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_november_approved - $div_november_availed, 2) . '(' . round(100 - $div_persent_november_availed, 2) . ' %)' }}
                                                </div>
                                            </div>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            @php
                                                $div_december_approved =
                                                    $division->budget_plan_sum_december +
                                                    ($division->transferBudgetTo
                                                        ? $division
                                                            ->transferBudgetTo()
                                                            ->where('month_to', 'december')
                                                            ->sum('amount')
                                                        : 0) -
                                                    ($division->transferBudgetFrom
                                                        ? $division
                                                            ->transferBudgetFrom()
                                                            ->where('month_from', 'december')
                                                            ->sum('amount')
                                                        : 0);
                                                $div_december_availed = $division->availment
                                                    ? $division
                                                        ->availment()
                                                        ->where('month', 'december')
                                                        ->sum('amount')
                                                    : 0;
                                                
                                                $div_persent_december_availed = $div_december_availed && $div_december_approved ? ($div_december_availed / $div_december_approved) * 100 : 0;
                                            @endphp
                                            <div class="flex items-center justify-between space-x-3">
                                                <div>
                                                    {{ number_format($div_december_approved, 2) }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_december_availed, 2) . '(' . round($div_persent_december_availed, 2) . ' %)' }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_december_approved - $div_december_availed, 2) . '(' . round(100 - $div_persent_december_availed, 2) . ' %)' }}
                                                </div>
                                            </div>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                        </td>
                                    </tr>
                                    @foreach ($division->budgetSource as $budget_source)
                                        @php
                                            $source_approved = $budget_source->budget_plan_sum_total_amount + $budget_source->transfer_budget_to_sum_amount - $budget_source->transfer_budget_from_sum_amount;
                                            $source_availed = $budget_source->availment_sum_amount;
                                            $source_persent_availed = $source_approved && $source_availed ? ($source_availed / $source_approved) * 100 : 0;
                                        @endphp
                                        <tr
                                            class="border-0 cursor-pointer bg-[#003399] text-white hover:text-white hover:bg-[#4068b8]">
                                            <td class="p-3 whitespace-nowrap">
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                {{ $budget_source->name }}
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                {{ number_format($source_approved, 2) }}
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                {{ number_format($source_availed, 2) . '(' . round($source_persent_availed, 2) . ' %)' }}
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                {{ number_format($source_approved - $source_availed, 2) . '(' . round(100 - $source_persent_availed, 2) . ' %)' }}
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                @php
                                                    $source_january_approved =
                                                        $budget_source->budget_plan_sum_january +
                                                        ($budget_source->transferBudgetTo
                                                            ? $budget_source
                                                                ->transferBudgetTo()
                                                                ->where('month_to', 'january')
                                                                ->sum('amount')
                                                            : 0) -
                                                        ($budget_source->transferBudgetFrom
                                                            ? $budget_source
                                                                ->transferBudgetFrom()
                                                                ->where('month_from', 'january')
                                                                ->sum('amount')
                                                            : 0);
                                                    $source_january_availed = $budget_source->availment
                                                        ? $budget_source
                                                            ->availment()
                                                            ->where('month', 'january')
                                                            ->sum('amount')
                                                        : 0;
                                                    
                                                    $source_persent_january_availed = $source_january_availed && $source_january_approved ? ($source_january_availed / $source_january_approved) * 100 : 0;
                                                @endphp
                                                <div class="flex items-center justify-between space-x-3">
                                                    <div>
                                                        {{ number_format($source_january_approved, 2) }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_january_availed, 2) . '(' . round($source_persent_january_availed, 2) . ' %)' }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_january_approved - $source_january_availed, 2) . '(' . round(100 - $source_persent_january_availed, 2) . ' %)' }}
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                @php
                                                    $source_february_approved =
                                                        $budget_source->budget_plan_sum_february +
                                                        ($budget_source->transferBudgetTo
                                                            ? $budget_source
                                                                ->transferBudgetTo()
                                                                ->where('month_to', 'february')
                                                                ->sum('amount')
                                                            : 0) -
                                                        ($budget_source->transferBudgetFrom
                                                            ? $budget_source
                                                                ->transferBudgetFrom()
                                                                ->where('month_from', 'february')
                                                                ->sum('amount')
                                                            : 0);
                                                    $source_february_availed = $budget_source->availment
                                                        ? $budget_source
                                                            ->availment()
                                                            ->where('month', 'february')
                                                            ->sum('amount')
                                                        : 0;
                                                    
                                                    $source_persent_february_availed = $source_february_availed && $source_february_approved ? ($source_february_availed / $source_february_approved) * 100 : 0;
                                                @endphp
                                                <div class="flex items-center justify-between space-x-3">
                                                    <div>
                                                        {{ number_format($source_february_approved, 2) }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_february_availed, 2) . '(' . round($source_persent_february_availed, 2) . ' %)' }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_february_approved - $source_february_availed, 2) . '(' . round(100 - $source_persent_february_availed, 2) . ' %)' }}
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                @php
                                                    $source_march_approved =
                                                        $budget_source->budget_plan_sum_march +
                                                        ($budget_source->transferBudgetTo
                                                            ? $budget_source
                                                                ->transferBudgetTo()
                                                                ->where('month_to', 'march')
                                                                ->sum('amount')
                                                            : 0) -
                                                        ($budget_source->transferBudgetFrom
                                                            ? $budget_source
                                                                ->transferBudgetFrom()
                                                                ->where('month_from', 'march')
                                                                ->sum('amount')
                                                            : 0);
                                                    $source_march_availed = $budget_source->availment
                                                        ? $budget_source
                                                            ->availment()
                                                            ->where('month', 'march')
                                                            ->sum('amount')
                                                        : 0;
                                                    
                                                    $source_persent_march_availed = $source_march_availed && $source_march_approved ? ($source_march_availed / $source_march_approved) * 100 : 0;
                                                @endphp
                                                <div class="flex items-center justify-between space-x-3">
                                                    <div>
                                                        {{ number_format($source_march_approved, 2) }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_march_availed, 2) . '(' . round($source_persent_march_availed, 2) . ' %)' }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_march_approved - $source_march_availed, 2) . '(' . round(100 - $source_persent_march_availed, 2) . ' %)' }}
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                @php
                                                    $source_april_approved =
                                                        $budget_source->budget_plan_sum_april +
                                                        ($budget_source->transferBudgetTo
                                                            ? $budget_source
                                                                ->transferBudgetTo()
                                                                ->where('month_to', 'april')
                                                                ->sum('amount')
                                                            : 0) -
                                                        ($budget_source->transferBudgetFrom
                                                            ? $budget_source
                                                                ->transferBudgetFrom()
                                                                ->where('month_from', 'april')
                                                                ->sum('amount')
                                                            : 0);
                                                    $source_april_availed = $budget_source->availment
                                                        ? $budget_source
                                                            ->availment()
                                                            ->where('month', 'april')
                                                            ->sum('amount')
                                                        : 0;
                                                    
                                                    $source_persent_april_availed = $source_april_availed && $source_april_approved ? ($source_april_availed / $source_april_approved) * 100 : 0;
                                                @endphp
                                                <div class="flex items-center justify-between space-x-3">
                                                    <div>
                                                        {{ number_format($source_april_approved, 2) }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_april_availed, 2) . '(' . round($source_persent_april_availed, 2) . ' %)' }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_april_approved - $source_april_availed, 2) . '(' . round(100 - $source_persent_april_availed, 2) . ' %)' }}
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                @php
                                                    $source_may_approved =
                                                        $budget_source->budget_plan_sum_may +
                                                        ($budget_source->transferBudgetTo
                                                            ? $budget_source
                                                                ->transferBudgetTo()
                                                                ->where('month_to', 'may')
                                                                ->sum('amount')
                                                            : 0) -
                                                        ($budget_source->transferBudgetFrom
                                                            ? $budget_source
                                                                ->transferBudgetFrom()
                                                                ->where('month_from', 'may')
                                                                ->sum('amount')
                                                            : 0);
                                                    $source_may_availed = $budget_source->availment
                                                        ? $budget_source
                                                            ->availment()
                                                            ->where('month', 'may')
                                                            ->sum('amount')
                                                        : 0;
                                                    
                                                    $source_persent_may_availed = $source_may_availed && $source_may_approved ? ($source_may_availed / $source_may_approved) * 100 : 0;
                                                @endphp
                                                <div class="flex items-center justify-between space-x-3">
                                                    <div>
                                                        {{ number_format($source_may_approved, 2) }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_may_availed, 2) . '(' . round($source_persent_may_availed, 2) . ' %)' }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_may_approved - $source_may_availed, 2) . '(' . round(100 - $source_persent_may_availed, 2) . ' %)' }}
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                @php
                                                    $source_june_approved =
                                                        $budget_source->budget_plan_sum_june +
                                                        ($budget_source->transferBudgetTo
                                                            ? $budget_source
                                                                ->transferBudgetTo()
                                                                ->where('month_to', 'june')
                                                                ->sum('amount')
                                                            : 0) -
                                                        ($budget_source->transferBudgetFrom
                                                            ? $budget_source
                                                                ->transferBudgetFrom()
                                                                ->where('month_from', 'june')
                                                                ->sum('amount')
                                                            : 0);
                                                    $source_june_availed = $budget_source->availment
                                                        ? $budget_source
                                                            ->availment()
                                                            ->where('month', 'june')
                                                            ->sum('amount')
                                                        : 0;
                                                    
                                                    $source_persent_june_availed = $source_june_availed && $source_june_approved ? ($source_june_availed / $source_june_approved) * 100 : 0;
                                                @endphp
                                                <div class="flex items-center justify-between space-x-3">
                                                    <div>
                                                        {{ number_format($source_june_approved, 2) }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_june_availed, 2) . '(' . round($source_persent_june_availed, 2) . ' %)' }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_june_approved - $source_june_availed, 2) . '(' . round(100 - $source_persent_june_availed, 2) . ' %)' }}
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                @php
                                                    $source_july_approved =
                                                        $budget_source->budget_plan_sum_july +
                                                        ($budget_source->transferBudgetTo
                                                            ? $budget_source
                                                                ->transferBudgetTo()
                                                                ->where('month_to', 'july')
                                                                ->sum('amount')
                                                            : 0) -
                                                        ($budget_source->transferBudgetFrom
                                                            ? $budget_source
                                                                ->transferBudgetFrom()
                                                                ->where('month_from', 'july')
                                                                ->sum('amount')
                                                            : 0);
                                                    $source_july_availed = $budget_source->availment
                                                        ? $budget_source
                                                            ->availment()
                                                            ->where('month', 'july')
                                                            ->sum('amount')
                                                        : 0;
                                                    
                                                    $source_persent_july_availed = $source_july_availed && $source_july_approved ? ($source_july_availed / $source_july_approved) * 100 : 0;
                                                @endphp
                                                <div class="flex items-center justify-between space-x-3">
                                                    <div>
                                                        {{ number_format($source_july_approved, 2) }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_july_availed, 2) . '(' . round($source_persent_july_availed, 2) . ' %)' }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_july_approved - $source_july_availed, 2) . '(' . round(100 - $source_persent_july_availed, 2) . ' %)' }}
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                @php
                                                    $source_august_approved =
                                                        $budget_source->budget_plan_sum_august +
                                                        ($budget_source->transferBudgetTo
                                                            ? $budget_source
                                                                ->transferBudgetTo()
                                                                ->where('month_to', 'august')
                                                                ->sum('amount')
                                                            : 0) -
                                                        ($budget_source->transferBudgetFrom
                                                            ? $budget_source
                                                                ->transferBudgetFrom()
                                                                ->where('month_from', 'august')
                                                                ->sum('amount')
                                                            : 0);
                                                    $source_august_availed = $budget_source->availment
                                                        ? $budget_source
                                                            ->availment()
                                                            ->where('month', 'august')
                                                            ->sum('amount')
                                                        : 0;
                                                    
                                                    $source_persent_august_availed = $source_august_availed && $source_august_approved ? ($source_august_availed / $source_august_approved) * 100 : 0;
                                                @endphp
                                                <div class="flex items-center justify-between space-x-3">
                                                    <div>
                                                        {{ number_format($source_august_approved, 2) }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_august_availed, 2) . '(' . round($source_persent_august_availed, 2) . ' %)' }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_august_approved - $source_august_availed, 2) . '(' . round(100 - $source_persent_august_availed, 2) . ' %)' }}
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                @php
                                                    $source_september_approved =
                                                        $budget_source->budget_plan_sum_september +
                                                        ($budget_source->transferBudgetTo
                                                            ? $budget_source
                                                                ->transferBudgetTo()
                                                                ->where('month_to', 'september')
                                                                ->sum('amount')
                                                            : 0) -
                                                        ($budget_source->transferBudgetFrom
                                                            ? $budget_source
                                                                ->transferBudgetFrom()
                                                                ->where('month_from', 'september')
                                                                ->sum('amount')
                                                            : 0);
                                                    $source_september_availed = $budget_source->availment
                                                        ? $budget_source
                                                            ->availment()
                                                            ->where('month', 'september')
                                                            ->sum('amount')
                                                        : 0;
                                                    
                                                    $source_persent_september_availed = $source_september_availed && $source_september_approved ? ($source_september_availed / $source_september_approved) * 100 : 0;
                                                @endphp
                                                <div class="flex items-center justify-between space-x-3">
                                                    <div>
                                                        {{ number_format($source_september_approved, 2) }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_september_availed, 2) . '(' . round($source_persent_september_availed, 2) . ' %)' }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_september_approved - $source_september_availed, 2) . '(' . round(100 - $source_persent_september_availed, 2) . ' %)' }}
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                @php
                                                    $source_october_approved =
                                                        $budget_source->budget_plan_sum_october +
                                                        ($budget_source->transferBudgetTo
                                                            ? $budget_source
                                                                ->transferBudgetTo()
                                                                ->where('month_to', 'october')
                                                                ->sum('amount')
                                                            : 0) -
                                                        ($budget_source->transferBudgetFrom
                                                            ? $budget_source
                                                                ->transferBudgetFrom()
                                                                ->where('month_from', 'october')
                                                                ->sum('amount')
                                                            : 0);
                                                    $source_october_availed = $budget_source->availment
                                                        ? $budget_source
                                                            ->availment()
                                                            ->where('month', 'october')
                                                            ->sum('amount')
                                                        : 0;
                                                    
                                                    $source_persent_october_availed = $source_october_availed && $source_october_approved ? ($source_october_availed / $source_october_approved) * 100 : 0;
                                                @endphp
                                                <div class="flex items-center justify-between space-x-3">
                                                    <div>
                                                        {{ number_format($source_october_approved, 2) }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_october_availed, 2) . '(' . round($source_persent_october_availed, 2) . ' %)' }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_october_approved - $source_october_availed, 2) . '(' . round(100 - $source_persent_october_availed, 2) . ' %)' }}
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                @php
                                                    $source_november_approved =
                                                        $budget_source->budget_plan_sum_november +
                                                        ($budget_source->transferBudgetTo
                                                            ? $budget_source
                                                                ->transferBudgetTo()
                                                                ->where('month_to', 'november')
                                                                ->sum('amount')
                                                            : 0) -
                                                        ($budget_source->transferBudgetFrom
                                                            ? $budget_source
                                                                ->transferBudgetFrom()
                                                                ->where('month_from', 'november')
                                                                ->sum('amount')
                                                            : 0);
                                                    $source_november_availed = $budget_source->availment
                                                        ? $budget_source
                                                            ->availment()
                                                            ->where('month', 'november')
                                                            ->sum('amount')
                                                        : 0;
                                                    
                                                    $source_persent_november_availed = $source_november_availed && $source_november_approved ? ($source_november_availed / $source_november_approved) * 100 : 0;
                                                @endphp
                                                <div class="flex items-center justify-between space-x-3">
                                                    <div>
                                                        {{ number_format($source_november_approved, 2) }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_november_availed, 2) . '(' . round($source_persent_november_availed, 2) . ' %)' }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_november_approved - $source_november_availed, 2) . '(' . round(100 - $source_persent_november_availed, 2) . ' %)' }}
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                @php
                                                    $source_december_approved =
                                                        $budget_source->budget_plan_sum_december +
                                                        ($budget_source->transferBudgetTo
                                                            ? $budget_source
                                                                ->transferBudgetTo()
                                                                ->where('month_to', 'december')
                                                                ->sum('amount')
                                                            : 0) -
                                                        ($budget_source->transferBudgetFrom
                                                            ? $budget_source
                                                                ->transferBudgetFrom()
                                                                ->where('month_from', 'december')
                                                                ->sum('amount')
                                                            : 0);
                                                    $source_december_availed = $budget_source->availment
                                                        ? $budget_source
                                                            ->availment()
                                                            ->where('month', 'december')
                                                            ->sum('amount')
                                                        : 0;
                                                    
                                                    $source_persent_december_availed = $source_december_availed && $source_december_approved ? ($source_december_availed / $source_december_approved) * 100 : 0;
                                                @endphp
                                                <div class="flex items-center justify-between space-x-3">
                                                    <div>
                                                        {{ number_format($source_december_approved, 2) }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_december_availed, 2) . '(' . round($source_persent_december_availed, 2) . ' %)' }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_december_approved - $source_december_availed, 2) . '(' . round(100 - $source_persent_december_availed, 2) . ' %)' }}
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                            </td>
                                        </tr>
                                        @foreach ($budget_source->budgetChart as $budget_chart)
                                            @php
                                                $chart_approved = $budget_chart->budget_plan_sum_total_amount + $budget_chart->transfer_budget_to_sum_amount - $budget_chart->transfer_budget_from_sum_amount;
                                                $chart_availed = $budget_chart->availment_sum_amount;
                                                $chart_persent_availed = $chart_approved && $chart_availed ? ($chart_availed / $chart_approved) * 100 : 0;
                                            @endphp
                                            <tr
                                                class="text-sm  border-0 cursor-pointer bg-[#1656d5] text-white hover:text-white hover:bg-[#4068b8]">
                                                <td class="p-3 whitespace-nowrap">
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    {{ $budget_chart->name }}
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    {{ number_format($chart_approved, 2) }}
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    {{ number_format($chart_availed, 2) . '(' . round($chart_persent_availed, 2) . ' %)' }}
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    {{ number_format($chart_approved - $chart_availed, 2) . '(' . round(100 - $chart_persent_availed, 2) . ' %)' }}
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    @php
                                                        $chart_january_approved =
                                                            $budget_chart->budget_plan_sum_january +
                                                            ($budget_chart->transferBudgetTo
                                                                ? $budget_chart
                                                                    ->transferBudgetTo()
                                                                    ->where('month_to', 'january')
                                                                    ->sum('amount')
                                                                : 0) -
                                                            ($budget_chart->transferBudgetFrom
                                                                ? $budget_chart
                                                                    ->transferBudgetFrom()
                                                                    ->where('month_from', 'january')
                                                                    ->sum('amount')
                                                                : 0);
                                                        $chart_january_availed = $budget_chart->availment
                                                            ? $budget_chart
                                                                ->availment()
                                                                ->where('month', 'january')
                                                                ->sum('amount')
                                                            : 0;
                                                        
                                                        $chart_persent_january_availed = $chart_january_availed && $chart_january_approved ? ($chart_january_availed / $chart_january_approved) * 100 : 0;
                                                    @endphp
                                                    <div class="flex items-center justify-between space-x-3">
                                                        <div>
                                                            {{ number_format($chart_january_approved, 2) }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_january_availed, 2) . '(' . round($chart_persent_january_availed, 2) . ' %)' }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_january_approved - $chart_january_availed, 2) . '(' . round(100 - $chart_persent_january_availed, 2) . ' %)' }}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    @php
                                                        $chart_february_approved =
                                                            $budget_chart->budget_plan_sum_february +
                                                            ($budget_chart->transferBudgetTo
                                                                ? $budget_chart
                                                                    ->transferBudgetTo()
                                                                    ->where('month_to', 'february')
                                                                    ->sum('amount')
                                                                : 0) -
                                                            ($budget_chart->transferBudgetFrom
                                                                ? $budget_chart
                                                                    ->transferBudgetFrom()
                                                                    ->where('month_from', 'february')
                                                                    ->sum('amount')
                                                                : 0);
                                                        $chart_february_availed = $budget_chart->availment
                                                            ? $budget_chart
                                                                ->availment()
                                                                ->where('month', 'february')
                                                                ->sum('amount')
                                                            : 0;
                                                        
                                                        $chart_persent_february_availed = $chart_february_availed && $chart_february_approved ? ($chart_february_availed / $chart_february_approved) * 100 : 0;
                                                    @endphp
                                                    <div class="flex items-center justify-between space-x-3">
                                                        <div>
                                                            {{ number_format($chart_february_approved, 2) }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_february_availed, 2) . '(' . round($chart_persent_february_availed, 2) . ' %)' }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_february_approved - $chart_february_availed, 2) . '(' . round(100 - $chart_persent_february_availed, 2) . ' %)' }}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    @php
                                                        $chart_march_approved =
                                                            $budget_chart->budget_plan_sum_march +
                                                            ($budget_chart->transferBudgetTo
                                                                ? $budget_chart
                                                                    ->transferBudgetTo()
                                                                    ->where('month_to', 'march')
                                                                    ->sum('amount')
                                                                : 0) -
                                                            ($budget_chart->transferBudgetFrom
                                                                ? $budget_chart
                                                                    ->transferBudgetFrom()
                                                                    ->where('month_from', 'march')
                                                                    ->sum('amount')
                                                                : 0);
                                                        $chart_march_availed = $budget_chart->availment
                                                            ? $budget_chart
                                                                ->availment()
                                                                ->where('month', 'march')
                                                                ->sum('amount')
                                                            : 0;
                                                        
                                                        $chart_persent_march_availed = $chart_march_availed && $chart_march_approved ? ($chart_march_availed / $chart_march_approved) * 100 : 0;
                                                    @endphp
                                                    <div class="flex items-center justify-between space-x-3">
                                                        <div>
                                                            {{ number_format($chart_march_approved, 2) }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_march_availed, 2) . '(' . round($chart_persent_march_availed, 2) . ' %)' }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_march_approved - $chart_march_availed, 2) . '(' . round(100 - $chart_persent_march_availed, 2) . ' %)' }}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    @php
                                                        $chart_april_approved =
                                                            $budget_chart->budget_plan_sum_april +
                                                            ($budget_chart->transferBudgetTo
                                                                ? $budget_chart
                                                                    ->transferBudgetTo()
                                                                    ->where('month_to', 'april')
                                                                    ->sum('amount')
                                                                : 0) -
                                                            ($budget_chart->transferBudgetFrom
                                                                ? $budget_chart
                                                                    ->transferBudgetFrom()
                                                                    ->where('month_from', 'april')
                                                                    ->sum('amount')
                                                                : 0);
                                                        $chart_april_availed = $budget_chart->availment
                                                            ? $budget_chart
                                                                ->availment()
                                                                ->where('month', 'april')
                                                                ->sum('amount')
                                                            : 0;
                                                        
                                                        $chart_persent_april_availed = $chart_april_availed && $chart_april_approved ? ($chart_april_availed / $chart_april_approved) * 100 : 0;
                                                    @endphp
                                                    <div class="flex items-center justify-between space-x-3">
                                                        <div>
                                                            {{ number_format($chart_april_approved, 2) }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_april_availed, 2) . '(' . round($chart_persent_april_availed, 2) . ' %)' }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_april_approved - $chart_april_availed, 2) . '(' . round(100 - $chart_persent_april_availed, 2) . ' %)' }}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    @php
                                                        $chart_may_approved =
                                                            $budget_chart->budget_plan_sum_may +
                                                            ($budget_chart->transferBudgetTo
                                                                ? $budget_chart
                                                                    ->transferBudgetTo()
                                                                    ->where('month_to', 'may')
                                                                    ->sum('amount')
                                                                : 0) -
                                                            ($budget_chart->transferBudgetFrom
                                                                ? $budget_chart
                                                                    ->transferBudgetFrom()
                                                                    ->where('month_from', 'may')
                                                                    ->sum('amount')
                                                                : 0);
                                                        $chart_may_availed = $budget_chart->availment
                                                            ? $budget_chart
                                                                ->availment()
                                                                ->where('month', 'may')
                                                                ->sum('amount')
                                                            : 0;
                                                        
                                                        $chart_persent_may_availed = $chart_may_availed && $chart_may_approved ? ($chart_may_availed / $chart_may_approved) * 100 : 0;
                                                    @endphp
                                                    <div class="flex items-center justify-between space-x-3">
                                                        <div>
                                                            {{ number_format($chart_may_approved, 2) }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_may_availed, 2) . '(' . round($chart_persent_may_availed, 2) . ' %)' }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_may_approved - $chart_may_availed, 2) . '(' . round(100 - $chart_persent_may_availed, 2) . ' %)' }}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    @php
                                                        $chart_june_approved =
                                                            $budget_chart->budget_plan_sum_june +
                                                            ($budget_chart->transferBudgetTo
                                                                ? $budget_chart
                                                                    ->transferBudgetTo()
                                                                    ->where('month_to', 'june')
                                                                    ->sum('amount')
                                                                : 0) -
                                                            ($budget_chart->transferBudgetFrom
                                                                ? $budget_chart
                                                                    ->transferBudgetFrom()
                                                                    ->where('month_from', 'june')
                                                                    ->sum('amount')
                                                                : 0);
                                                        $chart_june_availed = $budget_chart->availment
                                                            ? $budget_chart
                                                                ->availment()
                                                                ->where('month', 'june')
                                                                ->sum('amount')
                                                            : 0;
                                                        
                                                        $chart_persent_june_availed = $chart_june_availed && $chart_june_approved ? ($chart_june_availed / $chart_june_approved) * 100 : 0;
                                                    @endphp
                                                    <div class="flex items-center justify-between space-x-3">
                                                        <div>
                                                            {{ number_format($chart_june_approved, 2) }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_june_availed, 2) . '(' . round($chart_persent_june_availed, 2) . ' %)' }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_june_approved - $chart_june_availed, 2) . '(' . round(100 - $chart_persent_june_availed, 2) . ' %)' }}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    @php
                                                        $chart_july_approved =
                                                            $budget_chart->budget_plan_sum_july +
                                                            ($budget_chart->transferBudgetTo
                                                                ? $budget_chart
                                                                    ->transferBudgetTo()
                                                                    ->where('month_to', 'july')
                                                                    ->sum('amount')
                                                                : 0) -
                                                            ($budget_chart->transferBudgetFrom
                                                                ? $budget_chart
                                                                    ->transferBudgetFrom()
                                                                    ->where('month_from', 'july')
                                                                    ->sum('amount')
                                                                : 0);
                                                        $chart_july_availed = $budget_chart->availment
                                                            ? $budget_chart
                                                                ->availment()
                                                                ->where('month', 'july')
                                                                ->sum('amount')
                                                            : 0;
                                                        
                                                        $chart_persent_july_availed = $chart_july_availed && $chart_july_approved ? ($chart_july_availed / $chart_july_approved) * 100 : 0;
                                                    @endphp
                                                    <div class="flex items-center justify-between space-x-3">
                                                        <div>
                                                            {{ number_format($chart_july_approved, 2) }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_july_availed, 2) . '(' . round($chart_persent_july_availed, 2) . ' %)' }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_july_approved - $chart_july_availed, 2) . '(' . round(100 - $chart_persent_july_availed, 2) . ' %)' }}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    @php
                                                        $chart_august_approved =
                                                            $budget_chart->budget_plan_sum_august +
                                                            ($budget_chart->transferBudgetTo
                                                                ? $budget_chart
                                                                    ->transferBudgetTo()
                                                                    ->where('month_to', 'august')
                                                                    ->sum('amount')
                                                                : 0) -
                                                            ($budget_chart->transferBudgetFrom
                                                                ? $budget_chart
                                                                    ->transferBudgetFrom()
                                                                    ->where('month_from', 'august')
                                                                    ->sum('amount')
                                                                : 0);
                                                        $chart_august_availed = $budget_chart->availment
                                                            ? $budget_chart
                                                                ->availment()
                                                                ->where('month', 'august')
                                                                ->sum('amount')
                                                            : 0;
                                                        
                                                        $chart_persent_august_availed = $chart_august_availed && $chart_august_approved ? ($chart_august_availed / $chart_august_approved) * 100 : 0;
                                                    @endphp
                                                    <div class="flex items-center justify-between space-x-3">
                                                        <div>
                                                            {{ number_format($chart_august_approved, 2) }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_august_availed, 2) . '(' . round($chart_persent_august_availed, 2) . ' %)' }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_august_approved - $chart_august_availed, 2) . '(' . round(100 - $chart_persent_august_availed, 2) . ' %)' }}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    @php
                                                        $chart_september_approved =
                                                            $budget_chart->budget_plan_sum_september +
                                                            ($budget_chart->transferBudgetTo
                                                                ? $budget_chart
                                                                    ->transferBudgetTo()
                                                                    ->where('month_to', 'september')
                                                                    ->sum('amount')
                                                                : 0) -
                                                            ($budget_chart->transferBudgetFrom
                                                                ? $budget_chart
                                                                    ->transferBudgetFrom()
                                                                    ->where('month_from', 'september')
                                                                    ->sum('amount')
                                                                : 0);
                                                        $chart_september_availed = $budget_chart->availment
                                                            ? $budget_chart
                                                                ->availment()
                                                                ->where('month', 'september')
                                                                ->sum('amount')
                                                            : 0;
                                                        
                                                        $chart_persent_september_availed = $chart_september_availed && $chart_september_approved ? ($chart_september_availed / $chart_september_approved) * 100 : 0;
                                                    @endphp
                                                    <div class="flex items-center justify-between space-x-3">
                                                        <div>
                                                            {{ number_format($chart_september_approved, 2) }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_september_availed, 2) . '(' . round($chart_persent_september_availed, 2) . ' %)' }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_september_approved - $chart_september_availed, 2) . '(' . round(100 - $chart_persent_september_availed, 2) . ' %)' }}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    @php
                                                        $chart_october_approved =
                                                            $budget_chart->budget_plan_sum_october +
                                                            ($budget_chart->transferBudgetTo
                                                                ? $budget_chart
                                                                    ->transferBudgetTo()
                                                                    ->where('month_to', 'october')
                                                                    ->sum('amount')
                                                                : 0) -
                                                            ($budget_chart->transferBudgetFrom
                                                                ? $budget_chart
                                                                    ->transferBudgetFrom()
                                                                    ->where('month_from', 'october')
                                                                    ->sum('amount')
                                                                : 0);
                                                        $chart_october_availed = $budget_chart->availment
                                                            ? $budget_chart
                                                                ->availment()
                                                                ->where('month', 'october')
                                                                ->sum('amount')
                                                            : 0;
                                                        
                                                        $chart_persent_october_availed = $chart_october_availed && $chart_october_approved ? ($chart_october_availed / $chart_october_approved) * 100 : 0;
                                                    @endphp
                                                    <div class="flex items-center justify-between space-x-3">
                                                        <div>
                                                            {{ number_format($chart_october_approved, 2) }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_october_availed, 2) . '(' . round($chart_persent_october_availed, 2) . ' %)' }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_october_approved - $chart_october_availed, 2) . '(' . round(100 - $chart_persent_october_availed, 2) . ' %)' }}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    @php
                                                        $chart_november_approved =
                                                            $budget_chart->budget_plan_sum_november +
                                                            ($budget_chart->transferBudgetTo
                                                                ? $budget_chart
                                                                    ->transferBudgetTo()
                                                                    ->where('month_to', 'november')
                                                                    ->sum('amount')
                                                                : 0) -
                                                            ($budget_chart->transferBudgetFrom
                                                                ? $budget_chart
                                                                    ->transferBudgetFrom()
                                                                    ->where('month_from', 'november')
                                                                    ->sum('amount')
                                                                : 0);
                                                        $chart_november_availed = $budget_chart->availment
                                                            ? $budget_chart
                                                                ->availment()
                                                                ->where('month', 'november')
                                                                ->sum('amount')
                                                            : 0;
                                                        
                                                        $chart_persent_november_availed = $chart_november_availed && $chart_november_approved ? ($chart_november_availed / $chart_november_approved) * 100 : 0;
                                                    @endphp
                                                    <div class="flex items-center justify-between space-x-3">
                                                        <div>
                                                            {{ number_format($chart_november_approved, 2) }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_november_availed, 2) . '(' . round($chart_persent_november_availed, 2) . ' %)' }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_november_approved - $chart_november_availed, 2) . '(' . round(100 - $chart_persent_november_availed, 2) . ' %)' }}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    @php
                                                        $chart_december_approved =
                                                            $budget_chart->budget_plan_sum_december +
                                                            ($budget_chart->transferBudgetTo
                                                                ? $budget_chart
                                                                    ->transferBudgetTo()
                                                                    ->where('month_to', 'december')
                                                                    ->sum('amount')
                                                                : 0) -
                                                            ($budget_chart->transferBudgetFrom
                                                                ? $budget_chart
                                                                    ->transferBudgetFrom()
                                                                    ->where('month_from', 'december')
                                                                    ->sum('amount')
                                                                : 0);
                                                        $chart_december_availed = $budget_chart->availment
                                                            ? $budget_chart
                                                                ->availment()
                                                                ->where('month', 'december')
                                                                ->sum('amount')
                                                            : 0;
                                                        
                                                        $chart_persent_december_availed = $chart_december_availed && $chart_december_approved ? ($chart_december_availed / $chart_december_approved) * 100 : 0;
                                                    @endphp
                                                    <div class="flex items-center justify-between space-x-3">
                                                        <div>
                                                            {{ number_format($chart_december_approved, 2) }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_december_availed, 2) . '(' . round($chart_persent_december_availed, 2) . ' %)' }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_december_approved - $chart_december_availed, 2) . '(' . round(100 - $chart_persent_december_availed, 2) . ' %)' }}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                </td>
                                            </tr>
                                            @foreach ($budget_chart->budgetPlan as $budget_plan)
                                                @php
                                                    $plan_approved = $budget_plan->total_amount + $budget_plan->transfer_budget_to_sum_amount - $budget_plan->transfer_budget_from_sum_amount;
                                                    $plan_availed = $budget_plan->availment_sum_amount;
                                                    $plan_persent_availed = $plan_approved && $plan_availed ? ($plan_availed / $plan_approved) * 100 : 0;
                                                @endphp
                                                <tr
                                                    class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                                    <td class="p-3 whitespace-nowrap">
                                                        {{ $division->name }}
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap">
                                                        {{ $budget_plan->item }}
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap">
                                                        {{ number_format($plan_approved, 2) }}
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap">
                                                        {{ number_format($plan_availed, 2) . '(' . round($plan_persent_availed, 2) . ' %)' }}
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap">
                                                        {{ number_format($plan_approved - $plan_availed, 2) . '(' . round(100 - $plan_persent_availed, 2) . ' %)' }}
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap">
                                                        {{ $budget_plan->opexType ? $budget_plan->opexType->name : '' }}
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap">
                                                        {{ $budget_plan->year }}
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap"
                                                        wire:click="action({'id': {{ $budget_plan->id }}, 'month': 'january'}, 'details')">
                                                        @php
                                                            $plan_january_approved =
                                                                $budget_plan->january +
                                                                ($budget_plan->transferBudgetTo
                                                                    ? $budget_plan
                                                                        ->transferBudgetTo()
                                                                        ->where('month_to', 'january')
                                                                        ->sum('amount')
                                                                    : 0) -
                                                                ($budget_plan->transferBudgetFrom
                                                                    ? $budget_plan
                                                                        ->transferBudgetFrom()
                                                                        ->where('month_from', 'january')
                                                                        ->sum('amount')
                                                                    : 0);
                                                            $plan_january_availed = $budget_plan->availment
                                                                ? $budget_plan
                                                                    ->availment()
                                                                    ->where('month', 'january')
                                                                    ->sum('amount')
                                                                : 0;
                                                            
                                                            $plan_persent_january_availed = $plan_january_availed && $plan_january_approved ? ($plan_january_availed / $plan_january_approved) * 100 : 0;
                                                        @endphp
                                                        <div class="flex items-center justify-between space-x-3">
                                                            <div>
                                                                {{ number_format($plan_january_approved, 2) }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_january_availed, 2) . '(' . round($plan_persent_january_availed, 2) . ' %)' }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_january_approved - $plan_january_availed, 2) . '(' . round(100 - $plan_persent_january_availed, 2) . ' %)' }}
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap"
                                                        wire:click="action({'id': {{ $budget_plan->id }}, 'month': 'february'}, 'details')">
                                                        @php
                                                            $plan_february_approved =
                                                                $budget_plan->february +
                                                                ($budget_plan->transferBudgetTo
                                                                    ? $budget_plan
                                                                        ->transferBudgetTo()
                                                                        ->where('month_to', 'february')
                                                                        ->sum('amount')
                                                                    : 0) -
                                                                ($budget_plan->transferBudgetFrom
                                                                    ? $budget_plan
                                                                        ->transferBudgetFrom()
                                                                        ->where('month_from', 'february')
                                                                        ->sum('amount')
                                                                    : 0);
                                                            $plan_february_availed = $budget_plan->availment
                                                                ? $budget_plan
                                                                    ->availment()
                                                                    ->where('month', 'february')
                                                                    ->sum('amount')
                                                                : 0;
                                                            $plan_persent_february_availed = $plan_february_availed && $plan_february_approved ? ($plan_february_availed / $plan_february_approved) * 100 : 0;
                                                        @endphp
                                                        <div class="flex items-center justify-between space-x-3">
                                                            <div>
                                                                {{ number_format($plan_february_approved, 2) }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_february_availed, 2) . '(' . round($plan_persent_february_availed, 2) . ' %)' }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_february_approved - $plan_february_availed, 2) . '(' . round(100 - $plan_persent_february_availed, 2) . ' %)' }}
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap"
                                                        wire:click="action({'id': {{ $budget_plan->id }}, 'month': 'march'}, 'details')">
                                                        @php
                                                            $plan_march_approved =
                                                                $budget_plan->march +
                                                                ($budget_plan->transferBudgetTo
                                                                    ? $budget_plan
                                                                        ->transferBudgetTo()
                                                                        ->where('month_to', 'march')
                                                                        ->sum('amount')
                                                                    : 0) -
                                                                ($budget_plan->transferBudgetFrom
                                                                    ? $budget_plan
                                                                        ->transferBudgetFrom()
                                                                        ->where('month_from', 'march')
                                                                        ->sum('amount')
                                                                    : 0);
                                                            $plan_march_availed = $budget_plan->availment
                                                                ? $budget_plan
                                                                    ->availment()
                                                                    ->where('month', 'march')
                                                                    ->sum('amount')
                                                                : 0;
                                                            $plan_persent_march_availed = $plan_march_availed && $plan_march_approved ? ($plan_march_availed / $plan_march_approved) * 100 : 0;
                                                        @endphp
                                                        <div class="flex items-center justify-between space-x-3">
                                                            <div>
                                                                {{ number_format($plan_march_approved, 2) }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_march_availed, 2) . '(' . round($plan_persent_march_availed, 2) . ' %)' }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_march_approved - $plan_march_availed, 2) . '(' . round(100 - $plan_persent_march_availed, 2) . ' %)' }}
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap"
                                                        wire:click="action({'id': {{ $budget_plan->id }}, 'month': 'april'}, 'details')">
                                                        @php
                                                            $plan_april_approved =
                                                                $budget_plan->april +
                                                                ($budget_plan->transferBudgetTo
                                                                    ? $budget_plan
                                                                        ->transferBudgetTo()
                                                                        ->where('month_to', 'april')
                                                                        ->sum('amount')
                                                                    : 0) -
                                                                ($budget_plan->transferBudgetFrom
                                                                    ? $budget_plan
                                                                        ->transferBudgetFrom()
                                                                        ->where('month_from', 'april')
                                                                        ->sum('amount')
                                                                    : 0);
                                                            $plan_april_availed = $budget_plan->availment
                                                                ? $budget_plan
                                                                    ->availment()
                                                                    ->where('month', 'april')
                                                                    ->sum('amount')
                                                                : 0;
                                                            $plan_persent_april_availed = $plan_april_availed && $plan_april_approved ? ($plan_april_availed / $plan_april_approved) * 100 : 0;
                                                        @endphp
                                                        <div class="flex items-center justify-between space-x-3">
                                                            <div>
                                                                {{ number_format($plan_april_approved, 2) }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_april_availed, 2) . '(' . round($plan_persent_april_availed, 2) . ' %)' }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_april_approved - $plan_april_availed, 2) . '(' . round(100 - $plan_persent_april_availed, 2) . ' %)' }}
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap"
                                                        wire:click="action({'id': {{ $budget_plan->id }}, 'month': 'may'}, 'details')">
                                                        @php
                                                            $plan_may_approved =
                                                                $budget_plan->may +
                                                                ($budget_plan->transferBudgetTo
                                                                    ? $budget_plan
                                                                        ->transferBudgetTo()
                                                                        ->where('month_to', 'may')
                                                                        ->sum('amount')
                                                                    : 0) -
                                                                ($budget_plan->transferBudgetFrom
                                                                    ? $budget_plan
                                                                        ->transferBudgetFrom()
                                                                        ->where('month_from', 'may')
                                                                        ->sum('amount')
                                                                    : 0);
                                                            $plan_may_availed = $budget_plan->availment
                                                                ? $budget_plan
                                                                    ->availment()
                                                                    ->where('month', 'may')
                                                                    ->sum('amount')
                                                                : 0;
                                                            $plan_persent_may_availed = $plan_may_availed && $plan_may_approved ? ($plan_may_availed / $plan_may_approved) * 100 : 0;
                                                        @endphp
                                                        <div class="flex items-center justify-between space-x-3">
                                                            <div>
                                                                {{ number_format($plan_may_approved, 2) }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_may_availed, 2) . '(' . round($plan_persent_may_availed, 2) . ' %)' }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_may_approved - $plan_may_availed, 2) . '(' . round(100 - $plan_persent_may_availed, 2) . ' %)' }}
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap"
                                                        wire:click="action({'id': {{ $budget_plan->id }}, 'month': 'june'}, 'details')">
                                                        @php
                                                            $plan_june_approved =
                                                                $budget_plan->june +
                                                                ($budget_plan->transferBudgetTo
                                                                    ? $budget_plan
                                                                        ->transferBudgetTo()
                                                                        ->where('month_to', 'june')
                                                                        ->sum('amount')
                                                                    : 0) -
                                                                ($budget_plan->transferBudgetFrom
                                                                    ? $budget_plan
                                                                        ->transferBudgetFrom()
                                                                        ->where('month_from', 'june')
                                                                        ->sum('amount')
                                                                    : 0);
                                                            $plan_june_availed = $budget_plan->availment
                                                                ? $budget_plan
                                                                    ->availment()
                                                                    ->where('month', 'june')
                                                                    ->sum('amount')
                                                                : 0;
                                                            $plan_persent_june_availed = $plan_june_availed && $plan_june_approved ? ($plan_june_availed / $plan_june_approved) * 100 : 0;
                                                        @endphp
                                                        <div class="flex items-center justify-between space-x-3">
                                                            <div>
                                                                {{ number_format($plan_june_approved, 2) }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_june_availed, 2) . '(' . round($plan_persent_june_availed, 2) . ' %)' }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_june_approved - $plan_june_availed, 2) . '(' . round(100 - $plan_persent_june_availed, 2) . ' %)' }}
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap"
                                                        wire:click="action({'id': {{ $budget_plan->id }}, 'month': 'july'}, 'details')">
                                                        @php
                                                            $plan_july_approved =
                                                                $budget_plan->july +
                                                                ($budget_plan->transferBudgetTo
                                                                    ? $budget_plan
                                                                        ->transferBudgetTo()
                                                                        ->where('month_to', 'july')
                                                                        ->sum('amount')
                                                                    : 0) -
                                                                ($budget_plan->transferBudgetFrom
                                                                    ? $budget_plan
                                                                        ->transferBudgetFrom()
                                                                        ->where('month_from', 'july')
                                                                        ->sum('amount')
                                                                    : 0);
                                                            $plan_july_availed = $budget_plan->availment
                                                                ? $budget_plan
                                                                    ->availment()
                                                                    ->where('month', 'july')
                                                                    ->sum('amount')
                                                                : 0;
                                                            $plan_persent_july_availed = $plan_july_availed && $plan_july_approved ? ($plan_july_availed / $plan_july_approved) * 100 : 0;
                                                        @endphp
                                                        <div class="flex items-center justify-between space-x-3">
                                                            <div>
                                                                {{ number_format($plan_july_approved, 2) }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_july_availed, 2) . '(' . round($plan_persent_july_availed, 2) . ' %)' }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_july_approved - $plan_july_availed, 2) . '(' . round(100 - $plan_persent_july_availed, 2) . ' %)' }}
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap"
                                                        wire:click="action({'id': {{ $budget_plan->id }}, 'month': 'august'}, 'details')">
                                                        @php
                                                            $plan_august_approved =
                                                                $budget_plan->august +
                                                                ($budget_plan->transferBudgetTo
                                                                    ? $budget_plan
                                                                        ->transferBudgetTo()
                                                                        ->where('month_to', 'august')
                                                                        ->sum('amount')
                                                                    : 0) -
                                                                ($budget_plan->transferBudgetFrom
                                                                    ? $budget_plan
                                                                        ->transferBudgetFrom()
                                                                        ->where('month_from', 'august')
                                                                        ->sum('amount')
                                                                    : 0);
                                                            $plan_august_availed = $budget_plan->availment
                                                                ? $budget_plan
                                                                    ->availment()
                                                                    ->where('month', 'august')
                                                                    ->sum('amount')
                                                                : 0;
                                                            $plan_persent_august_availed = $plan_august_availed && $plan_august_approved ? ($plan_august_availed / $plan_august_approved) * 100 : 0;
                                                        @endphp
                                                        <div class="flex items-center justify-between space-x-3">
                                                            <div>
                                                                {{ number_format($plan_august_approved, 2) }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_august_availed, 2) . '(' . round($plan_persent_august_availed, 2) . ' %)' }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_august_approved - $plan_august_availed, 2) . '(' . round(100 - $plan_persent_august_availed, 2) . ' %)' }}
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap"
                                                        wire:click="action({'id': {{ $budget_plan->id }}, 'month': 'september'}, 'details')">
                                                        @php
                                                            $plan_september_approved =
                                                                $budget_plan->september +
                                                                ($budget_plan->transferBudgetTo
                                                                    ? $budget_plan
                                                                        ->transferBudgetTo()
                                                                        ->where('month_to', 'september')
                                                                        ->sum('amount')
                                                                    : 0) -
                                                                ($budget_plan->transferBudgetFrom
                                                                    ? $budget_plan
                                                                        ->transferBudgetFrom()
                                                                        ->where('month_from', 'september')
                                                                        ->sum('amount')
                                                                    : 0);
                                                            $plan_september_availed = $budget_plan->availment
                                                                ? $budget_plan
                                                                    ->availment()
                                                                    ->where('month', 'september')
                                                                    ->sum('amount')
                                                                : 0;
                                                            $plan_persent_september_availed = $plan_september_availed && $plan_september_approved ? ($plan_september_availed / $plan_september_approved) * 100 : 0;
                                                        @endphp
                                                        <div class="flex items-center justify-between space-x-3">
                                                            <div>
                                                                {{ number_format($plan_september_approved, 2) }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_september_availed, 2) . '(' . round($plan_persent_september_availed, 2) . ' %)' }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_september_approved - $plan_september_availed, 2) . '(' . round(100 - $plan_persent_september_availed, 2) . ' %)' }}
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap"
                                                        wire:click="action({'id': {{ $budget_plan->id }}, 'month': 'october'}, 'details')">
                                                        @php
                                                            $plan_october_approved =
                                                                $budget_plan->october +
                                                                ($budget_plan->transferBudgetTo
                                                                    ? $budget_plan
                                                                        ->transferBudgetTo()
                                                                        ->where('month_to', 'october')
                                                                        ->sum('amount')
                                                                    : 0) -
                                                                ($budget_plan->transferBudgetFrom
                                                                    ? $budget_plan
                                                                        ->transferBudgetFrom()
                                                                        ->where('month_from', 'october')
                                                                        ->sum('amount')
                                                                    : 0);
                                                            $plan_october_availed = $budget_plan->availment
                                                                ? $budget_plan
                                                                    ->availment()
                                                                    ->where('month', 'october')
                                                                    ->sum('amount')
                                                                : 0;
                                                            $plan_persent_october_availed = $plan_october_availed && $plan_october_approved ? ($plan_october_availed / $plan_october_approved) * 100 : 0;
                                                        @endphp
                                                        <div class="flex items-center justify-between space-x-3">
                                                            <div>
                                                                {{ number_format($plan_october_approved, 2) }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_october_availed, 2) . '(' . round($plan_persent_october_availed, 2) . ' %)' }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_october_approved - $plan_october_availed, 2) . '(' . round(100 - $plan_persent_october_availed, 2) . ' %)' }}
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap"
                                                        wire:click="action({'id': {{ $budget_plan->id }}, 'month': 'november'}, 'details')">
                                                        @php
                                                            $plan_november_approved =
                                                                $budget_plan->november +
                                                                ($budget_plan->transferBudgetTo
                                                                    ? $budget_plan
                                                                        ->transferBudgetTo()
                                                                        ->where('month_to', 'november')
                                                                        ->sum('amount')
                                                                    : 0) -
                                                                ($budget_plan->transferBudgetFrom
                                                                    ? $budget_plan
                                                                        ->transferBudgetFrom()
                                                                        ->where('month_from', 'november')
                                                                        ->sum('amount')
                                                                    : 0);
                                                            $plan_november_availed = $budget_plan->availment
                                                                ? $budget_plan
                                                                    ->availment()
                                                                    ->where('month', 'november')
                                                                    ->sum('amount')
                                                                : 0;
                                                            $plan_persent_november_availed = $plan_november_availed && $plan_november_approved ? ($plan_november_availed / $plan_november_approved) * 100 : 0;
                                                        @endphp
                                                        <div class="flex items-center justify-between space-x-3">
                                                            <div>
                                                                {{ number_format($plan_november_approved, 2) }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_november_availed, 2) . '(' . round($plan_persent_november_availed, 2) . ' %)' }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_november_approved - $plan_november_availed, 2) . '(' . round(100 - $plan_persent_november_availed, 2) . ' %)' }}
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap"
                                                        wire:click="action({'id': {{ $budget_plan->id }}, 'month': 'december'}, 'details')">
                                                        @php
                                                            $plan_december_approved =
                                                                $budget_plan->december +
                                                                ($budget_plan->transferBudgetTo
                                                                    ? $budget_plan
                                                                        ->transferBudgetTo()
                                                                        ->where('month_to', 'december')
                                                                        ->sum('amount')
                                                                    : 0) -
                                                                ($budget_plan->transferBudgetFrom
                                                                    ? $budget_plan
                                                                        ->transferBudgetFrom()
                                                                        ->where('month_from', 'december')
                                                                        ->sum('amount')
                                                                    : 0);
                                                            $plan_december_availed = $budget_plan->availment
                                                                ? $budget_plan
                                                                    ->availment()
                                                                    ->where('month', 'december')
                                                                    ->sum('amount')
                                                                : 0;
                                                            $plan_persent_december_availed = $plan_december_availed && $plan_december_approved ? ($plan_december_availed / $plan_december_approved) * 100 : 0;
                                                        @endphp
                                                        <div class="flex items-center justify-between space-x-3">
                                                            <div>
                                                                {{ number_format($plan_december_approved, 2) }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_december_availed, 2) . '(' . round($plan_persent_december_availed, 2) . ' %)' }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_december_approved - $plan_december_availed, 2) . '(' . round(100 - $plan_persent_december_availed, 2) . ' %)' }}
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap">
                                                        {{ date('M/d/Y', strtotime($budget_plan->created_at)) }}
                                                    </td>
                                                    @can('accounting_budget_plan_action')
                                                        <td class="p-3 whitespace-nowrap">
                                                            
                                                        </td>
                                                    @endcan
                                                </tr>
                                            @endforeach
                                        @endforeach
                                    @endforeach
                                @endforeach
                            </tbody>
                        </table>