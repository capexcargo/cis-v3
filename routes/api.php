<?php

use App\Http\Controllers\Admin\AccountManagementController;
use App\Http\Controllers\Admin\ForgotPasswordController;
use App\Http\Controllers\API\Accounting\CheckVoucherController;
use App\Http\Controllers\API\LoginController;
use App\Http\Controllers\API\Accounting\LiquidationManagementController;
use App\Http\Controllers\API\Accounting\LoaderManagementController;
use App\Http\Controllers\API\Accounting\RequestManagementController;
use App\Http\Controllers\Crm\CustomerInformation\TwilioWebhookController;
use App\Http\Controllers\Crm\Sales\SalesController;
use App\Http\Controllers\Fims\Accounting\BudgetMgmtController;
use App\Http\Controllers\Fims\Accounting\PurchasingLoaController;
use App\Http\Controllers\Fims\DataManagement\AcctngSupplierController;
use App\Http\Controllers\Fims\DataManagement\BankNameController;
use App\Http\Controllers\Fims\DataManagement\ChartofAccountsController;
use App\Http\Controllers\Fims\DataManagement\DataManagementController;
use App\Http\Controllers\Fims\DataManagement\IndustryController;
use App\Http\Controllers\Fims\DataManagement\ItemCategoryController;
use App\Http\Controllers\Fims\DataManagement\ItemDescriptionController;
use App\Http\Controllers\Fims\DataManagement\ManPowerController;
use App\Http\Controllers\Fims\DataManagement\OpexCategoryController;
use App\Http\Controllers\Fims\DataManagement\PurposeController;
use App\Http\Controllers\Fims\DataManagement\ReasonForRejectionController;
use App\Http\Controllers\Fims\DataManagement\ServiceCategoryController;
use App\Http\Controllers\Fims\DataManagement\ServiceDescriptionController;
use App\Http\Controllers\Fims\DataManagement\SubAccountsController;
use App\Http\Controllers\Fims\DataManagement\UnitController;
use App\Http\Controllers\Fims\ServicePurchaseRequisition\PurchaseReqController;
use App\Http\Controllers\Fims\ServicePurchaseRequisition\ServiceReqController;
use App\Http\Controllers\Hrim\CompanyManagement\BulletinController;
use App\Http\Controllers\Hrim\CompanyManagement\CalendarController;
use App\Http\Controllers\Hrim\CompensationAndBenefits\PayslipsController;
use App\Http\Controllers\Hrim\EmployeeAttendance\DailyTimeRecordsController;
use App\Http\Controllers\Hrim\EmployeeAttendance\LeaveRecordsController;
use App\Http\Controllers\Hrim\EmployeeAttendance\OvertimeRecordsController;
use App\Http\Controllers\Hrim\EmployeeCompensationAndBenefits\LoansAndLedgerMonitoringController;
use App\Http\Controllers\Hrim\EmployeeTalentManagement\PerformanceEvaluationController;
use App\Http\Controllers\Hrim\TimeInAndOutController;
use App\Http\Controllers\Oims\OrderManagement\OrderManagementController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [LoginController::class, 'login']);

Route::post('webhooks/twilio', [TwilioWebhookController::class, 'handle']);
Route::post('missedcall', [TwilioWebhookController::class, 'handlereq']);
Route::post('missedcallnotif', [TwilioWebhookController::class, 'handlereqnotif']);
// Route::post('webhooks/twilio', 'TwilioWebhookController@handle');


Route::group(['prefix' => 'user', 'middleware' => 'auth:sanctum'], function () {
    Route::group(['prefix' => 'accounting'], function () {
        Route::group(['prefix' => 'approval-management'], function () {
            Route::group(['prefix' => 'request-management'], function () {
                Route::get('dashboard', [RequestManagementController::class, 'dashboard']);
                Route::get('', [RequestManagementController::class, 'index']);
                Route::get('{id}/show', [RequestManagementController::class, 'show']);
                Route::put('{id}/approved', [RequestManagementController::class, 'approved']);
            });

            Route::group(['prefix' => 'check-voucher'], function () {
                Route::get('', [CheckVoucherController::class, 'index']);
                Route::get('{id}/show', [CheckVoucherController::class, 'show']);
                Route::put('{id}/approved', [CheckVoucherController::class, 'approved']);
            });
        });

        Route::group(['prefix' => 'request-management'], function () {
            Route::get('dashboard', [RequestManagementController::class, 'dashboard']);
            Route::get('', [RequestManagementController::class, 'index']);
            Route::get('{id}/show', [RequestManagementController::class, 'show']);
            Route::post('{id}/attachment', [RequestManagementController::class, 'attachment']);
        });

        Route::group(['prefix' => 'check-voucher'], function () {
            Route::get('', [CheckVoucherController::class, 'index']);
            Route::put('{id}/confirmation', [CheckVoucherController::class, 'confirmation']);
        });

        Route::group(['prefix' => 'liquidation-management'], function () {

            Route::group(['prefix' => 'references'], function () {
                Route::group(['prefix' => 'request-for-payments'], function () {
                    Route::post('', [LiquidationManagementController::class, 'loadRequestForPaymentReference']);
                    Route::post('/{id}/show', [LiquidationManagementController::class, 'showRequestForPaymentReference']);
                });
            });

            Route::get('', [LiquidationManagementController::class, 'index']);
            Route::get('{id}/show', [LiquidationManagementController::class, 'show']);
            Route::post('create', [LiquidationManagementController::class, 'create']);
            Route::put('{id}/update', [LiquidationManagementController::class, 'update']);
        });

        Route::group(['prefix' => 'loader-management'], function () {
            Route::post('create', [LoaderManagementController::class, 'create']);
            Route::put('{id}/update', [LoaderManagementController::class, 'update']);
        });
    });

    Route::group(['prefix' => 'om'], function () {
        Route::group(['prefix' => 'booking'], function () {
            Route::post('create', [SalesController::class, 'createbooking']);
            Route::post('{id}/show', [SalesController::class, 'showbooking']);
            Route::put('{id}/update', [SalesController::class, 'updatebooking']);
            Route::put('{id}/bookForConfirm', [SalesController::class, 'updatebconf']);
            Route::put('{id}/bookConfirmed', [SalesController::class, 'updatebconfirmed']);
            Route::put('{id}/bookOngoing', [SalesController::class, 'updatebong']);
            Route::put('{id}/bookCompleted', [SalesController::class, 'updatebcomp']);
            Route::put('{id}/actualStartTravel', [OrderManagementController::class, 'updateactualStartTravel']);
            // Route::put('{id}/update', [OrderManagementController::class, 'updatebooking']);
            Route::get('address/{customer_no_2}', [SalesController::class, 'ShipperCreateAddress']);
        });
    });

    Route::group(['prefix' => 'om'], function () {
        Route::group(['prefix' => 'dispatch'], function () {
            Route::put('{id}/actualStartTravel', [OrderManagementController::class, 'updateactualStartTravel']);
            Route::put('{id}/actualEndTravel', [OrderManagementController::class, 'updateactualEndTravel']);
            Route::put('{id}/pickupConfirmedtime', [OrderManagementController::class, 'updatepickupConfirmedtime']);
            Route::put('{id}/pickupStartTime', [OrderManagementController::class, 'updatepickupStartTime']);
            Route::put('{id}/pickupEndTime', [OrderManagementController::class, 'updatepickupEndTime']);
            Route::put('{id}/deliveryStartTime', [OrderManagementController::class, 'updatedeliveryStartTime']);
            Route::put('{id}/deliveryEndTime', [OrderManagementController::class, 'updatedeliveryEndTime']);
        });
    });

    Route::group(['prefix' => 'om'], function () {
        Route::group(['prefix' => 'transactionentry'], function () {
            Route::post('{id}/transanctionEntry', [OrderManagementController::class, 'createTransactionEntry']);
        });
    });

    Route::group(['prefix' => 'fims'], function () {
        Route::group(['prefix' => 'budget-source-mgmt'], function () {
            Route::post('create', [DataManagementController::class, 'createBSM']);
            Route::put('{id}/update', [DataManagementController::class, 'updateBSM']);
            Route::post('{id}/toggleStatus', [DataManagementController::class, 'toggleStatus']);
            Route::get('index', [DataManagementController::class, 'index']);
        });
        Route::group(['prefix' => 'opex-category-mgmt'], function () {
            Route::post('create', [OpexCategoryController::class, 'createOC']);
            Route::put('{id}/update', [OpexCategoryController::class, 'updateOC']);
            Route::post('{id}/toggleStatus', [OpexCategoryController::class, 'toggleStatusOC']);
            Route::get('index', [OpexCategoryController::class, 'indexOC']);
        });
        Route::group(['prefix' => 'chart-of-accounts-mgmt'], function () {
            Route::post('create', [ChartofAccountsController::class, 'createCA']);
            Route::put('{id}/update', [ChartofAccountsController::class, 'updateCA']);
            Route::post('{id}/toggleStatus', [ChartofAccountsController::class, 'toggleStatusCA']);
            Route::get('index', [ChartofAccountsController::class, 'indexCA']);
        });
        Route::group(['prefix' => 'sub-accounts-mgmt'], function () {
            Route::post('create', [SubAccountsController::class, 'createSA']);
            Route::put('{id}/update', [SubAccountsController::class, 'updateSA']);
            Route::post('{id}/toggleStatus', [SubAccountsController::class, 'toggleStatusSA']);
            Route::get('index', [SubAccountsController::class, 'indexSA']);
        });
        Route::group(['prefix' => 'reason-for-rejection-mgmt'], function () {
            Route::post('create', [ReasonForRejectionController::class, 'createRR']);
            Route::put('{id}/update', [ReasonForRejectionController::class, 'updateRR']);
            Route::post('{id}/toggleStatus', [ReasonForRejectionController::class, 'toggleStatusRR']);
            Route::get('index', [ReasonForRejectionController::class, 'indexRR']);
        });
        Route::group(['prefix' => 'purpose-mgmt'], function () {
            Route::post('create', [PurposeController::class, 'createP']);
            Route::put('{id}/update', [PurposeController::class, 'updateP']);
            Route::post('{id}/toggleStatus', [PurposeController::class, 'toggleStatusP']);
            Route::get('index', [PurposeController::class, 'indexP']);
        });
        Route::group(['prefix' => 'unit-mgmt'], function () {
            Route::post('create', [UnitController::class, 'createU']);
            Route::put('{id}/update', [UnitController::class, 'updateU']);
            Route::post('{id}/toggleStatus', [UnitController::class, 'toggleStatusU']);
            Route::get('index', [UnitController::class, 'indexU']);
        });
        Route::group(['prefix' => 'item-category-mgmt'], function () {
            Route::post('create', [ItemCategoryController::class, 'createIC']);
            Route::put('{id}/update', [ItemCategoryController::class, 'updateIC']);
            Route::post('{id}/toggleStatus', [ItemCategoryController::class, 'toggleStatusIC']);
            Route::get('index', [ItemCategoryController::class, 'indexIC']);
            Route::get('getBudget', [ItemCategoryController::class, 'getBudgetIC']);
            Route::get('getOpex', [ItemCategoryController::class, 'getOpexIC']);
            Route::get('getCharts', [ItemCategoryController::class, 'getChartsIC']);
            Route::get('getSubAcc', [ItemCategoryController::class, 'getSubAccIC']);
        });
        Route::group(['prefix' => 'service-category-mgmt'], function () {
            Route::post('create', [ServiceCategoryController::class, 'createSC']);
            Route::put('{id}/update', [ServiceCategoryController::class, 'updateSC']);
            Route::post('{id}/toggleStatus', [ServiceCategoryController::class, 'toggleStatusSC']);
            Route::get('index', [ServiceCategoryController::class, 'indexSC']);
            Route::get('getBudget', [ServiceCategoryController::class, 'getBudgetSC']);
            Route::get('getOpex', [ServiceCategoryController::class, 'getOpexSC']);
            Route::get('getCharts', [ServiceCategoryController::class, 'getChartsSC']);
            Route::get('getSubAcc', [ServiceCategoryController::class, 'getSubAccSC']);
        });
        Route::group(['prefix' => 'item-description-mgmt'], function () {
            Route::post('create', [ItemDescriptionController::class, 'createID']);
            Route::put('{id}/update', [ItemDescriptionController::class, 'updateID']);
            Route::post('{id}/toggleStatus', [ItemDescriptionController::class, 'toggleStatusID']);
            Route::get('index', [ItemDescriptionController::class, 'indexID']);
            Route::get('getItemCateg', [ItemDescriptionController::class, 'getItemCategID']);
        });
        Route::group(['prefix' => 'service-description-mgmt'], function () {
            Route::post('create', [ServiceDescriptionController::class, 'createSD']);
            Route::put('{id}/update', [ServiceDescriptionController::class, 'updateSD']);
            Route::post('{id}/toggleStatus', [ServiceDescriptionController::class, 'toggleStatusSD']);
            Route::get('index', [ServiceDescriptionController::class, 'indexSD']);
            Route::get('getServiceCateg', [ServiceDescriptionController::class, 'getServiceCategSD']);
        });
        Route::group(['prefix' => 'industry-mgmt'], function () {
            Route::post('create', [IndustryController::class, 'createIN']);
            Route::put('{id}/update', [IndustryController::class, 'updateIN']);
            Route::post('{id}/toggleStatus', [IndustryController::class, 'toggleStatusIN']);
            Route::get('index', [IndustryController::class, 'indexIN']);
        });
        Route::group(['prefix' => 'bank-name-mgmt'], function () {
            Route::post('create', [BankNameController::class, 'createBN']);
            Route::put('{id}/update', [BankNameController::class, 'updateBN']);
            Route::post('{id}/toggleStatus', [BankNameController::class, 'toggleStatusBN']);
            Route::get('index', [BankNameController::class, 'indexBN']);
        });
        Route::group(['prefix' => 'manpower-mgmt'], function () {
            Route::post('create', [ManPowerController::class, 'createMP']);
            Route::put('{id}/update', [ManPowerController::class, 'updateMP']);
            Route::post('{id}/toggleStatus', [ManPowerController::class, 'toggleStatusMP']);
            Route::get('index', [ManPowerController::class, 'indexMP']);
        });
        Route::group(['prefix' => 'acctng-supplier-mgmt'], function () {
            Route::post('create', [AcctngSupplierController::class, 'createAS']);
            Route::put('{id}/update', [AcctngSupplierController::class, 'updateAS']);
            Route::post('{id}/toggleStatus', [AcctngSupplierController::class, 'toggleStatusAS']);
            Route::get('index', [AcctngSupplierController::class, 'indexAS']);
            Route::get('getIndustry', [AcctngSupplierController::class, 'getIndustryAS']);
            Route::get('getRegion', [AcctngSupplierController::class, 'getRegionAS']);
            Route::get('getProvince', [AcctngSupplierController::class, 'getProvinceAS']);
            Route::get('getMunicipal', [AcctngSupplierController::class, 'getMunicipalAS']);
            Route::get('getBarangay', [AcctngSupplierController::class, 'getBarangayAS']);
            Route::get('getBank', [AcctngSupplierController::class, 'getBankAS']);
        });
        Route::group(['prefix' => 'purchasing-loa-mgmt'], function () {
            Route::post('create', [PurchasingLoaController::class, 'createPL']);
            Route::put('{id}/update', [PurchasingLoaController::class, 'updatePL']);
            Route::post('{id}/delete', [PurchasingLoaController::class, 'deletePL']);
            Route::get('index', [PurchasingLoaController::class, 'indexPL']);
        });
        Route::group(['prefix' => 'budget-mgmt'], function () {
            Route::post('create', [BudgetMgmtController::class, 'createBP']);
            Route::get('index', [BudgetMgmtController::class, 'indexBP']);
            Route::get('getCoa', [BudgetMgmtController::class, 'getCoaBP']);
        });

        Route::group(['prefix' => 'service-requisition-mgmt'], function () {
            Route::post('create', [ServiceReqController::class, 'createSR']);
            Route::put('{id}/reqstat', [ServiceReqController::class, 'reqstatSR']);
            Route::get('index', [ServiceReqController::class, 'indexSR']);
            Route::get('getServiceCateg', [ServiceReqController::class, 'getServiceCategSR']);
            Route::get('getServiceDesc', [ServiceReqController::class, 'getServiceDescSR']);
            Route::get('getPrefWork', [ServiceReqController::class, 'getPrefWorkSR']);
            Route::get('getPurp', [ServiceReqController::class, 'getPurpSR']);
            Route::get('getLoc', [ServiceReqController::class, 'getLocSR']);
        });

        Route::group(['prefix' => 'purchase-requisition-mgmt'], function () {
            Route::post('create', [PurchaseReqController::class, 'createPR']);
            Route::put('{id}/reqstat', [PurchaseReqController::class, 'reqstatPR']);
            Route::get('index', [PurchaseReqController::class, 'indexPR']);
            Route::get('getItemCateg', [PurchaseReqController::class, 'getItemCategPR']);
            Route::get('getItemDesc', [PurchaseReqController::class, 'getItemDescPR']);
            Route::get('getPurp', [PurchaseReqController::class, 'getPurpPR']);
            Route::get('getBenBranch', [PurchaseReqController::class, 'getBenBranchPR']);
            Route::get('getPrefSup', [PurchaseReqController::class, 'getPrefSupPR']);
            // Route::get('getLoc', [ServiceReqController::class, 'getLocSR']);
        });
    });

    Route::group(['prefix' => 'hrim'], function () {
        Route::group(['prefix' => 'time-in-and-out'], function () {
            Route::post('timein', [TimeInAndOutController::class, 'timeIn']);
            Route::post('timeout', [TimeInAndOutController::class, 'timeOut']);
        });

        Route::group(['prefix' => 'calendar'], function () {
            Route::post('show', [CalendarController::class, 'show']);
        });

        Route::group(['prefix' => 'daily-time-records'], function () {
            Route::post('show', [DailyTimeRecordsController::class, 'show']);
            Route::post('create', [DailyTimeRecordsController::class, 'create']);
            Route::post('get-tar-reason', [DailyTimeRecordsController::class, 'getTarReason']);
        });

        Route::group(['prefix' => 'employee-tar'], function () {
        });


        Route::group(['prefix' => 'employee-leave-records'], function () {
            Route::post('show', [LeaveRecordsController::class, 'show']);
            Route::post('create', [LeaveRecordsController::class, 'create']);
            Route::put('{id}/update', [LeaveRecordsController::class, 'update']);
        });

        Route::group(['prefix' => 'overtime-records'], function () {
            Route::post('show', [OvertimeRecordsController::class, 'show']);
            Route::post('create', [OvertimeRecordsController::class, 'create']);
            Route::post('get_date', [OvertimeRecordsController::class, 'get_date']);
            Route::post('get_date_specific', [OvertimeRecordsController::class, 'get_date_specific']);
            Route::post('get_compute', [OvertimeRecordsController::class, 'get_compute']);
        });

        Route::group(['prefix' => 'company-bulletin'], function () {
            Route::post('show-announcement', [BulletinController::class, 'showAnnouncement']);
            Route::post('show-memo', [BulletinController::class, 'showMemos']);
        });

        Route::group(['prefix' => 'profile'], function () {
            Route::post('show', [AccountManagementController::class, 'show']);
        });

        Route::group(['prefix' => 'loans-and-ledger'], function () {
            Route::post('show-payment-schedule', [LoansAndLedgerMonitoringController::class, 'showPaymentSchedule']);
            Route::post('show', [LoansAndLedgerMonitoringController::class, 'show']);
            Route::post('create', [LoansAndLedgerMonitoringController::class, 'create']);
        });

        Route::group(['prefix' => 'payslip'], function () {
            Route::post('show', [PayslipsController::class, 'show']);
        });

        Route::group(['prefix' => 'performance-evaluation'], function () {
            Route::post('show', [PerformanceEvaluationController::class, 'show']);
            Route::post('create', [PerformanceEvaluationController::class, 'create']);
        });
        Route::group(['prefix' => 'forgot-password'], function () {
            Route::post('forgot', [ForgotPasswordController::class, 'forgot']);
        });
    });

    Route::group(['prefix' => 'current'], function () {
        Route::get('', [AccountManagementController::class, 'showCurrent']);
    });
});
