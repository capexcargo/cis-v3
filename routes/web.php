<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PurposeController;
use App\Http\Controllers\ManpowerController;
use App\Http\Controllers\SubAccountsController;
use App\Http\Controllers\OpexCategoryController;
use App\Http\Controllers\DmBudgetSourceController;
use App\Http\Controllers\Accounting\UnitController;
use App\Http\Controllers\ChartofAccountsController;
use App\Http\Controllers\Crm\Sales\QuotaController;
use App\Http\Controllers\Crm\Sales\SalesController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\ReasonForRejectionController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Crm\Sales\QuotationController;
use App\Http\Controllers\Hrim\TimeinAndOutAllController;
use App\Http\Controllers\Oims\Reports\ReportsController;
use App\Http\Controllers\Accounting\ServiceReqController;
use App\Http\Controllers\Admin\RolesManagementController;
use App\Http\Controllers\Crm\Reports\SrReportsController;
use App\Http\Controllers\Accounting\PurchaseReqController;
use App\Http\Controllers\Crm\Reports\CdmReportsController;
use App\Http\Controllers\Hrim\Csp\CspManagementController;
use App\Http\Controllers\Accounting\ItemCategoryController;
use App\Http\Controllers\Admin\AccountManagementController;
use App\Http\Controllers\Crm\Contracts\ContractsController;
use App\Http\Controllers\Crm\Reports\QuotaReportsController;
use App\Http\Controllers\Crm\Sales\RateCalculatorController;
use App\Http\Controllers\Crm\Activities\ActivitiesController;
use App\Http\Controllers\Accounting\ItemDescriptionController;
use App\Http\Controllers\Accounting\ServiceCategoryController;
use App\Http\Controllers\Crm\Reports\BookingReportsController;
use App\Http\Controllers\Hrim\Attendance\TeamStatusController;
use App\Http\Controllers\Hrim\Csp\CspUserManagementController;
use App\Http\Controllers\Ticketing\TicketManagementController;
use App\Http\Controllers\Crm\Reports\ActivityReportsController;
use App\Http\Controllers\Crm\Reports\ContractReportsController;
use App\Http\Controllers\Crm\Sales\ChargesManagementController;
use App\Http\Controllers\Accounting\ServiceDescriptionController;
use App\Http\Controllers\Crm\Commercials\CommercialMgmtController;
use App\Http\Controllers\Crm\Reports\CommercialsReportsController;
use App\Http\Controllers\Crm\Reports\SalesModuleReportsController;
use App\Http\Controllers\Hrim\Reports\LoanGovtDeductionController;
use App\Http\Controllers\Ticketing\TicketSubcategoryMgmtController;
use App\Http\Controllers\Crm\ServiceRequest\SrRelatedMgmtController;
use App\Http\Controllers\Hrim\EmployeeManagement\SanctionController;
use App\Http\Controllers\Hrim\EmployeeManagement\ViolationController;
use App\Http\Controllers\Hrim\EmployeeManagement\CocSectionController;
use App\Http\Controllers\Ticketing\TicketCategoryManagementController;
use App\Http\Controllers\Crm\Sales\Quota\StakeholderCategoryController;
use App\Http\Controllers\Crm\CustomerInformation\IndustryMgmtController;
use App\Http\Controllers\Hrim\PayrollManagement\DlbManagementController;
use App\Http\Controllers\Hrim\PayrollManagement\SssManagementController;
use App\Http\Controllers\Hrim\RecruitmentAndHiring\OnboardingController;
use App\Http\Controllers\Oims\OrderManagement\OrderManagementController;
use App\Http\Controllers\Crm\CustomerInformation\TwilioWebhookController;
use App\Http\Controllers\Crm\ServiceRequest\ServiceRequestMgmtController;
use App\Http\Controllers\Hrim\EmployeeManagement\SanctionStatusController;
use App\Http\Controllers\Crm\CustomerInformation\CustomerDataMgmtController;
use App\Http\Controllers\Hrim\Attendance\TarController as HrimTarController;
use App\Http\Controllers\Hrim\PayrollManagement\PagibigManagementController;
use App\Http\Controllers\Hrim\DashboardController as HrimDashboardController;
use App\Http\Controllers\Crm\CustomerInformation\CustomerOnboardingController;
use App\Http\Controllers\Hrim\EmployeeManagement\DisciplinaryRecordController;
use App\Http\Controllers\Hrim\EmployeeManagement\DisciplinaryHistoryController;
use App\Http\Controllers\Hrim\PayrollManagement\PhilhealthManagementController;
use App\Http\Controllers\Hrim\RecruitmentAndHiring\ApplicantTrackingController;
use App\Http\Controllers\Hrim\Workforce\TeamsController as HrimTeamsController;
use App\Http\Controllers\Crm\CustomerInformation\MarketingChannelMgmtController;
use App\Http\Controllers\Hrim\PayrollManagement\GovernmentContributionController;
use App\Http\Controllers\Hrim\RecruitmentAndHiring\ApplicantStatusMgmtController;
use App\Http\Controllers\Hrim\RecruitmentAndHiring\EmployeeRequisitionController;
use App\Http\Controllers\Crm\CustomerInformation\CustomerOnboardingLinkController;
use App\Http\Controllers\Hrim\EmployeeManagement\DisciplinaryRecordMgmtController;
use App\Http\Controllers\Hrim\RecruitmentAndHiring\OnboardingStatusMgmtController;
use App\Http\Controllers\Hrim\CompensationAndBenefits\BenefitsManagementController;
use App\Http\Controllers\Hrim\Csp\OnboardingController as HrimOnboardingController;
use App\Http\Controllers\Hrim\TimeInAndOutController as HrimTimeInAndOutController;
use App\Http\Controllers\Hrim\CompensationAndBenefits\StatutoryManagementController;
use App\Http\Controllers\Hrim\RecruitmentAndHiring\EmploymentCategoryTypeController;
use App\Http\Controllers\Hrim\LoaManagementController as HrimLoaManagementController;
use App\Http\Controllers\Hrim\Workforce\JobLevelController as HrimJobLevelController;
use App\Http\Controllers\Hrim\Workforce\PositionController as HrimPositionController;
use App\Http\Controllers\Hrim\CompensationAndBenefits\SalaryGradeManagementController;
use App\Http\Controllers\Hrim\EmployeeManagement\EmploymentStatusManagementController;
use App\Http\Controllers\Accounting\CashFlowController as AccountingCashFlowController;
use App\Http\Controllers\Hrim\Attendance\SchedulesController as HrimSchedulesController;
use App\Http\Controllers\Hrim\Workforce\DepartmentController as HrimDepartmentController;
use App\Http\Controllers\Hrim\CompanyManagement\BulletinController as HrimBulletinController;
use App\Http\Controllers\Hrim\CompanyManagement\CalendarController as HrimCalendarController;
use App\Http\Controllers\Hrim\Workforce\WorkScheduleController as HrimWorkScheduleController;
use App\Http\Controllers\Hrim\Attendance\LeaveRecordsController as HrimLeaveRecordsController;
use App\Http\Controllers\Accounting\CheckVoucherController as AccountingCheckVoucherController;
use App\Http\Controllers\Hrim\PayrollManagement\Month13thController as HrimMonth13thController;
use App\Http\Controllers\Hrim\EmployeeManagement\ResignationReasonReferenceManagementController;
use App\Http\Controllers\Hrim\TalentManagement\KpiTaggingController as HrimKpiTaggingController;
use App\Http\Controllers\Hrim\CompensationAndBenefits\PayrollController as HrimPayrollController;
use App\Http\Controllers\Hrim\PayrollManagement\CutOffMgmtController as HrimCutOffMgmtController;
use App\Http\Controllers\Hrim\TalentManagement\PerformanceController as HrimPerformanceController;
use App\Http\Controllers\Accounting\BankValidationController as AccountingBankValidationController;
use App\Http\Controllers\Accounting\SupplierLedgerController as AccountingSupplierLedgerController;
use App\Http\Controllers\Hrim\CompensationAndBenefits\PayslipsController as HrimPayslipsController;
use App\Http\Controllers\Hrim\Attendance\OvertimeRecordsController as HrimOvertimeRecordsController;
use App\Http\Controllers\Hrim\Attendance\DailyTimeRecordsController as HrimDailyTimeRecordsController;
use App\Http\Controllers\Hrim\TalentManagement\KpiManagementController as HrimKpiManagementController;
use App\Http\Controllers\Hrim\TalentManagement\KraManagementController as HrimKraManagementController;
use App\Http\Controllers\Accounting\LoaderManagementController as AccountingLoaderManagementController;
use App\Http\Controllers\Hrim\PayrollManagement\LoaAdjustmentController as HrimLoaAdjustmentController;
use App\Http\Controllers\Hrim\TalentManagement\PercentageMgmtController as HrimPercentageMgmtController;
use App\Http\Controllers\Accounting\AccountManagementController as AccountingAccountManagementController;
use App\Http\Controllers\Accounting\RequestManagementController as AccountingRequestManagementController;
use App\Http\Controllers\Hrim\CompanyManagement\CompanyProfileController as HrimCompanyProfileController;
use App\Http\Controllers\Accounting\BudgetManagement\BudgetLoaController as AccountingBudgetLoaController;
use App\Http\Controllers\Accounting\SupplierManagement\SupplierController as AccountingSupplierController;
use App\Http\Controllers\Hrim\Attendance\ScheduleAdjustmentController as HrimScheduleAdjustmentController;
use App\Http\Controllers\Accounting\DivisionManagementController as AccountingDivisionManagementController;
use App\Http\Controllers\Accounting\OpexTypeManagementController as AccountingOpexTypeManagementController;
use App\Http\Controllers\Hrim\PayrollManagement\AdminAdjustmentController as HrimAdminAdjustmentController;
use App\Http\Controllers\Accounting\BudgetManagement\BudgetPlanController as AccountingBudgetPlanController;
use App\Http\Controllers\Accounting\BudgetManagement\BudgetChartController as AccountingBudgetChartController;
use App\Http\Controllers\Hrim\EmployeeAttendance\LeaveRecordsController as HrimEmployeeLeaveRecordsController;
use App\Http\Controllers\Accounting\CanvassingManagementController as AccountingCanvassingManagementController;
use App\Http\Controllers\Hrim\PayrollManagement\HolidayManagementController as HrimHolidayManagementController;
use App\Http\Controllers\Accounting\BudgetManagement\BudgetSourceController as AccountingBudgetSourceController;
use App\Http\Controllers\Accounting\CaReferenceManagementController as AccountingCaReferenceManagementController;
use App\Http\Controllers\Accounting\LiquidationManagementController as AccountingLiquidationManagementController;
use App\Http\Controllers\Hrim\CompanyManagement\EmployerComplianceController as HrimEmployerComplianceController;
use App\Http\Controllers\Hrim\TalentManagement\CoreValueManagementController as HrimCoreValueManagementController;
use App\Http\Controllers\Hrim\TalentManagement\EvaluatorManagementController as HrimEvaluatorManagementController;
use App\Http\Controllers\Hrim\TalentManagement\KraPointsManagementController as HrimKraPointsManagementController;
use App\Http\Controllers\Hrim\CompanyManagement\TableOfOrganizationController as HrimTableOfOrganizationController;
use App\Http\Controllers\Hrim\Workforce\OrganizationalStructureController as HrimOrganizationalStructureController;
use App\Http\Controllers\Hrim\EmployeeAttendance\OvertimeRecordsController as HrimEmployeeOvertimeRecordsController;
use App\Http\Controllers\Hrim\EmployeeManagement\EmployeeInformationController as HrimEmployeeInformationController;
use App\Http\Controllers\Accounting\PurchaseOrderManagementController as AccountingPurchaseOrderManagementController;
use App\Http\Controllers\Hrim\EmployeeAttendance\DailyTimeRecordsController as HrimEmployeeDailyTimeRecordsController;
use App\Http\Controllers\Hrim\TalentManagement\PerformanceEvaluationController as HrimPerformanceEvaluationController;
use App\Http\Controllers\Accounting\BudgetManagement\BudgetPlanSummaryController as AccountingBudgetPlanSummaryController;
use App\Http\Controllers\Accounting\SupplierManagement\SupplierIndustryController as AccountingSupplierIndustryController;
use App\Http\Controllers\Hrim\EmployeeAttendance\ScheduleAdjustmentController as HrimEmployeeScheduleAdjustmentController;
use App\Http\Controllers\Accounting\ApprovalManagement\RequestForPaymentController as AccountingRequestForPaymentController;
use App\Http\Controllers\Hrim\EmployeeRecruitmentAndHiring\EmployeeRequisitionController as ForEmployeeRequisitionController;
use App\Http\Controllers\Hrim\TalentManagement\LeadershipCompetenciesMgmtController as HrimLeadershipCompetenciesMgmtController;
use App\Http\Controllers\Accounting\ApprovalManagement\CheckVoucherApprovalController as AccountingCheckVoucherApprovalController;
use App\Http\Controllers\Hrim\TalentManagement\TrainingAndRefresherModulesController as HrimTrainingAndRefresherModulesController;
use App\Http\Controllers\Hrim\CompensationAndBenefits\LoansAndLedgerMonitoringController as HrimLoansAndLedgerMonitoringController;
use App\Http\Controllers\Hrim\EmployeeTalentManagement\PerformanceEvaluationController as HrimEmployeePerformanceEvaluationController;
use App\Http\Controllers\Hrim\EmployeeCompensationAndBenefits\LoansAndLedgerMonitoringController as HrimEmployeeLoansAndLedgerMonitoringController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/forgot-password', [ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.request');
Route::post('/forgot-password', [ForgotPasswordController::class, 'sendResetLinkEmail'])->name('password.email');
Route::get('/reset-password/{token}', [ResetPasswordController::class, 'showResetForm'])->name('password.reset');
Route::post('/reset-password', [ResetPasswordController::class, 'reset'])->name('password.update');

Route::post('webhooks/twilio', [TwilioWebhookController::class, 'handle']);

Route::get('time-in-and-out-all', [TimeinAndOutAllController::class, 'index'])->name('time-in-and-out-all.index');
Route::get('cis-registration', [AccountManagementController::class, 'cisRegistration'])->name('admin.account-management.cis-registration');

Route::get('customer-onboarding-link', [CustomerOnboardingLinkController::class, 'create'])->name('customer-onboarding-link.index');

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::get('/', function () {
        return view('dashboard');
    });

    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');

    Route::prefix('admin')->group(function () {
        Route::get('account-management', [AccountManagementController::class, 'index'])->name('admin.account-management.index')->middleware('can:admin_account_management_view');
        Route::get('roles-management', [RolesManagementController::class, 'index'])->name('admin.roles-management.index')->middleware('can:admin_roles_management_view');
    });

    Route::group(['prefix' => 'accounting', 'as' => 'accounting.'], function () {
        Route::group(['prefix' => 'canvassing-management', 'as' => 'canvassing-management.', 'middleware' => 'can:accounting_canvassing_management_view'], function () {
            Route::get('', [AccountingCanvassingManagementController::class, 'index'])->name('index');
        });

        Route::group(['prefix' => 'purchase-order-management', 'as' => 'purchase-order-management.', 'middleware' => 'can:accounting_purchase_order_management_view'], function () {
            Route::get('', [AccountingPurchaseOrderManagementController::class, 'index'])->name('index');
            Route::get('print-code/{selected?}', [AccountingPurchaseOrderManagementController::class, 'printPurchaseOrder'])->name('print-purchase-order');
        });

        Route::group(['prefix' => 'service-req', 'as' => 'service-req.', 'middleware' => 'can:accounting_request_management_view'], function () {
            Route::get('', [ServiceReqController::class, 'index'])->name('index');
        });

        Route::group(['prefix' => 'purchase-req', 'as' => 'purchase-req.', 'middleware' => 'can:accounting_request_management_view'], function () {
            Route::get('', [PurchaseReqController::class, 'index'])->name('index');
        });

        Route::group(['prefix' => 'data-management', 'middleware' => 'can:accounting_request_management_view'], function () {
            Route::get('service-category', [ServiceCategoryController::class, 'index'])->name('service-category.index');
            Route::get('service-description', [ServiceDescriptionController::class, 'index'])->name('service-description.index');
            Route::get('item-category', [ItemCategoryController::class, 'index'])->name('item-category.index');
            Route::get('item-description', [ItemDescriptionController::class, 'index'])->name('item-description.index');
            Route::get('unit', [UnitController::class, 'index'])->name('unit.index');
            Route::get('purpose', [PurposeController::class, 'index'])->name('purpose.index');
            Route::get('reason-for-rejection', [ReasonForRejectionController::class, 'index'])->name('reason-for-rejection.index');
            Route::get('sub-accounts', [SubAccountsController::class, 'index'])->name('sub-accounts.index');
            Route::get('chart-of-accounts', [ChartofAccountsController::class, 'index'])->name('chart-of-accounts.index');
            Route::get('opex-category', [OpexCategoryController::class, 'index'])->name('opex-category.index');
            Route::get('dmbudget-source', [DmBudgetSourceController::class, 'index'])->name('dmbudget-source.index');
            Route::get('manpower', [ManpowerController::class, 'index'])->name('manpower.index');

        });

        Route::group(['prefix' => 'request-management', 'as' => 'request-management.', 'middleware' => 'can:accounting_request_management_view'], function () {
            Route::get('', [AccountingRequestManagementController::class, 'index'])->name('index');
            Route::get('monitoring', [AccountingRequestManagementController::class, 'monitoring'])->name('monitoring')->middleware('can:accounting_request_management_monitoring');

            Route::get('print-code/{selected?}', [AccountingRequestManagementController::class, 'printRequestForPayment'])->name('print-request-for-payment');
        });

        Route::group(['prefix' => 'approval-management', 'as' => 'approval-management.'], function () {
            Route::get('request-for-payment', [AccountingRequestForPaymentController::class, 'index'])->name('request-for-payment.index')->middleware('can:accounting_approval_request_for_payment_view');
            Route::get('check-voucher', [AccountingCheckVoucherApprovalController::class, 'index'])->name('check-voucher.index')->middleware('can:accounting_approval_check_voucher_view');
        });

        Route::group(['prefix' => 'check-voucher', 'as' => 'check-voucher.', 'middleware' => 'can:accounting_check_voucher_view'], function () {
            Route::get('', [AccountingCheckVoucherController::class, 'index'])->name('index');
            Route::get('print/{selected?}', [AccountingCheckVoucherController::class, 'printCheckVoucher'])->name('print-check-voucher');
        });

        Route::group(['prefix' => 'supplier-ledger', 'as' => 'supplier-ledger.', 'middleware' => 'can:accounting_supplier_ledger_view'], function () {
            Route::get('', [AccountingSupplierLedgerController::class, 'index'])->name('index');
            Route::get('print-code/{selected?}', [AccountingSupplierLedgerController::class, 'printSupplierLedger'])->name('print-supplier-ledger');
        });

        Route::group(['prefix' => 'liquidation-management', 'as' => 'liquidation-management.', 'middleware' => 'can:accounting_liquidation_management_view'], function () {
            Route::get('', [AccountingLiquidationManagementController::class, 'index'])->name('index');
        });

        Route::group(['prefix' => 'bank-validation', 'as' => 'bank-validation.', 'middleware' => 'can:accounting_bank_validation_view'], function () {
            Route::get('', [AccountingBankValidationController::class, 'index'])->name('index');
        });
        Route::group(['prefix' => 'cash-flow', 'as' => 'cash-flow.', 'middleware' => 'can:accounting_cash_flow_view'], function () {
            Route::get('', [AccountingCashFlowController::class, 'index'])->name('index');
        });

        Route::group(['prefix' => 'budget-management'], function () {
            Route::get('budget-plan-summary', [AccountingBudgetPlanSummaryController::class, 'index'])->name('budget-plan-summary.index')->middleware('can:accounting_budget_plan_summary_view');
            Route::get('budget-plan', [AccountingBudgetPlanController::class, 'index'])->name('budget-plan.index')->middleware('can:accounting_budget_plan_view');
            Route::get('budget-source', [AccountingBudgetSourceController::class, 'index'])->name('budget-source.index')->middleware('can:accounting_budget_source_view');
            Route::get('budget-chart', [AccountingBudgetChartController::class, 'index'])->name('budget-chart.index')->middleware('can:accounting_budget_chart_view');
            Route::get('budget-loa', [AccountingBudgetLoaController::class, 'index'])->name('budget-loa.index')->middleware('can:accounting_budget_loa_view');
        });

        Route::group(['prefix' => 'supplier-management'], function () {
            Route::get('supplier', [AccountingSupplierController::class, 'index'])->name('supplier.index')->middleware('can:accounting_supplier_view');
            Route::get('supplier-industry', [AccountingSupplierIndustryController::class, 'index'])->name('supplier-industry.index')->middleware('can:accounting_supplier_industry_view');
        });

        Route::get('opex-type-management', [AccountingOpexTypeManagementController::class, 'index'])->name('opex-type-management.index')->middleware('can:accounting_opex_type_management_view');
        Route::get('division-management', [AccountingDivisionManagementController::class, 'index'])->name('division-management.index')->middleware('can:accounting_division_management_view');
        Route::get('ca-reference-management', [AccountingCaReferenceManagementController::class, 'index'])->name('ca-reference-management.index')->middleware('can:accounting_ca_reference_management_view');
        Route::get('loader-management', [AccountingLoaderManagementController::class, 'index'])->name('loader-management.index')->middleware('can:accounting_loader_management_view');
        Route::get('account-management', [AccountingAccountManagementController::class, 'index'])->name('account-management.index')->middleware('can:accounting_account_management_view');
    });

    Route::group(['prefix' => 'hrim', 'as' => 'hrim.'], function () {
        Route::get('dashboard', [HrimDashboardController::class, 'index'])->name('dashboard.index')->middleware('can:hrim_dashboard_view');

        Route::get('time-in-and-out', [HrimTimeInAndOutController::class, 'index'])->name('time-in-and-out.index')->middleware('can:hrim_time_in_and_time_out_view');

        Route::group(['prefix' => 'company-management', 'as' => 'company-management.', 'middleware' => 'can:hrim_company_management_view'], function () {
            Route::get('company-profile', [HrimCompanyProfileController::class, 'index'])->name('company-profile.index')->middleware('can:hrim_company_profile_view');
            Route::get('employer-compliance', [HrimEmployerComplianceController::class, 'index'])->name('employer-compliance.index')->middleware('can:hrim_employer_compliance_view');
            Route::get('calendar', [HrimCalendarController::class, 'index'])->name('calendar.index')->middleware('can:hrim_calendar_view');
            Route::get('bulletin', [HrimBulletinController::class, 'index'])->name('bulletin.index')->middleware('can:hrim_company_bulletin_view');
            Route::get('table-of-organization', [HrimTableOfOrganizationController::class, 'index'])->name('table-of-organization.index')->middleware('can:hrim_table_of_organization_view');
        });

        Route::group(['prefix' => 'employee-management', 'as' => 'employee-management.', 'middleware' => 'can:hrim_employee_management_view'], function () {
            Route::get('employee-information', [HrimEmployeeInformationController::class, 'index'])->name('employee-information.index')->middleware('can:hrim_employee_information_view');
            Route::get('disciplinary-history', [DisciplinaryHistoryController::class, 'index'])->name('disciplinary-history.index')->middleware('can:hrim_disciplinary_history_view');
            Route::get('disciplinary-record-management', [DisciplinaryRecordMgmtController::class, 'index'])->name('disciplinary-record-management.index')->middleware('can:hrim_disciplinary_record_mgmt_view');
            Route::get('coc-section', [CocSectionController::class, 'index'])->name('coc-section.index')->middleware('can:hrim_coc_section_view');
            Route::get('violation', [ViolationController::class, 'index'])->name('violation.index')->middleware('can:hrim_violation_view');
            Route::get('disciplinary-record', [DisciplinaryRecordController::class, 'index'])->name('disciplinary-record.index')->middleware('can:hrim_disciplinary_record_view');
            Route::get('sanction', [SanctionController::class, 'index'])->name('sanction.index')->middleware('can:hrim_sanction_view');
            Route::get('sanction-status', [SanctionStatusController::class, 'index'])->name('sanction-status.index')->middleware('can:hrim_sanction_status_view');
            Route::get('employment-status-management', [EmploymentStatusManagementController::class, 'index'])->name('employment-status-management.index')->middleware('can:hrim_employment_status_management_view');
            Route::get('resignation-reason-reference-management', [ResignationReasonReferenceManagementController::class, 'index'])->name('resignation-reason-reference-management.index')->middleware('can:hrim_resignation_reason_reference_management_view');
        });

        Route::group(['prefix' => 'workforce', 'as' => 'workforce.', 'middleware' => 'can:hrim_workforce_view'], function () {
            Route::get('work-schedule', [HrimWorkScheduleController::class, 'index'])->name('work-schedule.index')->middleware('can:hrim_work_schedule_view');
            Route::get('teams', [HrimTeamsController::class, 'index'])->name('teams.index')->middleware('can:hrim_work_schedule_view');
            Route::get('organizational-structure', [HrimOrganizationalStructureController::class, 'index'])->name('organizational-structure.index')->middleware('can:hrim_organizational_structure_view');
            Route::get('position', [HrimPositionController::class, 'index'])->name('position.index')->middleware('can:hrim_position_view');
            Route::get('position/{id}/print', [HrimPositionController::class, 'print'])->name('position.print'); //->middleware('can:hrim_position_print');
            Route::get('job-level', [HrimJobLevelController::class, 'index'])->name('job-level.index')->middleware('can:hrim_job_level_view');
            Route::get('department', [HrimDepartmentController::class, 'index'])->name('department.index')->middleware('can:hrim_department_view');
        });

        Route::group(['prefix' => 'attendance', 'as' => 'attendance.', 'middleware' => 'can:hrim_attendance_view'], function () {
            Route::get('daily-time-records', [HrimDailyTimeRecordsController::class, 'index'])->name('daily-time-records.index')->middleware('can:hrim_daily_time_records_view');

            Route::group(['prefix' => 'leave-records', 'as' => 'leave-records.', 'middleware' => 'can:hrim_leave_records_view'], function () {
                Route::get('', [HrimLeaveRecordsController::class, 'index'])->name('index');
                Route::get('{id}/view', [HrimLeaveRecordsController::class, 'view'])->name('view');
            });

            Route::group(['prefix' => 'overtime-records', 'as' => 'overtime-records.', 'middleware' => 'can:hrim_overtime_records_view'], function () {
                Route::get('', [HrimOvertimeRecordsController::class, 'index'])->name('index');
                Route::get('{id}/view', [HrimOvertimeRecordsController::class, 'view'])->name('view');
            });

            Route::group(['prefix' => 'schedules', 'as' => 'schedules.', 'middleware' => 'can:hrim_schedules_view'], function () {
                Route::get('', [HrimSchedulesController::class, 'index'])->name('index');
                Route::get('tar/{id}/view', [HrimTarController::class, 'view'])->name('tar.view');
                Route::get('schedule-adjustment/{id}/view', [HrimScheduleAdjustmentController::class, 'view'])->name('schedule-adjustment.view');
            });

            Route::group(['prefix' => 'team-status', 'as' => 'team-status.', 'middleware' => 'can:hrim_team_status_view'], function () {
                Route::get('', [TeamStatusController::class, 'index'])->name('index');
                Route::get('{id}/view', [TeamStatusController::class, 'view'])->name('view');
            });
        });

        Route::group(['prefix' => 'employee-attendance', 'as' => 'employee-attendance.', 'middleware' => 'can:hrim_employee_attendance_view'], function () {
            Route::get('daily-time-records', [HrimEmployeeDailyTimeRecordsController::class, 'index'])->name('daily-time-records.index')->middleware('can:hrim_employee_daily_time_records_view');
            Route::get('leave-records', [HrimEmployeeLeaveRecordsController::class, 'index'])->name('leave-records.index')->middleware('can:hrim_leave_records_view');
            Route::get('overtime-records', [HrimEmployeeOvertimeRecordsController::class, 'index'])->name('overtime-records.index')->middleware('can:hrim_employee_overtime_records_view');
            Route::get('schedule-adjustment', [HrimEmployeeScheduleAdjustmentController::class, 'index'])->name('schedule-adjustment.index')->middleware('can:hrim_employee_schedule_adjustment_view');
        });

        Route::get('loa-management', [HrimLoaManagementController::class, 'index'])->name('loa-management.index')->middleware('can:hrim_loa_management_view');

        Route::group(['prefix' => 'recruitment-and-hiring', 'as' => 'recruitment-and-hiring.', 'middleware' => 'can:hrim_recruitment_and_hiring_view'], function () {
            Route::get('employee-requisition', [EmployeeRequisitionController::class, 'index'])->name('employee-requisition.index')->middleware('can:hrim_employee_requisition_view');
            Route::get('applicant-tracking', [ApplicantTrackingController::class, 'index'])->name('applicant-tracking.index')->middleware('can:hrim_applicant_tracking_view');
            Route::get('applicant-status-mgmt', [ApplicantStatusMgmtController::class, 'index'])->name('applicant-status-mgmt.index')->middleware('can:hrim_applicant_status_mgmt_view');
            Route::get('onboarding', [OnboardingController::class, 'index'])->name('onboarding.index')->middleware('can:hrim_onboarding_view');
            Route::get('onboarding-status-mgmt', [OnboardingStatusMgmtController::class, 'index'])->name('onboarding-status-mgmt.index')->middleware('can:hrim_onboarding_status_mgmt_view');
            Route::get('employment-category-type', [EmploymentCategoryTypeController::class, 'index'])->name('employment-category-type.index')->middleware('can:hrim_employment_category_type_view');
        });

        Route::group(['prefix' => 'employee-recruitment-and-hiring', 'as' => 'employee-recruitment-and-hiring.', 'middleware' => 'can:hrim_employee_recruitment_and_hiring_view'], function () {
            Route::get('employee-requisition', [ForEmployeeRequisitionController::class, 'index'])->name('employee-requisition.index')->middleware('can:hrim_employee_recruitment_and_hiring_view');
        });

        Route::group(['prefix' => 'talent-management', 'as' => 'talent-management.', 'middleware' => 'can:hrim_talent_management_view'], function () {
            Route::get('performance', [HrimPerformanceController::class, 'index'])->name('performance.index')->middleware('can:hrim_performance_view');
            Route::get('kra-management', [HrimKraManagementController::class, 'index'])->name('kra-management.index')->middleware('can:hrim_kra_management_view');
            Route::get('kra-points-management', [HrimKraPointsManagementController::class, 'index'])->name('kra-points-management.index')->middleware('can:hrim_kra_points_management_view');
            Route::get('kpi-management', [HrimKpiManagementController::class, 'index'])->name('kpi-management.index')->middleware('can:hrim_kpi_management_view');
            Route::get('kpi-tagging', [HrimKpiTaggingController::class, 'index'])->name('kpi-tagging.index')->middleware('can:hrim_kpi_tagging_view');
            Route::get('evaluator-management', [HrimEvaluatorManagementController::class, 'index'])->name('evaluator-management.index')->middleware('can:hrim_evaluator_management_view');
            Route::get('core-value-management', [HrimCoreValueManagementController::class, 'index'])->name('core-value-management.index')->middleware('can:hrim_core_value_management_view');
            Route::get('leadership-competencies-management', [HrimLeadershipCompetenciesMgmtController::class, 'index'])->name('leadership-competencies-mgmt.index')->middleware('can:hrim_leadership_competencies_management_view');
            Route::get('percentage-management', [HrimPercentageMgmtController::class, 'index'])->name('percentage-management.index')->middleware('can:hrim_percentage_management_view');

            Route::group(['prefix' => 'performance-evaluation', 'as' => 'performance-evaluation.', 'middleware' => 'can:hrim_performance_evaluation_view'], function () {
                Route::get('', [HrimPerformanceEvaluationController::class, 'index'])->name('index');
                Route::get('{id}/{quarter}/{years}/view', [HrimPerformanceEvaluationController::class, 'view'])->name('view');
            });
            Route::get('training-and-refresher-modules', [HrimTrainingAndRefresherModulesController::class, 'index'])->name('training-and-refresher-modules.index')->middleware('can:hrim_training_and_refresher_modules_view');
        });

        Route::group(['prefix' => 'employee-talent-management', 'as' => 'employee-talent-management.', 'middleware' => 'can:hrim_employee_talent_management_view'], function () {
            // Route::get('performance-evaluation', [HrimEmployeePerformanceEvaluationController::class, 'index'])->name('performance-evaluation.index')->middleware('can:hrim_employee_talent_management_view');
            Route::group(['prefix' => 'performance-evaluation', 'as' => 'performance-evaluation.', 'middleware' => 'can:hrim_employee_talent_management_view'], function () {
                Route::get('', [HrimEmployeePerformanceEvaluationController::class, 'index'])->name('index');
                Route::get('{id}/{quarter}/{year}/view-evaluation', [HrimEmployeePerformanceEvaluationController::class, 'view'])->name('view-evaluation');
            });
        });

        Route::group(['prefix' => 'compensation-and-benefits', 'as' => 'compensation-and-benefits.', 'middleware' => 'can:hrim_compensation_and_benefits_view'], function () {
            Route::get('loans-and-ledger-monitoring', [HrimLoansAndLedgerMonitoringController::class, 'index'])->name('loans-and-ledger-monitoring.index')->middleware('can:hrim_loans_and_ledger_monitoring_view');
            Route::get('salary-grade-management', [SalaryGradeManagementController::class, 'index'])->name('salary-grade-management.index')->middleware('can:hrim_salary_grade_management_view');
            Route::get('benefits-management', [BenefitsManagementController::class, 'index'])->name('benefits-management.index')->middleware('can:hrim_benefits_management_view');
            Route::get('statutory-management', [StatutoryManagementController::class, 'index'])->name('statutory-management.index')->middleware('can:hrim_statutory_management_view');
            Route::get('payroll', [HrimPayrollController::class, 'index'])->name('payroll.index')->middleware('can:hrim_payroll_view');
            Route::get('payslips', [HrimPayslipsController::class, 'index'])->name('payslips.index')->middleware('can:hrim_payslips_view');
            Route::get('my-payslips', [HrimPayslipsController::class, 'my_payslip'])->name('payslips.my-payslip')->middleware('can:hrim_mypayslips_view');

            Route::get('print-code/{id}/{cutoff}/{month}/{year}', [HrimPayslipsController::class, 'printMyPayslip'])->name('print-my-payslip');
            // Route::get('print-code/{selected?}', [AccountingRequestManagementController::class, 'printRequestForPayment'])->name('print-request-for-payment');
        });

        Route::group(['prefix' => 'employee-compensation-and-benefits', 'as' => 'employee-compensation-and-benefits.', 'middleware' => 'can:hrim_employee_compensation_and_benefits_view'], function () {
            Route::get('loans-and-ledger-monitoring', [HrimEmployeeLoansAndLedgerMonitoringController::class, 'index'])->name('loans-and-ledger-monitoring.index')->middleware('can:hrim_employee_compensation_and_benefits_view');
            Route::get('print-atd/{id}', [HrimEmployeeLoansAndLedgerMonitoringController::class, 'atd'])->name('print-atd');
        });

        Route::group(['prefix' => 'payroll-management', 'as' => 'payroll-management.', 'middleware' => 'can:hrim_payroll_management_view'], function () {
            Route::get('holiday-management', [HrimHolidayManagementController::class, 'index'])->name('holiday-management.index')->middleware('can:hrim_holiday_management_view');
            Route::get('government-contribution', [GovernmentContributionController::class, 'index'])->name('government-contribution.index')->middleware('can:hrim_government_contribution_view');
            Route::get('sss-management', [SssManagementController::class, 'index'])->name('sss-management.index')->middleware('can:hrim_sss_management_view');
            Route::get('philhealth-management', [PhilhealthManagementController::class, 'index'])->name('philhealth-management.index')->middleware('can:hrim_sss_management_view');
            Route::get('pagibig-management', [PagibigManagementController::class, 'index'])->name('pagibig-management.index')->middleware('can:hrim_sss_management_view');
            Route::get('dlb-management', [DlbManagementController::class, 'index'])->name('dlb-management.index')->middleware('can:hrim_dlb_management_view');
            Route::get('13th-month-pay', [HrimMonth13thController::class, 'index'])->name('13th-month-pay.index')->middleware('can:hrim_13th_month_pay_view');
            Route::get('cut-off-management', [HrimCutOffMgmtController::class, 'index'])->name('cut-off-management.index')->middleware('can:hrim_cut_off_management_view');
            Route::get('admin-adjustment', [HrimAdminAdjustmentController::class, 'index'])->name('admin-adjustment.index')->middleware('can:hrim_admin_adjustment_view');
            Route::get('loa-adjustment-management', [HrimLoaAdjustmentController::class, 'index'])->name('loa-adjustment-management.index')->middleware('can:hrim_loa_adjustment_management_view');
        });

        Route::group(['prefix' => 'csp', 'as' => 'csp.', 'middleware' => 'can:hrim_csp_view'], function () {
            Route::get('csp-user-management', [CspUserManagementController::class, 'index'])->name('csp-user-management.index')->middleware('can:hrim_csp_user_management_view');
            Route::get('csp-management', [CspManagementController::class, 'index'])->name('csp-management.index')->middleware('can:hrim_csp_management_view');
            Route::get('onboarding', [HrimOnboardingController::class, 'index'])->name('onboarding.index')->middleware('can:hrim_csp_onboarding_view');
        });

        Route::group(['prefix' => 'reports', 'as' => 'reports.', 'middleware' => 'can:hrim_reports_view'], function () {
            Route::get('loans-govt-deduction', [LoanGovtDeductionController::class, 'index'])->name('loans-govt-deduction.index')->middleware('can:hrim_reports_loans_govt_deduction_view');
        });
    });

    Route::group(['prefix' => 'crm', 'as' => 'crm.'], function () {
        Route::group(['prefix' => 'reports', 'as' => 'reports.', 'middleware' => 'can:crm_reports_view'], function () {
            Route::get('cdm-reports', [CdmReportsController::class, 'index'])->name('cdm-reports.index')->middleware('can:crm_reports_cdm_reports_view');
            Route::get('sr-reports', [SrReportsController::class, 'index'])->name('sr-reports.index')->middleware('can:crm_reports_sr_reports_view');
            Route::get('sales-module-reports', [SalesModuleReportsController::class, 'index'])->name('sales-module-reports.index')->middleware('can:crm_reports_sales_module_reports_view');
            Route::get('booking-reports', [BookingReportsController::class, 'index'])->name('booking-reports.index')->middleware('can:crm_reports_booking_reports_view');
            Route::get('quota-reports', [QuotaReportsController::class, 'index'])->name('quota-reports.index')->middleware('can:crm_reports_quota_reports_view');
            Route::get('activity-reports', [ActivityReportsController::class, 'index'])->name('activity-reports.index')->middleware('can:crm_reports_activity_reports_view');
            Route::get('contract-reports', [ContractReportsController::class, 'index'])->name('contract-reports.index')->middleware('can:crm_reports_contract_reports_view');
            Route::get('commercials-reports', [CommercialsReportsController::class, 'index'])->name('commercials-reports.index')->middleware('can:crm_reports_commercials_reports_view');
        });

        Route::group(['prefix' => 'customer-information', 'as' => 'customer-information.', 'middleware' => 'can:crm_customer_information_view'], function () {
            Route::get('marketing-channel-mgmt', [MarketingChannelMgmtController::class, 'index'])->name('marketing-channel-mgmt.index')->middleware('can:crm_customer_information_marketing_channel_mgmt_view');
            Route::get('industry-mgmt', [IndustryMgmtController::class, 'index'])->name('industry-mgmt.index')->middleware('can:crm_customer_information_industry_mgmt_view');
            Route::get('customer-onboarding', [CustomerOnboardingController::class, 'index'])->name('customer-onboarding.index')->middleware('can:crm_customer_information_customer_onboarding_view');

            Route::group(['prefix' => 'customer-data-mgmt', 'as' => 'customer-data-mgmt.', 'middleware' => 'can:crm_customer_information_customer_onboarding_view'], function () {
                Route::get('', [CustomerDataMgmtController::class, 'index'])->name('index');
                Route::get('{id}/search-result', [CustomerDataMgmtController::class, 'search_result'])->name('search-result')->middleware('can:crm_customer_information_customer_data_mgmt_view');
            });
        });

        Route::group(['prefix' => 'service-request', 'as' => 'service-request.', 'middleware' => 'can:crm_service_request_view'], function () {
            Route::group(['prefix' => 'service-request-mgmt', 'as' => 'service-request-mgmt.', 'middleware' => 'can:crm_service_request_service_request_mgmt_view'], function () {
                Route::get('', [ServiceRequestMgmtController::class, 'index'])->name('index');
                Route::get('{id}/edit', [ServiceRequestMgmtController::class, 'edit'])->name('edit');
            });
            Route::get('sr-related-mgmt', [SrRelatedMgmtController::class, 'srtype'])->name('sr-related-mgmt.index')->middleware('can:crm_service_request_sr_related_mgmt_view');
            Route::get('sr-related-mgmt.sr-sub-category', [SrRelatedMgmtController::class, 'srsubcat'])->name('sr-related-mgmt.sr-sub-category.index')->middleware('can:crm_service_request_sr_related_mgmt_sr_sub_category_view');
            Route::get('sr-related-mgmt.response', [SrRelatedMgmtController::class, 'srres'])->name('sr-related-mgmt.response.index')->middleware('can:crm_service_request_sr_related_mgmt_response_view');
            Route::get('sr-related-mgmt.resolution', [SrRelatedMgmtController::class, 'srreso'])->name('sr-related-mgmt.resolution.index')->middleware('can:crm_service_request_sr_related_mgmt_resolution_view');
            Route::get('sr-related-mgmt.urgency', [SrRelatedMgmtController::class, 'urge'])->name('sr-related-mgmt.urgency.index')->middleware('can:crm_service_request_sr_related_mgmt_urgency_view');
            Route::get('sr-related-mgmt.hierarchy', [SrRelatedMgmtController::class, 'hier'])->name('sr-related-mgmt.hierarchy.index')->middleware('can:crm_service_request_sr_related_mgmt_hierarchy_view');
            Route::get('sr-related-mgmt.channel', [SrRelatedMgmtController::class, 'chan'])->name('sr-related-mgmt.channel.index')->middleware('can:crm_service_request_sr_related_mgmt_channel_view');
            Route::get('sr-related-mgmt.service', [SrRelatedMgmtController::class, 'serv'])->name('sr-related-mgmt.service.index')->middleware('can:crm_service_request_sr_related_mgmt_service_view');

            Route::get('knowledge', [SrRelatedMgmtController::class, 'know'])->name('knowledge.index')->middleware('can:crm_service_request_knowledge_view');
            Route::get('knowledge.sales-coach', [SrRelatedMgmtController::class, 'sale'])->name('knowledge.sales-coach.index')->middleware('can:crm_service_request_knowledge_sales_coach_view');
        });

        Route::group(['prefix' => 'commercials', 'as' => 'commercials.', 'middleware' => 'can:crm_commercials_view'], function () {
            Route::get('commercial-mgmt', [CommercialMgmtController::class, 'AirFreight'])->name('commercial-mgmt.index')->middleware('can:crm_commercials_commercial_mgmt_view');
            Route::get('commercial-mgmt.pouch', [CommercialMgmtController::class, 'Pouch'])->name('commercial-mgmt.pouch.index')->middleware('can:crm_commercials_commercial_mgmt_pouch_view');
            Route::get('commercial-mgmt.loa-mgmt', [CommercialMgmtController::class, 'Loa'])->name('commercial-mgmt.loa-mgmt.index')->middleware('can:crm_commercials_commercial_mgmt_loa_mgmt_view');
            Route::get('commercial-mgmt.box', [CommercialMgmtController::class, 'Box'])->name('commercial-mgmt.box.index')->middleware('can:crm_commercials_commercial_mgmt_box_view');
            Route::get('ancillary-display', [CommercialMgmtController::class, 'AncillaryDisplay'])->name('ancillary-display.index')->middleware('can:crm_commercials_ancillary_mgmt_display_view');
            Route::get('ancillary', [CommercialMgmtController::class, 'Ancillary'])->name('ancillary.index')->middleware('can:crm_commercials_ancillary_mgmt_view');
            Route::get('commercial-mgmt.crating', [CommercialMgmtController::class, 'Crating'])->name('commercial-mgmt.crating.index')->middleware('can:crm_commercials_commercial_mgmt_crating_view');
            Route::get('commercial-mgmt.warehousing', [CommercialMgmtController::class, 'Warehousing'])->name('commercial-mgmt.warehousing.index')->middleware('can:crm_commercials_commercial_mgmt_warehousing_view');
            Route::get('commercial-mgmt.land-freight', [CommercialMgmtController::class, 'LandFreight'])->name('commercial-mgmt.land-freight.index')->middleware('can:crm_commercials_commercial_mgmt_land_freight_view');
            Route::get('commercial-mgmt.sea-freight', [CommercialMgmtController::class, 'SeaFreight'])->name('commercial-mgmt.sea-freight.index')->middleware('can:crm_commercials_commercial_mgmt_sea_freight_view');
            Route::get('commercial-mgmt.air-freight-premium', [CommercialMgmtController::class, 'AirFreightPremium'])->name('commercial-mgmt.air-freight-premium.index')->middleware('can:crm_commercials_commercial_mgmt_air_freight_premium_view');
        });

        Route::group(['prefix' => 'sales', 'as' => 'sales.', 'middleware' => 'can:crm_sales_view'], function () {
            Route::get('leads', [SalesController::class, 'Leads'])->name('leads.index')->middleware('can:crm_sales_leads_view');
            Route::get('salescampaign', [SalesController::class, 'SalesCampaign'])->name('sales-campaign.index')->middleware('can:crm_sales_sales_campaign_view');
            Route::get('summary', [SalesController::class, 'SalesCampaignSummary'])->name('sales-campaign.summary.index')->middleware('can:crm_sales_sales_campaign_summary_view');
            Route::get('audiencesegmentation', [SalesController::class, 'AudienceSegmentaion'])->name('audience-segmentation.index')->middleware('can:crm_sales_audience_segmentation_view');
            Route::get('leads/{id}/edit', [SalesController::class, 'EditLeads'])->name('leads.edit')->middleware('can:crm_sales_leads_edit');
            Route::get('leads.qualification-mgmt', [SalesController::class, 'Qualification'])->name('leads.qualification-mgmt.index')->middleware('can:crm_sales_leads_qualification_mgmt_view');
            Route::get('opportunities', [SalesController::class, 'Opportunities'])->name('opportunities.index')->middleware('can:crm_sales_opportunities_view');
            Route::get('opportunities/{id}/edit', [SalesController::class, 'EditOpportunities'])->name('opportunities.edit')->middleware('can:crm_sales_opportunities_view');
            Route::get('opportunities.opportunity-status', [SalesController::class, 'OppStatus'])->name('opportunities.opportunity-status.index')->middleware('can:crm_sales_opportunities_opportunity_status_view');
            Route::get('opportunities.sales-stage', [SalesController::class, 'SalesStage'])->name('opportunities.sales-stage.index')->middleware('can:crm_sales_opportunities_sales_stage_view');
            Route::get('booking-dropdown-list', [SalesController::class, 'BookingDropList'])->name('booking-dropdown-list.index')->middleware('can:crm_sales_booking_dropdown_list_view');
            Route::get('booking-dropdown-list.timeslot', [SalesController::class, 'BookingTimeSlot'])->name('booking-dropdown-list.timeslot.index')->middleware('can:crm_sales_booking_dropdown_list_timeslot_view');
            Route::get('booking-mgmt', [SalesController::class, 'BookingMgmt'])->name('booking-mgmt.index')->middleware('can:crm_sales_booking_mgmt_view');
            Route::get('booking-mgmt.booking-history', [SalesController::class, 'BookingHistory'])->name('booking-mgmt.booking-history.index2')->middleware('can:crm_sales_booking_mgmt_booking_history_view');

            Route::get('rate-calculator', [RateCalculatorController::class, 'index'])->name('rate-calculator.index')->middleware('can:crm_sales_rate_calculator_view');
            Route::get('charges-management', [ChargesManagementController::class, 'index'])->name('charges-management.index')->middleware('can:crm_sales_charges_management_view');
            Route::get('quota-', [QuotaController::class, 'index'])->name('quota.index')->middleware('can:crm_sales_quota_view');
            Route::get('quota-/stakeholder-category', [StakeholderCategoryController::class, 'index'])->name('quota.stakeholder-category.index')->middleware('can:crm_sales_quota_stakeholder_category_view');

            Route::get('quotation', [QuotationController::class, 'index'])->name('quotation.index')->middleware('can:crm_sales_quotation_view');
            Route::get('quotation/{id}/{from_sr}', [QuotationController::class, 'showQuotation'])->name('quotation.index-quotation')->middleware('can:crm_sales_quotation_view');
            Route::get('quotation/{id}/{from_sr}/print-generated-quotation', [QuotationController::class, 'printGeneratedQuotation'])->name('print-generated-quotation');
        });

        Route::group(['prefix' => 'activities', 'as' => 'activities.', 'middleware' => 'can:crm_activities_view'], function () {
            Route::get('activities', [ActivitiesController::class, 'Activities'])->name('activities.index')->middleware('can:crm_activities_view');
            Route::get('activities.schedule-meeting', [ActivitiesController::class, 'schedameeting'])->name('schedule-meeting.index')->middleware('can:crm_activities_schedule_meeting_view');
        });

        Route::group(['prefix' => 'contracts', 'as' => 'contracts.', 'middleware' => 'can:crm_contracts_view'], function () {
            Route::get('requirements-mgmt', [ContractsController::class, 'ReqMgmt'])->name('requirements-mgmt.index')->middleware('can:crm_contracts_requirements_mgmt_view');
            Route::get('accounts-application', [ContractsController::class, 'AccApp'])->name('accounts-application.index')->middleware('can:crm_contracts_accounts_application_view');
        });
    });

    Route::group(['prefix' => 'oims', 'as' => 'oims.'], function () {
        Route::group(['prefix' => 'reports', 'as' => 'reports.', 'middleware' => 'can:oims_reports_view'], function () {
            Route::get('o-management-reports', [ReportsController::class, 'orderManagementReports'])->name('order-management-reports.index')->middleware('can:oims_reports_order_management_reports_view');
        });

        Route::group(['prefix' => 'order-management', 'as' => 'order-management.', 'middleware' => 'can:oims_order_management_view'], function () {
            Route::get('team-route-assignment', [OrderManagementController::class, 'teamRouteAssignmentOM'])->name('team-route-assignment.index')->middleware('can:oims_order_management_team_route_assignment_view');
            Route::get('team-route-assignment/{id}/print', [OrderManagementController::class, 'printTR'])->name('team-route-assignment.print');
            Route::get('team-route-assignment.route-category', [OrderManagementController::class, 'routeCategoryOM'])->name('team-route-assignment.route-category.index')->middleware('can:oims_order_management_team_route_assignment_route_category_view');
            Route::get('team-route-assignment.area-mgmt', [OrderManagementController::class, 'areaOM'])->name('team-route-assignment.area-mgmt.index')->middleware('can:oims_order_management_team_route_assignment_area_mgmt_view');
            Route::get('team-route-assignment.quadrant-mgmt', [OrderManagementController::class, 'quadrantOM'])->name('team-route-assignment.quadrant-mgmt.index')->middleware('can:oims_order_management_team_route_assignment_quadrant_mgmt_view');
            Route::get('team-route-assignment.team-mgmt', [OrderManagementController::class, 'teamOM'])->name('team-route-assignment.team-mgmt.index')->middleware('can:oims_order_management_team_route_assignment_team_mgmt_view');
            Route::get('t-e-dropdown-mgmt', [OrderManagementController::class, 'tEDropdowMgmtOM'])->name('t-e-dropdown-mgmt.index')->middleware('can:oims_order_management_t_e_dropdown_mgmt_view');
            Route::get('t-e-dropdown-mgmt.branch-mgmt', [OrderManagementController::class, 'branchMgmtOM'])->name('t-e-dropdown-mgmt.branch-mgmt.index')->middleware('can:oims_order_management_t_e_dropdown_mgmt_branch_mgmt_view');
            Route::get('t-e-dropdown-mgmt.transport-mgmt', [OrderManagementController::class, 'transportModeMgmtOM'])->name('t-e-dropdown-mgmt.transport-mgmt.index')->middleware('can:oims_order_management_t_e_dropdown_mgmt_transport_mgmt_view');
            Route::get('t-e-dropdown-mgmt.paymode-mgmt', [OrderManagementController::class, 'payModeMgmtOM'])->name('t-e-dropdown-mgmt.paymode-mgmt.index')->middleware('can:oims_order_management_t_e_dropdown_mgmt_paymode_mgmt_view');
            Route::get('t-e-dropdown-mgmt.service-mgmt', [OrderManagementController::class, 'serviceMgmtOM'])->name('t-e-dropdown-mgmt.service-mgmt.index')->middleware('can:oims_order_management_t_e_dropdown_mgmt_service_mgmt_view');
            Route::get('t-e-dropdown-mgmt.odaopa-mgmt', [OrderManagementController::class, 'odaopaMgmtOM'])->name('t-e-dropdown-mgmt.odaopa-mgmt.index')->middleware('can:oims_order_management_t_e_dropdown_mgmt_odaopa_mgmt_view');
            Route::get('t-e-dropdown-mgmt.service-area-mgmt', [OrderManagementController::class, 'serviceAreaMgmtOM'])->name('t-e-dropdown-mgmt.service-area-mgmt.index')->middleware('can:oims_order_management_t_e_dropdown_mgmt_service_area_mgmt_view');
            Route::get('t-e-dropdown-mgmt.serviceability-mgmt', [OrderManagementController::class, 'serviceabilityMgmtOM'])->name('t-e-dropdown-mgmt.serviceability-mgmt.index')->middleware('can:oims_order_management_t_e_dropdown_mgmt_serviceability_mgmt_view');
            Route::get('t-e-dropdown-mgmt.zipcode-mgmt', [OrderManagementController::class, 'zipcodeMgmtOM'])->name('t-e-dropdown-mgmt.zipcode-mgmt.index')->middleware('can:oims_order_management_t_e_dropdown_mgmt_zipcode_mgmt_view');
            Route::get('t-e-dropdown-mgmt.barangay-mgmt', [OrderManagementController::class, 'barangayMgmtOM'])->name('t-e-dropdown-mgmt.barangay-mgmt.index')->middleware('can:oims_order_management_t_e_dropdown_mgmt_barangay_mgmt_view');
            Route::get('t-e-dropdown-mgmt.municipality-mgmt', [OrderManagementController::class, 'municipalityMgmtOM'])->name('t-e-dropdown-mgmt.municipality-mgmt.index')->middleware('can:oims_order_management_t_e_dropdown_mgmt_municipality_mgmt_view');
            Route::get('t-e-dropdown-mgmt.province-mgmt', [OrderManagementController::class, 'provinceMgmtOM'])->name('t-e-dropdown-mgmt.province-mgmt.index')->middleware('can:oims_order_management_t_e_dropdown_mgmt_province_mgmt_view');
            Route::get('t-e-dropdown-mgmt.dangerous-mgmt', [OrderManagementController::class, 'dangerousMgmtOM'])->name('t-e-dropdown-mgmt.dangerous-mgmt.index')->middleware('can:oims_order_management_t_e_dropdown_mgmt_dangerous_mgmt_view');
            Route::get('t-e-dropdown-mgmt.packaging-mgmt', [OrderManagementController::class, 'packagingMgmtOM'])->name('t-e-dropdown-mgmt.packaging-mgmt.index')->middleware('can:oims_order_management_t_e_dropdown_mgmt_packaging_mgmt_view');
            Route::get('t-e-dropdown-mgmt.reason-mgmt', [OrderManagementController::class, 'reasonMgmtOM'])->name('t-e-dropdown-mgmt.reason-mgmt.index')->middleware('can:oims_order_management_t_e_dropdown_mgmt_reason_mgmt_view');
            Route::get('t-e-dropdown-mgmt.cargo-mgmt', [OrderManagementController::class, 'cargoMgmtOM'])->name('t-e-dropdown-mgmt.cargo-mgmt.index')->middleware('can:oims_order_management_t_e_dropdown_mgmt_cargo_mgmt_view');
            Route::get('t-e-dropdown-mgmt.remarks-mgmt', [OrderManagementController::class, 'remarksMgmtOM'])->name('t-e-dropdown-mgmt.remarks-mgmt.index')->middleware('can:oims_order_management_t_e_dropdown_mgmt_remarks_mgmt_view');
            Route::get('waybill-registry', [OrderManagementController::class, 'waybillRegistryOM'])->name('waybill-registry.index')->middleware('can:oims_order_management_waybill_registry_view');
            Route::get('dispatch-console', [OrderManagementController::class, 'dispatchConsole'])->name('dispatch-console.index')->middleware('can:oims_order_management_dispatch_console_view');
            Route::get('transaction-entry', [OrderManagementController::class, 'transactionEntryOM'])->name('transaction-entry.index')->middleware('can:oims_order_management_transaction_entry_view');
            Route::get('transaction-entry/{id}/create', [OrderManagementController::class, 'transactionEntryOMCreate'])->name('transaction-entry.create');
            Route::get('transaction-summary', [OrderManagementController::class, 'transactionSumOM'])->name('transaction-summary.index')->middleware('can:oims_order_management_transaction_summary_view');
            Route::get('waybill-summary', [OrderManagementController::class, 'transactionEntryWS'])->name('transaction-entry.waybill-summary')->middleware('can:oims_order_management_transaction_entry_waybill_summary');
            Route::get('transaction-entry.print', [OrderManagementController::class, 'printWS'])->name('transaction-entry.print');
            Route::get('transaction-entry.printsticker', [OrderManagementController::class, 'printSTCKR'])->name('transaction-entry.printsticker');
            Route::get('e-dtr', [OrderManagementController::class, 'eDtr'])->name('e-dtr.index')->middleware('can:oims_order_management_e_dtr_view');
            Route::get('e-dtr/{id}/print', [OrderManagementController::class, 'print'])->name('e-dtr.print');
            Route::get('vehicle-utilization', [OrderManagementController::class, 'vUtil'])->name('vehicle-utilization.index')->middleware('can:oims_order_management_vehicle_utilization_view');
        });
    });

    Route::group(['prefix' => 'ticketing', 'as' => 'ticketing.'], function () {
        Route::get('category-management', [TicketCategoryManagementController::class, 'index'])->name('category-management.index')->middleware('can:ticketing_category_management_view');
        Route::get('subcategory-management', [TicketSubcategoryMgmtController::class, 'index'])->name('subcategory-management.index')->middleware('can:ticketing_subcategory_management_view');
        Route::group(['prefix' => 'ticket-management', 'as' => 'ticket-management.'], function () {
            Route::get('dashboard', [TicketManagementController::class, 'index'])->name('dashboard.index')->middleware('can:ticket_management_dashboard_view');
            Route::get('all-tickets', [TicketManagementController::class, 'indexalltickets'])->name('all-tickets.indexalltickets')->middleware('can:ticket_management_all_tickets_view');
            Route::get('my-tickets', [TicketManagementController::class, 'indexmytickets'])->name('my-tickets.indexmytickets')->middleware('can:ticket_management_my_tickets_view');
            Route::get('my-tasks', [TicketManagementController::class, 'indexmytasks'])->name('my-tasks.indexmytasks')->middleware('can:ticket_management_my_tasks_view');
        });
    });
});
