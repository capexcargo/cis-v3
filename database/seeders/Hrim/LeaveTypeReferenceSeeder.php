<?php

namespace Database\Seeders\Hrim;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LeaveTypeReferenceSeeder extends Seeder
{

    public function run()
    {
        $hrim_leave_type_reference = [
            [
                'id' => 1,
                'code' => 'sick_leave',
                'display' => 'Sick Leave',
            ],
            [
                'id' => 2,
                'code' => 'vacation_leave',
                'display' => 'Vacation Leave',
            ],
            [
                'id' => 3,
                'code' => 'emergency_leave',
                'display' => 'Emergency Leave',
            ],
            [
                'id' => 4,
                'code' => 'others',
                'display' => 'Others',
            ],
        ];

        DB::table('hrim_leave_type_reference')->insert($hrim_leave_type_reference);
    }
}
