<?php

namespace Database\Seeders\Hrim\PayrollManagement;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HolidayTypeReferenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $holiday_type = array(
            array(
                "id" => 1,
                "name" => "Non Working Regular Holiday",
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 2,
                "name" => "Non Working Special Holiday",
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            )
        );
        DB::table('hrim_holiday_type')->insert($holiday_type);
    }
}
