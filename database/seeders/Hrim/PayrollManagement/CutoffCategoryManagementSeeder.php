<?php

namespace Database\Seeders\Hrim\PayrollManagement;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CutoffCategoryManagementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hrim_cutoff_category_reference = array(
            array(
                "id" => 1,
                "name" => "Salary Only",
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 2,
                "name" => "OT Only",
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 3,
                "name" => "Salary and OT",
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
        );
        
        DB::table('hrim_cutoff_category_reference')->insert($hrim_cutoff_category_reference);
    }
}
