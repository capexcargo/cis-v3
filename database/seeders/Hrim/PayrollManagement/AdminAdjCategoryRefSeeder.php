<?php

namespace Database\Seeders\Hrim\PayrollManagement;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminAdjCategoryRefSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adjustment_category = array(
            array(
                "id" => 1,
                "display" => "Category 1",
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 2,
                "display" => "Category 2",
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 3,
                "display" => "Category 3",
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            )
        );
        
        DB::table('hrim_admin_adj_category_reference')->insert($adjustment_category);
    }
}
