<?php

namespace Database\Seeders\Hrim\RecruitmentAndHiring;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmploymentCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $request_request = array(
            array(
                "id" => 1,
                "code" => "regular",
                "display" => "Regular Position",
                "sick_leave" => 10,
                "vacation_leave" => 10,
                "is_visible" => 1,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 2,
                "code" => "non-regular",
                "display" => "Non-Regular Position",
                "sick_leave" => 5,
                "vacation_leave" => 5,
                "is_visible" => 1,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
        );
        DB::table('hrim_employment_category')->insert($request_request);
    }
}
