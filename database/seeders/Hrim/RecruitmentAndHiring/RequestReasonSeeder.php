<?php

namespace Database\Seeders\Hrim\RecruitmentAndHiring;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RequestReasonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $request_request = array(
            array(
                "id" => 1,
                "code" => "reason1",
                "display" => "New Approved Position",
                "deleted_at" => NULL,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 2,
                "code" => "reason2",
                "display" => "Replacement",
                "deleted_at" => NULL,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
        );
        DB::table('hrim_requisition_reason')->insert($request_request);
    }
}
