<?php

namespace Database\Seeders\Hrim\RecruitmentAndHiring;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ApplicantStatusMgmtSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hrim_applicant_tracking_status = array(
            array(
                "id" => 1,
                "code" => "interview",
                "display" => "For HR Interview",
                "created_by" => 1,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 2,
                "code" => "assessment",
                "display" => "For Assessment",
                "created_by" => 1,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 3,
                "code" => "manager interview",
                "display" => "For Hiring Manager’s Interview",
                "created_by" => 1,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 4,
                "code" => "JO",
                "display" => "Job Offer",
                "created_by" => 1,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 5,
                "code" => "Bg Check",
                "display" => "For hiring – Background Check",
                "created_by" => 1,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 6,
                "code" => "Ref Check",
                "display" => "For hiring - Reference Check",
                "created_by" => 1,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 7,
                "code" => "Reqs Processing",
                "display" => "For hiring – Requirements Processing",
                "created_by" => 1,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 8,
                "code" => "CS",
                "display" => "Contract Signing",
                "created_by" => 1,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 9,
                "code" => "Hired",
                "display" => "Hired",
                "created_by" => 1,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
        );
        DB::table('hrim_applicant_tracking_status')->insert($hrim_applicant_tracking_status);
    }
}
