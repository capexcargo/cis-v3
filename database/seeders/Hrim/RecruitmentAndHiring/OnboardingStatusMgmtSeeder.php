<?php

namespace Database\Seeders\Hrim\RecruitmentAndHiring;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OnboardingStatusMgmtSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $onboarding_status = array(
            array(
                "id" => 1,
                "code" => "preboarding",
                "display" => "Preboarding",
                "description" => "Sign Contract",
                "created_by" => 1,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 2,
                "code" => "admin_provisioning",
                "display" => "Admin and Provisioning",
                "description" => "Prepare IT, phone, workspace, car, etc.",
                "created_by" => 1,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 3,
                "code" => "orientation_training",
                "display" => "Orientation and Training",
                "description" => "Company Intro",
                "created_by" => 1,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 4,
                "code" => "onboarding_evaluation",
                "display" => "Onboarding Evaluation",
                "description" => "Post-Onboarding evaluation interview",
                "created_by" => 1,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
        );
        DB::table('hrim_onboarding_status')->insert($onboarding_status);
    }
}
