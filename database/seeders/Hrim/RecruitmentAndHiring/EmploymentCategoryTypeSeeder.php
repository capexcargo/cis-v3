<?php

namespace Database\Seeders\Hrim\RecruitmentAndHiring;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmploymentCategoryTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ec_type = array(
            array(
                "id" => 1,
                "code" => "non-regular1",
                "display" => "Project Employment",
                "employment_category_id" => 2,
                "deleted_at" => null,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 2,
                "code" => "non-regular2",
                "display" => "Casual / Contractual",
                "employment_category_id" => 2,
                "deleted_at" => null,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 3,
                "code" => "non-regular3",
                "display" => "Term / Fixed Contract",
                "employment_category_id" => 2,
                "deleted_at" => null,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 4,
                "code" => "non-regular4",
                "display" => "Renewal / Contract Extension",
                "employment_category_id" => 2,
                "deleted_at" => null,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
        );
        DB::table('hrim_employment_category_type')->insert($ec_type);
    }
}
