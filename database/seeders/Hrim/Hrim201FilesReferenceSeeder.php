<?php

namespace Database\Seeders\Hrim;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Hrim201FilesReferenceSeeder extends Seeder
{
    public function run()
    {
        $hrim_201_files_reference = [
            [
                'id' => 1,
                'code' => 'education_/_transcripts_/_diplomas',
                'display' => 'Education / Transcripts / Diplomas',
            ],
            [
                'id' => 2,
                'code' => 'performance_assessments',
                'display' => 'Performance Assessments',
            ],
            [
                'id' => 3,
                'code' => 'clearance',
                'display' => 'Clearance',
            ],
            [
                'id' => 4,
                'code' => 'corrective_actions',
                'display' => 'Corrective Actions',
            ],
            [
                'id' => 5,
                'code' => 'hiring_requirements',
                'display' => 'Hiring Requirements',
            ],
            [
                'id' => 6,
                'code' => 'nbi_clearance',
                'display' => 'NBI Clearance',
            ],
            [
                'id' => 7,
                'code' => 'pre-employment_medical_examination',
                'display' => 'Pre-employment Medical Examination',
            ],
            [
                'id' => 8,
                'code' => 'id_information_slip',
                'display' => 'ID Information Slip',
            ],
            [
                'id' => 9,
                'code' => 'employment_contract',
                'display' => 'Employment Contract',
            ],
            [
                'id' => 10,
                'code' => 'sss_or_ei',
                'display' => 'SSS or EI',
            ],
            [
                'id' => 11,
                'code' => 'copy_of_valid_id',
                'display' => 'Copy of Valid ID',
            ],
            [
                'id' => 12,
                'code' => 'resume_or_cv',
                'display' => 'Resume or CV',
            ],
            [
                'id' => 13,
                'code' => 'disciplinary_history',
                'display' => 'Disciplinary History',
            ]
        ];

        DB::table('hrim_201_files_reference')->insert($hrim_201_files_reference);
    }
}
