<?php

namespace Database\Seeders\Hrim\EmployeeManagement;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmploymentStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hrim_employment_status = array(
            array(
                "id" => 1,
                "code" => "regular",
                "display" => "Regular",
                "is_visible" => 1,
                "created_at" => "2022-07-25 11:24:05",
                "updated_at" => NULL,
            ),
            array(
                "id" => 2,
                "code" => "non-regular",
                "display" => "Non-Regular",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 3,
                "code" => "probationary",
                "display" => "Probationary",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 4,
                "code" => "contractual",
                "display" => "Contractual",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
        );
        
        DB::table('hrim_employment_status')->insert($hrim_employment_status);
    }
}
