<?php

namespace Database\Seeders\Hrim\EmployeeManagement\EmployeeInformation;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmploymentStatusSeeder extends Seeder
{
    
    public function run()
    {
        $hrim_employment_status = [
            [
                'id' => 1,
                'code' => 'probationary',
                'display' => 'Probationary',
            ],
            [
                'id' => 2,
                'code' => 'contractual_and_regular',
                'display' => 'Contractual and Regular',
            ]
        ];

        DB::table('hrim_employment_status')->insert($hrim_employment_status);
    }
}
