<?php

namespace Database\Seeders\Hrim\EmployeeManagement\EmployeeInformation;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EducationalAttainmentSeeder extends Seeder
{
    public function run()
    {
        $hrim_ea_level_reference = [
            [
                'id' => 1,
                'code' => 'elementary_undergraduate',
                'display' => 'Elementary Undergraduate',
            ],
            [
                'id' => 2,
                'code' => 'elementary_graduate',
                'display' => 'Elementary Graduate',
            ],
            [
                'id' => 3,
                'code' => 'highschool_undergraduate',
                'display' => 'Highschool Undergraduate',
            ],
            [
                'id' => 4,
                'code' => 'highschool_graduate',
                'display' => 'Highschool Graduate',
            ],
            [
                'id' => 5,
                'code' => 'college_undergraduate',
                'display' => 'College Undergraduate',
            ],
            [
                'id' => 6,
                'code' => 'college_graduate',
                'display' => 'College Graduate',
            ],
            [
                'id' => 7,
                'code' => 'post_graduate',
                'display' => 'Post Graduate',
            ],
            [
                'id' => 8,
                'code' => 'vocational_education',
                'display' => 'Vocational Education',
            ]
        ];

        DB::table('hrim_ea_level_reference')->insert($hrim_ea_level_reference);
    }
}
