<?php

namespace Database\Seeders\Hrim;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ResignationReasonReferenceSeeder extends Seeder
{

    public function run()
    {
        $hrim_resignation_reason_reference = array(
            array(
                "id" => 1,
                "code" => "voluntary_resignation",
                "display" => "Voluntary Resignation",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 2,
                "code" => "awol",
                "display" => "AWOL",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 3,
                "code" => "retired",
                "display" => "Retired",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 4,
                "code" => "terminated",
                "display" => "Terminated",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 5,
                "code" => "early_retirement",
                "display" => "Early Retirement",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 6,
                "code" => "retrenched",
                "display" => "Retrenched",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 7,
                "code" => "redundancy",
                "display" => "Redundancy",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
        );

        DB::table('hrim_resignation_reason_reference')->insert($hrim_resignation_reason_reference);
    }
}
