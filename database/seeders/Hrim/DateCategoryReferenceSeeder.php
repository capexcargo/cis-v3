<?php

namespace Database\Seeders\Hrim;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DateCategoryReferenceSeeder extends Seeder
{
    public function run()
    {
        $hrim_date_category_reference = array(
            array(
                "id" => 1,
                "code" => "regular_days",
                "display" => "Regular Days",
                "rate_percentage" => "1",
                "is_visible" => 1,
                "created_by" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 2,
                "code" => "regular_holiday",
                "display" => "Regular Holiday",
                "rate_percentage" => "1",
                "is_visible" => 1,
                "created_by" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 3,
                "code" => "special_holiday",
                "display" => "Special Holiday",
                "rate_percentage" => "0.3",
                "is_visible" => 1,
                "created_by" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),

            array(
                "id" => 4,
                "code" => "rest_day",
                "display" => "Rest Day",
                "rate_percentage" => "2.6",

                "is_visible" => 1,
                "created_by" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
        );
        DB::table('hrim_date_category_reference')->insert($hrim_date_category_reference);
    }
}
