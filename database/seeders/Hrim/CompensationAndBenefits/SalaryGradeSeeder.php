<?php


namespace Database\Seeders\Hrim\CompensationAndBenefits;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SalaryGradeSeeder extends Seeder
{
    public function run()
    {
        $salary_grade = array(
            array(
                "id" => 1,
                "grade" => "A1",
                "minimun_amount" => null,
                "maximum_amount" => null,
                "created_by" => 4,
                "created_at" => "2022-09-13 10:13:09",
                "updated_at" => "2022-09-13 10:13:10",
            ),
            array(
                "id" => 2,
                "grade" => "A2",
                "minimun_amount" => null,
                "maximum_amount" => null,
                "created_by" => 4,
                "created_at" => "2022-09-13 10:13:09",
                "updated_at" => "2022-09-13 10:13:10",
            ),
            array(
                "id" => 3,
                "grade" => "A3",
                "minimun_amount" => null,
                "maximum_amount" => null,
                "created_by" => 4,
                "created_at" => "2022-09-13 10:13:09",
                "updated_at" => "2022-09-13 10:13:10",
            ),
            array(
                "id" => 4,
                "grade" => "B1",
                "minimun_amount" => null,
                "maximum_amount" => null,
                "created_by" => 4,
                "created_at" => "2022-09-13 10:13:09",
                "updated_at" => "2022-09-13 10:13:10",
            ),
            array(
                "id" => 5,
                "grade" => "B2",
                "minimun_amount" => null,
                "maximum_amount" => null,
                "created_by" => 4,
                "created_at" => "2022-09-13 10:13:09",
                "updated_at" => "2022-09-13 10:13:10",
            ),
            array(
                "id" => 6,
                "grade" => "B3",
                "minimun_amount" => null,
                "maximum_amount" => null,
                "created_by" => 4,
                "created_at" => "2022-09-13 10:13:09",
                "updated_at" => "2022-09-13 10:13:10",
            ),
            array(
                "id" => 7,
                "grade" => "C1",
                "minimun_amount" => null,
                "maximum_amount" => null,
                "created_by" => 4,
                "created_at" => "2022-09-13 10:13:09",
                "updated_at" => "2022-09-13 10:13:10",
            ),
            array(
                "id" => 8,
                "grade" => "C2",
                "minimun_amount" => null,
                "maximum_amount" => null,
                "created_by" => 4,
                "created_at" => "2022-09-13 10:13:09",
                "updated_at" => "2022-09-13 10:13:10",
            ),
            array(
                "id" => 9,
                "grade" => "C3",
                "minimun_amount" => null,
                "maximum_amount" => null,
                "created_by" => 4,
                "created_at" => "2022-09-13 10:13:09",
                "updated_at" => "2022-09-13 10:13:10",
            ),
            array(
                "id" => 10,
                "grade" => "D1",
                "minimun_amount" => null,
                "maximum_amount" => null,
                "created_by" => 4,
                "created_at" => "2022-09-13 10:13:09",
                "updated_at" => "2022-09-13 10:13:10",
            ),
            array(
                "id" => 11,
                "grade" => "D2",
                "minimun_amount" => null,
                "maximum_amount" => null,
                "created_by" => 4,
                "created_at" => "2022-09-13 10:13:09",
                "updated_at" => "2022-09-13 10:13:10",
            ),
        );
        DB::table('hrim_salary_grade')->insert($salary_grade);
    }
}
