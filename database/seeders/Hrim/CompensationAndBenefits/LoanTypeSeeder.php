<?php


namespace Database\Seeders\Hrim\CompensationAndBenefits;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LoanTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $loan_type = array(
            array(
                "id" => 1,
                "name" => "Cash Advance",
                'loan_type_category' => 0,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 2,
                "name" => "Sss Loan",
                'loan_type_category' => 1,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 3,
                "name" => "Pagibig Loan",
                'loan_type_category' => 1,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 4,
                "name" => "Philhealth Loan",
                'loan_type_category' => 1,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 5,
                "name" => "Salary Loan",
                'loan_type_category' => 0,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 6,
                "name" => "Educational Loan",
                'loan_type_category' => 0,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 7,
                "name" => "Emergency Loan",
                'loan_type_category' => 0,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 8,
                "name" => "Personal Loan",
                'loan_type_category' => 0,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 9,
                "name" => "Other Loans and Deductions",
                'loan_type_category' => 0,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),

        );
        DB::table('hrim_loan_type')->insert($loan_type);
    }
}
