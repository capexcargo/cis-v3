<?php

namespace Database\Seeders\Hrim\TalentManagement;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BreakdownPctSeeder extends Seeder
{
    public function run()
    {
        $breakdown_pct = array(
            array(
                "id" => 1,
                "job_level_id" => 1,
                "goals" => 20,
                "core_values" => 40,
                "leadership_competencies" => 40,
                "created_at" => "2022-08-08 10:13:09",
                "updated_at" => "2022-08-08 10:13:10",
            ), 
            array(
                "id" => 2,
                "job_level_id" => 2,
                "goals" => 30,
                "core_values" => 35,
                "leadership_competencies" => 35,
                "created_at" => "2022-08-08 10:13:09",
                "updated_at" => "2022-08-08 10:13:10",
            ),
            array(
                "id" => 3,
                "job_level_id" => 3,
                "goals" => 40,
                "core_values" => 30,
                "leadership_competencies" => 30,
                "created_at" => "2022-08-08 10:13:09",
                "updated_at" => "2022-08-08 10:13:10",
            ),
            array(
                "id" => 4,
                "job_level_id" => 4,
                "goals" => 50,
                "core_values" => 25,
                "leadership_competencies" => 25,
                "created_at" => "2022-08-08 10:13:09",
                "updated_at" => "2022-08-08 10:13:10",
            ), 
            array(
                "id" => 5,
                "job_level_id" => 5,
                "goals" => 60,
                "core_values" => 20,
                "leadership_competencies" => 20,
                "created_at" => "2022-08-08 10:13:09",
                "updated_at" => "2022-08-08 10:13:10",
            ),
            array(
                "id" => 6,
                "job_level_id" => 6,
                "goals" => 70,
                "core_values" => 20,
                "leadership_competencies" => 10,
                "created_at" => "2022-08-08 10:13:09",
                "updated_at" => "2022-08-08 10:13:10",
            ),

        );
        DB::table('hrim_performance_breakdown_pct')->insert($breakdown_pct);
    }
}
