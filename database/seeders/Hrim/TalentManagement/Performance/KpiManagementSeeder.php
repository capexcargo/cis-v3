<?php

namespace Database\Seeders\Hrim\TalentManagement\Performance;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KpiManagementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kpi = array(
            array(
                "id" => 1,
                "name" => "Compliance to company policies and code of conduct / 10 pts",
                "quarter" => 1,
                "year" => 2021,
                "points" => 10,
                "target" => '',
                "remarks" => '',
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ), 
            array(
                "id" => 2,
                "name" => "Flexibility and ability to be assigned to other area / task",
                "quarter" => 1,
                "year" => 2021,
                "points" => 5,
                "target" => '',
                "remarks" => '',
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ), 
            array(
                "id" => 3,
                "name" => "Knowledge in CaPEx Service",
                "quarter" => 1,
                "year" => 2021,
                "points" => 10,
                "target" => '',
                "remarks" => '',
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ), 
            array(
                "id" => 4,
                "name" => "On-time Delivery",
                "quarter" => 1,
                "year" => 2021,
                "points" => 10,
                "target" => '100% Delivery within lead time',
                "remarks" => '',
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ), 
            array(
                "id" => 5,
                "name" => "Road Safety",
                "quarter" => 4,
                "year" => 2021,
                "points" => 10,
                "target" => 'Zero Incident',
                "remarks" => '',
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ), 
            array(
                "id" => 6,
                "name" => "Traffic Compliance",
                "quarter" => 4,
                "year" => 2021,
                "points" => 10,
                "target" => 'Zero Incident',
                "remarks" => '',
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),

        );
        DB::table('hrim_kpi')->insert($kpi);
    }
}
