<?php

namespace Database\Seeders\Hrim\TalentManagement\Performance;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CoreValueManagementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $core_value = array(
            array(
                "id" => 1,
                "description" => 'Customer Obsession - Helps both internal and external customers. The employee sees his/her job as helping customers, more than selling or servicing a product. Employee is aware that product/service is a vehicle for making the biggest impact the people he/she serves.',
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ), 
            array(
                "id" => 2,
                "description" => 'Accountability - Owns, takes initiative, steps up, and does what is best for the business. Takes up responsibility over results and outcomes.',
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 3,
                "description" => 'Passion - Finds meaning and happiness in what one does. And, although one may struggle, never stops which elevates the EE beyond his self-interest.',
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ), 
            array(
                "id" => 4,
                "description" => "Engagement - Puts everything into the realization of the company's goals. Engages himself at work that gets done more.",
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 5,
                "description" => "Excellence - Goes the extra mile, opens himself to new learning and accepting of change, strives to be a valuable part of the team, challenges oneself to be self-motivated even things are not going his way.",
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ), 

        );
        DB::table('hrim_core_value')->insert($core_value);
    }
}
