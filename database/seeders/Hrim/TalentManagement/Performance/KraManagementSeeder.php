<?php

namespace Database\Seeders\Hrim\TalentManagement\Performance;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KraManagementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kra = array(
            array(
                "id" => 1,
                "name" => 'A. SCORECARD / PROCESS / TECHNICAL PERFORMANCE ("Be Best" Initiatives / "Be Fast" Initiatives)',
                "type" => 2,
                "quarter" => 1,
                "year" => 2021,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ), 
            array(
                "id" => 2,
                "name" => "B. CUSTOMER CARE (Best Experience Initiatives)",
                "type" => 6,
                "quarter" => 1,
                "year" => 2021,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ), 
            array(
                "id" => 3,
                "name" => "C. FINANCIALS / BUSINESS ACCUMEN (DCD Initiatives / DRU Initiatives)",
                "type" => 6,
                "quarter" => 1,
                "year" => 2021,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ), 
            array(
                "id" => 4,
                "name" => 'D. PEOPLE MANAGEMENT ("Be Engaged" Initiatives)',
                "type" => 6,
                "quarter" => 1,
                "year" => 2021,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ), 
            array(
                "id" => 5,
                "name" => 'E. INNOVATION & DEVELOPMENT ("Be New" Initiatives)',
                "type" => 6,
                "quarter" => 4,
                "year" => 2021,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ), 

        );
        DB::table('hrim_kra')->insert($kra);
    }
}
