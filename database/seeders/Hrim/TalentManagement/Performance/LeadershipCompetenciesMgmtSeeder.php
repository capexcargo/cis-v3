<?php

namespace Database\Seeders\Hrim\TalentManagement\Performance;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LeadershipCompetenciesMgmtSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $leadership_competencies = array(
            array(
                "id" => 1,
                "description" => 'Building Effective Teams - Blends people into teams when needed; creates strong morale and spirit in his/her team; shares wins and successes, fosters open dialogue, lets people finish and be responsible for their work.',
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ), 
            array(
                "id" => 2,
                "description" => 'Business Acumen - Knows how the business works; knowledgeable in current and possible future policies, practices, trends, and information affecting his/her business and organization, knows the competition, is aware of how strategies and tactics work in marketplace.',
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 3,
                "description" => 'Decision Quality - Makes good decisions based upon a mixture of analysis, wisdom experience, and judgement; most of his/her solutions and suggestions turn out to be correct and accurate when judged over time; sought out by others for advice and solutions',
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ), 
            array(
                "id" => 4,
                "description" => 'Innovation Management (Continuous Improvement) - is good at bringing the creative ideas of others forward. Generates novel and valuable ideas and uses these ideas to develop new or improved processes.',
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 5,
                "description" => "Priority Setting - Spends his/her time and the time of others on what's important; quickly zeros in on the critical few and puts the trivial many aside; can quickly sense what will help or hinder accomplishing a goal.",
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ), 

        );
        DB::table('hrim_leadership_competencies')->insert($leadership_competencies);
    }
}
