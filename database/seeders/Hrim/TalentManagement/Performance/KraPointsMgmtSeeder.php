<?php

namespace Database\Seeders\Hrim\TalentManagement\Performance;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KraPointsMgmtSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kra_points = array(
            array(
                "id" => 1,
                "points" => 1,
                "description" => "Needs Improvement",
                "quarter" => 2,
                "year" => 2022,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            
            array(
                "id" => 2,
                "points" => 2,
                "description" => "Satisfactory",
                "quarter" => 2,
                "year" => 2022,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 3,
                "points" => 3,
                "description" => "Good",
                "quarter" => 2,
                "year" => 2022,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 4,
                "points" => 4,
                "description" => "Excellent",
                "quarter" => 2,
                "year" => 2022,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 5,
                "points" => 5,
                "description" => "Outstanding",
                "quarter" => 2,
                "year" => 2022,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),

        );
        DB::table('hrim_kra_points')->insert($kra_points);
    }
}
