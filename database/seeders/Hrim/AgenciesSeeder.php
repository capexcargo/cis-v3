<?php

namespace Database\Seeders\Hrim;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AgenciesSeeder extends Seeder
{

    public function run()
    {
        $hrim_agencies = [
            [
                'id' => 1,
                'name' => 'QForte',
                'description' => '',
                'branch_id' => 1,
            ],
            [
                'id' => 2,
                'name' => 'Altavest',
                'description' => '',
                'branch_id' => 1,
            ],
            [
                'id' => 3,
                'name' => 'Corestaff',
                'description' => '',
                'branch_id' => 1,
            ],
            [
                'id' => 4,
                'name' => 'JNS',
                'description' => '',
                'branch_id' => 1,
            ]
        ];

        DB::table('hrim_agencies')->insert($hrim_agencies);
    }
}
