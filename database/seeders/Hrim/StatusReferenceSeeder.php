<?php

namespace Database\Seeders\Hrim;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusReferenceSeeder extends Seeder
{

    public function run()
    {
        $hrim_status_reference = [
            [
                'id' => 1,
                'code' => 'requested',
                'display' => 'Requested',
            ],
            [
                'id' => 2,
                'code' => 'pending',
                'display' => 'Pending',
            ],
            [
                'id' => 3,
                'code' => 'approved',
                'display' => 'Approved',
            ],
            [
                'id' => 4,
                'code' => 'declined',
                'display' => 'Declined',
            ],
        ];

        DB::table('hrim_status_reference')->insert($hrim_status_reference);
    }
}
