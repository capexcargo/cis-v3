<?php

namespace Database\Seeders\Hrim;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WorkModeReferenceSeeder extends Seeder
{

    public function run()
    {
        $hrim_work_mode_reference = [
            [
                'id' => 1,
                'code' => 'work_on_site',
                'display' => 'Work On Site',
            ],
            [
                'id' => 2,
                'code' => 'work_from_home',
                'display' => 'Work From Home',
            ]
        ];

        DB::table('hrim_work_mode_reference')->insert($hrim_work_mode_reference);
    }
}
