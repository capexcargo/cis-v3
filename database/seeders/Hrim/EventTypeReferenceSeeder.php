<?php

namespace Database\Seeders\Hrim;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventTypeReferenceSeeder extends Seeder
{
    public function run()
    {
        $hrim_event_type_reference = [
            [
                'id' => 1,
                'code' => 'company',
                'display' => 'Company',
            ],
        ];

        DB::table('hrim_event_type_reference')->insert($hrim_event_type_reference);
    }
}
