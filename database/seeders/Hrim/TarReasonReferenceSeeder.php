<?php

namespace Database\Seeders\Hrim;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TarReasonReferenceSeeder extends Seeder
{
    public function run()
    {
        $hrim_tar_reason_reference = [
            [
                'id' => 1,
                'code' => 'missed_punch_in_/_out',
                'display' => 'Missed Punch In / Out',
                'created_by' => 1,
            ],
            [
                'id' => 2,
                'code' => 'new_employee',
                'display' => 'New Employee',
                'created_by' => 1,
            ],
            [
                'id' => 3,
                'code' => 'double_time_punch',
                'display' => 'Double Time Punch',
                'created_by' => 1,
            ],
            [
                'id' => 4,
                'code' => 'offsite_event_/_meeting',
                'display' => 'Offsite Event / Meeting',
                'created_by' => 1,
            ],
            [
                'id' => 5,
                'code' => 'unregistered_time_punch',
                'display' => 'Unregistered Time Punch',
                'created_by' => 1,
            ],
            [
                'id' => 6,
                'code' => 'time_clock_offline',
                'display' => 'Time Clock Offline',
                'created_by' => 1,
            ],
            [
                'id' => 7,
                'code' => 'official_business',
                'display' => 'Official Business',
                'created_by' => 1,
            ],
        ];

        DB::table('hrim_tar_reason_reference')->insert($hrim_tar_reason_reference);
    }
}
