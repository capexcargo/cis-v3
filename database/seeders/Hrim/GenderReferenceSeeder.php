<?php

namespace Database\Seeders\Hrim;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GenderReferenceSeeder extends Seeder
{
    public function run()
    {
        $hrim_gender = [
            [
                'id' => 1,
                'code' => 'male',
                'display' => 'Male',
            ],
            [
                'id' => 2,
                'code' => 'female',
                'display' => 'Female',
            ],
        ];

        DB::table('hrim_gender')->insert($hrim_gender);
    }
}
