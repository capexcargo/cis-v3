<?php

namespace Database\Seeders\Hrim;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LoaTypeReferenceSeeder extends Seeder
{

    public function run()
    {
        $hrim_loa_type_reference = array(
            array(
                "id" => 1,
                "code" => "tar",
                "display" => "TAR",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 2,
                "code" => "sar",
                "display" => "SAR",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 3,
                "code" => "ot",
                "display" => "OT",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 4,
                "code" => "leave",
                "display" => "Leave",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 5,
                "code" => "employee_requisition",
                "display" => "Employee Requisition",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 6,
                "code" => "loans",
                "display" => "Loans",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
        );
        

        DB::table('hrim_loa_type_reference')->insert($hrim_loa_type_reference);
    }
}
