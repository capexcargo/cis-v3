<?php

namespace Database\Seeders\Hrim;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProvinceSeeder extends Seeder
{
    public function run()
    {
        $hrim_province = [
            [
                'id' => 1,
                'code' => 'manila',
                'display' => 'Manila',
            ],
        ];

        DB::table('hrim_province')->insert($hrim_province);
    }
}
