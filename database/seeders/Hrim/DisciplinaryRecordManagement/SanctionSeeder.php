<?php

namespace Database\Seeders\Hrim\DisciplinaryRecordManagement;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SanctionSeeder extends Seeder
{
    public function run()
    {
        $sanction = array(
            array(
                "id" => 1,
                "code" => "Reprimand",
                "display" => "Reprimand",
                "is_visible" => 1,
                "created_by" => 1,
                "deleted_at" => NULL,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 2,
                "code" => "Suspension",
                "display" => "Suspension for 15 Days",
                "is_visible" => 1,
                "created_by" => 1,
                "deleted_at" => NULL,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 3,
                "code" => "Dismissal",
                "display" => "Dismissal",
                "is_visible" => 1,
                "created_by" => 1,
                "deleted_at" => NULL,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 4,
                "code" => "30 Days suspension",
                "display" => "30 Days suspension",
                "is_visible" => 1,
                "created_by" => 1,
                "deleted_at" => NULL,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
        );
        DB::table('hrim_sanction_reference')->insert($sanction);
    }
}
