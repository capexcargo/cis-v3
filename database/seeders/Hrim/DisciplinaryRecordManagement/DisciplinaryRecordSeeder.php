<?php

namespace Database\Seeders\Hrim\DisciplinaryRecordManagement;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DisciplinaryRecordSeeder extends Seeder
{
    public function run()
    {
        $disciplinary_record = array(
            array(
                "id" => 1,
                "code" => "first",
                "display" => "1st Offense",
                "is_visible" => 1,
                "created_by" => 1,
                "deleted_at" => NULL,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 2,
                "code" => "second",
                "display" => "2nd Offense",
                "is_visible" => 1,
                "created_by" => 1,
                "deleted_at" => NULL,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 3,
                "code" => "third",
                "display" => "3rd Offense",
                "is_visible" => 1,
                "created_by" => 1,
                "deleted_at" => NULL,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
        );
        DB::table('hrim_disciplinary_record_reference')->insert($disciplinary_record);
    }
}
