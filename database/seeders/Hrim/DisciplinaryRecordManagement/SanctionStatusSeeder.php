<?php

namespace Database\Seeders\Hrim\DisciplinaryRecordManagement;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SanctionStatusSeeder extends Seeder
{
    public function run()
    {
        $sanction_status = array(
            array(
                "id" => 1,
                "code" => "For NTE",
                "display" => "For NTE",
                "is_visible" => 1,
                "created_by" => 1,
                "deleted_at" => NULL,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 2,
                "code" => "Served",
                "display" => "Served",
                "is_visible" => 1,
                "created_by" => 1,
                "deleted_at" => NULL,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 3,
                "code" => "Dismissed",
                "display" => "Dismissed",
                "is_visible" => 1,
                "created_by" => 1,
                "deleted_at" => NULL,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
        );
        DB::table('hrim_sanction_status_reference')->insert($sanction_status);
    }
}
