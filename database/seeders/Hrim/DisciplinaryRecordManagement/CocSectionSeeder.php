<?php

namespace Database\Seeders\Hrim\DisciplinaryRecordManagement;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CocSectionSeeder extends Seeder
{
    public function run()
    {
        $coc_section = array(
            array(
                "id" => 1,
                "code" => "Section 1.",
                "display" => "Section 1. Policy on Employee Conduct",
                "is_visible" => 1,
                "created_by" => 1,
                "deleted_at" => NULL,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 2,
                "code" => "Section 2.",
                "display" => "Section 2. Code of Ethics",
                "is_visible" => 1,
                "created_by" => 1,
                "deleted_at" => NULL,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 3,
                "code" => "Section 3.",
                "display" => "Section 3. Progressive Discipline",
                "is_visible" => 1,
                "created_by" => 1,
                "deleted_at" => NULL,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 4,
                "code" => "Section 4.",
                "display" => "Section 4. Definition",
                "is_visible" => 1,
                "created_by" => 1,
                "deleted_at" => NULL,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
        );
        DB::table('hrim_coc_section_reference')->insert($coc_section);
    }
}
