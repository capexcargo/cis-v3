<?php

namespace Database\Seeders\Hrim;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LeaveDayTypeReferenceSeeder extends Seeder
{
    public function run()
    {
        $hrim_leave_day_type_reference = [
            [
                'id' => 1,
                'code' => 'whole_day',
                'display' => 'Whole Day',
                'points' => 1,
            ],
            [
                'id' => 2,
                'code' => 'half_day_am',
                'display' => 'Half Day (AM)',
                'points' => 0.5,
            ],
            [
                'id' => 3,
                'code' => 'half_day_pm',
                'display' => 'Half Day (PM)',
                'points' => 0.5,
            ]
        ];

        DB::table('hrim_leave_day_type_reference')->insert($hrim_leave_day_type_reference);
    }
}
