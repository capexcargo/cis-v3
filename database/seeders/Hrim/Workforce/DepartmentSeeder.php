<?php

namespace Database\Seeders\Hrim\Workforce;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $department = array(
            array(
                "id" => 1,
                "code" => "BP",
                "display" => "Branding and Promotions",
                "is_visible" => 1,
                "created_by" => 1,
                "deleted_at" => NULL,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 2,
                "code" => "GD",
                "display" => "Graphic Design",
                "is_visible" => 1,
                "created_by" => 1,
                "deleted_at" => NULL,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 3,
                "code" => "OM",
                "display" => "Order Management",
                "is_visible" => 1,
                "created_by" => 1,
                "deleted_at" => NULL,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 4,
                "code" => "WM",
                "display" => "Warehouse Management",
                "is_visible" => 1,
                "created_by" => 1,
                "deleted_at" => NULL,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 5,
                "code" => "BI",
                "display" => "Business Intelligence",
                "is_visible" => 1,
                "created_by" => 1,
                "deleted_at" => NULL,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 6,
                "code" => "TD",
                "display" => "Tech Development",
                "is_visible" => 1,
                "created_by" => 1,
                "deleted_at" => NULL,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 7,
                "code" => "BLR",
                "display" => "Benefits and Labor Relations",
                "is_visible" => 1,
                "created_by" => 1,
                "deleted_at" => NULL,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 8,
                "code" => "CS",
                "display" => "Commercial and Sales",
                "is_visible" => 1,
                "created_by" => 1,
                "deleted_at" => NULL,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 9,
                "code" => "CC",
                "display" => "Customer Care",
                "is_visible" => 1,
                "created_by" => 1,
                "deleted_at" => NULL,
                "created_at" => "2022-05-20 10:13:09",
                "updated_at" => "2022-05-20 10:13:10",
            ),
        );
        DB::table('hrim_departments')->insert($department);
    }
}
