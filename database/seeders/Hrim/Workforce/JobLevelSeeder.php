<?php

namespace Database\Seeders\Hrim\Workforce;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JobLevelSeeder extends Seeder
{
    public function run()
    {
        $job_level = array(
            array(
                "id" => 1,
                "code" => "SEG",
                "display" => "SEG",
                "is_visible" => 1,
                "created_by" => 1,
                "deleted_at" => NULL,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 2,
                "code" => "DivH",
                "display" => "Division Head",
                "is_visible" => 1,
                "created_by" => 1,
                "deleted_at" => NULL,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 3,
                "code" => "DeputyH",
                "display" => "Deputy Head",
                "is_visible" => 1,
                "created_by" => 1,
                "deleted_at" => NULL,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 4,
                "code" => "Mngr",
                "display" => "Manager",
                "is_visible" => 1,
                "created_by" => 1,
                "deleted_at" => NULL,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 5,
                "code" => "Visor",
                "display" => "Supervisory",
                "is_visible" => 1,
                "created_by" => 1,
                "deleted_at" => NULL,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
            array(
                "id" => 6,
                "code" => "RF",
                "display" => "Rank & File",
                "is_visible" => 1,
                "created_by" => 1,
                "deleted_at" => NULL,
                "created_at" => "2022-05-14 10:13:09",
                "updated_at" => "2022-05-14 10:13:10",
            ),
        );
        

        DB::table('hrim_job_levels')->insert($job_level);
    }
}
