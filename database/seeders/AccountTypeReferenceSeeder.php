<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AccountTypeReferenceSeeder extends Seeder
{

    public function run()
    {
        $account_type_reference = [
            [
                'id' => 1,
                'code' => 'admin',
                'display' => 'Admin',
            ],
            [
                'id' => 2,
                'code' => 'accounting',
                'display' => 'Accounting',
            ]
        ];

        DB::table('account_type_reference')->insert($account_type_reference);
    }
}
