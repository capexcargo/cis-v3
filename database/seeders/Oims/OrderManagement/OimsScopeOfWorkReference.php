<?php

namespace Database\Seeders\Oims\OrderManagement;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OimsScopeOfWorkReference extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $oims_scope_of_work_references = array(
            array(
                "id" => 1,
                "name" => "Domestic",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),

            array(
                "id" => 2,
                "name" => "International",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
        );

        DB::table('oims_scope_of_work_references')->insert($oims_scope_of_work_references);
    }
}
