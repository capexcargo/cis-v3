<?php

namespace Database\Seeders\Oims\OrderManagement;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OimsTransactionEntryItemTypeReferenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $oims_transaction_entry_item_type_reference = array(
            array(
                "id" => 1,
                "name" => "Air Cargo",
                "transport_mode" => 1,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 2,
                "name" => "Air Pouch",
                "transport_mode" => 1,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 3,
                "name" => "Air Box",
                "transport_mode" => 1,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 4,
                "name" => "Sea Cargo",
                "transport_mode" => 2,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 5,
                "name" => "FCL",
                "transport_mode" => 2,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 6,
                "name" => "Sea Box",
                "transport_mode" => 2,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 7,
                "name" => "Rolling Cargo",
                "transport_mode" => 2,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 8,
                "name" => "Land Shippers Box",
                "transport_mode" => 3,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 9,
                "name" => "Land Pouch",
                "transport_mode" => 3,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 10,
                "name" => "Land Box",
                "transport_mode" => 3,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
        );

        DB::table('oims_transaction_entry_item_type_reference')->insert($oims_transaction_entry_item_type_reference);
    }
}
