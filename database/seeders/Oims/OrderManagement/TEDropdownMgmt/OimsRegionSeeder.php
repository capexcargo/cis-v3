<?php

namespace Database\Seeders\Oims\OrderManagement\TEDropdownMgmt;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OimsRegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $region = array(
            array(
                "id" => 1,
                "name" => "Region I - Ilocos Region",
                "island_group_id" => 1,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 2,
                "name" => "Region II - Cagayan Valley",
                "island_group_id" => 1,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 3,
                "name" => "Region III - Central Luzon",
                "island_group_id" => 1,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 4,
                "name" => "Region IV-A - CALABARZON",
                "island_group_id" => 1,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 5,
                "name" => "MIMAROPA Region",
                "island_group_id" => 1,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 6,
                "name" => "Region V - Bicol Region",
                "island_group_id" => 1,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 7,
                "name" => "Region VI - Western Visayas",
                "island_group_id" => 2,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 8,
                "name" => "Region VII - Central Visayas",
                "island_group_id" => 2,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 9,
                "name" => "Region VIII - Eastern Visayas",
                "island_group_id" => 2,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 10,
                "name" => "Region IX - Zamboanga Peninsula",
                "island_group_id" => 3,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 11,
                "name" => "Region X - Northern Mindanao",
                "island_group_id" => 3,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 12,
                "name" => "Region XI - Davao Region",
                "island_group_id" => 3,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 13,
                "name" => "Region XII - SOCCSKSARGEN",
                "island_group_id" => 3,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 14,
                "name" => "Region XIII - Caraga",
                "island_group_id" => 3,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 15,
                "name" => "NCR - National Capital Region",
                "island_group_id" => 1,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 16,
                "name" => "CAR - Cordillera Administrative Region",
                "island_group_id" => 1,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 17,
                "name" => "BARMM - Bangsamoro Autonomous Region in Muslim Mindanao",
                "island_group_id" => 3,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
        );

        DB::table('region')->insert($region);
    }
}
