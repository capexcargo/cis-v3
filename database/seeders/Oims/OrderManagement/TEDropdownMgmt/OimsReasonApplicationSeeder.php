<?php

namespace Database\Seeders\Oims\OrderManagement\TEDropdownMgmt;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OimsReasonApplicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $oims_reason_application_reference = array(
            array(
                "id" => 1,
                "name" => "Over SLA",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),

            array(
                "id" => 2,
                "name" => "Failed Delivery",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),

            array(
                "id" => 3,
                "name" => "Booking Cancellation",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),

            array(
                "id" => 4,
                "name" => "Booking Reschedule",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
        );

        DB::table('oims_reason_application_reference')->insert($oims_reason_application_reference);
    }
}
