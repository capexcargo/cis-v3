<?php

namespace Database\Seeders\Oims\OrderManagement;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OimsTransactionTypeReferenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $oims_transaction_type_reference = array(
            array(
                "id" => 1,
                "name" => "Walk-in",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),

            array(
                "id" => 2,
                "name" => "Pickup",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
        );

        DB::table('oims_transaction_type_reference')->insert($oims_transaction_type_reference);
    }
}
