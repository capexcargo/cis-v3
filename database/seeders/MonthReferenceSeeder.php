<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MonthReferenceSeeder extends Seeder
{
    public function run()
    {
        $month_reference = [
            [
                'id' => 1,
                'code' => 'january',
                'display' => 'January',
            ],
            [
                'id' => 2,
                'code' => 'february',
                'display' => 'February',
            ],
            [
                'id' => 3,
                'code' => 'march',
                'display' => 'March',
            ],
            [
                'id' => 4,
                'code' => 'april',
                'display' => 'April',
            ],
            [
                'id' => 5,
                'code' => 'may',
                'display' => 'May',
            ],
            [
                'id' => 6,
                'code' => 'june',
                'display' => 'June',
            ],
            [
                'id' => 7,
                'code' => 'july',
                'display' => 'July',
            ],
            [
                'id' => 8,
                'code' => 'august',
                'display' => 'August',
            ],
            [
                'id' => 9,
                'code' => 'september',
                'display' => 'September',
            ],
            [
                'id' => 10,
                'code' => 'october',
                'display' => 'October',
            ],
            [
                'id' => 11,
                'code' => 'november',
                'display' => 'November',
            ],
            [
                'id' => 12,
                'code' => 'december',
                'display' => 'December',
            ]
        ];

        DB::table('month_reference')->insert($month_reference);
    }
}
