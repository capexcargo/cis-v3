<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DivisionSeeder extends Seeder
{
    public function run()
    {
        $division = array(
            array(
                "id" => 1,
                "name" => "MIS",
                "portal_name" => "Admin",
                "description" => "Management Information System",
                "created_at" => "2022-02-14 10:13:09",
                "updated_at" => "2022-02-14 10:13:10",
            ),
            array(
                "id" => 2,
                "name" => "CSM",
                "portal_name" => "CIS V3",
                "description" => NULL,
                "created_at" => "2022-02-14 10:17:49",
                "updated_at" => "2022-02-14 10:17:49",
            ),
            array(
                "id" => 3,
                "name" => "ARM",
                "portal_name" => "CIS V3",
                "description" => NULL,
                "created_at" => "2022-02-14 10:18:42",
                "updated_at" => "2022-02-14 10:18:42",
            ),
            array(
                "id" => 4,
                "name" => "DMT",
                "portal_name" => "CIS V3",
                "description" => NULL,
                "created_at" => "2022-02-14 10:19:16",
                "updated_at" => "2022-02-14 10:19:17",
            ),
            array(
                "id" => 5,
                "name" => "CAS",
                "portal_name" => "CIS V3",
                "description" => NULL,
                "created_at" => "2022-02-14 10:19:45",
                "updated_at" => "2022-02-14 10:19:45",
            ),
            array(
                "id" => 6,
                "name" => "FLS",
                "portal_name" => "CIS V3",
                "description" => NULL,
                "created_at" => "2022-02-14 10:20:14",
                "updated_at" => "2022-02-14 10:20:14",
            ),
            array(
                "id" => 7,
                "name" => "FOM",
                "portal_name" => "CIS V3",
                "description" => NULL,
                "created_at" => "2022-02-14 10:20:41",
                "updated_at" => "2022-02-14 10:20:42",
            ),
            array(
                "id" => 8,
                "name" => "SEG",
                "portal_name" => "CIS V3",
                "description" => NULL,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
        );
        

        DB::table('division')->insert($division);
    }
}
