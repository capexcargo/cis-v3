<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TestDataSeeder extends Seeder
{
    
    public function run()
    {
        $supplier_industry_reference = [
            [
                'id' => 1,
                'code' => 'industry_1',
                'display' => 'Industry 1',
            ],
            [
                'id' => 2,
                'code' => 'industry_2',
                'display' => 'Industry 2',
            ],
        ];

        $supplier = [
            [
                'id' => 1,
                'company' => 'Company 1',
            ],
            [
                'id' => 2,
                'company' => 'Company 2',
            ]
        ];

        $budget_source = [
            [
                'id' => 1,
                'name' => 'Budget Source 1',
            ],
            [
                'id' => 2,
                'name' => 'Budget Source 2',
            ]
        ];

        $budget_chart = [
            [
                'id' => 1,
                'name' => 'Budget Chart 1',
            ],
            [
                'id' => 2,
                'name' => 'Budget Chart 2',
            ]
        ];

        $division = [
            [
                'id' => 1,
                'name' => 'Division 1',
            ],
            [
                'id' => 2,
                'name' => 'Division 2',
            ]
        ];

        $budget_plan = [
            [
                'id' => 1,
                'source_id' => 1,
                'chart_id' => 1,
                'item' => 'Budget Plan 1',
                'division_id' => 1,
            ],
            [
                'id' => 2,
                'source_id' => 2,
                'chart_id' => 2,
                'item' => 'Budget Plan 2',
                'division_id' => 2,
            ],
        ];

        $opex_type = [
            [
                'id' => 1,
                'name' => 'Opex 1',
            ],
            [
                'id' => 2,
                'name' => 'Opex 2',
            ]
        ];

        DB::table('supplier_industry_reference')->insert($supplier_industry_reference);
        DB::table('supplier')->insert($supplier);
        DB::table('budget_source')->insert($budget_source);
        DB::table('budget_chart')->insert($budget_chart);
        DB::table('division')->insert($division);
        DB::table('budget_plan')->insert($budget_plan);
        DB::table('opex_type')->insert($opex_type);
    }
}
