<?php

namespace Database\Seeders\Crm;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MarketingChannelSeeder extends Seeder
{
    public function run()
    {
        $crm_marketing_channel = array(
            array(
                "id" => 1,
                "name" => "Social Media",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 2,
                "name" => "Google Search",
                "created_at" => "2023-01-18 16:46:30",
                "updated_at" => "2023-01-18 16:46:31",
            ),
            array(
                "id" => 3,
                "name" => "Digital Advertisement",
                "created_at" => "2023-01-18 16:46:47",
                "updated_at" => "2023-01-18 16:46:48",
            ),
            array(
                "id" => 4,
                "name" => "Email Marketing",
                "created_at" => "2023-01-18 16:46:47",
                "updated_at" => "2023-01-18 16:46:48",
            ),
            array(
                "id" => 5,
                "name" => "Catalog Direct / Flyers",
                "created_at" => "2023-01-18 16:46:47",
                "updated_at" => "2023-01-18 16:46:48",
            ),
            array(
                "id" => 6,
                "name" => "Outdoor Ads (Banners/Signage/Posters)",
                "created_at" => "2023-01-18 16:46:47",
                "updated_at" => "2023-01-18 16:46:48",
            ),
            array(
                "id" => 7,
                "name" => "CaPEx Truck",
                "created_at" => "2023-01-18 16:46:47",
                "updated_at" => "2023-01-18 16:46:48",
            ),
            array(
                "id" => 8,
                "name" => "Events",
                "created_at" => "2023-01-18 16:46:47",
                "updated_at" => "2023-01-18 16:46:48",
            ),
            array(
                "id" => 9,
                "name" => "Direct Selling",
                "created_at" => "2023-01-18 16:46:47",
                "updated_at" => "2023-01-18 16:46:48",
            ),
        );

        DB::table('crm_marketing_channel')->insert($crm_marketing_channel);
    }
}
