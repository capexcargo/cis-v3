<?php

namespace Database\Seeders\Crm;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CommercialsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $crm_rate_type_reference = array(
            array(
                "id" => 1,
                "name" => "Air Freight",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 2,
                "name" => "Sea Freight",
                "created_at" => "2023-01-18 16:46:30",
                "updated_at" => "2023-01-18 16:46:31",
            ),
            array(
                "id" => 3,
                "name" => "Land Freight",
                "created_at" => "2023-01-18 16:46:47",
                "updated_at" => "2023-01-18 16:46:48",
            ),
        );

        $crm_rate_transport_mode_reference = array(
            array(
                "id" => 1,
                "name" => "Air",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 2,
                "name" => "Sea",
                "created_at" => "2023-01-18 16:46:30",
                "updated_at" => "2023-01-18 16:46:31",
            ),
            array(
                "id" => 3,
                "name" => "Land",
                "created_at" => "2023-01-18 16:46:47",
                "updated_at" => "2023-01-18 16:46:48",
            ),
        );

        $crm_booking_type_reference = array(
            array(
                "id" => 1,
                "name" => "Express",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 2,
                "name" => "Standard",
                "created_at" => "2023-01-18 16:46:30",
                "updated_at" => "2023-01-18 16:46:31",
            ),
        );

        $crm_rate_apply_for_reference = array(
            array(
                "id" => 1,
                "name" => "CIS3",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 2,
                "name" => "CaPEx Website",
                "created_at" => "2023-01-18 16:46:30",
                "updated_at" => "2023-01-18 16:46:31",
            ),
            array(
                "id" => 3,
                "name" => "Mobile Application",
                "created_at" => "2023-01-18 16:46:30",
                "updated_at" => "2023-01-18 16:46:31",
            ),
            array(
                "id" => 4,
                "name" => "VIP Portal",
                "created_at" => "2023-01-18 16:46:30",
                "updated_at" => "2023-01-18 16:46:31",
            ),
        );

        $crm_rate_pouch_type = array(
            array(
                "id" => 1,
                "name" => "Documents",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 2,
                "name" => "Non-Documents",
                "created_at" => "2023-01-18 16:46:30",
                "updated_at" => "2023-01-18 16:46:31",
            ),
        );

        $crm_approval_reference = array(
            array(
                "id" => 1,
                "name" => "For Approval",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 2,
                "name" => "Active",
                "created_at" => "2023-01-18 16:46:30",
                "updated_at" => "2023-01-18 16:46:31",
            ),
            array(
                "id" => 3,
                "name" => "Declined",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 4,
                "name" => "Deactivated",
                "created_at" => "2023-01-18 16:46:30",
                "updated_at" => "2023-01-18 16:46:31",
            ),
        );

        $crm_crating_type_reference = array(
            array(
                "id" => 1,
                "name" => "Skeletal",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 2,
                "name" => "Close",
                "created_at" => "2023-01-18 16:46:30",
                "updated_at" => "2023-01-18 16:46:31",
            ),
        );

        $crm_rate_warehousing_option_reference = array(
            array(
                "id" => 1,
                "option" => "Volume based",
                "rate_per_unit" => "Per CBM / Pallet Per Day",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 2,
                "option" => "Volume based",
                "rate_per_unit" => "Per SQM Per Day",
                "created_at" => "2023-01-18 16:46:30",
                "updated_at" => "2023-01-18 16:46:31",
            ),
            array(
                "id" => 3,
                "option" => "Allocated Space",
                "rate_per_unit" => "Per CBM / Pallet Per Day",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 4,
                "option" => "Allocated Space",
                "rate_per_unit" => "Per SQM Per Day",
                "created_at" => "2023-01-18 16:46:30",
                "updated_at" => "2023-01-18 16:46:31",
            ),
        );

        $crm_commodity_type_reference = array(
            array(
                "id" => 1,
                "name" => "General Cargo",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 2,
                "name" => "V-Cargo",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
        );

        $crm_service_mode_reference = array(
            array(
                "id" => 1,
                "name" => "Airport - Airport",
                "type" => 1,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 2,
                "name" => "Airport - Door",
                "type" => 1,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 3,
                "name" => "Door - Airport",
                "type" => 1,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 4,
                "name" => "Door - Door",
                "type" => 1,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 5,
                "name" => "Port - Port",
                "type" => 2,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 6,
                "name" => "Port - Door",
                "type" => 2,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 7,
                "name" => "Door - Port",
                "type" => 2,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 8,
                "name" => "Door - Door",
                "type" => 2,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
        );

        $crm_shipment_type = array(
            array(
                "id" => 1,
                "name" => "FCL",
                "type" => 2,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 2,
                "name" => "LCL",
                "type" => 2,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
        );

        DB::table('crm_rate_type_reference')->insert($crm_rate_type_reference);
        DB::table('crm_rate_transport_mode_reference')->insert($crm_rate_transport_mode_reference);
        DB::table('crm_booking_type_reference')->insert($crm_booking_type_reference);
        DB::table('crm_rate_apply_for_reference')->insert($crm_rate_apply_for_reference);
        DB::table('crm_rate_pouch_type')->insert($crm_rate_pouch_type);
        DB::table('crm_approval_reference')->insert($crm_approval_reference);
        DB::table('crm_crating_type_reference')->insert($crm_crating_type_reference);
        DB::table('crm_rate_warehousing_option_reference')->insert($crm_rate_warehousing_option_reference);
        DB::table('crm_commodity_type_reference')->insert($crm_commodity_type_reference);
        DB::table('crm_service_mode_reference')->insert($crm_service_mode_reference);
        DB::table('crm_shipment_type')->insert($crm_shipment_type);
    }
}
