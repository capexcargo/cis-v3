<?php

namespace Database\Seeders\Crm;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StakeholderSearchMgmtReferencesSeeder extends Seeder
{
    public function run()
    {

        $crm_stakeholder_search_management_reference = array(
            array(
                "id" => 1,
                "name" => "CSM",
                "table_name" => "users",
            ),
            array(
                "id" => 2,
                "name" => "NL/SL",
                "table_name" => "branch_reference",
            ),
            array(
                "id" => 3,
                "name" => "Metro Manila - Field",
                "table_name" => "branch_references_quadrant",
            ),
            array(
                "id" => 4,
                "name" => "Metro Manila - Walk In",
                "table_name" => "branch_references_walkin",
            ),
            array(
                "id" => 5,
                "name" => "Provincial Branches",
                "table_name" => "branch_reference",
            ),
            array(
                "id" => 6,
                "name" => "Online Channels",
                "table_name" => "crm_channel_sr_source",
            ),
        );
        DB::table('crm_stakeholder_search_management_reference')->insert($crm_stakeholder_search_management_reference);
    }
}
