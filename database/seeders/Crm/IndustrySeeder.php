<?php

namespace Database\Seeders\Crm;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class IndustrySeeder extends Seeder
{
    public function run()
    {
        $crm_industry = array(
            array(
                "id" => 1,
                "name" => "Food Industry",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 2,
                "name" => "Agriculture",
                "created_at" => "2023-01-18 16:46:30",
                "updated_at" => "2023-01-18 16:46:31",
            ),
            array(
                "id" => 3,
                "name" => "Construction",
                "created_at" => "2023-01-18 16:46:47",
                "updated_at" => "2023-01-18 16:46:48",
            ),
            array(
                "id" => 4,
                "name" => "Automotive",
                "created_at" => "2023-01-18 16:46:47",
                "updated_at" => "2023-01-18 16:46:48",
            ),
            array(
                "id" => 5,
                "name" => "Manufacturing",
                "created_at" => "2023-01-18 16:46:47",
                "updated_at" => "2023-01-18 16:46:48",
            ),
            array(
                "id" => 6,
                "name" => "Financial Services",
                "created_at" => "2023-01-18 16:46:47",
                "updated_at" => "2023-01-18 16:46:48",
            ),
            array(
                "id" => 7,
                "name" => "Transport",
                "created_at" => "2023-01-18 16:46:47",
                "updated_at" => "2023-01-18 16:46:48",
            ),
            array(
                "id" => 8,
                "name" => "Logistics",
                "created_at" => "2023-01-18 16:46:47",
                "updated_at" => "2023-01-18 16:46:48",
            ),
            array(
                "id" => 9,
                "name" => "Commerces",
                "created_at" => "2023-01-18 16:46:47",
                "updated_at" => "2023-01-18 16:46:48",
            ),
        );

        DB::table('crm_industry')->insert($crm_industry);
    }
}
