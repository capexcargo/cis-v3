<?php

namespace Database\Seeders\Crm;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BranchQuadrantReferenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $branch_references_quadrant = array(
            array(
                "name" => "Quadrant 1",
                "branch_reference_id" => 3,
                "quadrant" => 1,
            ),
            array(
                "name" => "Quadrant 2",
                "branch_reference_id" => 3,
                "quadrant" => 2,
            ),
            array(
                "name" => "Quadrant 3",
                "branch_reference_id" => 3,
                "quadrant" => 3,
            ),
            array(
                "name" => "Quadrant 4",
                "branch_reference_id" => 3,
                "quadrant" => 4,
            ),
        );
        DB::table('branch_references_quadrant')->insert($branch_references_quadrant);
    }
}
