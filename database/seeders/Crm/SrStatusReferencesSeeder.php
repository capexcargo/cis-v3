<?php

namespace Database\Seeders\Crm;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SrStatusReferencesSeeder extends Seeder
{
    public function run()
    {
        $sr_status_references = array(
            array(
                "id" => 1,
                "name" => "New",
                "created_at" => "2023-01-21 16:13:21",
                "updated_at" => "2023-01-21 16:13:22",
            ),
            array(
                "id" => 2,
                "name" => "In Progress",
                "created_at" => "2023-01-21 16:13:29",
                "updated_at" => "2023-01-21 16:13:30",
            ),
            array(
                "id" => 3,
                "name" => "Resolved",
                "created_at" => "2023-01-21 16:13:38",
                "updated_at" => "2023-01-21 16:13:38",
            ),
        );

        DB::table('crm_sr_status_references')->insert($sr_status_references);
    }
}
