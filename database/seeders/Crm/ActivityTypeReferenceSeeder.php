<?php

namespace Database\Seeders\Crm;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ActivityTypeReferenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $crm_activity_type_pickup_reference = array(
            array(
                "id" => 1,
                "name" => "Regular Volume",
                "time" => "15",
                "unit_time" => "mins",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 2,
                "name" => "Bulk Volume",
                "time" => "30",
                "unit_time" => "mins",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 3,
                "name" => "Full Track Load",
                "time" => "4",
                "unit_time" => "hours",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 4,
                "name" => "Full Container Load",
                "time" => "8",
                "unit_time" => "hours",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 5,
                "name" => "Special Project",
                "time" => "4",
                "unit_time" => "hours",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
        );

        $crm_activity_type_delivery_reference = array(
            array(
                "id" => 1,
                "name" => "Regular Volume",
                "time" => "10",
                "unit_time" => "mins",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 2,
                "name" => "Bulk Volume",
                "time" => "20",
                "unit_time" => "mins",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 3,
                "name" => "Full Track Load",
                "time" => "4",
                "unit_time" => "hours",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 4,
                "name" => "Full Container Load",
                "time" => "8",
                "unit_time" => "hours",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 5,
                "name" => "Special Project",
                "time" => "4",
                "unit_time" => "hours",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
        );

        DB::table('crm_activity_type_pickup_reference')->insert($crm_activity_type_pickup_reference);
        DB::table('crm_activity_type_delivery_reference')->insert($crm_activity_type_delivery_reference);
    }
}
