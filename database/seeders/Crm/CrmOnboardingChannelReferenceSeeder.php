<?php

namespace Database\Seeders\Crm;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CrmOnboardingChannelReferenceSeeder extends Seeder
{
    public function run()
    {
        $crm_onboarding_channel_reference = array(
            array(
                "id" => 1,
                "name" => "CRM",
                "created_at" => "2023-06-20 12:03:22",
                "updated_at" => "2023-06-20 12:03:22",
            ),
            array(
                "id" => 2,
                "name" => "Website",
                "created_at" => "2023-06-20 12:03:22",
                "updated_at" => "2023-06-20 12:03:22",
            ),
        );
        DB::table('crm_onboarding_channel_reference')->insert($crm_onboarding_channel_reference);
    }
}
