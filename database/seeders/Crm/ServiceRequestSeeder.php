<?php

namespace Database\Seeders\Crm;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServiceRequestSeeder extends Seeder
{
    public function run()
    {
        $crm_sr_type = array(
            array(
                "id" => 1,
                "name" => "Sales Inquiry",
                "created_at" => "2023-01-18 10:04:43",
                "updated_at" => "2023-01-18 10:04:44",
            ),
            array(
                "id" => 2,
                "name" => "Support",
                "created_at" => "2023-01-18 15:54:56",
                "updated_at" => "2023-01-18 15:54:57",
            ),
            array(
                "id" => 3,
                "name" => "Feedback",
                "created_at" => "2023-01-18 15:55:10",
                "updated_at" => "2023-01-18 15:55:11",
            ),
            array(
                "id" => 4,
                "name" => "Others",
                "created_at" => "2023-01-18 15:55:20",
                "updated_at" => "2023-01-18 15:55:21",
            ),
        );

        $crm_sr_type_subcategory = array(
            array(
                "id" => 1,
                "name" => "Booking",
                "sr_type_id" => 2,
                "created_at" => "2023-01-18 16:04:16",
                "updated_at" => "2023-01-18 16:04:17",
            ),
            array(
                "id" => 2,
                "name" => "Cargo Status",
                "sr_type_id" => 2,
                "created_at" => "2023-01-18 16:04:30",
                "updated_at" => "2023-01-18 16:04:31",
            ),
            array(
                "id" => 3,
                "name" => "Commendations",
                "sr_type_id" => 3,
                "created_at" => "2023-01-18 16:04:48",
                "updated_at" => "2023-01-18 16:04:48",
            ),
            array(
                "id" => 4,
                "name" => "Complains",
                "sr_type_id" => 3,
                "created_at" => "2023-01-18 16:05:04",
                "updated_at" => "2023-01-18 16:05:05",
            ),
            array(
                "id" => 5,
                "name" => "Job Application | HR Concern",
                "sr_type_id" => 4,
                "created_at" => "2023-01-18 16:05:22",
                "updated_at" => "2023-01-18 16:05:22",
            ),
            array(
                "id" => 6,
                "name" => "Vendor",
                "sr_type_id" => 4,
                "created_at" => "2023-01-18 16:05:35",
                "updated_at" => "2023-01-18 16:05:35",
            ),
            array(
                "id" => 7,
                "name" => "Spam",
                "sr_type_id" => 4,
                "created_at" => "2023-01-18 16:05:49",
                "updated_at" => "2023-01-18 16:05:50",
            ),
        );

        $crm_channel_sr_source = array(
            array(
                "id" => 1,
                "name" => "Trunkline",
                "created_at" => "2023-01-18 16:13:21",
                "updated_at" => "2023-01-18 16:13:22",
            ),
            array(
                "id" => 2,
                "name" => "Website",
                "created_at" => "2023-01-18 16:13:29",
                "updated_at" => "2023-01-18 16:13:30",
            ),
            array(
                "id" => 3,
                "name" => "Facebook Lead",
                "created_at" => "2023-01-18 16:13:38",
                "updated_at" => "2023-01-18 16:13:38",
            ),
            array(
                "id" => 4,
                "name" => "Social Media",
                "created_at" => "2023-01-18 16:13:49",
                "updated_at" => "2023-01-18 16:13:49",
            ),
            array(
                "id" => 5,
                "name" => "Email to default email",
                "created_at" => "2023-01-18 16:13:59",
                "updated_at" => "2023-01-18 16:13:59",
            ),
            array(
                "id" => 6,
                "name" => "Direct email to agent",
                "created_at" => "2023-01-18 16:14:06",
                "updated_at" => "2023-01-18 16:14:07",
            ),
        );

        $crm_service_requirements = array(
            array(
                "id" => 1,
                "name" => "Domestic Freight Forwarding",
                "created_at" => "2023-01-18 16:16:20",
                "updated_at" => "2023-01-18 16:16:21",
            ),
            array(
                "id" => 2,
                "name" => "Warehousing",
                "created_at" => "2023-01-18 16:16:28",
                "updated_at" => "2023-01-18 16:16:28",
            ),
            array(
                "id" => 3,
                "name" => "Trucking",
                "created_at" => "2023-01-18 16:16:36",
                "updated_at" => "2023-01-18 16:16:36",
            ),
            array(
                "id" => 4,
                "name" => "Packing and Crating",
                "created_at" => "2023-01-18 16:16:44",
                "updated_at" => "2023-01-18 16:16:45",
            ),
            array(
                "id" => 5,
                "name" => "General Sales Agent (GSA)",
                "created_at" => "2023-01-18 16:16:53",
                "updated_at" => "2023-01-18 16:16:53",
            ),
            array(
                "id" => 6,
                "name" => "International Freight Forwarding",
                "created_at" => "2023-01-18 16:17:05",
                "updated_at" => "2023-01-18 16:17:05",
            ),
            array(
                "id" => 7,
                "name" => "NVOCC",
                "created_at" => "2023-01-18 16:17:13",
                "updated_at" => "2023-01-18 16:17:13",
            ),
        );

        DB::table('crm_sr_type')->insert($crm_sr_type);
        DB::table('crm_sr_type_subcategory')->insert($crm_sr_type_subcategory);
        DB::table('crm_channel_sr_source')->insert($crm_channel_sr_source);
        DB::table('crm_service_requirements')->insert($crm_service_requirements);
    }
}
