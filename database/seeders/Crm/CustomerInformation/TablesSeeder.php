<?php

namespace Database\Seeders\Crm\CustomerInformation;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $account_type = array(
        //     array(
        //         "id" => 1,
        //         "name" => "Individual",
        //         "created_at" => "2021-09-10 03:03:50",
        //         "updated_at" => "2021-09-10 03:03:50",
        //     ),
        //     array(
        //         "id" => 2,
        //         "name" => "Corporate",
        //         "created_at" => "2021-09-10 03:03:50",
        //         "updated_at" => "2021-09-10 03:03:50",
        //     ),
        // );

        // $life_stage = array(
        //     array(
        //         "id" => 1,
        //         "name" => "Lead",
        //         "created_at" => "2021-09-10 03:03:50",
        //         "updated_at" => "2021-09-10 03:03:50",
        //     ),
        //     array(          
        //         "id" => 2,
        //         "name" => "Customer",
        //         "created_at" => "2021-09-10 03:03:50",
        //         "updated_at" => "2021-09-10 03:03:50",
        //     ),
        // );

        // $address_label = array(
        //     array(
        //         "id" => 1,
        //         "name" => "Home",
        //         "created_at" => "2021-09-10 03:03:50",
        //         "updated_at" => "2021-09-10 03:03:50",
        //     ),
        //     array(
        //         "id" => 2,
        //         "name" => "Office",
        //         "created_at" => "2021-09-10 03:03:50",
        //         "updated_at" => "2021-09-10 03:03:50",
        //     ),
        //     array(
        //         "id" => 3,
        //         "name" => "Warehouse",
        //         "created_at" => "2021-09-10 03:03:50",
        //         "updated_at" => "2021-09-10 03:03:50",
        //     ),
        // );

        $account_type_2 = array(
            array(
                "id" => 1,
                "name" => "Account",
                "created_at" => "2021-09-10 03:03:50",
                "updated_at" => "2021-09-10 03:03:50",
            ),
            array(
                "id" => 2,
                "name" => "Non-Account",
                "created_at" => "2021-09-10 03:03:50",
                "updated_at" => "2021-09-10 03:03:50",
            ),
        );

        // DB::table('crm_account_type')->insert($account_type);
        // DB::table('crm_life_stage')->insert($life_stage);
        // DB::table('crm_address_label')->insert($address_label);
        DB::table('crm_account_type_2')->insert($account_type_2);

    }
}
