<?php

namespace Database\Seeders\Crm;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BranchWalkinReferenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $branch_references_walkin = array(
            array(
                "name" => "Delta Office",
                "branch_reference_id" => 4,
            ),
            array(
                "name" => "Banawe Branch",
                "branch_reference_id" => 4,
            ),
            array(
                "name" => "Bambang Branch",
                "branch_reference_id" => 4,
            ),
            array(
                "name" => "Merville Warehouse",
                "branch_reference_id" => 4,
            ),
        );
        DB::table('branch_references_walkin')->insert($branch_references_walkin);
    }
}
