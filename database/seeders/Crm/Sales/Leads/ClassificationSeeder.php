<?php

namespace Database\Seeders\Crm\Sales\Leads;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClassificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $crm_lead_classification = array(
            array(
                "id" => 1,
                "name" => "Cold",
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 2,
                "name" => "Warm",
                "created_at" => "2023-01-18 16:46:30",
                "updated_at" => "2023-01-18 16:46:31",
            ),
            array(
                "id" => 3,
                "name" => "Hot",
                "created_at" => "2023-01-18 16:46:47",
                "updated_at" => "2023-01-18 16:46:48",
            ),
        );

        DB::table('crm_lead_classification')->insert($crm_lead_classification);
    }
}
