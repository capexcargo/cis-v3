<?php

namespace Database\Seeders\Crm\Sales\Leads;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class QualificationQuestionnaireSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $crm_qualification_question = array(
            array(
                "id" => 1,
                "question" => "Does the lead have a need / requirement to purchase CaPEx service/s?",
                "qualification_type" => 1,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 2,
                "question" => "Is the requirement of the lead workable for CaPEx?",
                "qualification_type" => 1,
                "created_at" => "2023-01-18 16:46:30",
                "updated_at" => "2023-01-18 16:46:31",
            ),
            array(
                "id" => 3,
                "question" => "Is the requirement an immediate one?",
                "qualification_type" => 1,
                "created_at" => "2023-01-18 16:46:47",
                "updated_at" => "2023-01-18 16:46:48",
            ),
            array(
                "id" => 4,
                "question" => "Does the lead request for a meeting presentation of CaPEx Services?",
                "qualification_type" => 1,
                "created_at" => "2023-01-18 16:46:47",
                "updated_at" => "2023-01-18 16:46:48",
            ),
            array(
                "id" => 5,
                "question" => "Is the requirement an immediate one?",
                "qualification_type" => 1,
                "created_at" => "2023-01-18 16:46:47",
                "updated_at" => "2023-01-18 16:46:48",
            ),
            array(
                "id" => 6,
                "question" => "Is the point of contact the decision-maker?",
                "qualification_type" => 1,
                "created_at" => "2023-01-18 16:46:47",
                "updated_at" => "2023-01-18 16:46:48",
            ),
            array(
                "id" => 7,
                "question" => "Does the lead shows interest to purchase?",
                "qualification_type" => 1,
                "created_at" => "2023-01-18 16:46:47",
                "updated_at" => "2023-01-18 16:46:48",
            ),
            array(
                "id" => 8,
                "question" => "Does the lead have a need / requirement to purchase CaPEx service/s?",
                "qualification_type" => 2,
                "created_at" => "2023-01-18 16:46:47",
                "updated_at" => "2023-01-18 16:46:48",
            ),
        );

        DB::table('crm_qualification_question_management')->insert($crm_qualification_question);
    }
}
