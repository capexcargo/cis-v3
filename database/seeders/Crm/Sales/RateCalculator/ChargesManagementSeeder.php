<?php

namespace Database\Seeders\Crm\Sales\RateCalculator;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ChargesManagementSeeder extends Seeder
{
    public function run()
    {
        $charges_management = array(
            array(
                "id" => 1,
                "rate_calcu_charges" => "Weight Charge (If AIR)",
                "charges_category" => 1,
                "status" => 1,
                "created_at" => "2023-08-23 04:28:09",
                "updated_at" => "2023-08-23 04:28:09",
            ),
            array(
                "id" => 2,
                "rate_calcu_charges" => "CBM Charge (If SEA and LAND)",
                "charges_category" => 1,
                "status" => 1,
                "created_at" => "2023-08-23 04:28:30",
                "updated_at" => "2023-08-23 04:28:31",
            ),
            array(
                "id" => 3,
                "rate_calcu_charges" => "AWB Fee",
                "charges_category" => 1,
                "status" => 1,
                "created_at" => "2023-08-23 04:28:47",
                "updated_at" => "2023-08-23 04:28:48",
            ),
            array(
                "id" => 4,
                "rate_calcu_charges" => "Valuation",
                "charges_category" => 1,
                "status" => 1,
                "created_at" => "2023-08-23 04:28:47",
                "updated_at" => "2023-08-23 04:28:48",
            ),
            array(
                "id" => 5,
                "rate_calcu_charges" => "COD Charges",
                "charges_category" => 1,
                "status" => 1,
                "created_at" => "2023-08-23 04:28:47",
                "updated_at" => "2023-08-23 04:28:48",
            ),
            array(
                "id" => 6,
                "rate_calcu_charges" => "Handling Fee",
                "charges_category" => 1,
                "status" => 1,
                "created_at" => "2023-08-23 04:28:47",
                "updated_at" => "2023-08-23 04:28:48",
            ),
            array(
                "id" => 7,
                "rate_calcu_charges" => "Doc Fee",
                "charges_category" => 1,
                "status" => 1,
                "created_at" => "2023-08-23 04:28:47",
                "updated_at" => "2023-08-23 04:28:48",
            ),
            array(
                "id" => 8,
                "rate_calcu_charges" => "OPA Fee",
                "charges_category" => 2,
                "status" => 1,
                "created_at" => "2023-08-23 04:28:47",
                "updated_at" => "2023-08-23 04:28:48",
            ),
            array(
                "id" => 9,
                "rate_calcu_charges" => "ODA Fee",
                "charges_category" => 2,
                "status" => 1,
                "created_at" => "2023-08-23 04:28:47",
                "updated_at" => "2023-08-23 04:28:48",
            ),
        );

        DB::table('charges_management')->insert($charges_management);
    }
}
