<?php

namespace Database\Seeders\Crm\Sales\Opportunities;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SalesStageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $crm_sales_stage = array(
            array(
                "id" => 1,
                "sales_stage" => "Initiation",
                "opportunity_status_id" => 1,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 2,
                "sales_stage" => "Quotation",
                "opportunity_status_id" => 1,
                "created_at" => "2023-01-18 16:46:30",
                "updated_at" => "2023-01-18 16:46:31",
            ),
            array(
                "id" => 3,
                "sales_stage" => "Negotiation",
                "opportunity_status_id" => 1,
                "created_at" => "2023-01-18 16:46:47",
                "updated_at" => "2023-01-18 16:46:48",
            ),
            array(
                "id" => 4,
                "sales_stage" => "Loss",
                "opportunity_status_id" => 1,
                "created_at" => "2023-01-18 16:46:47",
                "updated_at" => "2023-01-18 16:46:48",
            ),
            array(
                "id" => 5,
                "sales_stage" => "Won",
                "opportunity_status_id" => 1,
                "created_at" => "2023-01-18 16:46:47",
                "updated_at" => "2023-01-18 16:46:48",
            ),
        );

        DB::table('crm_sales_stage')->insert($crm_sales_stage);
    }
}
