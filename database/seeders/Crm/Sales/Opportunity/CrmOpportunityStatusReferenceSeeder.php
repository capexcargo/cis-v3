<?php

namespace Database\Seeders\Crm\Sales\Opportunity;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CrmOpportunityStatusReferenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $crm_opportunity_status_reference = array(
            array(
                "id" => 1,
                "name" => "Open",
                "status" => 1,
                "created_at" => "2023-01-18 16:46:09",
                "updated_at" => "2023-01-18 16:46:10",
            ),
            array(
                "id" => 2,
                "name" => "Lost",
                "status" => 1,
                "created_at" => "2023-01-18 16:46:30",
                "updated_at" => "2023-01-18 16:46:31",
            ),
            array(
                "id" => 3,
                "name" => "Won",
                "status" => 1,
                "created_at" => "2023-01-18 16:46:47",
                "updated_at" => "2023-01-18 16:46:48",
            ),
        );

        DB::table('crm_opportunity_status_reference')->insert($crm_opportunity_status_reference);
    }
}
