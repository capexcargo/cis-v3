<?php

namespace Database\Seeders\Crm;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomerStatusReferencesSeeder extends Seeder
{
    public function run()
    {

        $customer_status_references = array(
            array(
                "id" => 1,
                "name" => "Active",
                "created_at" => "2021-09-10 03:03:50",
                "updated_at" => "2021-09-10 03:03:50",
            ),
            array(
                "id" => 2,
                "name" => "Inactive",
                "created_at" => "2021-09-10 03:03:50",
                "updated_at" => "2021-09-10 03:03:50",
            ),
            array(
                "id" => 3,
                "name" => "Prospect",
                "created_at" => "2021-09-10 03:03:50",
                "updated_at" => "2021-09-10 03:03:50",
            ),
        );

        DB::table('crm_customer_status_references')->insert($customer_status_references);
    }
}
