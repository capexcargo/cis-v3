<?php

namespace Database\Seeders\Crm;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MovementSFTypeReferencesSeeder extends Seeder
{
    public function run()
    {
        $crm_movement_sea_freight_type_reference = array(
            array(
                "id" => 1,
                "name" => "LCL",
                "created_at" => "2023-01-21 16:13:21",
                "updated_at" => "2023-01-21 16:13:22",
            ),
            array(
                "id" => 2,
                "name" => "FCL",
                "created_at" => "2023-01-21 16:13:29",
                "updated_at" => "2023-01-21 16:13:30",
            ),
            array(
                "id" => 3,
                "name" => "RCL",
                "created_at" => "2023-01-21 16:13:38",
                "updated_at" => "2023-01-21 16:13:38",
            ),
            array(
                "id" => 4,
                "name" => "Project Shipment",
                "created_at" => "2023-01-21 16:13:38",
                "updated_at" => "2023-01-21 16:13:38",
            ),
        );

        DB::table('crm_movement_sea_freight_type_reference')->insert($crm_movement_sea_freight_type_reference);
    }
}
