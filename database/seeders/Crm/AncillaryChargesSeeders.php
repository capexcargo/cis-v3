<?php

namespace Database\Seeders\Crm;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AncillaryChargesSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $crm_ancillary_management = array(
            array(
                "id" => 1,
                "name" => "Waybill Fee",
                "charges_amount" => 50.00,
                "charges_rate" => 0,
                "description" => "Per Waybill",
                "created_at" => "2023-01-31 12:03:22",
                "updated_at" => "2023-01-31 12:03:22",
                "status" => 1,
            ),
            array(
                "id" => 2,
                "name" => "Air Freight Valuation Fee",
                "charges_amount" => 0.00,
                "charges_rate" => 1,
                "description" => "Declared Value",
                "created_at" => "2023-01-31 12:03:22",
                "updated_at" => "2023-01-31 12:03:22",
                "status" => 1,
            ),
            array(
                "id" => 3,
                "name" => "Sea Freight Valuation Fee",
                "charges_amount" => 0.00,
                "charges_rate" => 5,
                "description" => "Declared Value",
                "created_at" => "2023-01-31 12:03:22",
                "updated_at" => "2023-01-31 12:03:22",
                "status" => 1,
            ),
            array(
                "id" => 4,
                "name" => "Insurance Fee",
                "charges_amount" => 2.00,
                "charges_rate" => 0,
                "description" => "For Chargeable Weight",
                "created_at" => "2023-01-31 12:03:22",
                "updated_at" => "2023-01-31 12:03:22",
                "status" => 1,
            ),
            array(
                "id" => 5,
                "name" => "Air Freight Handling Fee 1",
                "charges_amount" => 4.00,
                "charges_rate" => 0,
                "description" => "Per KGS in Luzon",
                "created_at" => "2023-01-31 12:03:22",
                "updated_at" => "2023-01-31 12:03:22",
                "status" => 1,
            ),
            array(
                "id" => 6,
                "name" => "Air Freight Handling Fee 2",
                "charges_amount" => 4.00,
                "charges_rate" => 0,
                "description" => "Per KGS in Visayas",
                "created_at" => "2023-01-31 12:03:22",
                "updated_at" => "2023-01-31 12:03:22",
                "status" => 1,
            ),
            array(
                "id" => 7,
                "name" => "Air Freight Handling Fee 3",
                "charges_amount" => 5.00,
                "charges_rate" => 0,
                "description" => "Per KGS in Mindanao",
                "created_at" => "2023-01-31 12:03:22",
                "updated_at" => "2023-01-31 12:03:22",
                "status" => 1,
            ),
            array(
                "id" => 8,
                "name" => "Sea Freight Handling Fee",
                "charges_amount" => 1.00,
                "charges_rate" => 0,
                "description" => "Per KSG for cargoes over 50kg",
                "created_at" => "2023-01-31 12:03:22",
                "updated_at" => "2023-01-31 12:03:22",
                "status" => 1,
            ),
            array(
                "id" => 9,
                "name" => "Delivery Fee",
                "charges_amount" => 100.23,
                "charges_rate" => 0.005,
                "description" => NULL,
                "created_at" => "2023-01-31 12:09:49",
                "updated_at" => "2023-01-31 12:12:34",
                "status" => 1,
            ),
            array(
                "id" => 10,
                "name" => "Perishable Fee",
                "charges_amount" => 500.00,
                "charges_rate" => 1.23,
                "description" => NULL,
                "created_at" => "2023-01-31 12:12:55",
                "updated_at" => "2023-01-31 12:13:37",
                "status" => 1,
            ),
        );

        $crm_ancillary_display_management = array(
            array(
                "id" => 1,
                "name" => "Ancillary Charges - Air Freight - General Cargo",
                "status" => 1,
                "created_at" => "2023-01-31 16:41:22",
                "updated_at" => "2023-02-02 18:24:33",
            ),
            array(
                "id" => 2,
                "name" => "Ancillary Charges - Air Freight - V Cargo",
                "status" => 1,
                "created_at" => "2023-02-02 11:44:16",
                "updated_at" => "2023-02-02 18:24:57",
            ),
            array(
                "id" => 3,
                "name" => "Ancillary Charges - Sea Freight - LCL",
                "status" => 1,
                "created_at" => "2023-02-02 17:20:13",
                "updated_at" => "2023-02-02 18:25:14",
            ),
            array(
                "id" => 4,
                "name" => "Ancillary Charges - Land Freight",
                "status" => 1,
                "created_at" => "2023-02-02 18:26:15",
                "updated_at" => "2023-02-02 18:26:15",
            ),
        );

        $crm_ancillary_display_details = array(
            array(
                "id" => 1,
                "ancillary_display_id" => 1,
                "ancillary_charge_id" => 3,
                "created_at" => "2023-01-31 16:41:22",
                "updated_at" => "2023-01-31 16:41:22",
            ),
            array(
                "id" => 2,
                "ancillary_display_id" => 1,
                "ancillary_charge_id" => 1,
                "created_at" => "2023-01-31 16:41:22",
                "updated_at" => "2023-01-31 16:41:22",
            ),
            array(
                "id" => 3,
                "ancillary_display_id" => 2,
                "ancillary_charge_id" => 1,
                "created_at" => "2023-02-02 13:45:23",
                "updated_at" => "2023-02-02 13:45:23",
            ),
            array(
                "id" => 4,
                "ancillary_display_id" => 2,
                "ancillary_charge_id" => 2,
                "created_at" => "2023-02-02 13:45:23",
                "updated_at" => "2023-02-02 17:09:27",
            ),
            array(
                "id" => 5,
                "ancillary_display_id" => 2,
                "ancillary_charge_id" => 4,
                "created_at" => "2023-02-02 17:09:27",
                "updated_at" => "2023-02-02 17:09:27",
            ),
            array(
                "id" => 6,
                "ancillary_display_id" => 3,
                "ancillary_charge_id" => 1,
                "created_at" => "2023-02-02 17:20:13",
                "updated_at" => "2023-02-02 17:36:43",
            ),
            array(
                "id" => 7,
                "ancillary_display_id" => 3,
                "ancillary_charge_id" => 3,
                "created_at" => "2023-02-02 17:20:13",
                "updated_at" => "2023-02-02 17:20:13",
            ),
            array(
                "id" => 8,
                "ancillary_display_id" => 3,
                "ancillary_charge_id" => 2,
                "created_at" => "2023-02-02 17:20:13",
                "updated_at" => "2023-02-02 17:20:13",
            ),
            array(
                "id" => 9,
                "ancillary_display_id" => 3,
                "ancillary_charge_id" => 7,
                "created_at" => "2023-02-02 17:36:43",
                "updated_at" => "2023-02-02 17:36:43",
            ),
            array(
                "id" => 10,
                "ancillary_display_id" => 3,
                "ancillary_charge_id" => 6,
                "created_at" => "2023-02-02 17:36:43",
                "updated_at" => "2023-02-02 17:36:43",
            ),
            array(
                "id" => 11,
                "ancillary_display_id" => 4,
                "ancillary_charge_id" => 1,
                "created_at" => "2023-02-02 18:26:15",
                "updated_at" => "2023-02-02 18:26:15",
            ),
            array(
                "id" => 12,
                "ancillary_display_id" => 4,
                "ancillary_charge_id" => 4,
                "created_at" => "2023-02-02 18:26:15",
                "updated_at" => "2023-02-02 18:26:15",
            ),
            array(
                "id" => 13,
                "ancillary_display_id" => 4,
                "ancillary_charge_id" => 5,
                "created_at" => "2023-02-02 18:26:15",
                "updated_at" => "2023-02-02 18:26:15",
            ),
            array(
                "id" => 14,
                "ancillary_display_id" => 4,
                "ancillary_charge_id" => 6,
                "created_at" => "2023-02-02 18:26:15",
                "updated_at" => "2023-02-02 18:26:15",
            ),
            array(
                "id" => 15,
                "ancillary_display_id" => 4,
                "ancillary_charge_id" => 7,
                "created_at" => "2023-02-02 18:26:15",
                "updated_at" => "2023-02-02 18:26:15",
            ),
        );

        DB::table('crm_ancillary_management')->insert($crm_ancillary_management);
        DB::table('crm_ancillary_display_management')->insert($crm_ancillary_display_management);
        DB::table('crm_ancillary_display_details')->insert($crm_ancillary_display_details);
    }
}
