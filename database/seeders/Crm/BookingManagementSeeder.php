<?php

namespace Database\Seeders\Crm;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BookingManagementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $crm_booking_status_reference = array(
            array(
                "id" => 1,
                "name" => "For Dispatch",
                "created_at" => "2023-01-24 14:50:03",
                "updated_at" => "2023-01-24 14:50:04",
            ),
            array(
                "id" => 2,
                "name" => "For Confirmation",
                "created_at" => "2023-01-24 14:50:03",
                "updated_at" => "2023-01-24 14:50:04",
            ),
            array(
                "id" => 3,
                "name" => "Confirmed",
                "created_at" => "2023-01-24 14:50:03",
                "updated_at" => "2023-01-24 14:50:04",
            ),
            array(
                "id" => 4,
                "name" => "Ongoing",
                "created_at" => "2023-01-24 14:50:03",
                "updated_at" => "2023-01-24 14:50:04",
            ),
            array(
                "id" => 5,
                "name" => "Completed",
                "created_at" => "2023-01-24 14:50:03",
                "updated_at" => "2023-01-24 14:50:04",
            ),
            array(
                "id" => 6,
                "name" => "Rescheduled",
                "created_at" => "2023-01-24 14:50:03",
                "updated_at" => "2023-01-24 14:50:04",
            ),
            array(
                "id" => 7,
                "name" => "Cancelled",
                "created_at" => "2023-01-24 14:50:03",
                "updated_at" => "2023-01-24 14:50:04",
            ),
            array(
                "id" => 8,
                "name" => "Advance",
                "created_at" => "2023-01-24 14:50:03",
                "updated_at" => "2023-01-24 14:50:04",
            ),
            array(
                "id" => 9,
                "name" => "Request for Cancellation",
                "created_at" => "2023-01-24 14:50:03",
                "updated_at" => "2023-01-24 14:50:04",
            ),
            array(
                "id" => 10,
                "name" => "Request for Reschedule",
                "created_at" => "2023-01-24 14:50:03",
                "updated_at" => "2023-01-24 14:50:04",
            ),

        );

        $crm_vehicle_type_reference = array(
            array(
                "id" => 1,
                "name" => "Truck",
                "created_at" => "2023-01-24 14:50:03",
                "updated_at" => "2023-01-24 14:50:04",
            ),
            array(
                "id" => 2,
                "name" => "Motorcycle",
                "created_at" => "2023-01-24 14:50:03",
                "updated_at" => "2023-01-24 14:50:04",
            ),
        );

        $crm_mode_of_payment_reference = array(
            array(
                "id" => 1,
                "name" => "PP",
                "created_at" => "2023-01-24 14:50:03",
                "updated_at" => "2023-01-24 14:50:04",
            ),
            array(
                "id" => 2,
                "name" => "COD",
                "created_at" => "2023-01-24 14:50:03",
                "updated_at" => "2023-01-24 14:50:04",
            ),
            array(
                "id" => 3,
                "name" => "PDT",
                "created_at" => "2023-01-24 14:50:03",
                "updated_at" => "2023-01-24 14:50:04",
            ),
            array(
                "id" => 4,
                "name" => "CS",
                "created_at" => "2023-01-24 14:50:03",
                "updated_at" => "2023-01-24 14:50:04",
            ),
            array(
                "id" => 5,
                "name" => "CC",
                "created_at" => "2023-01-24 14:50:03",
                "updated_at" => "2023-01-24 14:50:04",
            ),
        );

        $crm_booking_failed_reason_reference = array(
            array(
                "id" => 1,
                "name" => "Cargo not yet ready",
                "created_at" => "2023-01-24 14:50:03",
                "updated_at" => "2023-01-24 14:50:04",
            ),
            array(
                "id" => 2,
                "name" => "Customer asked to reschedule",
                "created_at" => "2023-01-24 14:50:03",
                "updated_at" => "2023-01-24 14:50:04",
            ),
            array(
                "id" => 3,
                "name" => "Shipper not available",
                "created_at" => "2023-01-24 14:50:03",
                "updated_at" => "2023-01-24 14:50:04",
            ),
            array(
                "id" => 4,
                "name" => "Office is close",
                "created_at" => "2023-01-24 14:50:03",
                "updated_at" => "2023-01-24 14:50:04",
            ),
            array(
                "id" => 5,
                "name" => "Shipper refuse to acknowledge booking",
                "created_at" => "2023-01-24 14:50:03",
                "updated_at" => "2023-01-24 14:50:04",
            ),
            array(
                "id" => 6,
                "name" => "Wrong booking details",
                "created_at" => "2023-01-24 14:50:03",
                "updated_at" => "2023-01-24 14:50:04",
            ),
            array(
                "id" => 7,
                "name" => "Unresponsive shipper",
                "created_at" => "2023-01-24 14:50:03",
                "updated_at" => "2023-01-24 14:50:04",
            ),
            array(
                "id" => 8,
                "name" => "Pick up address not accessible",
                "created_at" => "2023-01-24 14:50:03",
                "updated_at" => "2023-01-24 14:50:04",
            ),
            array(
                "id" => 9,
                "name" => "Waiting time policy not followed",
                "created_at" => "2023-01-24 14:50:03",
                "updated_at" => "2023-01-24 14:50:04",
            ),
            array(
                "id" => 10,
                "name" => "Vehicle breakdown/malfunction",
                "created_at" => "2023-01-24 14:50:03",
                "updated_at" => "2023-01-24 14:50:04",
            ),
            array(
                "id" => 11,
                "name" => "Lack of time to complete the activity",
                "created_at" => "2023-01-24 14:50:03",
                "updated_at" => "2023-01-24 14:50:04",
            ),
        );
        
        DB::table('crm_booking_status_reference')->insert($crm_booking_status_reference);
        DB::table('crm_vehicle_type_reference')->insert($crm_vehicle_type_reference);
        DB::table('crm_mode_of_payment_reference')->insert($crm_mode_of_payment_reference);
        DB::table('crm_booking_failed_reason_reference')->insert($crm_booking_failed_reason_reference);
    }
}
