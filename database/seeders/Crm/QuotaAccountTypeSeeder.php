<?php

namespace Database\Seeders\Crm;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class QuotaAccountTypeSeeder extends Seeder
{
    public function run()
    {
        $crm_quota_account_type = array(
            array(
                "name" => "Corporate Accounts",
            ),
            array(
                "name" => "Existing Non-Accounts",
            ),
            array(
                "name" => "New Accounts",
            ),
            array(
                "name" => "Non-Accounts",
            ),
        );
        DB::table('crm_quota_account_type')->insert($crm_quota_account_type);
    }
}
