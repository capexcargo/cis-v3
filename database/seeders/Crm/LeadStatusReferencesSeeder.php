<?php

namespace Database\Seeders\Crm;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LeadStatusReferencesSeeder extends Seeder
{
    public function run()
    {
        $crm_lead_status = array(
            array(
                "id" => 1,
                "name" => "Qualified",
                "created_at" => "2023-01-24 14:50:03",
                "updated_at" => "2023-01-24 14:50:04",
            ),
            array(
                "id" => 2,
                "name" => "Unqualified",
                "created_at" => "2023-01-24 14:50:16",
                "updated_at" => "2023-01-24 14:50:16",
            ),
            array(
                "id" => 3,
                "name" => "Converted",
                "created_at" => "2023-01-24 14:50:28",
                "updated_at" => "2023-01-24 14:50:28",
            ),
            array(
                "id" => 4,
                "name" => "Retired",
                "created_at" => "2023-01-24 14:50:38",
                "updated_at" => "2023-01-24 14:50:39",
            ),
        );
        
        DB::table('crm_lead_status')->insert($crm_lead_status);
    }
}
