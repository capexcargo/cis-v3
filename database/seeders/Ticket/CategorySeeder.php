<?php

namespace Database\Seeders\Ticket;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    public function run()
    {
        $ticket_category = array(
            array(
                "id" => 1,
                "name" => "Technical Support",
                "division_id" => 1,
                "created_by" => 4,
                "created_at" => "2022-09-13 10:13:09",
                "updated_at" => "2022-09-13 10:13:10",
            ),
            array(
                "id" => 2,
                "name" => "Help Desk",
                "division_id" => 1,
                "created_by" => 4,
                "created_at" => "2022-09-13 10:13:09",
                "updated_at" => "2022-09-13 10:13:10",
            ),
            array(
                "id" => 3,
                "name" => "System Admin",
                "division_id" => 1,
                "created_by" => 4,
                "created_at" => "2022-09-13 10:13:09",
                "updated_at" => "2022-09-13 10:13:10",
            ),
            array(
                "id" => 4,
                "name" => "Category4",
                "division_id" => 1,
                "created_by" => 4,
                "created_at" => "2022-09-13 10:13:09",
                "updated_at" => "2022-09-13 10:13:10",
            ),
            array(
                "id" => 5,
                "name" => "Category5",
                "division_id" => 1,
                "created_by" => 4,
                "created_at" => "2022-09-13 10:13:09",
                "updated_at" => "2022-09-13 10:13:10",
            ),
            array(
                "id" => 6,
                "name" => "Category6",
                "division_id" => 1,
                "created_by" => 4,
                "created_at" => "2022-09-13 10:13:09",
                "updated_at" => "2022-09-13 10:13:10",
            ),
            array(
                "id" => 7,
                "name" => "Category7",
                "division_id" => 1,
                "created_by" => 4,
                "created_at" => "2022-09-13 10:13:09",
                "updated_at" => "2022-09-13 10:13:10",
            ),
            array(
                "id" => 8,
                "name" => "Category8",
                "division_id" => 1,
                "created_by" => 4,
                "created_at" => "2022-09-13 10:13:09",
                "updated_at" => "2022-09-13 10:13:10",
            ),
            array(
                "id" => 9,
                "name" => "Category9",
                "division_id" => 1,
                "created_by" => 4,
                "created_at" => "2022-09-13 10:13:09",
                "updated_at" => "2022-09-13 10:13:10",
            ),
            array(
                "id" => 10,
                "name" => "Category10",
                "division_id" => 1,
                "created_by" => 4,
                "created_at" => "2022-09-13 10:13:09",
                "updated_at" => "2022-09-13 10:13:10",
            ),
            array(
                "id" => 11,
                "name" => "Category11",
                "division_id" => 1,
                "created_by" => 4,
                "created_at" => "2022-09-13 10:13:09",
                "updated_at" => "2022-09-13 10:13:10",
            ),
        );
        DB::table('ticket_category')->insert($ticket_category);
    }
}
