<?php

namespace Database\Seeders\Accounting;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TermsReferenceSeeder extends Seeder
{
    public function run()
    {
        $accounting_terms_reference = [
            [
                'id' => 1,
                'code' => 'days',
                'display' => 'Days',
            ],
            [
                'id' => 2,
                'code' => 'months',
                'display' => 'Months',
            ],
            [
                'id' => 3,
                'code' => 'without_terms',
                'display' => 'Without Terms',
            ]
        ];

        DB::table('accounting_terms_reference')->insert($accounting_terms_reference);
    }
}
