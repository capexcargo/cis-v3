<?php

namespace Database\Seeders\Accounting;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RequestForPaymentTypeReferenceSeeder extends Seeder
{
    public function run()
    {
        $accounting_rfp_type_reference = [
            [
                'id' => 1,
                'code' => 'request_for_payment',
                'display' => 'Request For Payment',
            ],
            [
                'id' => 2,
                'code' => 'freight',
                'display' => 'Freight',
            ],
            [
                'id' => 3,
                'code' => 'payables',
                'display' => 'Payables',
            ],
            [
                'id' => 4,
                'code' => 'cash_advance',
                'display' => 'Cash Advance',
            ]
        ];

        DB::table('accounting_rfp_type_reference')->insert($accounting_rfp_type_reference);
    }
}
