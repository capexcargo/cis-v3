<?php

namespace Database\Seeders\Accounting;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AccountTypeReferenceSeeder extends Seeder
{

    public function run()
    {
        $accounting_account_type_reference = [
            [
                'id' => 1,
                'code' => 'with_terms',
                'display' => 'With Terms',
            ],
            [
                'id' => 2,
                'code' => 'without_terms',
                'display' => 'Without Terms',
            ]
        ];

        DB::table('accounting_account_type_reference')->insert($accounting_account_type_reference);
    }
}
