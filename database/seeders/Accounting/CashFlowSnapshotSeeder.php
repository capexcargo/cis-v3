<?php

namespace Database\Seeders\Accounting;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CashFlowSnapshotSeeder extends Seeder
{
    public function run()
    {
        $accounting_cash_flow_snapshot = [
            [
                'id' => 1,
                'current_balance' => 0,
                'floating_balance' => 0,
                'year' => 2022,
            ]
        ];

        DB::table('accounting_cash_flow_snapshot')->insert($accounting_cash_flow_snapshot);
    }
}
