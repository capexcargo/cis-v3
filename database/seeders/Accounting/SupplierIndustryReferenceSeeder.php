<?php

namespace Database\Seeders\Accounting;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SupplierIndustryReferenceSeeder extends Seeder
{
    public function run()
    {
        $accounting_supplier_industry_reference = [
            [
                'id' => 1,
                'code' => 'industry_1',
                'display' => 'Industry 1',
            ],
            [
                'id' => 2,
                'code' => 'industry_2',
                'display' => 'Industry 2',
            ],
        ];

        DB::table('accounting_supplier_industry_reference')->insert($accounting_supplier_industry_reference);
    }
}
