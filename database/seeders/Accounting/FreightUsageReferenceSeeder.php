<?php

namespace Database\Seeders\Accounting;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FreightUsageReferenceSeeder extends Seeder
{
    public function run()
    {
        $accounting_freight_usage_reference = [
            [
                'id' => 1,
                'code' => 'origin',
                'display' => 'Origin',
            ],
            [
                'id' => 2,
                'code' => 'destination',
                'display' => 'Destination',
            ]
        ];

        DB::table('accounting_freight_usage_reference')->insert($accounting_freight_usage_reference);
    }
}
