<?php

namespace Database\Seeders\Accounting;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TruckingTypeReferenceSeeder extends Seeder
{
    
    public function run()
    {
        $accounting_trucking_type_reference = array(
            array(
                "id" => 1,
                "code" => "l300",
                "display" => "L300",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 2,
                "code" => "suv",
                "display" => "SUV",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 3,
                "code" => "canter",
                "display" => "CANTER",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 4,
                "code" => "4_wheels_closed_van",
                "display" => "4 WHEELS CLOSED VAN",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 5,
                "code" => "4_wheels_open",
                "display" => "4 WHEELS OPEN",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 6,
                "code" => "6_wheels_closed_van",
                "display" => "6 WHEELS CLOSED VAN",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 7,
                "code" => "6_wheels_open",
                "display" => "6 WHEELS OPEN",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 8,
                "code" => "forward_closed_van",
                "display" => "FORWARD CLOSED VAN",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 9,
                "code" => "forward_open",
                "display" => "FORWARD OPEN",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 10,
                "code" => "10_wheels/wingvan",
                "display" => "10 WHEELS/WINGVAN",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 11,
                "code" => "16_wheels",
                "display" => "16 WHEELS",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 12,
                "code" => "boom_truck_4_wheels",
                "display" => "BOOM TRUCK 4 WHEELS",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 13,
                "code" => "boom_truck_6_wheels",
                "display" => "BOOM TRUCK 6 WHEELS",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 14,
                "code" => "boom_truck_10_wheels",
                "display" => "BOOM TRUCK 10 WHEELS",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 15,
                "code" => "20ft_chasis",
                "display" => "20FT CHASIS",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
            array(
                "id" => 16,
                "code" => "40ft_chasis/2x20_back_to_back",
                "display" => "40FT CHASIS/2X20 BACK TO BACK",
                "is_visible" => 1,
                "created_at" => NULL,
                "updated_at" => NULL,
            ),
        );

        DB::table('accounting_trucking_type_reference')->insert($accounting_trucking_type_reference);
    }
}
