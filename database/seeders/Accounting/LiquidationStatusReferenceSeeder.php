<?php

namespace Database\Seeders\Accounting;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LiquidationStatusReferenceSeeder extends Seeder
{
    public function run()
    {
        $accounting_liquidation_status_reference = [
            [
                'id' => 1,
                'code' => 'open',
                'display' => 'Open',
            ],
            [
                'id' => 2,
                'code' => 'equaled',
                'display' => 'Equaled',
            ],
            [
                'id' => 3,
                'code' => 'closed',
                'display' => 'Closed',
            ]
        ];

        DB::table('accounting_liquidation_status_reference')->insert($accounting_liquidation_status_reference);
    }
}
