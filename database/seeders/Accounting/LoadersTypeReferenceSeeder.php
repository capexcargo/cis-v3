<?php

namespace Database\Seeders\Accounting;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LoadersTypeReferenceSeeder extends Seeder
{
    public function run()
    {
        $accounting_loaders_type_reference = [
            [
                'id' => 1,
                'code' => 'airline',
                'display' => 'Airline',
            ],
            [
                'id' => 2,
                'code' => 'sea_freight',
                'display' => 'Sea Freight',
            ],
            [
                'id' => 3,
                'code' => 'wingvan',
                'display' => 'Wingvan',
            ]
        ];

        DB::table('accounting_loaders_type_reference')->insert($accounting_loaders_type_reference);
    }
}
