<?php

namespace Database\Seeders\Accounting;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MultipleBudgetReferenceSeeder extends Seeder
{
    
    public function run()
    {
        $accounting_multiple_budget_reference = [
            [
                'id' => 1,
                'code' => 'single',
                'display' => 'Single',
            ],
            [
                'id' => 2,
                'code' => 'multiple',
                'display' => 'Multiple',
            ]
        ];

        DB::table('accounting_multiple_budget_reference')->insert($accounting_multiple_budget_reference);
    }
}
