<?php

namespace Database\Seeders\Accounting;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FreightReferenceTypeSeeder extends Seeder
{
    public function run()
    {
        $accounting_freight_reference_type = [
            [
                'id' => 1,
                'code' => 'bl_no',
                'display' => 'BL No',
            ],
            [
                'id' => 2,
                'code' => 'waybill',
                'display' => 'Waybill',
            ],
            [
                'id' => 3,
                'code' => 'wingvan',
                'display' => 'Wingvan',
            ],
            [
                'id' => 4,
                'code' => 'mawb',
                'display' => 'MAWB',
            ]
        ];

        DB::table('accounting_freight_reference_type')->insert($accounting_freight_reference_type);
    }
}
