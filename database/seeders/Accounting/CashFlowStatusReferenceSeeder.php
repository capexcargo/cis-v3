<?php

namespace Database\Seeders\Accounting;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CashFlowStatusReferenceSeeder extends Seeder
{

    public function run()
    {
        $accounting_cf_status_reference = [
            [
                'id' => 1,
                'code' => 'unbudgeted',
                'display' => 'Unbudgeted',
            ],
            [
                'id' => 2,
                'code' => 'budgeted',
                'display' => 'Budgeted',
            ],
            [
                'id' => 3,
                'code' => 'good_in_bank',
                'display' => 'Good In Bank',
            ]
        ];

        DB::table('accounting_cf_status_reference')->insert($accounting_cf_status_reference);
    }
}
