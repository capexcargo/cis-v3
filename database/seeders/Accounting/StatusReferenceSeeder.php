<?php

namespace Database\Seeders\Accounting;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusReferenceSeeder extends Seeder
{
    public function run()
    {
        $accounting_status_reference = [
            [
                'id' => 1,
                'type' => 1,
                'code' => 0,
                'display' => "Pending",
            ],
            [
                'id' => 2,
                'type' => 1,
                'code' => 1,
                'display' => 'Approved',
            ],
            [
                'id' => 3,
                'type' => 1,
                'code' => 2,
                'display' => "Reject",
            ],
            
            [
                'id' => 4,
                'type' => 2,
                'code' => 0,
                'display' => "Pending",
            ],
            [
                'id' => 5,
                'type' => 2,
                'code' => 1,
                'display' => 'Approved',
            ],
            [
                'id' => 6,
                'type' => 2,
                'code' => 2,
                'display' => "Reject",
            ]
        ];

        DB::table('accounting_status_reference')->insert($accounting_status_reference);
    }
}
