<?php

namespace Database\Seeders\Accounting;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PriorityReferenceSeeder extends Seeder
{
    public function run()
    {
        $accounting_priority_reference = [
            [
                'id' => 1,
                'code' => 'critically_important',
                'display' => 'Critically Important (3-5 Days)',
            ],
            [
                'id' => 2,
                'code' => 'very_important',
                'display' => 'Very Important (5-7 Days)',
            ],
            [
                'id' => 3,
                'code' => 'important',
                'display' => 'Important (7-10 Days)',
            ],
            [
                'id' => 4,
                'code' => 'less_important',
                'display' => 'Less Important (More Than 10 Days)',
            ]
        ];

        DB::table('accounting_priority_reference')->insert($accounting_priority_reference);
    }
}
