<?php

namespace Database\Seeders\Accounting;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SupplierTypeReference extends Seeder
{
    public function run()
    {
        $accounting_supplier_type_reference = [
            [
                'id' => 1,
                'code' => 'individual',
                'display' => 'Individual',
            ],
            [
                'id' => 2,
                'code' => 'business',
                'display' => 'Business',
            ],
        ];

        DB::table('accounting_supplier_type_reference')->insert($accounting_supplier_type_reference);
    }
}
