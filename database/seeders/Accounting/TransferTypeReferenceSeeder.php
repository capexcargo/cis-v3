<?php

namespace Database\Seeders\Accounting;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TransferTypeReferenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $accounting_transfer_type_reference = [
            [
                'id' => 1,
                'code' => 'direct_transfer',
                'display' => 'Direct Transfer',
            ],
            [
                'id' => 2,
                'code' => 'transfer_budget',
                'display' => 'Transfer Budget',
            ]
        ];

        DB::table('accounting_transfer_type_reference')->insert($accounting_transfer_type_reference);
    }
}
