<?php

namespace Database\Seeders\Accounting;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CAReferenceTypesSeeder extends Seeder
{
    
    public function run()
    {
        $accounting_ca_reference_types = [
            [
                'id' => 1,
                'code' => 'mobile_no',
                'display' => 'Mobile No/s',
            ],
            [
                'id' => 2,
                'code' => 'cpx_awb_no',
                'display' => 'CPX AWB No.',
            ],
            [
                'id' => 3,
                'code' => 'mawb_no',
                'display' => 'MAWB No.',
            ],
            [
                'id' => 4,
                'code' => 'mbl_no',
                'display' => 'MBL No.',
            ],
            [
                'id' => 5,
                'code' => 'liquidation_no',
                'display' => 'Liquidation No.',
            ],
            [
                'id' => 6,
                'code' => 'billing_ref_no',
                'display' => 'Billing Ref. No.',
            ],
            [
                'id' => 7,
                'code' => 'plate_no',
                'display' => 'Plate No.',
            ],
            [
                'id' => 8,
                'code' => 'btta_no_/_obas_no',
                'display' => 'BTTA No/ OBAS No.',
            ]
        ];

        DB::table('accounting_ca_reference_types')->insert($accounting_ca_reference_types);
    }
}
