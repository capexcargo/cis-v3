<?php

namespace Database\Seeders\Accounting;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentTypeReferenceSeeder extends Seeder
{
    public function run()
    {
        $accounting_payment_type_reference = [
            [
                'id' => 1,
                'code' => 'cash',
                'display' => 'Cash',
            ],
            [
                'id' => 2,
                'code' => 'pdc',
                'display' => 'PDC',
            ],
            [
                'id' => 3,
                'code' => 'check',
                'display' => 'Check',
            ]
        ];

        DB::table('accounting_payment_type_reference')->insert($accounting_payment_type_reference);
    }
}
