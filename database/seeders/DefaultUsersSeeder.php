<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DefaultUsersSeeder extends Seeder
{
    public function run()
    {
        $users = [
            [
                'id' => 1,
                'role_id' => 1,
                'division_id' => 1,
                'level_id' => 5,
                'status_id' => 1,
                'name' => 'Administrator',
                'email' => 'it.cargopadala@capex.ph',
                'email_verified_at' => null,
                'email_verification_code' => null,
                'password' => Hash::make('!Passw0rd'),
                'attempt' => 0,
                'two_factor_secret' => null,
                'two_factor_recovery_codes' => null,
                'remember_token' => null,
                'current_team_id' => null,
                'profile_photo_path' => null,
                'created_at' => '2021-12-21 02:08:24',
                'updated_at' => '2021-12-21 02:08:24',
            ],
        ];

        $user_details = [
            [
                'id' => 1,
                'user_id' => 1,
                'employee_number' => null,
                'first_name' => 'Administrator',
                'middle_name' => null,
                'last_name' => null,
                'suffix_id' => null,
                'personal_email' => 'it.cargopadala@capex.ph',
                'company_email' => 'it.cargopadala@capex.ph',
                'age' => null,
                'birth_date' => null,
                'personal_mobile_number' => null,
                'personal_telephone_number' => null,
                'current_country_code' => null,
                'current_region_code' => null,
                'current_province_code' => null,
                'current_city_code' => null,
                'current_barangay_code' => null,
                'current_street' => null,
                'created_at' => '2021-12-21 02:08:24',
                'updated_at' => '2021-12-21 02:08:24',
            ],
        ];

        DB::table('users')->insert($users);
        DB::table('user_details')->insert($user_details);
    }
}
