<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AccountStatusReferenceSeeder extends Seeder
{

    public function run()
    {
        $account_status_reference = [
            [
                'id' => 1,
                'code' => 'active',
                'display' => 'Active',
                'text_color' => 'text-green',
                'bg_color' => 'bg-[#1C9C0D]',
            ],
            [
                'id' => 2,
                'code' => 'inactive',
                'display' => 'Inactive',
                'text_color' => 'text-red',
                'bg_color' => 'bg-[#CC0000]',
            ],
            [
                'id' => 3,
                'code' => 'deactivate',
                'display' => 'Deactivate',
                'text_color' => 'text-red',
                'bg_color' => 'bg-[#CC0000]',
            ]
        ];
        DB::table('account_status_reference')->insert($account_status_reference);
    }
}
