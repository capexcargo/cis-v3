<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
            Accounting\PaymentTypeReferenceSeeder::class,
            Accounting\MultipleBudgetReferenceSeeder::class,
            Accounting\PriorityReferenceSeeder::class,
            Accounting\RequestForPaymentTypeReferenceSeeder::class,
            Accounting\FreightReferenceTypeSeeder::class,
            Accounting\TermsReferenceSeeder::class,
            Accounting\SupplierIndustryReferenceSeeder::class,
            Accounting\SupplierTypeReference::class,
            Accounting\TransferTypeReferenceSeeder::class,
            Accounting\StatusReferenceSeeder::class,
            Accounting\FreightUsageReferenceSeeder::class,
            Accounting\AccountTypeReferenceSeeder::class,
            Accounting\CAReferenceTypesSeeder::class,
            Accounting\TruckingTypeReferenceSeeder::class,
            Accounting\LoadersTypeReferenceSeeder::class,
            Accounting\LoadersSeeder::class,
            Accounting\LiquidationStatusReferenceSeeder::class,
            Accounting\CashFlowStatusReferenceSeeder::class,
            Accounting\CashFlowSnapshotSeeder::class,

            Hrim\DisciplinaryRecordManagement\CocSectionSeeder::class,
            Hrim\DisciplinaryRecordManagement\ViolationSeeder::class,
            Hrim\DisciplinaryRecordManagement\DisciplinaryRecordSeeder::class,
            Hrim\DisciplinaryRecordManagement\SanctionSeeder::class,
            Hrim\DisciplinaryRecordManagement\SanctionStatusSeeder::class,
            Hrim\Workforce\DepartmentSeeder::class,
            Hrim\Workforce\JobLevelSeeder::class,

            DivisionSeeder::class,
            BranchReferenceSeeder::class,
            MonthReferenceSeeder::class,

            SuffixReferenceSeeder::class,
            AccountStatusReferenceSeeder::class,
            LevelReferenceSeeder::class,
            AccountTypeReferenceSeeder::class,
            RoleSeeder::class,
            RoleAccessSeeder::class,

            DefaultUsersSeeder::class,
        ]);
    }
}
