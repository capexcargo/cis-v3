<?php

namespace Database\Seeders\Fims\DataManagement;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FimsPurchasingTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $acctng_purchasing_type_reference = array(
            array(
                "id" => 1,
                "name" => "Standard purchase orders",
                "created_at" => "2023-12-06 16:46:09",
                "updated_at" => "2023-12-06 16:46:10",
            ),

            array(
                "id" => 2,
                "name" => "Special purchase orders",
                "created_at" => "2023-12-06 16:46:09",
                "updated_at" => "2023-12-06 16:46:10",
            ),

            array(
                "id" => 3,
                "name" => "Planned purchase orders",
                "created_at" => "2023-12-06 16:46:09",
                "updated_at" => "2023-12-06 16:46:10",
            ),

            array(
                "id" => 4,
                "name" => "Blanket purchase orders",
                "created_at" => "2023-12-06 16:46:09",
                "updated_at" => "2023-12-06 16:46:10",
            ),
            array(
                "id" => 5,
                "name" => "Contract purchase orders",
                "created_at" => "2023-12-06 16:46:09",
                "updated_at" => "2023-12-06 16:46:10",
            ),
        );

        DB::table('acctng_purchasing_type_reference')->insert($acctng_purchasing_type_reference);
    }
}
