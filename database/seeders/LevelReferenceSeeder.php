<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LevelReferenceSeeder extends Seeder
{
    public function run()
    {
        $level_reference = [
            [
                'id' => 1,
                'code' => 'level_1',
                'display' => 'Level 1',
            ],
            [
                'id' => 2,
                'code' => 'level_2',
                'display' => 'Level 2',
            ],
            [
                'id' => 3,
                'code' => 'level_3',
                'display' => 'Level 3',
            ],
            [
                'id' => 4,
                'code' => 'level_4',
                'display' => 'Level 4',
            ],
            [
                'id' => 5,
                'code' => 'level_5',
                'display' => 'Level 5',
            ],
        ];

        DB::table('level_reference')->insert($level_reference);
    }
}
