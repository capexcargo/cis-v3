<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SuffixReferenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $suffix_reference = [
            [
                'code' => 'II',
                'display' => "The Second",
            ],
            [
                'code' => 'III',
                'display' => "The Third",
            ],
            [
                'code' => 'IV',
                'display' => "The Fourth",
            ],
            [
                'code' => 'Jr',
                'display' => "Junior",
            ],
            [
                'code' => 'Sr',
                'display' => "Senior",
            ]
        ];

        DB::table('suffix_reference')->insert($suffix_reference);
    }
}
