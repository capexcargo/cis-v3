<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColsFromAcctngPurchasingLoaManagement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acctng_purchasing_loa_management', function (Blueprint $table) {
            $table->float('min_amount')->change();
            $table->float('max_amount')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acctng_purchasing_loa_management', function (Blueprint $table) {
            //
        });
    }
}
