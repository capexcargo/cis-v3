<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAnotherColumnsToHrimPerformanceEvaluation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hrim_performance_evaluation', function (Blueprint $table) {
            $table->integer('employee_eval_status')->nullable()->after('third_status');
            $table->date('first_eval_date')->nullable()->after('third_evaluator');
            $table->date('second_eval_date')->nullable()->after('first_eval_date');
            $table->date('third_eval_date')->nullable()->after('second_eval_date');
            $table->date('employee_eval_date')->nullable()->after('third_eval_date');
            $table->longText('first_remarks')->nullable()->after('employee_eval_date');
            $table->longText('second_remarks')->nullable()->after('first_remarks');
            $table->longText('third_remarks')->nullable()->after('second_remarks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hrim_performance_evaluation', function (Blueprint $table) {
            //
        });
    }
}
