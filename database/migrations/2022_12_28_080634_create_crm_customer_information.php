<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmCustomerInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_contact_person', function (Blueprint $table) {
            $table->id();
            $table->integer('account_id');
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('last_name');
            $table->string('position');
            $table->boolean('is_primary')->default(0);

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('crm_email_address_list', function (Blueprint $table) {
            $table->id();
            $table->integer('account_type');
            $table->integer('account_id');
            $table->string('email');
            $table->boolean('is_primary')->default(0);

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('crm_mobile_list', function (Blueprint $table) {
            $table->id();
            $table->integer('account_type');
            $table->integer('account_id');
            $table->string('mobile');
            $table->boolean('is_primary')->default(0);

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('crm_telephone_list', function (Blueprint $table) {
            $table->id();
            $table->integer('account_type');
            $table->integer('account_id');
            $table->string('telephone');
            $table->boolean('is_primary')->default(0);

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('state', function (Blueprint $table) {
            $table->id();
            $table->string('name');

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('barangay', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('state_id');
            $table->integer('city_id');

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('city', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('state_id');

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('crm_address_label', function (Blueprint $table) {
            $table->id();
            $table->string('name');

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('crm_address_list', function (Blueprint $table) {
            $table->id();
            $table->integer('account_type');
            $table->integer('account_id');
            $table->longText('address_line_1');
            $table->longText('address_line_2');
            $table->integer('address_type');
            $table->integer('address_label');
            $table->integer('state_id');
            $table->integer('city_id');
            $table->integer('barangay_id');
            $table->integer('country_id');
            $table->integer('postal_id');
            $table->boolean('is_primary')->default(0);

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('crm_account_type', function (Blueprint $table) {
            $table->id();
            $table->string('name');

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('crm_country', function (Blueprint $table) {
            $table->id();
            $table->string('name');

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('crm_life_stage', function (Blueprint $table) {
            $table->id();
            $table->string('name');

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('crm_customer_information', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('account_type');
            $table->integer('account_no');
            $table->string('fullname');
            $table->string('company_name');
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name');
            $table->string('company_link');
            $table->string('company_email_address');
            $table->integer('company_tel_no');
            $table->integer('company_mobile_no');
            $table->unsignedBigInteger('industry_id');
            $table->unsignedBigInteger('life_stage_id');
            $table->boolean('is_mother_account')->default(0);
            $table->unsignedBigInteger('child_account_id');
            $table->unsignedBigInteger('contact_owner_id');
            $table->string('tin');
            $table->date('company_anniversary');
            $table->unsignedBigInteger('marketing_channel_id');
            $table->longText('notes');
            $table->integer('category');
            $table->unsignedBigInteger('created_by');

            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_customer_information');
        Schema::dropIfExists('crm_life_stage');
        Schema::dropIfExists('crm_country');
        Schema::dropIfExists('crm_account_type');
        Schema::dropIfExists('crm_address_list');
        Schema::dropIfExists('crm_address_label');
        Schema::dropIfExists('city');
        Schema::dropIfExists('barangay');
        Schema::dropIfExists('state');
        Schema::dropIfExists('crm_telephone_list');
        Schema::dropIfExists('crm_mobile_list');
        Schema::dropIfExists('crm_email_address_list');
        Schema::dropIfExists('crm_contact_person');
    }
}
