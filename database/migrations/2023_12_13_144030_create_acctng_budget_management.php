<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcctngBudgetManagement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acctng_budget_management', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('subaccount_name');
            $table->unsignedBigInteger('budget_source_id');
            $table->unsignedBigInteger('budget_category_id');
            $table->unsignedBigInteger('budget_coa_id');
            $table->unsignedBigInteger('budget_opex_id');
            $table->integer('type');
            $table->integer('year');
            $table->float('jan');
            $table->float('feb');
            $table->float('mar');
            $table->float('apr');
            $table->float('may');
            $table->float('jun');
            $table->float('jul');
            $table->float('aug');
            $table->float('sep');
            $table->float('oct');
            $table->float('nov');
            $table->float('dec');
            $table->float('total_amount');

            $table->timestamps();

            $table->foreign('budget_source_id')->references('id')->on('acctng_budget_source_reference');
            $table->foreign('budget_opex_id')->references('id')->on('acctng_opex_category_reference');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acctng_budget_management');
    }
}
