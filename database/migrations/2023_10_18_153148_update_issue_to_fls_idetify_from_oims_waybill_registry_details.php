<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateIssueToFlsIdetifyFromOimsWaybillRegistryDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('oims_waybill_registry_details', function (Blueprint $table) {
            $table->renameColumn('issue_to_fls_idetify', 'issue_to_fls_identify');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fls_idetify_from_oims_waybill_registry_details', function (Blueprint $table) {
            //
        });
    }
}
