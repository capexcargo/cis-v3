<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropFeignkeyLiquidationIdInAccountingLiquidationAttachments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounting_liquidation_attachments', function (Blueprint $table) {
            $table->dropForeign('accounting_liquidation_attachments_liquidation_id_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounting_liquidation_attachments', function (Blueprint $table) {
            //
        });
    }
}
