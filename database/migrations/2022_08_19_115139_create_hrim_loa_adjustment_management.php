<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrimLoaAdjustmentManagement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrim_admin_adj_category_reference', function (Blueprint $table) {
            $table->id();
            $table->string('display');
            $table->timestamps();
        });

        Schema::create('hrim_loa_adjustment_management', function (Blueprint $table) {
            $table->id();
            $table->decimal('min_amount');
            $table->decimal('max_amount');
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('approver_id');
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('hrim_admin_adj_category_reference');
            $table->foreign('approver_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrim_admin_adj_category_reference');
        Schema::dropIfExists('hrim_loa_adjustment_management');
    }
}
