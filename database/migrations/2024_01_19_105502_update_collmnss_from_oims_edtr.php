<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCollmnssFromOimsEdtr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('oims_edtr', function (Blueprint $table) {
            $table->dateTime('requested_start_break')->nullable()->after('status');
            $table->dateTime('requested_end_break')->nullable()->after('requested_start_break');
            $table->dateTime('actual_start_break')->nullable()->after('requested_end_break');
            $table->dateTime('actual_end_break')->nullable()->after('actual_start_break');
            $table->dateTime('break_approval')->nullable()->after('actual_end_break');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('oims_edtr', function (Blueprint $table) {
            //
        });
    }
}
