<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddApproved extends Migration
{
    public function up()
    {
        Schema::table('accounting_request_for_payment', function (Blueprint $table) {
            $table->boolean('is_approved_1')->nullable()->after('approver_3_id');
            $table->boolean('is_approved_2')->nullable()->after('is_approved_1');;
            $table->boolean('is_approved_3')->nullable()->after('is_approved_2');;
        });
    }

    public function down()
    {
        Schema::table('accounting_request_for_payment', function (Blueprint $table) {
            $table->dropColumn(['is_approved_1', 'is_approved_2', 'is_approved_3']);
        });
    }
}
