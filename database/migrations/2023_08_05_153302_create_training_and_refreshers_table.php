<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingAndRefreshersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_and_refreshers', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('description');
            $table->boolean('is_tag_all_position')->default(0);
            $table->integer('tagged_division');

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('training_and_refreshers_attachment', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('train_and_ref_id');
            $table->string('path');
            $table->string('name');
            $table->string('extension');

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('train_and_ref_id')->references('id')->on('training_and_refreshers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_and_refreshers_attachment');
        Schema::dropIfExists('training_and_refreshers');
    }
}
