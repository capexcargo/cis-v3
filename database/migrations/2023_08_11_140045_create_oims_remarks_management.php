<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOimsRemarksManagement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oims_remarks_management', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('type_of_remarks');
            $table->unsignedBigInteger('transaction_stage_id');
            $table->integer('status');

            $table->timestamps();

            $table->foreign('transaction_stage_id')->references('id')->on('oims_cargo_status_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oims_remarks_management');
    }
}
