<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColunmInHrimLeave extends Migration
{
    
    public function up()
    {
        Schema::table('hrim_leave', function (Blueprint $table) {
            $table->unsignedBigInteger('admin_approver')->nullable()->after('third_approver');
            $table->date('admin_approval_date')->nullable()->after('third_approval_date');
            $table->longText('admin_approver_remarks')->nullable()->after('third_approver_remarks');

            $table->foreign('admin_approver')->references('id')->on('users');
        });

        Schema::table('hrim_overtime', function (Blueprint $table) {
            $table->unsignedBigInteger('admin_approver')->nullable()->after('third_approver');
            $table->date('admin_approval_date')->nullable()->after('third_approval_date');
            $table->longText('admin_approver_remarks')->nullable()->after('third_approver_remarks');

            $table->foreign('admin_approver')->references('id')->on('users');
        });

        Schema::table('hrim_tar', function (Blueprint $table) {
            $table->unsignedBigInteger('admin_approver')->nullable()->after('third_approver');
            $table->date('admin_approval_date')->nullable()->after('third_approval_date');
            $table->longText('admin_approver_remarks')->nullable()->after('third_approver_remarks');

            $table->foreign('admin_approver')->references('id')->on('users');
        });

        Schema::table('hrim_schedule_adjustment', function (Blueprint $table) {
            $table->unsignedBigInteger('admin_approver')->nullable()->after('third_approver');
            $table->date('admin_approval_date')->nullable()->after('third_approval_date');
            $table->longText('admin_approver_remarks')->nullable()->after('third_approver_remarks');

            $table->foreign('admin_approver')->references('id')->on('users');
        });
    }

    public function down()
    {
       
    }
}
