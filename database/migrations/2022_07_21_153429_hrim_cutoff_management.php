<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class HrimCutoffManagement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrim_cutoff_management', function (Blueprint $table) {
            $table->id();
       
            $table->integer('category_id');
            $table->date('cutoff_date');
            $table->integer('payout_year');
            $table->integer('payout_month');
            $table->integer('payout_cutoff');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrim_cutoff_management');
    }
}
