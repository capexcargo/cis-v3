<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrimResignationReason extends Migration
{

    public function up()
    {
        Schema::create('hrim_resignation_reason_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::create('hrim_resignation_reason', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->date('date_resignation');
            $table->longText('reason_description');
            $table->unsignedBigInteger('reason_reference_id');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('reason_reference_id')->references('id')->on('hrim_resignation_reason_reference');
        });
    }


    public function down()
    {
        Schema::dropIfExists('hrim_resignation_reason');
        Schema::dropIfExists('hrim_resignation_reason_reference');
    }
}
