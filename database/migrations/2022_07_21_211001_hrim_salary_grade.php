<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class HrimSalaryGrade extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrim_salary_grade', function (Blueprint $table) {
            $table->id();

            $table->string('grade');
            $table->float('minimun_amount', 11, 2)->nullable();
            $table->float('maximum_amount', 11, 2)->nullable();
            $table->unsignedBigInteger('created_by');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrim_salary_grade');
    }
}
