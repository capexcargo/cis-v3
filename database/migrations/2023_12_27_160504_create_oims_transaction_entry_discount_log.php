<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOimsTransactionEntryDiscountLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oims_transaction_entry_discount_log', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('waybill_id');
            $table->unsignedBigInteger('discount_id');
            $table->string('discount_name');
            $table->float('discount_price');
            $table->dateTime('transaction_date');
            $table->string('promo_code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oims_transaction_entry_discount_log');
    }
}
