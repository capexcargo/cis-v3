<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmBookingHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_booking_history', function (Blueprint $table) {
                $table->id();
                $table->string('booking_reference_no');
                $table->unsignedBigInteger('booking_type_id');
                $table->unsignedBigInteger('vehicle_type_id');
                $table->date('pickup_date');
                $table->date('booking_date');
                $table->unsignedBigInteger('time_slot_id');
                $table->unsignedBigInteger('booking_category');
                $table->unsignedBigInteger('walk_in_branch_id')->nullable();
                $table->unsignedBigInteger('activity_type');
                $table->unsignedBigInteger('shipper_id');
                $table->string('shipper_customer_no');
                $table->unsignedBigInteger('consignee_category');
                $table->unsignedBigInteger('consignee_count');
                $table->longText('work_instruction')->nullable();
                $table->longText('remarks')->nullable();
                $table->unsignedBigInteger('assigned_team_id')->nullable();
                $table->unsignedBigInteger('final_status_id');
                $table->unsignedBigInteger('booking_branch_id');
                $table->unsignedBigInteger('marketing_channel_id');
                $table->unsignedBigInteger('booking_channel');
                $table->unsignedBigInteger('approval_status')->nullable();

                $table->unsignedBigInteger('created_user');
    
                $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_booking_history');
    }
}
