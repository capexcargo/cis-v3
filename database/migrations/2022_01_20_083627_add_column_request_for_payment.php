<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnRequestForPayment extends Migration
{
    public function up()
    {
        Schema::table('accounting_request_for_payment', function (Blueprint $table) {
            $table->renameColumn('canvassing_id', 'canvassing_supplier_id')->nullable()->change();
        });
    }

    public function down()
    {
        Schema::table('accounting_request_for_payment', function (Blueprint $table) {
            $table->renameColumn('canvassing_supplier_id', 'canvassing_id')->nullable()->change();
        });
    }
}
