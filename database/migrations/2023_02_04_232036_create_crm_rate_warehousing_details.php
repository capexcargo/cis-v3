<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmRateWarehousingDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_rate_warehousing_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('rate_id');
            $table->unsignedBigInteger('warehousing_option_id');
            $table->float('amount_short_term')->nullable();
            $table->float('amount_long_term')->nullable();

            $table->timestamps();

            $table->foreign('rate_id')->references('id')->on('crm_rate_warehousing');
            $table->foreign('warehousing_option_id')->references('id')->on('crm_rate_warehousing_option_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_rate_warehousing_details');
    }
}
