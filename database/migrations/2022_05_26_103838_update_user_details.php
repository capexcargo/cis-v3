<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUserDetails extends Migration
{

    public function up()
    {
        Schema::table('user_details', function (Blueprint $table) {
            $table->unsignedBigInteger('branch_id')->nullable()->after('user_id');
            $table->unsignedBigInteger('schedule_id')->nullable()->after('branch_id');

            $table->unsignedBigInteger('position_id')->nullable()->after('last_name');
            $table->unsignedBigInteger('job_level_id')->nullable()->after('position_id');
            $table->unsignedBigInteger('department_id')->nullable()->after('job_level_id');
            $table->unsignedBigInteger('division_id')->nullable()->after('department_id');
            $table->unsignedBigInteger('employment_category_id')->nullable()->after('division_id');
            $table->unsignedBigInteger('employment_status_id')->nullable()->after('employment_category_id');
            $table->unsignedBigInteger('gender_id')->nullable()->after('employment_status_id');
            $table->date('hired_date')->nullable()->after('birth_date');

            $table->string('personal_mobile_number')->nullable()->after('hired_date');
            $table->string('personal_telephone_number')->nullable()->after('personal_mobile_number');
            $table->string('personal_email')->nullable()->after('personal_telephone_number');

            $table->renameColumn('mobile_number', 'company_mobile_number')->nullable();
            $table->renameColumn('telephone_number', 'company_telephone_number')->nullable();
            $table->renameColumn('email', 'company_email')->nullable();

            $table->string('current_country_code')->nullable()->after('telephone_number');
            $table->string('current_region_code')->nullable()->after('current_country_code');
            $table->string('current_province_code')->nullable()->after('current_region_code');
            $table->string('current_city_code')->nullable()->after('current_province_code');
            $table->string('current_barangay_code')->nullable()->after('current_city_code');
            $table->string('current_street')->nullable()->after('current_barangay_code');
            $table->longText('current_address')->nullable()->after('current_street');

            $table->renameColumn('country', 'permanent_country_code')->nullable()->after('current_address');
            $table->renameColumn('region', 'permanent_region_code')->nullable()->after('permanent_country_code');
            $table->renameColumn('province', 'permanent_province_code')->nullable()->after('permanent_region_code');
            $table->renameColumn('city', 'permanent_city_code')->nullable()->after('permanent_province_code');
            $table->renameColumn('barangay', 'permanent_barangay_code')->nullable()->after('permanent_city_code');
            $table->renameColumn('street', 'permanent_street')->nullable()->after('permanent_barangay_code');
            $table->longText('permanent_address')->nullable()->after('street');
        });
    }


    public function down()
    {
        //
    }
}
