<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmStakeholderCategoryManagement extends Migration
{
    public function up()
    {
        Schema::create('branch_references_walkin', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedBigInteger('branch_reference_id');
            $table->timestamps();

            $table->foreign('branch_reference_id')->references('id')->on('crm_stakeholder_search_management_reference');
        });

        Schema::create('branch_references_quadrant', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedBigInteger('branch_reference_id');
            $table->integer('quadrant');
            $table->timestamps();

            $table->foreign('branch_reference_id')->references('id')->on('crm_stakeholder_search_management_reference');
        });

        Schema::create('crm_stakeholder_category_management', function (Blueprint $table) {
            $table->id();
            $table->string('stakeholder_category_name');
            $table->string('name');
            $table->unsignedBigInteger('subcategory');
            $table->unsignedBigInteger('management_id');
            $table->integer('account_type');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('crm_stakeholder_category_management');
        Schema::dropIfExists('branch_references_quadrant');
        Schema::dropIfExists('branch_references_walkin');
    }
}
