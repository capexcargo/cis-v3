<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkModeReferenceSeeder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrim_work_mode_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->boolean('is_visible')->nullable();
            $table->timestamps();
        });

        Schema::table('hrim_work_schedule', function (Blueprint $table) {
            $table->unsignedBigInteger('work_mode_id')->after('name');
            $table->time('ot_break_from')->after('break_to')->nullable();
            $table->time('ot_break_to')->after('ot_break_from')->nullable();

            $table->foreign('work_mode_id')->references('id')->on('hrim_work_mode_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrim_work_mode_reference');
    }
}
