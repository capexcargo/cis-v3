<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCollmnssFromOimsEdtrDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('oims_edtr_details', function (Blueprint $table) {
            $table->dateTime('actual_start_travel_time')->nullable()->after('estimated_time_activity');
            $table->dateTime('actual_end_travel_time')->nullable()->after('actual_start_travel_time');
            $table->dateTime('pickup_confirmed_time')->nullable()->after('actual_end_travel_time');
            $table->dateTime('pickup_start_time')->nullable()->after('pickup_confirmed_time');
            $table->dateTime('pickup_end_time')->nullable()->after('pickup_start_time');
            $table->dateTime('delivery_start_time')->nullable()->after('pickup_end_time');
            $table->dateTime('delivery_end_time')->nullable()->after('delivery_start_time');
            $table->integer('delivery_status')->nullable()->after('delivery_end_time');
            $table->dateTime('completed_date')->nullable()->after('delivery_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('oims_edtr_details', function (Blueprint $table) {
            //
        });
    }
}
