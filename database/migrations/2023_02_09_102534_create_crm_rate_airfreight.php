<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmRateAirfreight extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_rate_airfreight', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->date('effectivity_date');
            $table->longText('description')->nullable();
            $table->unsignedBigInteger('transport_mode_id');
            $table->unsignedBigInteger('commodity_type_id');
            $table->unsignedBigInteger('apply_for_id')->nullable();
            $table->unsignedBigInteger('ancillary_charge_id');
            $table->unsignedBigInteger('service_mode_id');
            $table->boolean('is_vice_versa')->nullable();
            $table->unsignedBigInteger('approver1_id');
            $table->unsignedBigInteger('approver2_id');
            $table->unsignedBigInteger('approver1_status_id')->nullable();
            $table->unsignedBigInteger('approver2_status_id')->nullable();
            $table->date('approver1_date')->nullable();
            $table->date('appover2_date')->nullable();
            $table->unsignedBigInteger('final_status_id');
            $table->unsignedBigInteger('base_rate_id')->nullable();
            $table->unsignedBigInteger('created_by');

            $table->timestamps();

            $table->foreign('transport_mode_id')->references('id')->on('crm_rate_transport_mode_reference');
            $table->foreign('commodity_type_id')->references('id')->on('crm_commodity_type_reference');
            $table->foreign('apply_for_id')->references('id')->on('crm_rate_apply_for_reference');
            $table->foreign('ancillary_charge_id')->references('id')->on('crm_ancillary_display_management');
            $table->foreign('service_mode_id')->references('id')->on('crm_service_mode_reference');
            $table->foreign('approver1_id')->references('id')->on('users');
            $table->foreign('approver2_id')->references('id')->on('users');
            $table->foreign('approver1_status_id')->references('id')->on('crm_approval_reference');
            $table->foreign('approver2_status_id')->references('id')->on('crm_approval_reference');
            $table->foreign('final_status_id')->references('id')->on('crm_approval_reference');
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_rate_airfreight');
    }
}
