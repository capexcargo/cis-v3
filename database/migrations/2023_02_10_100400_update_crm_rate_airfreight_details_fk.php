<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCrmRateAirfreightDetailsFk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_rate_airfreight_details', function (Blueprint $table) {
            $table->dropForeign('crm_rate_airfreight_details_rate_id_foreign');
        });

        Schema::table('crm_rate_airfreight_details', function (Blueprint $table) {
            $table->foreign('rate_id')->references('id')->on('crm_rate_airfreight');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
