<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCollmnssFromOimsTransactionEntryCustomerAddressList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('oims_transaction_entry_customer_address_list', function (Blueprint $table) {
            $table->longText('address_line_2')->nullable()->change();
            $table->unsignedBigInteger('country_id')->nullable()->change();
            $table->string('contact_person_reference')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('oims_transaction_entry_customer_address_list', function (Blueprint $table) {
            //
        });
    }
}
