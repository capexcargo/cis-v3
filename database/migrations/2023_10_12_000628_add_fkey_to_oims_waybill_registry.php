<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkeyToOimsWaybillRegistry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('oims_waybill_registry', function (Blueprint $table) {
            $table->foreign('issued_to_emp_id')->references('id')->on('user_details');
            $table->foreign('issued_to_om')->references('id')->on('user_details');
            $table->foreign('issued_to_fls')->references('id')->on('user_details');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('oims_waybill_registry', function (Blueprint $table) {
            //
        });
    }
}
