<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRFPAttachments extends Migration
{
    
    public function up()
    {
        Schema::create('accounting_rfp_attachments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('request_for_payment_id');
            $table->string('path');
            $table->string('name');
            $table->timestamps();

            $table->foreign('request_for_payment_id')->references('id')->on('accounting_request_for_payment');
        });
    }

    public function down()
    {
        Schema::dropIfExists('accounting_rfp_attachments');
    }
}
