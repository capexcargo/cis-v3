<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgencies extends Migration
{

    public function up()
    {
        Schema::create('hrim_agencies', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->longText('description')->nullable();
            $table->unsignedBigInteger('branch_id')->nullable();
            $table->boolean('is_active')->default(1);
            $table->timestamps();

            $table->foreign('branch_id')->references('id')->on('branch_reference');
        });

        Schema::table('user_details', function (Blueprint $table) {
            $table->unsignedBigInteger('agency_id')->after('user_id')->nullable();

            $table->foreign('agency_id')->references('id')->on('hrim_agencies');
        });
    }


    public function down()
    {
        Schema::dropIfExists('hrim_agencies');
    }
}
