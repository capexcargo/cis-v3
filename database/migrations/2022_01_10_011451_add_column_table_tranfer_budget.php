<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTableTranferBudget extends Migration
{

    public function up()
    {
        Schema::table('accounting_transfer_budget', function (Blueprint $table) {
            $table->unsignedBigInteger('division_id_from')->nullable()->after('id');
            $table->unsignedBigInteger('division_id_to')->nullable()->after('division_id_from');
            $table->unsignedBigInteger('budget_source_id_from')->nullable()->after('division_id_to');
            $table->unsignedBigInteger('budget_source_id_to')->nullable()->after('budget_source_id_from');
            $table->unsignedBigInteger('budget_chart_id_from')->nullable()->after('budget_source_id_to');
            $table->unsignedBigInteger('budget_chart_id_to')->nullable()->after('budget_chart_id_from');
            $table->integer('year')->nullable()->after('amount');

            $table->renameColumn('budget_plan_id', 'budget_plan_id_to')->after('budget_plan_id_from')->change();

            $table->foreign('division_id_from')->references('id')->on('division');
            $table->foreign('division_id_to')->references('id')->on('division');
            $table->foreign('budget_source_id_from')->references('id')->on('accounting_budget_source');
            $table->foreign('budget_source_id_to')->references('id')->on('accounting_budget_source');
            $table->foreign('budget_chart_id_from')->references('id')->on('accounting_budget_chart');
            $table->foreign('budget_chart_id_to')->references('id')->on('accounting_budget_chart');
        });
    }


    public function down()
    {
        Schema::table('accounting_transfer_budget', function (Blueprint $table) {
            $table->dropForeign('accounting_transfer_budget_division_id_from_foreign');
            $table->dropForeign('accounting_transfer_budget_division_id_to_foreign');
            $table->dropForeign('accounting_transfer_budget_budget_source_id_from_foreign');
            $table->dropForeign('accounting_transfer_budget_budget_source_id_to_foreign');
            $table->dropForeign('accounting_transfer_budget_budget_chart_id_from_foreign');
            $table->dropForeign('accounting_transfer_budget_budget_chart_id_to_foreign');
            $table->renameColumn('budget_plan_id_to', 'budget_plan_id')->after('id');

            $table->dropColumn([
                'division_id_from',
                'division_id_to',
                'budget_source_id_from',
                'budget_source_id_to',
                'budget_chart_id_from',
                'budget_chart_id_to',
                'year'
            ]);
        });
    }
}
