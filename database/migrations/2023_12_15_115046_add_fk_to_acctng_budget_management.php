<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkToAcctngBudgetManagement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acctng_budget_management', function (Blueprint $table) {
            $table->foreign('budget_category_id')->references('id')->on('acctng_budget_category_reference');
            $table->foreign('budget_coa_id')->references('id')->on('acctng_budget_coa_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acctng_budget_management', function (Blueprint $table) {
            //
        });
    }
}
