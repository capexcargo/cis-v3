<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHolidayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('hrim_holiday_type', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });


        Schema::create('hrim_holiday', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->date('date')->nullable();
            $table->unsignedBigInteger('branch_id');
            $table->unsignedBigInteger('type_id');
            $table->timestamps();


            $table->foreign('branch_id')->references('id')->on('branch_reference');
            $table->foreign('type_id')->references('id')->on('hrim_holiday_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrim_holiday_type');
        Schema::dropIfExists('hrim_holiday');
    }
}
