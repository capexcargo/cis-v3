<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcctngPurchasingServiceReqDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acctng_purchasing_service_req_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('reference_no_id');
            $table->date('expected_start_date');
            $table->date('expected_end_date');
            $table->unsignedBigInteger('service_category_id');
            $table->unsignedBigInteger('service_description_id');
            $table->integer('number_of_workers');
            $table->integer('man_hours');
            $table->float('estimated_rate');
            $table->integer('preferred_worker')->nullable();
            $table->integer('purpose')->nullable();
            $table->unsignedBigInteger('location_id');
            $table->longText('comments')->nullable();
            $table->unsignedBigInteger('budget_id');
            $table->unsignedBigInteger('approver1_id');
            $table->unsignedBigInteger('approver2_id');
            $table->unsignedBigInteger('approver3_id');
            $table->integer('approver1_status');
            $table->integer('approver2_status');
            $table->integer('approver3_status');
            $table->dateTime('approver1_date');
            $table->dateTime('approver2_date');
            $table->dateTime('approver3_date');
            $table->longText('approver1_remarks');
            $table->longText('approver2_remarks');
            $table->longText('approver3_remarks');

            $table->timestamps();

            $table->foreign('reference_no_id')->references('id')->on('acctng_purchasing');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acctng_purchasing_service_req_details');
    }
}
