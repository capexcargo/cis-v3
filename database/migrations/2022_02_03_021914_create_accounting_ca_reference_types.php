<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountingCaReferenceTypes extends Migration
{
    public function up()
    {
        Schema::create('accounting_ca_reference_types', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::table('accounting_request_for_payment', function (Blueprint $table) {
            $table->string('ca_no')->after('canvassing_supplier_id')->nullable();
            $table->unsignedBigInteger('ca_reference_type_id')->after('ca_no')->nullable();

            $table->foreign('ca_reference_type_id')->references('id')->on('accounting_ca_reference_types');
        });

        Schema::table('accounting_rfp_details', function (Blueprint $table) {
            $table->string('ca_reference_no')->after('reference_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('accounting_ca_reference_types');
        Schema::table('accounting_request_for_payment', function (Blueprint $table) {
            $table->dropForeign('accounting_ca_reference_types_ca_reference_type_id_foreign');

            $table->dropColumn('ca_no');
            $table->dropColumn('ca_reference_type_id');
        });

        Schema::table('accounting_rfp_details', function (Blueprint $table) {
            $table->dropColumn('ca_reference_no');
        });
    }
}
