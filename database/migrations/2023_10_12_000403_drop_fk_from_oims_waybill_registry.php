<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropFkFromOimsWaybillRegistry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('oims_waybill_registry', function (Blueprint $table) {
            $table->dropForeign('oims_waybill_registry_issued_to_om_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('oims_waybill_registry', function (Blueprint $table) {
            //
        });
    }
}
