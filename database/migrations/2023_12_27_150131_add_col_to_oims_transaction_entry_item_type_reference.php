<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColToOimsTransactionEntryItemTypeReference extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('oims_transaction_entry_item_type_reference', function (Blueprint $table) {
            $table->unsignedBigInteger('transport_mode')->after('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('oims_transaction_entry_item_type_reference', function (Blueprint $table) {
            //
        });
    }
}
