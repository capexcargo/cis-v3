<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrimDisciplinaryAttachments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrim_disciplinary_attachments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('disciplinary_id');
            $table->string('path');
            $table->string('name');
            $table->string('extension');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('disciplinary_id')->references('id')->on('hrim_disciplinary_history');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrim_disciplinary_attachments');
    }
}
