<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrimEmployeeRequisitionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrim_employment_category_type', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->unsignedBigInteger('employment_category_id');

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('employment_category_id')->references('id')->on('hrim_employment_category');
        });

        Schema::create('hrim_requisition_reason', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('hrim_employee_requisition', function (Blueprint $table) {
            $table->id();
            $table->string('erf_reference_no');
            $table->unsignedBigInteger('branch_id');
            $table->unsignedBigInteger('division_id');
            $table->unsignedBigInteger('position_id');
            $table->unsignedBigInteger('job_level_id');
            $table->unsignedBigInteger('employment_category_id');
            $table->unsignedBigInteger('employment_type_id')->nullable();
            $table->string('project_name')->nullable();
            $table->date('duration_from')->nullable();
            $table->time('duration_to')->nullable();
            $table->unsignedBigInteger('requisition_reason_id');
            $table->date('target_hire');
            $table->unsignedBigInteger('1st_approver')->nullable();
            $table->unsignedBigInteger('2nd_approver')->nullable();
            $table->unsignedBigInteger('1st_status')->nullable();
            $table->unsignedBigInteger('2nd_status')->nullable();
            $table->unsignedBigInteger('final_status')->nullable();
            $table->date('1st_approval_date')->nullable();
            $table->date('2nd_approval_date')->nullable();
            $table->unsignedBigInteger('created_by');

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('branch_id')->references('id')->on('branch_reference');
            $table->foreign('division_id')->references('id')->on('division');
            $table->foreign('position_id')->references('id')->on('hrim_positions');
            $table->foreign('job_level_id')->references('id')->on('hrim_job_levels');
            $table->foreign('employment_category_id')->references('id')->on('hrim_employment_category');
            $table->foreign('employment_type_id')->references('id')->on('hrim_employment_category_type');
            $table->foreign('requisition_reason_id')->references('id')->on('hrim_requisition_reason');
            $table->foreign('created_by')->references('id')->on('users');
        });

        Schema::create('hrim_requisition_attachment', function (Blueprint $table) {
            $table->id();
            $table->string('path');
            $table->string('name');
            $table->string('extension');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrim_requisition_attachment');
        Schema::dropIfExists('hrim_employee_requisition');
        Schema::dropIfExists('hrim_requisition_reason');
        Schema::dropIfExists('hrim_employment_category_type');
    }
}
