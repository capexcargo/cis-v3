<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkzToAcctngBudgetManagement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acctng_budget_management', function (Blueprint $table) {
            $table->foreign('budget_coa_id')->references('id')->on('acctng_chart_of_accounts_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acctng_budget_management', function (Blueprint $table) {
            //
        });
    }
}
