<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmActivities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_activities', function (Blueprint $table) {
            $table->id();
            $table->integer('employee_id');
            $table->integer('activity_type_id');
            $table->integer('account_id');
            $table->longText('description');
            $table->string('sr_no');
            $table->string('quotation_reference_no');
            $table->integer('status');
            $table->dateTime('datetime_completed');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_activities');
    }
}
