<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcctngManpowerReference extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acctng_manpower_reference', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email_address');
            $table->integer('mobile_number');
            $table->integer('tel_number');
            $table->string('agency');
            $table->string('tin');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acctng_manpower_reference');
    }
}
