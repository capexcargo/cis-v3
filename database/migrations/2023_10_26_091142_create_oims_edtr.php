<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOimsEdtr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oims_edtr', function (Blueprint $table) {
            $table->id();
            $table->string('reference_no');
            $table->unsignedBigInteger('tra_id');
            $table->date('dispatch_date');
            $table->unsignedBigInteger('branch_id');
            $table->unsignedBigInteger('status');
            $table->unsignedBigInteger('created_by');
            $table->timestamps();

            $table->foreign('tra_id')->references('id')->on('oims_team_route_assignment');
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oims_edtr');
    }
}
