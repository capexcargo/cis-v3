<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkeyFromOimsEdtr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('oims_edtr', function (Blueprint $table) {
            $table->foreign('tra_id')->references('id')->on('oims_team_route_assignment_details');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('oims_edtr', function (Blueprint $table) {
            //
        });
    }
}
