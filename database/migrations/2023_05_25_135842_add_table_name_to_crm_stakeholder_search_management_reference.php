<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTableNameToCrmStakeholderSearchManagementReference extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_stakeholder_search_management_reference', function (Blueprint $table) {
            $table->string('table_name')->after('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_stakeholder_search_management_reference', function (Blueprint $table) {
            //
        });
    }
}
