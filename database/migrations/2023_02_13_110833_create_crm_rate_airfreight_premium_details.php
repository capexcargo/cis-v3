<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmRateAirfreightPremiumDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_rate_airfreight_premium_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('rate_id');
            $table->unsignedBigInteger('origin_id');
            $table->unsignedBigInteger('destination_id');
            $table->float('amount_weight_1')->nullable();
            $table->float('amount_weight_2')->nullable();
            $table->float('amount_weight_3')->nullable();
            $table->float('amount_weight_4')->nullable();
            $table->unsignedBigInteger('service_mode_id');
            $table->unsignedBigInteger('is_primary')->nullable();

            $table->timestamps();

            $table->foreign('rate_id')->references('id')->on('crm_rate_airfreight_premium');
            $table->foreign('origin_id')->references('id')->on('branch_reference');
            $table->foreign('destination_id')->references('id')->on('branch_reference');
            $table->foreign('service_mode_id')->references('id')->on('crm_service_mode_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_rate_airfreight_premium_details');
    }
}
