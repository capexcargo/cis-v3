<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOimsReasonReference extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oims_reason_reference', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('type_of_reason');
            $table->unsignedBigInteger('application_id');
            $table->integer('status');

            $table->timestamps();

            $table->foreign('application_id')->references('id')->on('oims_reason_application_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oims_reason_reference');
    }
}
