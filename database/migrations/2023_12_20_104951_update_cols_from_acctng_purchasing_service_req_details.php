<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColsFromAcctngPurchasingServiceReqDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acctng_purchasing_service_req_details', function (Blueprint $table) {
            $table->unsignedBigInteger('budget_id')->nullable()->change();
            $table->unsignedBigInteger('approver1_id')->nullable()->change();
            $table->unsignedBigInteger('approver2_id')->nullable()->change();
            $table->unsignedBigInteger('approver3_id')->nullable()->change();
            $table->unsignedBigInteger('approver1_status')->nullable()->change();
            $table->unsignedBigInteger('approver2_status')->nullable()->change();
            $table->unsignedBigInteger('approver3_status')->nullable()->change();
            $table->unsignedBigInteger('approver1_date')->nullable()->change();
            $table->unsignedBigInteger('approver2_date')->nullable()->change();
            $table->unsignedBigInteger('approver3_date')->nullable()->change();
            $table->unsignedBigInteger('approver1_remarks')->nullable()->change();
            $table->unsignedBigInteger('approver2_remarks')->nullable()->change();
            $table->unsignedBigInteger('approver3_remarks')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acctng_purchasing_service_req_details', function (Blueprint $table) {
            //
        });
    }
}
