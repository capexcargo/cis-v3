<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColonmInAccountingLiquidationAttachments extends Migration
{
    
    public function up()
    {
        Schema::table('accounting_liquidation_attachments', function (Blueprint $table) {
            $table->unsignedBigInteger('ld_id')->after('liquidation_id');
            $table->foreign('ld_id')->references('id')->on('accounting_liquidation_details');
        });
    }

    
    public function down()
    {
        Schema::table('accounting_liquidation_attachments', function (Blueprint $table) {
            //
        });
    }
}
