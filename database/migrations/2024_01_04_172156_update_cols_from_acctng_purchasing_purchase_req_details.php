<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColsFromAcctngPurchasingPurchaseReqDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acctng_purchasing_purchase_req_details', function (Blueprint $table) {
            $table->unsignedBigInteger('approver1_id')->nullable()->change();
            $table->unsignedBigInteger('approver2_id')->nullable()->change();
            $table->unsignedBigInteger('approver3_id')->nullable()->change();
            $table->integer('approver1_status')->nullable()->change();
            $table->integer('approver2_status')->nullable()->change();
            $table->integer('approver3_status')->nullable()->change();
            $table->dateTime('approver1_date')->nullable()->change();
            $table->dateTime('approver2_date')->nullable()->change();
            $table->dateTime('approver3_date')->nullable()->change();
            $table->longText('approver1_remarks')->nullable()->change();
            $table->longText('approver2_remarks')->nullable()->change();
            $table->longText('approver3_remarks')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acctng_purchasing_purchase_req_details', function (Blueprint $table) {
            //
        });
    }
}
