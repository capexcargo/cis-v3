<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTableRfdDetails extends Migration
{
    
    public function up()
    {
        Schema::table('accounting_rfp_details', function (Blueprint $table) {
            $table->string('quantity')->nullable()->after('unit_cost');
        });
    }

    
    public function down()
    {
        Schema::table('accounting_rfp_details', function (Blueprint $table) {
            $table->dropColumn('quantity');
        });
    }
}
