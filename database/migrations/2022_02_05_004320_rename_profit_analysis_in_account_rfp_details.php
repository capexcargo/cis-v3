<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameProfitAnalysisInAccountRfpDetails extends Migration
{
    public function up()
    {
        Schema::rename('accounting_profit_analysis_type_reference', 'accounting_freight_reference_type');

        Schema::table('accounting_rfp_details', function (Blueprint $table) {
            $table->renameColumn('profit_analysis_id', 'freight_reference_no')->nullable()->change();

            $table->dropForeign('accounting_rfp_details_profit_analysis_type_id_foreign');
            $table->renameColumn('profit_analysis_type_id', 'freight_reference_type_id')->nullable()->change();
        });
    }

    public function down()
    {
        Schema::table('accounting_rfp_details', function (Blueprint $table) {
            //
        });
    }
}
