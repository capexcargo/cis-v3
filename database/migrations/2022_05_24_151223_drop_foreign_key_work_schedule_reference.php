<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropForeignKeyWorkScheduleReference extends Migration
{

    public function up()
    {
        Schema::table('hrim_time_log', function (Blueprint $table) {
            $table->dropForeign('hrim_time_log_work_sched_id_foreign');
        });

        Schema::dropIfExists('hrim_work_schedule_reference');
    }


    public function down()
    {
        //
    }
}
