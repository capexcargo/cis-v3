<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumnFromCrmAudienceSegmentManagementCriteria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_audience_segment_management_criteria', function (Blueprint $table) {
            $table->integer('criteria_id')->nullable()->change();
            $table->integer('criteria_name')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_audience_segment_management_criteria', function (Blueprint $table) {
            //
        });
    }
}
