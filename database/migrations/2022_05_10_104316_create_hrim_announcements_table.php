<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrimAnnouncementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrim_announcements', function (Blueprint $table) {
            $table->id();
            $table->string('announcement_no');
            $table->date('date_posted');
            $table->string('title');
            $table->longText('details');
            $table->unsignedBigInteger('posted_by');
            $table->softDeletes();
            $table->timestamps();
        
            $table->foreign('posted_by')->references('id')->on('users');
        });

        Schema::create('hrim_announcement_attachments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('announcement_id');
            $table->string('path');
            $table->string('name');
            $table->string('extension');
            $table->softDeletes();
            $table->timestamps();


            $table->foreign('announcement_id')->references('id')->on('hrim_announcements');
        });

        Schema::create('hrim_memos', function (Blueprint $table) {
            $table->id();
            $table->string('memorandum_no');
            $table->date('date_posted');
            $table->unsignedBigInteger('attention_to');
            $table->string('title');
            $table->longText('details');
            $table->unsignedBigInteger('posted_by');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('attention_to')->references('id')->on('division');
            $table->foreign('posted_by')->references('id')->on('users');
        });

        Schema::create('hrim_memo_attachments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('memo_id');
            $table->string('path');
            $table->string('name');
            $table->string('extension');
            $table->softDeletes();
            $table->timestamps();


            $table->foreign('memo_id')->references('id')->on('hrim_memos');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrim_announcements');
        Schema::dropIfExists('hrim_memos');
    }
}
