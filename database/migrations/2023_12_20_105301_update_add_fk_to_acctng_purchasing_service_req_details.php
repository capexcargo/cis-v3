<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAddFkToAcctngPurchasingServiceReqDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acctng_purchasing_service_req_details', function (Blueprint $table) {
            $table->foreign(['service_category_id'], 'serv_cat_id')->references('id')->on('acctng_service_category_reference');
            $table->foreign(['service_description_id'], 'serv_desc_id')->references('id')->on('acctng_service_description_reference');
            $table->foreign('location_id')->references('id')->on('oims_branch_reference');
            $table->foreign('budget_id')->references('id')->on('acctng_budget_management');
            $table->foreign('approver1_id')->references('id')->on('users');
            $table->foreign('approver2_id')->references('id')->on('users');
            $table->foreign('approver3_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acctng_purchasing_service_req_details', function (Blueprint $table) {
            //
        });
    }
}
