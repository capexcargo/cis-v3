<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrim201Files extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrim_201_files_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::create('hrim_201_files', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('hrim_201_reference_id');
            $table->unsignedBigInteger('created_by');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('hrim_201_reference_id')->references('id')->on('hrim_201_files_reference');
            $table->foreign('created_by')->references('id')->on('users');
        });

        Schema::create('hrim_201_files_attachments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('hrim_201_files_id');
            $table->string('path');
            $table->string('name');
            $table->string('extension');
            $table->timestamps();

            $table->foreign('hrim_201_files_id')->references('id')->on('hrim_201_files');
        });
    }


    public function down()
    {
        Schema::dropIfExists('hrim_201_files_attachments');
        Schema::dropIfExists('hrim_201_files');
        Schema::dropIfExists('hrim_201_files_reference');
    }
}
