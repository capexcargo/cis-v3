<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToCrmBookingConsignee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_booking_consignee', function (Blueprint $table) {
            $table->unsignedBigInteger('state_id')->after('services_type_id');
            $table->unsignedBigInteger('city_id')->after('state_id');
            $table->unsignedBigInteger('barangay_id')->after('city_id');
            $table->unsignedBigInteger('postal_id')->after('barangay_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_booking_consignee', function (Blueprint $table) {
            //
        });
    }
}
