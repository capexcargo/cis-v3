<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAddColumnToCrmBookingCargoDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_booking_cargo_details', function (Blueprint $table) {
            $table->unsignedBigInteger('length')->nullable()->after('weight');
            $table->unsignedBigInteger('width')->nullable()->after('length');
            $table->unsignedBigInteger('height')->nullable()->after('width');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_booking_cargo_details', function (Blueprint $table) {
            //
        });
    }
}
