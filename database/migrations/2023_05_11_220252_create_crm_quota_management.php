<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmQuotaManagement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_quota_management_corporate', function (Blueprint $table) {
            $table->id();
            $table->string('stakeholder_category_name');
            $table->unsignedBigInteger('subcategory');
            $table->unsignedBigInteger('management_id');
            $table->integer('account_type');
            $table->integer('month');
            $table->integer('year');
            $table->float('monthly_tonnage', 11, 2);
            $table->float('distribution_pct', 11, 2);
            $table->float('monthly_cbm_sea', 11, 3);
            $table->float('monthly_kg_air', 11, 2);
            $table->float('monthly_cbm_land', 11, 3);
            $table->float('monthly_kg_int', 11, 2);
            $table->float('total_days_count', 11, 2);
            $table->float('monthly_target_amount', 11, 2);

            $table->timestamps();
        });

        Schema::create('crm_quota_management_stakeholder', function (Blueprint $table) {
            $table->id();
            $table->string('stakeholder_category_name');
            $table->unsignedBigInteger('subcategory');
            $table->unsignedBigInteger('management_id');
            $table->integer('account_type');
            $table->integer('month');
            $table->integer('year');
            $table->float('distribution_pct', 11, 2);
            $table->float('amount', 11, 2);
            $table->float('total_days_count', 11, 2)->nullable();
            $table->float('monthly_target_amount', 11, 2)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_quota_management_stakeholder');
        Schema::dropIfExists('crm_quota_management_corporate');
    }
}
