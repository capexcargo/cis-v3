<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrimPayrollStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrim_payroll_status', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user_id');
            $table->integer('month');
            $table->integer('cutoff');
            $table->integer('year');
            $table->integer('payroll_status')->nullable();
            $table->integer('initial_payslip_status')->nullable();
            $table->integer('final_payslip_status')->nullable();
            $table->integer('bank_file_status')->nullable();
            $table->date('payroll_updated_at')->nullable();
            $table->date('initial_payslip_updated_at')->nullable();
            $table->date('final_payslip_updated_at')->nullable();
            $table->date('bank_file_updated_at')->nullable();

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrim_payroll_status');
    }
}
