<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrimPayrollHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrim_payroll_history', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->integer('month');
            $table->integer('cutoff');
            $table->integer('year');
            $table->integer('employee_id');
            $table->string('employee_code')->nullable();
            $table->string('bank_account')->nullable();
            $table->date('date_hired');
            $table->integer('employment_status');
            $table->integer('job_rank');
            $table->integer('position');
            $table->integer('branch');
            $table->float('basic_pay', 11, 2)->nullable();
            $table->float('cola', 11, 2)->nullable();
            $table->float('gross_pay', 11, 2)->nullable();
            $table->float('holiday_pay', 11, 2)->nullable();
            $table->float('nd_pay', 11, 2)->nullable();
            $table->float('ot_pay', 11, 2)->nullable();
            $table->float('holiday_ot_pay', 11, 2)->nullable();
            $table->float('special_ot_pay', 11, 2)->nullable();
            $table->float('restday_ot_pay', 11, 2)->nullable();
            $table->float('restday_pay', 11, 2)->nullable();
            $table->float('leave_wt_pay', 11, 2)->nullable();
            $table->float('leave_wo_pay', 11, 2)->nullable();
            $table->float('tardiness', 11, 2)->nullable();
            $table->float('undertime', 11, 2)->nullable();
            $table->float('absence', 11, 2)->nullable();
            $table->float('government_contri_sss', 11, 2)->nullable();
            $table->float('government_contri_philhealth', 11, 2)->nullable();
            $table->float('government_contri_pagibig', 11, 2)->nullable();
            $table->float('government_loans_sss', 11, 2)->nullable();
            $table->float('government_loans_philhealth', 11, 2)->nullable();
            $table->float('government_loans_pagibig', 11, 2)->nullable();
            $table->float('tax', 11, 2)->nullable();
            $table->float('other_loans_deductions', 11, 2)->nullable();
            $table->float('admin_adjustments', 11, 2)->nullable();
            $table->float('dlb', 11, 2)->nullable();
            $table->float('net_pay', 11, 2)->nullable();
            
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrim_payroll_history');
    }
}
