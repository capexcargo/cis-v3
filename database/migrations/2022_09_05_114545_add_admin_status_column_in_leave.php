<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdminStatusColumnInLeave extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hrim_leave', function (Blueprint $table) {
            $table->boolean('admin_status')->nullable()->after('third_status');
        });

        Schema::table('hrim_overtime', function (Blueprint $table) {
            $table->boolean('admin_status')->nullable()->after('third_status');
        });

        Schema::table('hrim_tar', function (Blueprint $table) {
            $table->boolean('admin_status')->nullable()->after('third_status');
        });

        Schema::table('hrim_schedule_adjustment', function (Blueprint $table) {
            $table->boolean('admin_status')->nullable()->after('third_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leave', function (Blueprint $table) {
            //
        });
    }
}
