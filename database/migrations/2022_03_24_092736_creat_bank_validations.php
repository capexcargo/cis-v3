<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatBankValidations extends Migration
{

    public function up()
    {
        Schema::create('accounting_bank_validations', function (Blueprint $table) {
            $table->id();
            $table->date('transaction_date')->nullable();
            $table->string('check_no')->nullable();
            $table->longText('description')->nullable();
            $table->float('debit_amount', 11, 2)->nullable();
            $table->float('credit_amount', 11, 2)->nullable();
            $table->float('balance_amount', 11, 2)->nullable();
            $table->string('branch')->nullable();
            $table->string('mb_branch')->nullable();
            $table->string('account_no')->nullable();
            $table->unsignedBigInteger('payment_type_id')->nullable();
            $table->timestamps();

            $table->foreign('payment_type_id')->references('id')->on('accounting_payment_type_reference');
        });
    }


    public function down()
    {
        Schema::dropIfExists('accounting_bank_validations');
    }
}
