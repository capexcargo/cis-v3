<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToCrmBookingConsignee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_booking_consignee', function (Blueprint $table) {
            $table->unsignedBigInteger('booking_id')->after('id');
            $table->string('customer_no')->after('booking_id');
            $table->unsignedBigInteger('account_type_id')->after('customer_no');
            $table->string('company_name')->nullable()->after('account_type_id');
            $table->string('name')->after('company_name');
            $table->string('first_name')->after('name');
            $table->string('middle_name')->nullable()->after('first_name');
            $table->string('last_name')->after('middle_name');
            $table->string('mobile_number')->after('last_name');
            $table->string('email_address')->after('mobile_number');
            $table->longText('address')->after('email_address');

            $table->foreign('booking_id')->references('id')->on('crm_booking');
            $table->foreign('account_type_id')->references('id')->on('crm_account_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_booking_consignee', function (Blueprint $table) {
            //
        });
    }
}
