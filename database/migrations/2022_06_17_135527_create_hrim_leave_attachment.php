<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrimLeaveAttachment extends Migration
{

    public function up()
    {
        Schema::create('hrim_leave_attachments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('leave_id');
            $table->string('path');
            $table->string('name');
            $table->string('extension');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('leave_id')->references('id')->on('hrim_leave');
        });

        Schema::table('hrim_leave', function (Blueprint $table) {
            $table->boolean('is_with_medical_certificate')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrim_leave_attachments');
    }
}
