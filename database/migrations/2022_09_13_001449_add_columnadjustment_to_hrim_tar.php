<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnadjustmentToHrimTar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hrim_tar', function (Blueprint $table) {
            $table->time('current_time_in')->nullable()->after('actual_time_out');
            $table->time('current_time_out')->nullable()->after('current_time_in');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hrim_tar', function (Blueprint $table) {
            //
        });
    }
}
