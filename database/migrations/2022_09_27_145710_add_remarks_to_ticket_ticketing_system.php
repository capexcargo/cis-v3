<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRemarksToTicketTicketingSystem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ticket_ticketing_system', function (Blueprint $table) {
            $table->string('remarks')->nullable()->after('final_status');
            $table->date('assignment_date')->nullable()->after('remarks');
            $table->date('target_start_date')->nullable()->after('actual_end_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ticket_ticketing_system', function (Blueprint $table) {
            //
        });
    }
}
