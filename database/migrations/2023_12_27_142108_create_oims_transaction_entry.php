<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOimsTransactionEntry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oims_transaction_entry', function (Blueprint $table) {
            $table->id();
            $table->string('waybill');
            $table->unsignedBigInteger('scope_of_work');
            $table->date('transaction_date');
            $table->unsignedBigInteger('transaction_type');
            $table->unsignedBigInteger('booking_reference_id');
            $table->unsignedBigInteger('booking_type_id');
            $table->unsignedBigInteger('origin_id');
            $table->unsignedBigInteger('destination_id');
            $table->unsignedBigInteger('transhipment_id');
            $table->unsignedBigInteger('transport_mode_id');
            $table->unsignedBigInteger('shipper_id');
            $table->unsignedBigInteger('shipper_contact_person_id');
            $table->unsignedBigInteger('shipper_mobile_no_id');
            $table->unsignedBigInteger('shipper_address_id');
            $table->unsignedBigInteger('consignee__id');
            $table->unsignedBigInteger('consignee_contact_person_id');
            $table->unsignedBigInteger('consignee_mobile_no_id');
            $table->unsignedBigInteger('consignee_address_id');
            $table->integer('batch_no');
            $table->string('pickup_notation');
            $table->integer('transType');
            $table->unsignedBigInteger('item_type');
            $table->unsignedBigInteger('commodity_app_rate');
            $table->unsignedBigInteger('commodity_type');
            $table->unsignedBigInteger('type_of_goods');
            $table->unsignedBigInteger('paymode');
            $table->unsignedBigInteger('service_mode');
            $table->unsignedBigInteger('charge_to_id');
            $table->unsignedBigInteger('account_owner_id');
            $table->unsignedBigInteger('prepared_by_id');
            $table->unsignedBigInteger('qc_by_id');
            $table->unsignedBigInteger('overriden_by_id');
            $table->dateTime('qc_datetime');
            $table->dateTime('override_datetime');
            $table->unsignedBigInteger('collection_status');
            $table->unsignedBigInteger('track_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oims_transaction_entry');
    }
}
