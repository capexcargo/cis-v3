<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAddColumnToCrmBookingConsignee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_booking_consignee', function (Blueprint $table) {
            $table->float('declared_value')->after('address');
            $table->unsignedBigInteger('transposrt_mode_id')->after('declared_value');
            $table->unsignedBigInteger('service_mode_id')->after('transposrt_mode_id');
            $table->longText('description_goods')->after('service_mode_id');
            $table->unsignedBigInteger('mode_of_payment_id')->after('description_goods');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_booking_consignee', function (Blueprint $table) {
            //
        });
    }
}
