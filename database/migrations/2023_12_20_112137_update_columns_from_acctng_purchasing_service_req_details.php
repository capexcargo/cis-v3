<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumnsFromAcctngPurchasingServiceReqDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acctng_purchasing_service_req_details', function (Blueprint $table) {
            $table->unsignedBigInteger('preferred_worker')->change();
            $table->unsignedBigInteger('purpose')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acctng_purchasing_service_req_details', function (Blueprint $table) {
            //
        });
    }
}
