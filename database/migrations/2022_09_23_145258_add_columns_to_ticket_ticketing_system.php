<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToTicketTicketingSystem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ticket_ticketing_system', function (Blueprint $table) {
            $table->string('concern_reference')->nullable()->after('task_holder');
            $table->string('concern_reference_url')->nullable()->after('concern_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ticket_ticketing_system', function (Blueprint $table) {
            //
        });
    }
}
