<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOimsTransactionEntryCustomerInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oims_transaction_entry_customer_info', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('account_type');
            $table->string('account_no');
            $table->string('fullname');
            $table->string('company_name');
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('last_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oims_transaction_entry_customer_info');
    }
}
