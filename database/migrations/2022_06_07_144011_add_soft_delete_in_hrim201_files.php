<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSoftDeleteInHrim201Files extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hrim_201_files', function (Blueprint $table) {
            $table->softDeletes()->after('created_by');
        });

        Schema::table('hrim_201_files_attachments', function (Blueprint $table) {
            $table->softDeletes()->after('extension');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hrim_201_files', function (Blueprint $table) {
            //
        });
    }
}
