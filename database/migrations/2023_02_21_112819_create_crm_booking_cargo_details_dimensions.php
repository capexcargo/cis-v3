<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmBookingCargoDetailsDimensions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_booking_cargo_details_dimensions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('consignee_id')->nullable();
            $table->unsignedBigInteger('cargo_details_id')->nullable();
            $table->unsignedBigInteger('length')->nullable();
            $table->unsignedBigInteger('width')->nullable();
            $table->unsignedBigInteger('height')->nullable();

            $table->timestamps();

            $table->foreign('consignee_id')->references('id')->on('crm_booking_consignee');
            $table->foreign('cargo_details_id')->references('id')->on('crm_booking_cargo_details');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_booking_cargo_details_dimensions');
    }
}
