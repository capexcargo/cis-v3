<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForiegnKeyForEmployeeRequisitionAttachmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hrim_requisition_attachment', function (Blueprint $table) {
            $table->unsignedBigInteger('requisition_id')->after('id');;

            $table->foreign('requisition_id')->references('id')->on('hrim_employee_requisition');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrim_requisition_attachment');
    }
}
