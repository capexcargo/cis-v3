<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOimsEdtrDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oims_edtr_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('edtr_id');
            $table->unsignedBigInteger('edtr_item_id');
            $table->unsignedBigInteger('edtr_item_type');
            $table->time('estimated_time_travel');
            $table->time('estimated_time_activity');
            $table->timestamps();

            $table->foreign('edtr_id')->references('id')->on('oims_edtr');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oims_edtr_details');
    }
}
