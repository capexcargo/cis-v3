<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnInAccountingCheckVoucher extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounting_check_voucher', function (Blueprint $table) {
            $table->boolean('is_approved_1')->after('status_id')->default(0);
            $table->boolean('is_approved_2')->after('is_approved_1')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounting_check_voucher', function (Blueprint $table) {
            //
        });
    }
}
