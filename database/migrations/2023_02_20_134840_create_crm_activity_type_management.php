<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmActivityTypeManagement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_activity_type_management', function (Blueprint $table) {
            $table->id();
            $table->string('activity_type');
            $table->unsignedBigInteger('pickup_execution_time_id')->nullable();
            $table->unsignedBigInteger('delivery_execution_time_id')->nullable();

            $table->timestamps();

            $table->foreign('pickup_execution_time_id')->references('id')->on('crm_activity_type_pickup_reference');
            $table->foreign('delivery_execution_time_id')->references('id')->on('crm_activity_type_delivery_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_activity_type_management');
    }
}
