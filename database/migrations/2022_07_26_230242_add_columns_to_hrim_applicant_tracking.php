<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToHrimApplicantTracking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hrim_applicant_tracking', function (Blueprint $table) {
            $table->longText('hr_notes')->nullable()->after('date_applied');
            $table->longText('hiring_manager_notes')->nullable()->after('hr_notes');
            $table->longText('gm_notes')->nullable()->after('hiring_manager_notes');
            $table->longText('remarks')->nullable()->after('division_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hrim_applicant_tracking', function (Blueprint $table) {
            //
        });
    }
}
