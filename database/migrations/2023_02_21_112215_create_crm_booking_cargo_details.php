<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmBookingCargoDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_booking_cargo_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('consignee_id');
            $table->unsignedBigInteger('quantity')->nullable();
            $table->unsignedBigInteger('weight')->nullable();
            $table->float('declared_value');
            $table->unsignedBigInteger('transposrt_mode_id');
            $table->unsignedBigInteger('service_mode_id');
            $table->longText('description_goods');
            $table->unsignedBigInteger('mode_of_payment_id');

            $table->timestamps();
            
            $table->foreign('consignee_id')->references('id')->on('crm_booking_consignee');
            $table->foreign('transposrt_mode_id')->references('id')->on('crm_rate_transport_mode_reference');
            $table->foreign('service_mode_id')->references('id')->on('crm_service_mode_reference');
            $table->foreign('mode_of_payment_id')->references('id')->on('crm_mode_of_payment_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_booking_cargo_details');
    }
}
