<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToAccountingCvsSupplierItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounting_cvs_supplier_item', function (Blueprint $table) {
            $table->string('waybill_from')->nullable();
            $table->string('waybill_to')->nullable();
            $table->boolean('waybill_identify')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounting_cvs_supplier_item', function (Blueprint $table) {
            //
        });
    }
}
