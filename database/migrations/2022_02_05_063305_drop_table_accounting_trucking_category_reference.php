<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropTableAccountingTruckingCategoryReference extends Migration
{
    
    public function up()
    {
        Schema::dropIfExists('accounting_trucking_category_reference');
    }

    public function down()
    {
        //
    }
}
