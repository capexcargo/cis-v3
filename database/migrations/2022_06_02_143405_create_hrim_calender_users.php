<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrimCalenderUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hrim_calendar', function (Blueprint $table) {
            $table->dropForeign('hrim_calendar_user_id_foreign');
        });

        Schema::table('hrim_calendar', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });

        Schema::create('hrim_calender_users', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('calendar_id');
            $table->unsignedBigInteger('user_id');
            $table->timestamps();

            $table->foreign('calendar_id')->references('id')->on('hrim_calendar');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrim_calender_users');
    }
}
