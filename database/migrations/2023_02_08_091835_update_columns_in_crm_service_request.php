<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumnsInCrmServiceRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_service_request', function (Blueprint $table) {
            $table->unsignedBigInteger('service_request_type_id')->nullable()->change();
            $table->unsignedBigInteger('severity_id')->nullable()->change();
            $table->unsignedBigInteger('assigned_to_id')->nullable()->change();
            $table->unsignedBigInteger('channel_id')->nullable()->change();
            $table->unsignedBigInteger('status_id')->nullable()->change();
            $table->unsignedBigInteger('life_stage_id')->nullable()->change();
            $table->longText('description')->nullable()->change();
            $table->unsignedBigInteger('created_by')->nullable()->change();
            $table->unsignedBigInteger('updated_by')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_service_request', function (Blueprint $table) {
            //
        });
    }
}
