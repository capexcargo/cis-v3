<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOimsTeamRouteAssignmentDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oims_team_route_assignment_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('team_route_id');
            $table->unsignedBigInteger('team_id');
            $table->unsignedBigInteger('plate_no_id');
            $table->unsignedBigInteger('route_category_id');
            $table->unsignedBigInteger('driver_id');
            $table->unsignedBigInteger('checker1_id');
            $table->unsignedBigInteger('checker2_id');
            $table->longText('remarks');

            $table->timestamps();

            $table->foreign('team_route_id')->references('id')->on('oims_team_route_assignment');
            $table->foreign('team_id')->references('id')->on('oims_team_reference');
            $table->foreign('plate_no_id')->references('id')->on('oims_vehicle');
            $table->foreign('route_category_id')->references('id')->on('oims_route_category_reference');
            $table->foreign('driver_id')->references('id')->on('users');
            $table->foreign('checker1_id')->references('id')->on('users');
            $table->foreign('checker2_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oims_team_route_assignment_details');
    }
}
