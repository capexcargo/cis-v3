<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatecolTableBudgetPlanAcctng3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acctng_budget_management', function (Blueprint $table) {
            $table->float('jan', 12, 2)->change();
            $table->float('feb', 12, 2)->change();
            $table->float('mar', 12, 2)->change();
            $table->float('apr', 12, 2)->change();
            $table->float('may', 12, 2)->change();
            $table->float('jun', 12, 2)->change();
            $table->float('jul', 12, 2)->change();
            $table->float('aug', 12, 2)->change();
            $table->float('sep', 12, 2)->change();
            $table->float('oct', 12, 2)->change();
            $table->float('nov', 12, 2)->change();
            $table->float('dece', 12, 2)->change();
            $table->float('total_amount', 13, 2)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
