<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsFromCrmBooking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_booking', function (Blueprint $table) {
            $table->time('time_slot_from')->nullable()->after('time_slot_id');
            $table->time('time_slot_to')->nullable()->after('time_slot_from');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_booking', function (Blueprint $table) {
            //
        });
    }
}
