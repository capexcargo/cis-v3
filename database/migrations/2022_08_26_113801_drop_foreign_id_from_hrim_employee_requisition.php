<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropForeignIdFromHrimEmployeeRequisition extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hrim_employee_requisition', function (Blueprint $table) {
            $table->dropForeign('hrim_employee_requisition_employment_category_id_foreign');
            $table->dropForeign('hrim_employee_requisition_employment_type_id_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hrim_employee_requisition', function (Blueprint $table) {
            //
        });
    }
}
