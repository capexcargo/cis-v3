<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveColumnRequestForPayment extends Migration
{
    
    public function up()
    {
        Schema::table('accounting_request_for_payment', function (Blueprint $table) {
            $table->dropForeign('accounting_request_for_payment_category_id_foreign');

            $table->dropColumn('category_id');
        });

        Schema::dropIfExists('accounting_rfp_category_reference');
    }

    public function down()
    {
        Schema::create('accounting_rfp_category_reference', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('type_id')->nullable();
            $table->string('code')->nullable();
            $table->string('display')->nullable();
            $table->boolean('is_visible')->default(1);
            $table->timestamps();

            $table->foreign('type_id')->references('id')->on('accounting_rfp_type_reference');
        });

        Schema::table('accounting_request_for_payment', function (Blueprint $table) {
            $table->unsignedBigInteger('category_id')->after('priority_id')->nullable();

            $table->foreign('category_id')->references('id')->on('accounting_rfp_category_reference');
        });

        
    }
}
