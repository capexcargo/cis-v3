<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsFromOimsTeamRouteAssignmentDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('oims_team_route_assignment_details', function (Blueprint $table) {
            $table->unsignedBigInteger('vehicle_type')->after('plate_no_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('oims_team_route_assignment_details', function (Blueprint $table) {
            //
        });
    }
}
