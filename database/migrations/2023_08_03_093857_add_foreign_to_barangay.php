<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignToBarangay extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('barangay', function (Blueprint $table) {
            $table->foreign('city_id')->references('id')->on('city');
            $table->foreign('zipcode_id')->references('id')->on('oims_zipcode_reference');
            $table->foreign('region_id')->references('id')->on('region');
            $table->foreign('island_group_id')->references('id')->on('island_group');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barangay', function (Blueprint $table) {
            //
        });
    }
}
