<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcctngServiceCategoryReference extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acctng_service_category_reference', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->boolean('is_recurring');
            $table->unsignedBigInteger('budget_source_id');
            $table->unsignedBigInteger('opex_category_id');
            $table->unsignedBigInteger('chart_of_acccounts_id');
            $table->unsignedBigInteger('sub_accounts_id');
            $table->integer('status');
            $table->timestamps();

            $table->foreign('budget_source_id')->references('id')->on('acctng_budget_source_reference');
            $table->foreign('opex_category_id')->references('id')->on('acctng_opex_category_reference');
            $table->foreign('chart_of_acccounts_id')->references('id')->on('acctng_chart_of_accounts_reference');
            $table->foreign('sub_accounts_id')->references('id')->on('acctng_sub_accounts_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acctng_service_category_reference');
    }
}
