<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddClmnToOimsWaybillRegistry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('oims_waybill_registry', function (Blueprint $table) {
            $table->date('closed_date')->nullable()->after('pad_no');
            $table->date('issuance_date')->nullable()->after('closed_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('oims_waybill_registry', function (Blueprint $table) {
            //
        });
    }
}
