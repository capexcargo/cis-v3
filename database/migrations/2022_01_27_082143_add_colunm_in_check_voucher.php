<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColunmInCheckVoucher extends Migration
{

    public function up()
    {
        Schema::table('accounting_check_voucher', function (Blueprint $table) {
            $table->unsignedBigInteger('request_for_payment_id')->after('id')->nullable();

            $table->foreign('request_for_payment_id')->references('id')->on('accounting_request_for_payment');
        });

        Schema::create('accounting_budget_availment', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('request_for_payment_id');
            $table->unsignedBigInteger('division_id');
            $table->unsignedBigInteger('budget_source_id');
            $table->unsignedBigInteger('budget_chart_id');
            $table->unsignedBigInteger('budget_plan_id');
            $table->float('amount', 11, 2);
            $table->string('month');
            $table->integer('year');
            $table->timestamps();

            $table->foreign('request_for_payment_id')->references('id')->on('accounting_request_for_payment');
            $table->foreign('division_id')->references('id')->on('division');
            $table->foreign('budget_source_id')->references('id')->on('accounting_budget_source');
            $table->foreign('budget_chart_id')->references('id')->on('accounting_budget_chart');
            $table->foreign('budget_plan_id')->references('id')->on('accounting_budget_plan');
        });
    }


    public function down()
    {
        Schema::table('accounting_check_voucher', function (Blueprint $table) {
            $table->dropForeign('accounting_request_for_payment_request_for_payment_id_foreign');

            $table->dropColumn('request_for_payment_id');
        });

        Schema::dropIfExists('accounting_budget_availment');
    }
}
