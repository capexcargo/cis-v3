<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColsFromOimsTransactionEntry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('oims_transaction_entry', function (Blueprint $table) {
            $table->unsignedBigInteger('booking_type_id')->nullable()->change();
            $table->unsignedBigInteger('transhipment_id')->nullable()->change();
            $table->unsignedBigInteger('type_of_goods')->nullable()->change();
            $table->unsignedBigInteger('charge_to_id')->nullable()->change();
            $table->unsignedBigInteger('account_owner_id')->nullable()->change();
            $table->unsignedBigInteger('prepared_by_id')->nullable()->change();
            $table->unsignedBigInteger('qc_by_id')->nullable()->change();
            $table->unsignedBigInteger('overriden_by_id')->nullable()->change();
            $table->dateTime('qc_datetime')->nullable()->change();
            $table->dateTime('override_datetime')->nullable()->change();
            $table->unsignedBigInteger('collection_status')->nullable()->change();
            $table->unsignedBigInteger('track_status')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('oims_transaction_entry', function (Blueprint $table) {
            //
        });
    }
}
