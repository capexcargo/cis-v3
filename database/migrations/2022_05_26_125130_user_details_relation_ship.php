<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserDetailsRelationShip extends Migration
{

    public function up()
    {
        Schema::create('hrim_gender', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::create('hrim_government_information', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('sss_no')->nullable();
            $table->string('pagibig_no')->nullable();
            $table->string('philhealth_no')->nullable();
            $table->string('tin_no')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::create('hrim_character_reference', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('name');
            $table->string('position');
            $table->string('company');
            $table->string('mobile_number');
            $table->string('telephone_number');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::create('hrim_earnings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->float('basic_pay', 11, 2)->nullable();
            $table->float('cola', 11, 2)->nullable();
            $table->float('gross_pay', 11, 2)->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::create('hrim_employment_category', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::create('hrim_employment_status', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::create('hrim_employment_record', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('employer');
            $table->string('position');
            $table->integer('inclusive_year_from');
            $table->integer('inclusive_year_to');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::create('hrim_family_information_sibling', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('hrim_family_information_children', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('hrim_family_information', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('fathers_name');
            $table->string('mothers_maiden_name');
            $table->unsignedBigInteger('siblings_id');
            $table->string('spouse');
            $table->unsignedBigInteger('children_id');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('siblings_id')->references('id')->on('hrim_family_information_sibling');
            $table->foreign('children_id')->references('id')->on('hrim_family_information_children');
        });



        Schema::create('hrim_ecd_relationship_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::create('hrim_emergency_contact_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('name');
            $table->unsignedBigInteger('ecd_relationship_id');
            $table->string('address');
            $table->string('mobile_number');
            $table->string('telephone_number');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('ecd_relationship_id')->references('id')->on('hrim_ecd_relationship_reference');
        });
    }


    public function down()
    {
        Schema::dropIfExists('hrim_emergency_contact_details');
        Schema::dropIfExists('hrim_ecd_relationship_reference');

        Schema::dropIfExists('hrim_family_information');
        Schema::dropIfExists('hrim_family_information_children');
        Schema::dropIfExists('hrim_family_information_sibling');

        Schema::dropIfExists('hrim_employment_record');
        Schema::dropIfExists('hrim_employment_status');
        Schema::dropIfExists('hrim_employment_category');
        Schema::dropIfExists('hrim_earnings');
        Schema::dropIfExists('hrim_character_reference');
        Schema::dropIfExists('hrim_government_information');
        Schema::dropIfExists('hrim_gender');
    }
}
