<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSoftDeleteInAccountingRequestForPayment extends Migration
{

    public function up()
    {
        Schema::table('accounting_request_for_payment', function (Blueprint $table) {
            $table->softDeletes()->after('status_id');
        });

        Schema::table('accounting_rfp_details', function (Blueprint $table) {
            $table->softDeletes()->after('invoice_amount');
        });
    }


    public function down()
    {
        Schema::table('accounting_request_for_payment', function (Blueprint $table) {
            //
        });
    }
}
