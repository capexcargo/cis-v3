<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropForeignKeysInAccountingLiquidation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounting_liquidation', function (Blueprint $table) {
            $table->dropForeign('accounting_liquidation_request_for_payment_id_foreign');
            $table->dropForeign('accounting_liquidation_opex_type_id_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounting_liquidation', function (Blueprint $table) {
            //
        });
    }
}
