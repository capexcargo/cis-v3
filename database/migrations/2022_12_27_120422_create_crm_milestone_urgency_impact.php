<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmMilestoneUrgencyImpact extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_milestone_urgency_impact', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('severity_id');
            $table->unsignedBigInteger('high_priority_level');
            $table->unsignedBigInteger('medium_priority_level');
            $table->unsignedBigInteger('low_priority_level');
            $table->integer('status');

            $table->timestamps();
                
            $table->foreign('severity_id')->references('id')->on('crm_milestone_resolution');
            $table->foreign('high_priority_level')->references('id')->on('crm_milestone_resolution');
            $table->foreign('medium_priority_level')->references('id')->on('crm_milestone_resolution');
            $table->foreign('low_priority_level')->references('id')->on('crm_milestone_resolution');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_milestone_urgency_impact');
    }
}
