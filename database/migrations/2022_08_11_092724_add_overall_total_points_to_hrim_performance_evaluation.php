<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOverallTotalPointsToHrimPerformanceEvaluation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hrim_performance_evaluation', function (Blueprint $table) {
            $table->integer('overall_total_score')->nullable()->after('leadership_total_score');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hrim_performance_evaluation', function (Blueprint $table) {
            //
        });
    }
}
