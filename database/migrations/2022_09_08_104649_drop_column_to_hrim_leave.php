<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColumnToHrimLeave extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hrim_leave', function (Blueprint $table) {
            $table->dropColumn('overrule_approver');
            $table->dropColumn('overrule_approval_date');
            $table->dropColumn('overrule_approval_remarks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hrim_leave', function (Blueprint $table) {
            //
        });
    }
}
