<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Supplier extends Migration
{
    public function up()
    {
        Schema::create('accounting_supplier_industry_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::create('accounting_supplier_type_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::create('accounting_supplier', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->string('company')->unique();
            $table->unsignedBigInteger('industry_id')->nullable();
            $table->unsignedBigInteger('type_id')->nullable();
            $table->string('first_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->nullable();
            $table->string('mobile_number')->nullable();
            $table->string('telephone_number')->nullable();
            $table->longText('address')->nullable();
            $table->string('contact_person')->nullable();
            $table->string('terms')->nullable();
            
            $table->timestamps();

            $table->foreign('industry_id')->references('id')->on('accounting_supplier_industry_reference');
            $table->foreign('type_id')->references('id')->on('accounting_supplier_type_reference');
        });

        Schema::create('accounting_supplier_item', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('supplier_id');
            $table->string('name');
            $table->timestamps();

            $table->foreign('supplier_id')->references('id')->on('accounting_supplier');
        });
    }

    public function down()
    {
        Schema::dropIfExists('accounting_supplier_industry_reference');
        Schema::dropIfExists('accounting_supplier_type_reference');
        Schema::dropIfExists('accounting_supplier');
        Schema::dropIfExists('accounting_supplier_item');
    }
}
