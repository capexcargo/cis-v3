<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrimOnboardingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrim_onboarding_status', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->unsignedBigInteger('created_by');

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('created_by')->references('id')->on('users');
        });

        Schema::create('hrim_onboarding', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('erf_reference_no_id');
            $table->string('firstname');
            $table->string('middlename');
            $table->string('lastname');
            $table->date('date_applied');
            $table->unsignedBigInteger('status_id');
            $table->date('2nd_approval_date');

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('erf_reference_no_id')->references('id')->on('hrim_employee_requisition');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrim_onboarding');
        Schema::dropIfExists('hrim_onboarding_status');
    }
}
