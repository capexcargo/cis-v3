<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmSalesStage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_sales_stage', function (Blueprint $table) {
            $table->id();
            $table->string('sales_stage');
            $table->unsignedBigInteger('opportunity_status_id')->nullable();

            $table->timestamps();

            $table->foreign('opportunity_status_id')->references('id')->on('crm_opportunity_status_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_sales_stage');
    }
}
