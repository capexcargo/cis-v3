<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTableRFO extends Migration
{

    public function up()
    {
        Schema::table('accounting_request_for_payment', function (Blueprint $table) {
            $table->date('date_of_transaction')->nullable()->after('date_needed');
        });
    }

    public function down()
    {
        Schema::table('accounting_request_for_payment', function (Blueprint $table) {
            $table->dropColumn('date_of_transaction');
        });
    }
}
