<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnDateOfTransactionToInAccountingRequestForPayment extends Migration
{

    public function up()
    {
        Schema::table('accounting_request_for_payment', function (Blueprint $table) {
            $table->string('parent_reference_no')->nullable()->after('id');
            $table->renameColumn('date_of_transaction', 'date_of_transaction_from')->nullable()->change();
            $table->date('date_of_transaction_to')->nullable()->after('date_of_transaction');
        });
    }


    public function down()
    {
        Schema::table('accounting_request_for_payment', function (Blueprint $table) {
            //
        });
    }
}
