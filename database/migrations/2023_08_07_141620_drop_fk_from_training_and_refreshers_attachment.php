<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropFkFromTrainingAndRefreshersAttachment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('training_and_refreshers_attachment', function (Blueprint $table) {
            $table->dropForeign('training_and_refreshers_attachment_train_and_ref_id_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('training_and_refreshers_attachment', function (Blueprint $table) {
            //
        });
    }
}
