<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RolesManagement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_status_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->string('display')->nullable();
            $table->string('text_color')->nullable();
            $table->string('bg_color')->nullable();
            $table->unsignedBigInteger('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::create('account_type_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->unsignedBigInteger('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::create('level_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->unsignedBigInteger('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::create('account_type_roles_reference', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('account_type_id');
            $table->unsignedBigInteger('level_id');
            $table->string('code');
            $table->string('display');
            $table->unsignedBigInteger('is_visible')->default(1);
            $table->timestamps();

            $table->foreign('account_type_id')->references('id')->on('account_type_reference');
            // ->onDelete('cascade');
            $table->foreign('level_id')->references('id')->on('level_reference');
        });

        Schema::create('roles_parent_access_reference', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('account_type_id');
            $table->string('code');
            $table->string('display');
            $table->unsignedBigInteger('is_visible')->default(1);
            $table->timestamps();

            $table->foreign('account_type_id')->references('id')->on('account_type_reference');
        });

        Schema::create('roles_child_access_reference', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('roles_parent_access_id');
            $table->string('code');
            $table->string('display');
            $table->unsignedBigInteger('is_visible')->default(1);
            $table->timestamps();

            $table->foreign('roles_parent_access_id')->references('id')->on('roles_parent_access_reference');
        });

        Schema::create('account_type_roles_access', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('role_id');
            $table->unsignedBigInteger('access_id');
            $table->timestamps();

            $table->foreign('role_id')->references('id')->on('account_type_roles_reference');
            $table->foreign('access_id')->references('id')->on('roles_child_access_reference');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('role_id')->after('id');
            $table->unsignedBigInteger('account_type_id')->after('role_id');
            $table->unsignedBigInteger('level_id')->after('account_type_id');
            $table->unsignedBigInteger('status_id')->after('level_id')->default(1);
            $table->string('email_verification_code')->after('email_verified_at')->nullable();
            $table->integer('attempt')->after('password')->default(0);

            $table->foreign('role_id')->references('id')->on('account_type_roles_reference');
            $table->foreign('account_type_id')->references('id')->on('account_type_reference');
            $table->foreign('level_id')->references('id')->on('level_reference');
            $table->foreign('status_id')->references('id')->on('account_status_reference');
        });

        Schema::create('suffix_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->string('display')->nullable();
            $table->unsignedBigInteger('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::create('user_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('employee_number')->nullable();
            $table->string('first_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->unsignedBigInteger('suffix_id')->nullable();
            $table->string('email')->unique();
            $table->string('age')->nullable();
            $table->string('birth_date')->nullable();
            $table->string('mobile_number')->nullable();
            $table->string('telephone_number')->nullable();
            $table->string('country')->nullable();
            $table->string('region')->nullable();
            $table->string('province')->nullable();
            $table->string('city')->nullable();
            $table->string('barangay')->nullable();
            $table->string('street')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('suffix_id')->references('id')->on('suffix_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_status_reference');
        Schema::dropIfExists('account_type_reference');
        Schema::dropIfExists('level_reference');
        Schema::dropIfExists('account_type_roles_reference');
        Schema::dropIfExists('roles_parent_access_reference');
        Schema::dropIfExists('roles_child_access_reference');
        Schema::dropIfExists('account_type_roles_access');
        Schema::dropIfExists('suffix_reference');
        Schema::dropIfExists('user_details');
    }
}
