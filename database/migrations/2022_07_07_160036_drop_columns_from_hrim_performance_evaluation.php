<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColumnsFromHrimPerformanceEvaluation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hrim_performance_evaluation', function (Blueprint $table) {
            $table->dropColumn('core_values_id');
            $table->dropColumn('leadership_comp_id');
            $table->dropColumn('first_points');
            $table->dropColumn('second_points');
            $table->dropColumn('third_points');
            $table->dropColumn('first_status');
            $table->dropColumn('second_status');
            $table->dropColumn('third_status');
            $table->dropColumn('first_evaluator');
            $table->dropColumn('second_evaluator');
            $table->dropColumn('third_evaluator');
            $table->dropColumn('first_eval_date');
            $table->dropColumn('second_eval_date');
            $table->dropColumn('third_eval_date');
            $table->dropColumn('first_remarks');
            $table->dropColumn('second_remarks');
            $table->dropColumn('third_remarks');
            $table->dropColumn('user_id_remarks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hrim_performance_evaluation', function (Blueprint $table) {
            //
        });
    }
}
