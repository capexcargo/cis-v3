<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOimsWaybillRegistry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oims_waybill_registry', function (Blueprint $table) {
            $table->id();
            $table->string('waybill_start');
            $table->string('waybill_end');
            $table->string('po_reference');
            $table->unsignedBigInteger('branch_id');
            $table->unsignedBigInteger('status');
            $table->unsignedBigInteger('issued_to');
            $table->unsignedBigInteger('issued_to_emp_id')->nullable();
            $table->unsignedBigInteger('issued_to_customer_id')->nullable();
            $table->unsignedBigInteger('issued_to_agent_id')->nullable();
            $table->unsignedBigInteger('issued_to_om')->nullable();
            $table->unsignedBigInteger('issuance_status');
            $table->unsignedBigInteger('created_by');
            $table->timestamps();

            $table->foreign('branch_id')->references('id')->on('oims_branch_reference');
            $table->foreign('issued_to_emp_id')->references('id')->on('users');
            $table->foreign('issued_to_customer_id')->references('id')->on('crm_customer_information');
            $table->foreign('issued_to_agent_id')->references('id')->on('oims_agent');
            $table->foreign('issued_to_om')->references('id')->on('users');
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oims_waybill_registry');
    }
}
