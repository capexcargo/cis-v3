<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOimsTransactionEntryItemBox extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oims_transaction_entry_item_box', function (Blueprint $table) {
            $table->id();
            $table->string('waybill_id');
            $table->unsignedBigInteger('qty');
            $table->integer('box_size');
            $table->integer('weight');
            $table->float('amount');
            $table->integer('rate_id_applied');
            $table->integer('cargotype');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oims_transaction_entry_item_box');
    }
}
