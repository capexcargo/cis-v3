<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateOvertimeTable extends Migration
{
    public function up()
    {
        Schema::table('hrim_overtime', function (Blueprint $table) {
            $table->float('overtime_rendered', 11, 2)->change();
            $table->float('overtime_request', 11, 2)->change();
            $table->float('overtime_approved', 11, 2)->change();
        });
    }

    public function down()
    {
        //
    }
}
