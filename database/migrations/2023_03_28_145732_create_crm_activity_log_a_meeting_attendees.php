<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmActivityLogAMeetingAttendees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_activity_log_a_meeting_attendees', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('attendees_id');
            $table->unsignedBigInteger('meeting_id');

            $table->timestamps();

            $table->foreign('attendees_id')->references('id')->on('users');
            $table->foreign('meeting_id')->references('id')->on('crm_activity_log_a_meeting');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_activity_log_a_meeting_attendees');
    }
}
