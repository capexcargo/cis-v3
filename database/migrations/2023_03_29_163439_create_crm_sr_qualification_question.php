<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmSrQualificationQuestion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_sr_qualification_question', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('lead_id');
            $table->string('sr_no');
            $table->unsignedBigInteger('question_id');
            $table->integer('response');
            $table->longText('remarks');

            $table->timestamps();

            $table->foreign('lead_id')->references('id')->on('crm_leads');
            $table->foreign('question_id')->references('id')->on('crm_qualification_question_management');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_sr_qualification_question');
    }
}
