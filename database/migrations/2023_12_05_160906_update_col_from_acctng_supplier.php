<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColFromAcctngSupplier extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acctng_supplier', function (Blueprint $table) {
            $table->string('mobile_number')->change();
            $table->string('tel_number')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acctng_supplier', function (Blueprint $table) {
            //
        });
    }
}
