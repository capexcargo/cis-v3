<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkToOimsAreaReference extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('oims_area_reference', function (Blueprint $table) {
            $table->foreign('quadrant_id')->references('id')->on('branch_references_quadrant');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('oims_area_reference', function (Blueprint $table) {
            //
        });
    }
}
