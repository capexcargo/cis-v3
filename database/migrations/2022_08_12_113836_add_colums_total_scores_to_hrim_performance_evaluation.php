<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumsTotalScoresToHrimPerformanceEvaluation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hrim_performance_evaluation', function (Blueprint $table) {
            $table->float('performance_total_score', 5, 2)->nullable()->after('user_id_remarks');
            $table->float('core_value_total_score', 5, 2)->nullable()->after('performance_total_score');
            $table->float('leadership_total_score', 5, 2)->nullable()->after('core_value_total_score');
            $table->float('overall_total_score', 5, 2)->nullable()->after('leadership_total_score');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hrim_performance_evaluation', function (Blueprint $table) {
            //
        });
    }
}
