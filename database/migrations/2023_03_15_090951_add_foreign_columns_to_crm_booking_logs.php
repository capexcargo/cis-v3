<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignColumnsToCrmBookingLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_booking_logs', function (Blueprint $table) {
            $table->foreign('cancel_reason')->references('id')->on('crm_booking_failed_reason_reference');
            $table->foreign('resched_reason')->references('id')->on('crm_booking_failed_reason_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_booking_logs', function (Blueprint $table) {
            //
        });
    }
}
