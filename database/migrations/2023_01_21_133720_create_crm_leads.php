<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmLeads extends Migration
{
    public function up()
    {
        Schema::create('crm_leads', function (Blueprint $table) {
            $table->id();
            $table->string('sr_no');
            $table->unsignedBigInteger('account_id');
            $table->string('lead_name');
            $table->unsignedBigInteger('shipment_type_id');
            $table->unsignedBigInteger('service_requirement_id');
            $table->unsignedBigInteger('lead_status_id');
            $table->integer('qualification_score');
            $table->unsignedBigInteger('lead_qualification_id');
            $table->longText('description');
            $table->unsignedBigInteger('contact_owner_id');
            $table->unsignedBigInteger('customer_type_id');
            $table->unsignedBigInteger('channel_source_id');
            $table->integer('currency');
            $table->float('deal_size');
            $table->unsignedBigInteger('created_by');

            $table->softDeletes();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('crm_leads');
    }
}
