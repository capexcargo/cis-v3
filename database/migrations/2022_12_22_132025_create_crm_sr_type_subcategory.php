<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmSrTypeSubcategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_sr_type_subcategory', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedBigInteger('sr_type_id');

            $table->timestamps();

            $table->foreign('sr_type_id')->references('id')->on('crm_sr_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_sr_type_subcategory');
    }
}
