<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOimsServiceAreaCoverage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oims_service_area_coverage', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('island_group_id');
            $table->unsignedBigInteger('region_id');
            $table->unsignedBigInteger('state_id');
            $table->unsignedBigInteger('city_id');
            $table->unsignedBigInteger('barangay_id');
            $table->integer('zipcode');
            $table->unsignedBigInteger('port_to_cater_id');
            $table->unsignedBigInteger('serviceability_id');
            $table->float('cost_charge');
            $table->integer('status');

            $table->timestamps();

            $table->foreign('island_group_id')->references('id')->on('island_group');
            $table->foreign('region_id')->references('id')->on('region');
            $table->foreign('state_id')->references('id')->on('state');
            $table->foreign('city_id')->references('id')->on('city');
            $table->foreign('barangay_id')->references('id')->on('barangay');
            $table->foreign('port_to_cater_id')->references('id')->on('oims_branch_reference');
            $table->foreign('serviceability_id')->references('id')->on('oims_serviceability_reference');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oims_service_area_coverage');
    }
}
