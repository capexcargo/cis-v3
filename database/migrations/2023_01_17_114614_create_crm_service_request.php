<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmServiceRequest extends Migration
{
    public function up()
    {
        Schema::create('crm_service_request', function (Blueprint $table) {
            $table->id();
            $table->string('sr_no');
            $table->string('subject');
            $table->string('customer')->nullable();
            $table->unsignedBigInteger('account_id');
            $table->unsignedBigInteger('service_request_type_id');
            $table->unsignedBigInteger('severity_id');
            $table->unsignedBigInteger('sub_category_id')->nullable();
            $table->string('sub_category_reference')->nullable();
            $table->unsignedBigInteger('service_requirement_id')->nullable();
            $table->unsignedBigInteger('assigned_to_id');
            $table->unsignedBigInteger('channel_id');
            $table->unsignedBigInteger('status_id');
            $table->unsignedBigInteger('life_stage_id');
            $table->longText('description');
            $table->unsignedBigInteger('created_by');

            $table->softDeletes();

            $table->timestamps();
        });

        Schema::create('crm_sr_attachments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sr_id');
            $table->string('path');
            $table->string('name');
            $table->string('extension');
            $table->timestamps();

            // $table->foreign('sr_id')->references('id')->on('crm_service_request');
        });
    }

    public function down()
    {
        Schema::dropIfExists('crm_sr_attachments');
        Schema::dropIfExists('crm_service_request');
    }
}
