<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeams extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrim_section_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::create('hrim_teams', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('division_id');
            $table->unsignedBigInteger('position_id');
            $table->unsignedBigInteger('reports_to');
            $table->unsignedBigInteger('department_id');
            $table->unsignedBigInteger('section_id');
            $table->unsignedBigInteger('created_by');
            $table->timestamps();

            $table->foreign('division_id')->references('id')->on('division');
            $table->foreign('position_id')->references('id')->on('hrim_positions');
            $table->foreign('reports_to')->references('id')->on('hrim_positions');
            $table->foreign('department_id')->references('id')->on('hrim_departments');
            $table->foreign('section_id')->references('id')->on('hrim_section_reference');
            $table->foreign('created_by')->references('id')->on('users');
        });
    }


    public function down()
    {
        Schema::dropIfExists('hrim_teams');
        Schema::dropIfExists('hrim_section_reference');
    }
}
