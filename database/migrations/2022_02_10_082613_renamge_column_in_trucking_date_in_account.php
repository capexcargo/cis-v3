<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenamgeColumnInTruckingDateInAccount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounting_rfp_details', function (Blueprint $table) {
            $table->dropColumn('cargnage_fee');
            $table->renameColumn('trucking_category', 'trucking_type_id')->nullable()->change();
            $table->renameColumn('trucking_date', 'transaction_date')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounting_rfp_details', function (Blueprint $table) {
            //
        });
    }
}
