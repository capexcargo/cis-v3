<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropFkOpportunityIdFromCrmOpportunityAttachment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_opportunity_attachment', function (Blueprint $table) {
            $table->dropForeign('crm_opportunity_attachment_opportunity_id_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_opportunity_attachment', function (Blueprint $table) {
            //
        });
    }
}
