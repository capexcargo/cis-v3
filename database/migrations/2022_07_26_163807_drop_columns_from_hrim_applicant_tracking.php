<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColumnsFromHrimApplicantTracking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hrim_applicant_tracking', function (Blueprint $table) {
            $table->dropColumn('hr_notes');
            $table->dropColumn('hiring_manager_notes');
            $table->dropColumn('gm_notes');
            $table->dropColumn('remarks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hrim_applicant_tracking', function (Blueprint $table) {
            //
        });
    }
}
