<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RequestPayment extends Migration
{
    public function up()
    {
        Schema::create('accounting_request_for_payment', function (Blueprint $table) {
            $table->id();
            $table->string('reference_id')->nullable();
            $table->unsignedBigInteger('type_id')->nullable();
            $table->unsignedBigInteger('payee_id')->nullable();
            $table->longText('description')->nullable();
            $table->float('amount', 11, 2)->nullable();
            $table->longText('remarks')->nullable();
            $table->unsignedBigInteger('multiple_budget_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('branch_id')->nullable();
            $table->unsignedBigInteger('budget_id')->nullable();
            $table->unsignedBigInteger('priority_id')->nullable();
            $table->unsignedBigInteger('category_id')->nullable();
            $table->string('canvasser')->nullable();
            $table->date('date_needed')->nullable();
            $table->unsignedBigInteger('approver_1_id')->nullable();
            $table->unsignedBigInteger('approver_2_id')->nullable();
            $table->unsignedBigInteger('approver_3_id')->nullable();
            $table->date('approver_date_1')->nullable();
            $table->date('approver_date_2')->nullable();
            $table->date('approver_date_3')->nullable();
            $table->longText('approver_remarks_1')->nullable();
            $table->longText('approver_remarks_2')->nullable();
            $table->longText('approver_remarks_3')->nullable();
            $table->unsignedBigInteger('opex_type_id')->nullable();
            $table->boolean('final_status')->nullable()->default(0)->comment('0) Pending 1) Approved');
            $table->timestamps();

            $table->foreign('type_id')->references('id')->on('accounting_rfp_type_reference');
            $table->foreign('payee_id')->references('id')->on('accounting_supplier');
            $table->foreign('multiple_budget_id')->references('id')->on('accounting_multiple_budget_reference');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('branch_id')->references('id')->on('branch_reference');
            $table->foreign('budget_id')->references('id')->on('accounting_budget_plan');
            $table->foreign('priority_id')->references('id')->on('accounting_priority_reference');
            $table->foreign('category_id')->references('id')->on('accounting_rfp_category_reference');
            $table->foreign('approver_1_id')->references('id')->on('users');
            $table->foreign('approver_2_id')->references('id')->on('users');
            $table->foreign('approver_3_id')->references('id')->on('users');
            $table->foreign('opex_type_id')->references('id')->on('accounting_opex_type');
        });

        Schema::create('accounting_rfp_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('request_for_payment_id')->nullable();
            $table->string('reference_id')->nullable();

            $table->string('particulars')->nullable();
            $table->string('plate_no')->nullable();
            $table->float('labor_cost', 11, 2)->nullable();
            $table->float('unit_cost', 11, 2)->nullable();
            $table->float('amount', 11, 2)->nullable();

            $table->unsignedBigInteger('profit_analysis_id')->nullable();
            $table->unsignedBigInteger('profit_analysis_type_id')->nullable();
            $table->string('soa_no')->nullable();
            $table->unsignedBigInteger('trucking_category_id')->nullable();
            $table->float('trucking_amount', 11, 2)->nullable();
            $table->float('freight_amount', 11, 2)->nullable();
            $table->float('hustling_fee', 11, 2)->nullable();
            $table->float('arrastre', 11, 2)->nullable();
            $table->date('trucking_date')->nullable();
            $table->float('allowance', 11, 2)->nullable();

            $table->string('account_no')->nullable();
            $table->unsignedBigInteger('payment_type_id')->nullable();
            $table->unsignedBigInteger('terms_id')->nullable();
            $table->date('pdc_from')->nullable();
            $table->date('pdc_to')->nullable();
            $table->date('pdc_date')->nullable();
            $table->string('invoice')->nullable();
            $table->float('invoice_amount', 11, 2)->nullable();

            $table->timestamps();

            $table->foreign('request_for_payment_id')->references('id')->on('accounting_request_for_payment');
            // $table->foreign('profit_analysis_id')->references('id')->on('aaaaaaaaaaaaaaaaaaaaaaaaaaa');
            $table->foreign('profit_analysis_type_id')->references('id')->on('accounting_profit_analysis_type_reference');
            $table->foreign('trucking_category_id')->references('id')->on('accounting_trucking_category_reference');

            $table->foreign('payment_type_id')->references('id')->on('accounting_payment_type_reference');
            $table->foreign('terms_id')->references('id')->on('accounting_terms_reference');
        });
    }

    public function down()
    {
        Schema::dropIfExists('accounting_request_for_payment');
        Schema::dropIfExists('accounting_request_for_payment_details');
    }
}
