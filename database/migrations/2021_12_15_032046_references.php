<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class References extends Migration
{
    public function up()
    {
        Schema::create('accounting_multiple_budget_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->string('display')->nullable();
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::create('accounting_priority_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->string('display')->nullable();
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::create('accounting_rfp_type_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->string('display')->nullable();
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::create('accounting_rfp_category_reference', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('type_id')->nullable();
            $table->string('code')->nullable();
            $table->string('display')->nullable();
            $table->boolean('is_visible')->default(1);
            $table->timestamps();

            $table->foreign('type_id')->references('id')->on('accounting_rfp_type_reference');
        });

        Schema::create('accounting_profit_analysis_type_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->string('display')->nullable();
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::create('accounting_trucking_category_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->string('display')->nullable();
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::create('accounting_payment_type_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->string('display')->nullable();
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::create('accounting_terms_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->string('display')->nullable();
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::create('month_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->string('display')->nullable();
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::create('branch_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->string('display')->nullable();
            $table->unsignedBigInteger('area_type_id')->nullable();
            $table->unsignedBigInteger('area_level_id')->nullable();
            $table->unsignedBigInteger('area_destination_id')->nullable();
            $table->unsignedBigInteger('area_box_destination_id')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('accounting_multiple_budget_reference');
        Schema::dropIfExists('accounting_priority_reference');
        Schema::dropIfExists('accounting_request_for_payment_type_reference');
        Schema::dropIfExists('accounting_request_for_payment_category_reference');
        Schema::dropIfExists('accounting_profit_analysis_type_reference');
        Schema::dropIfExists('accounting_trucking_category_reference');
        Schema::dropIfExists('accounting_payment_type_reference');
        Schema::dropIfExists('accounting_terms_reference');
        Schema::dropIfExists('month_reference');
        Schema::dropIfExists('branch_reference');
    }
}
