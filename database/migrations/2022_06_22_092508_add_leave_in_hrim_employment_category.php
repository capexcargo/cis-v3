<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLeaveInHrimEmploymentCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hrim_employment_category', function (Blueprint $table) {
            $table->float('sick_leave', 11, 2)->after('display');
            $table->float('vacation_leave', 11, 2)->after('sick_leave');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hrim_employment_category', function (Blueprint $table) {
            $table->dropColumn(['sick_leave', 'vacation_leave']);
        });
    }
}
