<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnDateToHrimLoanDetails extends Migration
{
    public function up()
    {
        Schema::table('hrim_loan_details', function (Blueprint $table) {
            $table->date('payment_date')->after('loan_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hrim_loan_details', function (Blueprint $table) {
            //
        });
    }
}
