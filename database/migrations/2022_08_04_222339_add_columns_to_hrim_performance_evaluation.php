<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToHrimPerformanceEvaluation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hrim_performance_evaluation', function (Blueprint $table) {
            $table->integer('employee_self_points')->nullable()->after('third_points');
            $table->integer('performance_total_score')->nullable()->after('user_id_remarks');
            $table->integer('core_value_total_score')->nullable()->after('performance_total_score');
            $table->integer('leadership_total_score')->nullable()->after('core_value_total_score');
            $table->integer('employee_eval_date')->nullable()->after('third_eval_date');
            $table->longText('employee_remarks')->nullable()->after('third_remarks');
            $table->longText('critical_remarks')->nullable()->after('employee_remarks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hrim_performance_evaluation', function (Blueprint $table) {
            //
        });
    }
}
