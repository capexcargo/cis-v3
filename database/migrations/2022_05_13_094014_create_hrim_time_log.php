<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrimTimeLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrim_work_schedule', function (Blueprint $table) {
            $table->id();
            $table->string('days_from')->nullable();
            $table->string('days_to')->nullable();
            $table->time('time_from')->nullable();
            $table->time('time_to')->nullable();
            $table->time('break_time')->nullable();
            $table->unsignedBigInteger('created_by');
            $table->timestamps();

            $table->foreign('created_by')->references('id')->on('users');
        });

        Schema::create('hrim_work_schedule_reference', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('work_sched_id');
            $table->string('code')->nullable();
            $table->string('display')->nullable();
            $table->boolean('is_visible')->default(1);
            $table->unsignedBigInteger('created_by');
            $table->timestamps();

            $table->foreign('work_sched_id')->references('id')->on('hrim_work_schedule');
            $table->foreign('created_by')->references('id')->on('users');
        });

        Schema::create('hrim_date_category_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->string('display')->nullable();
            $table->boolean('is_visible')->default(1);
            $table->unsignedBigInteger('created_by');
            $table->timestamps();

            $table->foreign('created_by')->references('id')->on('users');
        });

        Schema::create('hrim_work_type_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->string('display')->nullable();
            $table->boolean('is_visible')->default(1);
            $table->unsignedBigInteger('created_by');
            $table->timestamps();

            $table->foreign('created_by')->references('id')->on('users');
        });

        Schema::create('hrim_time_log', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('work_sched_id');
            $table->unsignedBigInteger('date_category_id');
            $table->unsignedBigInteger('work_type_id');
            $table->string('time_in_img_path')->nullable();
            $table->string('time_in_img_name')->nullable();
            $table->time('time_in')->nullable();
            $table->string('time_out_img_path')->nullable();
            $table->string('time_out_img_name')->nullable();
            $table->time('time_out')->nullable();
            $table->date('date')->nullable();
            $table->float('late', 11, 2)->nullable();
            $table->float('undertime', 11, 2)->nullable();
            $table->float('computed_ot', 11, 2)->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('work_sched_id')->references('id')->on('hrim_work_schedule_reference');
            $table->foreign('date_category_id')->references('id')->on('hrim_date_category_reference');
            $table->foreign('work_type_id')->references('id')->on('hrim_work_type_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrim_time_log');
        Schema::dropIfExists('hrim_work_type_reference');
        Schema::dropIfExists('hrim_date_category_reference');
        Schema::dropIfExists('hrim_work_schedule_reference');
        Schema::dropIfExists('hrim_work_schedule');
    }
}
