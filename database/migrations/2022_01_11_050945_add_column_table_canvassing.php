<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTableCanvassing extends Migration
{

    public function up()
    {
        Schema::table('accounting_canvassing_supplier', function (Blueprint $table) {
            $table->string('po_reference_no')->nullable()->after('supplier_id');
        });

        Schema::table('accounting_cvs_supplier_item', function (Blueprint $table) {
            $table->float('unit_cost', 11, 2)->nullable()->after('supplier_item_id');
            $table->integer('quantity')->nullable()->after('unit_cost');
            $table->renameColumn('price', 'total_amount')->nullable()->after('quantity')->change();
            $table->boolean('recommended_status')->default(0)->comment('0) Not Approved 1) Approved')->after('price');
            $table->renameColumn('status', 'final_status')->after('recommended_status')->change();
        });
    }


    public function down()
    {
        Schema::table('accounting_canvassing_supplier', function (Blueprint $table) {
            $table->dropColumn('po_reference_no');
        });

        Schema::table('accounting_cvs_supplier_item', function (Blueprint $table) {
            $table->renameColumn('total_amount', 'price')->after('supplier_item_id')->change();
            $table->renameColumn('final_status', 'status')->after('price')->change();

            $table->dropColumn(['unit_cost', 'quantity', 'recommended_status']);
        });
    }
}
