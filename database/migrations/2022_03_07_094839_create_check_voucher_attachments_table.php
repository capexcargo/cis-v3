<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCheckVoucherAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounting_check_voucher_attachments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('check_voucher_id');
            $table->string('path');
            $table->string('name');
            $table->string('extension');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('check_voucher_id')->references('id')->on('accounting_check_voucher');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounting_check_voucher_attachments');
    }
}
