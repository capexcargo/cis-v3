<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsFromCrmActivityTypeManagement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_activity_type_management', function (Blueprint $table) {
            $table->unsignedInteger('status')->nullable()->after('delivery_execution_time_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_activity_type_management', function (Blueprint $table) {
            //
        });
    }
}
