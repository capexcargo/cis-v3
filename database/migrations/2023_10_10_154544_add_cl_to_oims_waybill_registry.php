<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddClToOimsWaybillRegistry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('oims_waybill_registry', function (Blueprint $table) {
            $table->unsignedBigInteger('issued_to_fls')->nullable()->after('issued_to_om');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('oims_waybill_registry', function (Blueprint $table) {
            //
        });
    }
}
