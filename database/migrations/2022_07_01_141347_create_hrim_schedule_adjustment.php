<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrimScheduleAdjustment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrim_schedule_adjustment', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('current_work_schedule_id');
            $table->unsignedBigInteger('new_work_schedule_id');
            $table->date('inclusive_date_from')->nullable();
            $table->date('inclusive_date_to')->nullable();
            $table->longText('reason')->nullable();
            $table->unsignedBigInteger('first_approver')->nullable();
            $table->unsignedBigInteger('second_approver')->nullable();
            $table->unsignedBigInteger('third_approver')->nullable();
            $table->boolean('first_status')->nullable();
            $table->boolean('second_status')->nullable();
            $table->boolean('third_status')->nullable();
            $table->unsignedBigInteger('final_status_id')->default(1);
            $table->date('first_approval_date')->nullable();
            $table->date('second_approval_date')->nullable();
            $table->date('third_approval_date')->nullable();
            $table->longText('first_remarks')->nullable();
            $table->longText('second_remarks')->nullable();
            $table->longText('third_remarks')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('current_work_schedule_id')->references('id')->on('hrim_work_schedule');
            $table->foreign('new_work_schedule_id')->references('id')->on('hrim_work_schedule');
            $table->foreign('first_approver')->references('id')->on('users');
            $table->foreign('second_approver')->references('id')->on('users');
            $table->foreign('third_approver')->references('id')->on('users');
            $table->foreign('final_status_id')->references('id')->on('hrim_status_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrim_schedule_adjustment');
    }
}
