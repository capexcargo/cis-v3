<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColFromBranchReferencesQuadrant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('branch_references_quadrant', function (Blueprint $table) {
            $table->unsignedBigInteger('area_id')->after('quadrant');
            $table->string('status')->after('area_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('branch_references_quadrant', function (Blueprint $table) {
            //
        });
    }
}
