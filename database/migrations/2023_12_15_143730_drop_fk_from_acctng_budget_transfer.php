<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropFkFromAcctngBudgetTransfer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acctng_budget_transfer', function (Blueprint $table) {
            $table->dropForeign('acctng_budget_transfer_subaccount_id_from_foreign');
            $table->dropForeign('acctng_budget_transfer_subaccount_id_to_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acctng_budget_transfer', function (Blueprint $table) {
            //
        });
    }
}
