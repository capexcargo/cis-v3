<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTimelogAndWorkSchedule extends Migration
{
   
    public function up()
    {
        Schema::table('hrim_time_log', function (Blueprint $table) {
            $table->foreign('work_sched_id')->references('id')->on('hrim_work_schedule');
        });

        Schema::table('hrim_work_schedule', function (Blueprint $table) {
            $table->boolean('monday')->default(0)->after('id');
            $table->boolean('tuesday')->default(0)->after('monday');
            $table->boolean('wednesday')->default(0)->after('tuesday');
            $table->boolean('thursday')->default(0)->after('wednesday');
            $table->boolean('friday')->default(0)->after('thursday');
            $table->boolean('saturday')->default(0)->after('friday');
            $table->boolean('sunday')->default(0)->after('saturday');

            $table->dropColumn(['days_from', 'days_to']);
        });
    }

   
    public function down()
    {
        //
    }
}
