<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumnNameInEmployeeRequisition extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hrim_employee_requisition', function (Blueprint $table) {
            $table->renameColumn('1st_approver', 'first_approver');
            $table->renameColumn('2nd_approver', 'second_approver');
            $table->renameColumn('1st_status', 'first_status');
            $table->renameColumn('2nd_status', 'second_status');
            $table->renameColumn('1st_approval_date', 'first_approval_date');
            $table->renameColumn('2nd_approval_date', 'second_approval_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hrim_employee_requisition', function (Blueprint $table) {
            //
        });
    }
}
