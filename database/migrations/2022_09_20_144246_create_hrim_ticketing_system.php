<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrimTicketingSystem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_ticketing_system', function (Blueprint $table) {
            $table->id();
            $table->string('ticket_reference_no');
            $table->unsignedBigInteger('division_id');
            $table->longText('subject');
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('subcategory_id');
            $table->longText('message');
            $table->integer('task_holder');
            $table->integer('requested_by');
            $table->date('actual_start_date');
            $table->date('actual_end_date');
            $table->date('target_end_date');
            $table->integer('task_holder_status');
            $table->integer('requester_status');
            $table->integer('final_status');
            $table->date('task_holder_status_date');
            $table->date('requester_status_date');
            $table->date('final_status_date');
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('ticket_category');
            $table->foreign('subcategory_id')->references('id')->on('ticket_subcategory');
            $table->foreign('division_id')->references('id')->on('division');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_ticketing_system');
    }
}
