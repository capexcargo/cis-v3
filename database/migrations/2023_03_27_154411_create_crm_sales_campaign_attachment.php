<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmSalesCampaignAttachment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_sales_campaign_attachment', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sales_campaign_id');
            $table->string('path');
            $table->string('name');
            $table->string('extension');

            $table->foreign('sales_campaign_id')->references('id')->on('crm_sales_campaign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_sales_campaign_attachment');
    }
}
