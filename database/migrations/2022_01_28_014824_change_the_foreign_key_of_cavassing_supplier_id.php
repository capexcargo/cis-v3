<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTheForeignKeyOfCavassingSupplierId extends Migration
{
    public function up()
    {
        Schema::table('accounting_request_for_payment', function (Blueprint $table) {
            $table->dropForeign('accounting_request_for_payment_canvassing_id_foreign');

            $table->foreign('canvassing_supplier_id')->references('id')->on('accounting_canvassing_supplier');
        });
    }

    public function down()
    {
        //
    }
}
