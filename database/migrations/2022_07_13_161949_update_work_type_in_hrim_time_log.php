<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateWorkTypeInHrimTimeLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        // Schema::table('hrim_time_log', function (Blueprint $table) {
        //     $table->renameColumn('work_mode_id', 'work_type_id');
        // });

        // Schema::table('hrim_time_log', function (Blueprint $table) {
        //     $table->foreign('work_mode_id')->references('id')->on('hrim_work_mode_reference');
        // });

        // Schema::table('hrim_time_log', function (Blueprint $table) {
        //     $table->dropForeign('hrim_time_log_work_mode_id_foreign');
        // });

        Schema::dropIfExists('hrim_work_type_reference');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hrim_time_log', function (Blueprint $table) {
            //
        });
    }
}
