<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCvsSupplierAttachments extends Migration
{
    public function up()
    {
        Schema::create('accounting_cvs_supp_attachments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('canvassing_supplier_id');
            $table->string('path');
            $table->string('name');
            $table->timestamps();

            $table->foreign('canvassing_supplier_id')->references('id')->on('accounting_canvassing_supplier');
        });
    }

    public function down()
    {
        Schema::dropIfExists('accounting_cvs_supp_attachments');
    }
}
