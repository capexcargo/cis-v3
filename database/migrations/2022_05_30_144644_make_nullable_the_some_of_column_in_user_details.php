<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakeNullableTheSomeOfColumnInUserDetails extends Migration
{

    public function up()
    {
        Schema::table('hrim_character_reference', function (Blueprint $table) {
            $table->string('name')->nullable()->change();
            $table->string('position')->nullable()->change();
            $table->string('company')->nullable()->change();
            $table->string('mobile_number')->nullable()->change();
            $table->string('telephone_number')->nullable()->change();
        });

        Schema::table('hrim_employment_record', function (Blueprint $table) {
            $table->string('employer')->nullable()->change();
            $table->string('position')->nullable()->change();
            $table->integer('inclusive_year_from')->nullable()->change();
            $table->integer('inclusive_year_to')->nullable()->change();
        });

        Schema::table('hrim_family_information_sibling', function (Blueprint $table) {
            $table->string('name')->nullable()->change();
        });

        Schema::table('hrim_family_information_children', function (Blueprint $table) {
            $table->string('name')->nullable()->change();
        });

        Schema::table('hrim_family_information', function (Blueprint $table) {
            $table->string('fathers_name')->nullable()->change();
            $table->string('mothers_maiden_name')->nullable()->change();
            $table->string('spouse')->nullable()->change();
        });

        Schema::table('hrim_emergency_contact_details', function (Blueprint $table) {
            $table->string('name')->nullable()->change();
            $table->unsignedBigInteger('ecd_relationship_id')->nullable()->change();
            $table->string('address')->nullable()->change();
            $table->string('mobile_number')->nullable()->change();
            $table->string('telephone_number')->nullable()->change();
        });

        Schema::table('hrim_educational_attainment', function (Blueprint $table) {
            $table->unsignedBigInteger('ea_level_id')->nullable()->change();
            $table->string('educational_institution')->nullable()->change();
            $table->integer('inclusive_year_from')->nullable()->change();
            $table->integer('inclusive_year_to')->nullable()->change();
        });
    }


    public function down()
    {
    }
}
