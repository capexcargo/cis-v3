<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumn extends Migration
{

    public function up()
    {
        Schema::table('accounting_supplier', function (Blueprint $table) {
            $table->unsignedBigInteger('branch_id')->nullable()->after('type_id');

            $table->foreign('branch_id')->references('id')->on('branch_reference');
        });

        Schema::table('accounting_opex_type', function (Blueprint $table) {
            $table->longText('description')->nullable()->after('name');
        });
    }

    public function down()
    {
        Schema::table('accounting_supplier', function (Blueprint $table) {
            $table->dropForeign('branch_reference_branch_id_foreign');

            $table->dropColumn('branch_id');
        });

        Schema::table('accounting_opex_type', function (Blueprint $table) {
            $table->dropColumn('description');
        });
    }
}
