<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOimsBranchReferenceTelephone extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oims_branch_reference_telephone', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('branch_reference_id');
            $table->string('telephone');

            $table->timestamps();

            $table->foreign('branch_reference_id')->references('id')->on('oims_branch_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oims_branch_reference_telephone');
    }
}
