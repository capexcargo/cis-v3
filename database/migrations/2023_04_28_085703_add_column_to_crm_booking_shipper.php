<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToCrmBookingShipper extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_booking_shipper', function (Blueprint $table) {
            $table->unsignedBigInteger('state_id')->after('address');
            $table->unsignedBigInteger('city_id')->after('state_id');
            $table->unsignedBigInteger('barangay_id')->after('city_id');
            $table->unsignedBigInteger('postal_id')->after('barangay_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_booking_shipper', function (Blueprint $table) {
            //
        });
    }
}
