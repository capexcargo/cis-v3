<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOimsTransactionEntryCustomerAddressList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oims_transaction_entry_customer_address_list', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('account_type');
            $table->unsignedBigInteger('account_id');
            $table->longText('address_line_1');
            $table->longText('address_line_2');
            $table->unsignedBigInteger('address_type');
            $table->unsignedBigInteger('address_label');
            $table->unsignedBigInteger('state_id');
            $table->unsignedBigInteger('city_id');
            $table->unsignedBigInteger('barangay_id');
            $table->unsignedBigInteger('country_id');
            $table->unsignedBigInteger('postal_id');
            $table->boolean('is_primary');
            $table->string('contact_person_reference')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oims_transaction_entry_customer_address_list');
    }
}
