<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmSalesCampaign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_sales_campaign', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('voucher_code');
            $table->integer('discount_type');
            $table->float('discount_amount');
            $table->float('discount_percentage');
            $table->dateTime('start_datetime');
            $table->dateTime('end_datetime');
            $table->integer('maximum_usage');
            $table->longText('terms_conditon');
            $table->integer('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_sales_campaign');
    }
}
