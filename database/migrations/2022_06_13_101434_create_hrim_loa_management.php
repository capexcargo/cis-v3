<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrimLoaManagement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrim_loa_type_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::create('hrim_loa_management', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('division_id');
            $table->unsignedBigInteger('branch_id');
            $table->unsignedBigInteger('loa_type_id');
            $table->unsignedBigInteger('first_approver');
            $table->unsignedBigInteger('second_approver')->nullable();
            $table->unsignedBigInteger('third_approver')->nullable();
            $table->timestamps();

            $table->foreign('division_id')->references('id')->on('division');
            $table->foreign('branch_id')->references('id')->on('branch_reference');
            $table->foreign('loa_type_id')->references('id')->on('hrim_loa_type_reference');
            $table->foreign('first_approver')->references('id')->on('users');
            $table->foreign('second_approver')->references('id')->on('users');
            $table->foreign('third_approver')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrim_loa_management');
        Schema::dropIfExists('hrim_loa_type_reference');
    }
}
