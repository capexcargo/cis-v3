<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateEcdColumnToHrimEmergencyContactDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hrim_emergency_contact_details', function (Blueprint $table) {
            $table->dropForeign('hrim_emergency_contact_details_ecd_relationship_id_foreign');
            // $table->string('ecd_relationship_id')->nullable()->change();
           

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hrim_emergency_contact_details', function (Blueprint $table) {
            //
        });
    }
}
