<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToOimsWaybillRegistryDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('oims_waybill_registry_details', function (Blueprint $table) {
            $table->unsignedBigInteger('skipped_status2')->after('skipped_status');
            $table->unsignedBigInteger('skipped_status3')->after('skipped_status2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('oims_waybill_registry_details', function (Blueprint $table) {
            //
        });
    }
}
