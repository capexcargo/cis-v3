<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrimPerformance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrim_kra', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedBigInteger('type');
            $table->integer('quarter');
            $table->integer('year');

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('type')->references('id')->on('hrim_job_levels');
        });

        Schema::create('hrim_kpi', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('quarter');
            $table->integer('year');
            $table->integer('points');
            $table->string('target');
            $table->longText('remarks');

            $table->softDeletes();
            $table->timestamps();
        });
        
        Schema::create('hrim_kpi_tagged', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('position_id');
            $table->integer('quarter');
            $table->integer('year');
            $table->unsignedBigInteger('kra_id');
            $table->unsignedBigInteger('kpi_id');

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('position_id')->references('id')->on('hrim_positions');
            $table->foreign('kra_id')->references('id')->on('hrim_kra');
            $table->foreign('kpi_id')->references('id')->on('hrim_kpi');
        });
        
        Schema::create('hrim_core_value', function (Blueprint $table) {
            $table->id();
            $table->longText('description');

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('hrim_leadership_competencies', function (Blueprint $table) {
            $table->id();
            $table->longText('description');

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('hrim_evaluator_management', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('first_approver');
            $table->unsignedBigInteger('second_approver');
            $table->unsignedBigInteger('third_approver');

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('hrim_performance_evaluation', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('position_id');
            $table->unsignedBigInteger('kra_id');
            $table->unsignedBigInteger('kpi_id');
            $table->unsignedBigInteger('core_values_id');
            $table->unsignedBigInteger('leadership_comp_id');

            $table->integer('first_points');
            $table->integer('second_points');
            $table->integer('third_points');

            $table->integer('first_status');
            $table->integer('second_status');
            $table->integer('third_status');

            $table->unsignedBigInteger('first_evaluator');
            $table->unsignedBigInteger('second_evaluator');
            $table->unsignedBigInteger('third_evaluator');
            
            $table->date('first_eval_date');
            $table->date('second_eval_date');
            $table->date('third_eval_date');

            $table->longText('first_remarks');
            $table->longText('second_remarks');
            $table->longText('third_remarks');
            $table->longText('user_id_remarks');

            $table->integer('quarter');
            $table->integer('year');

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('position_id')->references('id')->on('hrim_positions');

            $table->foreign('kra_id')->references('id')->on('hrim_kra');
            $table->foreign('kpi_id')->references('id')->on('hrim_kpi');
            $table->foreign('core_values_id')->references('id')->on('hrim_core_value');
            $table->foreign('leadership_comp_id')->references('id')->on('hrim_leadership_competencies');
            $table->foreign('first_evaluator')->references('id')->on('hrim_evaluator_management');
            $table->foreign('second_evaluator')->references('id')->on('hrim_evaluator_management');
            $table->foreign('third_evaluator')->references('id')->on('hrim_evaluator_management');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrim_performance_evaluation');
        Schema::dropIfExists('hrim_evaluator_management');
        Schema::dropIfExists('hrim_leadership_competencies');
        Schema::dropIfExists('hrim_core_value');
        Schema::dropIfExists('hrim_kpi_tagged');
        Schema::dropIfExists('hrim_kpi');
        Schema::dropIfExists('hrim_kra');
    }
}
