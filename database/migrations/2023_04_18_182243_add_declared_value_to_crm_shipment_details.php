<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDeclaredValueToCrmShipmentDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_shipment_details', function (Blueprint $table) {
            $table->float('declared_value', 11, 2)->nullable()->after('destination_address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_shipment_details', function (Blueprint $table) {
            //
        });
    }
}
