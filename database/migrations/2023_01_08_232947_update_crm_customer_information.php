<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCrmCustomerInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_customer_information', function (Blueprint $table) {
            $table->string('account_no')->nullable()->change();
            $table->string('fullname')->nullable()->change();
            $table->string('company_name')->nullable()->change();
            $table->string('first_name')->nullable()->change();
            $table->string('last_name')->nullable()->change();
            $table->string('company_link')->nullable()->change();
            $table->string('company_email_address')->nullable()->change();
            $table->integer('company_tel_no')->nullable()->change();
            $table->integer('company_mobile_no')->nullable()->change();
            $table->unsignedBigInteger('industry_id')->nullable()->change();
            $table->unsignedBigInteger('life_stage_id')->nullable()->change();
            $table->boolean('is_mother_account')->nullable()->change();
            $table->unsignedBigInteger('child_account_id')->nullable()->change();
            $table->unsignedBigInteger('contact_owner_id')->nullable()->change();
            $table->unsignedBigInteger('contact_owner_id')->nullable()->change();
            $table->string('tin')->nullable()->change();
            $table->date('company_anniversary')->nullable()->change();
            $table->unsignedBigInteger('marketing_channel_id')->nullable()->change();
            $table->longText('notes')->nullable()->change();
            $table->integer('category')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
