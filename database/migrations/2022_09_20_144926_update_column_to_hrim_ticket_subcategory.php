<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumnToHrimTicketSubcategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ticket_subcategory', function (Blueprint $table) {
            $table->integer('sla')->nullable()->after('category_id');
            $table->integer('severity')->nullable()->after('sla');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ticket_subcategory', function (Blueprint $table) {
            //
        });
    }
}
