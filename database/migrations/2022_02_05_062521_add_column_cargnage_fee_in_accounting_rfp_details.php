<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnCargnageFeeInAccountingRfpDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounting_rfp_details', function (Blueprint $table) {
            $table->string('trucking_category')->nullable()->change();
            $table->float('cargnage_fee', 11, 2)->after('freight_amount')->nullable();
            $table->unsignedBigInteger('freight_usage_id')->after('cargnage_fee')->nullable();
            $table->foreign('freight_usage_id')->references('id')->on('accounting_freight_usage_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounting_rfp_details', function (Blueprint $table) {
            //
        });
    }
}
