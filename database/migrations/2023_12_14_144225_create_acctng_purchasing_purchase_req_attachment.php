<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcctngPurchasingPurchaseReqAttachment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acctng_purchasing_purchase_req_atta', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('reference_no_id');
            $table->unsignedBigInteger('purchase_req_id');
            $table->string('path');
            $table->string('name');
            $table->string('extension');

            $table->timestamps();

            $table->foreign('reference_no_id')->references('id')->on('acctng_purchasing');
            $table->foreign('purchase_req_id')->references('id')->on('acctng_purchasing_purchase_req_details');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acctng_purchasing_purchase_req_attachment');
    }
}
