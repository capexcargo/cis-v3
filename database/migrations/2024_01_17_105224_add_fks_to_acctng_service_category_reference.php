<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFksToAcctngServiceCategoryReference extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acctng_service_category_reference', function (Blueprint $table) {
            $table->foreign('sub_accounts_id')->references('id')->on('acctng_sub_accounts_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acctng_service_category_reference', function (Blueprint $table) {
            //
        });
    }
}
