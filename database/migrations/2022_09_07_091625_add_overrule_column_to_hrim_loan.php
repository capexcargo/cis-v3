<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOverruleColumnToHrimLoan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hrim_loan', function (Blueprint $table) {
            $table->integer('overrule_approver')->nullable()->after('purpose');
            $table->date('overrule_approval_date')->nullable()->after('overrule_approver');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hrim_loan', function (Blueprint $table) {
            //
        });
    }
}
