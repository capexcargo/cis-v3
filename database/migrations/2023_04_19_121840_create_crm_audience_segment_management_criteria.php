<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmAudienceSegmentManagementCriteria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_audience_segment_management_criteria', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('audience_id');
            $table->integer('criteria_id');
            $table->string('criteria_name');
            $table->date('onboarded_date_from')->nullable();
            $table->date('onboarded_date_to')->nullable();
            $table->timestamps();

            $table->foreign('audience_id')->references('id')->on('crm_audience_segment_management');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_audience_segment_management_criteria');
    }
}
