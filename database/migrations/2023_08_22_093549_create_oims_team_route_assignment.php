<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOimsTeamRouteAssignment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oims_team_route_assignment', function (Blueprint $table) {
            $table->id();
            $table->string('tra_no');
            $table->unsignedBigInteger('branch_id');
            $table->unsignedBigInteger('created_user_id');
            $table->date('dispatch_date');

            $table->timestamps();

            $table->foreign('branch_id')->references('id')->on('branch_references_walkin');
            $table->foreign('created_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oims_team_route_assignment');
    }
}
