<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BudgetPlan extends Migration
{
    public function up()
    {
        Schema::create('division', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('accounting_budget_source', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('division_id');
            $table->string('name');
            $table->timestamps();

            $table->foreign('division_id')->references('id')->on('division');
        });

        Schema::create('accounting_budget_chart', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('division_id');
            $table->unsignedBigInteger('budget_source_id');
            $table->string('name');
            $table->timestamps();

            $table->foreign('division_id')->references('id')->on('division');
            $table->foreign('budget_source_id')->references('id')->on('accounting_budget_source');
        });

        Schema::create('accounting_opex_type', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('accounting_budget_plan', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('division_id')->nullable();
            $table->unsignedBigInteger('budget_source_id')->nullable();
            $table->unsignedBigInteger('budget_chart_id')->nullable();
            $table->string('item')->nullable();
            $table->unsignedBigInteger('opex_type_id')->nullable();
            $table->integer('year')->nullable();
            $table->float('january', 11, 2)->nullable();
            $table->float('february', 11, 2)->nullable();
            $table->float('march', 11, 2)->nullable();
            $table->float('april', 11, 2)->nullable();
            $table->float('may', 11, 2)->nullable();
            $table->float('june', 11, 2)->nullable();
            $table->float('july', 11, 2)->nullable();
            $table->float('august', 11, 2)->nullable();
            $table->float('september', 11, 2)->nullable();
            $table->float('october', 11, 2)->nullable();
            $table->float('november', 11, 2)->nullable();
            $table->float('december', 11, 2)->nullable();
            $table->float('total_amount', 11, 2)->nullable();
            $table->timestamps();

            $table->foreign('division_id')->references('id')->on('division');
            $table->foreign('budget_source_id')->references('id')->on('accounting_budget_source');
            $table->foreign('budget_chart_id')->references('id')->on('accounting_budget_chart');
            $table->foreign('opex_type_id')->references('id')->on('accounting_opex_type');
        });

        Schema::create('accounting_transfer_type_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->string('display')->nullable();
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::create('accounting_transfer_budget', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('budget_plan_id');
            $table->unsignedBigInteger('budget_plan_id_from')->nullable();
            $table->unsignedBigInteger('transfer_type_id');
            $table->string('month_from')->nullable();
            $table->string('month_to');
            $table->float('amount', 11, 2);
            $table->timestamps();

            $table->foreign('budget_plan_id')->references('id')->on('accounting_budget_plan');
            $table->foreign('budget_plan_id_from')->references('id')->on('accounting_budget_plan');
            $table->foreign('transfer_type_id')->references('id')->on('accounting_transfer_type_reference');
        });
    }

    public function down()
    {
        Schema::dropIfExists('accounting_budget_source');
        Schema::dropIfExists('accounting_budget_chart');
        Schema::dropIfExists('division');
        Schema::dropIfExists('accounting_opex_type');
        Schema::dropIfExists('accounting_budget_plan');
        Schema::dropIfExists('accounting_transfer_type_reference');
        Schema::dropIfExists('accounting_transfer_budget');
    }
}
