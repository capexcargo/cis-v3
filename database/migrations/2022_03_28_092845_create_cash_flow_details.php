<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCashFlowDetails extends Migration
{
    
    public function up()
    {
        Schema::create('accounting_cf_status_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::create('accounting_cash_flow_details', function (Blueprint $table) {
            $table->id();
            $table->date('encashment_date')->nullable();
            $table->string('check_no')->nullable();
            $table->date('cv_date')->nullable();
            $table->unsignedBigInteger('payee_id')->nullable();
            $table->longText('check_description')->nullable();
            $table->float('encashment_amount', 11, 2)->nullable();
            $table->float('locked_amount', 11, 2)->nullable();
            $table->unsignedBigInteger('status_id')->nullable();
            $table->timestamps();

            $table->foreign('payee_id')->references('id')->on('accounting_supplier');
            $table->foreign('status_id')->references('id')->on('accounting_cf_status_reference');
        });
    }

    
    public function down()
    {
        Schema::dropIfExists('cash_flow_details');
        Schema::dropIfExists('accounting_cf_status_reference');
    }
}
