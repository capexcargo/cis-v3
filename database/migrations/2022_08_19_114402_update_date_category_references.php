<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateDateCategoryReferences extends Migration
{

    public function up()
    {
        Schema::table('hrim_date_category_reference', function (Blueprint $table) {
            $table->float('rate_percentage', 11, 2)->after('display');
        });

        Schema::table('hrim_overtime', function (Blueprint $table) {
            $table->dropForeign('hrim_overtime_overtime_type_id_foreign');
        });

        Schema::table('hrim_overtime', function (Blueprint $table) {
            $table->renameColumn('overtime_type_id', 'date_category_id');
        });

        Schema::table('hrim_overtime', function (Blueprint $table) {
            $table->foreign('date_category_id')->references('id')->on('hrim_date_category_reference');
        });

        Schema::dropIfExists('hrim_overtime_type_reference');

    }


    public function down()
    {
        //
    }
}
