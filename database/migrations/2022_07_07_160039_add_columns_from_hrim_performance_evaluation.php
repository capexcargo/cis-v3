<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsFromHrimPerformanceEvaluation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hrim_performance_evaluation', function (Blueprint $table) {
            $table->unsignedBigInteger('core_values_id')->nullable()->after('kpi_id');
            $table->unsignedBigInteger('leadership_comp_id')->nullable()->after('core_values_id');
            
            $table->integer('first_points')->nullable()->after('leadership_comp_id');
            $table->integer('second_points')->nullable()->after('first_points');
            $table->integer('third_points')->nullable()->after('second_points');
            
            $table->integer('first_status')->nullable()->after('third_points');
            $table->integer('second_status')->nullable()->after('first_status');
            $table->integer('third_status')->nullable()->after('second_status');
            
            $table->unsignedBigInteger('first_evaluator')->nullable()->after('third_status');
            $table->unsignedBigInteger('second_evaluator')->nullable()->after('first_evaluator');
            $table->unsignedBigInteger('third_evaluator')->nullable()->after('second_evaluator');
            
            $table->integer('first_eval_date')->nullable()->after('third_evaluator');
            $table->integer('second_eval_date')->nullable()->after('first_eval_date');
            $table->integer('third_eval_date')->nullable()->after('second_eval_date');
            
            $table->integer('first_remarks')->nullable()->after('third_eval_date');
            $table->integer('second_remarks')->nullable()->after('first_remarks');
            $table->integer('third_remarks')->nullable()->after('second_remarks');
            
            $table->unsignedBigInteger('user_id_remarks')->nullable()->after('third_remarks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hrim_performance_evaluation', function (Blueprint $table) {
            //
        });
    }
}
