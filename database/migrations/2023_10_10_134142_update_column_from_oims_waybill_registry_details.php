<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumnFromOimsWaybillRegistryDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('oims_waybill_registry_details', function (Blueprint $table) {
           $table->unsignedBigInteger('skipped_status2')->nullable()->change();
           $table->unsignedBigInteger('skipped_status3')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('oims_waybill_registry_details', function (Blueprint $table) {
            //
        });
    }
}
