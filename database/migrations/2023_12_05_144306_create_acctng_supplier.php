<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcctngSupplier extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acctng_supplier', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('trade_name');
            $table->integer('tin');
            $table->unsignedBigInteger('industry_id');
            $table->unsignedBigInteger('region_id');
            $table->unsignedBigInteger('province_id');
            $table->unsignedBigInteger('barangay_id');
            $table->string('postal_code');
            $table->longText('address');
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name');
            $table->string('email_address');
            $table->integer('mobile_number');
            $table->integer('tel_number');
            $table->string('payee_name');
            $table->integer('payee_account_number');
            $table->integer('bank_account_number');
            $table->string('bank_account_name');
            $table->unsignedBigInteger('bank_id');

            $table->timestamps();

            $table->foreign('industry_id')->references('id')->on('acctng_industry_reference');
            $table->foreign('region_id')->references('id')->on('region');
            $table->foreign('province_id')->references('id')->on('state');
            $table->foreign('barangay_id')->references('id')->on('barangay');
            $table->foreign('bank_id')->references('id')->on('acctng_bank_name_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acctng_supplier');
    }
}
