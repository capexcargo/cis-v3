<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColmnFromOimsBranchReference extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('oims_branch_reference', function (Blueprint $table) {
            $table->foreign('origin_destination_id')->references('id')->on('oims_origin_destination_port_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('oims_branch_reference', function (Blueprint $table) {
            //
        });
    }
}
