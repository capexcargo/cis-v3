<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateToAccountingPoWaybill extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounting_po_waybill', function (Blueprint $table) {
            $table->id();
            $table->string('waybill');
            $table->unsignedBigInteger('branch_id');
            $table->unsignedBigInteger('cvs_reference_id');
            $table->timestamps();

            $table->foreign('cvs_reference_id')->references('id')->on('accounting_canvassing');
            $table->foreign('branch_id')->references('id')->on('branch_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('to_accounting_po_waybill');
    }
}
