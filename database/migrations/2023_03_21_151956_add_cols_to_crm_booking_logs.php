<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToCrmBookingLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_booking_logs', function (Blueprint $table) {
            $table->unsignedBigInteger('approve_user_id')->nullable()->after('user_id');
            $table->dateTime('approve_date')->nullable()->after('approve_user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_booking_logs', function (Blueprint $table) {
            //
        });
    }
}
