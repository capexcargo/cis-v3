<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmOpportunity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_opportunity', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sr_no');
            $table->unsignedBigInteger('account_id');
            $table->string('opportunity_name');
            $table->unsignedBigInteger('industry_id');
            $table->unsignedBigInteger('sales_stage_id');
            $table->unsignedBigInteger('opportunity_status_id');
            $table->float('deal_size', 11, 2);
            $table->float('actual_amount', 11, 2);
            $table->string('booking_reference');
            $table->date('lead_conversion_date')->nullable();
            $table->date('close_date')->nullable();
            $table->unsignedBigInteger('created_by');

            $table->timestamps();

            $table->foreign('industry_id')->references('id')->on('crm_industry');
            $table->foreign('sales_stage_id')->references('id')->on('crm_sales_stage');
            $table->foreign('opportunity_status_id')->references('id')->on('crm_opportunity_status_reference');
        });

        
        Schema::create('crm_opportunity_attachment', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('opportunity_id');
            $table->string('path');
            $table->string('name');
            $table->string('extension');
            
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('opportunity_id')->references('id')->on('crm_opportunity');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_opportunity_attachment');
        Schema::dropIfExists('crm_opportunity');
    }
}
