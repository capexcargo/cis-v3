<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeDataTypeFreightReferenceNoInAccountingRfpDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounting_rfp_details', function (Blueprint $table) {
            $table->string('freight_reference_no')->nullable()->change();
            $table->foreign('freight_reference_type_id')->references('id')->on('accounting_freight_reference_type');
            $table->renameColumn('trucking_category_id', 'trucking_category')->nullable()->change();

            $table->dropColumn(['hustling_fee', 'arrastre']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounting_rfp_details', function (Blueprint $table) {
            //
        });
    }
}
