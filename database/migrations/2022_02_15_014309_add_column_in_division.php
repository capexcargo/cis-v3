<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnInDivision extends Migration
{
   
    public function up()
    {
        Schema::table('division', function (Blueprint $table) {
            $table->string('portal_name')->nullable()->after('name');
            $table->string('decription')->nullable()->after('portal_name');
        });
    }

    public function down()
    {
        Schema::table('division', function (Blueprint $table) {
            //
        });
    }
}
