<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnExtensionInAttachments extends Migration
{
    public function up()
    {
        Schema::table('accounting_cvs_supp_attachments', function (Blueprint $table) {
            $table->string('extension')->after('name');
        });

        Schema::table('accounting_rfp_attachments', function (Blueprint $table) {
            $table->string('extension')->after('name');
        });
    }

    public function down()
    {
    }
}
