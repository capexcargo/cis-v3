<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOimsWaybillRegistryDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oims_waybill_registry_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('waybill_registry_id');
            $table->string('waybill');
            $table->unsignedBigInteger('branch_id');
            $table->unsignedBigInteger('status');
            $table->unsignedBigInteger('skipped_status')->nullable();
            $table->unsignedBigInteger('issue_to_om_identify')->nullable();
            $table->unsignedBigInteger('issue_to_checker_identify')->nullable();
            $table->dateTime('date_fls_issued')->nullable();
            $table->dateTime('date_om_issued')->nullable();
            $table->dateTime('date_checker_issued')->nullable();
            $table->dateTime('date_customer_issued')->nullable();
            $table->dateTime('date_agent_issued')->nullable();
            $table->timestamps();

            $table->foreign('waybill_registry_id')->references('id')->on('oims_waybill_registry');
            $table->foreign('branch_id')->references('id')->on('oims_branch_reference');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oims_waybill_registry_details');
    }
}
