<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnAccountTypeInAccountRequestForPayment extends Migration
{
    
    public function up()
    {
        Schema::table('accounting_request_for_payment', function (Blueprint $table) {
            $table->unsignedBigInteger('account_type_id')->nullable()->after('ca_reference_type_id');

            $table->foreign('account_type_id')->references('id')->on('accounting_account_type_reference');
        });
    }

    public function down()
    {
        Schema::table('accounting_request_for_payment', function (Blueprint $table) {
            //
        });
    }
}
