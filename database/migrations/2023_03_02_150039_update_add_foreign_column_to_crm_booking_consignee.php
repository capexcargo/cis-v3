<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAddForeignColumnToCrmBookingConsignee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_booking_consignee', function (Blueprint $table) {
            $table->foreign('transposrt_mode_id')->references('id')->on('crm_rate_transport_mode_reference');
            $table->foreign('service_mode_id')->references('id')->on('crm_service_mode_reference');
            $table->foreign('mode_of_payment_id')->references('id')->on('crm_mode_of_payment_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_booking_consignee', function (Blueprint $table) {
            //
        });
    }
}
