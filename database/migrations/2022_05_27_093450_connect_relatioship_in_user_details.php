<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ConnectRelatioshipInUserDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_details', function (Blueprint $table) {
            $table->foreign('branch_id')->references('id')->on('branch_reference');
            $table->foreign('schedule_id')->references('id')->on('hrim_work_schedule');
            $table->foreign('position_id')->references('id')->on('hrim_positions');
            $table->foreign('job_level_id')->references('id')->on('hrim_job_levels');
            $table->foreign('department_id')->references('id')->on('hrim_departments');
            $table->foreign('division_id')->references('id')->on('division');
            $table->foreign('employment_category_id')->references('id')->on('hrim_employment_category');
            $table->foreign('employment_status_id')->references('id')->on('hrim_employment_status');
            $table->foreign('gender_id')->references('id')->on('hrim_gender');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_details', function (Blueprint $table) {
            //
        });
    }
}
