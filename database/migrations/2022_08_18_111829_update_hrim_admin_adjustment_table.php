<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateHrimAdminAdjustmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hrim_admin_adjustment', function (Blueprint $table) {
            $table->decimal('amount')->change();
            $table->unsignedBigInteger('first_approver')->nullable()->after('amount');
            $table->unsignedBigInteger('second_approver')->nullable()->after('first_approver');
            $table->integer('first_status')->nullable()->after('second_approver');
            $table->integer('second_status')->nullable()->after('first_status');
            $table->longText('first_remarks')->nullable()->after('second_status');
            $table->longText('second_remarks')->nullable()->after('first_remarks');
            $table->date('first_date')->nullable()->after('second_remarks');
            $table->date('second_date')->nullable()->after('first_date');
            $table->integer('final_status')->nullable()->after('second_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrim_admin_adjustment');
    }
}
