<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToCrmCustomerInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_customer_information', function (Blueprint $table) {
            $table->dateTime('approval_date')->nullable()->after('created_by');
            $table->unsignedBigInteger('approver1_id')->nullable()->after('approval_date');
            $table->unsignedBigInteger('approver2_id')->nullable()->after('approver1_id');
            $table->unsignedBigInteger('approver3_id')->nullable()->after('approver2_id');
            $table->unsignedBigInteger('approver4_id')->nullable()->after('approver3_id');
            $table->unsignedBigInteger('approver1_status')->nullable()->after('approver4_id');
            $table->unsignedBigInteger('approver2_status')->nullable()->after('approver1_status');
            $table->unsignedBigInteger('approver3_status')->nullable()->after('approver2_status');
            $table->unsignedBigInteger('approver4_status')->nullable()->after('approver3_status');
            $table->unsignedBigInteger('approver1_remarks')->nullable()->after('approver4_status');
            $table->unsignedBigInteger('approver2_remarks')->nullable()->after('approver1_remarks');
            $table->unsignedBigInteger('approver3_remarks')->nullable()->after('approver2_remarks');
            $table->unsignedBigInteger('approver4_remarks')->nullable()->after('approver3_remarks');
            $table->dateTime('approver1_date')->nullable()->after('approver4_remarks');
            $table->dateTime('approver2_date')->nullable()->after('approver1_date');
            $table->dateTime('approver3_date')->nullable()->after('approver2_date');
            $table->dateTime('approver4_date')->nullable()->after('approver3_date');
            $table->unsignedBigInteger('final_status')->nullable()->after('approver4_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_customer_information', function (Blueprint $table) {
            //
        });
    }
}
