<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrimTar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrim_tar_reason_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->boolean('is_visible')->default(1);
            $table->unsignedBigInteger('created_by');
            $table->timestamps();

            $table->foreign('created_by')->references('id')->on('users');
        });

        Schema::create('hrim_status_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::create('hrim_tar', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->time('actual_time_in');
            $table->time('actual_time_out');
            $table->date('date');
            $table->unsignedBigInteger('tar_reason_id');
            $table->unsignedBigInteger('first_approver')->nullable();
            $table->unsignedBigInteger('second_approver')->nullable();
            $table->unsignedBigInteger('third_approver')->nullable();
            $table->boolean('first_status')->nullable();
            $table->boolean('second_status')->nullable();
            $table->boolean('third_status')->nullable();
            $table->unsignedBigInteger('final_status_id')->default(1);
            $table->date('first_approval_date')->nullable();
            $table->date('second_approval_date')->nullable();
            $table->date('third_approval_date')->nullable();
            $table->longText('first_approver_remarks')->nullable();
            $table->longText('second_approver_remarks')->nullable();
            $table->longText('third_approver_remarks')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('tar_reason_id')->references('id')->on('hrim_tar_reason_reference');
            $table->foreign('first_approver')->references('id')->on('users');
            $table->foreign('second_approver')->references('id')->on('users');
            $table->foreign('third_approver')->references('id')->on('users');
            $table->foreign('final_status_id')->references('id')->on('hrim_status_reference');
        });
    }


    public function down()
    {
        Schema::dropIfExists('hrim_tar');
        Schema::dropIfExists('hrim_status_reference');
        Schema::dropIfExists('hrim_tar_reason_reference');
    }
}
