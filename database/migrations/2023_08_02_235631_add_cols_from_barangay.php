<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsFromBarangay extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('barangay', function (Blueprint $table) {
            $table->unsignedBigInteger('zipcode_id')->after('city_id');
            $table->unsignedBigInteger('region_id')->nullable()->after('zipcode_id');
            $table->unsignedBigInteger('island_group_id')->nullable()->after('region_id');
            $table->unsignedBigInteger('status')->after('island_group_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barangay', function (Blueprint $table) {
            //
        });
    }
}
