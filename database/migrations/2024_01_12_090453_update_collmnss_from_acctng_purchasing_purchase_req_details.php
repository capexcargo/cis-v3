<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCollmnssFromAcctngPurchasingPurchaseReqDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acctng_purchasing_purchase_req_details', function (Blueprint $table) {
            $table->unsignedBigInteger('reason_id')->nullable()->after('budget_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acctng_purchasing_purchase_req_details', function (Blueprint $table) {
            //
        });
    }
}
