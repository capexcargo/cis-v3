<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOimsTeamReference extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oims_team_reference', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedBigInteger('quadrant_id');
            $table->integer('status');

            $table->timestamps();

            $table->foreign('quadrant_id')->references('id')->on('oims_area_reference');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oims_team_reference');
    }
}
