<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumntableAccountingRfpDetails extends Migration
{
    
    public function up()
    {
        Schema::table('accounting_request_for_payment', function (Blueprint $table) {
            $table->unsignedBigInteger('canvassing_id')->nullable()->after('canvasser');

            $table->foreign('canvassing_id')->references('id')->on('accounting_canvassing');
        });
    }

    
    public function down()
    {
        Schema::table('accounting_request_for_payment', function (Blueprint $table) {
            $table->dropForeign('accounting_canvassing_canvassing_id_foreign');
            $table->dropColumn('canvassing_id');
        });
    }
}
