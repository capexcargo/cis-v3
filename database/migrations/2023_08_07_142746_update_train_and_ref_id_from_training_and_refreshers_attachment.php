<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTrainAndRefIdFromTrainingAndRefreshersAttachment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('training_and_refreshers_attachment', function (Blueprint $table) {
            $table->renameColumn('train_and_ref_id', 'video_ref')->after('id')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('training_and_refreshers_attachment', function (Blueprint $table) {
            //
        });
    }
}
