<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddContactOwnerIdToCrmOpportunity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_opportunity', function (Blueprint $table) {
            $table->integer('contact_owner_id')->nullable()->after('opportunity_status_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_opportunity', function (Blueprint $table) {
            //
        });
    }
}
