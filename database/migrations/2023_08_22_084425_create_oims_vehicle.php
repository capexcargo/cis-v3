<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOimsVehicle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oims_vehicle', function (Blueprint $table) {
            $table->id();
            $table->string('plate_no');
            $table->unsignedBigInteger('vehicle_type');
            $table->unsignedBigInteger('branch_id');

            $table->timestamps();

            $table->foreign('vehicle_type')->references('id')->on('oims_vehicle_type');
            $table->foreign('branch_id')->references('id')->on('oims_branch_reference');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oims_vehicle');
    }
}
