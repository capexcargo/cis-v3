<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToCrmQuotaManagementStakeholder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_quota_management_stakeholder', function (Blueprint $table) {
            $table->float('actual_volume', 11, 2)->nullable()->after('amount');
            $table->float('actual_amount', 11, 2)->nullable()->after('actual_volume');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_quota_management_stakeholder', function (Blueprint $table) {
            //
        });
    }
}
