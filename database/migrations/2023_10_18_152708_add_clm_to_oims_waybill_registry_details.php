<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddClmToOimsWaybillRegistryDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('oims_waybill_registry_details', function (Blueprint $table) {
            $table->unsignedBigInteger('issue_to_fls_idetify')->nullable()->after('skipped_status3');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('oims_waybill_registry_details', function (Blueprint $table) {
            //
        });
    }
}
