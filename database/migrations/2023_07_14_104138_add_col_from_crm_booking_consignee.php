<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColFromCrmBookingConsignee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_booking_consignee', function (Blueprint $table) {
            $table->string('charge_to_name')->nullable()->after('charge_to_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_booking_consignee', function (Blueprint $table) {
            //
        });
    }
}
