<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropFkFromOimsTeamRouteAssignment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('oims_team_route_assignment', function (Blueprint $table) {
            $table->dropForeign('oims_team_route_assignment_branch_id_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('oims_team_route_assignment', function (Blueprint $table) {
            //
        });
    }
}
