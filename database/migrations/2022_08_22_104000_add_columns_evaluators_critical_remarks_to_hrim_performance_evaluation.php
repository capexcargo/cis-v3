<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsEvaluatorsCriticalRemarksToHrimPerformanceEvaluation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hrim_performance_evaluation', function (Blueprint $table) {
            $table->longText('core_val_1st_critical_remarks')->nullable()->after('critical_remarks');
            $table->longText('core_val_2nd_critical_remarks')->nullable()->after('core_val_1st_critical_remarks');
            $table->longText('core_val_3rd_critical_remarks')->nullable()->after('core_val_2nd_critical_remarks');
            $table->longText('lead_com_1st_critical_remarks')->nullable()->after('core_val_3rd_critical_remarks');
            $table->longText('lead_com_2nd_critical_remarks')->nullable()->after('lead_com_1st_critical_remarks');
            $table->longText('lead_com_3rd_critical_remarks')->nullable()->after('lead_com_2nd_critical_remarks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hrim_performance_evaluation', function (Blueprint $table) {
            //
        });
    }
}
