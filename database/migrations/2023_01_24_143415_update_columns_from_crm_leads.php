<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumnsFromCrmLeads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_leads', function (Blueprint $table) {
            $table->unsignedBigInteger('lead_status_id')->nullable()->change();
            $table->integer('qualification_score')->nullable()->change();
            $table->unsignedBigInteger('lead_qualification_id')->nullable()->change();
            $table->float('deal_size')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_leads', function (Blueprint $table) {
            //
        });
    }
}
