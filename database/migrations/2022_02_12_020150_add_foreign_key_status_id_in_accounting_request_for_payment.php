<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeyStatusIdInAccountingRequestForPayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounting_request_for_payment', function (Blueprint $table) {
            $table->foreign('status_id')->references('id')->on('accounting_status_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounting_request_for_payment', function (Blueprint $table) {
            //
        });
    }
}
