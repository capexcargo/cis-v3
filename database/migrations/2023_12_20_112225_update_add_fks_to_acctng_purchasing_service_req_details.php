<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAddFksToAcctngPurchasingServiceReqDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acctng_purchasing_service_req_details', function (Blueprint $table) {
            $table->foreign(['preferred_worker'], 'pref_worker_fore_id')->references('id')->on('acctng_manpower_reference');
            $table->foreign('purpose')->references('id')->on('acctng_purpose_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acctng_purchasing_service_req_details', function (Blueprint $table) {
            //
        });
    }
}
