<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrimSss extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrim_sss', function (Blueprint $table) {
            $table->id();
            $table->integer('year')->nullable();
            $table->float('grosspay_min', 11, 2)->nullable();
            $table->float('grosspay_max', 11, 2)->nullable();
            $table->float('regular_er', 11, 2)->nullable();
            $table->float('regular_ee', 11, 2)->nullable();
            $table->float('compensation_er', 11, 2)->nullable();
            $table->float('compensation_ee', 11, 2)->nullable();
            $table->float('mandatory_er', 11, 2)->nullable();
            $table->float('mandatory_ee', 11, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrim_sss');
    }
}
