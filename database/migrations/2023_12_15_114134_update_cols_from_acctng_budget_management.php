<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColsFromAcctngBudgetManagement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acctng_budget_management', function (Blueprint $table) {
            $table->string('subaccount_name')->change();
            $table->unsignedBigInteger('budget_category_id')->nullable()->change();
            $table->unsignedBigInteger('budget_coa_id')->nullable()->change();
            $table->unsignedBigInteger('budget_opex_id')->nullable()->change();
            $table->renameColumn('dec', 'dece');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acctng_budget_management', function (Blueprint $table) {
            //
        });
    }
}
