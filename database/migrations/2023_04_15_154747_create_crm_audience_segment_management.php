<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmAudienceSegmentManagement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_audience_segment_management', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('filter_id');
            $table->date('onboarded_date_from')->nullable();
            $table->date('onboarded_date_to')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_audience_segment_management');
    }
}
