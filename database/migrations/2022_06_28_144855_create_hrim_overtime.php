<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrimOvertime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrim_overtime_type_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->integer('rate_percentage');
            $table->boolean('is_visible')->default(1);
            $table->unsignedBigInteger('created_by');
            $table->timestamps();
        });

        Schema::create('hrim_overtime', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('time_log_id');
            $table->date('date_time_from')->nullable();
            $table->date('date_time_to')->nullable();
            $table->time('time_from')->nullable();
            $table->time('time_to')->nullable();
            $table->integer('overtime_rendered')->nullable();
            $table->integer('overtime_request')->nullable();
            $table->integer('overtime_approved')->nullable();
            $table->unsignedBigInteger('overtime_type_id')->nullable();
            $table->longText('reason')->nullable();
            $table->unsignedBigInteger('first_approver')->nullable();
            $table->unsignedBigInteger('second_approver')->nullable();
            $table->unsignedBigInteger('third_approver')->nullable();
            $table->boolean('first_status')->nullable();
            $table->boolean('second_status')->nullable();
            $table->boolean('third_status')->nullable();
            $table->unsignedBigInteger('final_status_id')->default(1);
            $table->date('first_approval_date')->nullable();
            $table->date('second_approval_date')->nullable();
            $table->date('third_approval_date')->nullable();
            $table->longText('first_remarks')->nullable();
            $table->longText('second_remarks')->nullable();
            $table->longText('third_remarks')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('time_log_id')->references('id')->on('hrim_time_log');
            $table->foreign('overtime_type_id')->references('id')->on('hrim_overtime_type_reference');
            $table->foreign('first_approver')->references('id')->on('users');
            $table->foreign('second_approver')->references('id')->on('users');
            $table->foreign('third_approver')->references('id')->on('users');
            $table->foreign('final_status_id')->references('id')->on('hrim_status_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrim_overtime');
        Schema::dropIfExists('hrim_overtime_type_reference');
    }
}
