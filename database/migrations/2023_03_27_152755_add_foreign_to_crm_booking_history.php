<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignToCrmBookingHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_booking_history', function (Blueprint $table) {
            $table->foreign('booking_type_id')->references('id')->on('crm_booking_type_reference');
            $table->foreign('vehicle_type_id')->references('id')->on('crm_vehicle_type_reference');
            $table->foreign('time_slot_id')->references('id')->on('crm_timeslot');
            $table->foreign('walk_in_branch_id')->references('id')->on('branch_reference');
            $table->foreign('activity_type')->references('id')->on('crm_activity_type_management');
            $table->foreign('shipper_id')->references('id')->on('crm_customer_information');
            $table->foreign('final_status_id')->references('id')->on('crm_booking_status_reference');
            $table->foreign('booking_branch_id')->references('id')->on('branch_reference');
            $table->foreign('marketing_channel_id')->references('id')->on('crm_marketing_channel');
            $table->foreign('created_user')->references('id')->on('users');
            $table->foreign('booking_channel')->references('id')->on('crm_rate_apply_for_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_booking_history', function (Blueprint $table) {
            //
        });
    }
}
