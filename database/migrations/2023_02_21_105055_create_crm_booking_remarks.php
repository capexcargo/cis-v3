<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmBookingRemarks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_booking_remarks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('booking_id');
            $table->string('booking_reference_no');
            $table->longText('remarks');
            $table->unsignedBigInteger('status_id');
            $table->unsignedBigInteger('user_id');

            $table->timestamps();

            $table->foreign('booking_id')->references('id')->on('crm_booking');
            $table->foreign('status_id')->references('id')->on('crm_booking_status_reference');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_booking_remarks');
    }
}
