<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumnsFromTicketTicketingSystem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ticket_ticketing_system', function (Blueprint $table) {
            $table->date('actual_start_date')->nullable()->change();
            $table->date('actual_end_date')->nullable()->change();
            $table->date('target_end_date')->nullable()->change();
            $table->integer('task_holder_status')->nullable()->change();
            $table->integer('requester_status')->nullable()->change();
            $table->date('task_holder_status_date')->nullable()->change();
            $table->date('requester_status_date')->nullable()->change();
            $table->date('final_status_date')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ticket_ticketing_system', function (Blueprint $table) {
            //
        });
    }
}
