<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveSomeColunmInAccountingChechVoucher1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounting_check_voucher', function (Blueprint $table) {
            $table->dropColumn([
                'rfp_reference_no',
                'payee_id',
                'opex_type_id',
                'series',
            ]);

            $table->unsignedBigInteger('created_by')->nullable()->after('received_date');
            $table->foreign('created_by')->references('id')->on('users');
        });

        Schema::table('accounting_check_voucher_details', function (Blueprint $table) {
            $table->dropColumn([
                'reference_no',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounting_check_voucher', function (Blueprint $table) {
            //
        });
    }
}
