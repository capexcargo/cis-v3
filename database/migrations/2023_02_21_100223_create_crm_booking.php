<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmBooking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_booking', function (Blueprint $table) {
            $table->id();
            $table->string('booking_reference_no');
            $table->unsignedBigInteger('booking_type_id');
            $table->unsignedBigInteger('vehicle_type_id');
            $table->date('pickup_date');
            $table->unsignedBigInteger('time_slot_id');
            $table->unsignedBigInteger('booking_category');
            $table->unsignedBigInteger('walk_in_branch_id')->nullable();
            $table->unsignedBigInteger('activity_type');
            $table->unsignedBigInteger('shipper_id');
            $table->string('shipper_customer_no');
            $table->unsignedBigInteger('consignee_count');
            $table->longText('work_instruction')->nullable();
            $table->longText('remarks')->nullable();
            $table->unsignedBigInteger('assigned_team_id')->nullable();
            $table->unsignedBigInteger('final_status_id');
            $table->unsignedBigInteger('booking_branch_id');
            $table->unsignedBigInteger('marketing_channel_id');
            $table->unsignedBigInteger('created_user');

            $table->timestamps();

            $table->foreign('booking_type_id')->references('id')->on('crm_booking_type_reference');
            $table->foreign('vehicle_type_id')->references('id')->on('crm_vehicle_type_reference');
            $table->foreign('time_slot_id')->references('id')->on('crm_timeslot');
            $table->foreign('walk_in_branch_id')->references('id')->on('branch_reference');
            $table->foreign('activity_type')->references('id')->on('crm_activity_type_management');
            $table->foreign('shipper_id')->references('id')->on('crm_customer_information');
            $table->foreign('final_status_id')->references('id')->on('crm_booking_status_reference');
            $table->foreign('booking_branch_id')->references('id')->on('branch_reference');
            $table->foreign('marketing_channel_id')->references('id')->on('crm_marketing_channel');
            $table->foreign('created_user')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_booking');
    }
}
