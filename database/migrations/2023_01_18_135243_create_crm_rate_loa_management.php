<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmRateLoaManagement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_rate_loa_management', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('rate_category');
            $table->unsignedBigInteger('approver1_id')->nullable();
            $table->unsignedBigInteger('approver2_id')->nullable();

            $table->timestamps();

            $table->foreign('approver1_id')->references('id')->on('users');
            $table->foreign('approver2_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_rate_loa_management');
    }
}
