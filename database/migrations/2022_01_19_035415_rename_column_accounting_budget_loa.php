<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameColumnAccountingBudgetLoa extends Migration
{
    public function up()
    {
        Schema::table('accounting_budget_loa', function (Blueprint $table) {
            $table->renameColumn('limit_min', 'limit_min_amount')->nullable()->change();
            $table->renameColumn('limit_max', 'limit_max_amount')->nullable()->change();
        });
    }

    public function down()
    {
        Schema::table('accounting_budget_loa', function (Blueprint $table) {
            $table->renameColumn('limit_min_amount', 'limit_min')->nullable()->change();
            $table->renameColumn('limit_max_amount', 'limit_max')->nullable()->change();
        });
    }
}
