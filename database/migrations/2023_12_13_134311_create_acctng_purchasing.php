<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcctngPurchasing extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acctng_purchasing', function (Blueprint $table) {
            $table->id();
            $table->string('reference_no');
            $table->unsignedBigInteger('priority_level_id');
            $table->date('expected_req_start_date')->nullable();
            $table->unsignedBigInteger('service_category_id')->nullable();
            $table->date('expected_purchase_delivery_date')->nullable();
            $table->unsignedBigInteger('purchasing_type_id');
            $table->integer('requisition_final_status');
            $table->integer('canvassing_final_status');
            $table->integer('request_final_status');
            $table->integer('order_final_status');
            $table->integer('rfp_final_status');
            $table->integer('check_voucher_final_status');
            $table->dateTime('requisition_status_update_date');
            $table->dateTime('canvassing_status_update_date');
            $table->dateTime('request_status_update_date');
            $table->dateTime('order_status_update_date');
            $table->dateTime('rfp_status_update_date');
            $table->dateTime('check_voucher_status_update_date');
            $table->unsignedBigInteger('budget_id');
            $table->unsignedBigInteger('created_by');

            $table->timestamps();

            $table->foreign('priority_level_id')->references('id')->on('acctng_priority_level_reference');
            $table->foreign('service_category_id')->references('id')->on('acctng_service_category_reference');
            $table->foreign('purchasing_type_id')->references('id')->on('acctng_purchasing_type_reference');
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acctng_purchasing');
    }
}
