<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AdjustTheBreakTImeInHrimWorkSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hrim_work_schedule', function (Blueprint $table) {
            $table->renameColumn('break_time','break_from')->nullable();
            $table->time('break_to')->after('break_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hrim_work_schedule', function (Blueprint $table) {
            //
        });
    }
}
