<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColsFromAcctngBudgetTransfer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acctng_budget_transfer', function (Blueprint $table) {
            $table->renameColumn('subaccount_id_from', 'coa_from');
            $table->renameColumn('subaccount_id_to', 'coa_to');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acctng_budget_transfer', function (Blueprint $table) {
            //
        });
    }
}
