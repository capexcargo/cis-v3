<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColumnsForeignKeyFromHrimPerformanceEvaluation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hrim_performance_evaluation', function (Blueprint $table) {
            $table->dropForeign('hrim_performance_evaluation_core_values_id_foreign');
            $table->dropForeign('hrim_performance_evaluation_leadership_comp_id_foreign');

            $table->dropForeign('hrim_performance_evaluation_first_evaluator_foreign');
            $table->dropForeign('hrim_performance_evaluation_second_evaluator_foreign');
            $table->dropForeign('hrim_performance_evaluation_third_evaluator_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hrim_performance_evaluation', function (Blueprint $table) {
            //
        });
    }
}
