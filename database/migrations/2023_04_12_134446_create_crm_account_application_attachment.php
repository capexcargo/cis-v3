<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmAccountApplicationAttachment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_account_application_attachment', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('account_info_id');
            $table->unsignedBigInteger('application_requirement_id');
            $table->string('path');
            $table->string('name');
            $table->string('extension');

            $table->timestamps();

            $table->foreign('account_info_id')->references('id')->on('crm_customer_information');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_account_application_attachment');
    }
}
