<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateHrimPositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::dropIfExists('hrim_job_levels');
        Schema::create('hrim_job_levels', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->boolean('is_visible')->default(1);
            $table->unsignedBigInteger('created_by');
            
            $table->softDeletes();
            $table->timestamps();
        
            $table->foreign('created_by')->references('id')->on('users');
        });

        Schema::dropIfExists('hrim_departments');
        Schema::create('hrim_departments', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->boolean('is_visible')->default(1);
            $table->unsignedBigInteger('created_by');
            
            $table->softDeletes();
            $table->timestamps();
        
            $table->foreign('created_by')->references('id')->on('users');
        });
               
        Schema::dropIfExists('hrim_positions');
        Schema::create('hrim_positions', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->boolean('is_visible')->default(1);
            $table->unsignedBigInteger('job_level_id');
            $table->unsignedBigInteger('department_level_id');
            $table->unsignedBigInteger('division_id');
            $table->longText('job_overview');
            $table->unsignedBigInteger('created_by');
            
            $table->softDeletes();
            $table->timestamps();
        
            $table->foreign('job_level_id')->references('id')->on('hrim_job_levels');
            $table->foreign('department_level_id')->references('id')->on('hrim_departments');
            $table->foreign('division_id')->references('id')->on('division');
            $table->foreign('created_by')->references('id')->on('users');
        });
                
        Schema::dropIfExists('hrim_position_responsibilities');

        Schema::create('hrim_position_responsibilities', function (Blueprint $table) {
            $table->id();
            $table->longText('duties');
            $table->unsignedBigInteger('position_id');
            $table->unsignedBigInteger('created_by');
            
            $table->softDeletes();
            $table->timestamps();
        
            $table->foreign('position_id')->references('id')->on('hrim_positions');
            $table->foreign('created_by')->references('id')->on('users');
        });

        Schema::dropIfExists('hrim_position_job_requirements');
        Schema::create('hrim_position_job_requirements', function (Blueprint $table) {
            $table->id();
            $table->longText('code');
            $table->unsignedBigInteger('position_id');
            $table->unsignedBigInteger('created_by');
            
            $table->softDeletes();
            $table->timestamps();
            
            $table->foreign('position_id')->references('id')->on('hrim_positions');
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        Schema::dropIfExists('hrim_job_levels');
        Schema::dropIfExists('hrim_departments');
        Schema::dropIfExists('hrim_positions');
        Schema::dropIfExists('hrim_position_responsibilities');
        Schema::dropIfExists('hrim_position_job_requirements');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

    }
}
