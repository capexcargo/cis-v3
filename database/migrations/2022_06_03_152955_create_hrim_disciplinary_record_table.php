<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrimDisciplinaryRecordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('hrim_coc_section_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->boolean('is_visible')->default(1);
            $table->unsignedBigInteger('created_by');
            
            $table->softDeletes();
            $table->timestamps();
        
            $table->foreign('created_by')->references('id')->on('users');
        });

        Schema::create('hrim_violation_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->boolean('is_visible')->default(1);
            $table->unsignedBigInteger('created_by');
            
            $table->softDeletes();
            $table->timestamps();
        
            $table->foreign('created_by')->references('id')->on('users');
        });

        Schema::create('hrim_disciplinary_record_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->boolean('is_visible')->default(1);
            $table->unsignedBigInteger('created_by');
            
            $table->softDeletes();
            $table->timestamps();
        
            $table->foreign('created_by')->references('id')->on('users');
        });
        
        Schema::create('hrim_sanction_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->boolean('is_visible')->default(1);
            $table->unsignedBigInteger('created_by');
            
            $table->softDeletes();
            $table->timestamps();
        
            $table->foreign('created_by')->references('id')->on('users');
        });
        
        Schema::create('hrim_sanction_status_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->boolean('is_visible')->default(1);
            $table->unsignedBigInteger('created_by');
            
            $table->softDeletes();
            $table->timestamps();
        
            $table->foreign('created_by')->references('id')->on('users');
        });
        
        Schema::create('hrim_disciplinary_history', function (Blueprint $table) {
            $table->id();
            $table->date('incident_date');
            $table->time('incident_time');
            $table->longText('description');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('incident_branch');
            $table->unsignedBigInteger('coc_section_id');
            $table->unsignedBigInteger('violation_id');
            $table->unsignedBigInteger('disciplinary_record_id');
            $table->unsignedBigInteger('sanction_id');
            $table->unsignedBigInteger('sanction_status_id');
            $table->unsignedBigInteger('created_by');
            
            $table->softDeletes();
            $table->timestamps();
        
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('incident_branch')->references('id')->on('branch_reference');
            $table->foreign('coc_section_id')->references('id')->on('hrim_coc_section_reference');
            $table->foreign('violation_id')->references('id')->on('hrim_violation_reference');
            $table->foreign('disciplinary_record_id')->references('id')->on('hrim_disciplinary_record_reference');
            $table->foreign('sanction_id')->references('id')->on('hrim_sanction_reference');
            $table->foreign('sanction_status_id')->references('id')->on('hrim_sanction_status_reference');
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrim_disciplinary_history');
        Schema::dropIfExists('hrim_sanction_status_reference');
        Schema::dropIfExists('hrim_sanction_reference');
        Schema::dropIfExists('hrim_disciplinary_record_reference');
        Schema::dropIfExists('hrim_violation_reference');
        Schema::dropIfExists('hrim_coc_section_reference');
    }
}
