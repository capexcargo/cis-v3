<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmQuotation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_quotation', function (Blueprint $table) {
            $table->id();
            $table->string('reference_no')->nullable();
            $table->unsignedBigInteger('account_id');
            $table->string('sr_no');
            $table->longText('subject');
            $table->unsignedBigInteger('shipment_type_id');
            $table->integer('assigned_to');
            $table->boolean('is_save');

            $table->timestamps();

            $table->foreign('shipment_type_id')->references('id')->on('crm_shipment_type');
        });


        Schema::create('crm_shipment_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('quotation_id');
            $table->unsignedBigInteger('origin_id')->nullable();
            $table->unsignedBigInteger('detination_id')->nullable();
            $table->longText('origin_address')->nullable();
            $table->longText('destination_address')->nullable();
            $table->unsignedBigInteger('transhipment_id')->nullable();
            $table->unsignedBigInteger('transport_mode_id')->nullable();
            $table->unsignedBigInteger('commodity_type_id')->nullable();
            $table->unsignedBigInteger('commodity_applicable_rate_id')->nullable();
            $table->unsignedBigInteger('paymode_id')->nullable();
            $table->unsignedBigInteger('servicemode_id')->nullable();
            $table->unsignedBigInteger('service_type_id')->nullable();
            $table->integer('sea_shipment_type')->nullable();
            $table->float('total_cbm', 11, 2)->nullable();
            $table->float('total_chgwt', 11, 2)->nullable();
            $table->float('total_amount', 11, 2)->nullable();

            $table->timestamps();

            $table->foreign('quotation_id')->references('id')->on('crm_quotation');
        });


        Schema::create('crm_shipment_dimensions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('quotation_id');
            $table->float('quantity', 11, 2)->nullable();
            $table->float('weight', 11, 2)->nullable();
            $table->float('length', 11, 2)->nullable();
            $table->float('width', 11, 2)->nullable();
            $table->float('height', 11, 2)->nullable();
            $table->unsignedBigInteger('unit_of_measurement_id')->nullable();
            $table->unsignedBigInteger('measurement_type_id')->nullable();
            $table->unsignedBigInteger('type_of_packaging_id')->nullable();
            $table->boolean('is_for_crating')->nullable();
            $table->unsignedBigInteger('crating_type_id')->nullable();

            $table->integer('pouch_box_size')->nullable();
            $table->float('pouch_box_amount', 11, 2)->nullable();
            $table->integer('container')->nullable();

            $table->timestamps();

            $table->foreign('quotation_id')->references('id')->on('crm_quotation');
        });

        Schema::create('crm_shipment_charges', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('quotation_id');
            $table->float('weight_charge', 11, 2)->nullable();
            $table->float('awb_fee', 11, 2)->nullable();
            $table->float('valuation', 11, 2)->nullable();
            $table->float('cod_charge', 11, 2)->nullable();
            $table->float('insurance', 11, 2)->nullable();
            $table->float('handling_fee', 11, 2)->nullable();
            $table->float('doc_fee', 11, 2)->nullable();
            $table->boolean('is_other_fee')->nullable();

            $table->float('total_other_fee', 11, 2)->nullable();
            $table->float('oda_fee', 11, 2)->nullable();
            $table->float('opa_fee', 11, 2)->nullable();
            $table->float('equipment_rental', 11, 2)->nullable();
            $table->float('lashing', 11, 2)->nullable();
            $table->float('manpower', 11, 2)->nullable();
            $table->float('dangerous_goods_fee', 11, 2)->nullable();
            $table->float('trucking', 11, 2)->nullable();
            $table->float('perishable_fee', 11, 2)->nullable();
            $table->float('packaging_fee', 11, 2)->nullable();
            $table->float('discount_amount', 11, 2)->nullable();
            $table->float('discount_percentage', 11, 2)->nullable();
            $table->float('subtotal', 11, 2)->nullable();
            $table->float('evat', 11, 2)->nullable();
            $table->boolean('is_evat')->nullable();
            $table->float('grandtotal', 11, 2)->nullable();

            $table->timestamps();

            $table->foreign('quotation_id')->references('id')->on('crm_quotation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_quotation');
    }
}
