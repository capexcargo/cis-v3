<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkToOimsTeamRouteAssignment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('oims_team_route_assignment', function (Blueprint $table) {
            $table->foreign('branch_id')->references('id')->on('oims_branch_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('oims_team_route_assignment', function (Blueprint $table) {
            //
        });
    }
}
