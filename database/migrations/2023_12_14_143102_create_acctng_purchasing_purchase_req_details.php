<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcctngPurchasingPurchaseReqDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acctng_purchasing_purchase_req_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('reference_no_id');
            $table->date('delivery_date');
            $table->boolean('is_sample_product');
            $table->unsignedBigInteger('item_category_id');
            $table->unsignedBigInteger('item_description_id');
            $table->integer('qty');
            $table->integer('unit');
            $table->float('estimated_rate');
            $table->float('estimated_total');
            $table->unsignedBigInteger('preferred_supplier_id');
            $table->integer('purpose');
            $table->unsignedBigInteger('beneficiary_branch_id');
            $table->string('series_start_no');
            $table->string('series_end_no');
            $table->longText('remarks');
            $table->unsignedBigInteger('budget_id');
            $table->unsignedBigInteger('approver1_id');
            $table->unsignedBigInteger('approver2_id');
            $table->unsignedBigInteger('approver3_id');
            $table->integer('approver1_status');
            $table->integer('approver2_status');
            $table->integer('approver3_status');
            $table->dateTime('approver1_date');
            $table->dateTime('approver2_date');
            $table->dateTime('approver3_date');
            $table->longText('approver1_remarks');
            $table->longText('approver2_remarks');
            $table->longText('approver3_remarks');

            $table->timestamps();

            $table->foreign('reference_no_id')->references('id')->on('acctng_purchasing');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acctng_purchasing_purchase_req_details');
    }
}
