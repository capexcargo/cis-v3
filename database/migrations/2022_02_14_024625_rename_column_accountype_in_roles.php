<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameColumnAccountypeInRoles extends Migration
{
    public function up()
    {
        Schema::table('account_type_roles_reference', function (Blueprint $table) {
            $table->renameColumn('account_type_id', 'division_id');
            $table->dropForeign('account_type_roles_reference_account_type_id_foreign');
            $table->foreign('division_id')->references('id')->on('division');
        });

        Schema::table('roles_parent_access_reference', function (Blueprint $table) {
            $table->renameColumn('account_type_id', 'division_id');
            $table->dropForeign('roles_parent_access_reference_account_type_id_foreign');
            $table->foreign('division_id')->references('id')->on('division');
        });
    }

    public function down()
    {
        Schema::table('account_type_roles_reference', function (Blueprint $table) {
            //
        });

        Schema::table('roles_parent_access_reference', function (Blueprint $table) {
            //
        });
    }
}
