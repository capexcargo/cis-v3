<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCratingFeeToCrmShipmentCharges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_shipment_charges', function (Blueprint $table) {
            $table->float('crating_fee', 11, 2)->nullable()->after('equipment_rental');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_shipment_charges', function (Blueprint $table) {
            //
        });
    }
}
