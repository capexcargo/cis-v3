<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkToAcctngPurchasingLoaManagement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acctng_purchasing_loa_management', function (Blueprint $table) {
            $table->foreign('purchasing_type_id')->references('id')->on('acctng_purchasing_type_reference');
            $table->foreign('division_id')->references('id')->on('acctng_budget_source_reference');
            $table->foreign('approval1_id')->references('id')->on('users');
            $table->foreign('approval2_id')->references('id')->on('users');
            $table->foreign('approval3_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acctng_purchasing_loa_management', function (Blueprint $table) {
            //
        });
    }
}
