<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColunmInAccountingLiquidation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounting_liquidation', function (Blueprint $table) {
            $table->dropColumn([
                'request_for_payment_id',
                'opex_type_id',
                'request_total_amount',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounting_liquidation', function (Blueprint $table) {
            //
        });
    }
}
