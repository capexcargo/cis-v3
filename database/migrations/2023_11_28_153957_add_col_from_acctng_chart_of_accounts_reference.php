<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColFromAcctngChartOfAccountsReference extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acctng_chart_of_accounts_reference', function (Blueprint $table) {
            $table->integer('status')->after('account_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acctng_chart_of_accounts_reference', function (Blueprint $table) {
            //
        });
    }
}
