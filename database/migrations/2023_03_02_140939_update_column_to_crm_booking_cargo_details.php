<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumnToCrmBookingCargoDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_booking_cargo_details', function (Blueprint $table) {
            // $table->dropColumn('description_goods');
            // $table->dropColumn('declared_value');
            $table->dropForeign('crm_booking_cargo_details_transposrt_mode_id_foreign');
            $table->dropForeign('crm_booking_cargo_details_service_mode_id_foreign');
            $table->dropForeign('crm_booking_cargo_details_mode_of_payment_id_foreign');
            $table->dropColumn('transposrt_mode_id');
            $table->dropColumn('service_mode_id');
            $table->dropColumn('mode_of_payment_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_booking_cargo_details', function (Blueprint $table) {
        });
    }
}
