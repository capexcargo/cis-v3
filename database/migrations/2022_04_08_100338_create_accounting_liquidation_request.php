<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountingLiquidationRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounting_liquidation_request', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('liquidation_id');
            $table->unsignedBigInteger('rfp_id');
            $table->timestamps();

            $table->foreign('liquidation_id')->references('id')->on('accounting_liquidation');
            $table->foreign('rfp_id')->references('id')->on('accounting_request_for_payment');
        });

        Schema::create('accounting_liquidation_details_request', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('ld_id');
            $table->unsignedBigInteger('rfp_id');
            $table->timestamps();

            $table->foreign('ld_id')->references('id')->on('accounting_liquidation_details');
            $table->foreign('rfp_id')->references('id')->on('accounting_request_for_payment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounting_liquidation_request');
    }
}
