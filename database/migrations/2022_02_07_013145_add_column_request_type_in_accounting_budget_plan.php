<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnRequestTypeInAccountingBudgetPlan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounting_budget_plan', function (Blueprint $table) {
           $table->unsignedBigInteger('rfp_type_id')->after('id')->nullable();

           $table->foreign('rfp_type_id')->references('id')->on('accounting_rfp_type_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounting_budget_plan', function (Blueprint $table) {
            //
        });
    }
}
