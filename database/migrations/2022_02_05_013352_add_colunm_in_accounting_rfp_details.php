<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColunmInAccountingRfpDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounting_freight_usage_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->string('display')->nullable();
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::table('accounting_rfp_details', function (Blueprint $table) {

            $table->dropForeign('accounting_rfp_details_trucking_category_id_foreign');
            
        });
    }

   
    public function down()
    {
        Schema::table('accounting_rfp_details', function (Blueprint $table) {
            //
        });
    }
}
