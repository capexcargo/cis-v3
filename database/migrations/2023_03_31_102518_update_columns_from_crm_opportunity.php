<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumnsFromCrmOpportunity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_opportunity', function (Blueprint $table) {
            $table->string('sr_no')->change();
            $table->string('sales_stage_id')->nullable()->change();
            $table->string('booking_reference')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_opportunity', function (Blueprint $table) {
            //
        });
    }
}
