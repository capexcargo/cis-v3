<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkToBranchReferencesQuadrant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('branch_references_quadrant', function (Blueprint $table) {
            $table->foreign('area_id')->references('id')->on('oims_area_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('branch_references_quadrant', function (Blueprint $table) {
            //
        });
    }
}
