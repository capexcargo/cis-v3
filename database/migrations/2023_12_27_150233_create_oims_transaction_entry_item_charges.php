<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOimsTransactionEntryItemCharges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oims_transaction_entry_item_charges', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('waybill_id');
            $table->float('weight_charge');
            $table->float('awb_fee');
            $table->float('declared_value');
            $table->float('valuation');
            $table->float('cod_charge')->nullable();
            $table->float('insurance')->nullable();
            $table->float('handling_fee')->nullable();
            $table->float('doc_fee')->nullable();
            $table->boolean('is_others_fee')->nullable();
            $table->float('opa_fee')->nullable();
            $table->float('oda_fee')->nullable();
            $table->float('crating_fee')->nullable();
            $table->float('equipment_rental')->nullable();
            $table->float('lashing')->nullable();
            $table->float('manpower')->nullable();
            $table->float('dangerous_goods_fee')->nullable();
            $table->float('trucking')->nullable();
            $table->float('perishable_fee')->nullable();
            $table->float('packaging_fee')->nullable();
            $table->float('subtotal')->nullable();
            $table->float('rate_discount')->nullable();
            $table->string('promo_code')->nullable();
            $table->float('promo_discount')->nullable();
            $table->float('evat')->nullable();
            $table->float('grand_total')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oims_transaction_entry_item_charges');
    }
}
