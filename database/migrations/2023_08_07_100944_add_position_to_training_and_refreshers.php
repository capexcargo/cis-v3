<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPositionToTrainingAndRefreshers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('training_and_refreshers', function (Blueprint $table) {
            $table->integer('position')->nullable()->after('tagged_division');
            $table->integer('tagged_division')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('training_and_refreshers', function (Blueprint $table) {
            //
        });
    }
}
