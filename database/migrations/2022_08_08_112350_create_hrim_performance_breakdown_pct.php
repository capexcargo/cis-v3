<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrimPerformanceBreakdownPct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrim_performance_breakdown_pct', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('job_level_id');
            $table->integer('goals');
            $table->integer('core_values');
            $table->integer('leadership_competencies');

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('job_level_id')->references('id')->on('hrim_job_levels');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrim_performance_breakdown_pct');
    }
}
