<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFinalScoreToHrimPerformanceEvaluation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hrim_performance_evaluation', function (Blueprint $table) {
            $table->float('final_score', 5, 2)->nullable()->after('overall_total_score');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hrim_performance_evaluation', function (Blueprint $table) {
            //
        });
    }
}
