<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcctngBudgetAvailment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acctng_budget_availment', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('requisition_id');
            $table->unsignedBigInteger('budget_subaccount_id');
            $table->float('request_amount');
            $table->float('accounting_savings');
            $table->float('division_savings');
            $table->string('request_month');
            $table->integer('year');

            $table->timestamps();

            $table->foreign('requisition_id')->references('id')->on('acctng_purchasing');
            $table->foreign('budget_subaccount_id')->references('id')->on('acctng_budget_management');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acctng_budget_availment');
    }
}
