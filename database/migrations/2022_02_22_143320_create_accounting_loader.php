<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountingLoader extends Migration
{
    public function up()
    {
        Schema::create('accounting_loaders_type_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::create('accounting_loaders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('loaders_type_id');
            $table->string('code');
            $table->string('name');
            $table->string('address')->nullable();
            $table->boolean('is_visible')->default(1);
            $table->timestamps();

            $table->foreign('loaders_type_id')->references('id')->on('accounting_loaders_type_reference');
        });

        Schema::table('accounting_rfp_details', function (Blueprint $table) {
            $table->unsignedBigInteger('loaders_id')->after('amount')->nullable();

            $table->foreign('loaders_id')->references('id')->on('accounting_loaders');
        });
    }

    public function down()
    {
        Schema::dropIfExists('accounting_loaders_type_reference');
        Schema::dropIfExists('accounting_loaders');
    }
}
