<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOimsTransactionEntryItemDetailsSummary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oims_transaction_entry_item_details_summary', function (Blueprint $table) {
            $table->id();
            $table->string('waybill_id');
            $table->float('total_qty');
            $table->float('total_weight');
            $table->float('total_dimension');
            $table->float('total_container')->nullable();
            $table->float('total_cbm')->nullable();
            $table->float('total_cwt')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oims_transaction_entry_item_details_summary');
    }
}
