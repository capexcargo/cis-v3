<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSoftDeleteInAccountingLiquidation extends Migration
{

    public function up()
    {
        Schema::table('accounting_liquidation', function (Blueprint $table) {
            $table->softDeletes()->after('status_id');
        });

        Schema::table('accounting_liquidation_details', function (Blueprint $table) {
            $table->softDeletes()->after('created_by');
        });

        Schema::table('accounting_liquidation_details_request', function (Blueprint $table) {
            $table->softDeletes()->after('rfp_id');
        });

        Schema::table('accounting_liquidation_request', function (Blueprint $table) {
            $table->softDeletes()->after('rfp_id');
        });
    }


    public function down()
    {
        Schema::table('accounting_liquidation', function (Blueprint $table) {
            //
        });
    }
}
