<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsVisibleHrimPositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hrim_job_levels', function (Blueprint $table) {
            $table->boolean('is_visible')->default(1)->after('display');
        });

        Schema::table('hrim_departments', function (Blueprint $table) {
            $table->boolean('is_visible')->default(1)->after('display');
        });

        Schema::table('hrim_positions', function (Blueprint $table) {
            $table->boolean('is_visible')->default(1)->after('display');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
