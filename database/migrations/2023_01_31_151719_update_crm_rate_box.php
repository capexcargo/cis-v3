<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCrmRateBox extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_rate_box', function (Blueprint $table) {
            $table->dropForeign('crm_rate_box_approver1_id_foreign');
            $table->dropForeign('crm_rate_box_approver2_id_foreign');
        });

        Schema::table('crm_rate_box', function (Blueprint $table) {
            $table->foreign('approver1_id')->references('id')->on('users');
            $table->foreign('approver2_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
