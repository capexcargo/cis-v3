<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakeTheCheckVoucherApproverNullable extends Migration
{

    public function up()
    {
        Schema::table('accounting_check_voucher', function (Blueprint $table) {
            $table->unsignedBigInteger('approver_1_id')->nullable()->change();
            $table->unsignedBigInteger('approver_2_id')->nullable()->change();
            $table->date('approver_1_date')->nullable()->change();
            $table->date('approver_2_date')->nullable()->change();
            $table->longText('approver_1_remarks')->nullable()->change();
            $table->longText('approver_2_remarks')->nullable()->change();
        });
    }


    public function down()
    {
        Schema::table('accounting_check_voucher', function (Blueprint $table) {
            $table->unsignedBigInteger('approver_1_id')->change();
            $table->unsignedBigInteger('approver_2_id')->change();
            $table->date('approver_1_date')->change();
            $table->date('approver_2_date')->change();
            $table->longText('approver_1_remarks')->change();
            $table->longText('approver_2_remarks')->change();
        });
    }
}
