<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmBookingShipper extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_booking_shipper', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('booking_id');
            $table->string('customer_no');
            $table->unsignedBigInteger('account_type_id');
            $table->string('company_name')->nullable();
            $table->string('name');
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name');
            $table->string('mobile_number');
            $table->string('email_address');
            $table->longText('address');

            $table->timestamps();
            
            $table->foreign('booking_id')->references('id')->on('crm_booking');
            $table->foreign('account_type_id')->references('id')->on('crm_account_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_booking_shipper');
    }
}
