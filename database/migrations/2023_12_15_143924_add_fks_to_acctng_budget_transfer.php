<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFksToAcctngBudgetTransfer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acctng_budget_transfer', function (Blueprint $table) {
            $table->foreign('coa_from')->references('id')->on('acctng_budget_coa_reference');
            $table->foreign('coa_to')->references('id')->on('acctng_budget_coa_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acctng_budget_transfer', function (Blueprint $table) {
            //
        });
    }
}
