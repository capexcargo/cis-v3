<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToCrmBookingLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_booking_logs', function (Blueprint $table) {
            $table->unsignedBigInteger('cancel_reason')->nullable()->after('status_id');
            $table->unsignedBigInteger('resched_reason')->nullable()->after('cancel_reason');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_booking_logs', function (Blueprint $table) {
            //
        });
    }
}
