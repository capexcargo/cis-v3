<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToUserDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_details', function (Blueprint $table) {
            $table->string('agency_contact_person')->nullable()->after('permanent_address');
            $table->string('agency_contact_no')->nullable()->after('agency_contact_person');
            $table->string('agency_position')->nullable()->after('agency_contact_no');
            $table->integer('agency_type')->nullable()->after('agency_contact_no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_details', function (Blueprint $table) {
            //
        });
    }
}
