<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropLiquidationIdInAccountingLiquidationAttachments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounting_liquidation_attachments', function (Blueprint $table) {
            $table->dropColumn([
                'liquidation_id',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounting_liquidation_attachments', function (Blueprint $table) {
            //
        });
    }
}
