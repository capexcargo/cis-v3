<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateFamilyRelationShip extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hrim_family_information', function (Blueprint $table) {
            $table->dropForeign('hrim_family_information_siblings_id_foreign');
            $table->dropForeign('hrim_family_information_children_id_foreign');
        });

        Schema::table('hrim_family_information', function (Blueprint $table) {
            $table->dropColumn(['siblings_id', 'children_id']);
        });

        Schema::table('hrim_family_information_sibling', function (Blueprint $table) {
            $table->unsignedBigInteger('family_information_id')->after('id');

            $table->foreign('family_information_id')->references('id')->on('hrim_family_information');
        });
        Schema::table('hrim_family_information_children', function (Blueprint $table) {
            $table->unsignedBigInteger('family_information_id')->after('id');

            $table->foreign('family_information_id')->references('id')->on('hrim_family_information');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
