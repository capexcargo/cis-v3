<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmQuotaCsmDistributionType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_quota_csm_distribution_type', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('csm_user_id');
            $table->unsignedBigInteger('type_id');
            $table->float('distribution_pct', 11, 2);
            $table->float('quota_amount', 11, 2);
            $table->timestamps();

            $table->foreign('csm_user_id')->references('id')->on('users');
            $table->foreign('type_id')->references('id')->on('crm_quota_account_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_quota_csm_distribution_type');
    }
}
