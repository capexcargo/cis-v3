<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcctngPurchasingLoaManagement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acctng_purchasing_loa_management', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('purchasing_type_id');
            $table->unsignedBigInteger('division_id');
            $table->string('min_amount');
            $table->integer('max_amount');
            $table->unsignedBigInteger('approval1_id');
            $table->unsignedBigInteger('approval2_id');
            $table->unsignedBigInteger('approval3_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acctng_purchasing_loa_management');
    }
}
