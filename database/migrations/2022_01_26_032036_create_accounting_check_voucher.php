<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountingCheckVoucher extends Migration
{
    public function up()
    {
        Schema::create('accounting_status_reference', function (Blueprint $table) {
            $table->id();
            $table->string('type')->default(1);
            $table->string('code')->nullable();
            $table->string('display')->nullable();
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::create('accounting_check_voucher', function (Blueprint $table) {
            $table->id();
            $table->string('cv_no');
            $table->string('reference_no');
            $table->string('rfp_reference_no');
            $table->unsignedBigInteger('payee_id');
            $table->float('total_amount', 11, 2);
            $table->longText('description');
            $table->unsignedBigInteger('opex_type_id');
            $table->unsignedBigInteger('status_id');
            $table->unsignedBigInteger('approver_1_id');
            $table->unsignedBigInteger('approver_2_id');
            $table->date('approver_1_date');
            $table->date('approver_2_date');
            $table->longText('approver_1_remarks')->nullable();
            $table->longText('approver_2_remarks')->nullable();
            $table->string('series')->nullable();
            $table->date('release_date')->nullable();
            $table->unsignedBigInteger('received_by')->nullable();
            $table->date('received_date')->nullable();
            $table->timestamps();

            $table->foreign('payee_id')->references('id')->on('accounting_supplier');
            $table->foreign('opex_type_id')->references('id')->on('accounting_opex_type');
            $table->foreign('status_id')->references('id')->on('accounting_status_reference');
            $table->foreign('approver_1_id')->references('id')->on('users');
            $table->foreign('approver_2_id')->references('id')->on('users');
            $table->foreign('received_by')->references('id')->on('users');

        });

        Schema::create('accounting_check_voucher_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('check_voucher_id');
            $table->string('reference_no');
            $table->string('voucher_no');
            $table->longText('description');
            $table->date('date');
            $table->float('amount', 11, 2);
            $table->timestamps();

            $table->foreign('check_voucher_id')->references('id')->on('accounting_check_voucher');
        });
    }

    public function down()
    {
        Schema::dropIfExists('accounting_check_voucher_details');
        Schema::dropIfExists('accounting_check_voucher');
        Schema::dropIfExists('accounting_status_reference');
    }
}
