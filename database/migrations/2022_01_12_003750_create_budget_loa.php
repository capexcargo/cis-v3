<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBudgetLoa extends Migration
{
    
    public function up()
    {
        Schema::create('accounting_budget_loa', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('budget_plan_id');
            $table->float('limit_min', 11, 2)->nullable();
            $table->float('limit_max', 11, 2)->nullable();
            $table->unsignedBigInteger('first_approver_id')->nullable();
            $table->unsignedBigInteger('second_approver_id')->nullable();
            $table->unsignedBigInteger('third_approver_id')->nullable();
            $table->timestamps();

            $table->foreign('budget_plan_id')->references('id')->on('accounting_budget_plan');
            $table->foreign('first_approver_id')->references('id')->on('users');
            $table->foreign('second_approver_id')->references('id')->on('users');
            $table->foreign('third_approver_id')->references('id')->on('users');
        });
    }

    public function down()
    {
        Schema::dropIfExists('accounting_budget_loa');
    }
}
