<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrimLoanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrim_loan_type', function (Blueprint $table) {
            $table->id();
            $table->string('name');

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('hrim_loan', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user_id');
            $table->string('reference_no');
            $table->unsignedBigInteger('type');
            $table->float('principal_amount');
            $table->integer('interest');
            $table->integer('terms');
            $table->float('term_type');
            $table->date('start_date');
            $table->date('end_date');
            $table->longtext('purpose')->nullable();
            $table->unsignedBigInteger('first_approver')->nullable();;
            $table->unsignedBigInteger('second_approver')->nullable();;
            $table->unsignedBigInteger('third_approver')->nullable();;
            $table->integer('first_status')->nullable();;
            $table->integer('second_status')->nullable();;
            $table->integer('third_status')->nullable();;
            $table->integer('final_status')->nullable();;

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('type')->references('id')->on('hrim_loan_type');
            $table->foreign('first_approver')->references('first_approver')->on('hrim_loa_management');
            $table->foreign('second_approver')->references('second_approver')->on('hrim_loa_management');
            $table->foreign('third_approver')->references('third_approver')->on('hrim_loa_management');

        });

        Schema::create('hrim_loan_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('loan_id');
            $table->integer('year');
            $table->integer('month');
            $table->integer('cutoff');
            $table->float('amount');
            $table->integer('interest');
            $table->integer('status');

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('loan_id')->references('id')->on('hrim_loan');

        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrim_loan_details');
        Schema::dropIfExists('hrim_loan');
        Schema::dropIfExists('hrim_loan_type');
    }
}
