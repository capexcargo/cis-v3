<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmSrEmailDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_sr_email_details', function (Blueprint $table) {
            $table->id();
            $table->string('sr_no');
            $table->string('email_external');
            $table->string('email_internal');
            $table->longText('body');
            $table->integer('status');
            $table->integer('type');
            
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_sr_email_details');
    }
}
