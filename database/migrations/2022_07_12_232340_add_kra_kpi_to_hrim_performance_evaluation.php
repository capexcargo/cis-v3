<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddKraKpiToHrimPerformanceEvaluation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hrim_performance_evaluation', function (Blueprint $table) {
            $table->unsignedBigInteger('kra_id')->nullable()->after('position_id');
            $table->unsignedBigInteger('kpi_id')->nullable()->after('kra_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hrim_performance_evaluation', function (Blueprint $table) {
            //
        });
    }
}
