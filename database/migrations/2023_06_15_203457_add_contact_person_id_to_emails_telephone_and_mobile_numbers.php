<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddContactPersonIdToEmailsTelephoneAndMobileNumbers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_email_address_list', function (Blueprint $table) {
            $table->unsignedBigInteger('contact_person_id')->after('id')->nullable();
        });

        Schema::table('crm_telephone_list', function (Blueprint $table) {
            $table->unsignedBigInteger('contact_person_id')->after('id')->nullable();
        });

        Schema::table('crm_mobile_list', function (Blueprint $table) {
            $table->unsignedBigInteger('contact_person_id')->after('id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_mobile_list', function (Blueprint $table) {
            //
        });

        Schema::table('crm_telephone_list', function (Blueprint $table) {
            //
        });

        Schema::table('crm_email_address_list', function (Blueprint $table) {
            //
        });
    }
}
