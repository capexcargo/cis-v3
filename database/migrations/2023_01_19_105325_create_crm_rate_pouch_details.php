<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmRatePouchDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_rate_pouch_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('pouch_id');
            $table->unsignedBigInteger('origin_id');
            $table->unsignedBigInteger('destination_id');
            $table->float('amount_small')->nullable();
            $table->float('amount_medium')->nullable();
            $table->float('amount_large')->nullable();

            $table->timestamps();

            $table->foreign('pouch_id')->references('id')->on('crm_rate_pouch');
            $table->foreign('origin_id')->references('id')->on('branch_reference');
            $table->foreign('destination_id')->references('id')->on('branch_reference');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_rate_pouch_details');
    }
}
