<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcctngBudgetTransfer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acctng_budget_transfer', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('source_id_from');
            $table->unsignedBigInteger('source_id_to');
            $table->unsignedBigInteger('subaccount_id_from');
            $table->unsignedBigInteger('subaccount_id_to');
            $table->string('month_from');
            $table->string('month_to');
            $table->integer('year');
            $table->float('transfer_amount');
            $table->integer('transfer_type');
            $table->unsignedBigInteger('transferred_by');

            $table->timestamps();

            $table->foreign('source_id_from')->references('id')->on('acctng_budget_source_reference');
            $table->foreign('source_id_to')->references('id')->on('acctng_budget_source_reference');
            $table->foreign('subaccount_id_from')->references('id')->on('acctng_budget_management');
            $table->foreign('subaccount_id_to')->references('id')->on('acctng_budget_management');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acctng_budget_transfer');
    }
}
