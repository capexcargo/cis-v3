<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrimApplicantTrackingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrim_applicant_tracking_status', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->unsignedBigInteger('created_by');

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('created_by')->references('id')->on('users');
        });

        Schema::create('hrim_applicant_tracking', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('erf_reference_no_id');
            $table->string('firstname');
            $table->string('middlename');
            $table->string('lastname');
            $table->date('date_applied');
            $table->longText('hr_notes');
            $table->longText('hiring_manager_notes');
            $table->longText('gm_notes');
            $table->unsignedBigInteger('status_id');
            $table->longText('remarks');
            $table->date('2nd_approval_date');

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('erf_reference_no_id')->references('id')->on('hrim_employee_requisition');
        });

        Schema::create('hrim_applicant_tracking_log', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('erf_reference_no_id');
            $table->date('date');
            $table->unsignedBigInteger('status');
            $table->longText('remarks');
            $table->boolean('type');
            $table->unsignedBigInteger('created_by');

            $table->softDeletes();
            $table->timestamps();

            // $table->foreign('erf_reference_no_id')->references('id')->on('hrim_applicant_tracking');
            // $table->foreign('status')->references('id')->on('hrim_applicant_tracking_status');
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrim_applicant_tracking');
        Schema::dropIfExists('hrim_applicant_tracking_status');
        Schema::dropIfExists('hrim_applicant_tracking_log');
    }
}
