<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAddColsToAcctngPurchasingServiceReqDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acctng_purchasing_service_req_details', function (Blueprint $table) {
            $table->unsignedBigInteger('reason_id')->nullable()->after('location_id');

            $table->foreign('reason_id')->references('id')->on('acctng_reason_for_rejection_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acctng_purchasing_service_req_details', function (Blueprint $table) {
            //
        });
    }
}
