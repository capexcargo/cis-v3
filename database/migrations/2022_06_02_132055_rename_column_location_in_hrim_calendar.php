<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameColumnLocationInHrimCalendar extends Migration
{

    public function up()
    {
        Schema::table('hrim_calendar', function (Blueprint $table) {
            $table->dropForeign('hrim_calendar_location_id_foreign');
        });

        Schema::table('hrim_calendar', function (Blueprint $table) {
            $table->foreign('location_id')->references('id')->on('branch_reference');
        });

        Schema::table('hrim_calendar', function (Blueprint $table) {
            $table->renameColumn('location_id', 'branch_id')->after('id');
        });
    }

    public function down()
    {
    }
}
