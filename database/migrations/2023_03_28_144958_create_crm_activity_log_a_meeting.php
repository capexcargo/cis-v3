<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmActivityLogAMeeting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_activity_log_a_meeting', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('customer_id');
            $table->unsignedBigInteger('outcome_id');
            $table->dateTime('datetime');
            $table->longText('notes');
            $table->string('sr_no');

            $table->timestamps();

            $table->foreign('customer_id')->references('id')->on('crm_customer_information');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_activity_log_a_meeting');
    }
}
