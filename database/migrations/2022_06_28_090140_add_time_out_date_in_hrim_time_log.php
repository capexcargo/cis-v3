<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTimeOutDateInHrimTimeLog extends Migration
{
    
    public function up()
    {
        Schema::table('hrim_time_log', function (Blueprint $table) {
            $table->date('time_out_date')->after('time_out')->nullable();
        });
    }

    public function down()
    {
        Schema::table('hrim_time_log', function (Blueprint $table) {
            //
        });
    }
}
