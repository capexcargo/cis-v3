<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOimsTransactionEntryItemDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oims_transaction_entry_item_details', function (Blueprint $table) {
            $table->id();
            $table->string('waybill_id');
            $table->float('qty');
            $table->float('weight');
            $table->float('length');
            $table->float('width');
            $table->float('height');
            $table->unsignedBigInteger('unit_of_measurement_id');
            $table->unsignedBigInteger('measurement_type_id');
            $table->unsignedBigInteger('type_of_packaging_id');
            $table->boolean('is_for_crating');
            $table->integer('crating_status');
            $table->unsignedBigInteger('crating_type_id');
            $table->float('cwt')->nullable();
            $table->float('cbm')->nullable();
            $table->float('container')->nullable();
            $table->integer('rate_id_applied')->nullable();
            $table->integer('cargotype');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oims_transaction_entry_item_details');
    }
}
