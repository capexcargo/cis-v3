<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class HrimCreateCalendar extends Migration
{

    public function up()
    {
        Schema::create('hrim_province', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::create('hrim_event_type_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::create('hrim_calendar', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('location_id');
            $table->unsignedBigInteger('event_type_id');
            $table->dateTime('event_date_time');
            $table->string('title');
            $table->longText('description');
            $table->unsignedBigInteger('created_by');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('location_id')->references('id')->on('hrim_province');
            $table->foreign('event_type_id')->references('id')->on('hrim_event_type_reference');
            $table->foreign('created_by')->references('id')->on('users');
        });
    }


    public function down()
    {
        Schema::dropIfExists('hrim_calendar');
        Schema::dropIfExists('hrim_event_type_reference');
        Schema::dropIfExists('hrim_province');
    }
}
