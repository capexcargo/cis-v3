<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveSomeColunmInAccountingChechVoucher extends Migration
{
    
    public function up()
    {
        Schema::table('accounting_check_voucher', function (Blueprint $table) {
            $table->dropForeign('accounting_check_voucher_payee_id_foreign');
            $table->dropForeign('accounting_check_voucher_opex_type_id_foreign');
        });
    }

    public function down()
    {
        Schema::table('accounting_check_voucher', function (Blueprint $table) {
            //
        });
    }
}
