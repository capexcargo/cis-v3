<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Canvassing extends Migration
{
    public function up()
    {
        Schema::create('accounting_canvassing', function (Blueprint $table) {
            $table->id();
            $table->string('reference_id')->nullable();
            $table->unsignedBigInteger('created_by');
            $table->timestamps();

            $table->foreign('created_by')->references('id')->on('users');
        });

        Schema::create('accounting_canvassing_supplier', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('canvassing_id');
            $table->unsignedBigInteger('supplier_id');
            $table->timestamps();

            $table->foreign('canvassing_id')->references('id')->on('accounting_canvassing');
            $table->foreign('supplier_id')->references('id')->on('accounting_supplier');
        });

        Schema::create('accounting_cvs_supplier_item', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('canvassing_supplier_id');
            $table->unsignedBigInteger('supplier_item_id');
            $table->float('price', 11, 2);
            $table->boolean('status')->default(0)->comment('0) Not Approved 1) Approved');
            $table->timestamps();

            $table->foreign('canvassing_supplier_id')->references('id')->on('accounting_canvassing_supplier');
            $table->foreign('supplier_item_id')->references('id')->on('accounting_supplier_item');
        });
    }

    public function down()
    {
        Schema::dropIfExists('accounting_canvassing');
        Schema::dropIfExists('accounting_canvassing_supplier');
        Schema::dropIfExists('accounting_cvs_supplier_item');
    }
}
