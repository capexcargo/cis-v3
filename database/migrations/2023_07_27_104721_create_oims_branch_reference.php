<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOimsBranchReference extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oims_branch_reference', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('region');
            $table->string('name');
            $table->longText('address');
            $table->unsignedBigInteger('status');
            $table->timestamps();

            $table->foreign('region')->references('id')->on('region');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oims_branch_reference');
    }
}
