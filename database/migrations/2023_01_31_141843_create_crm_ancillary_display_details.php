<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmAncillaryDisplayDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_ancillary_display_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('ancillary_display_id');
            $table->unsignedBigInteger('ancillary_charge_id');
            $table->timestamps();

            $table->foreign('ancillary_display_id')->references('id')->on('crm_ancillary_display_management');
            $table->foreign('ancillary_charge_id')->references('id')->on('crm_ancillary_management');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_ancillary_display_details');
    }
}
