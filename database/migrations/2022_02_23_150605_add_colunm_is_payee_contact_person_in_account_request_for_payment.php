<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColunmIsPayeeContactPersonInAccountRequestForPayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounting_request_for_payment', function (Blueprint $table) {
            $table->boolean('is_payee_contact_person')->default(0)->nullable()->after('type_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounting_request_for_payment', function (Blueprint $table) {
            //
        });
    }
}
