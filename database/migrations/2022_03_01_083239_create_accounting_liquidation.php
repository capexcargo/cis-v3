<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountingLiquidation extends Migration
{
    public function up()
    {
        Schema::create('accounting_liquidation_status_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::create('accounting_liquidation', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('request_for_payment_id');
            $table->string('liquidation_reference_no')->nullable();
            $table->unsignedBigInteger('opex_type_id')->nullable();
            $table->float('request_total_amount', 11, 2)->nullable();
            $table->float('liquidation_total_amount', 11, 2)->nullable();
            $table->unsignedBigInteger('status_id')->nullable();
            $table->timestamps();

            $table->foreign('request_for_payment_id')->references('id')->on('accounting_request_for_payment');
            $table->foreign('opex_type_id')->references('id')->on('accounting_opex_type');
            $table->foreign('status_id')->references('id')->on('accounting_liquidation_status_reference');
        });

        Schema::create('accounting_liquidation_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('liquidation_id');
            $table->string('or_no')->nullable();
            $table->date('transaction_date')->nullable();
            $table->float('amount', 11, 2)->nullable();
            $table->float('cash_on_hand', 11, 2)->nullable();
            $table->longText('description')->nullable();
            $table->longText('remarks')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->timestamps();

            $table->foreign('liquidation_id')->references('id')->on('accounting_liquidation');
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    public function down()
    {
        Schema::dropIfExists('accounting_liquidation_details');
        Schema::dropIfExists('accounting_liquidation');
        Schema::dropIfExists('accounting_liquidation_status_reference');
    }
}
