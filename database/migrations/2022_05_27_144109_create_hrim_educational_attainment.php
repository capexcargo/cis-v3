<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrimEducationalAttainment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrim_ea_level_reference', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('display');
            $table->boolean('is_visible')->default(1);
            $table->timestamps();
        });

        Schema::create('hrim_educational_attainment', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('ea_level_id');
            $table->string('educational_institution');
            $table->integer('inclusive_year_from');
            $table->integer('inclusive_year_to');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('ea_level_id')->references('id')->on('hrim_ea_level_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrim_educational_attainment');
        Schema::dropIfExists('hrim_ea_level_reference');
    }
}
