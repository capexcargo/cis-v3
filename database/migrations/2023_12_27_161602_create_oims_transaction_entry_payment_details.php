<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOimsTransactionEntryPaymentDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oims_transaction_entry_payment_details', function (Blueprint $table) {
            $table->id();
            $table->string('waybill_id');
            $table->boolean('is_single_waybill');
            $table->boolean('is_partial_payment');
            $table->integer('partial_payment_status');
            $table->unsignedBigInteger('payment_type');
            $table->float('amount');
            $table->string('or/ar')->nullable();
            $table->integer('payor')->nullable();
            $table->integer('payor_mobile_no')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oims_transaction_entry_payment_details');
    }
}
