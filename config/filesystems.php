<?php

return [

    'image_type' => [
        'jpg', 'jpeg', 'png'
    ],

    'file_type' => [
        'xlsx', 'doc', 'docx', 'csv', 'pdf', 'txt'
    ],

    'video_type' => [
        'mp4', 'mov'
    ],

    'validation_all' => 'mimes:jpg,jpeg,png,xlsx,doc,docx,csv,pdf',
    'crm_sales_campaign_validation' => 'mimes:jpg,jpeg,png,xlsx,doc,docx,csv,pdf,txt',
    // 'crm_sales_booking_mgmt_validation' => 'mimes:jpg,jpeg,png,xlsx,doc,docx,csv,pdf,txt',
    'crm_sales_booking_mgmt_validation' => 'mimes:jpg,jpeg,png,pdf',
    'validation_file' => 'mimes:xlsx,doc,docx,csv,pdf',
    'validation_image' => 'mimes:jpg,jpeg,png',
    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL') . '/storage',
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
            'url' => env('AWS_URL'),
            'endpoint' => env('AWS_ENDPOINT'),
            'use_path_style_endpoint' => env('AWS_USE_PATH_STYLE_ENDPOINT', false),
        ],

        'accounting_gcs' => [
            'driver' => 'gcs',
            'project_id' => env('GOOGLE_CLOUD_PROJECT_ID', 'your-project-id'),
            'key_file' => env('GOOGLE_CLOUD_KEY_FILE', null), // optional: /path/to/service-account.json
            'bucket' => env('ACCOUNTING_GC_STORAGE_BUCKET', 'your-bucket'),
            'visibility' => 'public'
        ],

        'hrim_gcs' => [
            'driver' => 'gcs',
            'project_id' => env('GOOGLE_CLOUD_PROJECT_ID', 'your-project-id'),
            'key_file' => env('GOOGLE_CLOUD_KEY_FILE', null), // optional: /path/to/service-account.json
            'bucket' => env('HRIM_GC_STORAGE_BUCKET', 'cisv3-hrim-live'),
            'visibility' => 'public'
        ],
        
        'hrim_gcs_icons' => [
            'driver' => 'gcs',
            'project_id' => env('GOOGLE_CLOUD_PROJECT_ID', 'your-project-id'),
            'key_file' => env('GOOGLE_CLOUD_KEY_FILE', null), // optional: /path/to/service-account.json
            'bucket' => env('HRIM_GC_STORAGE_BUCKET', 'cisv3-hrim-stag'),
            'visibility' => 'public'
        ],

        'ticket_gcs' => [
            'driver' => 'gcs',
            'project_id' => env('GOOGLE_CLOUD_PROJECT_ID', 'your-project-id'),
            'key_file' => env('GOOGLE_CLOUD_KEY_FILE', null), // optional: /path/to/service-account.json
            'bucket' => env('TICKETING_GC_STORAGE_BUCKET', 'your-bucket'),
            'visibility' => 'public'
        ],

        'crm_gcs' => [
            'driver' => 'gcs',
            'project_id' => env('GOOGLE_CLOUD_PROJECT_ID', 'your-project-id'),
            'key_file' => env('GOOGLE_CLOUD_KEY_FILE', null), // optional: /path/to/service-account.json
            'bucket' => env('CRM_GC_STORAGE_BUCKET', 'cisv3-crm-stag'),
            'visibility' => 'public'
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Symbolic Links
    |--------------------------------------------------------------------------
    |
    | Here you may configure the symbolic links that will be created when the
    | `storage:link` Artisan command is executed. The array keys should be
    | the locations of the links and the values should be their targets.
    |
    */

    'links' => [
        public_path('storage') => storage_path('app/public'),
    ],

];
