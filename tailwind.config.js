const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    mode: 'jit',
    purge: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './vendor/laravel/jetstream/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
    ],

    theme: {
        extend: {
            fontFamily: {
                sans: ['Nunito', ...defaultTheme.fontFamily.sans],
            },
            backgroundImage: theme => ({
                'capex': "url('/images/capex-bg.jpg')",
                'login': "url('/images/login-bg.jpg')",
            }),
            colors: {
                'requested': '#FF8800',
                'pending': '#FFCE48',
                'approved': '#003399',
                'declined': '#FF0000',
            },
        },
        fontSize: {
            '1xs': ['0.7rem', '1rem'],
            'xs': ['0.75rem', '1rem'],
            'sm': ['0.875rem', '1.25rem'],
            'base': ['1rem', '1.5rem'],
            'lg': ['1.125rem', '1.75rem'],
            'xl': ['1.25rem', '1.75rem'],
            '2xl': ['1.5rem', '2rem'],
            '3xl': ['1.875rem', '2.25rem'],
            '4xl': ['2.25rem', '2.5rem'],
            '5xl': ['3rem', '1'],
            '6xl': ['3.75rem', '1'],
            '7xl': ['4.5rem', '1'],
            '8xl': ['6rem', '1'],
            '9xl': ['8rem', '1'],
        },
        // fontSize: {
        //     'xs': '.75rem',
        //     'sm': '.875rem',
        //     'tiny': '.875rem',
        //     'base': '1rem',
        //     'lg': '1.125rem',
        //     'xl': '1.25rem',
        //     '2xl': '1.5rem',
        //     '3xl': '1.875rem',
        //     '4xl': '2.25rem',
        //     '5xl': '3rem',
        //     '6xl': '4rem',
        //     '7xl': '5rem',
        // }

        // screens: {

        //     'sm': '576px',
        //     // => @media (min-width: 640px) { ... }

        //     'md': '960px',
        //     // => @media (min-width: 768px) { ... }

        //     'lg': '1440px',
        //     // => @media (min-width: 1024px) { ... }

        //     '2xl': '1536px',
        //     // => @media (min-width: 1536px) { ... }

        //     // 'crmsm': '576px',
        //     // // => @media (min-width: 640px) { ... }

        //     // 'crmmd': '960px',
        //     // // => @media (min-width: 768px) { ... }

        //     // 'crmlg': '1440px',
        //     // // => @media (min-width: 1024px) { ... }

        //     // 'crm2xl': '1536px',
        //     // // => @media (min-width: 1536px) { ... }


        //     // 'tablet': '640px',
        //     // // => @media (min-width: 640px) { ... }

        //     // 'laptop': '1024px',
        //     // // => @media (min-width: 1024px) { ... }

        //     // 'desktop': '1280px',
        //     // // => @media (min-width: 1280px) { ... }
        // }
    },

    plugins: [require('@tailwindcss/forms'), require('@tailwindcss/typography')],
};
